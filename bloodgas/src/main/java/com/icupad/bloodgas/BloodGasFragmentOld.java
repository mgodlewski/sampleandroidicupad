package com.icupad.bloodgas;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.icupad.commons.ModuleFragment;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.grable.GrableView;
import com.icupad.grable.model.GrableMode;
import com.icupad.bloodgas.populator.BloodGasDataPopulator;

public class BloodGasFragmentOld extends ModuleFragment {
    private GrableView grableView;
    private Button tableGraphButton;
    private Button shiftToRightButton;
    private TextView informationTextView;

    private PublicIcupadRepository publicIcupadRepository;
    private BloodGasDataPopulator bloodGasDataPopulator;

    private boolean isSetGraph = false;
    private Long chosenPatientId;

    private final Handler handler = new Handler();

    public BloodGasFragmentOld() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blood_gas_fragment, container, false);
        publicIcupadRepository = fragmentListener.getRepository();
        chosenPatientId = fragmentListener.getChosenPaintId();
        bloodGasDataPopulator = new BloodGasDataPopulator(getActivity().getApplicationContext(), publicIcupadRepository);

        tableGraphButton = (Button) view.findViewById(R.id.viewTypeButton);
        shiftToRightButton = (Button) view.findViewById(R.id.shiftToRightButton);
        grableView = (GrableView) view.findViewById(R.id.canvasView);
        informationTextView = (TextView) view.findViewById(R.id.informationTextView);

        if(chosenPatientId != null) {
            informationTextView.setVisibility(View.GONE);
            tableGraphButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isSetGraph)
                        setupTable();
                    else
                        setupGraph();
                }
            });
            shiftToRightButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    grableView.setPosition(-1);
                }
            });

            new Thread(new Runnable(){
                @Override
                public void run(){
                    final GrableView tempGrableView = bloodGasDataPopulator.populate(
                            chosenPatientId, grableView);
                    handler.post(new Runnable(){
                        @Override
                        public void run(){
                            grableView = tempGrableView;
                            if(grableView.hasNoData()) {
                                informationTextView.setVisibility(View.VISIBLE);
                                informationTextView.setText(getResources().getString(R.string.no_data_about_patient));
                                tableGraphButton.setVisibility(View.GONE);
                                shiftToRightButton.setVisibility(View.GONE);
                            }
                            setupTable(); //has to be invoken to initialize size
                            fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
                        }
                    });
                }
            }).start();
        } else {
            informationTextView.setVisibility(View.VISIBLE);
            informationTextView.setText(getResources().getString(R.string.no_chosen_patient));
            tableGraphButton.setVisibility(View.GONE);
            shiftToRightButton.setVisibility(View.GONE);
            fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
        }

        return view;
    }

    private void setupTable() {
        if(isAdded()) {
            grableView.setVisibility(View.VISIBLE);
            tableGraphButton.setText(getResources().getString(R.string.graph_button));
            if (chosenPatientId != null) {
                grableView.setGrableMode(GrableMode.TABLE);
//                grableView.setInitiallyShift();
                grableView.invalidate();
            }
        }
        isSetGraph = false;
    }

    private void setupGraph() {
        if(isAdded()) {
            grableView.setVisibility(View.VISIBLE);
            tableGraphButton.setText(getResources().getString(R.string.table_button));
            if (chosenPatientId != null) {
                grableView.setGrableMode(GrableMode.GRAPH);
                grableView.invalidate();
            }
        }
        isSetGraph = true;
    }
}
