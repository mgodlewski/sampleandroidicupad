package com.icupad.bloodgas.populator;

import android.content.Context;
import android.graphics.Color;

import com.icupad.bloodgas.R;
import com.icupad.commons.repository.model.BloodGasMeasurement;
import com.icupad.commons.repository.model.Stay;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.commons.utils.function.StringSetToSortedStringListFunction;
import com.icupad.grable.GrableView;
import com.icupad.grable.model.Cell;
import com.icupad.commons.repository.PublicIcupadRepository;

import java.text.ParseException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class BloodGasDataPopulator {
    private static final String ABOVE_HIGH_NORM = "ABOVE_HIGH_NORM";
    private static final String BELOW_LOW_NORM = "BELOW_LOW_NORM";
    private PublicIcupadRepository publicIcupadRepository;
    private HalfDaysComputer halfDaysComputer;
    private String stayLabel;
    private String hospitalDayLabel;

    private Map<String, Integer> rowNameToRowId = new HashMap<>();
    private Map<String, Integer> columnNameToColumnId = new HashMap<>();
    private Map<String, Long> columnNameToStayId = new HashMap<>();
    private StringSetToSortedStringListFunction stringSetToSortedStringListFunction
            = new StringSetToSortedStringListFunction();

    public BloodGasDataPopulator(Context context, PublicIcupadRepository publicIcupadRepository) {
        this.publicIcupadRepository = publicIcupadRepository;
        this.halfDaysComputer = new HalfDaysComputer();
        stayLabel = context.getResources().getString(R.string.blood_gas_stay_label);
        hospitalDayLabel = context.getResources().getString(R.string.blood_gas_hospital_day_label);
    }

    public GrableView populate(Long patientId, GrableView grableView) {
//        List<BloodGasMeasurement> bloodGasMeasurements = publicIcupadRepository.getBloodGasMeasurementDOMs(patientId);

        Stay lastStay = publicIcupadRepository.getLastStay();
        List<BloodGasMeasurement> bloodGasMeasurements = publicIcupadRepository.getBloodGasTestMeasurement(lastStay.getId());

        populateRowsAndColumns(grableView, bloodGasMeasurements);
        populateData(grableView, bloodGasMeasurements);
        return grableView;
    }

    private void populateRowsAndColumns(GrableView grableView, List<BloodGasMeasurement> cells) {
        Set<String> rows = new HashSet<>();
        Set<String> columns = new TreeSet<>();

        for(BloodGasMeasurement cell : cells) {
            rows.add(getRowNameForCell(cell));
            columns.add(cell.getResultDate());
            columnNameToStayId.put(cell.getResultDate(), cell.getStayId());
        }

        populateColumns(grableView, columns);
        populateRows(grableView, stringSetToSortedStringListFunction.apply(rows));
    }

    private void populateColumns(GrableView grableView, Set<String> columns) {
        if(columns.isEmpty()) {
            return;
        }
        int i = 0;
        long lastStayId = columnNameToStayId.get(columns.toArray()[0]);
        int stayNo = 1;
        String firstInStayDateTime = (String) columns.toArray()[0];
        for(String dateTime : columns) {
            columnNameToColumnId.put(dateTime, i++);
            String[] columnLevels = new String[0];
            if(columnNameToStayId.get(dateTime) != lastStayId) {
                lastStayId = columnNameToStayId.get(dateTime);
                stayNo++;
                firstInStayDateTime = dateTime;
            }
            try {
                columnLevels = new String[]{
                        stayLabel + " " + stayNo,
                        hospitalDayLabel + " " + halfDaysComputer.compute(firstInStayDateTime, dateTime),
                        DateTimeFormatterHelper.getDateFromString(dateTime),
                        DateTimeFormatterHelper.getTimeFromString(dateTime)};
            } catch (ParseException e) {
                e.printStackTrace();
            }
            grableView.addColumn(columnLevels);
        }
    }

    private void populateRows(GrableView grableView, List<String> sortedRows) {
        for(int i = 0; i < sortedRows.size(); i++) {
            rowNameToRowId.put(sortedRows.get(i), i);
            grableView.addRow(sortedRows.get(i));
        }
    }

    private void populateData(GrableView grableView, List<BloodGasMeasurement> bloodGasMeasurements) {
        Cell[][] data = new Cell[rowNameToRowId.size()][columnNameToColumnId.size()];
        Integer backgroundColor;
        for(BloodGasMeasurement bloodGasMeasurement : bloodGasMeasurements) {
            backgroundColor = getBackgroundColorForCell(bloodGasMeasurement);
            Cell cell =  new Cell("" + bloodGasMeasurement.getValue(), backgroundColor);
            data[rowNameToRowId.get(getRowNameForCell(bloodGasMeasurement))][columnNameToColumnId.get(bloodGasMeasurement.getResultDate())] = cell;
        }
        grableView.addAllCells(data);
    }

    private Integer getBackgroundColorForCell(BloodGasMeasurement bloodGasCell) {
        if(bloodGasCell.getAbnormality() != null) {
            if(bloodGasCell.getAbnormality().equals(ABOVE_HIGH_NORM))
                return Color.RED;
            else if(bloodGasCell.getAbnormality().equals(BELOW_LOW_NORM))
                return Color.CYAN;
        }
        else if(bloodGasCell.getBottomDefaultNorm() != null && bloodGasCell.getTopDefaultNorm() != null
                && bloodGasCell.getBottomDefaultNorm() < bloodGasCell.getTopDefaultNorm()){
            if(bloodGasCell.getBottomDefaultNorm() > bloodGasCell.getValue())
                return Color.CYAN;
            if(bloodGasCell.getTopDefaultNorm() < bloodGasCell.getValue())
                return Color.RED;
        }
        return null;
    }

    private String getRowNameForCell(BloodGasMeasurement cell) {
        if(cell.getUnit() != null) {
            return cell.getName() + " [" + cell.getUnit() + "]";
        }
        else {
            return cell.getName();
        }
    }
}
