package com.icupad.bloodgas.populator;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HalfDaysComputer {
    private static final int START_HOSPITAL_DAY_HOUR = 7;
    private static final int INITIAL_HOSPITAL_DAY_NO = 0;
    private static final int NO_OF_HOSPITAL_DAYS_PER_REAL_DAY = 1;
    private static final int HOSPITAL_DAY_HOUR_LENGTH = 24;

    public int compute(String firstInStayDateTime, String now) {
        int halfDays = INITIAL_HOSPITAL_DAY_NO + NO_OF_HOSPITAL_DAYS_PER_REAL_DAY * getDaysBetween(firstInStayDateTime, now);
        halfDays = returnAdjustedHalfDaysForStartHour(halfDays, firstInStayDateTime);
        halfDays = returnAdjustedHalfDaysForNowHour(halfDays, now);
        return halfDays;
    }

    private int returnAdjustedHalfDaysForStartHour(int halfDays, String date) {
        int startHour = getHour(date);
        if(startHour < START_HOSPITAL_DAY_HOUR) {
            return halfDays + 1;
        }
        if(startHour > START_HOSPITAL_DAY_HOUR + HOSPITAL_DAY_HOUR_LENGTH) {
            return halfDays - 1;
        }
        return halfDays;
    }
    private int returnAdjustedHalfDaysForNowHour(int halfDays, String date) {
        int nowHour = getHour(date);
        if(nowHour < START_HOSPITAL_DAY_HOUR) {
            return halfDays - 1;
        }
        if(nowHour > START_HOSPITAL_DAY_HOUR + HOSPITAL_DAY_HOUR_LENGTH) {
            return halfDays + 1;
        }
        return halfDays;
    }

    private Integer getDaysBetween(String firstDateString, String secondDateString) {
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            DateTime firstDate = new DateTime(format.parse(firstDateString));
            DateTime secondDate = new DateTime(format.parse(secondDateString));
            return Days.daysBetween(firstDate, secondDate).getDays();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Integer getHour(String dateString) {
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date date = format.parse(dateString);
            DateFormat dateFormat = new SimpleDateFormat("HH");
            return Integer.valueOf(dateFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
