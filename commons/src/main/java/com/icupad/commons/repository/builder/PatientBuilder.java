package com.icupad.commons.repository.builder;

import com.icupad.commons.repository.model.Patient;

public final class PatientBuilder {
    private long id;
    private String name;
    private String surname;
    private String sex;
    private String birthDate;
    private String admitDate;
    private String dischargeDate;
    private String active;

    private PatientBuilder() {
    }

    public static PatientBuilder aPatient() {
        return new PatientBuilder();
    }

    public PatientBuilder withId(long id) {
        this.id = id;
        return this;
    }

    public PatientBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public PatientBuilder withSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public PatientBuilder withSex(String sex) {
        this.sex = sex;
        return this;
    }

    public PatientBuilder withBirthDate(String birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public PatientBuilder withAdmitDate(String admitDate) {
        this.admitDate = admitDate;
        return this;
    }

    public PatientBuilder withDischargeDate(String dischargeDate) {
        this.dischargeDate = dischargeDate;
        return this;
    }

    public PatientBuilder withActive(String active) {
        this.active = active;
        return this;
    }

    public Patient build() {
        Patient patient = new Patient();
        patient.setId(id);
        patient.setName(name);
        patient.setSurname(surname);
        patient.setSex(sex);
        patient.setBirthDate(birthDate);
        patient.setAdmitDate(admitDate);
        patient.setDischargeDate(dischargeDate);
        patient.setActive(active);
        return patient;
    }
}
