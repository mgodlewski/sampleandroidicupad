package com.icupad.commons.repository;

import com.icupad.commons.repository.model.AuscultatePointTest;
import com.icupad.commons.repository.model.AuscultateSuiteTest;
import com.icupad.commons.repository.model.BloodGasMeasurement;
import com.icupad.commons.repository.model.ConflictGeneral;
import com.icupad.commons.repository.model.EcmoAct;
import com.icupad.commons.repository.model.EcmoConfiguration;
import com.icupad.commons.repository.model.EcmoParameter;
import com.icupad.commons.repository.model.EcmoPatient;
import com.icupad.commons.repository.model.EcmoProcedure;
import com.icupad.commons.repository.model.ListAuscultateExamination;
import com.icupad.commons.repository.model.Operation;
import com.icupad.commons.repository.model.Patient;
import com.icupad.commons.repository.model.PatientDetail;
import com.icupad.commons.repository.model.SpinnerAuscultateSuiteSchema;
import com.icupad.commons.repository.model.Stay;

import java.util.List;

public interface PublicIcupadRepository {
    List<BloodGasMeasurement> getCompleteBloodCountMeasurement(long stayId);
    List<BloodGasMeasurement> getDefaultMeasurement(long stayId);
    List<BloodGasMeasurement> getBloodGasTestMeasurement(long stayId);

    List<Patient> getAllPatients();
    PatientDetail getPatientDetail(long id);
    List<Stay> getPatientStays(long id);
    long getStayId();
    Stay getCurrentStay();
    void updatePatient(long patientId, PatientDetail patientDetail);
    List<Long> getAllStayByUserId();
    Stay getLastStay();

    void addOperation(Operation operation, long stayId);
    void updateOperation(Operation operation);
    void removeOperation(long operationInternalId);

    List<ListAuscultateExamination> findAuscultateExaminationByPatientId(long patientId);
    List<SpinnerAuscultateSuiteSchema> findSyncedAuscultateSuiteSchemasForSpinnerPreparingExamination();
    void saveAuscultateSuiteSchema(SpinnerAuscultateSuiteSchema spinnerAuscultateSuiteSchema);
    AuscultateSuiteTest getAuscultateSuiteWithPointsSchema(long suiteId);
    void saveAuscultateSuiteTest(AuscultateSuiteTest auscultateSuiteTest);
    void updateAuscultateSuite(AuscultateSuiteTest auscultateSuiteTest);
    void updateAuscultate(AuscultatePointTest auscultatePointTest);

    List<ConflictGeneral> getConflicts();

//    Long getEcmoProcedureIdByStayIdAndSartDate(Long stayId, String startDate);
    EcmoProcedure getEcmoProcedureById(Long procedureId);
    EcmoConfiguration getEcmoFullConfigurationByProcedureId(Long procedureId);
     List<EcmoConfiguration> getAllEcmoFullConfigurationsByPorcedureId(Long procedureId);
    Long saveEcmoProcedure(EcmoProcedure ecmoProcedure);
    void saveEcmoFullConfiguration(EcmoConfiguration ecmoConfiguration);
    void setActualFalseInEcmoConfigurationsByProcedureId(Long procedureId);
    void endEcmoProcedure(EcmoProcedure ecmoProcedure);
    Long saveAct(EcmoAct ecmoAct);
    Long saveEcmoPatient(EcmoPatient ecmoPatient);
    Long saveEcmoParameter(EcmoParameter ecmoParameter);

    List<EcmoParameter> getEcmoParameterByProcedureId(Long procedureId);
    List<EcmoPatient> getEcmoPatientByProcedureId(Long procedureId);
    List<EcmoAct> getEcmoActByProcedureId(Long procedureId);
}
