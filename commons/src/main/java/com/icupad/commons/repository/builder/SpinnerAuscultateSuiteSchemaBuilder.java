package com.icupad.commons.repository.builder;

import com.icupad.commons.repository.model.AuscultatePoint;
import com.icupad.commons.repository.model.SpinnerAuscultateSuiteSchema;

import java.util.List;

public final class SpinnerAuscultateSuiteSchemaBuilder {
    private long id;
    private String name;
    private List<AuscultatePoint> points;

    private SpinnerAuscultateSuiteSchemaBuilder() {
    }

    public static SpinnerAuscultateSuiteSchemaBuilder aSpinnerAuscultateSuiteSchema() {
        return new SpinnerAuscultateSuiteSchemaBuilder();
    }

    public SpinnerAuscultateSuiteSchemaBuilder withId(long id) {
        this.id = id;
        return this;
    }

    public SpinnerAuscultateSuiteSchemaBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public SpinnerAuscultateSuiteSchemaBuilder withPoints(List<AuscultatePoint> points) {
        this.points = points;
        return this;
    }

    public SpinnerAuscultateSuiteSchema build() {
        SpinnerAuscultateSuiteSchema spinnerAuscultateSuiteSchema = new SpinnerAuscultateSuiteSchema();
        spinnerAuscultateSuiteSchema.setId(id);
        spinnerAuscultateSuiteSchema.setName(name);
        spinnerAuscultateSuiteSchema.setPoints(points);
        return spinnerAuscultateSuiteSchema;
    }
}
