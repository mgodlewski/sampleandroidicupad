package com.icupad.commons.repository.builder;

import com.icupad.commons.repository.model.AuscultatePoint;
import com.icupad.commons.repository.model.AuscultatePointTest;

public final class AuscultatePointTestBuilder {
    private AuscultatePoint auscultatePoint;
    private String description;
    private String wavFile;
    private int pollResult;
    private Long internalId;

    private AuscultatePointTestBuilder() {
    }

    public static AuscultatePointTestBuilder anAuscultatePointTest() {
        return new AuscultatePointTestBuilder();
    }

    public AuscultatePointTestBuilder withAuscultatePoint(AuscultatePoint auscultatePoint) {
        this.auscultatePoint = auscultatePoint;
        return this;
    }

    public AuscultatePointTestBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public AuscultatePointTestBuilder withWavFile(String wavFile) {
        this.wavFile = wavFile;
        return this;
    }

    public AuscultatePointTestBuilder withPollResult(int pollResult) {
        this.pollResult = pollResult;
        return this;
    }

    public AuscultatePointTestBuilder withInternalId(Long internalId) {
        this.internalId = internalId;
        return this;
    }

    public AuscultatePointTest build() {
        AuscultatePointTest auscultatePointTest = new AuscultatePointTest();
        auscultatePointTest.setAuscultatePoint(auscultatePoint);
        auscultatePointTest.setDescription(description);
        auscultatePointTest.setWavFile(wavFile);
        auscultatePointTest.setPollResult(pollResult);
        auscultatePointTest.setInternalId(internalId);
        return auscultatePointTest;
    }
}
