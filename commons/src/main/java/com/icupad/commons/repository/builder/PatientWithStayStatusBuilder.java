package com.icupad.commons.repository.builder;

import com.icupad.commons.repository.model.PatientWithStayStatus;

public final class PatientWithStayStatusBuilder {
    private long patientId;
    private boolean stayStatus;

    private PatientWithStayStatusBuilder() {
    }

    public static PatientWithStayStatusBuilder aPatientWithStayStatus() {
        return new PatientWithStayStatusBuilder();
    }

    public PatientWithStayStatusBuilder withPatientId(long patientId) {
        this.patientId = patientId;
        return this;
    }

    public PatientWithStayStatusBuilder withStayStatus(boolean stayStatus) {
        this.stayStatus = stayStatus;
        return this;
    }

    public PatientWithStayStatus build() {
        PatientWithStayStatus patientWithStayStatus = new PatientWithStayStatus();
        patientWithStayStatus.setPatientId(patientId);
        patientWithStayStatus.setStayStatus(stayStatus);
        return patientWithStayStatus;
    }
}
