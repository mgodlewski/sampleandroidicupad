package com.icupad.commons.repository.model;

public class AuscultatePointTest {
    private AuscultatePoint auscultatePoint;
    private String description;
    private String wavFile;
    private int pollResult;
    private Long internalId;

    public AuscultatePointTest() {

    }

    public AuscultatePointTest(AuscultatePointTest input) {
        this.auscultatePoint = input.getAuscultatePoint();
        this.description = input.getDescription();
        this.wavFile = input.getWavFile();
        this.pollResult = input.getPollResult();
        this.internalId = input.getInternalId();
    }

    public AuscultatePoint getAuscultatePoint() {
        return auscultatePoint;
    }

    public void setAuscultatePoint(AuscultatePoint auscultatePoint) {
        this.auscultatePoint = auscultatePoint;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWavFile() {
        return wavFile;
    }

    public void setWavFile(String wavFile) {
        this.wavFile = wavFile;
    }

    public int getPollResult() {
        return pollResult;
    }

    public void setPollResult(int pollResult) {
        this.pollResult = pollResult;
    }

    public Long getInternalId() {
        return internalId;
    }

    public void setInternalId(Long internalId) {
        this.internalId = internalId;
    }
}
