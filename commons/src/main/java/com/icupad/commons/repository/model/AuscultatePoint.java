package com.icupad.commons.repository.model;

public class AuscultatePoint {
    private Long id;
    private String name;
    private int queueOrder;
    private double  x;
    private double y;
    private boolean isFront;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQueueOrder() {
        return queueOrder;
    }

    public void setQueueOrder(int queueOrder) {
        this.queueOrder = queueOrder;
    }

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isFront() {
        return isFront;
    }

    public void setIsFront(boolean front) {
        isFront = front;
    }
}
