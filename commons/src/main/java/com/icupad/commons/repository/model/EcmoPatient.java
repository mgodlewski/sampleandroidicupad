package com.icupad.commons.repository.model;

/**
 * Created by Marcin on 31.03.2017.
 */
public class EcmoPatient {
    private Long id;
    private Long procedureId;
    private Long createdBy;
    private String date;
    private Double sat;
    private Double abp;
    private Double vcp;
    private Double hr;
    private Double tempSurface;
    private Double tempCore;
    private Double diuresis;
    private Double ultrafiltraction;

    public EcmoPatient() {
    }

    public EcmoPatient(Long id, Long procedureId, Long createdBy, String date, Double sat, Double abp, Double vcp, Double hr, Double tempSurface, Double tempCore, Double diuresis, Double ultrafiltraction) {
        this.id = id;
        this.procedureId = procedureId;
        this.createdBy = createdBy;
        this.date = date;
        this.sat = sat;
        this.abp = abp;
        this.vcp = vcp;
        this.hr = hr;
        this.tempSurface = tempSurface;
        this.tempCore = tempCore;
        this.diuresis = diuresis;
        this.ultrafiltraction = ultrafiltraction;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProcedureId() {
        return procedureId;
    }

    public void setProcedureId(Long procedureId) {
        this.procedureId = procedureId;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getSat() {
        return sat;
    }

    public void setSat(Double sat) {
        this.sat = sat;
    }

    public Double getAbp() {
        return abp;
    }

    public void setAbp(Double abp) {
        this.abp = abp;
    }

    public Double getVcp() {
        return vcp;
    }

    public void setVcp(Double vcp) {
        this.vcp = vcp;
    }

    public Double getHr() {
        return hr;
    }

    public void setHr(Double hr) {
        this.hr = hr;
    }

    public Double getTempSurface() {
        return tempSurface;
    }

    public void setTempSurface(Double tempSurface) {
        this.tempSurface = tempSurface;
    }

    public Double getTempCore() {
        return tempCore;
    }

    public void setTempCore(Double tempCore) {
        this.tempCore = tempCore;
    }

    public Double getDiuresis() {
        return diuresis;
    }

    public void setDiuresis(Double diuresis) {
        this.diuresis = diuresis;
    }

    public Double getUltrafiltraction() {
        return ultrafiltraction;
    }

    public void setUltrafiltraction(Double ultrafiltraction) {
        this.ultrafiltraction = ultrafiltraction;
    }
}
