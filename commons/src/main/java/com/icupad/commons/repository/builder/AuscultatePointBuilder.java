package com.icupad.commons.repository.builder;

import com.icupad.commons.repository.model.AuscultatePoint;

public final class AuscultatePointBuilder {
    private Long id;
    private String name;
    private int queueOrder;
    private double x;
    private double y;
    private boolean isFront;

    private AuscultatePointBuilder() {
    }

    public static AuscultatePointBuilder anAuscultatePoint() {
        return new AuscultatePointBuilder();
    }

    public AuscultatePointBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public AuscultatePointBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public AuscultatePointBuilder withQueueOrder(int queueOrder) {
        this.queueOrder = queueOrder;
        return this;
    }

    public AuscultatePointBuilder withX(double x) {
        this.x = x;
        return this;
    }

    public AuscultatePointBuilder withY(double y) {
        this.y = y;
        return this;
    }

    public AuscultatePointBuilder withIsFront(boolean isFront) {
        this.isFront = isFront;
        return this;
    }

    public AuscultatePoint build() {
        AuscultatePoint auscultatePoint = new AuscultatePoint();
        auscultatePoint.setId(id);
        auscultatePoint.setName(name);
        auscultatePoint.setQueueOrder(queueOrder);
        auscultatePoint.setX(x);
        auscultatePoint.setY(y);
        auscultatePoint.setIsFront(isFront);
        return auscultatePoint;
    }
}
