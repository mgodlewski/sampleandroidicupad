package com.icupad.commons.repository.builder;

import com.icupad.commons.repository.model.Operation;

public final class OperationBuilder {
    private long internalId;
    private long id;
    private String operatingDate;
    private String description;
    private boolean archived;

    private OperationBuilder() {
    }

    public static OperationBuilder anOperation() {
        return new OperationBuilder();
    }

    public OperationBuilder withInternalId(long internalId) {
        this.internalId = internalId;
        return this;
    }

    public OperationBuilder withId(long id) {
        this.id = id;
        return this;
    }

    public OperationBuilder withOperatingDate(String operatingDate) {
        this.operatingDate = operatingDate;
        return this;
    }

    public OperationBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public OperationBuilder withArchived(boolean archived) {
        this.archived = archived;
        return this;
    }

    public Operation build() {
        Operation operation = new Operation();
        operation.setInternalId(internalId);
        operation.setId(id);
        operation.setOperatingDate(operatingDate);
        operation.setDescription(description);
        operation.setArchived(archived);
        return operation;
    }
}
