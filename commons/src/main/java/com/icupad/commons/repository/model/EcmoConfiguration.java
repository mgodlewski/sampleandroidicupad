package com.icupad.commons.repository.model;

import java.util.List;

/**
 * Created by Marcin on 22.03.2017.
 */
public class EcmoConfiguration {
    private Long id;
    private Long procedureId;
    private Long createdBy;
    private String mode;
    private String caniulationType;
    private String wentowanieLeftVentricle;
    private String arteryCannulaType;
    private Double arteryCannulaSize;
    private String oxygeneratorType;
    private Boolean actual;
    private String date;
    private List<String> cannulationStructures;
    private List<VeinCannula> veinCannulaList;

    public EcmoConfiguration() {
    }

    public EcmoConfiguration(Long id, Long procedureId, Long createdBy, String mode, String caniulationType, String wentowanieLeftVentricle, String arteryCannulaType, Double arteryCannulaSize, String oxygeneratorType, Boolean actual, String date, List<String> cannulationStructures, List<VeinCannula> veinCannulaList) {
        this.id = id;
        this.procedureId = procedureId;
        this.createdBy = createdBy;
        this.mode = mode;
        this.caniulationType = caniulationType;
        this.wentowanieLeftVentricle = wentowanieLeftVentricle;
        this.arteryCannulaType = arteryCannulaType;
        this.arteryCannulaSize = arteryCannulaSize;
        this.oxygeneratorType = oxygeneratorType;
        this.actual = actual;
        this.date = date;
        this.cannulationStructures = cannulationStructures;
        this.veinCannulaList = veinCannulaList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProcedureId() {
        return procedureId;
    }

    public void setProcedureId(Long procedureId) {
        this.procedureId = procedureId;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getCaniulationType() {
        return caniulationType;
    }

    public void setCaniulationType(String caniulationType) {
        this.caniulationType = caniulationType;
    }

    public String getWentowanieLeftVentricle() {
        return wentowanieLeftVentricle;
    }

    public void setWentowanieLeftVentricle(String wentowanieLeftVentricle) {
        this.wentowanieLeftVentricle = wentowanieLeftVentricle;
    }

    public String getArteryCannulaType() {
        return arteryCannulaType;
    }

    public void setArteryCannulaType(String arteryCannulaType) {
        this.arteryCannulaType = arteryCannulaType;
    }

    public Double getArteryCannulaSize() {
        return arteryCannulaSize;
    }

    public void setArteryCannulaSize(Double arteryCannulaSize) {
        this.arteryCannulaSize = arteryCannulaSize;
    }

    public String getOxygeneratorType() {
        return oxygeneratorType;
    }

    public void setOxygeneratorType(String oxygeneratorType) {
        this.oxygeneratorType = oxygeneratorType;
    }

    public Boolean getActual() {
        return actual;
    }

    public void setActual(Boolean actual) {
        this.actual = actual;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<String> getCannulationStructures() {
        return cannulationStructures;
    }

    public void setCannulationStructures(List<String> cannulationStructures) {
        this.cannulationStructures = cannulationStructures;
    }

    public List<VeinCannula> getVeinCannulaList() {
        return veinCannulaList;
    }

    public void setVeinCannulaList(List<VeinCannula> veinCannulaList) {
        this.veinCannulaList = veinCannulaList;
    }

    public String getStringCannulationStructures(){
        String result = "";
        for(String s : cannulationStructures){
            result += s + ", ";
        }
        if(result.length()>3) {
            result = result.substring(0, result.length() - 2);
        }
        return result;
    }

    public String getStringVeinCannulaList(){
        String result = "";
        for(VeinCannula v : veinCannulaList){
            result += v.getType() + ":" + v.getSize().toString() + ", ";
        }
        if(result.length()>3) {
            result = result.substring(0, result.length() - 2);
        }
        return result;
    }
}
