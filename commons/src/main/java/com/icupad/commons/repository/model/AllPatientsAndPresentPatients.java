package com.icupad.commons.repository.model;

import java.util.Set;

public class AllPatientsAndPresentPatients {
    private Set<Long> allPatientIds;
    private Set<Long> presentPatientIds;

    public AllPatientsAndPresentPatients(Set<Long> allPatientIds, Set<Long> presentPatientIds) {
        this.allPatientIds = allPatientIds;
        this.presentPatientIds = presentPatientIds;
    }

    public Set<Long> getAllPatientIds() {
        return allPatientIds;
    }

    public void setAllPatientIds(Set<Long> allPatientIds) {
        this.allPatientIds = allPatientIds;
    }

    public Set<Long> getPresentPatientIds() {
        return presentPatientIds;
    }

    public void setPresentPatientIds(Set<Long> presentPatientIds) {
        this.presentPatientIds = presentPatientIds;
    }
}
