package com.icupad.commons.repository.model;

/**
 * Created by Marcin on 22.03.2017.
 */
public class EcmoProcedure {
    private Long id;
    private Long stayId;
    private Long createdById;
    private Long endById;
    private String startCause;
    private String startDate;
    private String endCause;
    private String endDate;

    public EcmoProcedure() {
    }

    public EcmoProcedure(Long id, Long stayId, Long createdById, Long endById, String startCause, String startDate, String endCause, String endDate) {
        this.id = id;
        this.stayId = stayId;
        this.createdById = createdById;
        this.endById = endById;
        this.startCause = startCause;
        this.startDate = startDate;
        this.endCause = endCause;
        this.endDate = endDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStayId() {
        return stayId;
    }

    public void setStayId(Long stayId) {
        this.stayId = stayId;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public Long getEndById() {
        return endById;
    }

    public void setEndById(Long endById) {
        this.endById = endById;
    }

    public String getStartCause() {
        return startCause;
    }

    public void setStartCause(String startCause) {
        this.startCause = startCause;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndCause() {
        return endCause;
    }

    public void setEndCause(String endCause) {
        this.endCause = endCause;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
