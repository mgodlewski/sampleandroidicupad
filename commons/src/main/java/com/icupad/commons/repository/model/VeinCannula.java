package com.icupad.commons.repository.model;

/**
 * Created by Marcin on 22.03.2017.
 */
public class VeinCannula {
    private String type;
    private Double size;

    public VeinCannula() {
    }

    public VeinCannula(String type, Double size) {
        this.type = type;
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }
}
