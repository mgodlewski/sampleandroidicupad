package com.icupad.commons.repository.model;

public class ListAuscultateExamination {
    private long suiteInternalId;
    private long suiteSchemaId;
    private String dateTime;
    private String examinationName;
    private String executor;
    private String description;
    private int position;
    private int temperature;
    private boolean respirated;
    private boolean passiveOxygenTherapy;

    public long getSuiteInternalId() {
        return suiteInternalId;
    }

    public void setSuiteInternalId(long suiteInternalId) {
        this.suiteInternalId = suiteInternalId;
    }

    public long getSuiteSchemaId() {
        return suiteSchemaId;
    }

    public void setSuiteSchemaId(long suiteSchemaId) {
        this.suiteSchemaId = suiteSchemaId;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getExaminationName() {
        return examinationName;
    }

    public void setExaminationName(String examinationName) {
        this.examinationName = examinationName;
    }

    public String getExecutor() {
        return executor;
    }

    public void setExecutor(String executor) {
        this.executor = executor;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public boolean isRespirated() {
        return respirated;
    }

    public void setRespirated(boolean respirated) {
        this.respirated = respirated;
    }

    public boolean isPassiveOxygenTherapy() {
        return passiveOxygenTherapy;
    }

    public void setPassiveOxygenTherapy(boolean passiveOxygenTherapy) {
        this.passiveOxygenTherapy = passiveOxygenTherapy;
    }
}
