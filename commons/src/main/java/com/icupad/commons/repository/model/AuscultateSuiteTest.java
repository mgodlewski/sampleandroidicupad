package com.icupad.commons.repository.model;

import java.util.ArrayList;
import java.util.List;

public class AuscultateSuiteTest {
    private String name;
    private String description;
    private List<AuscultatePointTest> auscultatePointTests = new ArrayList<>();
    private int position;
    private double temperature;
    private boolean isRespirated;
    private boolean passiveOxygenTherapy;
    private long patientId;
    private String examinationDateTime;
    private long auscultateSuiteSchemaId;
    private Long suiteInternalId;

    public AuscultateSuiteTest() {
    }

    public AuscultateSuiteTest(AuscultateSuiteTest input) {
        this.name = input.getName();
        this.description = input.getDescription();
        this.position = input.getPosition();
        this.temperature = input.getTemperature();
        this.isRespirated = input.isRespirated();
        this.passiveOxygenTherapy = input.isPassiveOxygenTherapy();
        this.patientId = input.getPatientId();
        this.examinationDateTime = input.getExaminationDateTime();
        this.auscultateSuiteSchemaId = input.getAuscultateSuiteSchemaId();

        for(AuscultatePointTest apt : input.getAuscultatePointTests()) {
            auscultatePointTests.add(new AuscultatePointTest(apt));
        }
    }

    public AuscultateSuiteTest(String description, Long suiteInternalId) {
        this.description = description;
        this.suiteInternalId = suiteInternalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AuscultatePointTest> getAuscultatePointTests() {
        return auscultatePointTests;
    }

    public void addAuctultatePointTest(AuscultatePointTest auscultatePointTest) {
        this.auscultatePointTests.add(auscultatePointTest);
    }

    public void setAuscultatePointTests(List<AuscultatePointTest> auscultatePointTests) {
        this.auscultatePointTests = auscultatePointTests;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public boolean isRespirated() {
        return isRespirated;
    }

    public void setRespirated(boolean respirated) {
        isRespirated = respirated;
    }

    public boolean isPassiveOxygenTherapy() {
        return passiveOxygenTherapy;
    }

    public void setPassiveOxygenTherapy(boolean passiveOxygenTherapy) {
        this.passiveOxygenTherapy = passiveOxygenTherapy;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public String getExaminationDateTime() {
        return examinationDateTime;
    }

    public void setExaminationDateTime(String examinationDateTime) {
        this.examinationDateTime = examinationDateTime;
    }

    public long getAuscultateSuiteSchemaId() {
        return auscultateSuiteSchemaId;
    }

    public void setAuscultateSuiteSchemaId(long auscultateSuiteSchemaId) {
        this.auscultateSuiteSchemaId = auscultateSuiteSchemaId;
    }

    public Long getSuiteInternalId() {
        return suiteInternalId;
    }

    public void setSuiteInternalId(Long suiteInternalId) {
        this.suiteInternalId = suiteInternalId;
    }
}
