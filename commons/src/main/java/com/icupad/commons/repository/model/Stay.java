package com.icupad.commons.repository.model;

import java.util.ArrayList;
import java.util.List;

public class Stay {
    private long id;
    private String admitDate;
    private String dischargeDate;
    private List<Operation> operations = new ArrayList<>();

    public Stay(long id, String admitDate, String dischargeDate) {
        this.id = id;
        this.admitDate = admitDate;
        this.dischargeDate = dischargeDate;
    }
    public void addOperation(Operation operation) {
        operations.add(operation);
    }


    public long getId() {
        return id;
    }

    public String getAdmitDate() {
        return admitDate;
    }

    public String getDischargeDate() {
        return dischargeDate;
    }

    public List<Operation> getOperations() {
        return operations;
    }
}
