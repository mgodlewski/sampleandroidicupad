package com.icupad.commons.repository.model;

import java.util.List;

public class SpinnerAuscultateSuiteSchema {
    private long id;
    private String name;
    private List<AuscultatePoint> points;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<AuscultatePoint> getPoints() {
        return points;
    }

    public void setPoints(List<AuscultatePoint> points) {
        this.points = points;
    }

    public void addPoint(AuscultatePoint point) {
        this.points.add(point);
    }
}
