package com.icupad.commons.repository.builder;

import com.icupad.commons.repository.model.AuscultatePointTest;
import com.icupad.commons.repository.model.AuscultateSuiteTest;

import java.util.ArrayList;
import java.util.List;

public final class AuscultateSuiteTestBuilder {
    private String name;
    private String description;
    private List<AuscultatePointTest> auscultatePointTests = new ArrayList<>();
    private int position;
    private double temperature;
    private boolean passiveOxygenTherapy;
    private long patientId;
    private long auscultateSuiteSchemaId;
    private Long suiteInternalId;

    private AuscultateSuiteTestBuilder() {
    }

    public static AuscultateSuiteTestBuilder anAuscultateSuiteTest() {
        return new AuscultateSuiteTestBuilder();
    }

    public AuscultateSuiteTestBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public AuscultateSuiteTestBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public AuscultateSuiteTestBuilder withAuscultatePointTests(List<AuscultatePointTest> auscultatePointTests) {
        this.auscultatePointTests = auscultatePointTests;
        return this;
    }

    public AuscultateSuiteTestBuilder withPosition(int position) {
        this.position = position;
        return this;
    }

    public AuscultateSuiteTestBuilder withTemperature(double temperature) {
        this.temperature = temperature;
        return this;
    }

    public AuscultateSuiteTestBuilder withPassiveOxygenTherapy(boolean passiveOxygenTherapy) {
        this.passiveOxygenTherapy = passiveOxygenTherapy;
        return this;
    }

    public AuscultateSuiteTestBuilder withPatientId(long patientId) {
        this.patientId = patientId;
        return this;
    }

    public AuscultateSuiteTestBuilder withAuscultateSuiteSchemaId(long auscultateSuiteSchemaId) {
        this.auscultateSuiteSchemaId = auscultateSuiteSchemaId;
        return this;
    }

    public AuscultateSuiteTestBuilder withSuiteInternalId(Long suiteInternalId) {
        this.suiteInternalId = suiteInternalId;
        return this;
    }

    public AuscultateSuiteTest build() {
        AuscultateSuiteTest auscultateSuiteTest = new AuscultateSuiteTest();
        auscultateSuiteTest.setName(name);
        auscultateSuiteTest.setDescription(description);
        auscultateSuiteTest.setAuscultatePointTests(auscultatePointTests);
        auscultateSuiteTest.setPosition(position);
        auscultateSuiteTest.setTemperature(temperature);
        auscultateSuiteTest.setPassiveOxygenTherapy(passiveOxygenTherapy);
        auscultateSuiteTest.setPatientId(patientId);
        auscultateSuiteTest.setAuscultateSuiteSchemaId(auscultateSuiteSchemaId);
        auscultateSuiteTest.setSuiteInternalId(suiteInternalId);
        return auscultateSuiteTest;
    }
}
