package com.icupad.commons.repository.builder;

import com.icupad.commons.repository.model.ListAuscultateExamination;

public final class ListAuscultateExaminationBuilder {
    private long suiteInternalId;
    private long suiteSchemaId;
    private String dateTime;
    private String examinationName;
    private String executor;
    private String description;
    private int position;
    private int temperature;
    private boolean respirated;
    private boolean passiveOxygenTherapy;

    private ListAuscultateExaminationBuilder() {
    }

    public static ListAuscultateExaminationBuilder aListAuscultateExamination() {
        return new ListAuscultateExaminationBuilder();
    }

    public ListAuscultateExaminationBuilder withSuiteInternalId(long suiteId) {
        this.suiteInternalId = suiteId;
        return this;
    }

    public ListAuscultateExaminationBuilder withSuiteSchemaId(long suiteSchemaId) {
        this.suiteSchemaId = suiteSchemaId;
        return this;
    }

    public ListAuscultateExaminationBuilder withDateTime(String dateTime) {
        this.dateTime = dateTime;
        return this;
    }

    public ListAuscultateExaminationBuilder withExaminationName(String examinationName) {
        this.examinationName = examinationName;
        return this;
    }

    public ListAuscultateExaminationBuilder withExecutor(String executor) {
        this.executor = executor;
        return this;
    }

    public ListAuscultateExaminationBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public ListAuscultateExaminationBuilder withPosition(int position) {
        this.position = position;
        return this;
    }

    public ListAuscultateExaminationBuilder withTemperature(int temperature) {
        this.temperature = temperature;
        return this;
    }

    public ListAuscultateExaminationBuilder withRespirated(boolean respirated) {
        this.respirated = respirated;
        return this;
    }

    public ListAuscultateExaminationBuilder withPassiveOxygenTherapy(boolean passiveOxygenTherapy) {
        this.passiveOxygenTherapy = passiveOxygenTherapy;
        return this;
    }

    public ListAuscultateExamination build() {
        ListAuscultateExamination listAuscultateExamination = new ListAuscultateExamination();
        listAuscultateExamination.setSuiteInternalId(suiteInternalId);
        listAuscultateExamination.setSuiteSchemaId(suiteSchemaId);
        listAuscultateExamination.setDateTime(dateTime);
        listAuscultateExamination.setExaminationName(examinationName);
        listAuscultateExamination.setExecutor(executor);
        listAuscultateExamination.setDescription(description);
        listAuscultateExamination.setPosition(position);
        listAuscultateExamination.setTemperature(temperature);
        listAuscultateExamination.setRespirated(respirated);
        listAuscultateExamination.setPassiveOxygenTherapy(passiveOxygenTherapy);

        return listAuscultateExamination;
    }
}
