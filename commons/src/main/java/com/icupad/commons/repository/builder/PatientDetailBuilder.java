package com.icupad.commons.repository.builder;

import com.icupad.commons.repository.model.PatientDetail;

public final class PatientDetailBuilder {
    private String surname;
    private String name;
    private String sex;
    private String birthday;
    private String pesel;
    private String street;
    private String streetNumber;
    private String houseNumber;
    private String postalCode;
    private String city;
    private String height;
    private String weight;
    private String bloodType;
    private String allergies;
    private String otherImportantInformations;

    private PatientDetailBuilder() {
    }

    public static PatientDetailBuilder aPatientDetail() {
        return new PatientDetailBuilder();
    }

    public PatientDetailBuilder withSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public PatientDetailBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public PatientDetailBuilder withSex(String sex) {
        this.sex = sex;
        return this;
    }

    public PatientDetailBuilder withBirthday(String birthday) {
        this.birthday = birthday;
        return this;
    }

    public PatientDetailBuilder withPesel(String pesel) {
        this.pesel = pesel;
        return this;
    }

    public PatientDetailBuilder withStreet(String street) {
        this.street = street;
        return this;
    }

    public PatientDetailBuilder withStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
        return this;
    }

    public PatientDetailBuilder withHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
        return this;
    }

    public PatientDetailBuilder withPostalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public PatientDetailBuilder withCity(String city) {
        this.city = city;
        return this;
    }

    public PatientDetailBuilder withHeight(String height) {
        this.height = height;
        return this;
    }

    public PatientDetailBuilder withWeight(String weight) {
        this.weight = weight;
        return this;
    }

    public PatientDetailBuilder withBloodType(String bloodType) {
        this.bloodType = bloodType;
        return this;
    }

    public PatientDetailBuilder withAllergies(String allergies) {
        this.allergies = allergies;
        return this;
    }

    public PatientDetailBuilder withOtherImportantInformations(String otherImportantInformations) {
        this.otherImportantInformations = otherImportantInformations;
        return this;
    }

    public PatientDetail build() {
        PatientDetail patientDetail = new PatientDetail();
        patientDetail.setSurname(surname);
        patientDetail.setName(name);
        patientDetail.setSex(sex);
        patientDetail.setBirthday(birthday);
        patientDetail.setPesel(pesel);
        patientDetail.setStreet(street);
        patientDetail.setStreetNumber(streetNumber);
        patientDetail.setHouseNumber(houseNumber);
        patientDetail.setPostalCode(postalCode);
        patientDetail.setCity(city);
        patientDetail.setHeight(height);
        patientDetail.setWeight(weight);
        patientDetail.setBloodType(bloodType);
        patientDetail.setAllergies(allergies);
        patientDetail.setOtherImportantInformations(otherImportantInformations);
        return patientDetail;
    }
}
