package com.icupad.commons.repository.builder;

import com.icupad.commons.repository.model.ConflictGeneral;

public final class ConflictGeneralBuilder {
    private String tableName;
    private long localInternalId;
    private long remoteInternalId;

    private ConflictGeneralBuilder() {
    }

    public static ConflictGeneralBuilder aConflictGeneral() {
        return new ConflictGeneralBuilder();
    }

    public ConflictGeneralBuilder withTableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public ConflictGeneralBuilder withLocalInternalId(long localInternalId) {
        this.localInternalId = localInternalId;
        return this;
    }

    public ConflictGeneralBuilder withRemoteInternalId(long remoteInternalId) {
        this.remoteInternalId = remoteInternalId;
        return this;
    }

    public ConflictGeneral build() {
        ConflictGeneral conflictGeneral = new ConflictGeneral();
        conflictGeneral.setTableName(tableName);
        conflictGeneral.setLocalInternalId(localInternalId);
        conflictGeneral.setRemoteInternalId(remoteInternalId);
        return conflictGeneral;
    }
}
