package com.icupad.commons.repository.model;

public class PatientWithStayStatus {
    private long patientId;
    private boolean stayStatus;

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public boolean isStayStatus() {
        return stayStatus;
    }

    public void setStayStatus(boolean stayStatus) {
        this.stayStatus = stayStatus;
    }
}
