package com.icupad.commons.repository.model;

/**
 * Created by Marcin on 31.03.2017.
 */
public class EcmoAct {
    private Long id;
    private Long procedureId;
    private Long createdBy;
    private String date;
    private Double act;

    public EcmoAct() {
    }

    public EcmoAct(Long id, Long procedureId, Long createdBy, String date, Double act) {
        this.id = id;
        this.procedureId = procedureId;
        this.createdBy = createdBy;
        this.date = date;
        this.act = act;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProcedureId() {
        return procedureId;
    }

    public void setProcedureId(Long procedureId) {
        this.procedureId = procedureId;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getAct() {
        return act;
    }

    public void setAct(Double act) {
        this.act = act;
    }
}
