package com.icupad.commons.repository.model;

public class Operation {
    private long internalId;
    private long id;
    private String operatingDate;
    private String description;
    private boolean archived;

    public long getInternalId() {
        return internalId;
    }

    public void setInternalId(long internalId) {
        this.internalId = internalId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOperatingDate() {
        return operatingDate;
    }

    public void setOperatingDate(String operatingDate) {
        this.operatingDate = operatingDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }
}
