package com.icupad.commons.repository.model;

public class ConflictGeneral {
    private String tableName;
    private long localInternalId;
    private long remoteInternalId;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public long getLocalInternalId() {
        return localInternalId;
    }

    public void setLocalInternalId(long localInternalId) {
        this.localInternalId = localInternalId;
    }

    public long getRemoteInternalId() {
        return remoteInternalId;
    }

    public void setRemoteInternalId(long remoteInternalId) {
        this.remoteInternalId = remoteInternalId;
    }
}
