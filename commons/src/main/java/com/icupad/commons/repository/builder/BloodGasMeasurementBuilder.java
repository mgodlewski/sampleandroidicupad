package com.icupad.commons.repository.builder;

import com.icupad.commons.repository.model.BloodGasMeasurement;
import com.icupad.commons.utils.DateTimeFormatterHelper;

public final class BloodGasMeasurementBuilder {
    private String name;
    private String unit;
    private String bloodSource;
    private double value;
    private String abnormality;
    private String norm;
    private long patientId;
    private String admittingDoctorHl7id;
    private String admittingDoctorSurname;
    private String admittingDoctorName;
    private String executorHl7id;
    private String executorSurname;
    private String executorName;
    private String requestDate;
    private String resultDate;
    private Double bottomDefaultNorm;
    private Double topDefaultNorm;
    private long stayId;

    private BloodGasMeasurementBuilder() {
    }

    public static BloodGasMeasurementBuilder aBloodGasMeasurement() {
        return new BloodGasMeasurementBuilder();
    }

    public BloodGasMeasurementBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public BloodGasMeasurementBuilder withUnit(String unit) {
        this.unit = unit;
        return this;
    }

    public BloodGasMeasurementBuilder withBloodSource(String bloodSource) {
        this.bloodSource = bloodSource;
        return this;
    }

    public BloodGasMeasurementBuilder withValue(double value) {
        this.value = value;
        return this;
    }

    public BloodGasMeasurementBuilder withAbnormality(String abnormality) {
        this.abnormality = abnormality;
        return this;
    }

    public BloodGasMeasurementBuilder withNorm(String norm) {
        this.norm = norm;
        return this;
    }

    public BloodGasMeasurementBuilder withPatientId(long patientId) {
        this.patientId = patientId;
        return this;
    }

    public BloodGasMeasurementBuilder withAdmittingDoctorHl7id(String admittingDoctorHl7id) {
        this.admittingDoctorHl7id = admittingDoctorHl7id;
        return this;
    }

    public BloodGasMeasurementBuilder withAdmittingDoctorSurname(String admittingDoctorSurname) {
        this.admittingDoctorSurname = admittingDoctorSurname;
        return this;
    }

    public BloodGasMeasurementBuilder withAdmittingDoctorName(String admittingDoctorName) {
        this.admittingDoctorName = admittingDoctorName;
        return this;
    }

    public BloodGasMeasurementBuilder withExecutorHl7id(String executorHl7id) {
        this.executorHl7id = executorHl7id;
        return this;
    }

    public BloodGasMeasurementBuilder withExecutorSurname(String executorSurname) {
        this.executorSurname = executorSurname;
        return this;
    }

    public BloodGasMeasurementBuilder withExecutorName(String executorName) {
        this.executorName = executorName;
        return this;
    }

    public BloodGasMeasurementBuilder withRequestDate(String requestDate) {
        this.requestDate = requestDate;
        return this;
    }

    public BloodGasMeasurementBuilder withResultDate(String resultDate) {
        this.resultDate = resultDate;
        return this;
    }

    public BloodGasMeasurementBuilder withBottomDefaultNorm(Double bottomDefaultNorm) {
        this.bottomDefaultNorm = bottomDefaultNorm;
        return this;
    }

    public BloodGasMeasurementBuilder withTopDefaultNorm(Double topDefaultNorm) {
        this.topDefaultNorm = topDefaultNorm;
        return this;
    }

    public BloodGasMeasurementBuilder withStayId(long stayId) {
        this.stayId = stayId;
        return this;
    }

    public BloodGasMeasurement build() {
        BloodGasMeasurement bloodGasMeasurement = new BloodGasMeasurement();
        bloodGasMeasurement.setName(name);
        bloodGasMeasurement.setUnit(unit);
        bloodGasMeasurement.setBloodSource(bloodSource);
        bloodGasMeasurement.setValue(value);
        bloodGasMeasurement.setAbnormality(abnormality);
//        bloodGasMeasurement.setNorm(norm);
//        bloodGasMeasurement.setPatientId(patientId);
//        bloodGasMeasurement.setAdmittingDoctorHl7id(admittingDoctorHl7id);
//        bloodGasMeasurement.setAdmittingDoctorSurname(admittingDoctorSurname);
//        bloodGasMeasurement.setAdmittingDoctorName(admittingDoctorName);
//        bloodGasMeasurement.setExecutorHl7id(executorHl7id);
//        bloodGasMeasurement.setExecutorSurname(executorSurname);
//        bloodGasMeasurement.setExecutorName(executorName);
//        bloodGasMeasurement.setRequestDate(requestDate);
        bloodGasMeasurement.setResultDate(DateTimeFormatterHelper.getMillisFromString(resultDate));
        bloodGasMeasurement.setBottomDefaultNorm(bottomDefaultNorm);
        bloodGasMeasurement.setTopDefaultNorm(topDefaultNorm);
        bloodGasMeasurement.setStayId(stayId);
        return bloodGasMeasurement;
    }
}
