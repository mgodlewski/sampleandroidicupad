package com.icupad.commons.repository.model;

/**
 * Created by Marcin on 31.03.2017.
 */
public class EcmoParameter {
    private Long id;
    private Long procedureId;
    private Long createdBy;
    private String date;
    private Double rpm;
    private Double bloodFlow;
    private Double fiO2;
    private Double gasFlow;
    private Double p1;
    private Double p2;
    private Double p3;
    private Double delta;
    private Double heparinSuply;
    private String description;

    public EcmoParameter() {
    }

    public EcmoParameter(Long id, Long procedureId, Long createdBy, String date, Double rpm, Double bloodFlow, Double fiO2, Double gasFlow, Double p1, Double p2, Double p3, Double delta, Double heparinSuply, String description) {
        this.id = id;
        this.procedureId = procedureId;
        this.createdBy = createdBy;
        this.date = date;
        this.rpm = rpm;
        this.bloodFlow = bloodFlow;
        this.fiO2 = fiO2;
        this.gasFlow = gasFlow;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.delta = delta;
        this.heparinSuply = heparinSuply;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProcedureId() {
        return procedureId;
    }

    public void setProcedureId(Long procedureId) {
        this.procedureId = procedureId;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getRpm() {
        return rpm;
    }

    public void setRpm(Double rpm) {
        this.rpm = rpm;
    }

    public Double getBloodFlow() {
        return bloodFlow;
    }

    public void setBloodFlow(Double bloodFlow) {
        this.bloodFlow = bloodFlow;
    }

    public Double getFiO2() {
        return fiO2;
    }

    public void setFiO2(Double fiO2) {
        this.fiO2 = fiO2;
    }

    public Double getGasFlow() {
        return gasFlow;
    }

    public void setGasFlow(Double gasFlow) {
        this.gasFlow = gasFlow;
    }

    public Double getP1() {
        return p1;
    }

    public void setP1(Double p1) {
        this.p1 = p1;
    }

    public Double getP2() {
        return p2;
    }

    public void setP2(Double p2) {
        this.p2 = p2;
    }

    public Double getP3() {
        return p3;
    }

    public void setP3(Double p3) {
        this.p3 = p3;
    }

    public Double getDelta() {
        return delta;
    }

    public void setDelta(Double delta) {
        this.delta = delta;
    }

    public Double getHeparinSuply() {
        return heparinSuply;
    }

    public void setHeparinSuply(Double heparinSuply) {
        this.heparinSuply = heparinSuply;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
