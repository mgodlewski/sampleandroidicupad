package com.icupad.commons.activitycommunication;

import android.app.Fragment;

import com.icupad.commons.repository.PublicIcupadRepository;

import java.io.Serializable;

public interface FragmentListener extends Serializable{
    public void onButtonPressed(Message msg);

    public PublicIcupadRepository getRepository();

    public Long getChosenPaintId();

    public void replaceFragment(Fragment replacingFragment);
}
