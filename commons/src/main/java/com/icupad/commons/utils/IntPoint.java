package com.icupad.commons.utils;

import android.view.MotionEvent;

public class IntPoint {
    public int x;
    public int y;

    public IntPoint(Double x, Double y) {
        this.x = x.intValue();
        this.y = y.intValue();
    }

    public IntPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public IntPoint(float x, float y) {
        this.x = (int)x;
        this.y = (int)y;
    }
}
