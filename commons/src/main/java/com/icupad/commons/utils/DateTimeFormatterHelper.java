package com.icupad.commons.utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.DateTimeParser;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeFormatterHelper {
    private static DateTimeFormatter formatter;
    static {
        DateTimeParser[] parsers = {
                DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS").getParser(),
                DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").getParser()
        };
        formatter = new DateTimeFormatterBuilder().append(null, parsers).toFormatter();
    }
    public static String getCurrentDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = (new Date(System.currentTimeMillis()));
        return dateFormat.format(date);
    }
    public static Long getCurrentTimestamp() {
        return getMillisFromString(getCurrentDateTime());
    }

    public static Long getMillisFromString(String input) {
        if(input == null || input.equals("")) {
            return null;
        }
        else {
            DateTime date = formatter.parseDateTime(input);
            return date.getMillis();
        }
    }

    public static String getStringWithoutSecondFromString(String input) {
        if(input == null) {
            return null;
        }
        DateTime dateTime = formatter.parseDateTime(input);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
        return fmt.print(dateTime);
    }

    public static String getStringFromMillis(Long input) {
        if(input == null) {
            return null;
        }
        DateTime dateTime = new DateTime(input);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        return fmt.print(dateTime);
    }

    public static String getStringWithoutSecondFromMillis(Long input) {
        if(input == null) {
            return null;
        }
        DateTime dateTime = new DateTime(input);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
        return fmt.print(dateTime);
    }

    public static String getStringWithoutSecondFromMillisInTwoLine(Long input) {
        if(input == null) {
            return null;
        }
        DateTime dateTime = new DateTime(input);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
        String dt = fmt.print(dateTime);
        String[] dtTab = dt.split(" ");
        return " " + dtTab[0] + " " + "\n" + dtTab[1];
    }


    public static String getStringWithoutYearAndSecondFromMillis(Long input) {
        if(input == null) {
            return null;
        }
        DateTime dateTime = new DateTime(input);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd.MM HH:mm");
        return fmt.print(dateTime);
    }

    public static String getTimeFromString(String s) throws ParseException {
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date date = format.parse(s);
            DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            return timeFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
        }
        return "";
    }

    //true if d1 is after d2
    public static Boolean compareDate(String d1, String d2){
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = format.parse(d1);
            Date date2 = format.parse(d2);
            if(date1.after(date2)){
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getDateFromString(String s) {
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date date = format.parse(s);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return dateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
        }
        return "";
    }

    public static String transformStringToDate(String s) {
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = format.parse(s);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            return dateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
        }
        return null;
    }
}