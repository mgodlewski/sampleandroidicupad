package com.icupad.commons.utils;

import android.graphics.Rect;

public class MathHelper {
    public static boolean isInRange(Point point1, Point point2, float range) {
        return (Math.sqrt(Math.pow(point1.x - point2.x, 2) + Math.pow(point1.y - point2.y, 2))
                < range);
    }

    public static boolean isInRect(Point point, Rect rect) {
        return ((point.x > rect.left && point.x < rect.right) &&
                (point.y > rect.top && point.y < rect.bottom));
    }

    public static Long returnGreaterNotNullLong(Long maxValue, Long newValue) {
        if(maxValue == null || (newValue != null && newValue > maxValue)) {
            return newValue;
        }
        else {
            return maxValue;
        }
    }
}
