package com.icupad.commons.utils.function;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class StringSetToSortedStringListFunction {

    public List<String> apply(Set<String> input) {
        List<String> sortedRows = new ArrayList<>();
        sortedRows.addAll(input);
        Collections.sort(sortedRows);
        return sortedRows;
    }
}
