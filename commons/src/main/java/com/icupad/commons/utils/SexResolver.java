package com.icupad.commons.utils;

import android.content.Context;

import com.icupad.androidcommons.R;

public class SexResolver {
    public static String getCorrectSexString(Context context, String input) {
        if(input.equals("MALE")) {
            return context.getResources().getString(R.string.male);
        }
        if(input.equals("FEMALE")) {
            return context.getResources().getString(R.string.female);
        }
        return "---";
    }
}
