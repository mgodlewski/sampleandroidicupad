package com.icupad.commons.utils;

import android.view.MotionEvent;

public class Point {
    public Double x;
    public Double y;

    public int getIntX() {
        return x.intValue();
    }

    public int getIntY() {
        return y.intValue();
    }

    public Point(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public Point(int x, int y) {
        this.x = new Double(x);
        this.y = new Double(y);
    }

    public Point(float x, float y) {
        this.x = new Double(x);
        this.y = new Double(y);
    }

    public Point(MotionEvent event) {
        this.x = new Double(event.getX());
        this.y = new Double(event.getY());
    }

    public Point(Point point) {
        this.x = point.x;
        this.y = point.y;
    }
}
