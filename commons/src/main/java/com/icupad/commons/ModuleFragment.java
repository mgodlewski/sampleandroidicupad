package com.icupad.commons;

import android.app.Activity;
import android.app.Fragment;

import com.icupad.commons.activitycommunication.FragmentListener;

public class ModuleFragment extends Fragment {

    protected FragmentListener fragmentListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            fragmentListener = (FragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() +
                    " must implement ButtonPressListener interface");
        }
    }
}
