package com.icupad.webconnection.model;

public class ConnectionType {
    private boolean wifi;
    private boolean mobile;

    public ConnectionType(boolean wifi, boolean mobile) {
        this.wifi = wifi;
        this.mobile = mobile;
    }

    public boolean isWifi() {
        return wifi;
    }

    public boolean isMobile() {
        return mobile;
    }

    public boolean isInternet() {
        return this.wifi | this.mobile;
    }

    public String toString() {
        return "There is " + (wifi?"":"no ") + "wifi and there is " + (mobile?"":"no ") + "mobile";
    }
}
