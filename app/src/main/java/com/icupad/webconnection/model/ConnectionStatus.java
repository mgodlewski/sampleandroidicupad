package com.icupad.webconnection.model;

public enum ConnectionStatus {
    NO_CONNECTION, CONNECTION_TO_INTERNET, CONNECTION_TO_SERVER, AUTHENTICATED_CONNECTION_TO_SERVER
}
