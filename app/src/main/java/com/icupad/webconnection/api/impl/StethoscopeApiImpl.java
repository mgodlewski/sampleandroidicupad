package com.icupad.webconnection.api.impl;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.icupad.synchronization.upload.responsedto.stethoscope.AuscultatePointSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.stethoscope.AuscultateRecordingSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.stethoscope.AuscultateSuiteSchemaSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.stethoscope.AuscultateSuiteSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.stethoscope.AuscultateSyncResponseDto;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.dto.stethoscope.AuscultateDto;
import com.icupad.webconnection.dto.stethoscope.AuscultatePointDto;
import com.icupad.webconnection.dto.stethoscope.AuscultateSuiteDto;
import com.icupad.webconnection.dto.stethoscope.AuscultateSuiteSchemaDto;
import com.icupad.webconnection.service.RestConnection;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class StethoscopeApiImpl {
    public static final String AFTER_PARAM = BackendApi.AFTER_PARAM;
    public static final String BASE_PATH = "/Stethoscope";
    private static final String AUSCULTATE_PATH = "/Auscultate";
    private static final String AUSCULTATE_POINT_PATH = "/AuscultatePoint";
    private static final String AUSCULTATE_RECORDING_PATH = "/AuscultateRecording";
    private static final String AUSCULTATE_SUITE_PATH = "/AuscultateSuite";
    private static final String AUSCULTATE_SUITE_SCHEMA_PATH = "/AuscultateSuiteSchema";
    private static final String UPLOAD_PATH = "/upload";

    private Gson gson;
    private RestConnection restConnection;

    public StethoscopeApiImpl(Gson gson, RestConnection restConnection) {
        this.gson = gson;
        this.restConnection = restConnection;
    }

    public List<AuscultateDto> getAuscultatesAfter(long time, List<Long> patientIdsToUpdate) throws IOException {
        String path = BASE_PATH + AUSCULTATE_PATH + AFTER_PARAM + time;
        Type listType = new TypeToken<ArrayList<AuscultateDto>>(){}.getType();
        List<AuscultateDto> result = gson.fromJson(restConnection.postAndGetJson(path, gson.toJson(patientIdsToUpdate)), listType);
        return (result == null) ? new ArrayList<AuscultateDto>() : result;
    }

    public AuscultateSyncResponseDto uploadAuscultate(AuscultateDto auscultateDto) throws IOException {
        String path = BASE_PATH + AUSCULTATE_PATH + UPLOAD_PATH;
        String json = restConnection.postAndGetJson(path, gson.toJson(auscultateDto));
        return gson.fromJson(json, AuscultateSyncResponseDto.class);
    }

    public List<AuscultatePointDto> getAuscultatePointsAfter(long time) throws IOException {
        String path = BASE_PATH + AUSCULTATE_POINT_PATH + AFTER_PARAM + time;
        Type listType = new TypeToken<ArrayList<AuscultatePointDto>>(){}.getType();
        List<AuscultatePointDto> result = gson.fromJson(restConnection.getJson(path), listType);
        return (result == null) ? new ArrayList<AuscultatePointDto>() : result;
    }

    public List<AuscultateSuiteDto> getAuscultateSuitesAfter(long time, List<Long> patientIdsToUpdate) throws IOException {
        String path = BASE_PATH + AUSCULTATE_SUITE_PATH + AFTER_PARAM + time;
        Type listType = new TypeToken<ArrayList<AuscultateSuiteDto>>(){}.getType();
        List<AuscultateSuiteDto> result = gson.fromJson(restConnection.postAndGetJson(path, gson.toJson(patientIdsToUpdate)), listType);
        return (result == null) ? new ArrayList<AuscultateSuiteDto>() : result;
    }

    public AuscultateSuiteSyncResponseDto uploadAuscultateSuite(AuscultateSuiteDto auscultateSuiteDto)throws IOException {
        String path = BASE_PATH + AUSCULTATE_SUITE_PATH + UPLOAD_PATH;
        String json = restConnection.postAndGetJson(path, gson.toJson(auscultateSuiteDto));
        return gson.fromJson(json, AuscultateSuiteSyncResponseDto.class);
    }

    public List<AuscultateSuiteSchemaDto> getAuscultateSuiteSchemasAfter(long time) throws IOException {
        String path = BASE_PATH + AUSCULTATE_SUITE_SCHEMA_PATH + AFTER_PARAM + time;
        Type listType = new TypeToken<ArrayList<AuscultateSuiteSchemaDto>>(){}.getType();
        List<AuscultateSuiteSchemaDto> result = gson.fromJson(restConnection.getJson(path), listType);
        return (result == null) ? new ArrayList<AuscultateSuiteSchemaDto>() : result;
    }

    public AuscultateSuiteSchemaSyncResponseDto uploadAuscultateSuiteSchema(AuscultateSuiteSchemaDto auscultateSuiteSchemaDto) throws IOException {
        String path = BASE_PATH + AUSCULTATE_SUITE_SCHEMA_PATH;
        String json = restConnection.postAndGetJson(path, gson.toJson(auscultateSuiteSchemaDto));
        return gson.fromJson(json, AuscultateSuiteSchemaSyncResponseDto.class);
    }

    //TODO
    public File getAuscultateRecordingById(String name) throws IOException {
        return null;
    }

    public AuscultateRecordingSyncResponseDto uploadAuscultateRecording(File file) throws IOException {
        String path = BASE_PATH + AUSCULTATE_RECORDING_PATH;
        String json =  restConnection.postFileAndGetJson(path, file);
        return gson.fromJson(json, AuscultateRecordingSyncResponseDto.class);
    }

    public AuscultatePointSyncResponseDto uploadAuscultatePoint(AuscultatePointDto auscultatePointDto) throws IOException {
        String path = BASE_PATH + AUSCULTATE_POINT_PATH;
        String json = restConnection.postAndGetJson(path, gson.toJson(auscultatePointDto));
        return gson.fromJson(json, AuscultatePointSyncResponseDto.class);
    }
}
