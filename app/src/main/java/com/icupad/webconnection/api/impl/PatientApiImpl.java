package com.icupad.webconnection.api.impl;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.icupad.synchronization.upload.responsedto.patient.OperationSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.patient.PatientSyncResponseDto;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.dto.bloodgas.BloodGasTestDefaultNormsDto;
import com.icupad.webconnection.dto.bloodgas.BloodGasTestPanelResultDto;
import com.icupad.webconnection.dto.patient.OperationDto;
import com.icupad.webconnection.dto.patient.PatientDto;
import com.icupad.webconnection.dto.patient.StayDto;
import com.icupad.webconnection.service.RestConnection;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class PatientApiImpl {
    public static final String AFTER_MILLIS_PARAM = BackendApi.AFTER_PARAM;
    public static final String BASE_PATH = "/Patient";
    public static final String STAY_PATH = "/Stay";
    public static final String OPERATION_PATH = "/Operation";
    public static final String UPDATE_PATH = "/upload";

    private Gson gson;
    private RestConnection restConnection;

    public PatientApiImpl(Gson gson, RestConnection restConnection) {
        this.gson = gson;
        this.restConnection = restConnection;
    }

    public List<PatientDto> getPatientsAfter(long time) throws IOException {
        String path = BASE_PATH + AFTER_MILLIS_PARAM + time;
        Type listType = new TypeToken<ArrayList<PatientDto>>(){}.getType();
        List<PatientDto> result = gson.fromJson(restConnection.getJson(path), listType);
        return (result == null) ? new ArrayList<PatientDto>() : result;
    }

    public PatientSyncResponseDto uploadPatient(PatientDto patientDto) throws IOException  {
        String path = BASE_PATH;
        String json = restConnection.postAndGetJson(path, gson.toJson(patientDto));
        return gson.fromJson(json, PatientSyncResponseDto.class);
    }

    public List<StayDto> getStaysAfter(long time) throws IOException {
        String path = BASE_PATH + STAY_PATH + AFTER_MILLIS_PARAM + time;
        Type listType = new TypeToken<ArrayList<StayDto>>(){}.getType();
        List<StayDto> result = gson.fromJson(restConnection.getJson(path), listType);
        return (result == null) ? new ArrayList<StayDto>() : result;
    }

    public List<OperationDto> getOperationsAfterForPatientsWithIds(long time, List<Long> patientIdsToUpdate) throws IOException {
        String path = BASE_PATH + OPERATION_PATH + AFTER_MILLIS_PARAM + time;
        Type listType = new TypeToken<ArrayList<OperationDto>>(){}.getType();
        List<OperationDto> result = gson.fromJson(restConnection.postAndGetJson(path, gson.toJson(patientIdsToUpdate)), listType);
        return (result == null) ? new ArrayList<OperationDto>() : result;
    }

    public OperationSyncResponseDto uploadOperation(OperationDto operationDto) throws IOException {
        String path = BASE_PATH + OPERATION_PATH + UPDATE_PATH ;
        String json = restConnection.postAndGetJson(path, gson.toJson(operationDto));
        return gson.fromJson(json, OperationSyncResponseDto.class);
    }
}
