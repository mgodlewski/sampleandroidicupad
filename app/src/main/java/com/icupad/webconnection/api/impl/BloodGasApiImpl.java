package com.icupad.webconnection.api.impl;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.icupad.db.repository.modulerepository.service.bloodgas.BloodGasRepositoryService;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.dto.bloodgas.BloodGasTestDefaultNormsDto;
import com.icupad.webconnection.dto.bloodgas.BloodGasTestDto;
import com.icupad.webconnection.dto.bloodgas.BloodGasTestMeasurementDto;
import com.icupad.webconnection.dto.bloodgas.BloodGasTestPanelResultDto;
import com.icupad.webconnection.dto.bloodgas.BloodGasTestRequestDto;
import com.icupad.webconnection.dto.bloodgas.BloodGasTestResultDto;
import com.icupad.webconnection.service.RestConnection;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class BloodGasApiImpl {
    public static final String AFTER_MILLIS_PARAM = BackendApi.AFTER_PARAM;
    public static final String BASE_PATH = "/BloodGasTest";

    private Gson gson;
    private RestConnection restConnection;

    public BloodGasApiImpl(Gson gson, RestConnection restConnection) {
        this.gson = gson;
        this.restConnection = restConnection;
    }

    public List<BloodGasTestMeasurementDto> getMeasurementsAfterAndPatientIdIn(long time, List<Long> patientIds, int page) throws IOException {
        String path = BASE_PATH + AFTER_MILLIS_PARAM + time + BackendApi.PAGE + page;
        Type listType = new TypeToken<ArrayList<BloodGasTestMeasurementDto>>(){}.getType();
        List<BloodGasTestMeasurementDto> result = gson.fromJson(restConnection.postAndGetJson(path, gson.toJson(patientIds)), listType);
        return (result == null) ? new ArrayList<BloodGasTestMeasurementDto>() : result;
    }
}
