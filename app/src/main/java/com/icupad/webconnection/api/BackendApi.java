package com.icupad.webconnection.api;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.icupad.synchronization.upload.responsedto.ecmo.EcmoActSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.ecmo.EcmoConfigurationSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.ecmo.EcmoParameterSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.ecmo.EcmoPatientSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.ecmo.EcmoProcedureSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.stethoscope.AuscultatePointSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.stethoscope.AuscultateRecordingSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.stethoscope.AuscultateSuiteSchemaSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.stethoscope.AuscultateSuiteSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.stethoscope.AuscultateSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.patient.OperationSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.patient.PatientSyncResponseDto;
import com.icupad.webconnection.api.impl.BloodGasApiImpl;
import com.icupad.webconnection.api.impl.CompleteBloodCountApiImpl;
import com.icupad.webconnection.api.impl.DefaultTestApiImpl;
import com.icupad.webconnection.api.impl.EcmoApiImpl;
import com.icupad.webconnection.api.impl.ManagementApiImpl;
import com.icupad.webconnection.api.impl.PatientApiImpl;
import com.icupad.webconnection.api.impl.StethoscopeApiImpl;
import com.icupad.webconnection.dto.bloodgas.BloodGasTestDefaultNormsDto;
import com.icupad.webconnection.dto.bloodgas.BloodGasTestDto;
import com.icupad.webconnection.dto.bloodgas.BloodGasTestMeasurementDto;
import com.icupad.webconnection.dto.bloodgas.BloodGasTestPanelResultDto;
import com.icupad.webconnection.dto.bloodgas.BloodGasTestRequestDto;
import com.icupad.webconnection.dto.bloodgas.BloodGasTestResultDto;
import com.icupad.webconnection.dto.completebloodcount.CompleteBloodCountTestDto;
import com.icupad.webconnection.dto.completebloodcount.CompleteBloodCountTestMeasurementDto;
import com.icupad.webconnection.dto.completebloodcount.CompleteBloodCountTestPanelResultDto;
import com.icupad.webconnection.dto.completebloodcount.CompleteBloodCountTestRequestDto;
import com.icupad.webconnection.dto.completebloodcount.CompleteBloodCountTestResultDto;
import com.icupad.webconnection.dto.defaulttest.DefaultTestDto;
import com.icupad.webconnection.dto.defaulttest.DefaultTestMeasurementDto;
import com.icupad.webconnection.dto.defaulttest.DefaultTestPanelResultDto;
import com.icupad.webconnection.dto.defaulttest.DefaultTestRequestDto;
import com.icupad.webconnection.dto.defaulttest.DefaultTestResultDto;
import com.icupad.webconnection.dto.ecmo.EcmoActDto;
import com.icupad.webconnection.dto.ecmo.EcmoConfigurationDto;
import com.icupad.webconnection.dto.ecmo.EcmoParameterDto;
import com.icupad.webconnection.dto.ecmo.EcmoPatientDto;
import com.icupad.webconnection.dto.ecmo.EcmoProcedureDto;
import com.icupad.webconnection.dto.general.UserDto;
import com.icupad.webconnection.dto.patient.OperationDto;
import com.icupad.webconnection.dto.stethoscope.AuscultateDto;
import com.icupad.webconnection.dto.stethoscope.AuscultatePointDto;
import com.icupad.webconnection.dto.stethoscope.AuscultateSuiteDto;
import com.icupad.webconnection.dto.stethoscope.AuscultateSuiteSchemaDto;
import com.icupad.webconnection.dto.patient.PatientDto;
import com.icupad.webconnection.dto.patient.StayDto;
import com.icupad.webconnection.service.RestConnection;

import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

//TODO make interfaces and parent class to subclass by concrete api impls
public class BackendApi  implements Serializable {
    public static final String AFTER_PARAM = "?after=";
    public static final String PAGE = "&page=";

    private Gson gson;
    private RestConnection restConnection;

    private ManagementApiImpl managementApi;
    private BloodGasApiImpl bloodGasApi;
    private PatientApiImpl patientApi;
    private StethoscopeApiImpl stethoscopeApi;
    private EcmoApiImpl ecmoApi;
    private CompleteBloodCountApiImpl completeBloodCountApi;
    private DefaultTestApiImpl defaultTestApi;

    public BackendApi(Context context) {
        GsonBuilder builder = new GsonBuilder();
        // Register an adapter to manage the date types as long values
        builder.registerTypeAdapter(Timestamp.class, new JsonDeserializer<Timestamp>() {
            public Timestamp deserialize(JsonElement json, Type typeOfT,
                    JsonDeserializationContext context) throws JsonParseException {
                return new Timestamp(json.getAsJsonPrimitive().getAsLong());
            }
        });
        gson = builder.create();
        restConnection = new RestConnection(context);

        managementApi = new ManagementApiImpl(gson, restConnection);
        bloodGasApi = new BloodGasApiImpl(gson, restConnection);
        patientApi = new PatientApiImpl(gson, restConnection);
        stethoscopeApi = new StethoscopeApiImpl(gson, restConnection);
        ecmoApi = new EcmoApiImpl(gson, restConnection);
        completeBloodCountApi = new CompleteBloodCountApiImpl(gson, restConnection);
        defaultTestApi = new DefaultTestApiImpl(gson, restConnection);
    }

    public Boolean pingServer() throws IOException {
        //TODO change to managementApi
        return restConnection.authPing();
    }

    public String getSqlitePassword() throws IOException {
        return managementApi.getSqlitePassword();
    }

    public Long getIcupadLastTimestamp() throws IOException {
        return managementApi.getIcupadLastTimestamp();
    }

    public Long getEskulapLastTimestamp() throws IOException {
        return managementApi.getEskulapLastTimestamp();
    }

    public UserDto getLoggedUserInfo() throws IOException {
        return managementApi.getLoggedUserInfo();
    }

    public List<UserDto> getUsers() throws IOException {
        return managementApi.getUsers();
    }

    public UserDto addUser(UserDto userDto) throws IOException {
        return managementApi.addUser(userDto);
    }

    public UserDto updateUser(UserDto userDto) throws IOException {
        return managementApi.updateUser(userDto);
    }

    public List<PatientDto> getPatientsAfter(long icupadLastSyncedTimestamp) throws IOException {
        return patientApi.getPatientsAfter(icupadLastSyncedTimestamp);
    }

    public PatientSyncResponseDto uploadPatient(PatientDto patientDto) throws IOException  {
        return patientApi.uploadPatient(patientDto);
    }


    public List<StayDto> getStaysAfter(long icupadLastSyncedTimestamp) throws IOException {
        return patientApi.getStaysAfter(icupadLastSyncedTimestamp);
    }

    public List<OperationDto> getOperationsAfterForPatientsWithIds(long icupadLastSyncedTimestamp, List<Long> patientIdsToUpdate) throws IOException {
        return patientApi.getOperationsAfterForPatientsWithIds(icupadLastSyncedTimestamp, patientIdsToUpdate);
    }

    public OperationSyncResponseDto uploadOperation(OperationDto operationDto) throws IOException {
        return patientApi.uploadOperation(operationDto);
    }

    public List<AuscultateDto> getAuscultatesAfterForPatientsWithIds(long icupadLastSyncedTimestamp, List<Long> patientIdsToUpdate) throws IOException {
        return stethoscopeApi.getAuscultatesAfter(icupadLastSyncedTimestamp, patientIdsToUpdate);
    }

    public AuscultateSyncResponseDto uploadAuscultate(AuscultateDto auscultateDto) throws IOException {
        return stethoscopeApi.uploadAuscultate(auscultateDto);
    }

    public List<AuscultatePointDto> getAuscultatePointsAfter(long icupadLastSyncedTimestamp) throws IOException {
        return stethoscopeApi.getAuscultatePointsAfter(icupadLastSyncedTimestamp);
    }

    public List<AuscultateSuiteDto> getAuscultateSuitesAfterForPatientsWithIds(long icupadLastSyncedTimestamp, List<Long> patientIdsToUpdate) throws IOException {
        return stethoscopeApi.getAuscultateSuitesAfter(icupadLastSyncedTimestamp, patientIdsToUpdate);
    }

    public AuscultateSuiteSyncResponseDto uploadAuscultateSuite(AuscultateSuiteDto auscultateSuiteDto)throws IOException {
        return stethoscopeApi.uploadAuscultateSuite(auscultateSuiteDto);
    }

    public AuscultatePointSyncResponseDto uploadAuscultatePoint(AuscultatePointDto auscultatePointDto) throws IOException {
        return stethoscopeApi.uploadAuscultatePoint(auscultatePointDto);
    }

    public AuscultateSuiteSchemaSyncResponseDto uploadAuscultateSuiteSchema(AuscultateSuiteSchemaDto auscultateSuiteSchemaDto) throws IOException {
        return stethoscopeApi.uploadAuscultateSuiteSchema(auscultateSuiteSchemaDto);
    }

    public List<AuscultateSuiteSchemaDto> getAuscultateSuiteSchemasAfter(long icupadLastSyncedTimestamp) throws IOException {
        return stethoscopeApi.getAuscultateSuiteSchemasAfter(icupadLastSyncedTimestamp);
    }

    public File getAuscultateRecordingById(String name) throws IOException {
        return stethoscopeApi.getAuscultateRecordingById(name);
    }

    public AuscultateRecordingSyncResponseDto uploadAuscultateRecording(File file) throws IOException {
        return stethoscopeApi.uploadAuscultateRecording(file);
    }

    public List<BloodGasTestMeasurementDto> getBloodGasTestsAfterForPatientsWithIds(long icupadLastSyncedTimestamp, List<Long> patientIdsToUpdate, int page) throws IOException {
        return bloodGasApi.getMeasurementsAfterAndPatientIdIn(icupadLastSyncedTimestamp, patientIdsToUpdate, page);
    }

    public List<DefaultTestMeasurementDto> getDefaultTestsAfterForPatientsWithIds(long icupadLastSyncedTimestamp, List<Long> patientIdsToUpdate, int page) throws IOException {
        return defaultTestApi.getMeasurementsAfterAndPatientIdIn(icupadLastSyncedTimestamp, patientIdsToUpdate, page);
    }

    public List<CompleteBloodCountTestMeasurementDto> getCompleteBloodCountTestsAfterForPatientsWithIds(long icupadLastSyncedTimestamp, List<Long> patientIdsToUpdate, int page) throws IOException {
        return completeBloodCountApi.getMeasurementsAfterAndPatientIdIn(icupadLastSyncedTimestamp, patientIdsToUpdate, page);
    }

    //*********ecmo*********//
    public List<EcmoProcedureDto> getEcmoProcedureAfter(Long time, List<Long> patientIds, int page) throws IOException {
        return ecmoApi.getProcedureAfter(time, patientIds, page);
    }

    public List<EcmoConfigurationDto> getEcmoConfigurationAfter(Long time, List<Long> patientIds, int page) throws IOException {
        return ecmoApi.getConfigurationAfter(time, patientIds, page);
    }

    public List<EcmoPatientDto> getEcmoPatientAfter(Long time, List<Long> patientIds, int page) throws IOException  {
        return ecmoApi.getPatientAfter(time, patientIds, page);
    }

    public List<EcmoActDto> getEcmoActAfter(Long time, List<Long> patientIds, int page) throws IOException  {
        return ecmoApi.getActAfter(time, patientIds, page);
    }

    public List<EcmoParameterDto> getEcmoParameterAfter(Long time, List<Long> patientIds, int page) throws IOException  {
        return ecmoApi.getParameterAfter(time, patientIds, page);
    }

    public EcmoProcedureSyncResponseDto createEcmoProcedure(EcmoProcedureDto ecmoProcedureDto) throws IOException {
        return ecmoApi.createProcedure(ecmoProcedureDto);
    }

    public EcmoConfigurationSyncResponseDto createEcmoConfiguration(EcmoConfigurationDto ecmoConfigurationDto) throws IOException {
        return ecmoApi.createConfiguration(ecmoConfigurationDto);
    }

    public EcmoPatientSyncResponseDto createEcmoPatient(EcmoPatientDto ecmoPatientDto) throws IOException {
        return ecmoApi.createPatient(ecmoPatientDto);
    }

    public EcmoActSyncResponseDto createEcmoAct(EcmoActDto ecmoActDto) throws IOException {
        return ecmoApi.createAct(ecmoActDto);
    }

    public EcmoParameterSyncResponseDto createEcmoParameter(EcmoParameterDto ecmoParameterDto) throws IOException {
        return ecmoApi.createParameter(ecmoParameterDto);
    }
}
