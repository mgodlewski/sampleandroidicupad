package com.icupad.webconnection.api.impl;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.icupad.webconnection.api.BackendApi;
//import com.icupad.webconnection.dto.defaulttest.DefaultTestDefaultNormsDto;
import com.icupad.webconnection.dto.defaulttest.DefaultTestDto;
import com.icupad.webconnection.dto.defaulttest.DefaultTestMeasurementDto;
import com.icupad.webconnection.dto.defaulttest.DefaultTestPanelResultDto;
import com.icupad.webconnection.dto.defaulttest.DefaultTestRequestDto;
import com.icupad.webconnection.dto.defaulttest.DefaultTestResultDto;
import com.icupad.webconnection.service.RestConnection;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 23.05.2017.
 */
public class DefaultTestApiImpl {
    public static final String AFTER_MILLIS_PARAM = BackendApi.AFTER_PARAM;
    public static final String BASE_PATH = "/DefaultTest";

    private Gson gson;
    private RestConnection restConnection;

    public DefaultTestApiImpl(Gson gson, RestConnection restConnection) {
        this.gson = gson;
        this.restConnection = restConnection;
    }

    public List<DefaultTestMeasurementDto> getMeasurementsAfterAndPatientIdIn(long time, List<Long> patientIds, int page) throws IOException {
        String path = BASE_PATH + AFTER_MILLIS_PARAM + time + BackendApi.PAGE + page;
        Type listType = new TypeToken<ArrayList<DefaultTestMeasurementDto>>(){}.getType();
        List<DefaultTestMeasurementDto> result = gson.fromJson(restConnection.postAndGetJson(path, gson.toJson(patientIds)), listType);
        return (result == null) ? new ArrayList<DefaultTestMeasurementDto>() : result;
    }
}
