package com.icupad.webconnection.api.impl;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.icupad.synchronization.upload.responsedto.ecmo.EcmoActSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.ecmo.EcmoConfigurationSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.ecmo.EcmoParameterSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.ecmo.EcmoPatientSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.ecmo.EcmoProcedureSyncResponseDto;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.dto.bloodgas.BloodGasTestResultDto;
import com.icupad.webconnection.dto.ecmo.EcmoActDto;
import com.icupad.webconnection.dto.ecmo.EcmoConfigurationDto;
import com.icupad.webconnection.dto.ecmo.EcmoParameterDto;
import com.icupad.webconnection.dto.ecmo.EcmoPatientDto;
import com.icupad.webconnection.dto.ecmo.EcmoProcedureDto;
import com.icupad.webconnection.service.RestConnection;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 18.03.2017.
 */
//TODO refactor rest get methods
public class EcmoApiImpl implements Serializable {
    public static final String AFTER_MILLIS_PARAM = BackendApi.AFTER_PARAM;
    public static final String BASE_PATH = "/ecmo";
    public static final String CONFIGURATION_PATH = "/configuration";
    public static final String PATIENT_PATH = "/patient";
    public static final String ACT_PATH = "/act";
    public static final String PARAMETER_PATH = "/parameter";
    public static final String GET = "/get";

    private Gson gson;
    private RestConnection restConnection;

    public EcmoApiImpl(Gson gson, RestConnection restConnection) {
        this.gson = gson;
        this.restConnection = restConnection;
    }

    /**********GET**********/
    public List<EcmoProcedureDto> getProcedureAfter(Long time, List<Long> patientIds, int page) throws IOException {
        String path = BASE_PATH + GET + AFTER_MILLIS_PARAM + time + BackendApi.PAGE + page;
        Type listType = new TypeToken<ArrayList<EcmoProcedureDto>>(){}.getType();
        List<EcmoProcedureDto> result = gson.fromJson(restConnection.postAndGetJson(path, gson.toJson(patientIds)), listType);
        return (result == null) ? new ArrayList<EcmoProcedureDto>() : result;
    }

    public List<EcmoConfigurationDto> getConfigurationAfter(Long time, List<Long> patientIds, int page) throws IOException {
        String path = BASE_PATH + CONFIGURATION_PATH + GET  + AFTER_MILLIS_PARAM + time + BackendApi.PAGE + page;
        Type listType = new TypeToken<ArrayList<EcmoConfigurationDto>>(){}.getType();
        List<EcmoConfigurationDto> result = gson.fromJson(restConnection.postAndGetJson(path, gson.toJson(patientIds)), listType);
        return (result == null) ? new ArrayList<EcmoConfigurationDto>() : result;
    }

    public List<EcmoPatientDto> getPatientAfter(Long time, List<Long> patientIds, int page) throws IOException  {
        String path = BASE_PATH + PATIENT_PATH  + GET + AFTER_MILLIS_PARAM + time + BackendApi.PAGE + page;
        Type listType = new TypeToken<ArrayList<EcmoPatientDto>>(){}.getType();
        List<EcmoPatientDto> result = gson.fromJson(restConnection.postAndGetJson(path, gson.toJson(patientIds)), listType);
        return (result == null) ? new ArrayList<EcmoPatientDto>() : result;
    }

    public List<EcmoActDto> getActAfter(Long time, List<Long> patientIds, int page) throws IOException  {
        String path = BASE_PATH + ACT_PATH + GET + AFTER_MILLIS_PARAM + time + BackendApi.PAGE + page;
        Type listType = new TypeToken<ArrayList<EcmoActDto>>(){}.getType();
        List<EcmoActDto> result = gson.fromJson(restConnection.postAndGetJson(path, gson.toJson(patientIds)), listType);
        return (result == null) ? new ArrayList<EcmoActDto>() : result;
    }

    public List<EcmoParameterDto> getParameterAfter(Long time, List<Long> patientIds, int page) throws IOException  {
        String path = BASE_PATH + PARAMETER_PATH + GET + AFTER_MILLIS_PARAM + time + BackendApi.PAGE + page;
        Type listType = new TypeToken<ArrayList<EcmoParameterDto>>(){}.getType();
        List<EcmoParameterDto> result = gson.fromJson(restConnection.postAndGetJson(path, gson.toJson(patientIds)), listType);
        return (result == null) ? new ArrayList<EcmoParameterDto>() : result;
    }

    /**********POST**********/
    public EcmoProcedureSyncResponseDto createProcedure(EcmoProcedureDto ecmoProcedureDto) throws IOException {
        String path = BASE_PATH;
        String json = restConnection.postAndGetJson(path, gson.toJson(ecmoProcedureDto));
        return gson.fromJson(json, EcmoProcedureSyncResponseDto.class); //HttpStatus.OK
}

    public EcmoConfigurationSyncResponseDto createConfiguration(EcmoConfigurationDto ecmoConfigurationDto) throws IOException {
        String path = BASE_PATH + CONFIGURATION_PATH;
        String json = restConnection.postAndGetJson(path, gson.toJson(ecmoConfigurationDto));
        return gson.fromJson(json, EcmoConfigurationSyncResponseDto.class);    //HttpStatus.CREATED
    }

    public EcmoPatientSyncResponseDto createPatient(EcmoPatientDto ecmoPatientDto) throws IOException {
        String path = BASE_PATH + PATIENT_PATH;
        String json = restConnection.postAndGetJson(path, gson.toJson(ecmoPatientDto));
        return gson.fromJson(json, EcmoPatientSyncResponseDto.class);    //HttpStatus.CREATED
    }

    public EcmoActSyncResponseDto createAct(EcmoActDto ecmoActDto) throws IOException {
        String path = BASE_PATH + ACT_PATH;
        String json = restConnection.postAndGetJson(path, gson.toJson(ecmoActDto));
        return gson.fromJson(json, EcmoActSyncResponseDto.class);    //HttpStatus.CREATED
    }

    public EcmoParameterSyncResponseDto createParameter(EcmoParameterDto ecmoParameterDto) throws IOException {
        String path = BASE_PATH + PARAMETER_PATH;
        String json = restConnection.postAndGetJson(path, gson.toJson(ecmoParameterDto));
        return gson.fromJson(json, EcmoParameterSyncResponseDto.class);    //HttpStatus.CREATED
    }
}
