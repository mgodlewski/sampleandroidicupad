package com.icupad.webconnection.api.impl;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.icupad.webconnection.dto.general.UserDto;
import com.icupad.webconnection.service.RestConnection;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ManagementApiImpl {
    public static final String BASE_PATH = "/";
    public static final String SQLITE_PASSWORD_PATH = "SqlitePassword";
    public static final String LOGGED_USER_INFO = "loggedUserInfo";
    public static final String USER = "user";
    public static final String USERS_LIST = "user/all";
    private static final String ICUPAD_LAST_TIMESTAMP = "lastTimestamp/icupad";
    private static final String ESKULAP_LAST_TIMESTAMP = "lastTimestamp/eskulap";

    private Gson gson;
    private RestConnection restConnection;

    public ManagementApiImpl(Gson gson, RestConnection restConnection) {
        this.gson = gson;
        this.restConnection = restConnection;
    }

    public String getSqlitePassword() throws  IOException {
        String path = BASE_PATH + SQLITE_PASSWORD_PATH;
        try {
            return gson.fromJson(restConnection.getJson(path), String.class);
        } catch(JsonSyntaxException e) {
            return null;
        }
    }

    public UserDto getLoggedUserInfo() throws  IOException {
        String path = BASE_PATH + LOGGED_USER_INFO;
        try {
            return gson.fromJson(restConnection.getJson(path), UserDto.class);
        } catch(JsonSyntaxException e) {
            return null;
        }
    }

    public List<UserDto> getUsers() throws  IOException {
        String path = BASE_PATH + USERS_LIST;
        Type listType = new TypeToken<ArrayList<UserDto>>(){}.getType();
        List<UserDto> result = gson.fromJson(restConnection.getJson(path), listType);
        return (result == null) ? new ArrayList<UserDto>() : result;
    }

    public UserDto addUser(UserDto userDto) throws  IOException {
        String path = BASE_PATH + USER;
        String json = restConnection.postAndGetJson(path, gson.toJson(userDto));
        return gson.fromJson(json, UserDto.class);
    }

    public UserDto updateUser(UserDto userDto) throws  IOException {
        String path = BASE_PATH + USER + "/" + userDto.getLogin();
        String json = restConnection.postAndGetJson(path, gson.toJson(userDto));
        return gson.fromJson(json, UserDto.class);
    }

    public Long getIcupadLastTimestamp() throws  IOException {
        String path = BASE_PATH + ICUPAD_LAST_TIMESTAMP;
        try {
            return gson.fromJson(restConnection.getJson(path), Long.class);
        } catch(JsonSyntaxException e) {
            return null;
        }
    }

    public Long getEskulapLastTimestamp() throws  IOException {
        String path = BASE_PATH + ESKULAP_LAST_TIMESTAMP;
        try {
            return gson.fromJson(restConnection.getJson(path), Long.class);
        } catch(JsonSyntaxException e) {
            return null;
        }
    }
}
