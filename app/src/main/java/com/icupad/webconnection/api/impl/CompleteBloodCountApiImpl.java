package com.icupad.webconnection.api.impl;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.icupad.webconnection.api.BackendApi;
//import com.icupad.webconnection.dto.completebloodcount.TestDefaultNormsDto;
import com.icupad.webconnection.dto.completebloodcount.CompleteBloodCountTestDto;
import com.icupad.webconnection.dto.completebloodcount.CompleteBloodCountTestMeasurementDto;
import com.icupad.webconnection.dto.completebloodcount.CompleteBloodCountTestPanelResultDto;
import com.icupad.webconnection.dto.completebloodcount.CompleteBloodCountTestRequestDto;
import com.icupad.webconnection.dto.completebloodcount.CompleteBloodCountTestResultDto;
import com.icupad.webconnection.service.RestConnection;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 23.05.2017.
 */
public class CompleteBloodCountApiImpl {
    public static final String AFTER_MILLIS_PARAM = BackendApi.AFTER_PARAM;
    public static final String BASE_PATH = "/CompleteBloodCount";

    private Gson gson;
    private RestConnection restConnection;

    public CompleteBloodCountApiImpl(Gson gson, RestConnection restConnection) {
        this.gson = gson;
        this.restConnection = restConnection;
    }

    public List<CompleteBloodCountTestMeasurementDto> getMeasurementsAfterAndPatientIdIn(long time, List<Long> patientIds, int page) throws IOException {
        String path = BASE_PATH + AFTER_MILLIS_PARAM + time + BackendApi.PAGE + page;
        Type listType = new TypeToken<ArrayList<CompleteBloodCountTestMeasurementDto>>(){}.getType();
        List<CompleteBloodCountTestMeasurementDto> result = gson.fromJson(restConnection.postAndGetJson(path, gson.toJson(patientIds)), listType);
        return (result == null) ? new ArrayList<CompleteBloodCountTestMeasurementDto>() : result;
    }
}
