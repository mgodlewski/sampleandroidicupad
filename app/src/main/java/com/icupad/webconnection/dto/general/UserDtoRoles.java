package com.icupad.webconnection.dto.general;

import java.util.ArrayList;
import java.util.List;

public class UserDtoRoles {
    private static final String DOCTOR_ROLE = "Doctor";
    private static final String NURSE_ROLE = "Nurse";
    private static final String ADMIN_ROLE = "Admin";
    private static final String DEV_ROLE = "Dev";

    private boolean doctorRole = false;
    private boolean nurseRole = false;
    private boolean adminRole = false;
    private boolean devRole = false;

    public UserDtoRoles(List<String> roleStrings) {
        if(roleStrings == null) {
            return;
        }
        for(String roleString : roleStrings) {
            if(DOCTOR_ROLE.equals(roleString)) {
                doctorRole = true;
            }
            if(NURSE_ROLE.equals(roleString)) {
                nurseRole = true;
            }
            if(ADMIN_ROLE.equals(roleString)) {
                adminRole = true;
            }
            if(DEV_ROLE.equals(roleString)) {
                devRole = true;
            }
        }
    }

    public List<String> getStrings() {
        List<String> result = new ArrayList<>();
        if(doctorRole) {
            result.add(DOCTOR_ROLE);
        }
        if(nurseRole) {
            result.add(NURSE_ROLE);
        }
        if(adminRole) {
            result.add(ADMIN_ROLE);
        }
        if(devRole) {
            result.add(DEV_ROLE);
        }
        return result;
    }

    public boolean isDoctorRole() {
        return doctorRole;
    }

    public void setDoctorRole(boolean doctorRole) {
        this.doctorRole = doctorRole;
    }

    public boolean isNurseRole() {
        return nurseRole;
    }

    public void setNurseRole(boolean nurseRole) {
        this.nurseRole = nurseRole;
    }

    public boolean isAdminRole() {
        return adminRole;
    }

    public void setAdminRole(boolean adminRole) {
        this.adminRole = adminRole;
    }

    public boolean isDevRole() {
        return devRole;
    }

    public void setDevRole(boolean devRole) {
        this.devRole = devRole;
    }
}
