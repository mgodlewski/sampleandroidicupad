package com.icupad.webconnection.dto.bloodgas;

import com.google.gson.annotations.SerializedName;

public class BloodGasTestResultDto {
    @SerializedName("id")
    private long id;
    @SerializedName("stayId")
    private long stayId;
    @SerializedName("testRequestId")
    private long testRequestId;
    @SerializedName("resultDate")
    private long resultDate;
    @SerializedName("unit")
    private String unit;
    @SerializedName("value")
    private double value;
    @SerializedName("norm")
    private String norm;
    @SerializedName("abnormality")
    private String abnormality;
    @SerializedName("hl7Id")
    private String hl7Id;
    @SerializedName("executorHl7Id")
    private String executorHl7Id;
    @SerializedName("executorName")
    private String executorName;
    @SerializedName("executorSurname")
    private String executorSurname;

    @SerializedName("createdById")
    private Long createdById;
    @SerializedName("lastModifiedById")
    private Long lastModifiedById;
    @SerializedName("createdDateTime")
    private long createdDateTime;
    @SerializedName("lastModifiedDateTime")
    private long lastModifiedDateTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getStayId() {
        return stayId;
    }

    public void setStayId(long stayId) {
        this.stayId = stayId;
    }

    public long getTestRequestId() {
        return testRequestId;
    }

    public void setTestRequestId(long testRequestId) {
        this.testRequestId = testRequestId;
    }

    public long getResultDate() {
        return resultDate;
    }

    public void setResultDate(long resultDate) {
        this.resultDate = resultDate;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getNorm() {
        return norm;
    }

    public void setNorm(String norm) {
        this.norm = norm;
    }

    public String getAbnormality() {
        return abnormality;
    }

    public void setAbnormality(String abnormality) {
        this.abnormality = abnormality;
    }

    public String getHl7Id() {
        return hl7Id;
    }

    public void setHl7Id(String hl7Id) {
        this.hl7Id = hl7Id;
    }

    public String getExecutorHl7Id() {
        return executorHl7Id;
    }

    public void setExecutorHl7Id(String executorHl7Id) {
        this.executorHl7Id = executorHl7Id;
    }

    public String getExecutorName() {
        return executorName;
    }

    public void setExecutorName(String executorName) {
        this.executorName = executorName;
    }

    public String getExecutorSurname() {
        return executorSurname;
    }

    public void setExecutorSurname(String executorSurname) {
        this.executorSurname = executorSurname;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public Long getLastModifiedById() {
        return lastModifiedById;
    }

    public void setLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
    }

    public long getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(long createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public long getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }
}
