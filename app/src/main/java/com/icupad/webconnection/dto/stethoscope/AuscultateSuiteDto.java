package com.icupad.webconnection.dto.stethoscope;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AuscultateSuiteDto {
    @SerializedName("id")
    private Long id;
    @SerializedName("patientId")
    private long patientId;
    @SerializedName("description")
    private String description;
    @SerializedName("executorHl7Id")
    private String executorHl7Id;
    @SerializedName("executorName")
    private String executorName;
    @SerializedName("executorSurname")
    private String executorSurname;
    @SerializedName("auscultateSuiteSchemaId")
    private long auscultateSuiteSchemaId;
    @SerializedName("position")
    private long position;
    @SerializedName("temperature")
    private double temperature;
    @SerializedName("isRespirated")
    private boolean isRespirated;
    @SerializedName("passiveOxygenTherapy")
    private boolean passiveOxygenTherapy;
    @SerializedName("examinationDateTime")
    private long examinationDateTime;
    @SerializedName("createdById")
    private Long createdById;
    @SerializedName("createdDateTime")
    private Long createdDateTime;
    @SerializedName("lastModifiedById")
    private Long lastModifiedById;
    @SerializedName("lastModifiedDateTime")
    private Long lastModifiedDateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExecutorHl7Id() {
        return executorHl7Id;
    }

    public void setExecutorHl7Id(String executorHl7Id) {
        this.executorHl7Id = executorHl7Id;
    }

    public String getExecutorName() {
        return executorName;
    }

    public void setExecutorName(String executorName) {
        this.executorName = executorName;
    }

    public String getExecutorSurname() {
        return executorSurname;
    }

    public void setExecutorSurname(String executorSurname) {
        this.executorSurname = executorSurname;
    }

    public long getAuscultateSuiteSchemaId() {
        return auscultateSuiteSchemaId;
    }

    public void setAuscultateSuiteSchemaId(long auscultateSuiteSchemaId) {
        this.auscultateSuiteSchemaId = auscultateSuiteSchemaId;
    }

    public long getPosition() {
        return position;
    }

    public void setPosition(long position) {
        this.position = position;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public boolean getIsRespirated() {
        return isRespirated;
    }

    public void setIsRespirated(boolean respirated) {
        isRespirated = respirated;
    }

    public boolean getPassiveOxygenTherapy() {
        return passiveOxygenTherapy;
    }

    public void setPassiveOxygenTherapy(boolean passiveOxygenTherapy) {
        this.passiveOxygenTherapy = passiveOxygenTherapy;
    }

    public long getExaminationDateTime() {
        return examinationDateTime;
    }

    public void setExaminationDateTime(long examinationDateTime) {
        this.examinationDateTime = examinationDateTime;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public Long getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Long createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Long getLastModifiedById() {
        return lastModifiedById;
    }

    public void setLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
    }

    public Long getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(Long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }
}
