package com.icupad.webconnection.dto.defaulttest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Marcin on 23.05.2017.
 */
public class DefaultTestPanelResultDto {
    @SerializedName("id")
    private long id;
    @SerializedName("executorHl7Id")
    private String executorHl7Id;
    @SerializedName("executorName")
    private String executorName;
    @SerializedName("executorSurname")
    private String executorSurname;
    @SerializedName("requestDate")
    private long requestDate;

    @SerializedName("createdDateTime")
    private long createdDateTime;
    @SerializedName("lastModifiedDateTime")
    private long lastModifiedDateTime;

    public DefaultTestPanelResultDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getExecutorHl7Id() {
        return executorHl7Id;
    }

    public void setExecutorHl7Id(String executorHl7Id) {
        this.executorHl7Id = executorHl7Id;
    }

    public String getExecutorName() {
        return executorName;
    }

    public void setExecutorName(String executorName) {
        this.executorName = executorName;
    }

    public String getExecutorSurname() {
        return executorSurname;
    }

    public void setExecutorSurname(String executorSurname) {
        this.executorSurname = executorSurname;
    }

    public long getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(long requestDate) {
        this.requestDate = requestDate;
    }

    public long getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(long createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public long getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }
}
