package com.icupad.webconnection.dto.patient;

import com.google.gson.annotations.SerializedName;

public class OperationDto {
    @SerializedName("id")
    private Long id;
    @SerializedName("stayId")
    private long stayId;
    @SerializedName("operatingDate")
    private long operatingDate;
    @SerializedName("description")
    private String description;
    @SerializedName("archived")
    private boolean archived;

    @SerializedName("lastModifiedById")
    private Long lastModifiedById;
    @SerializedName("createdDateTime")
    private Long createdDateTime;
    @SerializedName("createdById")
    private Long createdById;
    @SerializedName("lastModifiedDateTime")
    private Long lastModifiedDateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getStayId() {
        return stayId;
    }

    public void setStayId(long stayId) {
        this.stayId = stayId;
    }

    public long getOperatingDate() {
        return operatingDate;
    }

    public void setOperatingDate(long operatingDate) {
        this.operatingDate = operatingDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Long getLastModifiedById() {
        return lastModifiedById;
    }

    public void setLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
    }

    public Long getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Long createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public Long getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(Long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }
}
