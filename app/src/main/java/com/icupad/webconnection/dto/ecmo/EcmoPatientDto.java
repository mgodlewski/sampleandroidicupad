package com.icupad.webconnection.dto.ecmo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Marcin on 18.03.2017.
 */
public class EcmoPatientDto extends Object {
    @SerializedName("id")
    private Long id;
    @SerializedName("procedureId")
    private Long procedureId;
    @SerializedName("createdBy")
    private Long createdBy;
    @SerializedName("date")
    private Long date;
    @SerializedName("sat")
    private Double sat;
    @SerializedName("abp")
    private Double abp;
    @SerializedName("vcp")
    private Double vcp;
    @SerializedName("hr")
    private Double hr;
    @SerializedName("tempSurface")
    private Double tempSurface;
    @SerializedName("tempCore")
    private Double tempCore;
    @SerializedName("diuresis")
    private Double diuresis;
    @SerializedName("ultrafiltraction")
    private Double ultrafiltraction;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProcedureId() {
        return procedureId;
    }

    public void setProcedureId(Long procedureId) {
        this.procedureId = procedureId;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Double getSat() {
        return sat;
    }

    public void setSat(Double sat) {
        this.sat = sat;
    }

    public Double getAbp() {
        return abp;
    }

    public void setAbp(Double abp) {
        this.abp = abp;
    }

    public Double getVcp() {
        return vcp;
    }

    public void setVcp(Double vcp) {
        this.vcp = vcp;
    }

    public Double getHr() {
        return hr;
    }

    public void setHr(Double hr) {
        this.hr = hr;
    }

    public Double getTempSurface() {
        return tempSurface;
    }

    public void setTempSurface(Double tempSurface) {
        this.tempSurface = tempSurface;
    }

    public Double getTempCore() {
        return tempCore;
    }

    public void setTempCore(Double tempCore) {
        this.tempCore = tempCore;
    }

    public Double getDiuresis() {
        return diuresis;
    }

    public void setDiuresis(Double diuresis) {
        this.diuresis = diuresis;
    }

    public Double getUltrafiltraction() {
        return ultrafiltraction;
    }

    public void setUltrafiltraction(Double ultrafiltraction) {
        this.ultrafiltraction = ultrafiltraction;
    }
}
