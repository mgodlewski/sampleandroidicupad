package com.icupad.webconnection.dto.patient;

import com.google.gson.annotations.SerializedName;

public class PatientDto extends Object{
    @SerializedName("id")
    private Long id;
    @SerializedName("address")
    private AddressDto address;
    @SerializedName("birthDate")
    private long birthDate;
    @SerializedName("hl7Id")
    private String hl7Id;
    @SerializedName("name")
    private String name;
    @SerializedName("pesel")
    private String pesel;
    @SerializedName("sex")
    private String sex;
    @SerializedName("surname")
    private String surname;

    @SerializedName("height")
    private String height;
    @SerializedName("weight")
    private String weight;
    @SerializedName("bloodType")
    private String bloodType;
    @SerializedName("allergies")
    private String allergies;
    @SerializedName("otherImportantInformations")
    private String otherImportantInformations;

    @SerializedName("createdDateTime")
    private Long createdDateTime;
    @SerializedName("lastModifiedDateTime")
    private Long lastModifiedDateTime;
    @SerializedName("createdById")
    private Long createdById;
    @SerializedName("lastModifiedById")
    private Long lastModifiedById;

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public String getHl7Id() {
        return hl7Id;
    }

    public void setHl7Id(String hl7Id) {
        this.hl7Id = hl7Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getAllergies() {
        return allergies;
    }

    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }

    public String getOtherImportantInformations() {
        return otherImportantInformations;
    }

    public void setOtherImportantInformations(String otherImportantInformations) {
        this.otherImportantInformations = otherImportantInformations;
    }

    public Long getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Long createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Long getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(Long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public Long getLastModifiedById() {
        return lastModifiedById;
    }

    public void setLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
    }
}
