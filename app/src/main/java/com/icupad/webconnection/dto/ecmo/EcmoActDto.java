package com.icupad.webconnection.dto.ecmo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Marcin on 18.03.2017.
 */
public class EcmoActDto extends Object {
    @SerializedName("id")
    private Long id;
    @SerializedName("procedureId")
    private Long procedureId;
    @SerializedName("createdBy")
    private Long createdBy;
    @SerializedName("date")
    private Long date;
    @SerializedName("act")
    private Double act;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProcedureId() {
        return procedureId;
    }

    public void setProcedureId(Long procedureId) {
        this.procedureId = procedureId;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Double getAct() {
        return act;
    }

    public void setAct(Double act) {
        this.act = act;
    }

}
