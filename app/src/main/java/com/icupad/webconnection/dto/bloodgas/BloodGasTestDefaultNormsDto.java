package com.icupad.webconnection.dto.bloodgas;

import com.google.gson.annotations.SerializedName;

public class BloodGasTestDefaultNormsDto {
    @SerializedName("id")
    private long id;
    @SerializedName("testId")
    private long testId;
    @SerializedName("sex")
    private String sex;
    @SerializedName("bottomMonthsAge")
    private double bottomMonthsAge;
    @SerializedName("topMonthsAge")
    private double topMonthsAge;
    @SerializedName("bottomDefaultNorm")
    private double bottomDefaultNorm;
    @SerializedName("topDefaultNorm")
    private double topDefaultNorm;

    @SerializedName("createdById")
    private Long createdById;
    @SerializedName("lastModifiedById")
    private Long lastModifiedById;
    @SerializedName("createdDateTime")
    private long createdDateTime;
    @SerializedName("lastModifiedDateTime")
    private long lastModifiedDateTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTestId() {
        return testId;
    }

    public void setTestId(long testId) {
        this.testId = testId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public double getBottomMonthsAge() {
        return bottomMonthsAge;
    }

    public void setBottomMonthsAge(double bottomMonthsAge) {
        this.bottomMonthsAge = bottomMonthsAge;
    }

    public double getTopMonthsAge() {
        return topMonthsAge;
    }

    public void setTopMonthsAge(double topMonthsAge) {
        this.topMonthsAge = topMonthsAge;
    }

    public double getBottomDefaultNorm() {
        return bottomDefaultNorm;
    }

    public void setBottomDefaultNorm(double bottomDefaultNorm) {
        this.bottomDefaultNorm = bottomDefaultNorm;
    }

    public double getTopDefaultNorm() {
        return topDefaultNorm;
    }

    public void setTopDefaultNorm(double topDefaultNorm) {
        this.topDefaultNorm = topDefaultNorm;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public Long getLastModifiedById() {
        return lastModifiedById;
    }

    public void setLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
    }

    public long getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(long createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public long getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }
}
