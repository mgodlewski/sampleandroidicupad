package com.icupad.webconnection.dto.stethoscope;

import com.google.gson.annotations.SerializedName;

public class AuscultateDto {
    @SerializedName("id")
    private Long id;
    @SerializedName("description")
    private String description;
    @SerializedName("wavName")
    private String wavName;
    @SerializedName("auscultatePointId")
    private long auscultatePointId;
    @SerializedName("auscultateSuiteId")
    private long auscultateSuiteId;
    @SerializedName("pollResult")
    private long pollResult;
    @SerializedName("createdById")
    private Long createdById;
    @SerializedName("createdDateTime")
    private Long createdDateTime;
    @SerializedName("lastModifiedById")
    private Long lastModifiedById;
    @SerializedName("lastModifiedDateTime")
    private Long lastModifiedDateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWavName() {
        return wavName;
    }

    public void setWavName(String wavName) {
        this.wavName = wavName;
    }

    public long getAuscultatePointId() {
        return auscultatePointId;
    }

    public void setAuscultatePointId(long auscultatePointId) {
        this.auscultatePointId = auscultatePointId;
    }

    public long getAuscultateSuiteId() {
        return auscultateSuiteId;
    }

    public void setAuscultateSuiteId(long auscultateSuiteId) {
        this.auscultateSuiteId = auscultateSuiteId;
    }

    public long getPollResult() {
        return pollResult;
    }

    public void setPollResult(long pollResult) {
        this.pollResult = pollResult;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public Long getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Long createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Long getLastModifiedById() {
        return lastModifiedById;
    }

    public void setLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
    }

    public Long getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(Long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }
}
