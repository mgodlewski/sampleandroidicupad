package com.icupad.webconnection.dto.ecmo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Marcin on 18.03.2017.
 */
public class VeinCannulaDto extends Object {
    @SerializedName("type")
    private String type;
    @SerializedName("size")
    private Double size;

    public VeinCannulaDto(String type, Double size) {
        this.type = type;
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }
}
