package com.icupad.webconnection.dto.stethoscope;

import com.google.gson.annotations.SerializedName;

public class AuscultatePointDto {
    @SerializedName("id")
    private Long id;
    @SerializedName("name")
    private String name;
    @SerializedName("queuePosition")
    private int queuePosition;
    @SerializedName("x")
    private double x;
    @SerializedName("y")
    private double y;
    @SerializedName("front")
    private boolean front;
    @SerializedName("auscultateSuiteSchemaId")
    private long auscultateSuiteSchemaId;
    @SerializedName("createdById")
    private Long createdById;
    @SerializedName("createdDateTime")
    private Long createdDateTime;
    @SerializedName("lastModifiedById")
    private Long lastModifiedById;
    @SerializedName("lastModifiedDateTime")
    private Long lastModifiedDateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQueuePosition() {
        return queuePosition;
    }

    public void setQueuePosition(int queuePosition) {
        this.queuePosition = queuePosition;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public boolean isFront() {
        return front;
    }

    public void setFront(boolean front) {
        this.front = front;
    }

    public long getAuscultateSuiteSchemaId() {
        return auscultateSuiteSchemaId;
    }

    public void setAuscultateSuiteSchemaId(long auscultateSuiteSchemaId) {
        this.auscultateSuiteSchemaId = auscultateSuiteSchemaId;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public Long getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Long createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Long getLastModifiedById() {
        return lastModifiedById;
    }

    public void setLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
    }

    public Long getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(Long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }
}
