package com.icupad.webconnection.dto.ecmo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Marcin on 18.03.2017.
 */
public class EcmoProcedureDto extends Object {
    @SerializedName("id")
    private Long id;
    @SerializedName("stay")
    private Long stayId;
    @SerializedName("createdBy")
    private Long createdById;
    @SerializedName("endBy")
    private Long endById;
    @SerializedName("startCause")
    private String startCause;
    @SerializedName("startDate")
    private Long startDate;
    @SerializedName("endCause")
    private String endCause;
    @SerializedName("endDate")
    private Long endDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStayId() {
        return stayId;
    }

    public void setStayId(Long stayId) {
        this.stayId = stayId;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public Long getEndById() {
        return endById;
    }

    public void setEndById(Long endById) {
        this.endById = endById;
    }

    public String getStartCause() {
        return startCause;
    }

    public void setStartCause(String startCause) {
        this.startCause = startCause;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public String getEndCause() {
        return endCause;
    }

    public void setEndCause(String endCause) {
        this.endCause = endCause;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }
}
