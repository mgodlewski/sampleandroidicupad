package com.icupad.webconnection.dto.ecmo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Marcin on 18.03.2017.
 */
public class EcmoParameterDto extends Object  {
    @SerializedName("id")
    private Long id;
    @SerializedName("procedureId")
    private Long procedureId;
    @SerializedName("createdBy")
    private Long createdBy;
    @SerializedName("date")
    private Long date;
    @SerializedName("rpm")
    private Double rpm;
    @SerializedName("bloodFlow")
    private Double bloodFlow;
    @SerializedName("fiO2")
    private Double fiO2;
    @SerializedName("gasFlow")
    private Double gasFlow;
    @SerializedName("p1")
    private Double p1;
    @SerializedName("p2")
    private Double p2;
    @SerializedName("p3")
    private Double p3;
    @SerializedName("delta")
    private Double delta;
    @SerializedName("heparinSuply")
    private Double heparinSuply;
    @SerializedName("description")
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProcedureId() {
        return procedureId;
    }

    public void setProcedureId(Long procedureId) {
        this.procedureId = procedureId;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Double getRpm() {
        return rpm;
    }

    public void setRpm(Double rpm) {
        this.rpm = rpm;
    }

    public Double getBloodFlow() {
        return bloodFlow;
    }

    public void setBloodFlow(Double bloodFlow) {
        this.bloodFlow = bloodFlow;
    }

    public Double getFiO2() {
        return fiO2;
    }

    public void setFiO2(Double fiO2) {
        this.fiO2 = fiO2;
    }

    public Double getGasFlow() {
        return gasFlow;
    }

    public void setGasFlow(Double gasFlow) {
        this.gasFlow = gasFlow;
    }

    public Double getP1() {
        return p1;
    }

    public void setP1(Double p1) {
        this.p1 = p1;
    }

    public Double getP2() {
        return p2;
    }

    public void setP2(Double p2) {
        this.p2 = p2;
    }

    public Double getP3() {
        return p3;
    }

    public void setP3(Double p3) {
        this.p3 = p3;
    }

    public Double getDelta() {
        return delta;
    }

    public void setDelta(Double delta) {
        this.delta = delta;
    }

    public Double getHeparinSuply() {
        return heparinSuply;
    }

    public void setHeparinSuply(Double heparinSuply) {
        this.heparinSuply = heparinSuply;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
