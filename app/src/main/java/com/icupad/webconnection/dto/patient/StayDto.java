package com.icupad.webconnection.dto.patient;

import com.google.gson.annotations.SerializedName;

public class StayDto {
    @SerializedName("id")
    private long id;
    @SerializedName("active")
    private boolean active;
    @SerializedName("admitDate")
    private Long admitDate;
    @SerializedName("admittingDoctorHl7Id")
    private String admittingDoctorHl7Id;
    @SerializedName("admittingDoctorName")
    private String admittingDoctorName;
    @SerializedName("admittingDoctorSurname")
    private String admittingDoctorSurname;
    @SerializedName("admittingDoctorNpwz")
    private String admittingDoctorNpwz;
    @SerializedName("assignedPatientLocationName")
    private String assignedPatientLocationName;
    @SerializedName("dischargeDate")
    private Long dischargeDate;
    @SerializedName("hl7Id")
    private String hl7Id;
    @SerializedName("stayType")
    private String stayType;
    @SerializedName("patientId")
    private long patientId;
    @SerializedName("createdDateTime")
    private Long createdDateTime;
    @SerializedName("lastModifiedDateTime")
    private Long lastModifiedDateTime;
    @SerializedName("createdById")
    private Long createdById;
    @SerializedName("lastModifiedById")
    private Long lastModifiedById;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getAdmitDate() {
        return admitDate;
    }

    public void setAdmitDate(Long admitDate) {
        this.admitDate = admitDate;
    }

    public String getAdmittingDoctorHl7Id() {
        return admittingDoctorHl7Id;
    }

    public void setAdmittingDoctorHl7Id(String admittingDoctorHl7Id) {
        this.admittingDoctorHl7Id = admittingDoctorHl7Id;
    }

    public String getAdmittingDoctorName() {
        return admittingDoctorName;
    }

    public void setAdmittingDoctorName(String admittingDoctorName) {
        this.admittingDoctorName = admittingDoctorName;
    }

    public String getAdmittingDoctorSurname() {
        return admittingDoctorSurname;
    }

    public void setAdmittingDoctorSurname(String admittingDoctorSurname) {
        this.admittingDoctorSurname = admittingDoctorSurname;
    }

    public String getAdmittingDoctorNpwz() {
        return admittingDoctorNpwz;
    }

    public void setAdmittingDoctorNpwz(String admittingDoctorNpwz) {
        this.admittingDoctorNpwz = admittingDoctorNpwz;
    }

    public String getAssignedPatientLocationName() {
        return assignedPatientLocationName;
    }

    public void setAssignedPatientLocationName(String assignedPatientLocationName) {
        this.assignedPatientLocationName = assignedPatientLocationName;
    }

    public Long getDischargeDate() {
        return dischargeDate;
    }

    public void setDischargeDate(Long dischargeDate) {
        this.dischargeDate = dischargeDate;
    }

    public String getHl7Id() {
        return hl7Id;
    }

    public void setHl7Id(String hl7Id) {
        this.hl7Id = hl7Id;
    }

    public String getStayType() {
        return stayType;
    }

    public void setStayType(String stayType) {
        this.stayType = stayType;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public Long getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Long createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Long getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(Long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public Long getLastModifiedById() {
        return lastModifiedById;
    }

    public void setLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
    }

    @Override
    public String toString() {
        return "StayDto{" +
                "id=" + id +
                ", active=" + active +
                ", admitDate=" + admitDate +
                ", admittingDoctorHl7Id='" + admittingDoctorHl7Id + '\'' +
                ", admittingDoctorName='" + admittingDoctorName + '\'' +
                ", admittingDoctorSurname='" + admittingDoctorSurname + '\'' +
                ", admittingDoctorNpwz='" + admittingDoctorNpwz + '\'' +
                ", assignedPatientLocationName='" + assignedPatientLocationName + '\'' +
                ", dischargeDate=" + dischargeDate +
                ", hl7Id='" + hl7Id + '\'' +
                ", stayType='" + stayType + '\'' +
                ", patientId=" + patientId +
                ", createdDateTime=" + createdDateTime +
                ", lastModifiedDateTime=" + lastModifiedDateTime +
                ", createdById=" + createdById +
                ", lastModifiedById=" + lastModifiedById +
                '}';
    }
}
