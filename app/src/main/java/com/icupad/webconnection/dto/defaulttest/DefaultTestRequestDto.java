package com.icupad.webconnection.dto.defaulttest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Marcin on 23.05.2017.
 */
public class DefaultTestRequestDto {
    @SerializedName("id")
    private long id;
    @SerializedName("testId")
    private long testId;
    @SerializedName("testPanelResultId")
    private long testPanelResultId;
    @SerializedName("hl7Id")
    private String hl7Id;
    @SerializedName("requestDate")
    private long requestDate;

    @SerializedName("createdDateTime")
    private long createdDateTime;
    @SerializedName("lastModifiedDateTime")
    private long lastModifiedDateTime;

    public DefaultTestRequestDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTestId() {
        return testId;
    }

    public void setTestId(long testId) {
        this.testId = testId;
    }

    public long getTestPanelResultId() {
        return testPanelResultId;
    }

    public void setTestPanelResultId(long testPanelResultId) {
        this.testPanelResultId = testPanelResultId;
    }

    public String getHl7Id() {
        return hl7Id;
    }

    public void setHl7Id(String hl7Id) {
        this.hl7Id = hl7Id;
    }

    public long getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(long requestDate) {
        this.requestDate = requestDate;
    }

    public long getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(long createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public long getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }
}
