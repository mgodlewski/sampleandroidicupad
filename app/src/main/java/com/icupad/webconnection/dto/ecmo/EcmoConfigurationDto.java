package com.icupad.webconnection.dto.ecmo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Marcin on 18.03.2017.
 */
public class EcmoConfigurationDto extends Object {
    @SerializedName("id")
    private Long id;
    @SerializedName("procedureId")
    private Long procedureId;
    @SerializedName("createdBy")
    private Long createdBy;
    @SerializedName("mode")
    private String mode;
    @SerializedName("caniulationType")
    private String caniulationType;
    @SerializedName("wentowanieLeftVentricle")
    private String wentowanieLeftVentricle;
    @SerializedName("arteryCannulaType")
    private String arteryCannulaType;
    @SerializedName("arteryCannulaSize")
    private Double arteryCannulaSize;
    @SerializedName("oxygeneratorType")
    private String oxygeneratorType;
    @SerializedName("actual")
    private Boolean actual;
    @SerializedName("date")
    private Long date;
    @SerializedName("cannulationStructures")
    private List<String> cannulationStructures;
    @SerializedName("veinCannulaList")
    private List<VeinCannulaDto> veinCannulaList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProcedureId() {
        return procedureId;
    }

    public void setProcedureId(Long procedureId) {
        this.procedureId = procedureId;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getCaniulationType() {
        return caniulationType;
    }

    public void setCaniulationType(String caniulationType) {
        this.caniulationType = caniulationType;
    }

    public String getWentowanieLeftVentricle() {
        return wentowanieLeftVentricle;
    }

    public void setWentowanieLeftVentricle(String wentowanieLeftVentricle) {
        this.wentowanieLeftVentricle = wentowanieLeftVentricle;
    }

    public String getArteryCannulaType() {
        return arteryCannulaType;
    }

    public void setArteryCannulaType(String arteryCannulaType) {
        this.arteryCannulaType = arteryCannulaType;
    }

    public Double getArteryCannulaSize() {
        return arteryCannulaSize;
    }

    public void setArteryCannulaSize(Double arteryCannulaSize) {
        this.arteryCannulaSize = arteryCannulaSize;
    }

    public String getOxygeneratorType() {
        return oxygeneratorType;
    }

    public void setOxygeneratorType(String oxygeneratorType) {
        this.oxygeneratorType = oxygeneratorType;
    }

    public Boolean getActual() {
        return actual;
    }

    public void setActual(Boolean actual) {
        this.actual = actual;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public List<String> getCannulationStructures() {
        return cannulationStructures;
    }

    public void setCannulationStructures(List<String> cannulationStructures) {
        this.cannulationStructures = cannulationStructures;
    }

    public List<VeinCannulaDto> getVeinCannulaList() {
        return veinCannulaList;
    }

    public void setVeinCannulaList(List<VeinCannulaDto> veinCannulaList) {
        this.veinCannulaList = veinCannulaList;
    }
}
