package com.icupad.webconnection.dto.completebloodcount;

/**
 * Created by Marcin on 10.06.2017.
 */
public class CompleteBloodCountTestMeasurementDto {
    private String name;
    private String unit;
    private String bloodSource;
    private double value;
    private String abnormality;
    private long resultDate;
    private Double bottomDefaultNorm;
    private Double topDefaultNorm;
    private long stayId;
    private long testId;
    private long birthDate;

    public CompleteBloodCountTestMeasurementDto() {
    }

    public CompleteBloodCountTestMeasurementDto(String name, String unit, String bloodSource, double value, String abnormality, long resultDate, Double bottomDefaultNorm, Double topDefaultNorm, long stayId, long testId, long birthDate) {
        this.name = name;
        this.unit = unit;
        this.bloodSource = bloodSource;
        this.value = value;
        this.abnormality = abnormality;
        this.resultDate = resultDate;
        this.bottomDefaultNorm = bottomDefaultNorm;
        this.topDefaultNorm = topDefaultNorm;
        this.stayId = stayId;
        this.testId = testId;
        this.birthDate = birthDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getBloodSource() {
        return bloodSource;
    }

    public void setBloodSource(String bloodSource) {
        this.bloodSource = bloodSource;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getAbnormality() {
        return abnormality;
    }

    public void setAbnormality(String abnormality) {
        this.abnormality = abnormality;
    }

    public long getResultDate() {
        return resultDate;
    }

    public void setResultDate(long resultDate) {
        this.resultDate = resultDate;
    }

    public Double getBottomDefaultNorm() {
        return bottomDefaultNorm;
    }

    public void setBottomDefaultNorm(Double bottomDefaultNorm) {
        this.bottomDefaultNorm = bottomDefaultNorm;
    }

    public Double getTopDefaultNorm() {
        return topDefaultNorm;
    }

    public void setTopDefaultNorm(Double topDefaultNorm) {
        this.topDefaultNorm = topDefaultNorm;
    }

    public long getStayId() {
        return stayId;
    }

    public void setStayId(long stayId) {
        this.stayId = stayId;
    }

    public long getTestId() {
        return testId;
    }

    public void setTestId(long testId) {
        this.testId = testId;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }
}
