package com.icupad.webconnection.builder.ecmo;

import com.icupad.webconnection.dto.ecmo.EcmoPatientDto;

/**
 * Created by Marcin on 18.03.2017.
 */
public final class EcmoPatientDtoBuilder {
    private Long id;
    private Long procedureId;
    private Long createdBy;
    private Long date;
    private Double sat;
    private Double abp;
    private Double vcp;
    private Double hr;
    private Double tempSurface;
    private Double tempCore;
    private Double diuresis;
    private Double ultrafiltraction;

    public EcmoPatientDtoBuilder() {
    }

    public static EcmoPatientDtoBuilder aEcmoPatientDtoBuilder() {
        return new EcmoPatientDtoBuilder();
    }

    public EcmoPatientDtoBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public EcmoPatientDtoBuilder withProcedureId(Long procedureId) {
        this.procedureId = procedureId;
        return this;
    }

    public EcmoPatientDtoBuilder withCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public EcmoPatientDtoBuilder withDate(Long date) {
        this.date = date;
        return this;
    }

    public EcmoPatientDtoBuilder withSat(Double sat) {
        this.sat = sat;
        return this;
    }

    public EcmoPatientDtoBuilder withAbp(Double abp) {
        this.abp = abp;
        return this;
    }

    public EcmoPatientDtoBuilder withVcp(Double vcp) {
        this.vcp = vcp;
        return this;
    }

    public EcmoPatientDtoBuilder withHr(Double hr) {
        this.hr = hr;
        return this;
    }

    public EcmoPatientDtoBuilder withTempSurface(Double tempSurface) {
        this.tempSurface = tempSurface;
        return this;
    }

    public EcmoPatientDtoBuilder withTempCore(Double tempCore) {
        this.tempCore = tempCore;
        return this;
    }

    public EcmoPatientDtoBuilder withDiuresis(Double diuresis) {
        this.diuresis = diuresis;
        return this;
    }

    public EcmoPatientDtoBuilder withUltrafiltraction(Double ultrafiltraction) {
        this.ultrafiltraction = ultrafiltraction;
        return this;
    }

    public EcmoPatientDto build(){
        EcmoPatientDto ecmoPatientDto = new EcmoPatientDto();
        ecmoPatientDto.setId(id);
        ecmoPatientDto.setProcedureId(procedureId);
        ecmoPatientDto.setCreatedBy(createdBy);
        ecmoPatientDto.setDate(date);
        ecmoPatientDto.setSat(sat);
        ecmoPatientDto.setAbp(abp);
        ecmoPatientDto.setVcp(vcp);
        ecmoPatientDto.setHr(hr);
        ecmoPatientDto.setTempCore(tempCore);
        ecmoPatientDto.setTempSurface(tempSurface);
        ecmoPatientDto.setDiuresis(diuresis);
        ecmoPatientDto.setUltrafiltraction(ultrafiltraction);
        return ecmoPatientDto;
    }
}
