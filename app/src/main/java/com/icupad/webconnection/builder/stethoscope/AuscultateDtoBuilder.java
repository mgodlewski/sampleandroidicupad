package com.icupad.webconnection.builder.stethoscope;

import com.icupad.webconnection.dto.stethoscope.AuscultateDto;

public final class AuscultateDtoBuilder {
    private Long id;
    private String description;
    private String wavName;
    private long auscultatePointId;
    private long auscultateSuiteId;
    private long pollResult;
    private Long createdById;
    private Long createdDateTime;
    private Long lastModifiedById;
    private Long lastModifiedDateTime;

    private AuscultateDtoBuilder() {
    }

    public static AuscultateDtoBuilder anAuscultateDto() {
        return new AuscultateDtoBuilder();
    }

    public AuscultateDtoBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public AuscultateDtoBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public AuscultateDtoBuilder withWavName(String wavName) {
        this.wavName = wavName;
        return this;
    }

    public AuscultateDtoBuilder withAuscultatePointId(long auscultatePointId) {
        this.auscultatePointId = auscultatePointId;
        return this;
    }

    public AuscultateDtoBuilder withAuscultateSuiteId(long auscultateSuiteId) {
        this.auscultateSuiteId = auscultateSuiteId;
        return this;
    }

    public AuscultateDtoBuilder withPollResult(long pollResult) {
        this.pollResult = pollResult;
        return this;
    }

    public AuscultateDtoBuilder withCreatedById(Long createdById) {
        this.createdById = createdById;
        return this;
    }

    public AuscultateDtoBuilder withCreatedDateTime(Long createdDateTime) {
        this.createdDateTime = createdDateTime;
        return this;
    }

    public AuscultateDtoBuilder withLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
        return this;
    }

    public AuscultateDtoBuilder withLastModifiedDateTime(Long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
        return this;
    }

    public AuscultateDto build() {
        AuscultateDto auscultateDto = new AuscultateDto();
        auscultateDto.setId(id);
        auscultateDto.setDescription(description);
        auscultateDto.setWavName(wavName);
        auscultateDto.setAuscultatePointId(auscultatePointId);
        auscultateDto.setAuscultateSuiteId(auscultateSuiteId);
        auscultateDto.setPollResult(pollResult);
        auscultateDto.setCreatedById(createdById);
        auscultateDto.setCreatedDateTime(createdDateTime);
        auscultateDto.setLastModifiedById(lastModifiedById);
        auscultateDto.setLastModifiedDateTime(lastModifiedDateTime);
        return auscultateDto;
    }
}
