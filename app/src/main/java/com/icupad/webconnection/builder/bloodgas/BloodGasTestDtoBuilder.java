package com.icupad.webconnection.builder.bloodgas;

import com.icupad.webconnection.dto.bloodgas.BloodGasTestDto;

public final class BloodGasTestDtoBuilder {
    private long id;
    private String name;
    private String unit;
    private Long createdById;
    private Long lastModifiedById;
    private long createdDateTime;
    private long lastModifiedDateTime;

    private BloodGasTestDtoBuilder() {
    }

    public static BloodGasTestDtoBuilder aBloodGasTestDto() {
        return new BloodGasTestDtoBuilder();
    }

    public BloodGasTestDtoBuilder withId(long id) {
        this.id = id;
        return this;
    }

    public BloodGasTestDtoBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public BloodGasTestDtoBuilder withUnit(String unit) {
        this.unit = unit;
        return this;
    }

    public BloodGasTestDtoBuilder withCreatedById(Long createdById) {
        this.createdById = createdById;
        return this;
    }

    public BloodGasTestDtoBuilder withLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
        return this;
    }

    public BloodGasTestDtoBuilder withCreatedDateTime(long createdDateTime) {
        this.createdDateTime = createdDateTime;
        return this;
    }

    public BloodGasTestDtoBuilder withLastModifiedDateTime(long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
        return this;
    }

    public BloodGasTestDto build() {
        BloodGasTestDto bloodGasTestDto = new BloodGasTestDto();
        bloodGasTestDto.setId(id);
        bloodGasTestDto.setName(name);
        bloodGasTestDto.setUnit(unit);
        bloodGasTestDto.setCreatedById(createdById);
        bloodGasTestDto.setLastModifiedById(lastModifiedById);
        bloodGasTestDto.setCreatedDateTime(createdDateTime);
        bloodGasTestDto.setLastModifiedDateTime(lastModifiedDateTime);
        return bloodGasTestDto;
    }
}
