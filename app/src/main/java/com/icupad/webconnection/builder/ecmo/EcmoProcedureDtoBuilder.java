package com.icupad.webconnection.builder.ecmo;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.webconnection.dto.ecmo.EcmoProcedureDto;

/**
 * Created by Marcin on 18.03.2017.
 */
public final class EcmoProcedureDtoBuilder {
    private Long id;
    private Long stayId;
    private Long createdById;
    private Long endById;
    private String startCause;
    private Long startDate;
    private String endCause;
    private Long endDate;

    public EcmoProcedureDtoBuilder() {
    }

    public static EcmoProcedureDtoBuilder aEcmoProcedureDtoBuilder() {
        return new EcmoProcedureDtoBuilder();
    }

    public EcmoProcedureDtoBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public EcmoProcedureDtoBuilder withStayId(Long stayId) {
        this.stayId = stayId;
        return this;
    }

    public EcmoProcedureDtoBuilder withCreatedById(Long createdById) {
        this.createdById = createdById;
        return this;
    }

    public EcmoProcedureDtoBuilder withEndById(Long endById) {
        this.endById = endById;
        return this;
    }

    public EcmoProcedureDtoBuilder withStartCause(String startCause) {
        this.startCause = startCause;
        return this;
    }

    public EcmoProcedureDtoBuilder withStartDate(Long startDate) {
        this.startDate = startDate;
        return this;
    }

    public EcmoProcedureDtoBuilder withEndCause(String endCause) {
        this.endCause = endCause;
        return this;
    }

    public EcmoProcedureDtoBuilder withEndDate(Long endDate) {
        this.endDate = endDate;
        return this;
    }

    public EcmoProcedureDto build(){
        EcmoProcedureDto ecmoProcedureDto = new EcmoProcedureDto();
        ecmoProcedureDto.setId(id);
        ecmoProcedureDto.setCreatedById(createdById);
        ecmoProcedureDto.setEndById(endById);
        ecmoProcedureDto.setEndCause(endCause);
        ecmoProcedureDto.setEndDate(endDate);
        ecmoProcedureDto.setStartCause(startCause);
        ecmoProcedureDto.setStartDate(startDate);
        ecmoProcedureDto.setStayId(stayId);
        return ecmoProcedureDto;
    }
}
