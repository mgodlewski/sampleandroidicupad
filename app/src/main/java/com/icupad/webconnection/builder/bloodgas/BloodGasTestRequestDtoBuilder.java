package com.icupad.webconnection.builder.bloodgas;

import com.icupad.webconnection.dto.bloodgas.BloodGasTestRequestDto;

public final class BloodGasTestRequestDtoBuilder {
    private long id;
    private long testId;
    private long testPanelResultId;
    private String hl7Id;
    private long requestDate;
    private Long createdById;
    private Long lastModifiedById;
    private long createdDateTime;
    private long lastModifiedDateTime;

    private BloodGasTestRequestDtoBuilder() {
    }

    public static BloodGasTestRequestDtoBuilder aBloodGasTestRequestDto() {
        return new BloodGasTestRequestDtoBuilder();
    }

    public BloodGasTestRequestDtoBuilder withId(long id) {
        this.id = id;
        return this;
    }

    public BloodGasTestRequestDtoBuilder withTestId(long testId) {
        this.testId = testId;
        return this;
    }

    public BloodGasTestRequestDtoBuilder withTestPanelResultId(long testPanelResultId) {
        this.testPanelResultId = testPanelResultId;
        return this;
    }

    public BloodGasTestRequestDtoBuilder withHl7Id(String hl7Id) {
        this.hl7Id = hl7Id;
        return this;
    }

    public BloodGasTestRequestDtoBuilder withRequestDate(long requestDate) {
        this.requestDate = requestDate;
        return this;
    }

    public BloodGasTestRequestDtoBuilder withCreatedById(Long createdById) {
        this.createdById = createdById;
        return this;
    }

    public BloodGasTestRequestDtoBuilder withLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
        return this;
    }

    public BloodGasTestRequestDtoBuilder withCreatedDateTime(long createdDateTime) {
        this.createdDateTime = createdDateTime;
        return this;
    }

    public BloodGasTestRequestDtoBuilder withLastModifiedDateTime(long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
        return this;
    }

    public BloodGasTestRequestDto build() {
        BloodGasTestRequestDto bloodGasTestRequestDto = new BloodGasTestRequestDto();
        bloodGasTestRequestDto.setId(id);
        bloodGasTestRequestDto.setTestId(testId);
        bloodGasTestRequestDto.setTestPanelResultId(testPanelResultId);
        bloodGasTestRequestDto.setHl7Id(hl7Id);
        bloodGasTestRequestDto.setRequestDate(requestDate);
        bloodGasTestRequestDto.setCreatedById(createdById);
        bloodGasTestRequestDto.setLastModifiedById(lastModifiedById);
        bloodGasTestRequestDto.setCreatedDateTime(createdDateTime);
        bloodGasTestRequestDto.setLastModifiedDateTime(lastModifiedDateTime);
        return bloodGasTestRequestDto;
    }
}
