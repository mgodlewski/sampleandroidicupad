package com.icupad.webconnection.builder.ecmo;

import com.icupad.webconnection.dto.ecmo.VeinCannulaDto;

/**
 * Created by Marcin on 18.03.2017.
 */
public final class VeinCannulaDtoBuilder {
    private String type;
    private Double size;

    public VeinCannulaDtoBuilder() {
    }

    public static VeinCannulaDtoBuilder aVeinCannulaDtoBuilder() {
        return new VeinCannulaDtoBuilder();
    }

    public VeinCannulaDtoBuilder withType(String type) {
        this.type = type;
        return this;
    }

    public VeinCannulaDtoBuilder withSize(Double size) {
        this.size = size;
        return this;
    }

    public VeinCannulaDto build(){
        VeinCannulaDto veinCannulaDto = new VeinCannulaDto(type, size);
        return veinCannulaDto;
    }
}
