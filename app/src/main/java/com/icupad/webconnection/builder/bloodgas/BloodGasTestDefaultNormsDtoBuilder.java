package com.icupad.webconnection.builder.bloodgas;

import com.icupad.webconnection.dto.bloodgas.BloodGasTestDefaultNormsDto;

public final class BloodGasTestDefaultNormsDtoBuilder {
    private long id;
    private long testId;
    private String sex;
    private double bottomMonthsAge;
    private double topMonthsAge;
    private double bottomDefaultNorm;
    private double topDefaultNorm;
    private Long createdById;
    private Long lastModifiedById;
    private long createdDateTime;
    private long lastModifiedDateTime;

    private BloodGasTestDefaultNormsDtoBuilder() {
    }

    public static BloodGasTestDefaultNormsDtoBuilder aBloodGasTestDefaultNormsDto() {
        return new BloodGasTestDefaultNormsDtoBuilder();
    }

    public BloodGasTestDefaultNormsDtoBuilder withId(long id) {
        this.id = id;
        return this;
    }

    public BloodGasTestDefaultNormsDtoBuilder withTestId(long testId) {
        this.testId = testId;
        return this;
    }

    public BloodGasTestDefaultNormsDtoBuilder withSex(String sex) {
        this.sex = sex;
        return this;
    }

    public BloodGasTestDefaultNormsDtoBuilder withBottomMonthsAge(double bottomMonthsAge) {
        this.bottomMonthsAge = bottomMonthsAge;
        return this;
    }

    public BloodGasTestDefaultNormsDtoBuilder withTopMonthsAge(double topMonthsAge) {
        this.topMonthsAge = topMonthsAge;
        return this;
    }

    public BloodGasTestDefaultNormsDtoBuilder withBottomDefaultNorm(double bottomDefaultNorm) {
        this.bottomDefaultNorm = bottomDefaultNorm;
        return this;
    }

    public BloodGasTestDefaultNormsDtoBuilder withTopDefaultNorm(double topDefaultNorm) {
        this.topDefaultNorm = topDefaultNorm;
        return this;
    }

    public BloodGasTestDefaultNormsDtoBuilder withCreatedById(Long createdById) {
        this.createdById = createdById;
        return this;
    }

    public BloodGasTestDefaultNormsDtoBuilder withLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
        return this;
    }

    public BloodGasTestDefaultNormsDtoBuilder withCreatedDateTime(long createdDateTime) {
        this.createdDateTime = createdDateTime;
        return this;
    }

    public BloodGasTestDefaultNormsDtoBuilder withLastModifiedDateTime(long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
        return this;
    }

    public BloodGasTestDefaultNormsDto build() {
        BloodGasTestDefaultNormsDto bloodGasTestDefaultNormsDto = new BloodGasTestDefaultNormsDto();
        bloodGasTestDefaultNormsDto.setId(id);
        bloodGasTestDefaultNormsDto.setTestId(testId);
        bloodGasTestDefaultNormsDto.setSex(sex);
        bloodGasTestDefaultNormsDto.setBottomMonthsAge(bottomMonthsAge);
        bloodGasTestDefaultNormsDto.setTopMonthsAge(topMonthsAge);
        bloodGasTestDefaultNormsDto.setBottomDefaultNorm(bottomDefaultNorm);
        bloodGasTestDefaultNormsDto.setTopDefaultNorm(topDefaultNorm);
        bloodGasTestDefaultNormsDto.setCreatedById(createdById);
        bloodGasTestDefaultNormsDto.setLastModifiedById(lastModifiedById);
        bloodGasTestDefaultNormsDto.setCreatedDateTime(createdDateTime);
        bloodGasTestDefaultNormsDto.setLastModifiedDateTime(lastModifiedDateTime);
        return bloodGasTestDefaultNormsDto;
    }
}
