package com.icupad.webconnection.builder.ecmo;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.webconnection.dto.ecmo.EcmoActDto;

/**
 * Created by Marcin on 18.03.2017.
 */
public final class EcmoActDtoBuilder {
    private Long id;
    private Long procedureId;
    private Long createdBy;
    private Long date;
    private Double act;

    public EcmoActDtoBuilder() {
    }

    public static EcmoActDtoBuilder aEcmoActDtoBuilder(){
        return new EcmoActDtoBuilder();
    }

    public EcmoActDtoBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public EcmoActDtoBuilder withProcedureId(Long procedureId) {
        this.procedureId = procedureId;
        return this;
    }

    public EcmoActDtoBuilder withCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public EcmoActDtoBuilder withDate(Long date) {
        this.date = date;
        return this;
    }

    public EcmoActDtoBuilder withAct(Double act) {
        this.act = act;
        return this;
    }

    public EcmoActDto build(){
        EcmoActDto ecmoActDto = new EcmoActDto();
        ecmoActDto.setAct(act);
        ecmoActDto.setCreatedBy(createdBy);
        ecmoActDto.setDate(date);
        ecmoActDto.setId(id);
        ecmoActDto.setProcedureId(procedureId);
        return ecmoActDto;
    }
}
