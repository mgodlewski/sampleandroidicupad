package com.icupad.webconnection.builder.ecmo;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.webconnection.dto.ecmo.EcmoParameterDto;

/**
 * Created by Marcin on 18.03.2017.
 */
public final class EcmoParameterDtoBuilder {
    private Long id;
    private Long procedureId;
    private Long createdBy;
    private Long date;
    private Double rpm;
    private Double bloodFlow;
    private Double fiO2;
    private Double gasFlow;
    private Double p1;
    private Double p2;
    private Double p3;
    private Double delta;
    private Double heparinSuply;
    private String description;

    public EcmoParameterDtoBuilder() {
    }

    public static EcmoParameterDtoBuilder aEcmoParameterDtoBuilder() {
        return new EcmoParameterDtoBuilder();
    }

    public EcmoParameterDtoBuilder withP3(Double p3) {
        this.p3 = p3;
        return this;
    }

    public EcmoParameterDtoBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public EcmoParameterDtoBuilder withProcedureId(Long procedureId) {
        this.procedureId = procedureId;
        return this;
    }

    public EcmoParameterDtoBuilder withCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public EcmoParameterDtoBuilder withDate(Long date) {
        this.date = date;
        return this;
    }

    public EcmoParameterDtoBuilder withRpm(Double rpm) {
        this.rpm = rpm;
        return this;
    }

    public EcmoParameterDtoBuilder withBloodFlow(Double bloodFlow) {
        this.bloodFlow = bloodFlow;
        return this;
    }

    public EcmoParameterDtoBuilder withFiO2(Double fiO2) {
        this.fiO2 = fiO2;
        return this;
    }

    public EcmoParameterDtoBuilder withGasFlow(Double gasFlow) {
        this.gasFlow = gasFlow;
        return this;
    }

    public EcmoParameterDtoBuilder withP1(Double p1) {
        this.p1 = p1;
        return this;
    }

    public EcmoParameterDtoBuilder withP2(Double p2) {
        this.p2 = p2;
        return this;
    }

    public EcmoParameterDtoBuilder withDelta(Double delta) {
        this.delta = delta;
        return this;
    }

    public EcmoParameterDtoBuilder withHeparinSuply(Double heparinSuply) {
        this.heparinSuply = heparinSuply;
        return this;
    }

    public EcmoParameterDtoBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public EcmoParameterDto build(){
        EcmoParameterDto ecmoParameterDto = new EcmoParameterDto();
        ecmoParameterDto.setId(id);
        ecmoParameterDto.setProcedureId(procedureId);
        ecmoParameterDto.setCreatedBy(createdBy);
        ecmoParameterDto.setDate(date);
        ecmoParameterDto.setRpm(rpm);
        ecmoParameterDto.setBloodFlow(bloodFlow);
        ecmoParameterDto.setFiO2(fiO2);
        ecmoParameterDto.setGasFlow(gasFlow);
        ecmoParameterDto.setP1(p1);
        ecmoParameterDto.setP2(p2);
        ecmoParameterDto.setP3(p3);
        ecmoParameterDto.setDelta(delta);
        ecmoParameterDto.setHeparinSuply(heparinSuply);
        ecmoParameterDto.setDescription(description);
        return ecmoParameterDto;
    }
}
