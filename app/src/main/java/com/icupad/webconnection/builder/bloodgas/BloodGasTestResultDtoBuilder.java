package com.icupad.webconnection.builder.bloodgas;

import com.icupad.webconnection.dto.bloodgas.BloodGasTestResultDto;

public final class BloodGasTestResultDtoBuilder {
    private long id;
    private long stayId;
    private long testRequestId;
    private long resultDate;
    private String unit;
    private double value;
    private String norm;
    private String abnormality;
    private String hl7Id;
    private String executorHl7Id;
    private String executorName;
    private String executorSurname;
    private Long createdById;
    private Long lastModifiedById;
    private long createdDateTime;
    private long lastModifiedDateTime;

    private BloodGasTestResultDtoBuilder() {
    }

    public static BloodGasTestResultDtoBuilder aBloodGasTestResultDto() {
        return new BloodGasTestResultDtoBuilder();
    }

    public BloodGasTestResultDtoBuilder withId(long id) {
        this.id = id;
        return this;
    }

    public BloodGasTestResultDtoBuilder withStayId(long stayId) {
        this.stayId = stayId;
        return this;
    }

    public BloodGasTestResultDtoBuilder withTestRequestId(long testRequestId) {
        this.testRequestId = testRequestId;
        return this;
    }

    public BloodGasTestResultDtoBuilder withResultDate(long resultDate) {
        this.resultDate = resultDate;
        return this;
    }

    public BloodGasTestResultDtoBuilder withUnit(String unit) {
        this.unit = unit;
        return this;
    }

    public BloodGasTestResultDtoBuilder withValue(double value) {
        this.value = value;
        return this;
    }

    public BloodGasTestResultDtoBuilder withNorm(String norm) {
        this.norm = norm;
        return this;
    }

    public BloodGasTestResultDtoBuilder withAbnormality(String abnormality) {
        this.abnormality = abnormality;
        return this;
    }

    public BloodGasTestResultDtoBuilder withHl7Id(String hl7Id) {
        this.hl7Id = hl7Id;
        return this;
    }

    public BloodGasTestResultDtoBuilder withExecutorHl7Id(String executorHl7Id) {
        this.executorHl7Id = executorHl7Id;
        return this;
    }

    public BloodGasTestResultDtoBuilder withExecutorName(String executorName) {
        this.executorName = executorName;
        return this;
    }

    public BloodGasTestResultDtoBuilder withExecutorSurname(String executorSurname) {
        this.executorSurname = executorSurname;
        return this;
    }

    public BloodGasTestResultDtoBuilder withCreatedById(Long createdById) {
        this.createdById = createdById;
        return this;
    }

    public BloodGasTestResultDtoBuilder withLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
        return this;
    }

    public BloodGasTestResultDtoBuilder withCreatedDateTime(long createdDateTime) {
        this.createdDateTime = createdDateTime;
        return this;
    }

    public BloodGasTestResultDtoBuilder withLastModifiedDateTime(long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
        return this;
    }

    public BloodGasTestResultDto build() {
        BloodGasTestResultDto bloodGasTestResultDto = new BloodGasTestResultDto();
        bloodGasTestResultDto.setId(id);
        bloodGasTestResultDto.setStayId(stayId);
        bloodGasTestResultDto.setTestRequestId(testRequestId);
        bloodGasTestResultDto.setResultDate(resultDate);
        bloodGasTestResultDto.setUnit(unit);
        bloodGasTestResultDto.setValue(value);
        bloodGasTestResultDto.setNorm(norm);
        bloodGasTestResultDto.setAbnormality(abnormality);
        bloodGasTestResultDto.setHl7Id(hl7Id);
        bloodGasTestResultDto.setExecutorHl7Id(executorHl7Id);
        bloodGasTestResultDto.setExecutorName(executorName);
        bloodGasTestResultDto.setExecutorSurname(executorSurname);
        bloodGasTestResultDto.setCreatedById(createdById);
        bloodGasTestResultDto.setLastModifiedById(lastModifiedById);
        bloodGasTestResultDto.setCreatedDateTime(createdDateTime);
        bloodGasTestResultDto.setLastModifiedDateTime(lastModifiedDateTime);
        return bloodGasTestResultDto;
    }
}
