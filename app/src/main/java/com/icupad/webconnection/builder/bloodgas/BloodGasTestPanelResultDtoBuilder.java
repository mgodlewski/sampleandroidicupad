package com.icupad.webconnection.builder.bloodgas;

import com.icupad.webconnection.dto.bloodgas.BloodGasTestPanelResultDto;

public final class BloodGasTestPanelResultDtoBuilder {
    private long id;
    private String bloodSource;
    private String executorHl7Id;
    private String executorName;
    private String executorSurname;
    private long requestDate;
    private Long createdById;
    private Long lastModifiedById;
    private long createdDateTime;
    private long lastModifiedDateTime;

    private BloodGasTestPanelResultDtoBuilder() {
    }

    public static BloodGasTestPanelResultDtoBuilder aBloodGasTestPanelResultDto() {
        return new BloodGasTestPanelResultDtoBuilder();
    }

    public BloodGasTestPanelResultDtoBuilder withId(long id) {
        this.id = id;
        return this;
    }

    public BloodGasTestPanelResultDtoBuilder withBloodSource(String bloodSource) {
        this.bloodSource = bloodSource;
        return this;
    }

    public BloodGasTestPanelResultDtoBuilder withExecutorHl7Id(String executorHl7Id) {
        this.executorHl7Id = executorHl7Id;
        return this;
    }

    public BloodGasTestPanelResultDtoBuilder withExecutorName(String executorName) {
        this.executorName = executorName;
        return this;
    }

    public BloodGasTestPanelResultDtoBuilder withExecutorSurname(String executorSurname) {
        this.executorSurname = executorSurname;
        return this;
    }

    public BloodGasTestPanelResultDtoBuilder withRequestDate(long requestDate) {
        this.requestDate = requestDate;
        return this;
    }

    public BloodGasTestPanelResultDtoBuilder withCreatedById(Long createdById) {
        this.createdById = createdById;
        return this;
    }

    public BloodGasTestPanelResultDtoBuilder withLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
        return this;
    }

    public BloodGasTestPanelResultDtoBuilder withCreatedDateTime(long createdDateTime) {
        this.createdDateTime = createdDateTime;
        return this;
    }

    public BloodGasTestPanelResultDtoBuilder withLastModifiedDateTime(long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
        return this;
    }

    public BloodGasTestPanelResultDto build() {
        BloodGasTestPanelResultDto bloodGasTestPanelResultDto = new BloodGasTestPanelResultDto();
        bloodGasTestPanelResultDto.setId(id);
        bloodGasTestPanelResultDto.setBloodSource(bloodSource);
        bloodGasTestPanelResultDto.setExecutorHl7Id(executorHl7Id);
        bloodGasTestPanelResultDto.setExecutorName(executorName);
        bloodGasTestPanelResultDto.setExecutorSurname(executorSurname);
        bloodGasTestPanelResultDto.setRequestDate(requestDate);
        bloodGasTestPanelResultDto.setCreatedById(createdById);
        bloodGasTestPanelResultDto.setLastModifiedById(lastModifiedById);
        bloodGasTestPanelResultDto.setCreatedDateTime(createdDateTime);
        bloodGasTestPanelResultDto.setLastModifiedDateTime(lastModifiedDateTime);
        return bloodGasTestPanelResultDto;
    }
}
