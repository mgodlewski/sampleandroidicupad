package com.icupad.webconnection.builder.ecmo;

import com.icupad.webconnection.dto.ecmo.EcmoConfigurationDto;
import com.icupad.webconnection.dto.ecmo.VeinCannulaDto;

import java.util.List;

/**
 * Created by Marcin on 18.03.2017.
 */
public final class EcmoConfigurationDtoBuilder {
    private Long id;
    private Long procedureId;
    private Long createdBy;
    private String mode;
    private String caniulationType;
    private String wentowanieLeftVentricle;
    private String arteryCannulaType;
    private Double arteryCannulaSize;
    private String oxygeneratorType;
    private Boolean actual;
    private Long date;
    private List<String> cannulationStructures;
    private List<VeinCannulaDto> veinCannulaList;

    public EcmoConfigurationDtoBuilder() {
    }

    public static EcmoConfigurationDtoBuilder aEcmoConfigurationDtoBuilder() {
        return new EcmoConfigurationDtoBuilder();
    }

    public EcmoConfigurationDtoBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public EcmoConfigurationDtoBuilder withProcedureId(Long procedureId) {
        this.procedureId = procedureId;
        return this;
    }

    public EcmoConfigurationDtoBuilder withCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public EcmoConfigurationDtoBuilder withMode(String mode) {
        this.mode = mode;
        return this;
    }

    public EcmoConfigurationDtoBuilder withCaniulationType(String caniulationType) {
        this.caniulationType = caniulationType;
        return this;
    }

    public EcmoConfigurationDtoBuilder withWentowanieLeftVentricle(String wentowanieLeftVentricle) {
        this.wentowanieLeftVentricle = wentowanieLeftVentricle;
        return this;
    }

    public EcmoConfigurationDtoBuilder withArteryCannulaType(String arteryCannulaType) {
        this.arteryCannulaType = arteryCannulaType;
        return this;
    }

    public EcmoConfigurationDtoBuilder withArteryCannulaSize(Double arteryCannulaSize) {
        this.arteryCannulaSize = arteryCannulaSize;
        return this;
    }

    public EcmoConfigurationDtoBuilder withOxygeneratorType(String oxygeneratorType) {
        this.oxygeneratorType = oxygeneratorType;
        return this;
    }

    public EcmoConfigurationDtoBuilder withActual(Boolean actual) {
        this.actual = actual;
        return this;
    }

    public EcmoConfigurationDtoBuilder withDate(Long date) {
        this.date = date;
        return this;
    }

    public EcmoConfigurationDtoBuilder withCannulationStructures(List<String> cannulationStructures) {
        this.cannulationStructures = cannulationStructures;
        return this;
    }

    public EcmoConfigurationDtoBuilder withVeinCannulaList(List<VeinCannulaDto> veinCannulaList) {
        this.veinCannulaList = veinCannulaList;
        return this;
    }

    public EcmoConfigurationDto build(){
        EcmoConfigurationDto ecmoConfigurationDto = new EcmoConfigurationDto();
        ecmoConfigurationDto.setId(id);
        ecmoConfigurationDto.setProcedureId(procedureId);
        ecmoConfigurationDto.setDate(date);
        ecmoConfigurationDto.setCreatedBy(createdBy);
        ecmoConfigurationDto.setActual(actual);
        ecmoConfigurationDto.setArteryCannulaSize(arteryCannulaSize);
        ecmoConfigurationDto.setArteryCannulaType(arteryCannulaType);
        ecmoConfigurationDto.setCaniulationType(caniulationType);
        ecmoConfigurationDto.setCannulationStructures(cannulationStructures);
        ecmoConfigurationDto.setMode(mode);
        ecmoConfigurationDto.setOxygeneratorType(oxygeneratorType);
        ecmoConfigurationDto.setVeinCannulaList(veinCannulaList);
        ecmoConfigurationDto.setWentowanieLeftVentricle(wentowanieLeftVentricle);
        return ecmoConfigurationDto;
    }
}
