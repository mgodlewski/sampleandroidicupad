package com.icupad.webconnection.service;

import android.content.Context;

import com.icupad.webconnection.model.ConnectionStatus;

public class ConnectionStatusUpdater {

    private ConnectionStatus connectionStatus;
    private Context context;
    private RestConnection restConnection;

    public ConnectionStatusUpdater(Context context) {
        this.context = context;
        connectionStatus = ConnectionStatus.NO_CONNECTION;
        restConnection = new RestConnection(context);
    }

    public ConnectionStatus updateAndReturnConnectionStatus() {
        if(ConnectionManager.checkInternetConnection(context).isInternet()) {
            connectionStatus = getStatusWhenConnectedToInternet();
        }
        else {
            connectionStatus = ConnectionStatus.NO_CONNECTION;
        }
        return connectionStatus;
    }

    private ConnectionStatus getStatusWhenConnectedToInternet() {
        if (restConnection.checkConnectionToServer()) {
            if(restConnection.checkAuthenticatedConnectionToServer()) {
                return ConnectionStatus.AUTHENTICATED_CONNECTION_TO_SERVER;
            }
            else {
                return ConnectionStatus.CONNECTION_TO_SERVER;
            }
        }
        else {
            return ConnectionStatus.CONNECTION_TO_INTERNET;
        }
    }
}
