package com.icupad.webconnection.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.icupad.webconnection.model.ConnectionType;

public class ConnectionManager {
    public static ConnectionType checkInternetConnection(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return new ConnectionType(checkWifi(connMgr), checkMobile(connMgr));
    }

    private static boolean checkWifi(ConnectivityManager connMgr) {
        NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (networkInfo != null) {
            return networkInfo.isConnected();
        }
        return false;
    }

    private static boolean checkMobile(ConnectivityManager connMgr) {
        NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (networkInfo != null) {
            return networkInfo.isConnected();
        }
        return false;
    }

}
