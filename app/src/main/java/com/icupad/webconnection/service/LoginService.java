package com.icupad.webconnection.service;

import android.app.IntentService;
import android.content.Intent;

import com.icupad.utils.LoggedUserInfo;
import com.icupad.webconnection.model.LoginStatus;
import com.icupad.webconnection.api.BackendApi;

import java.io.IOException;

public class LoginService extends IntentService {

    public static final String MESSAGE = "message";
    public static final String LOGIN_FIELD = "login";
    public static final String PASSWORD_FIELD = "password";
    public static final String NOTIFICATION = "com.icupad.webconnection.service.loginservice" ;

    public LoginService() {
        super(LoginService.class.toString());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        BackendApi backendApi = new BackendApi(getApplicationContext());
        String login = intent.getStringExtra(LOGIN_FIELD);
        String password = intent.getStringExtra(PASSWORD_FIELD);
        LoggedUserInfo.setLogin(login);
        LoggedUserInfo.setPassword(password);

        LoginStatus loginStatus = LoginStatus.INVALID;
        try {
            if(backendApi.pingServer()) {
                loginStatus = LoginStatus.VALID;
                LoggedUserInfo.updateAdditionalInfo(backendApi.getLoggedUserInfo());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Intent answerIntent = new Intent(NOTIFICATION);
        answerIntent.putExtra(MESSAGE, loginStatus.toString());
        getApplicationContext().sendBroadcast(answerIntent);
    }
}
