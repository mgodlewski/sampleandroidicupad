package com.icupad.webconnection.service;

import android.content.Context;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.icupad.utils.LoggedUserInfo;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.webconnection.dto.general.ErrorDto;
import com.icupad.webconnection.model.MyHttpClient;

import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.logging.Logger;

public class RestConnection {
    private Logger logger;

    private final String url;

    private Context context;
    private Gson gson;

    public RestConnection (Context context) {
        this.url = generateUrl();
        this.context = context;
        this.logger = Logger.getLogger(this.getClass().toString());

        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
    }

    private String generateUrl() {
        String defaultProtocol = PrivateDataAccessor.getDefaultProtocol();
        String defaultAddress = PrivateDataAccessor.getDefaultAddress();
        String defaultPort = PrivateDataAccessor.getDefaultPort();
        return defaultProtocol + "://" + defaultAddress + ":" + defaultPort;
    }

    public String getJson(String path) throws IOException {
        String response = EntityUtils.toString(get(path).getEntity());
        return checkResponseForError(response);
    }

    @Nullable
    private String checkResponseForError(String response) {
        //gson is not strict with it's documentation, it's not always throw exception when cannot parse
        try {
            ErrorDto errorDto = gson.fromJson(response, ErrorDto.class);
            if(errorDto.getStatus() == 0) {
                throw new Exception();
            }
            logger.info(errorDto.toString());
        } catch(Exception e) {
            return response;
        }
        return null;
    }

    public HttpResponse get(String path) throws IOException {
        return get(path, new BasicHttpParams());
    }

    public HttpResponse get(String path, HttpParams httpParams) throws IOException {
        HttpClient httpClient = new MyHttpClient(context, httpParams);
        System.out.println(url+path);
        HttpGet httpGet = new HttpGet(url + path);
        authenticate(httpGet);
        return httpClient.execute(httpGet);
    }

    public HttpResponse noAuthGet(String path, HttpParams httpParams) throws IOException {
        HttpClient httpClient = new MyHttpClient(context, httpParams);
        HttpGet httpGet = new HttpGet(url + path);
        return httpClient.execute(httpGet);
    }

    public String postAndGetJson(String path, String json)throws IOException {
        String response = EntityUtils.toString(post(path, json).getEntity());
        return checkResponseForError(response);
    }

    public HttpResponse post(String path, String json) throws IOException {
        HttpClient httpClient = new MyHttpClient(context, new BasicHttpParams());
        HttpPost httpPost = new HttpPost(url + path);

        StringEntity stringEntity = new StringEntity(new String(json.getBytes(), "utf-8"), "utf-8");
        stringEntity.setContentEncoding(HTTP.UTF_8);
        stringEntity.setContentType("application/json");
        httpPost.setEntity(stringEntity);
        httpPost.setHeader("Accept", "application/json");

        authenticate(httpPost);
        HttpResponse response = httpClient.execute(httpPost);
        return response;
    }

    public String postFileAndGetJson(String path, File file) throws IOException {
        String response = EntityUtils.toString(postFile(path, file).getEntity());
        return checkResponseForError(response);
    }

    private HttpResponse postFile(String path, File file) throws IOException {
        HttpClient httpClient = new MyHttpClient(context, new BasicHttpParams());
        HttpPost httpPost = new HttpPost(url + path);

        MultipartEntity reqEntity = new MultipartEntity();
        reqEntity.addPart("file", new FileBody(file));

        httpPost.setEntity(reqEntity);
        authenticate(httpPost);
        HttpResponse response = httpClient.execute(httpPost);
        return response;
    }

    public boolean checkConnectionToServer() {
        try {
            return noAuthPing();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean checkAuthenticatedConnectionToServer() {
        try {
            return authPing();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void authenticate(HttpGet httpGet) {
        httpGet.addHeader(BasicScheme.authenticate(
                new UsernamePasswordCredentials(LoggedUserInfo.getLogin(), LoggedUserInfo.getPassword()),
                "UTF-8", false));
    }

    private void authenticate(HttpPost httpPost) {
        httpPost.addHeader(BasicScheme.authenticate(
                new UsernamePasswordCredentials(LoggedUserInfo.getLogin(), LoggedUserInfo.getPassword()),
                "UTF-8", false));
    }

    public Boolean authPing() throws IOException {
        String path = "/ping";
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, 1000);

        InputStream is = get(path, httpParams).getEntity().getContent();

        String inputStreamString = new Scanner(is,"UTF-8").useDelimiter("\\A").next();
        return inputStreamString.equals("pong");
    }

    public Boolean noAuthPing() throws IOException {
        String path = "/noAuthPing";
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, 1000);

        InputStream is = noAuthGet(path, httpParams).getEntity().getContent();

        String inputStreamString = new Scanner(is,"UTF-8").useDelimiter("\\A").next();
        return inputStreamString.equals("noAuthPong");
    }
}
