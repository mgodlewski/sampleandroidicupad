package com.icupad.utils;

import android.util.Base64;

import org.apache.commons.codec.DecoderException;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

//source:
//https://developer.android.com/reference/android/util/Base64.html#decode(java.lang.String, int)
//http://stackoverflow.com/questions/34342136/alternatives-a-datatypeconverter-in-androidsolved
//http://commons.apache.org/proper/commons-codec/
//http://stackoverflow.com/questions/14109970/alternative-of-datatypeconverter-printhexbinarybyte-array-and-datatypeconver
//http://stackoverflow.com/questions/4551263/how-can-i-convert-a-string-to-a-secretkey/8828196#8828196
//http://stackoverflow.com/questions/34342136/alternatives-a-datatypeconverter-in-androidsolved
//http://stackoverflow.com/users/589259/maarten-bodewes
public class AESHelper {

    private static final String STANDARD_HEX_KEY_SUFIX = "447570614368756a43796375737a6b69";

    public static String encrypt(final String plainMessage,
                                 final String symKey) throws DecoderException, UnsupportedEncodingException {
        final String symKeyHex = getHexKeyWithCorrectLength(symKey);
        final byte[] symKeyData = org.apache.commons.codec.binary.Hex.decodeHex(symKeyHex.toCharArray());
        final byte[] encodedMessage = plainMessage.getBytes(Charset.forName("UTF-8"));
        try {
            final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            final int blockSize = cipher.getBlockSize();

            final SecretKeySpec secretKeySpec = new SecretKeySpec(symKeyData, "AES");

            final byte[] ivData = new byte[blockSize];
            final SecureRandom rnd = SecureRandom.getInstance("SHA1PRNG");
            rnd.nextBytes(ivData);
            final IvParameterSpec iv = new IvParameterSpec(ivData);

            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, iv);

            final byte[] encryptedMessage = cipher.doFinal(encodedMessage);

            final byte[] ivAndEncryptedMessage = new byte[ivData.length
                    + encryptedMessage.length];
            System.arraycopy(ivData, 0, ivAndEncryptedMessage, 0, blockSize);
            System.arraycopy(encryptedMessage, 0, ivAndEncryptedMessage,
                    blockSize, encryptedMessage.length);

            final String ivAndEncryptedMessageBase64 = android.util.Base64.encodeToString(ivAndEncryptedMessage,16);

            return ivAndEncryptedMessageBase64;
        } catch (InvalidKeyException e) {
            throw new IllegalArgumentException(
                    "key argument does not contain a valid AES key");
        } catch (GeneralSecurityException e) {
            throw new IllegalStateException(
                    "Unexpected exception during encryption", e);
        }
    }

    public static String decrypt(final String ivAndEncryptedMessageBase64,
                                 final String symKey) throws DecoderException, UnsupportedEncodingException {
        final String symKeyHex = getHexKeyWithCorrectLength(symKey);
        final byte[] symKeyData = org.apache.commons.codec.binary.Hex.decodeHex(symKeyHex.toCharArray());
        final byte[] ivAndEncryptedMessage = Base64.decode(ivAndEncryptedMessageBase64, Base64.DEFAULT);

        try {
            final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            final int blockSize = cipher.getBlockSize();

            final SecretKeySpec secretKeySpec = new SecretKeySpec(symKeyData, "AES");

            final byte[] ivData = new byte[blockSize];
            System.arraycopy(ivAndEncryptedMessage, 0, ivData, 0, blockSize);
            final IvParameterSpec iv = new IvParameterSpec(ivData);

            final byte[] encryptedMessage = new byte[ivAndEncryptedMessage.length
                    - blockSize];
            System.arraycopy(ivAndEncryptedMessage, blockSize,
                    encryptedMessage, 0, encryptedMessage.length);

            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, iv);

            final byte[] encodedMessage = cipher.doFinal(encryptedMessage);

            final String message = new String(encodedMessage,
                    Charset.forName("UTF-8"));

            return message;
        } catch (InvalidKeyException e) {
            throw new IllegalArgumentException(
                    "key argument does not contain a valid AES key");
        } catch (BadPaddingException e) {
            // you'd better know about padding oracle attacks
            return null;
        } catch (GeneralSecurityException e) {
            throw new IllegalStateException(
                    "Unexpected exception during decryption", e);
        }
    }

    private static String getHexKeyWithCorrectLength(String symKey) throws UnsupportedEncodingException {
        String rawHexKey = toHex(symKey);
        if(rawHexKey.length() >= 256/4) {
            return rawHexKey.substring(0, 256/4);
        }
        if(rawHexKey.length() >= 192/4) {
            return rawHexKey.substring(0, 192/4);
        }
        if(rawHexKey.length() >= 128/4) {
            return rawHexKey.substring(0, 128/4);
        }
        return rawHexKey.concat(STANDARD_HEX_KEY_SUFIX).substring(0, 128/4);
    }

    private static String toHex(String arg) throws UnsupportedEncodingException {
        return String.format("%x", new BigInteger(1, arg.getBytes("UTF-8")));
    }
}
