package com.icupad.utils.model;

/**
 * Created by Marcin on 03.06.2017.
 */
public enum Mode {
    TABLE, CHART
}
