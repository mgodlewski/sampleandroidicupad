package com.icupad.utils.model;

import java.util.List;

public class InterruptedDownloadStateModel {
    private boolean savedInterruptedDownload;
    private String downloadPhase;
    private String dataLabel;
    private List<Long> patientIdsToAdd;
    private List<Long> patientIdsToRemove;
    private List<Long> patientIdsToUpdate;
    private List<Long> presentPatientsBeforeSynchronize;

    public boolean isSavedInterruptedDownload() {
        return savedInterruptedDownload;
    }

    public void setSavedInterruptedDownload(boolean savedInterruptedDownload) {
        this.savedInterruptedDownload = savedInterruptedDownload;
    }

    public String getDownloadPhase() {
        return downloadPhase;
    }

    public void setDownloadPhase(String downloadPhase) {
        this.downloadPhase = downloadPhase;
    }

    public String getDataLabel() {
        return dataLabel;
    }

    public void setDataLabel(String dataLabel) {
        this.dataLabel = dataLabel;
    }

    public List<Long> getPatientIdsToAdd() {
        return patientIdsToAdd;
    }

    public void setPatientIdsToAdd(List<Long> patientIdsToAdd) {
        this.patientIdsToAdd = patientIdsToAdd;
    }

    public List<Long> getPatientIdsToRemove() {
        return patientIdsToRemove;
    }

    public void setPatientIdsToRemove(List<Long> patientIdsToRemove) {
        this.patientIdsToRemove = patientIdsToRemove;
    }

    public List<Long> getPatientIdsToUpdate() {
        return patientIdsToUpdate;
    }

    public void setPatientIdsToUpdate(List<Long> patientIdsToUpdate) {
        this.patientIdsToUpdate = patientIdsToUpdate;
    }

    public List<Long> getPresentPatientsBeforeSynchronize() {
        return presentPatientsBeforeSynchronize;
    }

    public void setPresentPatientsBeforeSynchronize(List<Long> presentPatientsBeforeSynchronize) {
        this.presentPatientsBeforeSynchronize = presentPatientsBeforeSynchronize;
    }
}
