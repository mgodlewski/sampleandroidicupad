package com.icupad.utils.model.builder;

import com.icupad.utils.model.InterruptedDownloadStateModel;

import java.util.List;

public final class InterruptedDownloadStateModelBuilder {
    private boolean savedInterruptedDownload;
    private String downloadPhase;
    private String dataLabel;
    private List<Long> patientIdsToAdd;
    private List<Long> patientIdsToRemove;
    private List<Long> patientIdsToUpdate;
    private List<Long> presentPatientsBeforeSynchronize;

    private InterruptedDownloadStateModelBuilder() {
    }

    public static InterruptedDownloadStateModelBuilder anInterruptedDownloadStateModel() {
        return new InterruptedDownloadStateModelBuilder();
    }

    public InterruptedDownloadStateModelBuilder withSavedInterruptedDownload(boolean savedInterruptedDownload) {
        this.savedInterruptedDownload = savedInterruptedDownload;
        return this;
    }

    public InterruptedDownloadStateModelBuilder withDownloadPhase(String downloadPhase) {
        this.downloadPhase = downloadPhase;
        return this;
    }

    public InterruptedDownloadStateModelBuilder withDataLabel(String dataLabel) {
        this.dataLabel = dataLabel;
        return this;
    }

    public InterruptedDownloadStateModelBuilder withPatientIdsToAdd(List<Long> patientIdsToAdd) {
        this.patientIdsToAdd = patientIdsToAdd;
        return this;
    }

    public InterruptedDownloadStateModelBuilder withPatientIdsToRemove(List<Long> patientIdsToRemove) {
        this.patientIdsToRemove = patientIdsToRemove;
        return this;
    }

    public InterruptedDownloadStateModelBuilder withPatientIdsToUpdate(List<Long> patientIdsToUpdate) {
        this.patientIdsToUpdate = patientIdsToUpdate;
        return this;
    }

    public InterruptedDownloadStateModelBuilder withPresentPatientsBeforeSynchronize(List<Long> presentPatientsBeforeSynchronize) {
        this.presentPatientsBeforeSynchronize = presentPatientsBeforeSynchronize;
        return this;
    }

    public InterruptedDownloadStateModel build() {
        InterruptedDownloadStateModel interruptedDownloadStateModel = new InterruptedDownloadStateModel();
        interruptedDownloadStateModel.setSavedInterruptedDownload(savedInterruptedDownload);
        interruptedDownloadStateModel.setDownloadPhase(downloadPhase);
        interruptedDownloadStateModel.setDataLabel(dataLabel);
        interruptedDownloadStateModel.setPatientIdsToAdd(patientIdsToAdd);
        interruptedDownloadStateModel.setPatientIdsToRemove(patientIdsToRemove);
        interruptedDownloadStateModel.setPatientIdsToUpdate(patientIdsToUpdate);
        interruptedDownloadStateModel.setPresentPatientsBeforeSynchronize(presentPatientsBeforeSynchronize);
        return interruptedDownloadStateModel;
    }
}
