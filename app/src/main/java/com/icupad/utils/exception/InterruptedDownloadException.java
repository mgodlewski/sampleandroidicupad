package com.icupad.utils.exception;

import com.icupad.utils.model.InterruptedDownloadStateModel;

public class InterruptedDownloadException extends Exception {
    private InterruptedDownloadStateModel interruptedDownloadStateModel;
    public InterruptedDownloadException() {
        super();
    }

    public InterruptedDownloadException(String detailMessage) {
        super(detailMessage);
    }

    public InterruptedDownloadException(InterruptedDownloadStateModel interruptedDownloadStateModel) {
        super();
        this.interruptedDownloadStateModel = interruptedDownloadStateModel;
    }

    public InterruptedDownloadStateModel getInterruptedDownloadStateModel() {
        return interruptedDownloadStateModel;
    }
}
