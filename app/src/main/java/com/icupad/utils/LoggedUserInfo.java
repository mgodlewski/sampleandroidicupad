package com.icupad.utils;

import com.icupad.webconnection.dto.general.UserDto;

import java.util.ArrayList;
import java.util.List;

public class LoggedUserInfo {
    private static final String ADMIN_ROLE = "Admin";
    private static final String DEV_ROLE = "Dev";
    private static boolean loggedIn = false;
    public static String login = "";
    public static String password = "";
    public static List<String> roles = new ArrayList<>();
    public static String name = "";
    public static String surname = "";
    public static String hl7Id = "";

    public static void updateAdditionalInfo(UserDto userDto) {
        roles = userDto.getRoles();
        name = userDto.getName();
        surname = userDto.getSurname();
        hl7Id = userDto.getHl7Id();
        loggedIn = true;
    }

    public static void logOut() {
        login = "";
        password = "";
        roles = new ArrayList<>();
        name = "";
        surname = "";
        hl7Id = "";
        loggedIn = false;
    }

    public static boolean hasAdminRole() {
        for(String role : roles) {
            if(role.equals(ADMIN_ROLE)) {
                return true;
            }
        }
        return false;
    }

    public static boolean hasDevRole() {
        for(String role : roles) {
            if(role.equals(DEV_ROLE)) {
                return true;
            }
        }
        return false;
    }

    public static String getLogin() {
        return login;
    }

    public static void setLogin(String login) {
        LoggedUserInfo.login = login;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        LoggedUserInfo.password = password;
    }

    public static List<String> getRoles() {
        return roles;
    }

    public static void setRoles(List<String> roles) {
        LoggedUserInfo.roles = roles;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        LoggedUserInfo.name = name;
    }

    public static String getSurname() {
        return surname;
    }

    public static void setSurname(String surname) {
        LoggedUserInfo.surname = surname;
    }

    public static String getHl7Id() {
        return hl7Id;
    }

    public static void setHl7Id(String hl7Id) {
        LoggedUserInfo.hl7Id = hl7Id;
    }

    public static boolean isLoggedIn() {
        return loggedIn;
    }
}
