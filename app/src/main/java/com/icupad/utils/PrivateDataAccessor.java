package com.icupad.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.icupad.R;
import com.icupad.synchronization.utils.ThresholdTime;
import com.icupad.utils.model.InterruptedDownloadStateModel;
import com.icupad.utils.model.builder.InterruptedDownloadStateModelBuilder;
import com.icupad.webconnection.api.BackendApi;

import org.apache.commons.codec.DecoderException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class PrivateDataAccessor {
    private static final String USER_SHARED_PREFERENCES_FILE_SUFIX = ":user_shared_preferences";
    private static final String COMMON_SHARED_PREFERENCES_FILE = "common_shared_preferences";

    private static final String VERSION = "VERSION";
    private static final int WRONG_VERSION = -1;
    private static final String ENCRYPTED_SQLITE_PASSWORD = "ENCRYPTED_SQLITE_PASSWORD";
    private static final String ENCRYPTED_LOGIN = "ENCRYPTED_LOGIN";
    private static final String ICUPAD_LAST_SYNCED_TIMESTAMP = "ICUPAD_LAST_SYNCED_TIMESTAMP";
    private static final String SAVED_INTERRUPTED_DOWNLOAD = "SAVED_INTERRUPTED_DOWNLOAD";
    private static final String INTERRUPTED_DOWNLOAD_DATA_LABEL = "INTERRUPTED_DOWNLOAD_DATA_LABEL";
    private static final String INTERRUPTED_DOWNLOAD_PATIENT_IDS_TO_ADD = "INTERRUPTED_DOWNLOAD_PATIENT_IDS_TO_ADD";
    private static final String INTERRUPTED_DOWNLOAD_PATIENT_IDS_TO_REMOVE = "INTERRUPTED_DOWNLOAD_PATIENT_IDS_TO_REMOVE";
    private static final String INTERRUPTED_DOWNLOAD_PATIENT_IDS_TO_UPDATE = "INTERRUPTED_DOWNLOAD_PATIENT_IDS_TO_UPDATE";
    private static final String INTERRUPTED_DOWNLOAD_PHASE = "INTERRUPTED_DOWNLOAD_PHASE";
    private static final String INTERRUPTED_DOWNLOAD_PRESENT_PATIENT_IDS_BEFORE_SYNCHRONIZE = "INTERRUPTED_DOWNLOAD_PRESENT_PATIENT_IDS_BEFORE_SYNCHRONIZE";
    protected static String CONNECTION_OPTIONS_PROTOCOL = "CONNECTION_OPTIONS_PROTOCOL";
    protected static String CONNECTION_OPTIONS_ADDRESS = "CONNECTION_OPTIONS_ADDRESS";
    protected static String CONNECTION_OPTIONS_PORT = "CONNECTION_OPTIONS_PORT";
    private static SharedPreferences userSharedPref;
    private static SharedPreferences commonSharedPref;
    private static boolean ready = true;

    private static String sqlitePassword;
    private static Long icupadLastSyncedTimestamp;
    private static String defaultProtocol;
    private static String defaultAddress;
    private static String defaultPort;

    private static Context context;

    public static void initCommonSharedPreferences (Context context) {
        PrivateDataAccessor.context = context;
        commonSharedPref = context.getSharedPreferences(
                COMMON_SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        updateCommonSharedPrefToRightVersion(context);
        icupadLastSyncedTimestamp = commonSharedPref.getLong(ICUPAD_LAST_SYNCED_TIMESTAMP, 0);
        defaultProtocol = commonSharedPref.getString(CONNECTION_OPTIONS_PROTOCOL, "");
        defaultAddress = commonSharedPref.getString(CONNECTION_OPTIONS_ADDRESS, "");
        defaultPort = commonSharedPref.getString(CONNECTION_OPTIONS_PORT, "");
    }

    public static void saveConnectionDefaultOptions() {
        SharedPreferences.Editor editor = commonSharedPref.edit();
        editor.putString(CONNECTION_OPTIONS_PROTOCOL, context.getResources().getString(R.string.default_protocol));
        editor.putString(CONNECTION_OPTIONS_ADDRESS, context.getResources().getString(R.string.default_address));
        editor.putString(CONNECTION_OPTIONS_PORT, context.getResources().getString(R.string.default_port));
        editor.apply();
    }

    public static boolean loginOffline(Context context, String login, String password) {
        SharedPreferences tempUserSharedPref = context.getSharedPreferences(
                login + USER_SHARED_PREFERENCES_FILE_SUFIX, Context.MODE_PRIVATE);
        int version = tempUserSharedPref.getInt(VERSION, WRONG_VERSION);
        String encryptedLogin = tempUserSharedPref.getString(ENCRYPTED_LOGIN, null);
        try {
            if(version != WRONG_VERSION && encryptedLogin != null) {
                String decryptedLogin = AESHelper.decrypt(encryptedLogin, password);
                if (decryptedLogin != null && decryptedLogin.equals(login)) {
                    initUserSharedPreferencesAndSqlitePassword(context, login, password);
                    return true;
                }
            }
        } catch (DecoderException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void initUserSharedPreferencesAndSqlitePassword(Context context, String login, String password) throws DecoderException, UnsupportedEncodingException {
        userSharedPref = context.getSharedPreferences(
                login + USER_SHARED_PREFERENCES_FILE_SUFIX, Context.MODE_PRIVATE);
        String encryptedSqlitePassword = userSharedPref.getString(ENCRYPTED_SQLITE_PASSWORD, null);
        sqlitePassword = AESHelper.decrypt(encryptedSqlitePassword, password);
    }

    public static void updateAndInitUserSharedPreferences(Context context, String login, String password) throws DecoderException, UnsupportedEncodingException {
        PrivateDataAccessor.ready = false;
        new UserSharedPrefUpdater(context, login, password).execute();
        while(!ready);
        String encryptedSqlitePassword = userSharedPref.getString(ENCRYPTED_SQLITE_PASSWORD, null);
        sqlitePassword = AESHelper.decrypt(encryptedSqlitePassword, password);

    }

    private static void updateCommonSharedPrefToRightVersion(Context context) {
        int version = commonSharedPref.getInt(VERSION, WRONG_VERSION);
        if(version == WRONG_VERSION) {
            SharedPreferences.Editor editor = commonSharedPref.edit();
            editor.putInt(VERSION, context.getResources().getInteger(R.integer.common_shared_preferences_version));
            editor.putLong(ICUPAD_LAST_SYNCED_TIMESTAMP, 0);
            editor.putString(CONNECTION_OPTIONS_PROTOCOL, context.getResources().getString(R.string.default_protocol));
            editor.putString(CONNECTION_OPTIONS_ADDRESS, context.getResources().getString(R.string.default_address));
            editor.putString(CONNECTION_OPTIONS_PORT, context.getResources().getString(R.string.default_port));
            editor.apply();
        }
    }

    public static String getSqlitePassword() {
        return sqlitePassword;
    }

    public static ThresholdTime getLastSyncWithBackend() {
        return new ThresholdTime(PrivateDataAccessor.icupadLastSyncedTimestamp);
    }

    public static void setLastSyncTimestamps(ThresholdTime thresholdTime) {
        PrivateDataAccessor.icupadLastSyncedTimestamp = thresholdTime.getIcupadLastSyncedTimestamp();
        SharedPreferences.Editor editor = commonSharedPref.edit();
        editor.putLong(ICUPAD_LAST_SYNCED_TIMESTAMP, thresholdTime.getIcupadLastSyncedTimestamp());
        editor.apply();
    }

    public static void cleanInterruptedDownloadState() {
        InterruptedDownloadStateModel stateModel = InterruptedDownloadStateModelBuilder.anInterruptedDownloadStateModel()
                .withSavedInterruptedDownload(false)
                .withDownloadPhase("")
                .withDataLabel("")
                .withPatientIdsToAdd(new ArrayList<Long>())
                .withPatientIdsToUpdate(new ArrayList<Long>())
                .withPatientIdsToRemove(new ArrayList<Long>())
                .withPresentPatientsBeforeSynchronize(new ArrayList<Long>())
                .build();
        saveInterruptedDownloadState(stateModel);
    }

    public static void saveInterruptedDownloadState(InterruptedDownloadStateModel stateModel) {
        SharedPreferences.Editor editor = commonSharedPref.edit();
        editor.putBoolean(SAVED_INTERRUPTED_DOWNLOAD, stateModel.isSavedInterruptedDownload());
        editor.putString(INTERRUPTED_DOWNLOAD_PHASE, stateModel.getDownloadPhase());
        editor.putString(INTERRUPTED_DOWNLOAD_DATA_LABEL, stateModel.getDataLabel());
        if(stateModel.getPatientIdsToAdd() != null) {
            editor.putString(INTERRUPTED_DOWNLOAD_PATIENT_IDS_TO_ADD, TextUtils.join(",", stateModel.getPatientIdsToAdd()));
        }
        if(stateModel.getPatientIdsToRemove() != null) {
            editor.putString(INTERRUPTED_DOWNLOAD_PATIENT_IDS_TO_REMOVE, TextUtils.join(",", stateModel.getPatientIdsToRemove()));
        }
        if(stateModel.getPatientIdsToUpdate() != null) {
            editor.putString(INTERRUPTED_DOWNLOAD_PATIENT_IDS_TO_UPDATE, TextUtils.join(",", stateModel.getPatientIdsToUpdate()));
        }
        if(stateModel.getPresentPatientsBeforeSynchronize() != null) {
            editor.putString(INTERRUPTED_DOWNLOAD_PRESENT_PATIENT_IDS_BEFORE_SYNCHRONIZE, TextUtils.join(",", stateModel.getPresentPatientsBeforeSynchronize()));
        }
        editor.apply();
    }

    public static InterruptedDownloadStateModel loadInterruptedDownloadState() {
        InterruptedDownloadStateModel stateModel = InterruptedDownloadStateModelBuilder.anInterruptedDownloadStateModel()
                .withSavedInterruptedDownload(commonSharedPref.getBoolean(SAVED_INTERRUPTED_DOWNLOAD, false))
                .withDownloadPhase(commonSharedPref.getString(INTERRUPTED_DOWNLOAD_PHASE, ""))
                .withDataLabel(commonSharedPref.getString(INTERRUPTED_DOWNLOAD_DATA_LABEL, ""))
                .withPatientIdsToAdd(getByLabelFromCommonSharedPrefAndReturnAsListOfLongs(INTERRUPTED_DOWNLOAD_PATIENT_IDS_TO_ADD))
                .withPatientIdsToRemove(getByLabelFromCommonSharedPrefAndReturnAsListOfLongs(INTERRUPTED_DOWNLOAD_PATIENT_IDS_TO_REMOVE))
                .withPatientIdsToUpdate(getByLabelFromCommonSharedPrefAndReturnAsListOfLongs(INTERRUPTED_DOWNLOAD_PATIENT_IDS_TO_UPDATE))
                .withPresentPatientsBeforeSynchronize(getByLabelFromCommonSharedPrefAndReturnAsListOfLongs(INTERRUPTED_DOWNLOAD_PRESENT_PATIENT_IDS_BEFORE_SYNCHRONIZE))
                .build();
        return stateModel;
    }

    private static List<Long> getByLabelFromCommonSharedPrefAndReturnAsListOfLongs(String label) {
        String valuesStringRawStringList = commonSharedPref.getString(label, "");
        String[] valuesStringArray = valuesStringRawStringList.split(",");
        List<Long> valuesLongList = new ArrayList<>();
        for(int i = 0; i < valuesStringArray.length; i++) {
            if( !valuesStringArray[i].isEmpty()) {
                valuesLongList.add(Long.valueOf(valuesStringArray[i]));
            }
        }
        return valuesLongList;
    }

    public static String getDefaultProtocol() {
        return defaultProtocol;
    }

    public static void setDefaultProtocol(String defaultProtocol) {
        PrivateDataAccessor.defaultProtocol = defaultProtocol;
    }

    public static String getDefaultAddress() {
        return defaultAddress;
    }

    public static void setDefaultAddress(String defaultAddress) {
        PrivateDataAccessor.defaultAddress = defaultAddress;
    }

    public static String getDefaultPort() {
        return defaultPort;
    }

    public static void setDefaultPort(String defaultPort) {
        PrivateDataAccessor.defaultPort = defaultPort;
    }

    private static class UserSharedPrefUpdater extends AsyncTask<Void, Void, Void> {
        private Context context;
        private String login;
        private String password;

        public UserSharedPrefUpdater (Context context, String login, String password) {
            this.context = context;
            this.login = login;
            this.password = password;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            userSharedPref = context.getSharedPreferences(
                login + USER_SHARED_PREFERENCES_FILE_SUFIX, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = userSharedPref.edit();
            editor.putInt(VERSION, context.getResources().getInteger(R.integer.user_shared_preferences_version));
            BackendApi backendApi = new BackendApi(context);
            try {
                String sqlitePassword = backendApi.getSqlitePassword();
                editor.putString(ENCRYPTED_SQLITE_PASSWORD, AESHelper.encrypt(sqlitePassword, password));
                editor.putString(ENCRYPTED_LOGIN, AESHelper.encrypt(login, password));
            } catch (DecoderException | UnsupportedEncodingException e) {
                e.printStackTrace();
            }catch (IOException e) {
                e.printStackTrace();
            }
            editor.apply();
            PrivateDataAccessor.ready = true;
            return null;
        }
    }
}
