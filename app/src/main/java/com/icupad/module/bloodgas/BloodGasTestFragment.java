package com.icupad.module.bloodgas;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.LineDataSet;
import com.google.common.primitives.Ints;
import com.icupad.R;
import com.icupad.commons.repository.model.BloodGasMeasurement;
import com.icupad.commons.repository.model.Stay;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.module.ResultsModuleFragment;
import com.icupad.tableView.CellData;
import com.inqbarna.tablefixheaders.TableFixHeaders;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Marcin on 13.06.2017.
 */
public class BloodGasTestFragment extends ResultsModuleFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_blood_gas_test, container, false);

        prepareView(view, false);

        return view;
    }

    @Override
    protected List<BloodGasMeasurement> getDataFromDB() {
        Stay lastStay = publicIcupadRepository.getLastStay();
        return publicIcupadRepository.getBloodGasTestMeasurement(lastStay.getId());
    }

    @Override
    protected void getAllViewElements(View view) {
        table = (TableFixHeaders) view.findViewById(R.id.table);
        informationTextView = (TextView) view.findViewById(R.id.informationTextView);
        tableChartButton = (Button) view.findViewById(R.id.viewTypeButton);
        shiftToRightButton = (Button) view.findViewById(R.id.shiftToRightButton);
        chart = (LineChart) view.findViewById(R.id.lineChart);
    }

    @Override
    protected void getTableData(List<String> header, List<List<CellData>> rows) {
        Map<Long,CellData> pHCols = new LinkedHashMap();
        Map<Long,CellData> pCO2Cols = new LinkedHashMap();
        Map<Long,CellData> pO2Cols = new LinkedHashMap();
        Map<Long,CellData> kCols = new LinkedHashMap();
        Map<Long,CellData> naCols = new LinkedHashMap();
        Map<Long,CellData> caCols = new LinkedHashMap();
        Map<Long,CellData> clCols = new LinkedHashMap();
        Map<Long,CellData> glukozaCols = new LinkedHashMap();
        Map<Long,CellData> tHbCols = new LinkedHashMap();
        Map<Long,CellData> sO2Cols = new LinkedHashMap();

        Map<Long,CellData> tBilCols = new LinkedHashMap();
        Map<Long,CellData> cbaseCols = new LinkedHashMap();
        Map<Long,CellData> chco3Cols = new LinkedHashMap();
        Map<Long,CellData> hctCols = new LinkedHashMap();
        Map<Long,CellData> fo2hbCols = new LinkedHashMap();
        Map<Long,CellData> fcohbCols = new LinkedHashMap();
        Map<Long,CellData> fmetHbCols = new LinkedHashMap();
        Map<Long,CellData> mleczanyCols = new LinkedHashMap();
        Map<Long,CellData> sbcCols = new LinkedHashMap();
        Map<Long,CellData> hco3aCols = new LinkedHashMap();

        Map<Long,CellData> tCO2Cols = new LinkedHashMap();
        Map<Long,CellData> satProcentCols = new LinkedHashMap();
        Map<Long,CellData> FHHbCols = new LinkedHashMap();
        Map<Long,CellData> HbCols = new LinkedHashMap();
        Map<Long,CellData> bebCols = new LinkedHashMap();
        Map<Long,CellData> beecfCols = new LinkedHashMap();
        Map<Long,CellData> wapnCols = new LinkedHashMap();
        Map<Long,CellData> sodCols = new LinkedHashMap();
        Map<Long,CellData> potasCols = new LinkedHashMap();
        Map<Long,CellData> wapńCols = new LinkedHashMap();

        Map<Long,CellData> lacCols = new LinkedHashMap();
        Map<Long,CellData> chlorkiCols = new LinkedHashMap();


        pHCols.put(0L,new CellData(getResources().getString(R.string.pH)));
        pCO2Cols.put(0L,new CellData(getResources().getString(R.string.pCO2)));
        pO2Cols.put(0L,new CellData(getResources().getString(R.string.pO2)));
        kCols.put(0L,new CellData(getResources().getString(R.string.k)));
        naCols.put(0L,new CellData(getResources().getString(R.string.na)));
        caCols.put(0L,new CellData(getResources().getString(R.string.ca)));
        clCols.put(0L,new CellData(getResources().getString(R.string.cl)));
        glukozaCols.put(0L,new CellData(getResources().getString(R.string.glukoza)));
        tHbCols.put(0L,new CellData(getResources().getString(R.string.tHb)));
        sO2Cols.put(0L,new CellData(getResources().getString(R.string.sO2)));

        tBilCols.put(0L,new CellData(getResources().getString(R.string.tBil)));
        cbaseCols.put(0L,new CellData(getResources().getString(R.string.cbase)));
        chco3Cols.put(0L,new CellData(getResources().getString(R.string.chco3)));
        hctCols.put(0L,new CellData(getResources().getString(R.string.hct)));
        fo2hbCols.put(0L,new CellData(getResources().getString(R.string.fo2hb)));
        fcohbCols.put(0L,new CellData(getResources().getString(R.string.fcohb)));
        fmetHbCols.put(0L,new CellData(getResources().getString(R.string.fmetHb)));
        mleczanyCols.put(0L,new CellData(getResources().getString(R.string.mleczany)));
        sbcCols.put(0L,new CellData(getResources().getString(R.string.sbc)));
        hco3aCols.put(0L,new CellData(getResources().getString(R.string.hco3a)));

        tCO2Cols.put(0L,new CellData(getResources().getString(R.string.tCO2)));
        satProcentCols.put(0L,new CellData(getResources().getString(R.string.satProcent)));
        FHHbCols.put(0L,new CellData(getResources().getString(R.string.FHHb)));
        HbCols.put(0L,new CellData(getResources().getString(R.string.Hb)));
        bebCols.put(0L,new CellData(getResources().getString(R.string.beb)));
        beecfCols.put(0L,new CellData(getResources().getString(R.string.beecf)));
        wapnCols.put(0L,new CellData(getResources().getString(R.string.wapnZj)));
        sodCols.put(0L,new CellData(getResources().getString(R.string.sod)));
        potasCols.put(0L,new CellData(getResources().getString(R.string.potas)));
        wapńCols.put(0L,new CellData(getResources().getString(R.string.wapn)));

        lacCols.put(0L,new CellData(getResources().getString(R.string.lac)));
        chlorkiCols.put(0L,new CellData(getResources().getString(R.string.chlorki)));

        for(BloodGasMeasurement completeBloodCountMeasurement : testMeasurements) {
            if (!header.contains(DateTimeFormatterHelper.getStringWithoutSecondFromMillisInTwoLine(completeBloodCountMeasurement.getResultDateLong()))) {
                header.add(DateTimeFormatterHelper.getStringWithoutSecondFromMillisInTwoLine(completeBloodCountMeasurement.getResultDateLong()));
                pHCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                pCO2Cols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                pO2Cols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                kCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                naCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                caCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                clCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                glukozaCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                tHbCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                sO2Cols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));

                tBilCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                cbaseCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                chco3Cols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                hctCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                fo2hbCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                fcohbCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                fmetHbCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                mleczanyCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                sbcCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                hco3aCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));

                tCO2Cols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                satProcentCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                FHHbCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                HbCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                bebCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                beecfCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                wapnCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                sodCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                potasCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                wapńCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));

                lacCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                chlorkiCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
            }
        }

        for(BloodGasMeasurement completeBloodCountMeasurement : testMeasurements){
            switch (Ints.checkedCast(completeBloodCountMeasurement.getTestId())) {
                case 1:
                    pHCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 2:
                    pCO2Cols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 3:
                    pO2Cols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 4:
                    kCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 5:
                    naCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 6:
                    caCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 7:
                    clCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 8:
                    glukozaCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 9:
                    tHbCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 10:
                    sO2Cols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 11:
                    tBilCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 12:
                    cbaseCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 13:
                    chco3Cols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 14:
                    hctCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 15:
                    fo2hbCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 16:
                    fcohbCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 17:
                    fmetHbCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 18:
                    mleczanyCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 19:
                    sbcCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 20:
                    hco3aCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 21:
                    tCO2Cols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 22:
                    satProcentCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 23:
                    FHHbCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 24:
                    HbCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 25:
                    bebCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 26:
                    beecfCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 27:
                    wapnCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 28:
                    sodCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 29:
                    potasCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 30:
                    wapnCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 31:
                    lacCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 32:
                    chlorkiCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;

            }
        }

        rows.add(new ArrayList<>(pHCols.values()));
        rows.add(new ArrayList<>(pCO2Cols.values()));
        rows.add(new ArrayList<>(pO2Cols.values()));
        rows.add(new ArrayList<>(kCols.values()));
        rows.add(new ArrayList<>(naCols.values()));
        rows.add(new ArrayList<>(caCols.values()));
        rows.add(new ArrayList<>(clCols.values()));
        rows.add(new ArrayList<>(glukozaCols.values()));
        rows.add(new ArrayList<>(tHbCols.values()));
        rows.add(new ArrayList<>(sO2Cols.values()));

        rows.add(new ArrayList<>( tBilCols.values()));
        rows.add(new ArrayList<>(cbaseCols.values()));
        rows.add(new ArrayList<>(chco3Cols.values()));
        rows.add(new ArrayList<>(hctCols.values()));
        rows.add(new ArrayList<>(fo2hbCols.values()));
        rows.add(new ArrayList<>(fcohbCols.values()));
        rows.add(new ArrayList<>(fmetHbCols.values()));
        rows.add(new ArrayList<>(mleczanyCols.values()));
        rows.add(new ArrayList<>(sbcCols.values()));
        rows.add(new ArrayList<>(hco3aCols.values()));

        rows.add(new ArrayList<>(tCO2Cols.values()));
        rows.add(new ArrayList<>(satProcentCols.values()));
        rows.add(new ArrayList<>(FHHbCols.values()));
        rows.add(new ArrayList<>(HbCols.values()));
        rows.add(new ArrayList<>(bebCols.values()));
        rows.add(new ArrayList<>(beecfCols.values()));
        rows.add(new ArrayList<>(wapnCols.values()));
        rows.add(new ArrayList<>(sodCols.values()));
        rows.add(new ArrayList<>(potasCols.values()));
        rows.add(new ArrayList<>(wapńCols.values()));

        rows.add(new ArrayList<>(lacCols.values()));
        rows.add(new ArrayList<>(chlorkiCols.values()));
    }

    @Override
    protected void addClickListenersToChartButtons() {

    }

    @Override
    protected List<LineDataSet> prepareChartData() {
        return null;
    }

    @Override
    protected void addStartCharts() {

    }

    @Override
    protected void setButtonsLinearLayoutVisibility(int visible) {

    }
}
