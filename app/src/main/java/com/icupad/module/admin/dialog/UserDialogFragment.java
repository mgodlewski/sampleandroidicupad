package com.icupad.module.admin.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.icupad.R;
import com.icupad.module.admin.AdminFragment;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.builder.general.UserDtoBuilder;
import com.icupad.webconnection.dto.general.UserDto;
import com.icupad.webconnection.dto.general.UserDtoRoles;

import java.io.IOException;

public class UserDialogFragment extends DialogFragment {
    public static String BACKEND_API = "BACKEND_API";
    public static String USER_DTO = "USER_DTO";
    public static String CREATING = "CREATING";

    public static UserDialogFragment newInstance(BackendApi backendApi, UserDto userDto, boolean creating) {
        UserDialogFragment patientDialogFragment = new UserDialogFragment();

        Bundle args = new Bundle();
        args.putSerializable(BACKEND_API, backendApi);
        args.putSerializable(USER_DTO, userDto);
        args.putSerializable(CREATING, creating);
        patientDialogFragment.setArguments(args);

        return patientDialogFragment;
    }

    private BackendApi backendApi;
    private UserDto userDto;
    private boolean creating;
    private UserDtoRoles userDtoRoles;
    private final Handler handler = new Handler();

    private View layout;
    private Resources resources;

    private EditText userDialogLoginEditText;
    private CheckBox userDialogSetPasswordCheckbox;
    private TextView userDialogInfoTextView;
    private EditText userDialogPasswordEditText;
    private EditText userDialogRepeatedPasswordEditText;
    private CheckBox userDialogRoleDoctorCheckbox;
    private CheckBox userDialogRoleNurseCheckbox;
    private CheckBox userDialogRoleAdminCheckbox;
    private CheckBox userDialogRoleDevCheckbox;
    private EditText userDialogNameEditText;
    private EditText userDialogSurnameEditText;
    private EditText userDialogHl7IdEditText;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        resources = getActivity().getApplicationContext().getResources();
        getStringsAndFindAndInitiateViews();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String title;
        if(creating) {
            title = resources.getString(R.string.new_user_dialog_title);
        }
        else {
            title = resources.getString(R.string.edit_user_dialog_title);
        }
        builder.setTitle(title).setView(layout)
                .setNegativeButton(resources.getString(R.string.user_dialog_cancel_button), new OnCancelClickListener())
                .setPositiveButton(resources.getString(R.string.user_dialog_save_button), null);
        return builder.create();
    }

    @Override
    public void onStart()
    {
        super.onStart();    //super.onStart() is where dialog.show() is actually called on the underlying dialog, so we have to do it after this point
        final AlertDialog d = (AlertDialog)getDialog();
        if(d != null)
        {
            Button positiveButton = (Button) d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new OnSaveClickListener());
        }
    }

    private void getStringsAndFindAndInitiateViews() {
        backendApi = (BackendApi) getArguments().getSerializable(BACKEND_API);
        userDto = (UserDto) getArguments().getSerializable(USER_DTO);
        creating = getArguments().getBoolean(CREATING);
        userDtoRoles = new UserDtoRoles(userDto.getRoles());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        layout = inflater.inflate(R.layout.dialog_user, null);

        userDialogLoginEditText = (EditText) layout.findViewById(R.id.userDialogLogin);
        userDialogSetPasswordCheckbox = (CheckBox) layout.findViewById(R.id.user_dialog_set_password);
        userDialogInfoTextView = (TextView) layout.findViewById(R.id.user_dialog_info);
        userDialogPasswordEditText = (EditText) layout.findViewById(R.id.userDialogPassword);
        userDialogRepeatedPasswordEditText = (EditText) layout.findViewById(R.id.userDialogRepeatedPassword);
        userDialogRoleDoctorCheckbox = (CheckBox) layout.findViewById(R.id.user_role_doctor_checkbox);
        userDialogRoleNurseCheckbox = (CheckBox) layout.findViewById(R.id.user_role_nurse_checkbox);
        userDialogRoleAdminCheckbox = (CheckBox) layout.findViewById(R.id.user_role_admin_checkbox);
        userDialogRoleDevCheckbox = (CheckBox) layout.findViewById(R.id.user_role_dev_checkbox);
        userDialogNameEditText = (EditText) layout.findViewById(R.id.userDialogName);
        userDialogSurnameEditText = (EditText) layout.findViewById(R.id.userDialogSurname);
        userDialogHl7IdEditText = (EditText) layout.findViewById(R.id.userDialogHl7Id);

        userDialogLoginEditText.setText(userDto.getLogin());
        userDialogRoleDoctorCheckbox.setChecked(userDtoRoles.isDoctorRole());
        userDialogRoleNurseCheckbox.setChecked(userDtoRoles.isNurseRole());
        userDialogRoleAdminCheckbox.setChecked(userDtoRoles.isAdminRole());
        userDialogNameEditText.setText(userDto.getName());
        userDialogSurnameEditText.setText(userDto.getSurname());
        userDialogHl7IdEditText.setText(userDto.getHl7Id());

        userDialogLoginEditText.setEnabled(creating);
        userDialogSetPasswordCheckbox.setChecked(creating);
        userDialogSetPasswordCheckbox.setEnabled(!creating);
        userDialogSetPasswordCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                setupPasswordEditTextsVisibility(b);
            }
        });
        setupPasswordEditTextsVisibility(creating);

    }

    private void setupPasswordEditTextsVisibility(boolean visibility) {
        if(visibility) {
            userDialogPasswordEditText.setVisibility(View.VISIBLE);
            userDialogRepeatedPasswordEditText.setVisibility(View.VISIBLE);
        }
        else {
            userDialogPasswordEditText.setVisibility(View.GONE);
            userDialogRepeatedPasswordEditText.setVisibility(View.GONE);
        }
    }

    private class OnCancelClickListener implements DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int id) {
        }
    }

    private class OnSaveClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            if(userDialogLoginEditText.getText().toString().isEmpty()) {
                userDialogInfoTextView.setText(resources.getString(R.string.user_dialog_no_login_error_text));
                return;
            }
            if(userDialogSetPasswordCheckbox.isChecked() &&
                    userDialogPasswordEditText.getText().toString().isEmpty()) {
                userDialogInfoTextView.setText(resources.getString(R.string.user_dialog_no_password_error_text));
                return;
            }
            if(userDialogSetPasswordCheckbox.isChecked() &&
                    !userDialogPasswordEditText.getText().toString().equals(
                            userDialogRepeatedPasswordEditText.getText().toString())) {
                userDialogInfoTextView.setText(resources.getString(R.string.user_dialog_no_matching_passwords_error_texts));
                return;
            }
            userDtoRoles.setDoctorRole(userDialogRoleDoctorCheckbox.isChecked());
            userDtoRoles.setNurseRole(userDialogRoleNurseCheckbox.isChecked());
            userDtoRoles.setAdminRole(userDialogRoleAdminCheckbox.isChecked());
            userDtoRoles.setDevRole(userDialogRoleDevCheckbox.isChecked());
            UserDto userDto = UserDtoBuilder.anUserDto()
                    .withLogin(getStringOrNullIfEmpty(userDialogLoginEditText))
                    .withRoles(userDtoRoles.getStrings())
                    .withName(getStringOrNullIfEmpty(userDialogNameEditText))
                    .withSurname(getStringOrNullIfEmpty(userDialogSurnameEditText))
                    .withHl7Id(getStringOrNullIfEmpty(userDialogHl7IdEditText))
                    .withPasswordAttached(userDialogSetPasswordCheckbox.isChecked())
                    .withPassword(getStringOrNullIfEmpty(userDialogPasswordEditText))
                    .build();

            if(creating) {
                new AddingUserThread(userDto).start();
            }
            else {
                new UpdatingUserThread(userDto).start();
            }
        }

        private String getStringOrNullIfEmpty(EditText userDialogLoginEditText) {
            String text = userDialogLoginEditText.getText().toString();
            if(text.isEmpty()) {
                return null;
            }
            else {
                return text;
            }
        }
    }

    private class AddingUserThread extends Thread {
        private UserDto userDto;
        public AddingUserThread(UserDto userDto) {
            this.userDto = userDto;
        }
        @Override
        public void run(){
            try {
                userDto = backendApi.addUser(userDto);
                if(userDto == null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            userDialogInfoTextView.setText(resources.getString(R.string.user_dialog_used_login_is_duplicate));
                        }
                    });
                }
                else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Fragment fragment = getTargetFragment();
                            fragment.onResume();
                            dismiss();
                        }
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class UpdatingUserThread extends Thread {
        private UserDto userDto;
        public UpdatingUserThread(UserDto userDto) {
            this.userDto = userDto;
        }
        @Override
        public void run(){
            try {
                backendApi.updateUser(userDto);
                handler.post(new Runnable(){
                    @Override
                    public void run(){
                        Fragment fragment = getTargetFragment();
                        fragment.onResume();
                        dismiss();
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
