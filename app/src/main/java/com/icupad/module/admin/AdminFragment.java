package com.icupad.module.admin;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.icupad.R;
import com.icupad.commons.ModuleFragment;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.grable.GrableView;
import com.icupad.grable.listener.OnRowClickListener;
import com.icupad.grable.model.Cell;
import com.icupad.grable.model.GrableMode;
import com.icupad.module.admin.dialog.UserDialogFragment;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.dto.general.UserDto;

import java.io.IOException;
import java.util.List;

public class AdminFragment extends ModuleFragment {
    static public final int DIALOG_FRAGMENT = 12496;

    private BackendApi backendApi;
    private GrableView usersView;
    private Button addUserButton;

    private List<UserDto> users;

    private final Handler handler = new Handler();

    public AdminFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin, container, false);
        backendApi = new BackendApi(getActivity().getApplicationContext());

        fragmentListener.onButtonPressed(Message.FRAGMENT_LOADING);
        usersView = (GrableView) view.findViewById(R.id.usersView);
        addUserButton = (Button) view.findViewById(R.id.addUser);

        new LoadingUsersThread().start();
        addUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserDialogFragment userDialogFragment = UserDialogFragment.newInstance(
                        backendApi, new UserDto(), true);
                userDialogFragment.setTargetFragment(AdminFragment.this, DIALOG_FRAGMENT);
                userDialogFragment.show(getFragmentManager(), "");
            }
        });
        fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(usersView != null) {
            new LoadingUsersThread().start();
        }
    }

    private class LoadingUsersThread extends Thread {
        @Override
        public void run(){
            try {
                users = backendApi.getUsers();
                handler.post(new Runnable(){
                    @Override
                    public void run(){
                        createTable();
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void createTable() {
        usersView.clear();
        addColumnsToGrable();
        addRowsAndDataToGrable();
        usersView.setOnRowClickListener(new OnRowClickListenerImpl());
        usersView.setGrableMode(GrableMode.TABLE);
        usersView.invalidate();
        usersView.requestLayout();
    }

    private void addColumnsToGrable() {
        usersView.addColumn(new String[]{getResources().getString(R.string.user_login)});
        usersView.addColumn(new String[]{getResources().getString(R.string.user_password)});
        usersView.addColumn(new String[]{getResources().getString(R.string.user_roles)});
        usersView.addColumn(new String[]{getResources().getString(R.string.user_name)});
        usersView.addColumn(new String[]{getResources().getString(R.string.user_surname)});
        usersView.addColumn(new String[]{getResources().getString(R.string.user_hl7Id)});
    }

    private void addRowsAndDataToGrable() {
        Cell[][] data = new Cell[users.size()][6];
        int userRowNo = 0;
        for(UserDto userDto : users) {
            usersView.addRow("");
            data[userRowNo][0] = new Cell(userDto.getLogin());
            data[userRowNo][1] = new Cell("*****");
            data[userRowNo][2] = new Cell(getStringFromListOfStrings(userDto.getRoles()));
            data[userRowNo][3] = new Cell(userDto.getName());
            data[userRowNo][4] = new Cell(userDto.getSurname());
            data[userRowNo][5] = new Cell(userDto.getHl7Id());
            userRowNo++;
        }
        usersView.addAllCells(data);
    }

    private String getStringFromListOfStrings(List<String> roles) {
        String result = "";
        if( !roles.isEmpty()) {
            for(int i = 0; i < roles.size() - 1; i++) {
                result += roles.get(i) + ", ";
            }
            result += roles.get(roles.size() - 1);
        }
        return result;
    }

    private class OnRowClickListenerImpl implements OnRowClickListener {
        @Override
        public void onRowClick(int y) {
            if(y >= 0) {
                UserDialogFragment userDialogFragment = UserDialogFragment.newInstance(
                        backendApi, users.get(y), false);
                userDialogFragment.setTargetFragment(AdminFragment.this, DIALOG_FRAGMENT);
                userDialogFragment.show(getFragmentManager(), "");
                new LoadingUsersThread().start();
            }
        }
    }
}
