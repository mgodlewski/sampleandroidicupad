package com.icupad.module.login;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.icupad.R;
import com.icupad.synchronization.SynchronizerService;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.webconnection.model.ConnectionStatus;
import com.icupad.webconnection.model.LoginStatus;
import com.icupad.webconnection.service.LoginService;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.ModuleFragment;

import org.apache.commons.codec.DecoderException;

import java.io.UnsupportedEncodingException;

public class LoginFragment extends ModuleFragment {

    private Button buttonLogin;
    private ProgressBar progressBar;
    private TextView connectionErrorTextView;
    private TextView loginErrorTextView;
    private TextView protocolTextView;
    private EditText protocolEditText;
    private TextView addressTextView;
    private EditText addressEditText;
    private TextView portTextView;
    private EditText portEditText;
    private TextView settingsTextView;
    private TextView settingsInformationTextView;
    private EditText loginEditText;
    private EditText passwordEditText;

    public LoginFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        findAllViews(view);
        initializeConnectionOptions();
        startSynchronizationService();

        buttonLogin.setOnClickListener(new LoginInitiator());
        view.findViewById(R.id.repoSettingsLink).setOnClickListener(new RepositorySettingsToggler());
        fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);

        return view;
    }

    private void findAllViews(View view) {
        buttonLogin = (Button) view.findViewById(R.id.loginButton);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        loginErrorTextView = (TextView) view.findViewById(R.id.loginErrorTextView);
        connectionErrorTextView = (TextView) view.findViewById(R.id.connectionErrorTextView);
        loginErrorTextView.setVisibility(View.INVISIBLE);

        protocolTextView = (TextView)view.findViewById(R.id.repoSettingsProtocolTextView);
        protocolEditText = (EditText)view.findViewById(R.id.repoSettingsProtocolEditText);
        addressTextView = (TextView)view.findViewById(R.id.repoSettingsAddressTextView);
        addressEditText = (EditText)view.findViewById(R.id.repoSettingsAddressEditText);
        portTextView = (TextView)view.findViewById(R.id.repoSettingsPortTextView);
        portEditText = (EditText)view.findViewById(R.id.repoSettingsPortEditText);
        settingsTextView = (TextView)view.findViewById(R.id.repoSettingsLink);
        settingsInformationTextView = (TextView)view.findViewById(R.id.repoSettingsInformationTextView);

        loginEditText = (EditText)view.findViewById(R.id.loginEditText);
        passwordEditText = (EditText)view.findViewById(R.id.passwordEditText);
    }

    private void initializeConnectionOptions() {
        protocolEditText.setText(PrivateDataAccessor.getDefaultProtocol());
        addressEditText.setText(PrivateDataAccessor.getDefaultAddress());
        portEditText.setText(PrivateDataAccessor.getDefaultPort());
    }

    private class LoginInitiator implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            String login = loginEditText.getText().toString();
            String password = passwordEditText.getText().toString();
            buttonLogin.setEnabled(false);
            tryLogin(login, password);
        }
    }

    private void tryLogin(String login, String password) {
        setupProgressBar();
        PrivateDataAccessor.setDefaultProtocol(protocolEditText.getText().toString());
        PrivateDataAccessor.setDefaultAddress(addressEditText.getText().toString());
        PrivateDataAccessor.setDefaultPort(portEditText.getText().toString());

        startLoginService(login, password);
    }

    private boolean loginServiceStarted = false;
    private void startLoginService(String login, String password) {
        loginServiceStarted = true;
        Intent loginServiceIntent = new Intent(getActivity(), LoginService.class);
        loginServiceIntent.putExtra(LoginService.LOGIN_FIELD, login);
        loginServiceIntent.putExtra(LoginService.PASSWORD_FIELD, password);
        getActivity().startService(loginServiceIntent);
        getActivity().registerReceiver(loginReceiver, new IntentFilter(LoginService.NOTIFICATION));
    }

    private BroadcastReceiver loginReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            stopLoginService();

            if (bundle != null) {
                LoginStatus loginStatus = LoginStatus.valueOf(bundle.getString(LoginService.MESSAGE));
                reactOnLoginStatus(loginStatus);
            }
            buttonLogin.setEnabled(true);
            setupProgressBar();
        }
    };

    private void stopLoginService() {
        if(loginServiceStarted) {
            getActivity().unregisterReceiver(loginReceiver);
        }
        loginServiceStarted = false;
    }

    private void reactOnLoginStatus(LoginStatus loginStatus) {
        String login = ((EditText)getActivity().findViewById(R.id.loginEditText)).getText().toString();
        String password = ((EditText)getActivity().findViewById(R.id.passwordEditText)).getText().toString();
        switch(loginStatus) {
            case VALID:
                try {
                    PrivateDataAccessor.updateAndInitUserSharedPreferences(getActivity().getApplicationContext(), login, password);
                    succeedLogin();
                } catch (DecoderException | UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            case INVALID:
                loginErrorTextView.setText(getResources().getString(R.string.login_error));
                loginErrorTextView.setVisibility(View.VISIBLE);
                if(PrivateDataAccessor.loginOffline(getActivity().getApplicationContext(), login, password)) {
                    succeedLogin();
                }
                break;
        }
    }

    private void succeedLogin() {
        PrivateDataAccessor.saveConnectionDefaultOptions();
        getActivity().stopService(new Intent(getActivity(), SynchronizerService.class));
        getActivity().unregisterReceiver(synchronizationReceiver);
        fragmentListener.onButtonPressed(Message.LOGIN);
        passwordEditText.setText("");

    }

    private void startSynchronizationService() {
        Intent synchronizeServiceIntent = new Intent(getActivity(), SynchronizerService.class);
        synchronizeServiceIntent.putExtra(SynchronizerService.DB_SYNCHRONIZATION, false);
        getActivity().startService(synchronizeServiceIntent);
        getActivity().registerReceiver(synchronizationReceiver, new IntentFilter(SynchronizerService.NOTIFICATION));
    }

    private BroadcastReceiver synchronizationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String message = bundle.getString(SynchronizerService.MESSAGE);
                switch(bundle.getString(SynchronizerService.TYPE)) {
                    case SynchronizerService.TYPE_CONNECTION_STATUS:
                        reactOnConnectionStatus(ConnectionStatus.valueOf(message));
                        break;
                }
            }
        }
    };

    private void reactOnConnectionStatus(ConnectionStatus connectionStatus) {
        switch(connectionStatus) {
            case NO_CONNECTION:
                connectionErrorTextView.setVisibility(View.VISIBLE);
                connectionErrorTextView.setText("No Internet connection.");
                break;
            case CONNECTION_TO_INTERNET:
                connectionErrorTextView.setVisibility(View.VISIBLE);
                connectionErrorTextView.setText("No connection to the server.");
                break;
            case CONNECTION_TO_SERVER:
                connectionErrorTextView.setVisibility(View.INVISIBLE);
                break;
        }
    }

    private void setupProgressBar() {
        if( !buttonLogin.isEnabled()) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

    private class RepositorySettingsToggler implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            if(protocolTextView.getVisibility() == View.GONE) {
                settingsTextView.setText(R.string.close_repo_settings_link);
                protocolTextView.setVisibility(View.VISIBLE);
                protocolEditText.setVisibility(View.VISIBLE);
                addressTextView.setVisibility(View.VISIBLE);
                addressEditText.setVisibility(View.VISIBLE);
                portTextView.setVisibility(View.VISIBLE);
                portEditText.setVisibility(View.VISIBLE);
                settingsInformationTextView.setVisibility(View.VISIBLE);
            } else {
                settingsTextView.setText(R.string.open_repo_settings_link);
                protocolTextView.setVisibility(View.GONE);
                protocolEditText.setVisibility(View.GONE);
                addressTextView.setVisibility(View.GONE);
                addressEditText.setVisibility(View.GONE);
                portTextView.setVisibility(View.GONE);
                portEditText.setVisibility(View.GONE);
                settingsInformationTextView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        stopLoginService();
    }
}