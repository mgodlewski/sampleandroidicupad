package com.icupad.module.conflictresolver.populator;

import android.content.Context;

import com.icupad.R;
import com.icupad.commons.repository.model.ConflictGeneral;
import com.icupad.grable.GrableView;
import com.icupad.grable.model.Cell;

import java.util.List;

public class ConflictsDataPopulator {
    private List<ConflictGeneral> conflicts;

    public ConflictsDataPopulator(List<ConflictGeneral> conflicts) {
        this.conflicts = conflicts;
    }

    public GrableView populate(Context context, GrableView grableView) {
        Cell[][] data = new Cell[conflicts.size()][1];
        grableView.addColumn(new String[]{context.getResources().getString(R.string.conflict_resolver_table_name_column)});
        for(int i = 0; i < conflicts.size(); i++) {
            grableView.addRow("");
            data[i][0] = new Cell(conflicts.get(i).getTableName());
        }
        grableView.addAllCells(data);

        return grableView;
    }
}
