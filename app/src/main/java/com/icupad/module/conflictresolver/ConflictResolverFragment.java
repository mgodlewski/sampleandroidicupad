package com.icupad.module.conflictresolver;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.icupad.R;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.module.conflictresolver.populator.ConflictsDataPopulator;
import com.icupad.synchronization.conflict.resolver.ConflictResolver;
import com.icupad.synchronization.conflict.resolver.patient.OperationConflictResolver;
import com.icupad.synchronization.conflict.resolver.patient.PatientConflictResolver;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.ModuleFragment;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.ConflictGeneral;
import com.icupad.grable.GrableView;
import com.icupad.grable.listener.OnCellClickListener;
import com.icupad.grable.listener.OnRowClickListener;
import com.icupad.grable.model.GrableMode;
import com.icupad.synchronization.conflict.resolver.stethoscope.AuscultateConflictResolver;
import com.icupad.synchronization.conflict.resolver.stethoscope.AuscultatePointConflictResolver;
import com.icupad.synchronization.conflict.resolver.stethoscope.AuscultateSuiteConflictResolver;
import com.icupad.synchronization.conflict.resolver.stethoscope.AuscultateSuiteSchemaConflictResolver;

import org.greenrobot.eventbus.EventBus;

import java.util.List;


public class ConflictResolverFragment extends ModuleFragment {

    private GrableView grableView;
    private Button generalConflictsButton;
    private Button saveResultButton;
    private TextView informationTextView;

    private PublicIcupadRepository publicIcupadRepository;
    private List<ConflictGeneral> conflicts;
    private ConflictsDataPopulator conflictsDataPopulator;

    private PatientConflictResolver patientConflictResolver;
    private OperationConflictResolver operationConflictResolver;

    private AuscultateConflictResolver auscultateConflictResolver;
    private AuscultateSuiteConflictResolver auscultateSuiteConflictResolver;
    private AuscultatePointConflictResolver auscultatePointConflictResolver;
    private AuscultateSuiteSchemaConflictResolver auscultateSuiteSchemaConflictResolver;

    private ConflictGeneral selectedConflictGeneral;

    private int chosenConflict;

    private final Handler handler = new Handler();

    public ConflictResolverFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_conflict_resolver, container, false);
        Context context = getActivity().getApplicationContext();

        fragmentListener.onButtonPressed(Message.FRAGMENT_LOADING);
        grableView = (GrableView)view.findViewById(R.id.canvasView);
        generalConflictsButton = (Button)view.findViewById(R.id.generalConflictsButton);
        saveResultButton = (Button)view.findViewById(R.id.SaveResultButton);
        informationTextView = (TextView)view.findViewById(R.id.informationTextView);

        patientConflictResolver = new PatientConflictResolver(context);
        operationConflictResolver = new OperationConflictResolver(context);

        auscultateConflictResolver = new AuscultateConflictResolver(context);
        auscultateSuiteConflictResolver = new AuscultateSuiteConflictResolver(context);
        auscultatePointConflictResolver = new AuscultatePointConflictResolver(context);
        auscultateSuiteSchemaConflictResolver = new AuscultateSuiteSchemaConflictResolver(context);

        generalConflictsButton.setVisibility(View.INVISIBLE);
        saveResultButton.setVisibility(View.INVISIBLE);

        generalConflictsButton.setOnClickListener(new ShowConflictsOnClickListener());

        publicIcupadRepository = fragmentListener.getRepository();

        new LoadingPatientsThread().start();

        return view;
    }

    private class LoadingPatientsThread extends Thread {
        @Override
        public void run(){
            conflicts = publicIcupadRepository.getConflicts();
            conflictsDataPopulator = new ConflictsDataPopulator(conflicts);
            handler.post(new Runnable(){
                @Override
                public void run(){
                    setupGeneralConflicts();
                    fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
                }
            });
        }
    }

    public void setupGeneralConflicts() {
        grableView.clear();
        grableView = conflictsDataPopulator.populate(getActivity().getApplicationContext(), grableView);
        grableView.setOnRowClickListener(new OnRowClickListenerImpl());
        grableView.setOnCellClickListener(null);
        grableView.setGrableMode(GrableMode.TABLE);
        generalConflictsButton.setVisibility(View.INVISIBLE);
        saveResultButton.setVisibility(View.INVISIBLE);
        grableView.postInvalidate();
        grableView.invalidate();
    }

    private class OnRowClickListenerImpl implements OnRowClickListener {
        @Override
        public void onRowClick(int position) {
            if(position >= 0) {
                chosenConflict = position;
                selectedConflictGeneral = conflicts.get(position);
                setUpButtonsAndGrableViewForSelectedRow(selectedConflictGeneral);
            }
        }

        private void setUpButtonsAndGrableViewForSelectedRow(ConflictGeneral selected) {
            switch(selected.getTableName()) {
                case(IcupadDbContract.Patient.TABLE_NAME):
                    setUpButtonsAndGrableView(patientConflictResolver, selected.getLocalInternalId(), selected.getRemoteInternalId());
                    break;
                case(IcupadDbContract.Operation.TABLE_NAME):
                    setUpButtonsAndGrableView(operationConflictResolver, selected.getLocalInternalId(), selected.getRemoteInternalId());
                    break;
                case(IcupadDbContract.Auscultate.TABLE_NAME):
                    setUpButtonsAndGrableView(auscultateConflictResolver, selected.getLocalInternalId(), selected.getRemoteInternalId());
                    break;
                case(IcupadDbContract.AuscultateSuite.TABLE_NAME):
                    setUpButtonsAndGrableView(auscultateSuiteConflictResolver, selected.getLocalInternalId(), selected.getRemoteInternalId());
                    break;
                case(IcupadDbContract.AuscultatePoint.TABLE_NAME):
                    setUpButtonsAndGrableView(auscultatePointConflictResolver, selected.getLocalInternalId(), selected.getRemoteInternalId());
                    break;
                case(IcupadDbContract.AuscultateSuiteSchema.TABLE_NAME):
                    setUpButtonsAndGrableView(auscultateSuiteSchemaConflictResolver, selected.getLocalInternalId(), selected.getRemoteInternalId());
                    break;
            }
        }

        private void setUpButtonsAndGrableView(ConflictResolver conflictResolver, long baseRowId, long remoteRowId) {
            grableView.clear();
            grableView = conflictResolver.populateConflictResolvingGrableView(
                    grableView, baseRowId, remoteRowId);
            grableView.setGrableMode(GrableMode.TABLE);
            grableView.setOnRowClickListener(null);
            grableView.setOnCellClickListener(new OnCellClickListenerImpl());
            grableView.invalidate();
            saveResultButton.setVisibility(View.VISIBLE);
            saveResultButton.setOnClickListener(new SaveResultOnClickListener());
            generalConflictsButton.setVisibility(View.VISIBLE);
        }
    }

    private class OnCellClickListenerImpl implements OnCellClickListener {
        @Override
        public void onCellClick(int x, int y) {
            if(y < 2) {
                String value = grableView.getCell(x, y).getValue();
                grableView.updateCellBackgroundColorAtXAtY(null, x, 0);
                grableView.updateCellBackgroundColorAtXAtY(null, x, 1);
                grableView.updateCellValueAtXAtY(value, x, 2);
                grableView.updateCellBackgroundColorAtXAtY(Color.GREEN, x, y);
            }
            else if(y == 2) {
                Handler handler = new Handler();
                handler.post(new StringChangerPopup(x, y));
            }
        }
    }

    private class SaveResultOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            patientConflictResolver.succeedConflict(grableView, selectedConflictGeneral.getLocalInternalId(), selectedConflictGeneral.getRemoteInternalId());
            conflicts.remove(chosenConflict);
            if(conflicts.size() == 0) {
                EventBus.getDefault().post(Message.DISABLE_CONFLICT_BUTTON);
            }
            conflictsDataPopulator = new ConflictsDataPopulator(conflicts);
            setupGeneralConflicts();
        }
    }

    private class ShowConflictsOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            setupGeneralConflicts();
        }
    }

    private class StringChangerPopup implements Runnable {
        private int x, y;
        public StringChangerPopup(int x, int y) {
            this.x = x;
            this.y = y;
        }
        @Override
        public void run() {
            Context context = getActivity();
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            final EditText text = new EditText(context);
            text.setText(grableView.getCell(x, y).getValue());

            builder.setTitle(getResources().getString(R.string.conflict_resolver_change_text_title))
                    .setMessage(getResources().getString(R.string.conflict_resolver_remote_row) +
                            "\n" + grableView.getCell(x, 0).getValue() +
                            "\n\n" + getResources().getString(R.string.conflict_resolver_local_row) +
                            ":\n" + grableView.getCell(x, 1).getValue() +
                            "\n\n" + getResources().getString(R.string.conflict_resolver_solution_row) + ":")
                    .setView(text);
            builder.setPositiveButton(getResources().getString(R.string.conflict_resolver_change_value),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface di, int i) {
                            grableView.updateCellValueAtXAtY(text.getText().toString(), x, y);
                            grableView.invalidate();
                        }
                    });
            builder.setNegativeButton(getResources().getString(R.string.conflict_resolver_cancel_change),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface di, int i) {
                        }
                    });
            builder.create().show();
        }
    }

}
