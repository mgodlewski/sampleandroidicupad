package com.icupad.module.debug;

import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.icupad.R;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.commons.ModuleFragment;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.grable.GrableView;
import com.icupad.grable.model.Cell;
import com.icupad.grable.model.GrableMode;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DebugFragment extends ModuleFragment {

    private PublicIcupadRepository publicIcupadRepository;
    private GrableView grableView;
    private Spinner spinner;

    private String[] tableNames;
    private Map<String, String[]> tableNameToColumnNames;

    IcupadDbHelper mDbHelper;

    private final Handler handler = new Handler();

    private Logger LOGGER = Logger.getLogger(this.getClass().toString());

    public DebugFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_debug, container, false);
        publicIcupadRepository = fragmentListener.getRepository();

        grableView = (GrableView) view.findViewById(R.id.grableView);
        spinner = (Spinner) view.findViewById(R.id.spinner);
        mDbHelper = new IcupadDbHelper(getActivity().getApplicationContext());

        setupSpinner();
        fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
        return view;
    }

    private void setupSpinner() {
        createTablesInfo();

        ArrayAdapter<String> adapter = getTablesArrayAdapter();
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new TableShowerOnClickListener());
    }

    private void createTablesInfo() {
        TableDataPopulator tableDataPopulator = new TableDataPopulator();
        tableDataPopulator.populate();
        this.tableNames = tableDataPopulator.getTableNames();
        this.tableNameToColumnNames = tableDataPopulator.getTableNameToColumnNames();
    }

    @NonNull
    private ArrayAdapter<String> getTablesArrayAdapter() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity().getApplicationContext(),
                R.layout.spinner_item_black_text, tableNames);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        return adapter;
    }

    private class TableShowerOnClickListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            LOGGER.log(Level.WARNING, "[GradleInit] onItemSelected");
            String tableNameSelected = tableNames[i];
            String[] columns = tableNameToColumnNames.get(tableNameSelected);
            SQLiteDatabase db = mDbHelper.getReadableDatabase();

            Query query = QueryBuilder.aQuery(db)
                    .withTable(tableNameSelected)
                    .withColumns(columns)
                    .build();

            Cursor cursor = query.select();
            cursor.moveToFirst();

            grableView.clear();
            addColumnsToGrable(columns);

            int row = 1;
            LOGGER.log(Level.WARNING, "[GradleInit] init data");
            long addingRowsTime = 0;
            long addingCellsTime = 0;
            while(!cursor.isAfterLast()) {
                long time1 = System.currentTimeMillis();
                grableView.addRow("");
                long time2 = System.currentTimeMillis();
                for(int col = 0; col < columns.length; col++) {
                    grableView.addCellAtColumnAtRow(
                            new Cell( (cursor.getString(col)==null) ? "" : cursor.getString(col) ), col+1, row);
                }
                long time3 = System.currentTimeMillis();
                addingRowsTime += time2 - time1;
                addingCellsTime += time3 - time2;
                cursor.moveToNext();
                row++;
            }
            LOGGER.log(Level.WARNING, "[GradleInit] addingRowsTime=" + addingRowsTime/1000 +"s, addingCellsTime=" + addingCellsTime/1000 + "s");
            LOGGER.log(Level.INFO, "[GradleInit] added " + row + " rows");
            LOGGER.log(Level.INFO, "[GradleInit] added " + columns.length + " columns");
            LOGGER.log(Level.WARNING, "[GradleInit] invalidate");

            cursor.close();
            mDbHelper.closeDb(db);

            grableView.setGrableMode(GrableMode.TABLE);
            grableView.requestLayout();
            grableView.invalidate();
            LOGGER.log(Level.WARNING, "[GradleInit] end");
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private void addColumnsToGrable(String[] columns) {
        for(int i = 0; i < columns.length; i++) {
            grableView.addColumn(new String[]{columns[i]});
        }
    }
}
