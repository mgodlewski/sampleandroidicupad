package com.icupad.module.debug;

import com.icupad.db.management.IcupadDbContract;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class TableDataPopulator {
    private Map<String, String[]> tableNameToColumnNames;

    public void populate() {
        Map<String, String[]> tableNameToColumnNames = new TreeMap<>();
        tableNameToColumnNames.put( IcupadDbContract.Patient.TABLE_NAME, new String[]{
                IcupadDbContract.Patient._ID, IcupadDbContract.Patient.COLUMN_NAME_ID,
                IcupadDbContract.Patient.COLUMN_NAME_NAME, IcupadDbContract.Patient.COLUMN_NAME_SURNAME,
                IcupadDbContract.Patient.COLUMN_NAME_SEX, IcupadDbContract.Patient.COLUMN_NAME_BIRTH_DATE,
                IcupadDbContract.Patient.COLUMN_NAME_PESEL, IcupadDbContract.Patient.COLUMN_NAME_HL7ID,
                IcupadDbContract.Patient.COLUMN_NAME_STREET, IcupadDbContract.Patient.COLUMN_NAME_STREET_NUMBER,
                IcupadDbContract.Patient.COLUMN_NAME_HOUSE_NUMBER, IcupadDbContract.Patient.COLUMN_NAME_POSTAL_CODE,
                IcupadDbContract.Patient.COLUMN_NAME_CITY, IcupadDbContract.Patient.COLUMN_NAME_BLOOD_TYPE,
                IcupadDbContract.Patient.COLUMN_NAME_ALLERGIES, IcupadDbContract.Patient.COLUMN_NAME_OTHER_IMPORTANT_INFORMATIONS,
                IcupadDbContract.Patient.COLUMN_NAME_CREATED_DATE, IcupadDbContract.Patient.COLUMN_NAME_LAST_MODIFIED_DATE,
                IcupadDbContract.Patient.COLUMN_NAME_CREATED_BY_ID, IcupadDbContract.Patient.COLUMN_NAME_LAST_MODIFIED_BY_ID,
                IcupadDbContract.Patient.COLUMN_NAME_SYNC});

        tableNameToColumnNames.put( IcupadDbContract.AuscultateSuiteSchema.TABLE_NAME, new String[]{
                IcupadDbContract.AuscultateSuiteSchema._ID, IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_ID,
                IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_NAME, IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_IS_PUBLIC,
                IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_CREATED_DATE, IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_LAST_MODIFIED_DATE,
                IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_CREATED_BY_ID, IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_LAST_MODIFIED_BY_ID,
                IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_SYNC});

        tableNameToColumnNames.put( IcupadDbContract.AuscultatePoint.TABLE_NAME, new String[]{
                IcupadDbContract.AuscultatePoint._ID, IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_ID,
                IcupadDbContract.AuscultatePoint.COLUMN_NAME_NAME, IcupadDbContract.AuscultatePoint.COLUMN_NAME_QUEUE_POSITION,
                IcupadDbContract.AuscultatePoint.COLUMN_NAME_X, IcupadDbContract.AuscultatePoint.COLUMN_NAME_Y,
                IcupadDbContract.AuscultatePoint.COLUMN_NAME_IS_FRONT, IcupadDbContract.AuscultatePoint.COLUMN_NAME_SUITE_SCHEMA_ID,
                IcupadDbContract.AuscultatePoint.COLUMN_NAME_CREATED_DATE, IcupadDbContract.AuscultatePoint.COLUMN_NAME_LAST_MODIFIED_DATE,
                IcupadDbContract.AuscultatePoint.COLUMN_NAME_CREATED_BY_ID, IcupadDbContract.AuscultatePoint.COLUMN_NAME_LAST_MODIFIED_BY_ID,
                IcupadDbContract.AuscultatePoint.COLUMN_NAME_SYNC});

        tableNameToColumnNames.put( IcupadDbContract.AuscultateSuite.TABLE_NAME, new String[]{
                IcupadDbContract.AuscultateSuite._ID, IcupadDbContract.AuscultateSuite.COLUMN_NAME_ID,
                IcupadDbContract.AuscultateSuite.COLUMN_NAME_PATIENT_ID, IcupadDbContract.AuscultateSuite.COLUMN_NAME_DESCRIPTION,
                IcupadDbContract.AuscultateSuite.COLUMN_NAME_SUITE_SCHEMA_ID, IcupadDbContract.AuscultateSuite.COLUMN_NAME_POSITION,
                IcupadDbContract.AuscultateSuite.COLUMN_NAME_TEMPERATURE, IcupadDbContract.AuscultateSuite.COLUMN_NAME_IS_RESPIRATED,
                IcupadDbContract.AuscultateSuite.COLUMN_NAME_PASSIVE_OXYGEN_THERAPY, IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXAMINATION_DATETIME,
                IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXECUTOR_HL7ID, IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXECUTOR_NAME,
                IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXECUTOR_SURNAME,
                IcupadDbContract.AuscultateSuite.COLUMN_NAME_CREATED_DATE, IcupadDbContract.AuscultateSuite.COLUMN_NAME_LAST_MODIFIED_DATE,
                IcupadDbContract.AuscultateSuite.COLUMN_NAME_CREATED_BY_ID, IcupadDbContract.AuscultateSuite.COLUMN_NAME_LAST_MODIFIED_BY_ID,
                IcupadDbContract.AuscultateSuite.COLUMN_NAME_SYNC});

        tableNameToColumnNames.put( IcupadDbContract.Auscultate.TABLE_NAME, new String[]{
                IcupadDbContract.Auscultate._ID, IcupadDbContract.Auscultate.COLUMN_NAME_ID,
                IcupadDbContract.Auscultate.COLUMN_NAME_DESCRIPTION, IcupadDbContract.Auscultate.COLUMN_NAME_WAV_NAME,
                IcupadDbContract.Auscultate.COLUMN_NAME_POINT_ID, IcupadDbContract.Auscultate.COLUMN_NAME_SUITE_ID,
                IcupadDbContract.Auscultate.COLUMN_NAME_SUITE_INTERNAL_ID, IcupadDbContract.Auscultate.COLUMN_NAME_POLL_RESULT,
                IcupadDbContract.Auscultate.COLUMN_NAME_CREATED_DATE, IcupadDbContract.Auscultate.COLUMN_NAME_LAST_MODIFIED_DATE,
                IcupadDbContract.Auscultate.COLUMN_NAME_CREATED_BY_ID, IcupadDbContract.Auscultate.COLUMN_NAME_LAST_MODIFIED_BY_ID,
                IcupadDbContract.Auscultate.COLUMN_NAME_SYNC});

        tableNameToColumnNames.put( IcupadDbContract.Stay.TABLE_NAME, new String[]{
                IcupadDbContract.Stay._ID, IcupadDbContract.Stay.COLUMN_NAME_ID,
                IcupadDbContract.Stay.COLUMN_NAME_ACTIVE, IcupadDbContract.Stay.COLUMN_NAME_ADMIT_DATE,
                IcupadDbContract.Stay.COLUMN_NAME_ADMITTING_DOCTOR_HL7ID, IcupadDbContract.Stay.COLUMN_NAME_ADMITTING_DOCTOR_NAME,
                IcupadDbContract.Stay.COLUMN_NAME_NPWZ, IcupadDbContract.Stay.COLUMN_NAME_SURNAME,
                IcupadDbContract.Stay.COLUMN_NAME_NAME, IcupadDbContract.Stay.COLUMN_NAME_DISCHARGE_DATE,
                IcupadDbContract.Stay.COLUMN_NAME_HL7ID, IcupadDbContract.Stay.COLUMN_NAME_TYPE,
                IcupadDbContract.Stay.COLUMN_NAME_PATIENT_ID,
                IcupadDbContract.Stay.COLUMN_NAME_CREATED_DATE, IcupadDbContract.Stay.COLUMN_NAME_LAST_MODIFIED_DATE,
                IcupadDbContract.Stay.COLUMN_NAME_CREATED_BY_ID, IcupadDbContract.Stay.COLUMN_NAME_LAST_MODIFIED_BY_ID,
                IcupadDbContract.Stay.COLUMN_NAME_SYNC});

        tableNameToColumnNames.put( IcupadDbContract.Operation.TABLE_NAME, new String[]{
                IcupadDbContract.Operation._ID,
                IcupadDbContract.Operation.COLUMN_NAME_ID,
                IcupadDbContract.Operation.COLUMN_NAME_STAY_ID,
                IcupadDbContract.Operation.COLUMN_NAME_OPERATING_DATE,
                IcupadDbContract.Operation.COLUMN_NAME_CREATED_DATE,
                IcupadDbContract.Operation.COLUMN_NAME_LAST_MODIFIED_DATE,
                IcupadDbContract.Operation.COLUMN_NAME_DESCRIPTION,
                IcupadDbContract.Operation.COLUMN_NAME_ARCHIVED,

                IcupadDbContract.Operation.COLUMN_NAME_CREATED_BY_ID,
                IcupadDbContract.Operation.COLUMN_NAME_LAST_MODIFIED_BY_ID,
                IcupadDbContract.Operation.COLUMN_NAME_SYNC});

        tableNameToColumnNames.put( IcupadDbContract.SyncTable.TABLE_NAME, new String[]{
                IcupadDbContract.SyncTable._ID,  IcupadDbContract.SyncTable.COLUMN_NAME_MODIFY_DATE,
                IcupadDbContract.SyncTable.COLUMN_NAME_TABLE_NAME, IcupadDbContract.SyncTable.COLUMN_NAME_INFO_INTERNAL_ID});

        tableNameToColumnNames.put( IcupadDbContract.ConflictTable.TABLE_NAME, new String[]{
                IcupadDbContract.ConflictTable._ID, IcupadDbContract.ConflictTable.COLUMN_NAME_TABLE_NAME,
                IcupadDbContract.ConflictTable.COLUMN_NAME_INFO_INTERNAL_ID, IcupadDbContract.ConflictTable.COLUMN_NAME_CONFLICT_INFO_INTERNAL_ID});

        //**********ECMO*********//
        tableNameToColumnNames.put(IcupadDbContract.EcmoVeinCannula.TABLE_NAME, new String[]{
                IcupadDbContract.EcmoVeinCannula.TABLE_NAME_TYPE,
                IcupadDbContract.EcmoVeinCannula.TABLE_NAME_SIZE, IcupadDbContract.EcmoVeinCannula.TABLE_NAME_DATE,
                IcupadDbContract.EcmoVeinCannula.TABLE_NAME_CONFIGURATION_ID });

        tableNameToColumnNames.put(IcupadDbContract.EcmoUsedStructures.TABLE_NAME, new String[]{
                IcupadDbContract.EcmoUsedStructures.TABLE_NAME_CONFIGURATION_ID,
                IcupadDbContract.EcmoUsedStructures.TABLE_NAME_DATE, IcupadDbContract.EcmoUsedStructures.TABLE_NAME_NAME });

        tableNameToColumnNames.put(IcupadDbContract.EcmoProcedure.TABLE_NAME, new String[]{
                IcupadDbContract.EcmoProcedure.TABLE_NAME_ID, IcupadDbContract.EcmoProcedure.TABLE_NAME_STAY,
                IcupadDbContract.EcmoProcedure.TABLE_NAME_CREATED_BY, IcupadDbContract.EcmoProcedure.TABLE_NAME_END_BY,
                IcupadDbContract.EcmoProcedure.TABLE_NAME_START_CAUSE, IcupadDbContract.EcmoProcedure.TABLE_NAME_START_DATE,
                IcupadDbContract.EcmoProcedure.TABLE_NAME_END_CAUSE, IcupadDbContract.EcmoProcedure.TABLE_NAME_END_DATE });

        tableNameToColumnNames.put(IcupadDbContract.EcmoPatient.TABLE_NAME, new String[]{
                IcupadDbContract.EcmoPatient.TABLE_NAME_ID, IcupadDbContract.EcmoPatient.TABLE_NAME_PROCEDURE_ID,
                IcupadDbContract.EcmoPatient.TABLE_NAME_USER, IcupadDbContract.EcmoPatient.TABLE_NAME_DATE,
                IcupadDbContract.EcmoPatient.TABLE_NAME_SAT, IcupadDbContract.EcmoPatient.TABLE_NAME_ABP,
                IcupadDbContract.EcmoPatient.TABLE_NAME_VCP, IcupadDbContract.EcmoPatient.TABLE_NAME_HR,
                IcupadDbContract.EcmoPatient.TABLE_NAME_TEMP_SURFACE, IcupadDbContract.EcmoPatient.TABLE_NAME_TEMP_CORE,
                IcupadDbContract.EcmoPatient.TABLE_NAME_DIURESIS, IcupadDbContract.EcmoPatient.TABLE_NAME_ULTRAFILTRACTION});

        tableNameToColumnNames.put(IcupadDbContract.EcmoParameter.TABLE_NAME, new String[]{
                IcupadDbContract.EcmoParameter.TABLE_NAME_ID, IcupadDbContract.EcmoParameter.TABLE_NAME_PROCEDURE_ID,
                IcupadDbContract.EcmoParameter.TABLE_NAME_USER, IcupadDbContract.EcmoParameter.TABLE_NAME_DATE,
                IcupadDbContract.EcmoParameter.TABLE_NAME_RPM, IcupadDbContract.EcmoParameter.TABLE_NAME_BLOOD_FLOW,
                IcupadDbContract.EcmoParameter.TABLE_NAME_FIO2, IcupadDbContract.EcmoParameter.TABLE_NAME_GAS_FLOW,
                IcupadDbContract.EcmoParameter.TABLE_NAME_P1, IcupadDbContract.EcmoParameter.TABLE_NAME_P2,
                IcupadDbContract.EcmoParameter.TABLE_NAME_P3, IcupadDbContract.EcmoParameter.TABLE_NAME_DELTA,
                IcupadDbContract.EcmoParameter.TABLE_NAME_HEPARIN_SUPPLY, IcupadDbContract.EcmoParameter.TABLE_NAME_DESCRIPTION});

        tableNameToColumnNames.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME, new String[]{
                IcupadDbContract.EcmoConfiguration.TABLE_NAME_ID, IcupadDbContract.EcmoConfiguration.TABLE_NAME_PROCEDURE_ID,
                IcupadDbContract.EcmoConfiguration.TABLE_NAME_USER, IcupadDbContract.EcmoConfiguration.TABLE_NAME_MODE,
                IcupadDbContract.EcmoConfiguration.TABLE_NAME_CANIULATION_TYPE, IcupadDbContract.EcmoConfiguration.TABLE_NAME_WENTOWANIE_LEFT_VERTICLE,
                IcupadDbContract.EcmoConfiguration.TABLE_NAME_ARTERY_CANNULA_TYPE, IcupadDbContract.EcmoConfiguration.TABLE_NAME_ARTERY_CANNULA_SIZE,
                IcupadDbContract.EcmoConfiguration.TABLE_NAME_OXYGENERATOR_TYPE, IcupadDbContract.EcmoConfiguration.TABLE_NAME_ACTUAL,
                IcupadDbContract.EcmoConfiguration.TABLE_NAME_DATE });

        tableNameToColumnNames.put(IcupadDbContract.EcmoAct.TABLE_NAME, new String[]{
                IcupadDbContract.EcmoAct.TABLE_NAME_ID, IcupadDbContract.EcmoAct.TABLE_NAME_USER,
                IcupadDbContract.EcmoAct.TABLE_NAME_PROCEDURE_ID, IcupadDbContract.EcmoAct.TABLE_NAME_DATE});

        this.tableNameToColumnNames = tableNameToColumnNames;
    }

    public String[] getTableNames() {
        Set<String> set = tableNameToColumnNames.keySet();
        return set.toArray(new String[set.size()]);
    }

    public Map<String, String[]> getTableNameToColumnNames() {
        return tableNameToColumnNames;
    }
}
