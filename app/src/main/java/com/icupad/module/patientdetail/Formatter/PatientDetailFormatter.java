package com.icupad.module.patientdetail.Formatter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PatientDetailFormatter {
    public static final int MARGIN = 50;
    public static final String TEXT_SPACE = "   ";

    public static LinearLayout getStyledStayLinearLayout(Context context) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(0, 0, MARGIN, 0); //substitute parameters for left, top, right, bottom
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setLayoutParams(params);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        return linearLayout;
    }

    public static TextView getStyledStayTextView(Context context, SpannableString spannableString) {
        TextView textView = new TextView(context);
        textView.setTextSize(24);
        textView.setTextColor(Color.parseColor("#6D6D6D"));
        textView.setText(spannableString);
        return textView;
    }

    public static Button getStyledAddOperationButton(Context context, String text) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(MARGIN/2, 0, 0, 0); //substitute parameters for left, top, right, bottom

        Button button = new Button(context);
        button.setText(text);
        button.setLayoutParams(params);
        button.setTextColor(Color.BLACK);

        return button;
    }

    public static LinearLayout getStyledOperationLinearLayout(Context context) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(MARGIN, 0, MARGIN, 0); //substitute parameters for left, top, right, bottom
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setLayoutParams(params);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        return linearLayout;
    }

    public static TextView getStyledOperationTextView(Context context, SpannableString spannableString) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(0, 0, 0, 0); //substitute parameters for left, top, right, bottom

        TextView textView = new TextView(context);
        textView.setTextSize(24);
        textView.setTextColor(Color.parseColor("#6D6D6D"));
        textView.setText(spannableString);
        textView.setLayoutParams(params);

        return textView;
    }

    public static Button getStyledEditOperationButton(Context context, String text) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(MARGIN/2, 0, 0, 0); //substitute parameters for left, top, right, bottom

        Button button = new Button(context);
        button.setText(text);
        button.setLayoutParams(params);
        button.setTextColor(Color.BLACK);

        return button;
    }

    public static TextView getStyledOperationDescriptionTextView(Context context, SpannableString spannableString) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(MARGIN, 0, MARGIN, 0); //substitute parameters for left, top, right, bottom

        TextView textView = new TextView(context);
        textView.setTextSize(24);
        textView.setTextColor(Color.parseColor("#6D6D6D"));
        textView.setText(spannableString);
        textView.setLayoutParams(params);

        return textView;
    }
}
