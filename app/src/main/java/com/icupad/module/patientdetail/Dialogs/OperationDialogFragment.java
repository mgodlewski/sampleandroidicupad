package com.icupad.module.patientdetail.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.icupad.R;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.repository.builder.OperationBuilder;
import com.icupad.module.patientdetail.Dialogs.model.OperationDialogModel;
import com.icupad.module.patientdetail.PatientDetailFragment;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.Operation;

import java.util.Calendar;

public class OperationDialogFragment extends DialogFragment {
    public static String OPERATION_DIALOG_MODEL = "OPERATION_DIALOG_MODEL";
    public static String OPERATION_ID_ARG = "OPERATION_ID_ARG";
    public static String DATETIME_ARG = "DATETIME_ARG";
    public static String DESCRIPTION_ARG = "DESCRIPTION_ARG";
    public static String FRAGMENT_LISTENER = "FRAGMENT_LISTENER";

    private OperationDialogModel model;
    private PublicIcupadRepository publicIcupadRepository;
    private DatePicker datePicker;
    private TimePicker timePicker;
    private EditText descriptionEditText;
    private View layout;

    public static OperationDialogFragment newInstance(OperationDialogModel operationDialogModel) {
        Bundle args = new Bundle();
        args.putSerializable(OPERATION_DIALOG_MODEL, operationDialogModel);

        OperationDialogFragment operationDialogFragment = new OperationDialogFragment();
        operationDialogFragment.setArguments(args);
        return operationDialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        getArgumentsAndFindViews();
        ((TimePicker)layout.findViewById(R.id.timePicker)).setIs24HourView(true);

        updateStrings(layout);

        Resources resources = getActivity().getApplicationContext().getResources();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(resources.getString(R.string.operation_dialog_title)).setView(layout)
                .setNegativeButton(resources.getString(R.string.operation_dialog_remove_button), new OnRemoveClickListener())
                .setNeutralButton(resources.getString(R.string.operation_dialog_cancel_button), new OnCancelClickListener())
                .setPositiveButton(resources.getString(R.string.operation_dialog_save_button), new OnSaveClickListener());

        return builder.create();
    }

    private void getArgumentsAndFindViews() {
        model = (OperationDialogModel)getArguments().getSerializable(OPERATION_DIALOG_MODEL);
        publicIcupadRepository = model.getFragmentListener().getRepository();

        LayoutInflater inflater = getActivity().getLayoutInflater();
        layout = inflater.inflate(R.layout.dialog_edit_operation, null);

        descriptionEditText = (EditText) layout.findViewById(R.id.operationDescriptionEditText);
        datePicker = ((DatePicker)layout.findViewById(R.id.datePicker));
        timePicker = ((TimePicker)layout.findViewById(R.id.timePicker));
    }

    private class OnRemoveClickListener implements DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int id) {
            if(model.getDateTime() != null) {
                publicIcupadRepository.removeOperation(model.getOperationInternalId());
            }
            ((PatientDetailFragment)getTargetFragment()). new LoadingPatientsDetailsThread().start();
        }
    }

    private class OnCancelClickListener implements DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int id) {
        }
    }

    private class OnSaveClickListener implements DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int id) {
            String operatingDate = String.format("%d-%02d-%02d %02d:%02d:00",
                    datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(),
                    timePicker.getCurrentHour(), timePicker.getCurrentMinute());
            Operation operation = OperationBuilder.anOperation()
                    .withOperatingDate(operatingDate)
                    .withDescription(descriptionEditText.getText().toString())
                    .withArchived(false)
                    .withInternalId(model.getOperationInternalId())
                    .build();
            if(model.getDateTime() == null) {//there was no operation before, therefore this is creating new one
                publicIcupadRepository.addOperation(operation, model.getStayId());
            }
            else {
                publicIcupadRepository.updateOperation(operation);
            }
            ((PatientDetailFragment)getTargetFragment()). new LoadingPatientsDetailsThread().start();
        }
    }

    private void updateStrings(View layout) {
        try {
            DatePicker datePicker = (DatePicker)layout.findViewById(R.id.datePicker);
            TimePicker timePicker = (TimePicker)layout.findViewById(R.id.timePicker);
            EditText descriptionEditText = (EditText)layout.findViewById(R.id.operationDescriptionEditText);

            timePicker.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
            descriptionEditText.setText(model.getDescription());

            if(model.getDateTime() != null) {
                String[] date = model.getDateTime().split(" ")[0].split("-");
                datePicker.updateDate(
                        Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2]));

                String[] time = model.getDateTime().split(" ")[1].split(":");
                timePicker.setCurrentHour(Integer.parseInt(time[0]));
                timePicker.setCurrentMinute(Integer.parseInt(time[1]));
            }


        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }
}
