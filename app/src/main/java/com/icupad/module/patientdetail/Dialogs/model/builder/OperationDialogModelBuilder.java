package com.icupad.module.patientdetail.Dialogs.model.builder;

import com.icupad.commons.activitycommunication.FragmentListener;
import com.icupad.module.patientdetail.Dialogs.model.OperationDialogModel;

public final class OperationDialogModelBuilder {
    private FragmentListener fragmentListener;
    private long stayId = 0;
    private long operationId = 0;
    private String dateTime;
    private String description;

    private OperationDialogModelBuilder() {
    }

    public static OperationDialogModelBuilder anOperationDialogModel() {
        return new OperationDialogModelBuilder();
    }

    public OperationDialogModelBuilder withFragmentListener(FragmentListener fragmentListener) {
        this.fragmentListener = fragmentListener;
        return this;
    }

    public OperationDialogModelBuilder withStayId(long stayId) {
        this.stayId = stayId;
        return this;
    }

    public OperationDialogModelBuilder withOperationInternalId(long operationId) {
        this.operationId = operationId;
        return this;
    }

    public OperationDialogModelBuilder withDateTime(String dateTime) {
        this.dateTime = dateTime;
        return this;
    }

    public OperationDialogModelBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public OperationDialogModel build() {
        OperationDialogModel operationDialogModel = new OperationDialogModel();
        operationDialogModel.setFragmentListener(fragmentListener);
        operationDialogModel.setStayId(stayId);
        operationDialogModel.setOperationInternalId(operationId);
        operationDialogModel.setDateTime(dateTime);
        operationDialogModel.setDescription(description);
        return operationDialogModel;
    }
}
