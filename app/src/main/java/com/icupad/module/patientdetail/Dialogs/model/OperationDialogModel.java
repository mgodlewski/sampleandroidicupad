package com.icupad.module.patientdetail.Dialogs.model;

import com.icupad.commons.activitycommunication.FragmentListener;

import java.io.Serializable;

public class OperationDialogModel implements Serializable{
    private FragmentListener fragmentListener;
    private long stayId;
    private long operationInternalId;
    private String dateTime;
    private String description;

    public FragmentListener getFragmentListener() {
        return fragmentListener;
    }

    public void setFragmentListener(FragmentListener fragmentListener) {
        this.fragmentListener = fragmentListener;
    }

    public long getStayId() {
        return stayId;
    }

    public void setStayId(long stayId) {
        this.stayId = stayId;
    }

    public long getOperationInternalId() {
        return operationInternalId;
    }

    public void setOperationInternalId(long operationInternalId) {
        this.operationInternalId = operationInternalId;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
