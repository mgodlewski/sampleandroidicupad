package com.icupad.module.patientdetail.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.google.common.base.Strings;
import com.icupad.R;
import com.icupad.commons.repository.builder.PatientDetailBuilder;
import com.icupad.commons.repository.model.PatientDetail;
import com.icupad.module.patientdetail.PatientDetailFragment;
import com.icupad.commons.activitycommunication.FragmentListener;
import com.icupad.commons.repository.PublicIcupadRepository;

public class PatientDialogFragment extends DialogFragment {
    public static String PATIENT_ID = "PATIENT_ID";
    public static String PATIENT_DETAIL = "PATIENT_DETAIL";
    public static String FRAGMENT_LISTENER = "FRAGMENT_LISTENER";

    public static PatientDialogFragment newInstance(PatientDetail patientDetail, Long patientId, FragmentListener fragmentListener) {
        PatientDialogFragment patientDialogFragment = new PatientDialogFragment();

        Bundle args = new Bundle();
        args.putLong(PATIENT_ID, patientId);
        args.putSerializable(PATIENT_DETAIL, patientDetail);
        args.putSerializable(FRAGMENT_LISTENER, fragmentListener);
        patientDialogFragment.setArguments(args);

        return patientDialogFragment;
    }

    private PublicIcupadRepository publicIcupadRepository;

    private Long patientId;
    private EditText bloodTypeEditText;
    private EditText allergiesEditText;
    private EditText otherInformationsEditText;
    private EditText heightEditText;
    private EditText weightEditText;
    private View layout;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        getStringsAndFindAndInitiateViews();

        Resources resources = getActivity().getApplicationContext().getResources();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(resources.getString(R.string.patient_dialog_title)).setView(layout)
                .setNegativeButton(resources.getString(R.string.patient_dialog_cancel_button), new OnCancelClickListener())
                .setPositiveButton(resources.getString(R.string.patient_dialog_save_button), new OnSaveClickListener());
        return builder.create();
    }

    private void getStringsAndFindAndInitiateViews() {
        PatientDetail patientDetail = (PatientDetail) getArguments().getSerializable(PATIENT_DETAIL);
        FragmentListener fragmentListener = (FragmentListener) getArguments().getSerializable(FRAGMENT_LISTENER);

        patientId = getArguments().getLong(PATIENT_ID);
        publicIcupadRepository = fragmentListener.getRepository();

        LayoutInflater inflater = getActivity().getLayoutInflater();
        layout = inflater.inflate(R.layout.dialog_edit_patient, null);

        heightEditText = (EditText) layout.findViewById(R.id.patientDialogHeight);
        weightEditText = (EditText) layout.findViewById(R.id.patientDialogWeight);
        bloodTypeEditText = (EditText) layout.findViewById(R.id.patientDialogBloodType);
        allergiesEditText = (EditText) layout.findViewById(R.id.patientDialogAllergies);
        otherInformationsEditText = (EditText) layout.findViewById(R.id.patientDialogOtherInformationsTextView);


        heightEditText.setText(Strings.nullToEmpty(patientDetail.getHeight()));
        weightEditText.setText(Strings.nullToEmpty(patientDetail.getWeight()));
        bloodTypeEditText.setText(Strings.nullToEmpty(patientDetail.getBloodType()));
        allergiesEditText.setText(Strings.nullToEmpty(patientDetail.getAllergies()));
        otherInformationsEditText.setText(Strings.nullToEmpty(patientDetail.getOtherImportantInformations()));
    }

    private class OnCancelClickListener implements DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int id) {
        }
    }

    private class OnSaveClickListener implements DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int id) {
            PatientDetail patientDetail = PatientDetailBuilder.aPatientDetail()
                    .withHeight(heightEditText.getText().toString())
                    .withWeight(weightEditText.getText().toString())
                    .withBloodType(bloodTypeEditText.getText().toString())
                    .withAllergies(allergiesEditText.getText().toString())
                    .withOtherImportantInformations(otherInformationsEditText.getText().toString())
                    .build();

            publicIcupadRepository.updatePatient(patientId, patientDetail);
            ((PatientDetailFragment)getTargetFragment()).reloadPatientDetailsInformation();
        }
    }
}
