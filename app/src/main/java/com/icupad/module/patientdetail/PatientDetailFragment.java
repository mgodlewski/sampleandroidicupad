package com.icupad.module.patientdetail;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.icupad.R;
import com.icupad.commons.repository.builder.PatientDetailBuilder;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.commons.utils.SexResolver;
import com.icupad.module.patientchooser.PatientChooserFragment;
import com.icupad.module.patientdetail.Dialogs.PatientDialogFragment;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.ModuleFragment;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.Operation;
import com.icupad.commons.repository.model.PatientDetail;
import com.icupad.commons.repository.model.Stay;
import com.icupad.module.patientdetail.Formatter.LinearElementCreator;

import java.util.List;

public class PatientDetailFragment extends ModuleFragment{
    static public final int DIALOG_FRAGMENT = 1;

    private Context context;
    private PublicIcupadRepository publicIcupadRepository;
    private LinearElementCreator linearElementCreator;

    private Button changeOtherInformationsButton;
    private LinearLayout staysLinearLayout;

    private String noDataString;

    private Long patientId;
    private PatientDetail patientDetail;

    private TextView surnameTextView;
    private TextView nameTextView;
    private TextView sexTextView;
    private TextView birthdayTextView;
    private TextView peselTextView;

    private TextView heightTextView;
    private TextView weightTextView;
    private TextView bloodTypeTextView;
    private TextView allergiesTextView;
    private TextView otherInformationsTextView;

    private final Handler handler = new Handler();

    public PatientDetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getActivity().getApplicationContext();
        publicIcupadRepository = fragmentListener.getRepository();
        linearElementCreator = new LinearElementCreator(context, this, fragmentListener);
        patientId = PatientChooserFragment.getLastChosenPatientId();

        View view = inflater.inflate(R.layout.fragment_patient_detail, container, false);
        findAllViews(view);
        changeOtherInformationsButton.setOnClickListener(new ChangeInformationsButtonClickListener());

        new LoadingPatientsDetailsThread().start();

        return view;
    }

    public void reloadPatientDetailsInformation(){
        new LoadingPatientsDetailsThread().start();
    }

    public class LoadingPatientsDetailsThread extends Thread {
        @Override
        public void run(){
            patientDetail = publicIcupadRepository.getPatientDetail(patientId);
            final List<Stay> stays = publicIcupadRepository.getPatientStays(patientId);
            handler.post(new Runnable(){
                @Override
                public void run(){
                    setPatientTextInformations();
                    createListOfStays(stays);
                    fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
                }
            });
        }
    }

    private void findAllViews(View view) {
        changeOtherInformationsButton = (Button) view.findViewById(R.id.patientDetailChangeOtherInformationsButton);
        staysLinearLayout = ((LinearLayout)view.findViewById(R.id.patientDetailStaysLinearLayout));

        surnameTextView = (TextView) view.findViewById(R.id.patientDetailSurname);
        nameTextView = (TextView) view.findViewById(R.id.patientDetailName);
        sexTextView = (TextView) view.findViewById(R.id.patientDetailSex);
        birthdayTextView = (TextView) view.findViewById(R.id.patientDetailBirthDay);
        peselTextView = (TextView) view.findViewById(R.id.patientDetailPesel);

        heightTextView = (TextView) view.findViewById(R.id.patientDetailHeight);
        weightTextView = (TextView) view.findViewById(R.id.patientDetailWeight);
        bloodTypeTextView = (TextView) view.findViewById(R.id.patientDetailBloodType);
        allergiesTextView = (TextView) view.findViewById(R.id.patientDetailAllergies);
        otherInformationsTextView = (TextView) view.findViewById(R.id.patientDetailOtherInformationsTextView);
    }

    private void setPatientTextInformations() {
        noDataString = getResources().getString(R.string.patient_detail_no_data);

        surnameTextView.setText(patientDetail.getSurname());
        nameTextView.setText(patientDetail.getName());
        sexTextView.setText(SexResolver.getCorrectSexString(context, patientDetail.getSex()));
        birthdayTextView.setText(DateTimeFormatterHelper.getDateFromString(patientDetail.getBirthday()));
        peselTextView.setText(patientDetail.getPesel());

        heightTextView.setText(patientDetail.getHeight());
        heightTextView.setHint(noDataString);

        weightTextView.setText(patientDetail.getWeight());
        weightTextView.setHint(noDataString);

        bloodTypeTextView.setText(patientDetail.getBloodType());
        bloodTypeTextView.setHint(noDataString);

        allergiesTextView.setText(patientDetail.getAllergies());
        allergiesTextView.setHint(noDataString);

        otherInformationsTextView.setText(patientDetail.getOtherImportantInformations());
        otherInformationsTextView.setHint(noDataString);
    }

    public void createListOfStays(List<Stay> stays) {
        staysLinearLayout.removeAllViews();
        for(final Stay stay : stays) {
            LinearLayout stayLinearLayout = linearElementCreator.getStayLinearElement(stay);
            staysLinearLayout.addView(stayLinearLayout);

            appendListOfOperations(stay);
        }
    }

    private void appendListOfOperations(Stay stay) {
        for(final Operation operation : stay.getOperations()) {
            LinearLayout linearLayout = linearElementCreator.getOperationLinearElement();

            LinearLayout operationLinearLayout = linearElementCreator.getOperationDateLinearElement(stay, operation);
            linearLayout.addView(operationLinearLayout);

            View description = linearElementCreator.getOperationDescriptionLinearElement(operation);
            linearLayout.addView(description);

            linearLayout.addView(linearElementCreator.getHorizontalLine(),new ViewGroup.LayoutParams( ViewGroup.LayoutParams.FILL_PARENT, 2));

            staysLinearLayout.addView(linearLayout);
        }
    }

    private class ChangeInformationsButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            PatientDetail patientDetail = PatientDetailBuilder.aPatientDetail()
                    .withHeight(heightTextView.getText().toString())
                    .withWeight(weightTextView.getText().toString())
                    .withBloodType(bloodTypeTextView.getText().toString())
                    .withAllergies(allergiesTextView.getText().toString())
                    .withOtherImportantInformations(otherInformationsTextView.getText().toString())
                    .build();
            PatientDialogFragment patientDialogFragment = PatientDialogFragment.newInstance(
                    patientDetail, Long.valueOf(patientId), fragmentListener);
            patientDialogFragment.setTargetFragment(PatientDetailFragment.this, DIALOG_FRAGMENT);
            patientDialogFragment.show(getFragmentManager(), "");
        }
    }
}
