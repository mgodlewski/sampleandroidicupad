package com.icupad.module.patientdetail.Formatter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.icupad.R;
import com.icupad.commons.activitycommunication.FragmentListener;
import com.icupad.commons.repository.model.Operation;
import com.icupad.commons.repository.model.Stay;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.module.patientdetail.Dialogs.OperationDialogFragment;
import com.icupad.module.patientdetail.Dialogs.model.OperationDialogModel;
import com.icupad.module.patientdetail.Dialogs.model.builder.OperationDialogModelBuilder;
import com.icupad.module.patientdetail.PatientDetailFragment;

import static com.icupad.module.patientdetail.Formatter.PatientDetailFormatter.MARGIN;
import static com.icupad.module.patientdetail.Formatter.PatientDetailFormatter.TEXT_SPACE;
import static com.icupad.module.patientdetail.Formatter.PatientDetailFormatter.getStyledAddOperationButton;
import static com.icupad.module.patientdetail.Formatter.PatientDetailFormatter.getStyledEditOperationButton;
import static com.icupad.module.patientdetail.Formatter.PatientDetailFormatter.getStyledOperationDescriptionTextView;
import static com.icupad.module.patientdetail.Formatter.PatientDetailFormatter.getStyledOperationLinearLayout;
import static com.icupad.module.patientdetail.Formatter.PatientDetailFormatter.getStyledOperationTextView;
import static com.icupad.module.patientdetail.Formatter.PatientDetailFormatter.getStyledStayLinearLayout;
import static com.icupad.module.patientdetail.Formatter.PatientDetailFormatter.getStyledStayTextView;

public class LinearElementCreator {
    private Context context;
    private Fragment fragment;
    private FragmentListener fragmentListener;
    private FragmentManager fragmentManager;

    private String operationAddButtonText;
    private String stayText;
    private String operationText;
    private String operationEditButtonText;
    private String operationDescriptionText;

    public LinearElementCreator(Context context, Fragment fragment, FragmentListener fragmentListener) {
        this.context = context;
        this.fragment = fragment;
        this.fragmentListener = fragmentListener;
        this.fragmentManager = fragment.getFragmentManager();

        loadAllTexts(context);
    }

    private void loadAllTexts(Context context) {
        operationAddButtonText = context.getResources().getString(R.string.patient_detail_operation_add_button);
        stayText = context.getResources().getString(R.string.patient_detail_stay);
        operationText = context.getResources().getString(R.string.patient_detail_operation);
        operationEditButtonText = context.getResources().getString(R.string.patient_detail_operation_edit_button);
        operationDescriptionText = context.getResources().getString(R.string.patient_detail_operation_description);
    }

    @NonNull
    public LinearLayout getStayLinearElement(Stay stay) {
        LinearLayout stayLinearLayout = getStyledStayLinearLayout(context);

        Spanned spanned = getStayText(stay);
        SpannableString spannableString = new SpannableString(spanned);
        spannableString.setSpan(new ForegroundColorSpan(Color.BLACK), (stayText).length()-1, spannableString.length(), 0);

        TextView textView = getStyledStayTextView(context, spannableString);
        stayLinearLayout.addView(textView);

        Button button = getStyledAddOperationButton(context, operationAddButtonText);
        button.setOnClickListener(new OnNewOperationClickListener(stay));
        stayLinearLayout.addView(button);

        return stayLinearLayout;
    }

    @NonNull
    private Spanned getStayText(Stay stay) {
        return Html.fromHtml(stayText + TEXT_SPACE + "<b>" +
                getDateFromString(stay.getAdmitDate()) + " - " +
                ((stay.getDischargeDate()==null)?"*":getDateFromString(stay.getDischargeDate() + "</b>" )));
    }

    private String getDateFromString(String string) {
        return DateTimeFormatterHelper.getDateFromString(string);
    }

    @NonNull
    public LinearLayout getOperationLinearElement(){
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(MARGIN/2, 0, MARGIN/2, 20); //substitute parameters for left, top, right, bottom
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setLayoutParams(params);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        return linearLayout;
    }

    @NonNull
    public View getHorizontalLine(){
        View ruler = new View(context);
        ruler.setBackgroundColor(Color.BLACK);
        return ruler;
    }

    @NonNull
    public LinearLayout getOperationDateLinearElement(Stay stay, Operation operation) {
        LinearLayout operationLinearLayout = getStyledOperationLinearLayout(context);
//        operationLinearLayout.setPadding(0,0,0,40);

        Spanned spanned = Html.fromHtml(operationText + TEXT_SPACE + "<b>" + DateTimeFormatterHelper.getStringWithoutSecondFromString(operation.getOperatingDate()) + "</b>");
        SpannableString spannableString = new SpannableString(spanned);
        spannableString.setSpan(new ForegroundColorSpan(Color.BLACK), (operationText).length()-1, spannableString.length(), 0);

        TextView textView = getStyledOperationTextView(context,spannableString);
        operationLinearLayout.addView(textView);

        Button button = getStyledEditOperationButton(context, operationEditButtonText);
        button.setOnClickListener(new OnEditOperationClickListener(stay, operation) );
        operationLinearLayout.addView(button);

        return operationLinearLayout;
    }

    public View getOperationDescriptionLinearElement(Operation operation) {
        Spanned spanned = Html.fromHtml(operationDescriptionText + TEXT_SPACE + "<b>" + operation.getDescription() + "</b>");
        SpannableString spannableString = new SpannableString(spanned);
        spannableString.setSpan(new ForegroundColorSpan(Color.BLACK), (operationDescriptionText).length()-1, spannableString.length(), 0);

        return getStyledOperationDescriptionTextView(
                context,spannableString);
    }

    private class OnNewOperationClickListener implements View.OnClickListener {
        private Stay stay;

        public OnNewOperationClickListener(Stay stay) {
            this.stay = stay;
        }

        public void onClick(View v) {
            OperationDialogModel model = OperationDialogModelBuilder.anOperationDialogModel()
                    .withFragmentListener(fragmentListener).withStayId(stay.getId()).build();

            OperationDialogFragment operationDialogFragment =
                    OperationDialogFragment.newInstance(model);
            operationDialogFragment.setTargetFragment(fragment, PatientDetailFragment.DIALOG_FRAGMENT);
            operationDialogFragment.show(fragmentManager, "custom_dialog");
        }
    }

    private class OnEditOperationClickListener implements View.OnClickListener {
        private Stay stay;
        private Operation operation;

        public OnEditOperationClickListener(Stay stay, Operation operation) {
            this.stay = stay;
            this.operation = operation;
        }

        public void onClick(View v) {
            OperationDialogModel model = OperationDialogModelBuilder.anOperationDialogModel()
                    .withFragmentListener(fragmentListener).withStayId(stay.getId())
                    .withOperationInternalId(operation.getInternalId()).withDateTime(operation.getOperatingDate())
                    .withDescription(operation.getDescription()).build();

            OperationDialogFragment operationDialogFragment
                    = OperationDialogFragment.newInstance(model);
            operationDialogFragment.setTargetFragment(fragment, PatientDetailFragment.DIALOG_FRAGMENT);
            operationDialogFragment.show(fragmentManager, "custom_dialog");
        }
    }
}
