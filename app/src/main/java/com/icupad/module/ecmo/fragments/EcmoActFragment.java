package com.icupad.module.ecmo.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.icupad.R;
import com.icupad.bloodgas.populator.HalfDaysComputer;
import com.icupad.commons.ModuleFragment;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.EcmoAct;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.module.ecmo.EcmoDataTimeUtils;
import com.icupad.module.ecmo.InputFilterMinMax;
import com.icupad.module.ecmo.dialogs.DataTimeFragment;
import com.icupad.module.ecmo.dialogs.DialogCloseListener;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.logging.Logger;

/**
 * Created by Marcin on 21.03.2017.
 */
public class EcmoActFragment extends ModuleFragment {

    private Logger LOGGER = Logger.getLogger(this.getClass().toString());

    private PublicIcupadRepository publicIcupadRepository;

    private Button saveButton;
    private Button cancelButton;
    private Button dateTimeInput;

    private EditText actInput;
    private TextView dayTextView;

    private Long procedureId;
    private String dateTime;
    private boolean active;
    private String startDate;

    public EcmoActFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ecmo_act, container, false);

        publicIcupadRepository = fragmentListener.getRepository();

        getAllViewElements(view);
        setInputFilters();
        addClickListeners();
        setNewDataTime(DateTimeFormatterHelper.getCurrentDateTime());

        Bundle args = getArguments();
        procedureId = args.getLong("procadureId");
        active = args.getBoolean("active");
        startDate = args.getString("startDate");

        EcmoDataTimeUtils.setDataTextViews(dayTextView, startDate, publicIcupadRepository);

        fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
        return view;
    }

    private void addClickListeners() {
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Resources resources = getActivity().getApplicationContext().getResources();
                if(actInput.getText().toString().isEmpty()){
                    actInput.setError(resources.getString(R.string.empty_input));
                    return;
                }
                EcmoAct ecmoAct = new EcmoAct();
                ecmoAct.setProcedureId(procedureId);
                ecmoAct.setAct(Double.parseDouble(actInput.getText().toString()));
                ecmoAct.setCreatedBy(1L);   //TODO set real userId
                ecmoAct.setDate(dateTime);
                ecmoAct.setProcedureId(procedureId);

                publicIcupadRepository.saveAct(ecmoAct);

                backToVisualisationView();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backToVisualisationView();
            }
        });
        dateTimeInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                DataTimeFragment dtf = DataTimeFragment.newInstance(dateTime);
                dtf.setDialogCloseListener(new DialogCloseListener(){
                    @Override
                    public void onDialogClose(String... parameters) {
                        setNewDataTime(parameters[0]);
                    }
                });
                dtf.show(ft, "dialog");
            }
        });
    }

    private void backToVisualisationView(){
        EcmoVisualizationFragment ecmoVisualizationFragment = new EcmoVisualizationFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("procadureId",procedureId);
        bundle.putBoolean("active", active);
        bundle.putString("startDate",startDate);
        ecmoVisualizationFragment.setArguments(bundle);

        fragmentListener.replaceFragment(ecmoVisualizationFragment);
    }

    private void getAllViewElements(View view) {
        saveButton = (Button) view.findViewById(R.id.ecmoSaveButton);
        cancelButton = (Button) view.findViewById(R.id.ecmoCancelButton);
        actInput = (EditText) view.findViewById(R.id.actInput);
        dateTimeInput = (Button) view.findViewById(R.id.dateTimeInput);
        dayTextView = (TextView) view.findViewById(R.id.day);
    }

    private void setInputFilters(){
        actInput.setFilters(new InputFilter[]{ new InputFilterMinMax("0","10000")});
    }

    private void setNewDataTime(String dateTime){
        dateTimeInput.setText(dateTime);
        this.dateTime = dateTime;
    }

}
