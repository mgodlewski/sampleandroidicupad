package com.icupad.module.ecmo.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.icupad.R;
import com.icupad.bloodgas.populator.HalfDaysComputer;
import com.icupad.commons.ModuleFragment;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.EcmoParameter;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.module.ecmo.EcmoDataTimeUtils;
import com.icupad.module.ecmo.InputFilterMinMax;
import com.icupad.module.ecmo.dialogs.DataTimeFragment;
import com.icupad.module.ecmo.dialogs.DialogCloseListener;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.logging.Logger;

/**
 * Created by Marcin on 21.03.2017.
 */
public class EcmoParameterFragment extends ModuleFragment {

    private Logger LOGGER = Logger.getLogger(this.getClass().toString());

    private PublicIcupadRepository publicIcupadRepository;

    private Button saveButton;
    private Button cancelButton;

    private EditText rpmInput;
    private EditText bloodFlowInput;
    private EditText fio2Input;
    private EditText gasFlowInput;
    private EditText p1Input;
    private EditText p2Input;
    private EditText p3Input;
    private EditText heparinInput;
    private EditText descriptionInput;
    private Button dateTimeInput;
    private TextView dayTextView;

    private Long procedureId;
    private String dateTime;
    private boolean active;
    private String startDate;

    public EcmoParameterFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ecmo_parameter, container, false);

        publicIcupadRepository = fragmentListener.getRepository();

        getAllViewElements(view);
        setInputFilters();
        addClickListeners();
        setNewDataTime(DateTimeFormatterHelper.getCurrentDateTime());

        Bundle args = getArguments();
        procedureId = args.getLong("procadureId");
        active = args.getBoolean("active");
        startDate = args.getString("startDate");

        EcmoDataTimeUtils.setDataTextViews(dayTextView, startDate, publicIcupadRepository);

        fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
        return view;
    }

    private void addClickListeners() {
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!editTextValidation()){
                    return;
                }

                EcmoParameter ecmoParameter = new EcmoParameter();
                ecmoParameter.setDate(dateTime);
                ecmoParameter.setProcedureId(procedureId);
                ecmoParameter.setCreatedBy(1L);   //TODO set real userId
                ecmoParameter.setBloodFlow(bloodFlowInput.getText().toString().isEmpty() ? null : Double.parseDouble((bloodFlowInput.getText().toString())));
                ecmoParameter.setRpm(rpmInput.getText().toString().isEmpty() ? null : Double.parseDouble((rpmInput.getText().toString())));
                ecmoParameter.setFiO2(fio2Input.getText().toString().isEmpty() ? null : Double.parseDouble((fio2Input.getText().toString())));
                ecmoParameter.setDescription(descriptionInput.getText().toString());
                ecmoParameter.setGasFlow(gasFlowInput.getText().toString().isEmpty() ? null : Double.parseDouble((gasFlowInput.getText().toString())));
                ecmoParameter.setHeparinSuply(heparinInput.getText().toString().isEmpty() ? null : Double.parseDouble((heparinInput.getText().toString())));
                ecmoParameter.setP1(p1Input.getText().toString().isEmpty() ? null : Double.parseDouble((p1Input.getText().toString())));
                ecmoParameter.setP2(p2Input.getText().toString().isEmpty() ? null : Double.parseDouble((p2Input.getText().toString())));
                ecmoParameter.setP3(p3Input.getText().toString().isEmpty() ? null : Double.parseDouble((p3Input.getText().toString())));
                ecmoParameter.setDelta(p2Input.getText().toString().isEmpty() || p3Input.getText().toString().isEmpty() ?
                        null : Double.parseDouble((p2Input.getText().toString())) - Double.parseDouble((p3Input.getText().toString())));

                publicIcupadRepository.saveEcmoParameter(ecmoParameter);

                backToVisualisationView();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backToVisualisationView();
            }
        });
        dateTimeInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDataTimeFragment();
            }
        });
    }

    private boolean editTextValidation(){
        Resources resources = getActivity().getApplicationContext().getResources();
        if(rpmInput.getText().toString().isEmpty() && bloodFlowInput.getText().toString().isEmpty() &&
                fio2Input.getText().toString().isEmpty() && gasFlowInput.getText().toString().isEmpty() &&
                p1Input.getText().toString().isEmpty() && p2Input.getText().toString().isEmpty() &&
                p3Input.getText().toString().isEmpty() && heparinInput.getText().toString().isEmpty() &&
                descriptionInput.getText().toString().isEmpty()){

            rpmInput.setError(resources.getString(R.string.empty_input));
            bloodFlowInput.setError(resources.getString(R.string.empty_input));
            fio2Input.setError(resources.getString(R.string.empty_input));
            gasFlowInput.setError(resources.getString(R.string.empty_input));
            p1Input.setError(resources.getString(R.string.empty_input));
            p2Input.setError(resources.getString(R.string.empty_input));
            p3Input.setError(resources.getString(R.string.empty_input));
            heparinInput.setError(resources.getString(R.string.empty_input));
            descriptionInput.setError(resources.getString(R.string.empty_input));
            return false;
        }
        return true;
    }

    private void showDataTimeFragment(){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        DataTimeFragment dtf = DataTimeFragment.newInstance(dateTime);
        dtf.setDialogCloseListener(new DialogCloseListener(){
            @Override
            public void onDialogClose(String... parameters) {
                setNewDataTime(parameters[0]);
            }
        });
        dtf.show(ft, "dialog");
    }

    private void backToVisualisationView(){
        EcmoVisualizationFragment ecmoVisualizationFragment = new EcmoVisualizationFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("procadureId",procedureId);
        bundle.putBoolean("active", active);
        bundle.putString("startDate",startDate);
        ecmoVisualizationFragment.setArguments(bundle);

        fragmentListener.replaceFragment(ecmoVisualizationFragment);
    }

    private void getAllViewElements(View view) {
        saveButton = (Button) view.findViewById(R.id.ecmoSaveButton);
        cancelButton= (Button) view.findViewById(R.id.ecmoCancelButton);
        dateTimeInput = (Button) view.findViewById(R.id.dateTimeInput);
        rpmInput = (EditText) view.findViewById(R.id.rpmInput);
        bloodFlowInput = (EditText) view.findViewById(R.id.bloodFlowInput);
        fio2Input = (EditText) view.findViewById(R.id.fio2Input);
        gasFlowInput = (EditText) view.findViewById(R.id.gasFlowInput);;
        p1Input = (EditText) view.findViewById(R.id.p1Input);
        p2Input = (EditText) view.findViewById(R.id.p2Input);
        p3Input = (EditText) view.findViewById(R.id.p3Input);
        heparinInput = (EditText) view.findViewById(R.id.heparinInput);
        descriptionInput = (EditText) view.findViewById(R.id.descriptionInput);
        dayTextView = (TextView) view.findViewById(R.id.day);
    }

    private void setInputFilters(){
        rpmInput.setFilters(new InputFilter[]{ new InputFilterMinMax("0","1000")});
        bloodFlowInput.setFilters(new InputFilter[]{ new InputFilterMinMax("0","10")});
        gasFlowInput.setFilters(new InputFilter[]{ new InputFilterMinMax("0","10")});
        p1Input.setFilters(new InputFilter[]{ new InputFilterMinMax("-300","300")});
        p2Input.setFilters(new InputFilter[]{ new InputFilterMinMax("0","500")});
        p3Input.setFilters(new InputFilter[]{ new InputFilterMinMax("0","500")});
    }

    private void setNewDataTime(String dateTime){
        dateTimeInput.setText(dateTime);
        this.dateTime = dateTime;
    }
}
