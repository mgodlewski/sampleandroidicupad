package com.icupad.module.ecmo.charts;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.icupad.commons.utils.DateTimeFormatterHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Marcin on 23.04.2017.
 */
public class DateXAxisFormatter implements IAxisValueFormatter {
    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return DateTimeFormatterHelper.getStringWithoutSecondFromMillis((long)value);
    }

}