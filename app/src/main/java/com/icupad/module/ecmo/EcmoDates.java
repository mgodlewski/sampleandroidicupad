package com.icupad.module.ecmo;

/**
 * Created by Marcin on 19.03.2017.
 */
public class EcmoDates {
    private String startDate;
    private String endDate;
    private Long procedureId;

    public EcmoDates(String startDate, String endDate, Long procedureId) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.procedureId = procedureId;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public Long getProcedureId() {
        return procedureId;
    }
}
