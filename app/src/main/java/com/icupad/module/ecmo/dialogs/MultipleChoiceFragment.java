package com.icupad.module.ecmo.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;

import com.icupad.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Marcin on 20.03.2017.
 */
public class MultipleChoiceFragment extends DialogFragment {

    private String title;
    private String[] elements;
    private DialogCloseListener dialogCloseListener;
    private List<String> selectedItems;

    public static MultipleChoiceFragment newInstance(String title, String[] elements, String selectedItems){
        MultipleChoiceFragment scf = new MultipleChoiceFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putStringArray("elements", elements);
        args.putString("selectedItems", selectedItems);
        scf.setArguments(args);
        return scf;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        title = getArguments().getString("title");
        elements = getArguments().getStringArray("elements");
        selectedItems = new ArrayList<>(Arrays.asList(getArguments().getString("selectedItems").split(", ")));

        Resources resources = getActivity().getApplicationContext().getResources();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        boolean[] checked = new boolean[elements.length];
        for (int i =0; i < elements.length; i ++) {
            checked[i] = selectedItems.contains(elements[i]);
        }
        builder.setTitle(title)
                .setMultiChoiceItems(elements, checked, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        if (isChecked) {
                            selectedItems.add(elements[which]);
                        } else if (selectedItems.contains(elements[which])) {
                            selectedItems.remove(elements[which]);
                        }
                    }
                })
                .setPositiveButton(resources.getString(R.string.multiple_choice_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if(dialogCloseListener!=null) {
                            String[] array = new String[selectedItems.size()];
                            dialogCloseListener.onDialogClose(selectedItems.toArray(array));
                        }
                    }
                })
                .setNegativeButton(resources.getString(R.string.multiple_choice_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        return builder.create();
    }

    public void setDialogCloseListener(DialogCloseListener dialogCloseListener){
        this.dialogCloseListener = dialogCloseListener;
    }
}
