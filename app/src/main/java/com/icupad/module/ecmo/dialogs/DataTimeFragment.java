package com.icupad.module.ecmo.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.icupad.R;

import java.util.logging.Logger;

/**
 * Created by Marcin on 20.03.2017.
 */
public class DataTimeFragment extends DialogFragment {

    private Logger LOGGER = Logger.getLogger(this.getClass().toString());

    private DatePicker datePicker;
    private TimePicker timePicker;
    private String dateTime;
    private DialogCloseListener dialogCloseListener;

    public static DataTimeFragment newInstance(String dateTime){
        DataTimeFragment dtf = new DataTimeFragment();
        Bundle args = new Bundle();
        args.putString("dateTime", dateTime);
        dtf.setArguments(args);
        return dtf;
    }

    public void setDialogCloseListener(DialogCloseListener dialogCloseListener){
        this.dialogCloseListener = dialogCloseListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layout = inflater.inflate(R.layout.dialog_data_time, null);

        dateTime = getArguments().getString("dateTime");

        timePicker = (TimePicker)layout.findViewById(R.id.timePicker);
        datePicker = (DatePicker)layout.findViewById(R.id.datePicker);

        timePicker.setIs24HourView(true);

        Resources resources = getActivity().getApplicationContext().getResources();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(layout)
                .setNegativeButton(resources.getString(R.string.save_button), new OnSaveClickListener())
                .setPositiveButton(resources.getString(R.string.cancel_button), new OnRemoveClickListener());

        setCurrentDataTime();
        return builder.create();
    }

    private class OnRemoveClickListener implements DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int id) {
        }
    }

    private class OnSaveClickListener implements DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int id) {
            dateTime = String.format("%d-%02d-%02d %02d:%02d:00",
                    datePicker.getYear(), (datePicker.getMonth()+1), datePicker.getDayOfMonth(),
                    timePicker.getCurrentHour(), timePicker.getCurrentMinute());
            if(dialogCloseListener!=null) {
                dialogCloseListener.onDialogClose(dateTime);
            }
        }
    }

    private void setCurrentDataTime(){
        String[] dataTimeTab = dateTime.split(" ");
        String[] data = dataTimeTab[0].split("-");
        String[] time = dataTimeTab[1].split(":");

        timePicker.setCurrentMinute(Integer.parseInt(time[1]));
        timePicker.setCurrentHour(Integer.parseInt(time[0]));
        datePicker.updateDate(Integer.parseInt(data[0]),Integer.parseInt(data[1])-1,Integer.parseInt(data[2]));
    }
}
