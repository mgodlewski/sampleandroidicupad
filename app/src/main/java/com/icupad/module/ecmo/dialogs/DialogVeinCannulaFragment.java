package com.icupad.module.ecmo.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.icupad.R;

import java.util.logging.Logger;

/**
 * Created by Marcin on 21.03.2017.
 */
public class DialogVeinCannulaFragment extends DialogFragment {

    private Logger LOGGER = Logger.getLogger(this.getClass().toString());

    private EditText veinCannulaType1;
    private EditText veinCannulaSize1;
    private EditText veinCannulaType2;
    private EditText veinCannulaSize2;
    private EditText veinCannulaType3;
    private EditText veinCannulaSize3;
    private DialogCloseListener dialogCloseListener;
    private String veinCannulas;

    public static DialogVeinCannulaFragment newInstance(String veinVannulas) {
        DialogVeinCannulaFragment dvc = new DialogVeinCannulaFragment();
        Bundle args = new Bundle();
        args.putString("veinCannulas", veinVannulas);
        dvc.setArguments(args);
        return dvc;
    }

    public void setDialogCloseListener(DialogCloseListener dialogCloseListener) {
        this.dialogCloseListener = dialogCloseListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Resources resources = getActivity().getApplicationContext().getResources();
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layout = inflater.inflate(R.layout.dialog_vein_cannula, null);


        veinCannulaType1 = (EditText)layout.findViewById(R.id.veinCannulaType1);
        veinCannulaSize1 = (EditText)layout.findViewById(R.id.veinCannulaSize1);
        veinCannulaType2 = (EditText)layout.findViewById(R.id.veinCannulaType2);
        veinCannulaSize2 = (EditText)layout.findViewById(R.id.veinCannulaSize2);
        veinCannulaType3 = (EditText)layout.findViewById(R.id.veinCannulaType3);
        veinCannulaSize3 = (EditText)layout.findViewById(R.id.veinCannulaSize3);

        veinCannulas = getArguments().getString("veinCannulas");

        if (!veinCannulas.isEmpty()) {
            int i=1;
            for (String s : veinCannulas.split("; ")) {
                String[] cannula = s.split(":");
                switch (i) {
                    case 1:
                        veinCannulaType1.setText(cannula[0]);
                        veinCannulaSize1.setText(cannula[1]);
                        break;
                    case 2:
                        veinCannulaType2.setText(cannula[0]);
                        veinCannulaSize2.setText(cannula[1]);
                        break;
                    case 3:
                        veinCannulaType3.setText(cannula[0]);
                        veinCannulaSize3.setText(cannula[1]);
                        break;
                }
                i++;
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(layout)
                .setPositiveButton(resources.getString(R.string.save_button), null)
                .setNegativeButton(resources.getString(R.string.cancel_button), null);

        AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(!editTextValidation()){
                            return;
                        }
                        String veinVannulas1 = veinCannulaType1.getText().toString() + ":" + veinCannulaSize1.getText().toString();
                        String veinVannulas2 = veinCannulaType2.getText().toString() + ":" + veinCannulaSize2.getText().toString();
                        String veinVannulas3 = veinCannulaType3.getText().toString() + ":" + veinCannulaSize3.getText().toString();
                        if(dialogCloseListener!=null) {
                            dialogCloseListener.onDialogClose(veinVannulas1, veinVannulas2, veinVannulas3);
                        }
                        dialog.dismiss();
                    }
                });
            }
        });

        return dialog;
    }

    private boolean editTextValidation() {
        Resources resources = getActivity().getApplicationContext().getResources();
        if (veinCannulaType1.getText().toString().isEmpty() && !veinCannulaSize1.getText().toString().isEmpty()) {
            veinCannulaType1.setError(resources.getString(R.string.empty_input));
            return false;
        }
        if (!veinCannulaType1.getText().toString().isEmpty() && veinCannulaSize1.getText().toString().isEmpty()) {
            veinCannulaSize1.setError(resources.getString(R.string.empty_input));
            return false;
        }
        if (veinCannulaType2.getText().toString().isEmpty() && !veinCannulaSize2.getText().toString().isEmpty()) {
            veinCannulaType2.setError(resources.getString(R.string.empty_input));
            return false;
        }
        if (!veinCannulaType2.getText().toString().isEmpty() && veinCannulaSize2.getText().toString().isEmpty()) {
            veinCannulaSize2.setError(resources.getString(R.string.empty_input));
            return false;
        }
        if (veinCannulaType3.getText().toString().isEmpty() && !veinCannulaSize3.getText().toString().isEmpty()) {
            veinCannulaType3.setError(resources.getString(R.string.empty_input));
            return false;
        }
        if (!veinCannulaType3.getText().toString().isEmpty() && veinCannulaSize3.getText().toString().isEmpty()) {
            veinCannulaSize3.setError(resources.getString(R.string.empty_input));
            return false;
        }
        return true;
    }
}
