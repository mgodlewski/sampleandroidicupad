package com.icupad.module.ecmo.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.icupad.R;
import com.icupad.commons.utils.DateTimeFormatterHelper;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.logging.Logger;

/**
 * Created by Marcin on 23.03.2017.
 */
public class DialogEndProcedureFragment extends DialogFragment {

    private Logger LOGGER = Logger.getLogger(this.getClass().toString());

    private DialogCloseListener dialogCloseListener;

    private Spinner causeOfEndInput;
    private Button dateOfEndInput;

    private String dateTime;

    public static DialogEndProcedureFragment newInstance(){
        DialogEndProcedureFragment denp = new DialogEndProcedureFragment();
        return denp;
    }

    public void setDialogCloseListener(DialogCloseListener dialogCloseListener) {
        this.dialogCloseListener = dialogCloseListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Resources resources = getActivity().getApplicationContext().getResources();
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layout = inflater.inflate(R.layout.dialog_end_ecmo_procedure, null);

        causeOfEndInput =(Spinner) layout.findViewById(R.id.causeOfEndInput);
        dateOfEndInput =(Button) layout.findViewById(R.id.dateTimeInput);

        ArrayAdapter<CharSequence> causeOfEndAdapter = ArrayAdapter.createFromResource(this.getActivity(),R.array.ecmo_end_causes, R.layout.spinner_text_view);
        causeOfEndAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        causeOfEndInput.setAdapter(causeOfEndAdapter);

        setNewDataTime(DateTimeFormatterHelper.getCurrentDateTime());

        dateOfEndInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                DataTimeFragment dtf = DataTimeFragment.newInstance(dateTime);
                dtf.setDialogCloseListener(new DialogCloseListener(){
                    @Override
                    public void onDialogClose(String... parameters) {
                        setNewDataTime(parameters[0]);
                    }
                });
                dtf.show(ft, "dialog");
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(layout)
                .setNegativeButton(resources.getString(R.string.save_button), new OnSaveClickListener())
                .setPositiveButton(resources.getString(R.string.cancel_button), new OnRemoveClickListener());

        return builder.create();
    }

    private class OnRemoveClickListener implements DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int id) {
        }
    }

    private class OnSaveClickListener implements DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int id) {
            String[] paramters = new String[2];
            paramters[0] = causeOfEndInput.getSelectedItem().toString();
            paramters[1] = dateTime;
            if(dialogCloseListener!=null) {
                dialogCloseListener.onDialogClose(paramters);
            }
        }
    }

    private void setNewDataTime(String dateTime){
        dateOfEndInput.setText(dateTime);
        this.dateTime = dateTime;
    }
}
