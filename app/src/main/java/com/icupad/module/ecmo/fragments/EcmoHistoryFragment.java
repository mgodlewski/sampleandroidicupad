package com.icupad.module.ecmo.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.icupad.R;
import com.icupad.commons.ModuleFragment;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.module.ecmo.EcmoDates;
import com.icupad.module.ecmo.adapters.EcmoHistoryAdapter;
import com.icupad.module.patientchooser.PatientChooserFragment;

import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Marcin on 19.03.2017.
 */
public class EcmoHistoryFragment extends ModuleFragment {

    private Logger LOGGER = Logger.getLogger(this.getClass().toString());

    final Handler handler = new Handler();

    private TextView informationTextView;
    private TextView startDateLabel;
    private TextView endDateLabel;
    private Button createNewProcedureButton;
    private ListView listView;

    private List<EcmoDates> dates;

    private IcupadDbHelper mDbHelper;
    private PublicIcupadRepository publicIcupadRepository;
    private long stayId;
    private boolean inProgres;

    public EcmoHistoryFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ecmo_history, container, false);

        publicIcupadRepository = fragmentListener.getRepository();
        listView = (ListView) view.findViewById(R.id.ecmoHistoryListView);
        informationTextView = (TextView) view.findViewById(R.id.informationTextView);
        startDateLabel = (TextView) view.findViewById(R.id.startDate);
        endDateLabel = (TextView) view.findViewById(R.id.endDate);
        createNewProcedureButton = (Button)  view.findViewById(R.id.newEcmoButton);
        mDbHelper = new IcupadDbHelper(getActivity().getApplicationContext());
        dates = new ArrayList<>();
        inProgres = false;

        final EcmoHistoryAdapter listViewAdapter = new EcmoHistoryAdapter(getActivity(), dates);
        listView.setAdapter(listViewAdapter);

        if(PatientChooserFragment.getLastChosenPatientId() != null) {
            new Thread(new Runnable(){
                @Override
                public void run(){
                    populateList();
                    handler.post(new Runnable(){
                        @Override
                        public void run(){
                            if(dates.isEmpty()){
                                startDateLabel.setVisibility(View.GONE);
                                endDateLabel.setVisibility(View.GONE);
                            }
                            listViewAdapter.notifyDataSetChanged();
                            if(inProgres){
                                createNewProcedureButton.setEnabled(false);
                            }

                            addClickListeners();
                            fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
                        }
                    });
                }
            }).start();
        }else{
            startDateLabel.setVisibility(View.GONE);
            endDateLabel.setVisibility(View.GONE);
            createNewProcedureButton.setVisibility(View.GONE);
            informationTextView.setVisibility(View.VISIBLE);
            informationTextView.setText(getResources().getString(R.string.no_chosen_patient));
            fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
        }
        return view;
    }

    private void populateList(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        List<Long> stayIds = publicIcupadRepository.getAllStayByUserId();

        for(Long stayId : stayIds) {
            Query query = QueryBuilder.aQuery(db)
                    .withTable(IcupadDbContract.EcmoProcedure.TABLE_NAME)
                    .withColumns(IcupadDbContract.EcmoProcedure.TABLE_NAME_START_DATE, IcupadDbContract.EcmoProcedure.TABLE_NAME_END_DATE,
                            IcupadDbContract.EcmoProcedure._ID)
                    .withSelection(IcupadDbContract.EcmoProcedure.TABLE_NAME_STAY + " =?")
                    .withSelectionArgs(stayId)
                    .build();

            Cursor cursor = query.select();
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                if (cursor.getString(1) != null) {
                    dates.add(new EcmoDates(DateTimeFormatterHelper.transformStringToDate(cursor.getString(0)), DateTimeFormatterHelper.transformStringToDate(cursor.getString(1)), cursor.getLong(2)));
                } else {
                    dates.add(new EcmoDates(DateTimeFormatterHelper.transformStringToDate(cursor.getString(0)), getString(R.string.ecmo_no_end_data), cursor.getLong(2)));
                    inProgres = true;
                }
                cursor.moveToNext();
            }

            cursor.close();
        }
        mDbHelper.closeDb(db);
    }

    private void addClickListeners(){
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                EcmoDates p = (EcmoDates) adapterView.getItemAtPosition(i);
                EcmoVisualizationFragment ecmoVisualizationFragment = new EcmoVisualizationFragment();
                Bundle bundle = new Bundle();
                bundle.putLong("procadureId",p.getProcedureId());
                bundle.putString("startDate",p.getStartDate());

                if(((TextView) view.findViewById(R.id.endDate)).getText().toString().equals(getString(R.string.ecmo_no_end_data))){
                    bundle.putBoolean("active", true);
                }else{
                    bundle.putBoolean("active", false);
                }

                ecmoVisualizationFragment.setArguments(bundle);

                fragmentListener.replaceFragment(ecmoVisualizationFragment);
            }
        });

        createNewProcedureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EcmoCreateNewFragment ecmoCreateNewFragment = new EcmoCreateNewFragment();
                fragmentListener.replaceFragment(ecmoCreateNewFragment);
            }
        });
    }
}
