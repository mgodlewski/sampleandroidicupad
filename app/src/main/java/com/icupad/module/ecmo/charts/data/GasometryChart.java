package com.icupad.module.ecmo.charts.data;

import android.graphics.Color;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.BloodGasMeasurement;
import com.icupad.commons.utils.DateTimeFormatterHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 21.05.2017.
 */
public class GasometryChart extends BaseChart {

    List<BloodGasMeasurement> bloodGasMeasurements;

    public GasometryChart(Long patientId, PublicIcupadRepository publicIcupadRepository, List<BloodGasMeasurement> bloodGasMeasurements) {
        super(patientId, publicIcupadRepository);
        this.bloodGasMeasurements = bloodGasMeasurements;
        prepateChartData();
    }

    @Override
    protected LineData prepareChart(){
        List<Entry> phValues = new ArrayList<>();
        List<Entry> phTopNorm = new ArrayList<>();
        List<Entry> phBottomNorm = new ArrayList<>();

        List<Entry> pco2Values = new ArrayList<>();
        List<Entry> pco2TopNorm = new ArrayList<>();
        List<Entry> pco2BottomNorm = new ArrayList<>();

        List<Entry> lactasesValues = new ArrayList<>();
        List<Entry> lactasesTopNorm = new ArrayList<>();
        List<Entry> lactasesBottomNorm = new ArrayList<>();

        List<Entry> sbcValues = new ArrayList<>();
        List<Entry> sbcTopNorm = new ArrayList<>();
        List<Entry> sbcBottomNorm = new ArrayList<>();

        List<Entry> beValues = new ArrayList<>();
        List<Entry> beTopNorm = new ArrayList<>();
        List<Entry> beBottomNorm = new ArrayList<>();

        for(BloodGasMeasurement bloodGasMeasurement : bloodGasMeasurements){
            if(bloodGasMeasurement.getName().equals("pH")){
                phValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getValue().floatValue()));
                phTopNorm.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getTopDefaultNorm().floatValue()));
                phBottomNorm.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getBottomDefaultNorm().floatValue()));
                continue;
            }else if(bloodGasMeasurement.getName().equals("pCO2")){
                pco2Values.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getValue().floatValue()));
                pco2TopNorm.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getTopDefaultNorm().floatValue()));
                pco2BottomNorm.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getBottomDefaultNorm().floatValue()));
                continue;
            }else if(bloodGasMeasurement.getName().equals("Mleczany") || bloodGasMeasurement.getName().equals("Lac")){
                lactasesValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getValue().floatValue()));
                lactasesTopNorm.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getTopDefaultNorm().floatValue()));
                lactasesBottomNorm.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getBottomDefaultNorm().floatValue()));
                continue;
            }else if(bloodGasMeasurement.getName().equals("SBC")){
                sbcValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getValue().floatValue()));
                sbcTopNorm.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getTopDefaultNorm().floatValue()));
                sbcBottomNorm.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getBottomDefaultNorm().floatValue()));
                continue;
            }else if(bloodGasMeasurement.getName().equals("BE-B")){
                beValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getValue().floatValue()));
                beTopNorm.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getTopDefaultNorm().floatValue()));
                beBottomNorm.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getBottomDefaultNorm().floatValue()));
                continue;
            }
        }

        LineDataSet phSet = new LineDataSet(phValues, "pH");
        LineDataSet phTopNormSet = new LineDataSet(phTopNorm,"");
        LineDataSet phBottomNormSet = new LineDataSet(phBottomNorm,"");
        configurateChart(phSet, Color.RED, YAxis.AxisDependency.LEFT);
        configurateNormsCharts(phTopNormSet, phBottomNormSet, Color.RED, YAxis.AxisDependency.LEFT);

        LineDataSet pco2Set = new LineDataSet(pco2Values, "pCO2 [mmHg]");
        LineDataSet pco2TopNormSet = new LineDataSet(pco2TopNorm,"");
        LineDataSet pco2BottomNormSet = new LineDataSet(pco2BottomNorm,"");
        configurateChart(pco2Set, Color.BLUE, YAxis.AxisDependency.LEFT);
        configurateNormsCharts(pco2TopNormSet, pco2BottomNormSet, Color.BLUE, YAxis.AxisDependency.LEFT);

        LineDataSet lactasesSet = new LineDataSet(lactasesValues, "Mleczany [mmol/l]");
        LineDataSet lactasesTopNormSet = new LineDataSet(lactasesTopNorm,"");
        LineDataSet lactasesBottomNormSet = new LineDataSet(lactasesBottomNorm,"");
        configurateChart(lactasesSet, Color.GREEN, YAxis.AxisDependency.RIGHT);
        configurateNormsCharts(lactasesTopNormSet, lactasesBottomNormSet, Color.GREEN, YAxis.AxisDependency.RIGHT);

        LineDataSet sbcSet = new LineDataSet(sbcValues, "SBC [mmol/l]");
        LineDataSet sbcTopNormSet = new LineDataSet(sbcTopNorm,"");
        LineDataSet sbcBottomNormSet = new LineDataSet(sbcBottomNorm,"");
        configurateChart(sbcSet,  Color.YELLOW, YAxis.AxisDependency.RIGHT);
        configurateNormsCharts(sbcTopNormSet, sbcBottomNormSet, Color.YELLOW, YAxis.AxisDependency.RIGHT);

        LineDataSet beSet = new LineDataSet(beValues, "BE-B [mmol/l]");
        LineDataSet beTopNormSet = new LineDataSet(beTopNorm,"");
        LineDataSet beBottomNormSet = new LineDataSet(beBottomNorm,"");
        configurateChart(beSet, Color.GRAY, YAxis.AxisDependency.RIGHT);
        configurateNormsCharts(beTopNormSet, beBottomNormSet, Color.GRAY, YAxis.AxisDependency.RIGHT);

        List<ILineDataSet> dataSets = new ArrayList<>();
        if(phSet.getEntryCount()>0){
            dataSets.add(phSet);
            dataSets.add(phTopNormSet);
            dataSets.add(phBottomNormSet);
        }

        if(pco2Set.getEntryCount()>0){
            dataSets.add(pco2Set);
            dataSets.add(pco2TopNormSet);
            dataSets.add(pco2BottomNormSet);
        }

        if(lactasesSet.getEntryCount()>0) {
            dataSets.add(lactasesSet);
            dataSets.add(lactasesTopNormSet);
            dataSets.add(lactasesBottomNormSet);
        }

        if(sbcSet.getEntryCount()>0) {
            dataSets.add(sbcSet);
            dataSets.add(sbcTopNormSet);
            dataSets.add(sbcBottomNormSet);
        }

        if(beSet.getEntryCount()>0) {
            dataSets.add(beSet);
            dataSets.add(beTopNormSet);
            dataSets.add(beBottomNormSet);
        }

        LineData lineData = new LineData(dataSets);
        lineData.setValueFormatter(new DefaultValueFormatter(3));

        return lineData;
    }

    @Override
    protected List<LegendEntry> prepareLegend(){
        List<LegendEntry> legend = new ArrayList<>();
        legend.add(new LegendEntry("pH", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.RED));
        legend.add(new LegendEntry("pCO2 [mmHg]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.BLUE));
        legend.add(new LegendEntry("Mleczany [mmol/l]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.GREEN));
        legend.add(new LegendEntry("SBC [mmol/l]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.YELLOW));
        legend.add(new LegendEntry("BE-B [mmol/l]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.GRAY));
        return legend;
    }
}
