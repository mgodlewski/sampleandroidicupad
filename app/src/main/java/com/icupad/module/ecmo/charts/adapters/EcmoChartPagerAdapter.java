package com.icupad.module.ecmo.charts.adapters;

import android.app.Fragment;
import android.app.FragmentManager;

import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;

import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.LineData;
import com.icupad.commons.ModuleFragment;
import com.icupad.module.ecmo.charts.data.BaseChart;
import com.icupad.module.ecmo.charts.fragments.EcmoChartTabFragment;
import com.icupad.module.ecmo.charts.fragments.EcmoChartTabScrollFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 21.05.2017.
 */
public class EcmoChartPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    private ArrayList<LineData> lineDatas = new ArrayList<>();
    private ArrayList<List<LegendEntry>> legendList = new ArrayList<>();

    public EcmoChartPagerAdapter(FragmentManager fm, int NumOfTabs, ArrayList<BaseChart> charts) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;

        for (BaseChart baseChart : charts){
            lineDatas.add(baseChart.getLineData());
            legendList.add(baseChart.getLegendEntry());
        }
    }

    @Override
    public Fragment getItem(int position) {
        ModuleFragment tab;
        Bundle bundle = new Bundle();
        bundle.putSerializable("lineDatas", lineDatas);
        bundle.putSerializable("legendList", legendList);

        if(position!=0) {
            tab = new EcmoChartTabFragment();
            bundle.putInt("id", position + 1);
        }else{
            tab = new EcmoChartTabScrollFragment();
        }

        tab.setArguments(bundle);
        return tab;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

}
