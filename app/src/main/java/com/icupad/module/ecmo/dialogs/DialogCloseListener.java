package com.icupad.module.ecmo.dialogs;

import android.content.DialogInterface;

import java.util.List;

/**
 * Created by Marcin on 20.03.2017.
 */
public interface DialogCloseListener {

    void onDialogClose(String... parameters);
}
