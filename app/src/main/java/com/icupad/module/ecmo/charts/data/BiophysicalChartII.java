package com.icupad.module.ecmo.charts.data;

import android.graphics.Color;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.EcmoPatient;
import com.icupad.commons.utils.DateTimeFormatterHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 22.05.2017.
 */
public class BiophysicalChartII extends BaseChart {

    List<EcmoPatient> ecmoPatients;

    public BiophysicalChartII(Long patientId, PublicIcupadRepository publicIcupadRepository, List<EcmoPatient> ecmoPatients ) {
        super(patientId, publicIcupadRepository);
        this.ecmoPatients = ecmoPatients;
        prepateChartData();
    }

    @Override
    protected LineData prepareChart() {
        List<Entry> diuresisValues = new ArrayList<>();
        List<Entry> ultrafiltractionValues = new ArrayList<>();

        for(EcmoPatient ecmoPatient : ecmoPatients){
            if(ecmoPatient.getDiuresis()!=null) {
                diuresisValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(ecmoPatient.getDate()), ecmoPatient.getDiuresis().floatValue()));
            }
            if(ecmoPatient.getUltrafiltraction()!=null) {
                ultrafiltractionValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(ecmoPatient.getDate()), ecmoPatient.getUltrafiltraction().floatValue()));
            }
        }

        LineDataSet diuresisSet = new LineDataSet(diuresisValues, "Diureza godzinowa [ml]");
        LineDataSet ultrafiltractionwSet = new LineDataSet(ultrafiltractionValues, "Ultrafiltracja godzinowa [ml]");

        configurateChart(diuresisSet, Color.RED, YAxis.AxisDependency.LEFT);
        configurateChart(ultrafiltractionwSet, Color.BLUE, YAxis.AxisDependency.LEFT);

        List<ILineDataSet> dataSets = new ArrayList<>();
        if(diuresisSet.getEntryCount()>0){
            dataSets.add(diuresisSet);
        }
        if(ultrafiltractionwSet.getEntryCount()>0){
            dataSets.add(ultrafiltractionwSet);
        }

        LineData lineData = new LineData(dataSets);
        lineData.setValueFormatter(new DefaultValueFormatter(3));

        return lineData;
    }

    @Override
    protected List<LegendEntry> prepareLegend() {
        List<LegendEntry> legend = new ArrayList<>();
        legend.add(new LegendEntry("Diureza godzinowa [ml]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.RED));
        legend.add(new LegendEntry("Ultrafiltracja godzinowa [ml]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.BLUE));
        return legend;
    }
}
