package com.icupad.module.ecmo.charts.data;

import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.icupad.commons.repository.PublicIcupadRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 21.05.2017.
 */
public abstract class BaseChart {

    protected Long patientId;
    protected PublicIcupadRepository publicIcupadRepository;
    protected LineData lineData;
    protected List<LegendEntry> legendEntry = new ArrayList<>();

    public BaseChart(Long patientId, PublicIcupadRepository publicIcupadRepository) {
        this.patientId = patientId;
        this.publicIcupadRepository = publicIcupadRepository;
    }

    public void prepateChartData(){
        this.lineData = prepareChart();
        this.legendEntry = prepareLegend();
    }

    public static void configurateChart(LineDataSet dataSet, int color, YAxis.AxisDependency axisDependency){
        dataSet.setColor(color);
        dataSet.setCircleColor(color);
        dataSet.setAxisDependency(axisDependency);
        dataSet.setDrawValues(false);
    }

    protected void configurateNormsCharts(LineDataSet topNormDataSet, LineDataSet bottomNormDataSet, int color, YAxis.AxisDependency axisDependency){
        topNormDataSet.setDrawCircles(false);
        topNormDataSet.setColor(color);
        topNormDataSet.setAxisDependency(axisDependency);
        topNormDataSet.setDrawValues(false);
        topNormDataSet.enableDashedLine(10,20,0);
        topNormDataSet.setDrawHighlightIndicators(false);

        bottomNormDataSet.setDrawCircles(false);
        bottomNormDataSet.setColor(color);
        bottomNormDataSet.setAxisDependency(axisDependency);
        bottomNormDataSet.setDrawValues(false);
        bottomNormDataSet.enableDashedLine(10,20,0);
        bottomNormDataSet.setDrawHighlightIndicators(false);
    }

    protected abstract LineData prepareChart();
    protected abstract List<LegendEntry> prepareLegend();

    public LineData getLineData() {
        return lineData;
    }
    public List<LegendEntry> getLegendEntry() {
        return legendEntry;
    }
}
