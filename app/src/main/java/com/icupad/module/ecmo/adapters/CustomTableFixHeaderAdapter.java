package com.icupad.module.ecmo.adapters;

import android.content.Context;

import com.icupad.module.ecmo.CellViewGroup;

import java.util.ArrayList;
import java.util.List;

import miguelbcr.ui.tableFixHeadesWrapper.TableFixHeaderAdapter;

/**
 * Created by Marcin on 03.06.2017.
 */
public class CustomTableFixHeaderAdapter extends TableFixHeaderAdapter<
        String, CellViewGroup,
        String, CellViewGroup,
        List<String>,
        CellViewGroup,
        CellViewGroup,
        CellViewGroup> {

    private Context context;

    public CustomTableFixHeaderAdapter(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected CellViewGroup inflateFirstHeader() {
        return new CellViewGroup(context);
    }

    @Override
    protected CellViewGroup inflateHeader() {
        return new CellViewGroup(context);
    }

    @Override
    protected CellViewGroup inflateFirstBody() {
        return new CellViewGroup(context);
    }

    @Override
    protected CellViewGroup inflateBody() {
        return new CellViewGroup(context);
    }

    @Override
    protected CellViewGroup inflateSection() {
        return new CellViewGroup(context);
    }

    @Override
    protected List<Integer> getHeaderWidths() {
        List<Integer> headerWidths = new ArrayList<>();

        // First header
        headerWidths.add(250);

        for (int i = 0; i < getHeader().size(); i++)
            headerWidths.add(100);

        return headerWidths;
    }

    @Override
    protected int getHeaderHeight() {
        return (100);
    }

    @Override
    protected int getSectionHeight() {
        return (0);
    }

    @Override
    protected int getBodyHeight() {
        return (60);
    }

    @Override
    protected boolean isSection(List<List<String>> items, int row) {
        return false;
    }
}
