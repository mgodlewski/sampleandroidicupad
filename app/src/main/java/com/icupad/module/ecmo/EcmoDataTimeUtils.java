package com.icupad.module.ecmo;

import android.widget.TextView;

import com.icupad.bloodgas.populator.HalfDaysComputer;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.utils.DateTimeFormatterHelper;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by Marcin on 02.06.2017.
 */
public abstract class EcmoDataTimeUtils {

    public static void setDataTextViews(TextView dayTextView, String startDate, PublicIcupadRepository publicIcupadRepository){
        HalfDaysComputer halfDaysComputer = new HalfDaysComputer();
        if(publicIcupadRepository.getCurrentStay()==null) {
            dayTextView.setText("Doba pobytu: - , doba zabiegu: - ");
        }else{
            int stayTime = halfDaysComputer.compute(publicIcupadRepository.getCurrentStay().getAdmitDate(), DateTimeFormatterHelper.getCurrentDateTime());
            int ecmoTime = 0;
            if (startDate != null) {   //TODO poprawić obliczanie daty
                ecmoTime = halfDaysComputer.compute(startDate, DateTimeFormatterHelper.getCurrentDateTime());
            }
            dayTextView.setText("Doba pobytu: " + stayTime + ", doba zabiegu: " + ecmoTime);
        }
    }
}
