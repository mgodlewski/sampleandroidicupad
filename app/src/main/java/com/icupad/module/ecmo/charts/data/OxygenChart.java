package com.icupad.module.ecmo.charts.data;

import android.graphics.Color;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.BloodGasMeasurement;
import com.icupad.commons.repository.model.EcmoParameter;
import com.icupad.commons.utils.DateTimeFormatterHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 21.05.2017.
 */
public class OxygenChart extends BaseChart {

    List<BloodGasMeasurement> bloodGasMeasurements;
    List<EcmoParameter> ecmoParameters;

    public OxygenChart(Long patientId, PublicIcupadRepository publicIcupadRepository, List<BloodGasMeasurement> bloodGasMeasurements, List<EcmoParameter> ecmoParameters) {
        super(patientId, publicIcupadRepository);
        this.bloodGasMeasurements = bloodGasMeasurements;
        this.ecmoParameters = ecmoParameters;
        prepateChartData();
    }

    @Override
    protected LineData prepareChart() {
        List<Entry> fio2Values = new ArrayList<>();

        List<Entry> sp02Value = new ArrayList<>();

        List<Entry> po2Value  = new ArrayList<>();
        List<Entry> poTop2Value  = new ArrayList<>();
        List<Entry> poBottom2Value  = new ArrayList<>();

        for(EcmoParameter ecmoParameter : ecmoParameters){
            if(ecmoParameter.getFiO2()!=null) {
                fio2Values.add(new Entry(DateTimeFormatterHelper.getMillisFromString(ecmoParameter.getDate()), ecmoParameter.getFiO2().floatValue()));
            }
        }

        for(BloodGasMeasurement bloodGasMeasurement : bloodGasMeasurements) {
            if (bloodGasMeasurement.getName().equals("pO2")) {
                po2Value.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getValue().floatValue()));
                poTop2Value.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getTopDefaultNorm().floatValue()));
                poBottom2Value.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getBottomDefaultNorm().floatValue()));
                continue;
            }else if (bloodGasMeasurement.getName().equals("sO2")) {
                sp02Value.add(new Entry(DateTimeFormatterHelper.getMillisFromString(bloodGasMeasurement.getResultDate()), bloodGasMeasurement.getValue().floatValue()));
                continue;
            }
        }

        LineDataSet fio2Set = new LineDataSet(fio2Values, "FiO2 [%]");
        LineDataSet sp02Set = new LineDataSet(sp02Value, "SpO2 [%]");
        LineDataSet po2Set = new LineDataSet(po2Value, "pO2 [%]");
        LineDataSet po2TopSet = new LineDataSet(poTop2Value, "");
        LineDataSet po2BottomSet = new LineDataSet(poBottom2Value, "");

        configurateChart(fio2Set, Color.RED, YAxis.AxisDependency.LEFT);
        configurateChart(sp02Set, Color.BLUE, YAxis.AxisDependency.LEFT);
        configurateChart(po2Set, Color.GREEN, YAxis.AxisDependency.RIGHT);
        configurateNormsCharts(po2TopSet, po2BottomSet, Color.GREEN, YAxis.AxisDependency.RIGHT);

        List<ILineDataSet> dataSets = new ArrayList<>();
        if(fio2Set.getEntryCount()>0){
            dataSets.add(fio2Set);
        }
        if(sp02Set.getEntryCount()>0){
            dataSets.add(sp02Set);
        }
        if(po2Set.getEntryCount()>0){
            dataSets.add(po2Set);
            dataSets.add(po2TopSet);
            dataSets.add(po2BottomSet);
        }

        LineData lineData = new LineData(dataSets);
        lineData.setValueFormatter(new DefaultValueFormatter(3));

        return lineData;
    }

    @Override
    protected List<LegendEntry> prepareLegend() {
        List<LegendEntry> legend = new ArrayList<>();
        legend.add(new LegendEntry("FiO2 [%]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.RED));
        legend.add(new LegendEntry("SpO2 [%]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.BLUE));
        legend.add(new LegendEntry("pO2 [mmHg]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.GREEN));
        return legend;
    }
}
