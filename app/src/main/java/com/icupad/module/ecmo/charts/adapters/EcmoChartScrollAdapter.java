package com.icupad.module.ecmo.charts.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.LineData;
import com.icupad.R;
import com.icupad.module.ecmo.charts.ChartConfiguration;

import java.util.List;

/**
 * Created by Marcin on 22.05.2017.
 */
public class EcmoChartScrollAdapter extends BaseAdapter {

    private List<LineData> chartDataList;
    private Context context;
    private List<List<LegendEntry>> legendList;

    public EcmoChartScrollAdapter(List<LineData> chartDataList, Context context, List<List<LegendEntry>> legendList) {
        this.chartDataList = chartDataList;
        this.context = context;
        this.legendList = legendList;
    }

    @Override
    public int getCount() {
        return chartDataList.size();
    }

    @Override
    public Object getItem(int i) {
        return chartDataList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LineData lineData = chartDataList.get(i);
        ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(view == null){
            view = inflater.inflate(R.layout.row_line_chart, null);
            holder = new ViewHolder();
            holder.lineChart = (LineChart) view.findViewById(R.id.lineChart);
            view.setTag(holder);
        }else{
            holder = (ViewHolder) view.getTag();
        }

        if(!ChartConfiguration.isLineChartEmpty(lineData)) {
            holder.lineChart.setData(lineData);
            ChartConfiguration.configurateChart(holder.lineChart, context, false);
            ChartConfiguration.configurateLegend(holder.lineChart, legendList.get(i));
        }else{
            holder.lineChart.setData(null);
            holder.lineChart.setNoDataText(context.getResources().getString(R.string.no_data));
            Paint p = holder.lineChart.getPaint(Chart.PAINT_INFO);
            p.setTextSize(15f);
            p.setColor(Color.BLACK);
        }

        holder.lineChart.invalidate();

        return view;
    }


    private class ViewHolder{
        LineChart lineChart;
    }
}
