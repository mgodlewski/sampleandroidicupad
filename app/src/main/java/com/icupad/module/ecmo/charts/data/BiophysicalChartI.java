package com.icupad.module.ecmo.charts.data;

import android.graphics.Color;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.EcmoPatient;
import com.icupad.commons.utils.DateTimeFormatterHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 22.05.2017.
 */
public class BiophysicalChartI extends BaseChart {

    List<EcmoPatient> ecmoPatients;

    public BiophysicalChartI(Long patientId, PublicIcupadRepository publicIcupadRepository, List<EcmoPatient> ecmoPatients ) {
        super(patientId, publicIcupadRepository);
        this.ecmoPatients = ecmoPatients;
        prepateChartData();
    }

    @Override
    protected LineData prepareChart() {
        List<Entry> abpValues = new ArrayList<>();
        List<Entry> vcpValues = new ArrayList<>();
        List<Entry> hrValues = new ArrayList<>();

        for(EcmoPatient ecmoPatient : ecmoPatients){
            if(ecmoPatient.getAbp()!=null) {
                abpValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(ecmoPatient.getDate()), ecmoPatient.getAbp().floatValue()));
            }
            if(ecmoPatient.getVcp()!=null) {
                vcpValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(ecmoPatient.getDate()), ecmoPatient.getVcp().floatValue()));
            }
            if(ecmoPatient.getHr()!=null) {
                hrValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(ecmoPatient.getDate()), ecmoPatient.getHr().floatValue()));
            }
        }

        LineDataSet abpSet = new LineDataSet(abpValues, "ABP [mmHg]");
        LineDataSet vcpSet = new LineDataSet(vcpValues, "CVP [mmHg]");
        LineDataSet hrSet = new LineDataSet(hrValues, "HR [/min]");

        configurateChart(abpSet, Color.RED, YAxis.AxisDependency.LEFT);
        configurateChart(vcpSet, Color.BLUE, YAxis.AxisDependency.LEFT);
        configurateChart(hrSet, Color.GREEN, YAxis.AxisDependency.RIGHT);

        List<ILineDataSet> dataSets = new ArrayList<>();
        if(abpSet.getEntryCount()>0){
            dataSets.add(abpSet);
        }
        if(vcpSet.getEntryCount()>0){
            dataSets.add(vcpSet);
        }
        if(hrSet.getEntryCount()>0){
            dataSets.add(hrSet);
        }

        LineData lineData = new LineData(dataSets);
        lineData.setValueFormatter(new DefaultValueFormatter(3));

        return lineData;
    }

    @Override
    protected List<LegendEntry> prepareLegend() {
        List<LegendEntry> legend = new ArrayList<>();
        legend.add(new LegendEntry("ABP [mmHg]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.RED));
        legend.add(new LegendEntry("CVP [mmHg]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.BLUE));
        legend.add(new LegendEntry("HR [/min]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.GREEN));
        return legend;
    }
}
