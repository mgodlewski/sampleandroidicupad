package com.icupad.module.ecmo.charts.data;

import android.graphics.Color;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.EcmoParameter;
import com.icupad.commons.utils.DateTimeFormatterHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 21.05.2017.
 */
public class OxygeneratorChart extends BaseChart {
    List<EcmoParameter> ecmoParameters;

    public OxygeneratorChart(Long patientId, PublicIcupadRepository publicIcupadRepository, List<EcmoParameter> ecmoParameters) {
        super(patientId, publicIcupadRepository);
        this.ecmoParameters = ecmoParameters;
        prepateChartData();
    }

    @Override
    protected LineData prepareChart() {
        List<Entry> rpmValues = new ArrayList<>();
        List<Entry> bloodFlowValues = new ArrayList<>();
        List<Entry> gasFlowValues = new ArrayList<>();

        for(EcmoParameter ecmoParameter : ecmoParameters){
            if(ecmoParameter.getRpm()!=null) {
                rpmValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(ecmoParameter.getDate()), ecmoParameter.getRpm().floatValue()));
            }
            if(ecmoParameter.getBloodFlow()!=null) {
                bloodFlowValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(ecmoParameter.getDate()), ecmoParameter.getBloodFlow().floatValue()));
            }
            if(ecmoParameter.getGasFlow()!=null) {
                gasFlowValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(ecmoParameter.getDate()), ecmoParameter.getGasFlow().floatValue()));
            }
        }

        LineDataSet rpmSet = new LineDataSet(rpmValues, "RPM");
        LineDataSet bloodFlowSet = new LineDataSet(bloodFlowValues, "BLOOD FLOW [l/min]");
        LineDataSet gasFlowSet = new LineDataSet(gasFlowValues, "GAS FLOW [l/mim]");

        configurateChart(rpmSet, Color.RED, YAxis.AxisDependency.LEFT);
        configurateChart(bloodFlowSet, Color.BLUE, YAxis.AxisDependency.RIGHT);
        configurateChart(gasFlowSet, Color.GREEN, YAxis.AxisDependency.RIGHT);

        List<ILineDataSet> dataSets = new ArrayList<>();
        if(rpmSet.getEntryCount()>0){
            dataSets.add(rpmSet);
        }
        if(bloodFlowSet.getEntryCount()>0){
            dataSets.add(bloodFlowSet);
        }
        if(gasFlowSet.getEntryCount()>0){
            dataSets.add(gasFlowSet);
        }

        LineData lineData = new LineData(dataSets);
        lineData.setValueFormatter(new DefaultValueFormatter(3));

        return lineData;
    }

    @Override
    protected List<LegendEntry> prepareLegend() {
        List<LegendEntry> legend = new ArrayList<>();
        legend.add(new LegendEntry("RPM", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.RED));
        legend.add(new LegendEntry("BLOOD FLOW [l/min]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.BLUE));
        legend.add(new LegendEntry("GAS FLOW [l/mim]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.GREEN));
        return legend;
    }
}
