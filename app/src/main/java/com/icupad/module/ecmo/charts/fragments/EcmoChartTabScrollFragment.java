package com.icupad.module.ecmo.charts.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.LineData;
import com.icupad.R;
import com.icupad.commons.ModuleFragment;
import com.icupad.module.ecmo.charts.adapters.EcmoChartScrollAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 22.05.2017.
 */
public class EcmoChartTabScrollFragment extends ModuleFragment {

    private LineChart lineChart;
    private ArrayList<LineData> lineDatas;
    private ArrayList<List<LegendEntry>> legendList;

    private ListView chartListView ;

    public EcmoChartTabScrollFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_scroll_chart_tab, container, false);
        chartListView = (ListView) view.findViewById(R.id.chartListView);

        Bundle args = getArguments();
        lineDatas = (ArrayList<LineData>) args.getSerializable("lineDatas");
        legendList = (ArrayList<List<LegendEntry>>) args.getSerializable("legendList");

        ArrayList<LineData> chartDataList = new ArrayList<>();
        chartDataList.add(lineDatas.get(0));
        chartDataList.add(lineDatas.get(1));

        EcmoChartScrollAdapter lineChartAdapter = new EcmoChartScrollAdapter(chartDataList, getActivity(), legendList);
        chartListView.setAdapter(lineChartAdapter);

        return view;
    }
}
