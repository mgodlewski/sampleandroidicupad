package com.icupad.module.ecmo.charts.fragments;

import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.LineData;
import com.icupad.R;
import com.icupad.commons.ModuleFragment;
import com.icupad.module.ecmo.charts.ChartConfiguration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 21.05.2017.
 */
public class EcmoChartTabFragment extends ModuleFragment{

    int id;
    private LineChart lineChart;
    private ArrayList<LineData> lineDatas;
    private ArrayList<List<LegendEntry>> legendList;

    public EcmoChartTabFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ecmo_chart_tab, container, false);
        lineChart = (LineChart) view.findViewById(R.id.lineChart);

        Bundle args = getArguments();
        id = args.getInt("id");
        lineDatas = (ArrayList<LineData>) args.getSerializable("lineDatas");
        legendList = (ArrayList<List<LegendEntry>>) args.getSerializable("legendList");

        LineData lineData = lineDatas.get(id);

        if(!ChartConfiguration.isLineChartEmpty(lineData)) {
            lineChart.setData(lineData);
            ChartConfiguration.configurateChart(lineChart, getActivity(), false);
            ChartConfiguration.configurateLegend(lineChart, legendList.get(id));
        }else{
            lineChart.setData(null);
            lineChart.setNoDataText(getActivity().getResources().getString(R.string.no_data));
            Paint p = lineChart.getPaint(Chart.PAINT_INFO);
            p.setTextSize(15f);
            p.setColor(Color.BLACK);
        }

        return view;
    }
}
