package com.icupad.module.ecmo.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.icupad.R;
import com.icupad.bloodgas.populator.HalfDaysComputer;
import com.icupad.commons.ModuleFragment;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.EcmoConfiguration;
import com.icupad.commons.repository.model.EcmoProcedure;
import com.icupad.commons.repository.model.VeinCannula;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.module.ecmo.EcmoDataTimeUtils;
import com.icupad.module.ecmo.dialogs.DataTimeFragment;
import com.icupad.module.ecmo.dialogs.DialogCloseListener;
import com.icupad.module.ecmo.dialogs.DialogEndProcedureFragment;
import com.icupad.module.ecmo.dialogs.DialogVeinCannulaFragment;
import com.icupad.module.ecmo.dialogs.MultipleChoiceFragment;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;

/**
 * Created by Marcin on 21.03.2017.
 */
public class EcmoInfoFragment extends ModuleFragment {
    
    private Logger LOGGER = Logger.getLogger(this.getClass().toString());

    private final Handler handler = new Handler();

    private IcupadDbHelper mDbHelper;
    private PublicIcupadRepository publicIcupadRepository;

    private Button saveButton;
    private Button visualizationButton;
    private Button endButton;
    private Button histButton;

    private Spinner causeOfStartInput;
    private Spinner modeInput;
    private Spinner canniulationTypeInput;
    private EditText usedStructuresInput;
    private Spinner wentowanieLeftVerticleInput;
    private EditText veinCannulaInput;
    private EditText arteryCannulaTypeInput;
    private EditText arteryCannulaSizeInput;
    private EditText oxygeneratorTypeInput;
    private Button dateTimeInputButton;
    private TextView dayTextView;

    private EcmoProcedure ecmoProcedure;
    private EcmoConfiguration ecmoConfiguration;
    private Long procedureId;
    private boolean active;
    private String startDate;

    private String selectedItems;
    private String veinCannulas;

    private TextView causeOfEndTextView;
    private EditText causeOfEndInput;
    private TextView endDateTime;
    private Button endDateTimeInput;

    public EcmoInfoFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_ecmo_info, container, false);

        publicIcupadRepository = fragmentListener.getRepository();
        mDbHelper = new IcupadDbHelper(getActivity().getApplicationContext());

        Bundle args = getArguments();
        procedureId = args.getLong("procadureId");
        active = args.getBoolean("active");
        startDate = args.getString("startDate");

        selectedItems = "";
        veinCannulas = "";

        new Thread(new Runnable(){
            @Override
            public void run(){
                ecmoProcedure = publicIcupadRepository.getEcmoProcedureById(procedureId);
                ecmoConfiguration = publicIcupadRepository.getEcmoFullConfigurationByProcedureId(procedureId);

                handler.post(new Runnable(){
                    @Override
                    public void run(){

                        getAllViewElements(view);
                        setAdapters();
                        setStartValues();

                        causeOfStartInput.setEnabled(false);
                        ((TextView)causeOfStartInput.getSelectedView()).setTextColor(getResources().getColor(R.color.black));

                        if(!active){
                            disableInputs();
                            showEndInfo();
                        }else{
                            hideEndInfo();
                        }

                        EcmoDataTimeUtils.setDataTextViews(dayTextView, startDate, publicIcupadRepository);
                        addClickListeners();

                        fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
                    }
                });
            }
        }).start();

        return view;
    }

    private void addClickListeners() {
        veinCannulaInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogVeinCannulaFragment();
            }
        });

        usedStructuresInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMultipleChoiceFragment();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextValidation()){
                    showDataTimeFragment();
                }
            }
        });
        visualizationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EcmoVisualizationFragment ecmoVisualizationFragment = new EcmoVisualizationFragment();
                Bundle bundle = new Bundle();
                bundle.putLong("procadureId",procedureId);
                bundle.putBoolean("active", active);
                bundle.putString("startDate",startDate);
                ecmoVisualizationFragment.setArguments(bundle);
                fragmentListener.replaceFragment(ecmoVisualizationFragment);
            }
        });
        endButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogEndProcedureFragment();
            }
        });
        histButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EcmoHistValuesFragment ecmoHistValuesFragment = new EcmoHistValuesFragment();
                Bundle bundle = new Bundle();
                bundle.putLong("procadureId",procedureId);
                bundle.putBoolean("active", active);
                bundle.putString("startDate",startDate);
                ecmoHistValuesFragment.setArguments(bundle);
                fragmentListener.replaceFragment(ecmoHistValuesFragment);
            }
        });
    }

    private void showDialogVeinCannulaFragment(){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        DialogVeinCannulaFragment dvcf = DialogVeinCannulaFragment.newInstance(veinCannulas);
        dvcf.setDialogCloseListener(new DialogCloseListener() {
            @Override
            public void onDialogClose(String... parameters) {
                veinCannulas = "";
                for (String s : parameters){
                    if(s.split(":").length>0){
                        veinCannulas += s + "; ";
                    }
                }
                veinCannulas = veinCannulas.length()>2 ? veinCannulas.substring(0, veinCannulas.length()-2) : veinCannulas;
                veinCannulaInput.setText(veinCannulas);
            }
        });
        dvcf.show(ft, "dialog");
    }

    private void showMultipleChoiceFragment(){
        Resources resources = getActivity().getApplicationContext().getResources();

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        MultipleChoiceFragment mtf = MultipleChoiceFragment.newInstance(
                resources.getString(R.string.multiple_choice_used_structures), resources.getStringArray(R.array.used_structures), selectedItems);
        mtf.setDialogCloseListener(new DialogCloseListener(){
            @Override
            public void onDialogClose(String... parameter) {
                selectedItems = "";
                usedStructuresInput.setText(new String());
                for(String s : parameter){
                    if(!s.isEmpty()) {
                        selectedItems += s + ", ";
                    }
                }
                selectedItems = selectedItems.length()>2 ? selectedItems.substring(0, selectedItems.length()-2) : selectedItems;
                usedStructuresInput.setText(selectedItems);
            }
        });
        mtf.show(ft, "dialog");
    }

    private void showDialogEndProcedureFragment(){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        DialogEndProcedureFragment depf = DialogEndProcedureFragment.newInstance();
        depf.setDialogCloseListener(new DialogCloseListener(){
            @Override
            public void onDialogClose(String... parameter) {
                EcmoProcedure ecmoProcedure = publicIcupadRepository.getEcmoProcedureById(procedureId);
                ecmoProcedure.setEndById(1L);  //TODO set real userId
                ecmoProcedure.setEndCause(parameter[0]);
                ecmoProcedure.setEndDate(parameter[1]);
                ecmoProcedure.setId(procedureId);
                publicIcupadRepository.endEcmoProcedure(ecmoProcedure);
                fragmentListener.onButtonPressed(Message.OPEN_ECMO_HISTORY_FRAGMENT);
            }
        });
        depf.show(ft, "dialog");
    }

    private void showDataTimeFragment(){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        DataTimeFragment dtf = DataTimeFragment.newInstance(DateTimeFormatterHelper.getCurrentDateTime());
        dtf.setDialogCloseListener(new DialogCloseListener(){
            @Override
            public void onDialogClose(String... parameters) {
                upadteConfiguration(parameters[0]);
                //TODO potwierdzenia zapisania
            }
        });
        dtf.show(ft, "dialog");
    }

    private void upadteConfiguration(String date){
        List<VeinCannula> veinCannula = new ArrayList<>();
        String[] veinCannulaTab = veinCannulaInput.getText().toString().split("; ");
        for (String s : veinCannulaTab){
            String[] sSplit = s.split(":");
            if(sSplit.length>0) {
                veinCannula.add(new VeinCannula(sSplit[0], Double.parseDouble(sSplit[1])));
            }
        }

        EcmoConfiguration ecmoConfiguration = new EcmoConfiguration();
        ecmoConfiguration.setProcedureId(procedureId);
        ecmoConfiguration.setCreatedBy(1L);  //TODO set real userId
        ecmoConfiguration.setMode(modeInput.getSelectedItem().toString());
        ecmoConfiguration.setCaniulationType(canniulationTypeInput.getSelectedItem().toString());
        ecmoConfiguration.setWentowanieLeftVentricle(wentowanieLeftVerticleInput.getSelectedItem().toString());
        ecmoConfiguration.setActual(true);
        ecmoConfiguration.setDate(date);

        ecmoConfiguration.setOxygeneratorType(oxygeneratorTypeInput.getText().toString());
        ecmoConfiguration.setCannulationStructures(new ArrayList<>(Arrays.asList(usedStructuresInput.getText().toString().split(", "))));
        ecmoConfiguration.setVeinCannulaList(veinCannula);

        if(arteryCannulaTypeInput.getText().toString().isEmpty()){
            ecmoConfiguration.setArteryCannulaType("");
        }else {
            ecmoConfiguration.setArteryCannulaType(arteryCannulaTypeInput.getText().toString());
        }
        if(arteryCannulaSizeInput.getText().toString().isEmpty()){
            ecmoConfiguration.setArteryCannulaSize(null);
        }else {
            ecmoConfiguration.setArteryCannulaSize(Double.parseDouble(arteryCannulaSizeInput.getText().toString()));
        }

        publicIcupadRepository.setActualFalseInEcmoConfigurationsByProcedureId(procedureId);
        publicIcupadRepository.saveEcmoFullConfiguration(ecmoConfiguration);
    }


    private void setStartValues() {
        int causeOfStartId = getItemIdOnSpinnerList(getResources().getStringArray(R.array.ecmo_start_causes), ecmoProcedure.getStartCause());
        if (causeOfStartId != -1) {
            causeOfStartInput.setSelection(causeOfStartId, false);
        }

        int modeId = getItemIdOnSpinnerList(getResources().getStringArray(R.array.ecmo_mode), ecmoConfiguration.getMode());
        if (modeId != -1) {
            modeInput.setSelection(modeId, false);
        }

        int canniulationTypeID = getItemIdOnSpinnerList(getResources().getStringArray(R.array.caniulation_type), ecmoConfiguration.getCaniulationType());
        if (canniulationTypeID != -1) {
            canniulationTypeInput.setSelection(canniulationTypeID, false);
        }

        selectedItems = "";
        for (String s : ecmoConfiguration.getCannulationStructures()) {
            selectedItems += s + ", ";
        }
        if(selectedItems.length()>2) {
            selectedItems = selectedItems.substring(0, selectedItems.length() - 2);
        }
        usedStructuresInput.setText(selectedItems);

        int wentowanieId = getItemIdOnSpinnerList(getResources().getStringArray(R.array.wentowanie_left_verticle), ecmoConfiguration.getWentowanieLeftVentricle());
        if(wentowanieId != -1){
            wentowanieLeftVerticleInput.setSelection(wentowanieId, false);
        }

        veinCannulas = "";
        for (VeinCannula vc : ecmoConfiguration.getVeinCannulaList()){
            veinCannulas += vc.getType() + ":" + vc.getSize() + "; ";
        }
        veinCannulaInput.setText(veinCannulas);

        arteryCannulaTypeInput.setText(ecmoConfiguration.getArteryCannulaType());
        if(ecmoConfiguration.getArteryCannulaType().isEmpty() || ecmoConfiguration.getArteryCannulaSize()==null){
            arteryCannulaSizeInput.setText("");
        }else{
            arteryCannulaSizeInput.setText(String.valueOf(ecmoConfiguration.getArteryCannulaSize()));
        }

        oxygeneratorTypeInput.setText(ecmoConfiguration.getOxygeneratorType());

        setNewDataTime(ecmoProcedure.getStartDate());
    }

    private int getItemIdOnSpinnerList(String[] list, String item){
        int i=0;
        for (String s : list){
            if(s.equals(item)){
                return i;
            }
            i++;
        }
        return 0;
    }

    private void setAdapters(){
        ArrayAdapter<CharSequence> causeOfStartAdapter = ArrayAdapter.createFromResource(this.getActivity(),R.array.ecmo_start_causes, R.layout.spinner_text_view);
        causeOfStartAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        causeOfStartInput.setAdapter(causeOfStartAdapter);

        ArrayAdapter<CharSequence> modeInputAdapter = ArrayAdapter.createFromResource(this.getActivity(),R.array.ecmo_mode, R.layout.spinner_text_view);
        modeInputAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        modeInput.setAdapter(modeInputAdapter);

        ArrayAdapter<CharSequence> canniulationTypeInputAdapter = ArrayAdapter.createFromResource(this.getActivity(),R.array.caniulation_type, R.layout.spinner_text_view);
        canniulationTypeInputAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        canniulationTypeInput.setAdapter(canniulationTypeInputAdapter);

        ArrayAdapter<CharSequence> wentowanieLeftVerticleInputAdapter = ArrayAdapter.createFromResource(this.getActivity(),R.array.wentowanie_left_verticle, R.layout.spinner_text_view);
        wentowanieLeftVerticleInputAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        wentowanieLeftVerticleInput.setAdapter(wentowanieLeftVerticleInputAdapter);
    }

    private void getAllViewElements(View view) {
        saveButton = (Button) view.findViewById(R.id.ecmoSaveButton);
        visualizationButton= (Button) view.findViewById(R.id.ecmoVisualisationButton);
        endButton = (Button) view.findViewById(R.id.ecmoEndButton);
        histButton = (Button) view.findViewById(R.id.ecmoHistoryButton);

        arteryCannulaTypeInput = (EditText) view.findViewById(R.id.arteryCannulaTypeInput);
        arteryCannulaSizeInput = (EditText) view.findViewById(R.id.arteryCannulaSizeInput);
        oxygeneratorTypeInput = (EditText) view.findViewById(R.id.oxygeneratorTypeInput);
        dateTimeInputButton = (Button) view.findViewById(R.id.dateTimeInput);
        causeOfStartInput = (Spinner) view.findViewById(R.id.causeOfStartInput);
        modeInput =(Spinner) view.findViewById(R.id.modeInput);
        canniulationTypeInput =(Spinner) view.findViewById(R.id.caniulationTypeInput);
        wentowanieLeftVerticleInput = (Spinner) view.findViewById(R.id.wentowanieLeftVerticleInput);
        usedStructuresInput = (EditText) view.findViewById(R.id.usedStructuresInput);
        veinCannulaInput = (EditText) view.findViewById(R.id.veinCannulaInput);

        dayTextView = (TextView) view.findViewById(R.id.day);

        causeOfEndTextView = (TextView) view.findViewById(R.id.causeOfEnd);
        causeOfEndInput = (EditText) view.findViewById(R.id.causeOfEndInput);
        endDateTime = (TextView) view.findViewById(R.id.endDateTime);
        endDateTimeInput = (Button) view.findViewById(R.id.endDateTimeInput);
    }

    private void setNewDataTime(String dateTime){
        dateTimeInputButton.setText(dateTime);
        this.ecmoProcedure.setStartDate(dateTime);
    }

    private boolean editTextValidation(){
        Resources resources = getActivity().getApplicationContext().getResources();
        if(oxygeneratorTypeInput.getText().toString().isEmpty()){
            oxygeneratorTypeInput.setError(resources.getString(R.string.empty_input));
            return false;
        }
        if(usedStructuresInput.getText().toString().isEmpty()){
            usedStructuresInput.setError(resources.getString(R.string.empty_input));
            return false;
        }
        if(veinCannulaInput.getText().toString().isEmpty()){
            veinCannulaInput.setError(resources.getString(R.string.empty_input));
            return false;
        }
        if(!arteryCannulaTypeInput.getText().toString().isEmpty() && arteryCannulaSizeInput.getText().toString().isEmpty()){
            arteryCannulaSizeInput.setError(resources.getString(R.string.empty_input));
            return false;
        }
        if(arteryCannulaTypeInput.getText().toString().isEmpty() && !arteryCannulaSizeInput.getText().toString().isEmpty()){
            arteryCannulaTypeInput.setError(resources.getString(R.string.empty_input));
            return false;
        }
        return true;
    }

    private void disableInputs(){
        saveButton.setEnabled(false);
        endButton.setEnabled(false);
        arteryCannulaTypeInput.setEnabled(false);
        arteryCannulaSizeInput.setEnabled(false);
        oxygeneratorTypeInput.setEnabled(false);
        dateTimeInputButton.setEnabled(false);
        modeInput.setEnabled(false);
        canniulationTypeInput.setEnabled(false);
        wentowanieLeftVerticleInput.setEnabled(false);
        usedStructuresInput.setEnabled(false);
        veinCannulaInput.setEnabled(false);

        ((TextView)modeInput.getSelectedView()).setTextColor(getResources().getColor(R.color.black));
        ((TextView)canniulationTypeInput.getSelectedView()).setTextColor(getResources().getColor(R.color.black));
        usedStructuresInput.setTextColor(getResources().getColor(R.color.black));
        ((TextView)wentowanieLeftVerticleInput.getSelectedView()).setTextColor(getResources().getColor(R.color.black));
        veinCannulaInput.setTextColor(getResources().getColor(R.color.black));
        arteryCannulaTypeInput.setTextColor(getResources().getColor(R.color.black));
        arteryCannulaSizeInput.setTextColor(getResources().getColor(R.color.black));
        oxygeneratorTypeInput.setTextColor(getResources().getColor(R.color.black));
        dateTimeInputButton.setTextColor(getResources().getColor(R.color.black));
    }

    private void hideEndInfo(){
        causeOfEndTextView.setVisibility(View.GONE);
        causeOfEndInput.setVisibility(View.GONE);
        endDateTime.setVisibility(View.GONE);
        endDateTimeInput.setVisibility(View.GONE);
    }

    private void showEndInfo(){
        causeOfEndTextView.setVisibility(View.VISIBLE);
        causeOfEndInput.setVisibility(View.VISIBLE);
        endDateTime.setVisibility(View.VISIBLE);
        endDateTimeInput.setVisibility(View.VISIBLE);

        causeOfEndInput.setEnabled(false);
        endDateTimeInput.setEnabled(false);

        endDateTimeInput.setText(ecmoProcedure.getEndDate());
        causeOfEndInput.setText(ecmoProcedure.getEndCause());

        endDateTimeInput.setTextColor(getResources().getColor(R.color.black));
        causeOfEndInput.setTextColor(getResources().getColor(R.color.black));
    }
}
