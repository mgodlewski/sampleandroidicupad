package com.icupad.module.ecmo.charts.data;

import android.graphics.Color;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.EcmoParameter;
import com.icupad.commons.utils.DateTimeFormatterHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 21.05.2017.
 */
public class PressureChart extends BaseChart {

    List<EcmoParameter> ecmoParameters;

    public PressureChart(Long patientId, PublicIcupadRepository publicIcupadRepository, List<EcmoParameter> ecmoParameters) {
        super(patientId, publicIcupadRepository);
        this.ecmoParameters = ecmoParameters;
        prepateChartData();
    }

    @Override
    protected LineData prepareChart() {
        List<Entry> p1Values = new ArrayList<>();
        List<Entry> p2Values = new ArrayList<>();
        List<Entry> p3Values = new ArrayList<>();
        List<Entry> deltaValues = new ArrayList<>();

        for(EcmoParameter ecmoParameter : ecmoParameters){
            if(ecmoParameter.getP1()!=null) {
                p1Values.add(new Entry(DateTimeFormatterHelper.getMillisFromString(ecmoParameter.getDate()), ecmoParameter.getP1().floatValue()));
            }
            if(ecmoParameter.getP2()!=null) {
                p2Values.add(new Entry(DateTimeFormatterHelper.getMillisFromString(ecmoParameter.getDate()), ecmoParameter.getP2().floatValue()));
            }
            if(ecmoParameter.getP3()!=null) {
                p3Values.add(new Entry(DateTimeFormatterHelper.getMillisFromString(ecmoParameter.getDate()), ecmoParameter.getP3().floatValue()));
            }
            if(ecmoParameter.getDelta()!=null) {
                deltaValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(ecmoParameter.getDate()), ecmoParameter.getDelta().floatValue()));
            }
        }

        LineDataSet p1Set = new LineDataSet(p1Values, "P1 [mmHg]");
        LineDataSet p2Set = new LineDataSet(p2Values, "P2 [mmHg]");
        LineDataSet p3Set = new LineDataSet(p3Values, "P3 [mmHg]");
        LineDataSet deltaSet = new LineDataSet(deltaValues, "delta [mmHg]");

        configurateChart(p1Set, Color.RED, YAxis.AxisDependency.LEFT);
        configurateChart(p2Set, Color.BLUE, YAxis.AxisDependency.LEFT);
        configurateChart(p3Set, Color.GREEN, YAxis.AxisDependency.LEFT);
        configurateChart(deltaSet, Color.YELLOW, YAxis.AxisDependency.LEFT);

        List<ILineDataSet> dataSets = new ArrayList<>();
        if(p1Set.getEntryCount()>0){
            dataSets.add(p1Set);
        }
        if(p2Set.getEntryCount()>0){
            dataSets.add(p2Set);
        }
        if(p3Set.getEntryCount()>0){
            dataSets.add(p3Set);
        }
        if(deltaSet.getEntryCount()>0){
            dataSets.add(deltaSet);
        }

        LineData lineData = new LineData(dataSets);
        lineData.setValueFormatter(new DefaultValueFormatter(3));

        return lineData;
    }

    @Override
    protected List<LegendEntry> prepareLegend() {
        List<LegendEntry> legend = new ArrayList<>();
        legend.add(new LegendEntry("P1 [mmHg]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.RED));
        legend.add(new LegendEntry("P2 [mmHg]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.BLUE));
        legend.add(new LegendEntry("P3 [mmHg]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.GREEN));
        legend.add(new LegendEntry("delta [mmHg]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.YELLOW));
        return legend;
    }
}
