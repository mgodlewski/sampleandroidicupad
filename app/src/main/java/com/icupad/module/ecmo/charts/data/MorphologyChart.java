package com.icupad.module.ecmo.charts.data;

import android.graphics.Color;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.BloodGasMeasurement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 23.05.2017.
 */
public class MorphologyChart extends BaseChart  {

    List<BloodGasMeasurement> completeBloodCountMeasurements;

    public MorphologyChart(Long patientId, PublicIcupadRepository publicIcupadRepository, List<BloodGasMeasurement> completeBloodCountMeasurements) {
        super(patientId, publicIcupadRepository);
        this.completeBloodCountMeasurements = completeBloodCountMeasurements;
        prepateChartData();
    }

    @Override
    protected LineData prepareChart() {
        List<Entry> hgbValues = new ArrayList<>();
        List<Entry> hctValues = new ArrayList<>();
        List<Entry> pltValues = new ArrayList<>();

        for(BloodGasMeasurement completeBloodCountMeasurement : completeBloodCountMeasurements){
            if(completeBloodCountMeasurement.getName().equals("Hemoglobina")){
                hgbValues.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
            }else
            if(completeBloodCountMeasurement.getName().equals("Hematokryt")){
                hctValues.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
            }else
            if(completeBloodCountMeasurement.getName().equals("Płytki")){
                pltValues.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
            }
        }

        LineDataSet hgbSet = new LineDataSet(hgbValues, "HGB [g/dl");
        LineDataSet hctSet = new LineDataSet(hctValues, "HCT [%]");
        LineDataSet pltSet = new LineDataSet(pltValues, "PLT  [/microl]");

        configurateChart(hgbSet, Color.RED, YAxis.AxisDependency.LEFT);
        configurateChart(hctSet, Color.BLUE, YAxis.AxisDependency.RIGHT);
        configurateChart(pltSet, Color.GREEN, YAxis.AxisDependency.RIGHT);

        List<ILineDataSet> dataSets = new ArrayList<>();
        if(hgbSet.getEntryCount()>0){
            dataSets.add(hgbSet);
        }
        if(hctSet.getEntryCount()>0){
            dataSets.add(hctSet);
        }
        if(pltSet.getEntryCount()>0){
            dataSets.add(pltSet);
        }

        LineData lineData = new LineData(dataSets);
        lineData.setValueFormatter(new DefaultValueFormatter(3));

        return lineData;
    }

    @Override
    protected List<LegendEntry> prepareLegend() {
        List<LegendEntry> legend = new ArrayList<>();
        legend.add(new LegendEntry("HGB [g/dl", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.RED));
        legend.add(new LegendEntry("HCT [%]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.BLUE));
        legend.add(new LegendEntry("PLT  [/microl]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.GREEN));
        return legend;
    }
}
