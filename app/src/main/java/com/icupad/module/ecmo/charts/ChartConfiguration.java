package com.icupad.module.ecmo.charts;

import android.content.Context;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.IMarker;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.LineData;
import com.icupad.R;
import com.icupad.module.ecmo.ChartMarkerView;

import java.util.List;

/**
 * Created by Marcin on 24.05.2017.
 */
public abstract class ChartConfiguration {

    public static void configurateChart(final LineChart chart, Context context, boolean leftYGidLines){
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setTextSize(20f);
        leftAxis.setDrawGridLines(leftYGidLines);
        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setTextSize(20f);
        rightAxis.setDrawGridLines(false);

        Description description = chart.getDescription();
        description.setEnabled(false);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(20f);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(true);
        xAxis.setValueFormatter(new DateXAxisFormatter());
        xAxis.setLabelCount(4, false);
        xAxis.setGranularity(1*24*60*60*1000l);

        chart.setTouchEnabled(true);
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);

        chart.setLogEnabled(false);

        moveChartToLastDay(chart);

        //This cause null pointerException when remove the line where the marker is on at the moment
//        IMarker marker = new ChartMarkerView(context, R.layout.marker_layout);
//        chart.setMarker(marker);

    }

    public static void configurateLegend(final LineChart chart, List<LegendEntry> legendList){
        Legend legend = chart.getLegend();
        if(legendList!=null) {
            setLegendSize(legend);
            legend.setCustom(legendList);
        } else {
            legend.setEnabled(false);
        }
    }

    public static void setLegendSize(Legend legend){
        legend.setTextSize(15f);
        legend.setFormSize(10f);
        legend.setXEntrySpace(10f);
    }

    public static boolean isLineChartEmpty(LineData lineData ){
        if(lineData.getDataSets().size()==0)
            return true;
        else
            return false;
    }

    public static void moveChartToLastDay(LineChart chart){
        float difference = chart.getLineData().getXMax()-chart.getLineData().getXMin();
        float dayes = difference/(24*60*60*1000);

        chart.zoom(dayes, 1, 0, 0); //zoom(float scaleX, float scaleY, float x, float y)
        chart.moveViewToX(chart.getLineData().getXMax() - 24*60*60*1000l);
    }

    public static void resetZoom(LineChart chart){
        chart.fitScreen();
    }
}
