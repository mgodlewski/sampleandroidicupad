package com.icupad.module.ecmo.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.icupad.R;
import com.icupad.commons.ModuleFragment;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.EcmoConfiguration;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.module.ecmo.EcmoDataTimeUtils;
import com.icupad.module.ecmo.adapters.CustomTableFixHeaderAdapter;
import com.inqbarna.tablefixheaders.TableFixHeaders;
import com.inqbarna.tablefixheaders.adapters.BaseTableAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Marcin on 03.06.2017.
 */
public class EcmoHistValuesFragment extends ModuleFragment {
    private Logger LOGGER = Logger.getLogger(this.getClass().toString());

    private final Handler handler = new Handler();

    private IcupadDbHelper mDbHelper;
    private PublicIcupadRepository publicIcupadRepository;

    private Button visualizationButton;
    private Button infoButton;
    private TextView dayTextView;
    private TableFixHeaders table;

    private Long procedureId;
    private boolean active;
    private String startDate;

    private List<EcmoConfiguration> ecmoConfigurations;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_ecmo_hist_values, container, false);

        publicIcupadRepository = fragmentListener.getRepository();
        mDbHelper = new IcupadDbHelper(getActivity().getApplicationContext());

        Bundle args = getArguments();
        procedureId = args.getLong("procadureId");
        active = args.getBoolean("active");
        startDate = args.getString("startDate");

        getAllViewElements(view);

        new Thread(new Runnable(){
            @Override
            public void run() {
                EcmoDataTimeUtils.setDataTextViews(dayTextView, startDate, publicIcupadRepository);
                ecmoConfigurations = publicIcupadRepository.getAllEcmoFullConfigurationsByPorcedureId(procedureId);

                handler.post(new Runnable(){
                    @Override
                    public void run(){
                        addClickListeners();
                        table.setAdapter(getTableAdapter());
                        fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
                    }
                });
            }
            }).start();

        return view;
    }

    private void addClickListeners() {
        visualizationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EcmoVisualizationFragment ecmoVisualizationFragment = new EcmoVisualizationFragment();
                Bundle bundle = new Bundle();
                bundle.putLong("procadureId",procedureId);
                bundle.putBoolean("active", active);
                bundle.putString("startDate",startDate);
                ecmoVisualizationFragment.setArguments(bundle);
                fragmentListener.replaceFragment(ecmoVisualizationFragment);
            }
        });
        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EcmoInfoFragment ecmoInfoFragment = new EcmoInfoFragment();
                Bundle bundle = new Bundle();
                bundle.putLong("procadureId",procedureId);
                bundle.putBoolean("active", active);
                bundle.putString("startDate",startDate);
                ecmoInfoFragment.setArguments(bundle);
                fragmentListener.replaceFragment(ecmoInfoFragment);
            }
        });
    }

    private void getAllViewElements(View view) {
        visualizationButton = (Button) view.findViewById(R.id.visualizationButton);
        infoButton = (Button) view.findViewById(R.id.infoButton);
        dayTextView = (TextView) view.findViewById(R.id.day);
        table = (TableFixHeaders) view.findViewById(R.id.table);
    }

    public BaseTableAdapter getTableAdapter() {
        CustomTableFixHeaderAdapter adapter = new CustomTableFixHeaderAdapter(getActivity());
        List<String> header = new ArrayList<>();
        List<List<String>> body = new ArrayList<>();
        getTableData(header, body);

        adapter.setFirstHeader("");
        adapter.setHeader(header);
        adapter.setFirstBody(body);
        adapter.setBody(body);
        adapter.setSection(body);

        return adapter;
    }

    private void getTableData(List<String> header, List<List<String>> rows) {
        List<String>  modeCols = new ArrayList<>();
        List<String>  caniulationTypeCols = new ArrayList<>();
        List<String>  usedStructuresCols = new ArrayList<>();
        List<String>  wentowanieCols = new ArrayList<>();
        List<String>  veinCannulaCols = new ArrayList<>();
        List<String>  arteryCannulaTypeCols = new ArrayList<>();
        List<String>  arteryCannulaSizeCols = new ArrayList<>();
        List<String>  oxygeneratorSizeCols = new ArrayList<>();

        modeCols.add(getResources().getString(R.string.ecmo_create_new_mode));
        caniulationTypeCols.add(getResources().getString(R.string.ecmo_create_new_caniulation_type));
        usedStructuresCols.add(getResources().getString(R.string.ecmo_create_new_used_structures));
        wentowanieCols.add(getResources().getString(R.string.ecmo_create_new_wentowanie));
        veinCannulaCols.add(getResources().getString(R.string.ecmo_create_new_vein_cannula));
        arteryCannulaTypeCols.add(getResources().getString(R.string.ecmo_create_new_artery_cannula_type));
        arteryCannulaSizeCols.add(getResources().getString(R.string.ecmo_create_new_artery_cannula_size));
        oxygeneratorSizeCols.add(getResources().getString(R.string.ecmo_create_new_oxygenerator));

        for(EcmoConfiguration ecmoConfiguration : ecmoConfigurations){
            header.add(ecmoConfiguration.getDate());
            if(ecmoConfiguration.getMode()!=null) {
                modeCols.add(ecmoConfiguration.getMode());
            }else{
                modeCols.add("-");
            }

            if(ecmoConfiguration.getCaniulationType()!=null) {
                caniulationTypeCols.add(ecmoConfiguration.getCaniulationType());
            }else{
                caniulationTypeCols.add("-");
            }

            if(ecmoConfiguration.getCannulationStructures()!=null) {
                usedStructuresCols.add(ecmoConfiguration.getStringCannulationStructures());
            }else{
                usedStructuresCols.add("-");
            }

            if(ecmoConfiguration.getWentowanieLeftVentricle()!=null) {
                wentowanieCols.add(ecmoConfiguration.getWentowanieLeftVentricle());
            }else{
                wentowanieCols.add("-");
            }

            if(ecmoConfiguration.getVeinCannulaList()!=null) {
                veinCannulaCols.add(ecmoConfiguration.getStringVeinCannulaList());
            }else{
                veinCannulaCols.add("-");
            }

            if(ecmoConfiguration.getArteryCannulaType()!=null) {
                arteryCannulaTypeCols.add(ecmoConfiguration.getArteryCannulaType());
            }else{
                arteryCannulaTypeCols.add("-");
            }

            if(ecmoConfiguration.getArteryCannulaSize()!=null) {
                arteryCannulaSizeCols.add(ecmoConfiguration.getArteryCannulaSize().toString());
            }else{
                arteryCannulaSizeCols.add("-");
            }

            if(ecmoConfiguration.getOxygeneratorType()!=null) {
                oxygeneratorSizeCols.add(ecmoConfiguration.getOxygeneratorType());
            }else{
                oxygeneratorSizeCols.add("-");
            }
        }

        rows.add(modeCols);
        rows.add(caniulationTypeCols);
        rows.add(usedStructuresCols);
        rows.add(wentowanieCols);
        rows.add(veinCannulaCols);
        rows.add(arteryCannulaTypeCols);
        rows.add(arteryCannulaSizeCols);
        rows.add(oxygeneratorSizeCols);
    }
}
