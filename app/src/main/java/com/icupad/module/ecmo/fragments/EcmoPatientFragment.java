package com.icupad.module.ecmo.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.icupad.R;
import com.icupad.bloodgas.populator.HalfDaysComputer;
import com.icupad.commons.ModuleFragment;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.EcmoPatient;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.module.ecmo.EcmoDataTimeUtils;
import com.icupad.module.ecmo.InputFilterMinMax;
import com.icupad.module.ecmo.dialogs.DataTimeFragment;
import com.icupad.module.ecmo.dialogs.DialogCloseListener;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.logging.Logger;

/**
 * Created by Marcin on 21.03.2017.
 */
public class EcmoPatientFragment extends ModuleFragment {

    private Logger LOGGER = Logger.getLogger(this.getClass().toString());

    private PublicIcupadRepository publicIcupadRepository;

    private Button saveButton;
    private Button cancelButton;

    private EditText satInput;
    private EditText vcpInput;
    private EditText abpInput;
    private EditText hrInput;
    private EditText diuresisInput;
    private EditText ultrafiltractionInput;
    private EditText tempInput;
    private EditText tempCorInput;
    private Button dateTimeInput;
    private TextView dayTextView;

    private Long procedureId;
    private String dateTime;
    private boolean active;
    private String startDate;

    public EcmoPatientFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ecmo_patient, container, false);

        publicIcupadRepository = fragmentListener.getRepository();

        getAllViewElements(view);
        setInputFilters();
        addClickListeners();
        setNewDataTime(DateTimeFormatterHelper.getCurrentDateTime());

        Bundle args = getArguments();
        procedureId = args.getLong("procadureId");
        active = args.getBoolean("active");
        startDate = args.getString("startDate");

        EcmoDataTimeUtils.setDataTextViews(dayTextView, startDate, publicIcupadRepository);

        fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
        return view;
    }

    private void addClickListeners() {
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!editTextValidation()){
                    return;
                }

                EcmoPatient ecmoPatient = new EcmoPatient();
                ecmoPatient.setDate(dateTime);
                ecmoPatient.setProcedureId(procedureId);
                ecmoPatient.setCreatedBy(1L);   //TODO set real userId
                ecmoPatient.setAbp(abpInput.getText().toString().isEmpty() ? null : Double.parseDouble((abpInput.getText().toString())));
                ecmoPatient.setDiuresis(diuresisInput.getText().toString().isEmpty() ? null : Double.parseDouble((diuresisInput.getText().toString())));
                ecmoPatient.setHr(hrInput.getText().toString().isEmpty() ? null : Double.parseDouble((hrInput.getText().toString())));
                ecmoPatient.setSat(satInput.getText().toString().isEmpty() ? null : Double.parseDouble((satInput.getText().toString())));
                ecmoPatient.setTempCore(tempCorInput.getText().toString().isEmpty() ? null : Double.parseDouble((tempCorInput.getText().toString())));
                ecmoPatient.setTempSurface(tempInput.getText().toString().isEmpty() ? null : Double.parseDouble((tempInput.getText().toString())));
                ecmoPatient.setUltrafiltraction(ultrafiltractionInput.getText().toString().isEmpty() ? null : Double.parseDouble((ultrafiltractionInput.getText().toString())));
                ecmoPatient.setVcp(vcpInput.getText().toString().isEmpty() ? null : Double.parseDouble((vcpInput.getText().toString())));

                publicIcupadRepository.saveEcmoPatient(ecmoPatient);

                backToVisualisationView();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backToVisualisationView();
            }
        });
        dateTimeInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDataTimeFragment();
            }
        });
    }

    private void backToVisualisationView(){
        EcmoVisualizationFragment ecmoVisualizationFragment = new EcmoVisualizationFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("procadureId",procedureId);
        bundle.putBoolean("active", active);
        bundle.putString("startDate",startDate);
        ecmoVisualizationFragment.setArguments(bundle);

        fragmentListener.replaceFragment(ecmoVisualizationFragment);
    }

    private boolean editTextValidation(){
        Resources resources = getActivity().getApplicationContext().getResources();
        if(abpInput.getText().toString().isEmpty() && diuresisInput.getText().toString().isEmpty() &&
                hrInput.getText().toString().isEmpty() && satInput.getText().toString().isEmpty() &&
                tempCorInput.getText().toString().isEmpty() && tempInput.getText().toString().isEmpty() &&
                ultrafiltractionInput.getText().toString().isEmpty() && vcpInput.getText().toString().isEmpty()){

            abpInput.setError(resources.getString(R.string.empty_input));
            diuresisInput.setError(resources.getString(R.string.empty_input));
            hrInput.setError(resources.getString(R.string.empty_input));
            satInput.setError(resources.getString(R.string.empty_input));
            tempCorInput.setError(resources.getString(R.string.empty_input));
            tempInput.setError(resources.getString(R.string.empty_input));
            ultrafiltractionInput.setError(resources.getString(R.string.empty_input));
            vcpInput.setError(resources.getString(R.string.empty_input));
            return false;
        }
        return true;
    }

    private void showDataTimeFragment(){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        DataTimeFragment dtf = DataTimeFragment.newInstance(dateTime);
        dtf.setDialogCloseListener(new DialogCloseListener(){
            @Override
            public void onDialogClose(String... parameters) {
                setNewDataTime(parameters[0]);
            }
        });
        dtf.show(ft, "dialog");
    }

    private void getAllViewElements(View view) {
        saveButton = (Button) view.findViewById(R.id.ecmoSaveButton);
        cancelButton = (Button) view.findViewById(R.id.ecmoCancelButton);
        satInput = (EditText) view.findViewById(R.id.satInput);
        vcpInput = (EditText) view.findViewById(R.id.vcpInput);
        abpInput = (EditText) view.findViewById(R.id.abpInput);
        hrInput = (EditText) view.findViewById(R.id.hrInput);
        diuresisInput = (EditText) view.findViewById(R.id.diuresisInput);
        ultrafiltractionInput = (EditText) view.findViewById(R.id.ultrafiltractionInput);
        tempInput = (EditText) view.findViewById(R.id.tempInput);
        tempCorInput = (EditText) view.findViewById(R.id.tempCorInput);
        dateTimeInput = (Button) view.findViewById(R.id.dateTimeInput);
        dayTextView = (TextView) view.findViewById(R.id.day);
    }

    private void setInputFilters(){
        abpInput.setFilters(new InputFilter[]{ new InputFilterMinMax("0","200")});
        vcpInput.setFilters(new InputFilter[]{ new InputFilterMinMax("-10","50")});
        hrInput.setFilters(new InputFilter[]{ new InputFilterMinMax("0","300")});
        diuresisInput.setFilters(new InputFilter[]{ new InputFilterMinMax("0","3000")});
        ultrafiltractionInput.setFilters(new InputFilter[]{ new InputFilterMinMax("0","3000")});
    }

    private void setNewDataTime(String dateTime){
        dateTimeInput.setText(dateTime);
        this.dateTime = dateTime;
    }
}
