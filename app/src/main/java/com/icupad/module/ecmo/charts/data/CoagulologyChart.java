package com.icupad.module.ecmo.charts.data;

import android.graphics.Color;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.BloodGasMeasurement;
import com.icupad.commons.repository.model.EcmoAct;
import com.icupad.commons.utils.DateTimeFormatterHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 23.05.2017.
 */
public class CoagulologyChart extends BaseChart  {

    List<EcmoAct> acts;
    List<BloodGasMeasurement> defaultTestMeasurements;

    public CoagulologyChart(Long patientId, PublicIcupadRepository publicIcupadRepository, List<EcmoAct> acts, List<BloodGasMeasurement> defaultTestMeasurements) {
        super(patientId, publicIcupadRepository);
        this.acts = acts;
        this.defaultTestMeasurements = defaultTestMeasurements;
        prepateChartData();
    }

    @Override
    protected LineData prepareChart() {
        List<Entry> actValues = new ArrayList<>();
        List<Entry> apttValues = new ArrayList<>();

        for(EcmoAct act : acts){
            if(act.getAct()!=null){
                actValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(act.getDate()), act.getAct().floatValue()));
            }
        }

        for(BloodGasMeasurement defaultTestMeasurement : defaultTestMeasurements){
            if(defaultTestMeasurement.getName().equals("Czas kaolinowo-kefalinowy")){
                apttValues.add(new Entry(defaultTestMeasurement.getResultDateLong(), defaultTestMeasurement.getValue().floatValue()));
            }
        }

        LineDataSet actSet = new LineDataSet(actValues, "ACT [s]");
        LineDataSet apttSet = new LineDataSet(apttValues, "APTT [s]");

        configurateChart(actSet, Color.RED, YAxis.AxisDependency.LEFT);
        configurateChart(apttSet, Color.BLUE, YAxis.AxisDependency.RIGHT);

        List<ILineDataSet> dataSets = new ArrayList<>();
        if(actSet.getEntryCount()>0){
            dataSets.add(actSet);
        }
        if(apttSet.getEntryCount()>0){
            dataSets.add(apttSet);
        }

        LineData lineData = new LineData(dataSets);
        lineData.setValueFormatter(new DefaultValueFormatter(3));

        return lineData;
    }

    @Override
    protected List<LegendEntry> prepareLegend() {
        List<LegendEntry> legend = new ArrayList<>();
        legend.add(new LegendEntry("ACT [s]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.RED));
        legend.add(new LegendEntry("APTT [s]", Legend.LegendForm.SQUARE, Float.NaN, Float.NaN, null, Color.BLUE));
        return legend;
    }
}
