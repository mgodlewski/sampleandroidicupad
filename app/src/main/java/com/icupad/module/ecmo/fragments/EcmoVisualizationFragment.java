package com.icupad.module.ecmo.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.icupad.R;
import com.icupad.commons.ModuleFragment;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.BloodGasMeasurement;
import com.icupad.commons.repository.model.EcmoAct;
import com.icupad.commons.repository.model.EcmoParameter;
import com.icupad.commons.repository.model.EcmoPatient;
import com.icupad.commons.repository.model.Stay;
import com.icupad.module.ecmo.EcmoDataTimeUtils;
import com.icupad.module.ecmo.adapters.CustomTableFixHeaderAdapter;
import com.icupad.module.ecmo.charts.adapters.EcmoChartPagerAdapter;
import com.icupad.module.ecmo.charts.data.BaseChart;
import com.icupad.module.ecmo.charts.data.BiophysicalChartI;
import com.icupad.module.ecmo.charts.data.BiophysicalChartII;
import com.icupad.module.ecmo.charts.data.CoagulologyChart;
import com.icupad.module.ecmo.charts.data.GasometryChart;
import com.icupad.module.ecmo.charts.data.MorphologyChart;
import com.icupad.module.ecmo.charts.data.OxygenChart;
import com.icupad.module.ecmo.charts.data.OxygeneratorChart;
import com.icupad.module.ecmo.charts.data.PressureChart;
import com.icupad.utils.model.Mode;
import com.inqbarna.tablefixheaders.TableFixHeaders;
import com.inqbarna.tablefixheaders.adapters.BaseTableAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Marcin on 21.03.2017.
 */
public class EcmoVisualizationFragment extends ModuleFragment {

    private Logger LOGGER = Logger.getLogger(this.getClass().toString());

    private PublicIcupadRepository publicIcupadRepository;

    private final Handler handler = new Handler();

    private Button infoButton;
    private Button tablesButton;
    private Button addPatientButton;
    private Button addActButton;
    private Button addParameterButton;

    private TextView dayTextView;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    private Long procedureId;
    private boolean active;
    private Long patientId;
    private String startDate;

    private ArrayList<BaseChart> charts = new ArrayList<>();

    private TableFixHeaders table;

    private Mode mode = Mode.CHART;

    List<EcmoParameter> ecmoParameters;

    public EcmoVisualizationFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ecmo_visualization, container, false);

        publicIcupadRepository = fragmentListener.getRepository();

        Bundle args = getArguments();
        procedureId = args.getLong("procadureId");
        active = args.getBoolean("active");
        startDate = args.getString("startDate");
        patientId = fragmentListener.getChosenPaintId();

        getAllViewElements(view);
        addClickListeners();

        if(!active){
            disableInputs();
        }

        new Thread(new Runnable(){
            @Override
            public void run(){
                EcmoDataTimeUtils.setDataTextViews(dayTextView, startDate, publicIcupadRepository);
                prepareChartData();
                handler.post(new Runnable(){
                    @Override
                    public void run(){
                        tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.machine_chart)));
                        tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.oxygen_chart)));
                        tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.gasometry_chart)));
                        tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.coagulogy_chart)));
                        tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.morphology_chart)));
                        tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.biophysical_chart_I)));
                        tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.biophysical_chart_II)));

                        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

                        final PagerAdapter adapter = new EcmoChartPagerAdapter(getActivity().getFragmentManager(), tabLayout.getTabCount(), charts);
                        viewPager.setAdapter(adapter);
                        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
                        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                            @Override
                            public void onTabSelected(TabLayout.Tab tab) {
                                viewPager.setCurrentItem(tab.getPosition());
                            }

                            @Override
                            public void onTabUnselected(TabLayout.Tab tab) {
                            }

                            @Override
                            public void onTabReselected(TabLayout.Tab tab) {
                            }
                        });

                        table.setAdapter(getTableAdapter());

                        fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
                    }
                });
            }
        }).start();
        return view;
    }

    private void getAllViewElements(View view) {
        infoButton = (Button) view.findViewById(R.id.ecmoInfoButton);
        tablesButton = (Button) view.findViewById(R.id.ecmoTablesButton);
        addPatientButton = (Button) view.findViewById(R.id.ecmoAddPatientButton);
        addActButton = (Button) view.findViewById(R.id.ecmoAddActButton);
        addParameterButton = (Button) view.findViewById(R.id.ecmoAddParameterButton);

        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        viewPager = (ViewPager) view.findViewById(R.id.pager);

        dayTextView = (TextView) view.findViewById(R.id.day);

        table = (TableFixHeaders) view.findViewById(R.id.table);
    }

    private void addClickListeners() {
        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EcmoInfoFragment ecmoInfoFragment = new EcmoInfoFragment();
                Bundle bundle = new Bundle();
                bundle.putLong("procadureId",procedureId);
                bundle.putBoolean("active", active);
                bundle.putString("startDate",startDate);
                ecmoInfoFragment.setArguments(bundle);
                fragmentListener.replaceFragment(ecmoInfoFragment);
            }
        });
        tablesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mode.equals(Mode.CHART)) {
                    tabLayout.setVisibility(View.GONE);
                    viewPager.setVisibility(View.GONE);
                    tablesButton.setText(getActivity().getResources().getString(R.string.charts_button));
                    table.setVisibility(View.VISIBLE);
                    mode= Mode.TABLE;
                }else{
                    tabLayout.setVisibility(View.VISIBLE);
                    viewPager.setVisibility(View.VISIBLE);
                    tablesButton.setText(getActivity().getResources().getString(R.string.tables_button));
                    table.setVisibility(View.GONE);
                    mode=Mode.CHART;
                }
            }
        });
        addPatientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EcmoPatientFragment ecmoPatientFragment = new EcmoPatientFragment();
                Bundle bundle = new Bundle();
                bundle.putLong("procadureId",procedureId);
                bundle.putBoolean("active", active);
                bundle.putString("startDate",startDate);
                ecmoPatientFragment.setArguments(bundle);
                fragmentListener.replaceFragment(ecmoPatientFragment);
            }
        });
        addActButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EcmoActFragment ecmoActFragment = new EcmoActFragment();
                Bundle bundle = new Bundle();
                bundle.putLong("procadureId",procedureId);
                bundle.putBoolean("active", active);
                bundle.putString("startDate",startDate);
                ecmoActFragment.setArguments(bundle);
                fragmentListener.replaceFragment(ecmoActFragment);
            }
        });
        addParameterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EcmoParameterFragment ecmoParameterFragment = new EcmoParameterFragment();
                Bundle bundle = new Bundle();
                bundle.putLong("procadureId",procedureId);
                bundle.putBoolean("active", active);
                bundle.putString("startDate",startDate);
                ecmoParameterFragment.setArguments(bundle);
                fragmentListener.replaceFragment(ecmoParameterFragment);
            }
        });
    }

    private void disableInputs(){
        addPatientButton.setEnabled(false);
        addActButton.setEnabled(false);
        addParameterButton.setEnabled(false);
    }

    private void prepareChartData(){
        List<EcmoPatient> ecmoPatients = publicIcupadRepository.getEcmoPatientByProcedureId(procedureId);   //biophisical I, II
        List<EcmoAct> acts = publicIcupadRepository.getEcmoActByProcedureId(procedureId);   //coagulology
        ecmoParameters = publicIcupadRepository.getEcmoParameterByProcedureId(procedureId); //oxygen, oxygenerator, pressure

        Stay lastStay = publicIcupadRepository.getLastStay();

        List<BloodGasMeasurement> bloodGasMeasurements = publicIcupadRepository.getBloodGasTestMeasurement(lastStay.getId()); //gasometry, oxygen
        List<BloodGasMeasurement> defaultTestMeasurements = publicIcupadRepository.getDefaultMeasurement(lastStay.getId()); //coagulology
        List<BloodGasMeasurement> completeBloodCountMeasurements = publicIcupadRepository.getCompleteBloodCountMeasurement(lastStay.getId());    //morphology

        charts.add(new OxygeneratorChart(patientId, publicIcupadRepository, ecmoParameters));
        charts.add(new PressureChart(patientId, publicIcupadRepository, ecmoParameters));
        charts.add(new OxygenChart(patientId, publicIcupadRepository, bloodGasMeasurements, ecmoParameters));
        charts.add(new GasometryChart(patientId, publicIcupadRepository, bloodGasMeasurements));
        charts.add(new CoagulologyChart(patientId, publicIcupadRepository, acts, defaultTestMeasurements));
        charts.add(new MorphologyChart(patientId, publicIcupadRepository, completeBloodCountMeasurements));
        charts.add(new BiophysicalChartI(patientId, publicIcupadRepository, ecmoPatients));
        charts.add(new BiophysicalChartII(patientId, publicIcupadRepository, ecmoPatients));
    }


    public BaseTableAdapter getTableAdapter() {
        CustomTableFixHeaderAdapter adapter = new CustomTableFixHeaderAdapter(getActivity());
        List<String> header = new ArrayList<>();
        List<List<String>> body = new ArrayList<>();
        getTableData(header, body);

        adapter.setFirstHeader("");
        adapter.setHeader(header);
        adapter.setFirstBody(body);
        adapter.setBody(body);
        adapter.setSection(body);

        return adapter;
    }

    private void getTableData(List<String> header, List<List<String>> rows) {
        List<String>  rpmCols = new ArrayList<>();
        List<String>  bloodFlowCols = new ArrayList<>();
        List<String>  gasFlowCols = new ArrayList<>();
        List<String>  p1Cols = new ArrayList<>();
        List<String>  p2Cols = new ArrayList<>();
        List<String>  p3Cols = new ArrayList<>();
        List<String>  deltaCols = new ArrayList<>();

        rpmCols.add("RPM");
        bloodFlowCols.add("BLOOD FLOW [l/min]");
        gasFlowCols.add("GAS FLOW [l/mim]");
        p1Cols.add("P1 [mmHg]");
        p2Cols.add("P2 [mmHg]");
        p3Cols.add("P3 [mmHg]");
        deltaCols.add("delta [mmHg]");

        for(EcmoParameter ecmoParameter : ecmoParameters){
            header.add(ecmoParameter.getDate());
            if(ecmoParameter.getRpm()!=null) {
                rpmCols.add(ecmoParameter.getRpm().toString());
            }else{
                rpmCols.add("-");
            }

            if(ecmoParameter.getBloodFlow()!=null) {
                bloodFlowCols.add(ecmoParameter.getBloodFlow().toString());
            }else{
                bloodFlowCols.add("-");
            }

            if(ecmoParameter.getGasFlow()!=null) {
                gasFlowCols.add(ecmoParameter.getGasFlow().toString());
            }else{
                gasFlowCols.add("-");
            }

            if(ecmoParameter.getP1()!=null) {
                p1Cols.add(ecmoParameter.getP1().toString());
            }else{
                p1Cols.add("-");
            }

            if(ecmoParameter.getP2()!=null) {
                p2Cols.add(ecmoParameter.getP2().toString());
            }else{
                p2Cols.add("-");
            }

            if(ecmoParameter.getP3()!=null) {
                p3Cols.add(ecmoParameter.getP3().toString());
            }else{
                p3Cols.add("-");
            }

            if(ecmoParameter.getDelta()!=null) {
                deltaCols.add(ecmoParameter.getDelta().toString());
            }else{
                deltaCols.add("-");
            }
        }
        rows.add(rpmCols);
        rows.add(bloodFlowCols);
        rows.add(gasFlowCols);
        rows.add(p1Cols);
        rows.add(p2Cols);
        rows.add(p3Cols);
        rows.add(deltaCols);
    }

}
