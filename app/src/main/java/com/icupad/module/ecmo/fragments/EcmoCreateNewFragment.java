package com.icupad.module.ecmo.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.icupad.R;
import com.icupad.commons.ModuleFragment;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.EcmoConfiguration;
import com.icupad.commons.repository.model.EcmoProcedure;
import com.icupad.commons.repository.model.VeinCannula;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.module.ecmo.EcmoDataTimeUtils;
import com.icupad.module.ecmo.dialogs.DataTimeFragment;
import com.icupad.module.ecmo.dialogs.DialogCloseListener;
import com.icupad.module.ecmo.dialogs.DialogVeinCannulaFragment;
import com.icupad.module.ecmo.dialogs.MultipleChoiceFragment;
import com.icupad.module.patientchooser.PatientChooserFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;

/**
 * Created by Marcin on 20.03.2017.
 */
public class EcmoCreateNewFragment extends ModuleFragment{

    private Logger LOGGER = Logger.getLogger(this.getClass().toString());

    private PublicIcupadRepository publicIcupadRepository;

    private Spinner causeOfStartInput;
    private Spinner modeInput;
    private Spinner canniulationTypeInput;
    private EditText usedStructuresInput;
    private Spinner wentowanieLeftVerticleInput;
    private EditText veinCannulaInput;
    private EditText arteryCannulaTypeInput;
    private EditText arteryCannulaSizeInput;
    private EditText oxygeneratorTypeInput;
    private Button dateTimeInputButton;
    private Button startButton;

    private String dateTime;
    private String selectedItems;
    private String veinCannulas;

    public EcmoCreateNewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ecmo_create_new, container, false);

        publicIcupadRepository = fragmentListener.getRepository();

        getAllViewElements(view);
        setAdapters();
        selectedItems = "";
        veinCannulas = "";

        setNewDataTime(DateTimeFormatterHelper.getCurrentDateTime());

        addClickListeners();

        fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
        return view;
    }

    private void getAllViewElements(View view){
        arteryCannulaTypeInput = (EditText) view.findViewById(R.id.arteryCannulaTypeInput);
        arteryCannulaSizeInput = (EditText) view.findViewById(R.id.arteryCannulaSizeInput);
        oxygeneratorTypeInput = (EditText) view.findViewById(R.id.oxygeneratorTypeInput);
        dateTimeInputButton = (Button) view.findViewById(R.id.dateTimeInput);
        startButton = (Button) view.findViewById(R.id.startEcmoButton);
        causeOfStartInput = (Spinner) view.findViewById(R.id.causeOfStartInput);
        modeInput =(Spinner) view.findViewById(R.id.modeInput);
        canniulationTypeInput =(Spinner) view.findViewById(R.id.caniulationTypeInput);
        wentowanieLeftVerticleInput = (Spinner) view.findViewById(R.id.wentowanieLeftVerticleInput);
        usedStructuresInput = (EditText) view.findViewById(R.id.usedStructuresInput);
        veinCannulaInput = (EditText) view.findViewById(R.id.veinCannulaInput);
    }

    private void setAdapters(){
        ArrayAdapter<CharSequence> causeOfStartAdapter = ArrayAdapter.createFromResource(this.getActivity(),R.array.ecmo_start_causes, R.layout.spinner_text_view);
        causeOfStartAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        causeOfStartInput.setAdapter(causeOfStartAdapter);

        ArrayAdapter<CharSequence> modeInputAdapter = ArrayAdapter.createFromResource(this.getActivity(),R.array.ecmo_mode, R.layout.spinner_text_view);
        modeInputAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        modeInput.setAdapter(modeInputAdapter);

        ArrayAdapter<CharSequence> canniulationTypeInputAdapter = ArrayAdapter.createFromResource(this.getActivity(),R.array.caniulation_type, R.layout.spinner_text_view);
        canniulationTypeInputAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        canniulationTypeInput.setAdapter(canniulationTypeInputAdapter);

        ArrayAdapter<CharSequence> wentowanieLeftVerticleInputAdapter = ArrayAdapter.createFromResource(this.getActivity(),R.array.wentowanie_left_verticle, R.layout.spinner_text_view);
        wentowanieLeftVerticleInputAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        wentowanieLeftVerticleInput.setAdapter(wentowanieLeftVerticleInputAdapter);
    }

    private void addClickListeners(){
        dateTimeInputButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                DataTimeFragment dtf = DataTimeFragment.newInstance(dateTime);
                dtf.setDialogCloseListener(new DialogCloseListener(){
                    @Override
                    public void onDialogClose(String... parameters) {
                        setNewDataTime(parameters[0]);
                    }
                });
                dtf.show(ft, "dialog");
            }
        });

        veinCannulaInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                DialogVeinCannulaFragment dvcf = DialogVeinCannulaFragment.newInstance(veinCannulas);
                dvcf.setDialogCloseListener(new DialogCloseListener() {
                    @Override
                    public void onDialogClose(String... parameters) {
                        veinCannulas = "";
                        for (String s : parameters){
                            if(s.split(":").length>0){
                                veinCannulas += s + "; ";
                            }
                        }
                        veinCannulas = veinCannulas.length()>2 ? veinCannulas.substring(0, veinCannulas.length()-2) : veinCannulas;
                        veinCannulaInput.setText(veinCannulas);
                    }
                });
                dvcf.show(ft, "dialog");
            }
        });

        usedStructuresInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Resources resources = getActivity().getApplicationContext().getResources();

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                MultipleChoiceFragment mtf = MultipleChoiceFragment.newInstance(
                        resources.getString(R.string.multiple_choice_used_structures), resources.getStringArray(R.array.used_structures), selectedItems);
                mtf.setDialogCloseListener(new DialogCloseListener(){
                    @Override
                    public void onDialogClose(String... parameter) {
                        selectedItems = "";
                        usedStructuresInput.setText(new String());
                        for(String s : parameter){
                            if(!s.isEmpty()) {
                                selectedItems += s + ", ";
                            }
                        }
                        selectedItems = selectedItems.substring(0, selectedItems.length()-2);
                        usedStructuresInput.setText(selectedItems);
                    }
                });
                mtf.show(ft, "dialog");
            }
        });

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!editTextValidation()){
                    return;
                }

                EcmoProcedure ecmoProcedure = new EcmoProcedure();
                ecmoProcedure.setCreatedById(1L);    //TODO set real userId
                ecmoProcedure.setStartDate(dateTime);
                ecmoProcedure.setStartCause(causeOfStartInput.getSelectedItem().toString());
                ecmoProcedure.setStayId(publicIcupadRepository.getStayId());

                Long procedureId = publicIcupadRepository.saveEcmoProcedure(ecmoProcedure);

                List<VeinCannula> veinCannula = new ArrayList<>();
                String[] veinCannulaTab = veinCannulaInput.getText().toString().split("; ");
                for (String s : veinCannulaTab){
                    String[] sSplit = s.split(":");
                    if(sSplit.length>0) {
                        veinCannula.add(new VeinCannula(sSplit[0], Double.parseDouble(sSplit[1])));
                    }
                }

                EcmoConfiguration ecmoConfiguration = new EcmoConfiguration();
                ecmoConfiguration.setProcedureId(procedureId);
                ecmoConfiguration.setCreatedBy(1L);  //TODO set real userId
                ecmoConfiguration.setMode(modeInput.getSelectedItem().toString());
                ecmoConfiguration.setCaniulationType(canniulationTypeInput.getSelectedItem().toString());
                ecmoConfiguration.setWentowanieLeftVentricle(wentowanieLeftVerticleInput.getSelectedItem().toString());
                ecmoConfiguration.setActual(true);
                ecmoConfiguration.setDate(dateTime);

                ecmoConfiguration.setOxygeneratorType(oxygeneratorTypeInput.getText().toString());
                ecmoConfiguration.setCannulationStructures(new ArrayList<>(Arrays.asList(usedStructuresInput.getText().toString().split(", "))));
                ecmoConfiguration.setVeinCannulaList(veinCannula);

                if(arteryCannulaTypeInput.getText().toString().isEmpty()){
                    ecmoConfiguration.setArteryCannulaType("");
                }else {
                    ecmoConfiguration.setArteryCannulaType(arteryCannulaTypeInput.getText().toString());
                }
                if(arteryCannulaSizeInput.getText().toString().isEmpty()){
                    ecmoConfiguration.setArteryCannulaSize(null);
                }else {
                    ecmoConfiguration.setArteryCannulaSize(Double.parseDouble(arteryCannulaSizeInput.getText().toString()));
                }

                publicIcupadRepository.setActualFalseInEcmoConfigurationsByProcedureId(procedureId);
                publicIcupadRepository.saveEcmoFullConfiguration(ecmoConfiguration);

                EcmoVisualizationFragment ecmoVisualizationFragment = new EcmoVisualizationFragment();
                Bundle bundle = new Bundle();
                bundle.putLong("procadureId",procedureId);
                bundle.putBoolean("active", true);
                bundle.putString("startDate", dateTime);
                ecmoVisualizationFragment.setArguments(bundle);

                fragmentListener.replaceFragment(ecmoVisualizationFragment);
            }
        });
    }

    private boolean editTextValidation(){
        Resources resources = getActivity().getApplicationContext().getResources();
        if(oxygeneratorTypeInput.getText().toString().isEmpty()){
            oxygeneratorTypeInput.setError(resources.getString(R.string.empty_input));
            return false;
        }
        if(usedStructuresInput.getText().toString().isEmpty()){
            usedStructuresInput.setError(resources.getString(R.string.empty_input));
            return false;
        }
        if(veinCannulaInput.getText().toString().isEmpty()){
            veinCannulaInput.setError(resources.getString(R.string.empty_input));
            return false;
        }
        if(!arteryCannulaTypeInput.getText().toString().isEmpty() && arteryCannulaSizeInput.getText().toString().isEmpty()){
            arteryCannulaSizeInput.setError(resources.getString(R.string.empty_input));
            return false;
        }
        if(arteryCannulaTypeInput.getText().toString().isEmpty() && !arteryCannulaSizeInput.getText().toString().isEmpty()){
            arteryCannulaTypeInput.setError(resources.getString(R.string.empty_input));
            return false;
        }
        return true;
    }

    private void setNewDataTime(String dateTime){
        dateTimeInputButton.setText(dateTime);
        this.dateTime = dateTime;
    }

}
