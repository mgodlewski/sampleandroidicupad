package com.icupad.module.ecmo;

import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.icupad.R;
import com.icupad.commons.utils.DateTimeFormatterHelper;

/**
 * Created by Marcin on 24.04.2017.
 */
public class ChartMarkerView extends MarkerView {

    private TextView tvContent;

    public ChartMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);

        tvContent = (TextView) findViewById(R.id.txtValue);
    }

    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        tvContent.setText("" + DateTimeFormatterHelper.getStringWithoutYearAndSecondFromMillis((long)e.getX()) + " : " +  e.getY());
        super.refreshContent(e, highlight);
    }

    private MPPointF mOffset;

    @Override
    public MPPointF getOffset() {
        if(mOffset == null) {
            mOffset = new MPPointF(-(getWidth() / 2), -getHeight());
        }

        return mOffset;
    }

}
