package com.icupad.module.ecmo;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.icupad.R;

import java.util.List;

import miguelbcr.ui.tableFixHeadesWrapper.TableFixHeaderAdapter;

/**
 * Created by Marcin on 03.06.2017.
 */
public class CellViewGroup extends FrameLayout
        implements
        TableFixHeaderAdapter.FirstHeaderBinder<String>,
        TableFixHeaderAdapter.HeaderBinder<String>,
        TableFixHeaderAdapter.FirstBodyBinder<List<String>>,
        TableFixHeaderAdapter.BodyBinder<List<String>>,
        TableFixHeaderAdapter.SectionBinder<List<String>> {

    public TextView textView;
    public View vg_root;

    public CellViewGroup(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.cell_text_view, this, true);
        textView = (TextView) findViewById(R.id.tv_text);
        vg_root = findViewById(R.id.vg_root);
    }


    @Override
    public void bindFirstHeader(String headerName) {
        textView.setText(headerName);
        vg_root.setBackgroundResource(R.drawable.cell_gray);
    }

    @Override
    public void bindHeader(String headerName, int column) {
        textView.setText(headerName);
        textView.setGravity(Gravity.CENTER);
        textView.setTypeface(null, Typeface.BOLD);
        vg_root.setBackgroundResource(R.drawable.cell_gray);
    }

    @Override
    public void bindFirstBody(List<String> items, int row) {
        textView.setText(items.get(0));
        textView.setGravity(Gravity.LEFT);
        textView.setTypeface(null, Typeface.BOLD);
        vg_root.setBackgroundResource(row % 2 == 0 ? R.drawable.cell_white : R.drawable.cell_gray);
    }

    @Override
    public void bindBody(List<String> items, int row, int column) {
        textView.setText(items.get(column + 1));
        textView.setGravity(Gravity.CENTER);
        if(row % 2 == 0){
            vg_root.setBackgroundResource(R.drawable.cell_white);
//            vg_root.setBackgroundResource(R.drawable.cell_blue_border_white);
//            vg_root.setBackgroundResource(R.drawable.cell_red_border_white);
        }else{
            vg_root.setBackgroundResource(R.drawable.cell_gray);
//            vg_root.setBackgroundResource(R.drawable.cell_blue_border_gray);
//            vg_root.setBackgroundResource(R.drawable.cell_red_border_gray);
        }
    }

    @Override
    public void bindSection(List<String> item, int row, int column) {
    }
}