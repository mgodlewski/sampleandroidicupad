package com.icupad.module.ecmo.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.icupad.R;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.module.ecmo.EcmoDates;

import java.util.List;

/**
 * Created by Marcin on 19.03.2017.
 */
public class EcmoHistoryAdapter extends BaseAdapter {

    public List<EcmoDates> dates;
    Context context;

    public EcmoHistoryAdapter(Context context, List<EcmoDates> dates) {
        super();
        this.context = context;
        this.dates = dates;
    }

    @Override
    public int getCount() {
        return dates.size();
    }

    @Override
    public Object getItem(int i) {
        return dates.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(view == null){
            view = inflater.inflate(R.layout.row_ecmo_history, null);
            holder = new ViewHolder();
            holder.startDate = (TextView) view.findViewById(R.id.startDate);
            holder.endDate = (TextView) view.findViewById(R.id.endDate);
            view.setTag(holder);
        }else{
            holder = (ViewHolder) view.getTag();
        }

        holder.startDate.setText(dates.get(i).getStartDate());
        holder.endDate.setText(dates.get(i).getEndDate());

        return view;
    }

    private class ViewHolder{
        TextView startDate;
        TextView endDate;
    }

}
