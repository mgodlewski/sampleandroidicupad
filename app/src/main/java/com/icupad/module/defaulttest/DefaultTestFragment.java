package com.icupad.module.defaulttest;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.common.primitives.Ints;
import com.icupad.R;
import com.icupad.commons.ModuleFragment;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.BloodGasMeasurement;
import com.icupad.commons.repository.model.Stay;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.module.defaulttest.adapters.ChartPagerAdapter;
import com.icupad.module.ecmo.charts.data.BaseChart;
import com.icupad.module.patientchooser.PatientChooserFragment;
import com.icupad.tableView.CellData;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Marcin on 13.06.2017.
 */
public class DefaultTestFragment extends ModuleFragment {

    private final Handler handler = new Handler();

    private PublicIcupadRepository publicIcupadRepository;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ArrayList<LineData> charts = new ArrayList<>();
    private TextView informationTextView;
    private Long patientId;

    private List<BloodGasMeasurement> testMeasurements;
    private ArrayList<String> header = new ArrayList<>();
    private ArrayList<List<CellData>> body = new ArrayList<>();

    List<ILineDataSet> inflammatorySets = new ArrayList<>();
    List<ILineDataSet> kindneySets = new ArrayList<>();
    List<ILineDataSet> liverSets = new ArrayList<>();
    List<ILineDataSet> heartsSets = new ArrayList<>();
    List<ILineDataSet> antibodiesSets = new ArrayList<>();
    List<ILineDataSet> coagulogySets = new ArrayList<>();
    List<ILineDataSet> urineSets = new ArrayList<>();
    List<ILineDataSet> proteinsSets = new ArrayList<>();
    List<ILineDataSet> macroelementsSets = new ArrayList<>();
    List<ILineDataSet> drugsSets = new ArrayList<>();
    List<ILineDataSet> bodyfluidsSets = new ArrayList<>();
    List<ILineDataSet> hormonsSets = new ArrayList<>();

    public DefaultTestFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_default_test, container, false);

        publicIcupadRepository = fragmentListener.getRepository();

        patientId = fragmentListener.getChosenPaintId();

        getAllViewElements(view);

        if(PatientChooserFragment.getLastChosenPatientId() != null) {
            informationTextView.setVisibility(View.GONE);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    prepareChartData();
                    getTableData(header, body);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.inflammatory_chart)));
                            tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.kidney_chart)));
                            tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.liver_chart)));
                            tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.heart_chart)));
                            tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.antibodies_chart)));
                            tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.coagulogy_def_chart)));
                            tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.urine_chart)));
                            tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.proteins_chart)));
                            tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.macroelements_chart)));
                            tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.drugs_chart)));
                            tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.bodyfluids_chart)));
                            tabLayout.addTab(tabLayout.newTab().setText(getActivity().getResources().getString(R.string.hormones_chart)));

                            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

                            final PagerAdapter adapter = new ChartPagerAdapter(getActivity().getFragmentManager(), tabLayout.getTabCount(), charts, header, body);
                            viewPager.setAdapter(adapter);
                            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
                            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                                @Override
                                public void onTabSelected(TabLayout.Tab tab) {
                                    viewPager.setCurrentItem(tab.getPosition());
                                }

                                @Override
                                public void onTabUnselected(TabLayout.Tab tab) {
                                }

                                @Override
                                public void onTabReselected(TabLayout.Tab tab) {
                                }
                            });

                            fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
                        }
                    });
                }
            }).start();
        } else {
            informationTextView.setText(getResources().getString(com.icupad.androidcommons.R.string.no_chosen_patient));
            informationTextView.setVisibility(View.VISIBLE);
            fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
        }
        return view;
    }

    protected void getAllViewElements(View view) {
        informationTextView = (TextView) view.findViewById(R.id.informationTextView);
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        viewPager = (ViewPager) view.findViewById(R.id.pager);
    }

    protected void getTableData(List<String> header, List<List<CellData>> rows) {
        Map<Long, CellData> bialkoCols = new LinkedHashMap();
        Map<Long, CellData> kreatyninaCols = new LinkedHashMap();
        Map<Long, CellData> mocznikCols = new LinkedHashMap();
        Map<Long, CellData> magnezCols = new LinkedHashMap();
        Map<Long, CellData> antytrombinaCols = new LinkedHashMap();
        Map<Long, CellData> fibrynogenCols = new LinkedHashMap();
        Map<Long, CellData> wskaznikProtrombinowyCols = new LinkedHashMap();
        Map<Long, CellData> inrCols = new LinkedHashMap();
        Map<Long, CellData> czasProtrombinowyCols = new LinkedHashMap();
        Map<Long, CellData> apttCols = new LinkedHashMap();

        Map<Long, CellData> ddimerCols = new LinkedHashMap();
        Map<Long, CellData> alATCols = new LinkedHashMap();
        Map<Long, CellData> aspAtCols = new LinkedHashMap();
        Map<Long, CellData> bilurbinaCalkowitaCols = new LinkedHashMap();
        Map<Long, CellData> bialkoCalkowiteCols = new LinkedHashMap();
        Map<Long, CellData> antyHbsCols = new LinkedHashMap();
        Map<Long, CellData> antyHcvCols = new LinkedHashMap();
        Map<Long, CellData> albuminaCols = new LinkedHashMap();
        Map<Long, CellData> fosforanyCols = new LinkedHashMap();
        Map<Long, CellData> moczCiezarCols = new LinkedHashMap();

        Map<Long, CellData> moczUrobilinogenCols = new LinkedHashMap();
        Map<Long, CellData> moczOdczynCols = new LinkedHashMap();
        Map<Long, CellData> wankomycynaCols = new LinkedHashMap();
        Map<Long, CellData> prokalcytoninaCols = new LinkedHashMap();
        Map<Long, CellData> triglicerydyCols = new LinkedHashMap();
        Map<Long, CellData> wapnCalkowitCols = new LinkedHashMap();
        Map<Long, CellData> troponinaCols = new LinkedHashMap();
        Map<Long, CellData> obCols = new LinkedHashMap();
        Map<Long, CellData> hbsCols = new LinkedHashMap();
        Map<Long, CellData> plynZJamTriglicerydyCols = new LinkedHashMap();

        Map<Long, CellData> plynZJamGlukozaCols = new LinkedHashMap();
        Map<Long, CellData> ggtpCols = new LinkedHashMap();
        Map<Long, CellData> kwasWalproinowyCols = new LinkedHashMap();
        Map<Long, CellData> plynZJamCiezarCols = new LinkedHashMap();
        Map<Long, CellData> plynZJamOgolneCols = new LinkedHashMap();
        Map<Long, CellData> plynZJamBialkoCols = new LinkedHashMap();
        Map<Long, CellData> trojjododotyroninaCols = new LinkedHashMap();
        Map<Long, CellData> tyroksynaCols = new LinkedHashMap();
        Map<Long, CellData> tyreotropCols = new LinkedHashMap();
        Map<Long, CellData> bilurbinaBezposredniaCols = new LinkedHashMap();

        Map<Long, CellData> klinezaKreatynowaCols = new LinkedHashMap();
        Map<Long, CellData> plynZJamErytrocytyCols = new LinkedHashMap();
        Map<Long, CellData> plynZJamLeukocytyCols = new LinkedHashMap();
        Map<Long, CellData> immunoglobulinaGCols = new LinkedHashMap();
        Map<Long, CellData> antytryosynaCols = new LinkedHashMap();
        Map<Long, CellData> immunoglobulinaACols = new LinkedHashMap();
        Map<Long, CellData> immunoglobulinaMCols = new LinkedHashMap();

        bialkoCols.put(0L, new CellData(getResources().getString(R.string.bialko)));
        kreatyninaCols.put(0L, new CellData(getResources().getString(R.string.kreatynina)));
        mocznikCols.put(0L, new CellData(getResources().getString(R.string.mocznik)));
        magnezCols.put(0L, new CellData(getResources().getString(R.string.magnez)));
        antytrombinaCols.put(0L, new CellData(getResources().getString(R.string.antytrombina)));
        fibrynogenCols.put(0L, new CellData(getResources().getString(R.string.fibrynogen)));
        wskaznikProtrombinowyCols.put(0L, new CellData(getResources().getString(R.string.wskaznikProtrombinowy)));
        inrCols.put(0L, new CellData(getResources().getString(R.string.inr)));
        czasProtrombinowyCols.put(0L, new CellData(getResources().getString(R.string.czasProtrombinowy)));
        apttCols.put(0L, new CellData(getResources().getString(R.string.aptt)));

        ddimerCols.put(0L, new CellData(getResources().getString(R.string.ddimer)));
        alATCols.put(0L, new CellData(getResources().getString(R.string.alat)));
        aspAtCols.put(0L, new CellData(getResources().getString(R.string.aspAt)));
        bilurbinaCalkowitaCols.put(0L, new CellData(getResources().getString(R.string.bilurbinaCalkowita)));
        bialkoCalkowiteCols.put(0L, new CellData(getResources().getString(R.string.bialkoCalkowite)));
        antyHbsCols.put(0L, new CellData(getResources().getString(R.string.antyHbs)));
        antyHcvCols.put(0L, new CellData(getResources().getString(R.string.antyHcv)));
        albuminaCols.put(0L, new CellData(getResources().getString(R.string.albumina)));
        fosforanyCols.put(0L, new CellData(getResources().getString(R.string.fosforany)));
        moczCiezarCols.put(0L, new CellData(getResources().getString(R.string.moczCiezar)));

        moczUrobilinogenCols.put(0L, new CellData(getResources().getString(R.string.moczUrobilinogen)));
        moczOdczynCols.put(0L, new CellData(getResources().getString(R.string.moczOdczyn)));
        wankomycynaCols.put(0L, new CellData(getResources().getString(R.string.wankomycyna)));
        prokalcytoninaCols.put(0L, new CellData(getResources().getString(R.string.prokalcytonina)));
        triglicerydyCols.put(0L, new CellData(getResources().getString(R.string.triglicerydy)));
        wapnCalkowitCols.put(0L, new CellData(getResources().getString(R.string.wapnCalkowity)));
        troponinaCols.put(0L, new CellData(getResources().getString(R.string.troponina)));
        obCols.put(0L, new CellData(getResources().getString(R.string.ob)));
        hbsCols.put(0L, new CellData(getResources().getString(R.string.hbs)));
        plynZJamTriglicerydyCols.put(0L, new CellData(getResources().getString(R.string.plynZJamTriglicerydy)));

        plynZJamGlukozaCols.put(0L, new CellData(getResources().getString(R.string.plynZJamGlukoza)));
        ggtpCols.put(0L, new CellData(getResources().getString(R.string.ggtp)));
        kwasWalproinowyCols.put(0L, new CellData(getResources().getString(R.string.kwasWalproinowy)));
        plynZJamCiezarCols.put(0L, new CellData(getResources().getString(R.string.plynZJamCiezar)));
        plynZJamOgolneCols.put(0L, new CellData(getResources().getString(R.string.plynZJamOgolne)));
        plynZJamBialkoCols.put(0L, new CellData(getResources().getString(R.string.plynZJamBialko)));
        trojjododotyroninaCols.put(0L, new CellData(getResources().getString(R.string.trojjododotyronina)));
        tyroksynaCols.put(0L, new CellData(getResources().getString(R.string.tyroksyna)));
        tyreotropCols.put(0L, new CellData(getResources().getString(R.string.tyreotrop)));
        bilurbinaBezposredniaCols.put(0L, new CellData(getResources().getString(R.string.kinezaKreatynowa)));

        klinezaKreatynowaCols.put(0L, new CellData(getResources().getString(R.string.kinezaKreatynowa)));
        plynZJamErytrocytyCols.put(0L, new CellData(getResources().getString(R.string.plynZJamErytrocyty)));
        plynZJamLeukocytyCols.put(0L, new CellData(getResources().getString(R.string.plynZJamLeukocyty)));
        immunoglobulinaGCols.put(0L, new CellData(getResources().getString(R.string.immunoglobulinaG)));
        antytryosynaCols.put(0L, new CellData(getResources().getString(R.string.antytryosyna)));
        immunoglobulinaACols.put(0L, new CellData(getResources().getString(R.string.immunoglobulinaA)));
        immunoglobulinaMCols.put(0L, new CellData(getResources().getString(R.string.immunoglobulinaM)));

        for (BloodGasMeasurement defaultTestMeasurement : testMeasurements) {
            if (!header.contains(DateTimeFormatterHelper.getStringWithoutSecondFromMillisInTwoLine(defaultTestMeasurement.getResultDateLong()))) {
                header.add(DateTimeFormatterHelper.getStringWithoutSecondFromMillisInTwoLine(defaultTestMeasurement.getResultDateLong()));
                bialkoCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                kreatyninaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                mocznikCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                magnezCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                antytrombinaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                fibrynogenCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                wskaznikProtrombinowyCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                inrCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                czasProtrombinowyCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                apttCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));

                ddimerCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                alATCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                aspAtCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                bilurbinaCalkowitaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                bialkoCalkowiteCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                antyHbsCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                antyHcvCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                albuminaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                fosforanyCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                moczCiezarCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));

                moczUrobilinogenCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                moczOdczynCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                wankomycynaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                prokalcytoninaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                triglicerydyCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                wapnCalkowitCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                troponinaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                obCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                hbsCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                plynZJamTriglicerydyCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));

                plynZJamGlukozaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                ggtpCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                kwasWalproinowyCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                plynZJamCiezarCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                plynZJamOgolneCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                plynZJamBialkoCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                trojjododotyroninaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                tyroksynaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                tyreotropCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                bilurbinaBezposredniaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));

                klinezaKreatynowaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                plynZJamErytrocytyCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                plynZJamLeukocytyCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                immunoglobulinaGCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                antytryosynaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                immunoglobulinaACols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
                immunoglobulinaMCols.put(defaultTestMeasurement.getResultDateLong(), new CellData("-"));
            }
        }

        for (BloodGasMeasurement defaultTestMeasurement : testMeasurements) {
            switch (Ints.checkedCast(defaultTestMeasurement.getTestId())) {
                case 1:
                    bialkoCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 2:
                    kreatyninaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 3:
                    mocznikCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 4:
                    magnezCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 5:
                    antytrombinaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 6:
                    fibrynogenCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 7:
                    wskaznikProtrombinowyCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 8:
                    inrCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 9:
                    czasProtrombinowyCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 10:
                    apttCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 11:
                    ddimerCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 12:
                    alATCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 13:
                    aspAtCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 14:
                    bilurbinaCalkowitaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 15:
                    bialkoCalkowiteCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 16:
                    antyHbsCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 17:
                    antyHcvCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 18:
                    albuminaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 19:
                    fosforanyCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 20:
                    moczCiezarCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 21:
                    moczUrobilinogenCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 22:
                    moczOdczynCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 23:
                    wankomycynaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 24:
                    prokalcytoninaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 25:
                    triglicerydyCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 26:
                    wapnCalkowitCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 27:
                    troponinaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 28:
                    obCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 29:
                    hbsCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 30:
                    plynZJamTriglicerydyCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 31:
                    plynZJamGlukozaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 32:
                    ggtpCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 33:
                    kwasWalproinowyCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 34:
                    plynZJamCiezarCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 35:
                    plynZJamOgolneCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 36:
                    plynZJamBialkoCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 37:
                    trojjododotyroninaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 38:
                    tyroksynaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 39:
                    tyreotropCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 40:
                    bilurbinaBezposredniaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 41:
                    klinezaKreatynowaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 42:
                    plynZJamErytrocytyCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 43:
                    plynZJamLeukocytyCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 44:
                    immunoglobulinaGCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 45:
                    antytryosynaCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 46:
                    immunoglobulinaACols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
                case 47:
                    immunoglobulinaMCols.put(defaultTestMeasurement.getResultDateLong(), new CellData(defaultTestMeasurement.getValue(), defaultTestMeasurement.getAbnormality()));
                    break;
            }
        }

        rows.add(new ArrayList<>(bialkoCols.values()));
        rows.add(new ArrayList<>(kreatyninaCols.values()));
        rows.add(new ArrayList<>(mocznikCols.values()));
        rows.add(new ArrayList<>(magnezCols.values()));
        rows.add(new ArrayList<>(antytrombinaCols.values()));
        rows.add(new ArrayList<>(fibrynogenCols.values()));
        rows.add(new ArrayList<>(wskaznikProtrombinowyCols.values()));
        rows.add(new ArrayList<>(inrCols.values()));
        rows.add(new ArrayList<>(czasProtrombinowyCols.values()));
        rows.add(new ArrayList<>(apttCols.values()));

        rows.add(new ArrayList<>(ddimerCols.values()));
        rows.add(new ArrayList<>(alATCols.values()));
        rows.add(new ArrayList<>(aspAtCols.values()));
        rows.add(new ArrayList<>(bilurbinaCalkowitaCols.values()));
        rows.add(new ArrayList<>(bialkoCalkowiteCols.values()));
        rows.add(new ArrayList<>(antyHbsCols.values()));
        rows.add(new ArrayList<>(antyHcvCols.values()));
        rows.add(new ArrayList<>(albuminaCols.values()));
        rows.add(new ArrayList<>(fosforanyCols.values()));
        rows.add(new ArrayList<>(moczCiezarCols.values()));

        rows.add(new ArrayList<>(moczUrobilinogenCols.values()));
        rows.add(new ArrayList<>(moczOdczynCols.values()));
        rows.add(new ArrayList<>(wankomycynaCols.values()));
        rows.add(new ArrayList<>(prokalcytoninaCols.values()));
        rows.add(new ArrayList<>(triglicerydyCols.values()));
        rows.add(new ArrayList<>(wapnCalkowitCols.values()));
        rows.add(new ArrayList<>(troponinaCols.values()));
        rows.add(new ArrayList<>(obCols.values()));
        rows.add(new ArrayList<>(hbsCols.values()));
        rows.add(new ArrayList<>(plynZJamTriglicerydyCols.values()));

        rows.add(new ArrayList<>(plynZJamGlukozaCols.values()));
        rows.add(new ArrayList<>(ggtpCols.values()));
        rows.add(new ArrayList<>(kwasWalproinowyCols.values()));
        rows.add(new ArrayList<>(plynZJamCiezarCols.values()));
        rows.add(new ArrayList<>(plynZJamOgolneCols.values()));
        rows.add(new ArrayList<>(plynZJamBialkoCols.values()));
        rows.add(new ArrayList<>(trojjododotyroninaCols.values()));
        rows.add(new ArrayList<>(tyroksynaCols.values()));
        rows.add(new ArrayList<>(tyreotropCols.values()));
        rows.add(new ArrayList<>(bilurbinaBezposredniaCols.values()));

        rows.add(new ArrayList<>(klinezaKreatynowaCols.values()));
        rows.add(new ArrayList<>(plynZJamErytrocytyCols.values()));
        rows.add(new ArrayList<>(plynZJamLeukocytyCols.values()));
        rows.add(new ArrayList<>(immunoglobulinaGCols.values()));
        rows.add(new ArrayList<>(antytryosynaCols.values()));
        rows.add(new ArrayList<>(immunoglobulinaACols.values()));
        rows.add(new ArrayList<>(immunoglobulinaMCols.values()));
    }

    private void prepareChartData() {
        Stay lastStay = publicIcupadRepository.getLastStay();
        testMeasurements = publicIcupadRepository.getDefaultMeasurement(lastStay.getId());
        getChartData(testMeasurements);

        charts.add(new LineData(inflammatorySets));
        charts.add(new LineData(kindneySets));
        charts.add(new LineData(liverSets));
        charts.add(new LineData(heartsSets));
        charts.add(new LineData(antibodiesSets));
        charts.add(new LineData(coagulogySets));
        charts.add(new LineData(urineSets));
        charts.add(new LineData(proteinsSets));
        charts.add(new LineData(macroelementsSets));
        charts.add(new LineData(drugsSets));
        charts.add(new LineData(bodyfluidsSets));
        charts.add(new LineData(hormonsSets));
    }

    private void getChartData(List<BloodGasMeasurement> testMeasurements){
          List<Entry> bialkoValues = new ArrayList<>();
          List<Entry> kreatyninaValues = new ArrayList<>();
          List<Entry> mocznikValues = new ArrayList<>();
          List<Entry> magnezValues = new ArrayList<>();
          List<Entry> antytrombinaValues = new ArrayList<>();
          List<Entry> fibrynogenValues = new ArrayList<>();
          List<Entry> wskaznikProtrombinowyValues = new ArrayList<>();
          List<Entry> inrValues = new ArrayList<>();
          List<Entry> czasProtrombinowyValues = new ArrayList<>();
          List<Entry> apttValues = new ArrayList<>();

          List<Entry> ddimerValues = new ArrayList<>();
          List<Entry> alATValues = new ArrayList<>();
          List<Entry> aspAtValues = new ArrayList<>();
          List<Entry> bilurbinaCalkowitaValues = new ArrayList<>();
          List<Entry> bialkoCalkowiteValues = new ArrayList<>();
          List<Entry> antyHbsValues = new ArrayList<>();
          List<Entry> antyHcvValues = new ArrayList<>();
          List<Entry> albuminaValues = new ArrayList<>();
          List<Entry> fosforanyValues = new ArrayList<>();
          List<Entry> moczCiezarValues = new ArrayList<>();

          List<Entry> moczUrobilinogenValues = new ArrayList<>();
          List<Entry> moczOdczynValues = new ArrayList<>();
          List<Entry> wankomycynaValues = new ArrayList<>();
          List<Entry> prokalcytoninaValues = new ArrayList<>();
          List<Entry> triglicerydyValues = new ArrayList<>();
          List<Entry> wapnCalkowitValues = new ArrayList<>();
          List<Entry> troponinaValues = new ArrayList<>();
          List<Entry> obValues = new ArrayList<>();
          List<Entry> hbsValues = new ArrayList<>();
          List<Entry> plynZJamTriglicerydyValues = new ArrayList<>();

          List<Entry> plynZJamGlukozaValues = new ArrayList<>();
          List<Entry> ggtpValues = new ArrayList<>();
          List<Entry> kwasWalproinowyValues = new ArrayList<>();
          List<Entry> plynZJamCiezarValues = new ArrayList<>();
          List<Entry> plynZJamOgolneValues = new ArrayList<>();
          List<Entry> plynZJamBialkoValues = new ArrayList<>();
          List<Entry> trojjododotyroninaValues = new ArrayList<>();
          List<Entry> tyroksynaValues = new ArrayList<>();
          List<Entry> tyreotropValues = new ArrayList<>();
          List<Entry> bilurbinaBezposredniaValues = new ArrayList<>();

          List<Entry> kinezaKreatywnaValues = new ArrayList<>();
          List<Entry> plynZJamErytrocytyValues = new ArrayList<>();
          List<Entry> plynZJamLeukocytyValues = new ArrayList<>();
          List<Entry> immunoglobulinaGValues = new ArrayList<>();
          List<Entry> antytryosynaValues = new ArrayList<>();
          List<Entry> immunoglobulinaAValues = new ArrayList<>();
          List<Entry> immunoglobulinaMValues = new ArrayList<>();

        for (BloodGasMeasurement defaultTestMeasurement : testMeasurements) {
            switch (Ints.checkedCast(defaultTestMeasurement.getTestId())) {
                case 1:
                    bialkoValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                case 2:
                    kreatyninaValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 3:
                    mocznikValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 4:
                    magnezValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 5:
                    antytrombinaValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 6:
                    fibrynogenValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 7:
                    wskaznikProtrombinowyValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 8:
                    inrValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 9:
                    czasProtrombinowyValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 10:
                    apttValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 11:
                    ddimerValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 12:
                    alATValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 13:
                    aspAtValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 14:
                    bilurbinaCalkowitaValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 15:
                    bialkoCalkowiteValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 16:
                    antyHbsValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 17:
                    antyHcvValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 18:
                    albuminaValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 19:
                    fosforanyValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 20:
                    moczCiezarValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 21:
                    moczUrobilinogenValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 22:
                    moczOdczynValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 23:
                    wankomycynaValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 24:
                    prokalcytoninaValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 25:
                    triglicerydyValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 26:
                    wapnCalkowitValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 27:
                    troponinaValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 28:
                    obValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 29:
                    hbsValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 30:
                    plynZJamTriglicerydyValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 31:
                    plynZJamGlukozaValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 32:
                    ggtpValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 33:
                    kwasWalproinowyValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 34:
                    plynZJamCiezarValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 35:
                    plynZJamOgolneValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 36:
                    plynZJamBialkoValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 37:
                    trojjododotyroninaValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 38:
                    tyroksynaValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 39:
                    tyreotropValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 40:
                    bilurbinaBezposredniaValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 41:
                    kinezaKreatywnaValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 42:
                    plynZJamErytrocytyValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 43:
                    plynZJamLeukocytyValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 44:
                    immunoglobulinaGValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 45:
                    antytryosynaValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 46:
                    immunoglobulinaAValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
                case 47:
                    immunoglobulinaMValues.add(new Entry(DateTimeFormatterHelper.getMillisFromString(defaultTestMeasurement.getResultDate()),defaultTestMeasurement.getValue().floatValue()));
                    break;
            }
        }

        LineDataSet bialkoSet = new LineDataSet(bialkoValues, getResources().getString(R.string.bialko));
        LineDataSet kreatyninaSet = new LineDataSet(kreatyninaValues, getResources().getString(R.string.kreatynina));
        LineDataSet mocznikSet = new LineDataSet(mocznikValues,  getResources().getString(R.string.mocznik));
        LineDataSet magnezkSet = new LineDataSet(magnezValues,  getResources().getString(R.string.magnez));
        LineDataSet antytrombinaSet = new LineDataSet(antytrombinaValues,  getResources().getString(R.string.antytrombina));
        LineDataSet fibrynogenSet = new LineDataSet(fibrynogenValues,  getResources().getString(R.string.fibrynogen));
        LineDataSet wskaznikProtrombinowySet = new LineDataSet(wskaznikProtrombinowyValues,  getResources().getString(R.string.wskaznikProtrombinowy));
        LineDataSet inrSet = new LineDataSet(inrValues,  getResources().getString(R.string.inr));
        LineDataSet czasProtrombinowySet = new LineDataSet(czasProtrombinowyValues,  getResources().getString(R.string.czasProtrombinowy));
        LineDataSet apttSet = new LineDataSet(apttValues,  getResources().getString(R.string.aptt));

        LineDataSet dimmerSet = new LineDataSet(ddimerValues,  getResources().getString(R.string.ddimer));
        LineDataSet alATVSet = new LineDataSet(alATValues,  getResources().getString(R.string.alat));
        LineDataSet aspAtSet = new LineDataSet(aspAtValues,  getResources().getString(R.string.aspAt));
        LineDataSet bilurbinaCalkowitaSet = new LineDataSet(bilurbinaCalkowitaValues,  getResources().getString(R.string.bilurbinaCalkowita));
        LineDataSet bialkoCalkowiteSet = new LineDataSet(bialkoCalkowiteValues,  getResources().getString(R.string.bialkoCalkowite));
        LineDataSet antyHbsSet = new LineDataSet(antyHbsValues,  getResources().getString(R.string.antyHbs));
        LineDataSet antyHcvSet = new LineDataSet(antyHcvValues,  getResources().getString(R.string.antyHcv));
        LineDataSet albuminaSet = new LineDataSet(albuminaValues,  getResources().getString(R.string.albumina));
        LineDataSet fosforanySet = new LineDataSet(fosforanyValues,  getResources().getString(R.string.fosforany));
        LineDataSet moczCiezarSet = new LineDataSet(moczCiezarValues,  getResources().getString(R.string.moczCiezar));

        LineDataSet moczUrobilinogenSet = new LineDataSet(moczUrobilinogenValues,  getResources().getString(R.string.moczUrobilinogen));
        LineDataSet moczOdczynSet = new LineDataSet(moczOdczynValues,  getResources().getString(R.string.moczOdczyn));
        LineDataSet wankomycynaSet = new LineDataSet(wankomycynaValues,  getResources().getString(R.string.wankomycyna));
        LineDataSet prokalcytoninaSet = new LineDataSet(prokalcytoninaValues, getResources().getString(R.string.prokalcytonina));
        LineDataSet triglicerydySet = new LineDataSet(triglicerydyValues,  getResources().getString(R.string.triglicerydy));
        LineDataSet wapnCalkowitSet = new LineDataSet(wapnCalkowitValues,  getResources().getString(R.string.wapnCalkowity));
        LineDataSet troponinaSet = new LineDataSet(troponinaValues,  getResources().getString(R.string.troponina));
        LineDataSet obSet = new LineDataSet(obValues, getResources().getString(R.string.ob));
        LineDataSet hbsSet = new LineDataSet(hbsValues,  getResources().getString(R.string.hbs));
        LineDataSet plynZJamTriglicerydySet = new LineDataSet(plynZJamTriglicerydyValues,  getResources().getString(R.string.plynZJamTriglicerydy));

        LineDataSet plynZJamGlukozaSet = new LineDataSet(plynZJamGlukozaValues,  getResources().getString(R.string.plynZJamGlukoza));
        LineDataSet ggtpSet = new LineDataSet(ggtpValues,  getResources().getString(R.string.ggtp));
        LineDataSet kwasWalproinowySet = new LineDataSet(kwasWalproinowyValues,  getResources().getString(R.string.kwasWalproinowy));
        LineDataSet plynZJamCiezarSet = new LineDataSet(plynZJamCiezarValues,  getResources().getString(R.string.plynZJamCiezar));
        LineDataSet plynZJamOgolnSet = new LineDataSet(plynZJamOgolneValues,  getResources().getString(R.string.plynZJamOgolne));
        LineDataSet plynZJamBialkoSet = new LineDataSet(plynZJamBialkoValues,  getResources().getString(R.string.plynZJamBialko));
        LineDataSet trojjododotyroninaSet = new LineDataSet(trojjododotyroninaValues,  getResources().getString(R.string.trojjododotyronina));
        LineDataSet tyroksynaSet = new LineDataSet(tyroksynaValues,  getResources().getString(R.string.tyroksyna));
        LineDataSet tyreotropSet = new LineDataSet(tyreotropValues,  getResources().getString(R.string.tyreotrop));
        LineDataSet bilurbinaBezposredniaSet = new LineDataSet(bilurbinaBezposredniaValues,  getResources().getString(R.string.bilurbinaBezp));

        LineDataSet kinezaKreatynowaSet = new LineDataSet(kinezaKreatywnaValues,  getResources().getString(R.string.kinezaKreatynowa));
        LineDataSet plynZJamErytrocytySet = new LineDataSet(plynZJamErytrocytyValues,  getResources().getString(R.string.plynZJamErytrocyty));
        LineDataSet plynZJamLeukocytySet = new LineDataSet(plynZJamLeukocytyValues,  getResources().getString(R.string.plynZJamLeukocyty));
        LineDataSet immunoglobulinaGSet = new LineDataSet(immunoglobulinaGValues,  getResources().getString(R.string.immunoglobulinaG));
        LineDataSet antytryosynaSet = new LineDataSet(antytryosynaValues,  getResources().getString(R.string.antytryosyna));
        LineDataSet immunoglobulinaASet = new LineDataSet(immunoglobulinaAValues,  getResources().getString(R.string.immunoglobulinaA));
        LineDataSet immunoglobulinaMSet = new LineDataSet(immunoglobulinaMValues,  getResources().getString(R.string.immunoglobulinaM));

//        inflammatorySets
        BaseChart.configurateChart(bialkoSet, Color.RED, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(prokalcytoninaSet, Color.BLUE, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(obSet, Color.GREEN, YAxis.AxisDependency.LEFT);
        if(bialkoSet.getEntryCount()>0){
            inflammatorySets.add(bialkoSet);
        }
        if(prokalcytoninaSet.getEntryCount()>0){
            inflammatorySets.add(prokalcytoninaSet);
        }
        if(obSet.getEntryCount()>0){
            inflammatorySets.add(obSet);
        }

//        kindneySets
        BaseChart.configurateChart(kreatyninaSet, Color.RED, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(mocznikSet, Color.BLUE, YAxis.AxisDependency.LEFT);
        if(kreatyninaSet.getEntryCount()>0){
            kindneySets.add(kreatyninaSet);
        }
        if(mocznikSet.getEntryCount()>0){
            kindneySets.add(mocznikSet);
        }

//        liverSets
        BaseChart.configurateChart(alATVSet, Color.RED, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(aspAtSet, Color.BLUE, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(ggtpSet, Color.GREEN, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(bilurbinaCalkowitaSet, Color.CYAN, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(bilurbinaBezposredniaSet, Color.MAGENTA, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(triglicerydySet, Color.LTGRAY, YAxis.AxisDependency.LEFT);
        if(alATVSet.getEntryCount()>0){
            liverSets.add(alATVSet);
        }
        if(aspAtSet.getEntryCount()>0){
            liverSets.add(aspAtSet);
        }
        if(ggtpSet.getEntryCount()>0){
            liverSets.add(ggtpSet);
        }
        if(bilurbinaCalkowitaSet.getEntryCount()>0){
            liverSets.add(bilurbinaCalkowitaSet);
        }
        if(bilurbinaBezposredniaSet.getEntryCount()>0){
            liverSets.add(bilurbinaBezposredniaSet);
        }
        if(triglicerydySet.getEntryCount()>0){
            liverSets.add(triglicerydySet);
        }

//        heartsSets
        BaseChart.configurateChart(troponinaSet, Color.RED, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(kinezaKreatynowaSet, Color.BLUE, YAxis.AxisDependency.LEFT);
        if(troponinaSet.getEntryCount()>0){
            heartsSets.add(troponinaSet);
        }
        if(kinezaKreatynowaSet.getEntryCount()>0){
            heartsSets.add(kinezaKreatynowaSet);
        }

//        antibodiesSets
        BaseChart.configurateChart(hbsSet, Color.RED, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(antyHcvSet, Color.BLUE, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(antyHbsSet, Color.GREEN, YAxis.AxisDependency.LEFT);
        if(hbsSet.getEntryCount()>0){
            antibodiesSets.add(hbsSet);
        }
        if(antyHcvSet.getEntryCount()>0){
            antibodiesSets.add(antyHcvSet);
        }
        if(antyHbsSet.getEntryCount()>0){
            antibodiesSets.add(antyHbsSet);
        }

//        coagulogySets
        BaseChart.configurateChart(fibrynogenSet, Color.RED, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(apttSet, Color.BLUE, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(inrSet, Color.GREEN, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(antytrombinaSet, Color.CYAN, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(dimmerSet, Color.MAGENTA, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(wskaznikProtrombinowySet, Color.LTGRAY, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(czasProtrombinowySet, Color.BLACK, YAxis.AxisDependency.LEFT);
        if(fibrynogenSet.getEntryCount()>0){
            coagulogySets.add(fibrynogenSet);
        }
        if(apttSet.getEntryCount()>0){
            coagulogySets.add(apttSet);
        }
        if(inrSet.getEntryCount()>0){
            coagulogySets.add(inrSet);
        }
        if(antytrombinaSet.getEntryCount()>0){
            coagulogySets.add(antytrombinaSet);
        }
        if(dimmerSet.getEntryCount()>0){
            coagulogySets.add(dimmerSet);
        }
        if(wskaznikProtrombinowySet.getEntryCount()>0){
            coagulogySets.add(wskaznikProtrombinowySet);
        }
        if(czasProtrombinowySet.getEntryCount()>0){
            coagulogySets.add(czasProtrombinowySet);
        }

//        urineSets
        BaseChart.configurateChart(moczCiezarSet, Color.RED, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(moczUrobilinogenSet, Color.BLUE, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(moczOdczynSet, Color.GREEN, YAxis.AxisDependency.LEFT);
        if(moczCiezarSet.getEntryCount()>0){
            urineSets.add(moczCiezarSet);
        }
        if(moczUrobilinogenSet.getEntryCount()>0){
            urineSets.add(moczUrobilinogenSet);
        }
        if(moczOdczynSet.getEntryCount()>0){
            urineSets.add(moczOdczynSet);
        }

//        proteinsSets
        BaseChart.configurateChart(bialkoCalkowiteSet, Color.RED, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(albuminaSet, Color.BLUE, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(immunoglobulinaGSet, Color.GREEN, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(antytryosynaSet, Color.CYAN, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(immunoglobulinaASet, Color.MAGENTA, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(immunoglobulinaMSet, Color.LTGRAY, YAxis.AxisDependency.LEFT);
        if(alATVSet.getEntryCount()>0){
            proteinsSets.add(alATVSet);
        }
        if(aspAtSet.getEntryCount()>0){
            proteinsSets.add(aspAtSet);
        }
        if(ggtpSet.getEntryCount()>0){
            proteinsSets.add(ggtpSet);
        }
        if(bilurbinaCalkowitaSet.getEntryCount()>0){
            proteinsSets.add(bilurbinaCalkowitaSet);
        }
        if(bilurbinaCalkowitaSet.getEntryCount()>0){
            proteinsSets.add(bilurbinaCalkowitaSet);
        }
        if(triglicerydySet.getEntryCount()>0){
            proteinsSets.add(triglicerydySet);
        }

//        macroelementsSets
        BaseChart.configurateChart(magnezkSet, Color.RED, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(fosforanySet, Color.BLUE, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(wapnCalkowitSet, Color.GREEN, YAxis.AxisDependency.LEFT);
        if(magnezkSet.getEntryCount()>0){
            macroelementsSets.add(magnezkSet);
        }
        if(fosforanySet.getEntryCount()>0){
            macroelementsSets.add(fosforanySet);
        }
        if(wapnCalkowitSet.getEntryCount()>0){
            macroelementsSets.add(wapnCalkowitSet);
        }

//        drugsSets
        BaseChart.configurateChart(wankomycynaSet, Color.RED, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(kwasWalproinowySet, Color.BLUE, YAxis.AxisDependency.LEFT);
        if(wankomycynaSet.getEntryCount()>0){
            drugsSets.add(wankomycynaSet);
        }
        if(kwasWalproinowySet.getEntryCount()>0){
            drugsSets.add(kwasWalproinowySet);
        }
//        bodyfluidsSets
        BaseChart.configurateChart(plynZJamTriglicerydySet, Color.RED, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(plynZJamGlukozaSet, Color.BLUE, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(plynZJamCiezarSet, Color.GREEN, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(plynZJamOgolnSet, Color.CYAN, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(plynZJamBialkoSet, Color.MAGENTA, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(plynZJamErytrocytySet, Color.LTGRAY, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(plynZJamLeukocytySet, Color.BLACK, YAxis.AxisDependency.LEFT);
        if(plynZJamTriglicerydySet.getEntryCount()>0){
            bodyfluidsSets.add(plynZJamTriglicerydySet);
        }
        if(plynZJamGlukozaSet.getEntryCount()>0){
            bodyfluidsSets.add(plynZJamGlukozaSet);
        }
        if(plynZJamCiezarSet.getEntryCount()>0){
            bodyfluidsSets.add(plynZJamCiezarSet);
        }
        if(plynZJamOgolnSet.getEntryCount()>0){
            bodyfluidsSets.add(plynZJamOgolnSet);
        }
        if(plynZJamBialkoSet.getEntryCount()>0){
            bodyfluidsSets.add(plynZJamBialkoSet);
        }
        if(plynZJamErytrocytySet.getEntryCount()>0){
            bodyfluidsSets.add(plynZJamErytrocytySet);
        }
        if(plynZJamLeukocytySet.getEntryCount()>0){
            bodyfluidsSets.add(plynZJamLeukocytySet);
        }

//        hormonsSets
        BaseChart.configurateChart(tyreotropSet, Color.RED, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(trojjododotyroninaSet, Color.BLUE, YAxis.AxisDependency.LEFT);
        BaseChart.configurateChart(tyroksynaSet, Color.GREEN, YAxis.AxisDependency.LEFT);
        if(tyreotropSet.getEntryCount()>0){
            hormonsSets.add(tyreotropSet);
        }
        if(trojjododotyroninaSet.getEntryCount()>0){
            hormonsSets.add(trojjododotyroninaSet);
        }
        if(tyroksynaSet.getEntryCount()>0){
            hormonsSets.add(tyroksynaSet);
        }
    }

}
