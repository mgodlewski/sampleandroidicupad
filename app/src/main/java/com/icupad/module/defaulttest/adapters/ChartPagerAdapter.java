package com.icupad.module.defaulttest.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;

import com.github.mikephil.charting.data.LineData;
import com.icupad.commons.ModuleFragment;
import com.icupad.module.defaulttest.fragments.ChartTabFragment;
import com.icupad.module.ecmo.charts.fragments.EcmoChartTabFragment;
import com.icupad.tableView.CellData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marci on 26.06.2017.
 */

public class ChartPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    private ArrayList<LineData> lineDatas = new ArrayList<>();
    private ArrayList<String> header;
    private ArrayList<List<CellData>> body;

    public ChartPagerAdapter(FragmentManager fm, int NumOfTabs, ArrayList<LineData> charts, ArrayList<String> header, ArrayList<List<CellData>> body) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.lineDatas = charts;
        this.header = header;
        this.body = body;
    }

    @Override
    public Fragment getItem(int position) {
        ModuleFragment tab;
        Bundle bundle = new Bundle();
        bundle.putSerializable("lineDatas", lineDatas);
        bundle.putInt("id", position);
        bundle.putStringArrayList("header", header);
        bundle.putSerializable("body", body);

        tab = new ChartTabFragment();

        tab.setArguments(bundle);
        return tab;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

}
