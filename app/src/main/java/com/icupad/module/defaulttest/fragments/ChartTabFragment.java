package com.icupad.module.defaulttest.fragments;

import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.LineData;
import com.icupad.R;
import com.icupad.commons.ModuleFragment;
import com.icupad.module.ecmo.charts.ChartConfiguration;
import com.icupad.tableView.CellData;
import com.icupad.tableView.MeasurementsTableFixHeaderAdapter;
import com.icupad.utils.model.Mode;
import com.inqbarna.tablefixheaders.TableFixHeaders;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marci on 26.06.2017.
 */

public class ChartTabFragment extends ModuleFragment {

    private int id;
    private LineChart lineChart;
    private TableFixHeaders table;
    protected Button tableChartButton;
    protected Button shiftToRightButton;

    private ArrayList<LineData> lineDatas;
    private ArrayList<String> header;
    private ArrayList<List<CellData>> body;

    private Mode mode;

    public ChartTabFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_defaulttest_chart_tab, container, false);
        lineChart = (LineChart) view.findViewById(R.id.lineChart);
        table = (TableFixHeaders) view.findViewById(R.id.table);
        tableChartButton = (Button) view.findViewById(R.id.viewTypeButton);
        shiftToRightButton = (Button) view.findViewById(R.id.shiftToRightButton);

        Bundle args = getArguments();
        id = args.getInt("id");
        lineDatas = (ArrayList<LineData>) args.getSerializable("lineDatas");
        header = args.getStringArrayList("header");
        body = (ArrayList<List<CellData>>) args.getSerializable("body");

        addClickListeners(true);

        LineData lineData = lineDatas.get(id);

        if(!ChartConfiguration.isLineChartEmpty(lineData)) {
            lineChart.setData(lineData);
            ChartConfiguration.configurateChart(lineChart, getActivity(), false);
            ChartConfiguration.setLegendSize(lineChart.getLegend());
        }else{
            lineChart.setData(null);
            lineChart.setNoDataText(getActivity().getResources().getString(R.string.no_data));
            Paint p = lineChart.getPaint(Chart.PAINT_INFO);
            p.setTextSize(15f);
            p.setColor(Color.BLACK);
        }

        //TODO konfiguracja tabel
        table.setAdapter(getTableAdapter());

        mode = Mode.CHART;
        table.setVisibility(View.GONE);
        lineChart.setVisibility(View.VISIBLE);
        tableChartButton.setText(getActivity().getResources().getString(com.icupad.R.string.tables_button));


        return view;
    }

    private void addClickListeners(final boolean withCharts ) {
        tableChartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mode.equals(Mode.CHART)) {
                    lineChart.setVisibility(View.GONE);
                    table.setVisibility(View.VISIBLE);
                    tableChartButton.setText(getActivity().getResources().getString(com.icupad.R.string.charts_button));
                    mode= Mode.TABLE;
                }else{
                    table.setVisibility(View.GONE);
                    lineChart.setVisibility(View.VISIBLE);
                    tableChartButton.setText(getActivity().getResources().getString(com.icupad.R.string.tables_button));
                    mode=Mode.CHART;
                }
            }
        });
        shiftToRightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO dodanie przesówania do ostatniej daty dla tabel
                if(withCharts) {
                    ChartConfiguration.resetZoom(lineChart);
                    ChartConfiguration.moveChartToLastDay(lineChart);
                }
            }
        });
    }

    private MeasurementsTableFixHeaderAdapter getTableAdapter() {
        MeasurementsTableFixHeaderAdapter adapter = new MeasurementsTableFixHeaderAdapter(getActivity());

        adapter.setHeader(header);
        adapter.setFirstBody(body);
        adapter.setBody(body);
        adapter.setSection(body);
        adapter.setFirstHeader("");

        return adapter;
    }
}
