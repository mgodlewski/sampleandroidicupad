package com.icupad.module.completebloodcount;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.common.primitives.Ints;
import com.icupad.R;
import com.icupad.commons.repository.model.BloodGasMeasurement;
import com.icupad.commons.repository.model.Stay;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.module.ResultsModuleFragment;
import com.icupad.tableView.CellData;
import com.inqbarna.tablefixheaders.TableFixHeaders;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class CompleteBloodCountTestFragment extends ResultsModuleFragment {

    private Button erytrocytyButton;
    private Button hemoglobinaButton;
    private Button hematokrytButton;
    private Button mcvButton;
    private Button mchButton;
    
    private Button mchcButton;
    private Button plytkiButton;
    private Button leukocytyButton;
    private Button rdwsdButton;
    private Button rdwcvButton;
    
    private Button pdwButton;
    private Button mpvButton;
    private Button plcrButton;
    private Button pctButton;
    private Button erytroblasty1Button;
    
    private Button erytroblasty2Button;

    LinearLayout linearLayoutB1;
    LinearLayout linearLayoutB2;
    LinearLayout linearLayoutB3;
    LinearLayout linearLayoutB4;

    public CompleteBloodCountTestFragment() {
    }

    @Override
    protected List<BloodGasMeasurement> getDataFromDB() {
        Stay lastStay = publicIcupadRepository.getLastStay();
        return publicIcupadRepository.getCompleteBloodCountMeasurement(lastStay.getId());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_complete_blood_count, container, false);

        prepareView(view, true);

        return view;
    }

    @Override
    protected  void addClickListenersToChartButtons(){
        erytrocytyButton.setOnClickListener(changeDataSet);
        hemoglobinaButton.setOnClickListener(changeDataSet);
        hematokrytButton.setOnClickListener(changeDataSet);
        mcvButton.setOnClickListener(changeDataSet);
        mchButton.setOnClickListener(changeDataSet);
        mchcButton.setOnClickListener(changeDataSet);
        plytkiButton.setOnClickListener(changeDataSet);
        leukocytyButton.setOnClickListener(changeDataSet);
        rdwsdButton.setOnClickListener(changeDataSet);
        rdwcvButton.setOnClickListener(changeDataSet);
        pdwButton.setOnClickListener(changeDataSet);
        mpvButton.setOnClickListener(changeDataSet);
        plcrButton.setOnClickListener(changeDataSet);;
        pctButton.setOnClickListener(changeDataSet);;
        erytroblasty1Button.setOnClickListener(changeDataSet);
        erytroblasty2Button.setOnClickListener(changeDataSet);
    }

    View.OnClickListener changeDataSet = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.erytrocytyButton:
                    v.setBackgroundColor(addDataSet(0));
                    break;
                case R.id.hemoglobinaButton:
                    v.setBackgroundColor(addDataSet(1));
                    break;
                case R.id.hematokrytButton:
                    v.setBackgroundColor(addDataSet(2));
                    break;
                case R.id.mcvButton:
                    v.setBackgroundColor(addDataSet(3));
                    break;
                case R.id.mchButton:
                    v.setBackgroundColor(addDataSet(4));
                    break;
                case R.id.mchcButton:
                    v.setBackgroundColor(addDataSet(5));
                    break;
                case R.id.plytkiButton:
                    v.setBackgroundColor(addDataSet(6));
                    break;
                case R.id.leukocytyButton:
                    v.setBackgroundColor(addDataSet(7));
                    break;
                case R.id.rdwsdButton:
                    v.setBackgroundColor(addDataSet(8));
                    break;
                case R.id.rdwcvButton:
                    v.setBackgroundColor(addDataSet(9));
                    break;
                case R.id.pdwButton:
                    v.setBackgroundColor(addDataSet(10));
                    break;
                case R.id.mpvButton:
                    v.setBackgroundColor(addDataSet(11));
                    break;
                case R.id.plcrButton:
                    v.setBackgroundColor(addDataSet(12));
                    break;
                case R.id.pctButton:
                    v.setBackgroundColor(addDataSet(13));
                    break;
                case R.id.erytroblasty1Button:
                    v.setBackgroundColor(addDataSet(14));
                    break;
                case R.id.erytroblasty2Button:
                    v.setBackgroundColor(addDataSet(15));
                    break;
            }
        }
    };

    @Override
    protected  void getAllViewElements(View view) {
        table = (TableFixHeaders) view.findViewById(R.id.table);
        informationTextView = (TextView) view.findViewById(R.id.informationTextView);
        tableChartButton = (Button) view.findViewById(R.id.viewTypeButton);
        shiftToRightButton = (Button) view.findViewById(R.id.shiftToRightButton);
        chart = (LineChart) view.findViewById(R.id.lineChart);

        erytrocytyButton = (Button) view.findViewById(R.id.erytrocytyButton);
        hemoglobinaButton = (Button) view.findViewById(R.id.hemoglobinaButton);
        hematokrytButton = (Button) view.findViewById(R.id.hematokrytButton);
        mcvButton = (Button) view.findViewById(R.id.mcvButton);
        mchButton = (Button) view.findViewById(R.id.mchButton);
        mchcButton = (Button) view.findViewById(R.id.mchcButton);
        plytkiButton = (Button) view.findViewById(R.id.plytkiButton);
        leukocytyButton = (Button) view.findViewById(R.id.leukocytyButton);
        rdwsdButton = (Button) view.findViewById(R.id.rdwsdButton);
        rdwcvButton = (Button) view.findViewById(R.id.rdwcvButton);
        pdwButton = (Button) view.findViewById(R.id.pdwButton);
        mpvButton = (Button) view.findViewById(R.id.mpvButton);
        plcrButton = (Button) view.findViewById(R.id.plcrButton);
        pctButton = (Button) view.findViewById(R.id.pctButton);
        erytroblasty1Button = (Button) view.findViewById(R.id.erytroblasty1Button);
        erytroblasty2Button = (Button) view.findViewById(R.id.erytroblasty2Button);

        linearLayoutB1 = (LinearLayout) view.findViewById(R.id.b1);
        linearLayoutB2 = (LinearLayout) view.findViewById(R.id.b2);
        linearLayoutB3 = (LinearLayout) view.findViewById(R.id.b3);
        linearLayoutB4 = (LinearLayout) view.findViewById(R.id.b4);

        erytrocytyButton.setBackgroundColor(Color.LTGRAY);
        hemoglobinaButton.setBackgroundColor(Color.LTGRAY);
        hematokrytButton.setBackgroundColor(Color.LTGRAY);
        mcvButton.setBackgroundColor(Color.LTGRAY);
        mchButton.setBackgroundColor(Color.LTGRAY);
        mchcButton.setBackgroundColor(Color.LTGRAY);
        plytkiButton.setBackgroundColor(Color.LTGRAY);
        leukocytyButton.setBackgroundColor(Color.LTGRAY);
        rdwsdButton.setBackgroundColor(Color.LTGRAY);
        rdwcvButton.setBackgroundColor(Color.LTGRAY);
        pdwButton.setBackgroundColor(Color.LTGRAY);
        mpvButton.setBackgroundColor(Color.LTGRAY);
        plcrButton.setBackgroundColor(Color.LTGRAY);
        pctButton.setBackgroundColor(Color.LTGRAY);
        erytroblasty1Button.setBackgroundColor(Color.LTGRAY);
        erytroblasty2Button.setBackgroundColor(Color.LTGRAY);
    }

    @Override
    protected  void setButtonsLinearLayoutVisibility(int visible){
        linearLayoutB1.setVisibility(visible);
        linearLayoutB2.setVisibility(visible);
        linearLayoutB3.setVisibility(visible);
        linearLayoutB4.setVisibility(visible);
    }

    @Override
    protected  void getTableData(List<String> header, List<List<CellData>> rows) {
        Map<Long,CellData> erytrocytyCols = new LinkedHashMap();
        Map<Long,CellData> hemoglobinaCols = new LinkedHashMap();
        Map<Long,CellData> hematokrytCols = new LinkedHashMap();
        Map<Long,CellData> mcvCols = new LinkedHashMap();
        Map<Long,CellData> mchCols = new LinkedHashMap();
        Map<Long,CellData> mchcCols = new LinkedHashMap();
        Map<Long,CellData> plytkiCols = new LinkedHashMap();
        Map<Long,CellData> leukocytyCols = new LinkedHashMap();
        Map<Long,CellData> rdwsdCols = new LinkedHashMap();
        Map<Long,CellData> rdwcvCols = new LinkedHashMap();
        Map<Long,CellData> pdwCols = new LinkedHashMap();
        Map<Long,CellData> mpvCols = new LinkedHashMap();
        Map<Long,CellData> plcrCols = new LinkedHashMap();
        Map<Long,CellData> pctCols = new LinkedHashMap();
        Map<Long,CellData> erytroblasty1Cols = new LinkedHashMap();
        Map<Long,CellData> erytroblasty2Cols = new LinkedHashMap();

        erytrocytyCols.put(0L,new CellData(getResources().getString(R.string.erytrocyty)));
        hemoglobinaCols.put(0L,new CellData(getResources().getString(R.string.hemoglobina)));
        hematokrytCols.put(0L,new CellData(getResources().getString(R.string.hematokryt)));
        mcvCols.put(0L,new CellData(getResources().getString(R.string.mcv)));
        mchCols.put(0L,new CellData(getResources().getString(R.string.mch)));
        mchcCols.put(0L,new CellData(getResources().getString(R.string.mchc)));
        plytkiCols.put(0L,new CellData(getResources().getString(R.string.plytki)));
        leukocytyCols.put(0L,new CellData(getResources().getString(R.string.leukocyty)));
        rdwsdCols.put(0L,new CellData(getResources().getString(R.string.rdwsd)));
        rdwcvCols.put(0L,new CellData(getResources().getString(R.string.rdwcv)));
        pdwCols.put(0L,new CellData(getResources().getString(R.string.pdw)));
        mpvCols.put(0L,new CellData(getResources().getString(R.string.mpv)));
        plcrCols.put(0L,new CellData(getResources().getString(R.string.plcr)));
        pctCols.put(0L,new CellData(getResources().getString(R.string.pct)));
        erytroblasty1Cols.put(0L,new CellData(getResources().getString(R.string.erytroblasty1)));
        erytroblasty2Cols.put(0L,new CellData(getResources().getString(R.string.erytroblasty2)));

        for(BloodGasMeasurement completeBloodCountMeasurement : testMeasurements) {
            if (!header.contains(DateTimeFormatterHelper.getStringWithoutSecondFromMillisInTwoLine(completeBloodCountMeasurement.getResultDateLong()))) {
                header.add(DateTimeFormatterHelper.getStringWithoutSecondFromMillisInTwoLine(completeBloodCountMeasurement.getResultDateLong()));
                erytrocytyCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                hemoglobinaCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                hematokrytCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                mcvCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                mchCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                mchcCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                plytkiCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                leukocytyCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                rdwsdCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                rdwcvCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                pdwCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                mpvCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                plcrCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                pctCols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                erytroblasty1Cols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
                erytroblasty2Cols.put(completeBloodCountMeasurement.getResultDateLong(), new CellData("-"));
            }
        }

        for(BloodGasMeasurement completeBloodCountMeasurement : testMeasurements){
            switch (Ints.checkedCast(completeBloodCountMeasurement.getTestId())){
                case 1:
                    erytrocytyCols.put(completeBloodCountMeasurement.getResultDateLong(),new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 2:
                    hemoglobinaCols.put(completeBloodCountMeasurement.getResultDateLong(),new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 3:
                    hematokrytCols.put(completeBloodCountMeasurement.getResultDateLong(),new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 4:
                    mcvCols.put(completeBloodCountMeasurement.getResultDateLong(),new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 5:
                    mchCols.put(completeBloodCountMeasurement.getResultDateLong(),new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 6:
                    mchcCols.put(completeBloodCountMeasurement.getResultDateLong(),new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 7:
                    plytkiCols.put(completeBloodCountMeasurement.getResultDateLong(),new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 8:
                    leukocytyCols.put(completeBloodCountMeasurement.getResultDateLong(),new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 9:
                    rdwsdCols.put(completeBloodCountMeasurement.getResultDateLong(),new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 10:
                    rdwcvCols.put(completeBloodCountMeasurement.getResultDateLong(),new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 11:
                    pdwCols.put(completeBloodCountMeasurement.getResultDateLong(),new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 12:
                    mpvCols.put(completeBloodCountMeasurement.getResultDateLong(),new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 13:
                    plcrCols.put(completeBloodCountMeasurement.getResultDateLong(),new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 14:
                    pctCols.put(completeBloodCountMeasurement.getResultDateLong(),new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 15:
                    erytroblasty1Cols.put(completeBloodCountMeasurement.getResultDateLong(),new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
                case 16:
                    erytroblasty2Cols.put(completeBloodCountMeasurement.getResultDateLong(),new CellData(completeBloodCountMeasurement.getValue(), completeBloodCountMeasurement.getAbnormality()));
                    break;
            }
        }

        rows.add(new ArrayList<>(erytrocytyCols.values()));
        rows.add(new ArrayList<>(hemoglobinaCols.values()));
        rows.add(new ArrayList<>(hematokrytCols.values()));
        rows.add(new ArrayList<>(mcvCols.values()));
        rows.add(new ArrayList<>(mchCols.values()));
        rows.add(new ArrayList<>(mchcCols.values()));
        rows.add(new ArrayList<>(plytkiCols.values()));
        rows.add(new ArrayList<>(leukocytyCols.values()));
        rows.add(new ArrayList<>(rdwsdCols.values()));
        rows.add(new ArrayList<>(rdwcvCols.values()));
        rows.add(new ArrayList<>(pdwCols.values()));
        rows.add(new ArrayList<>(mpvCols.values()));
        rows.add(new ArrayList<>(mpvCols.values()));
        rows.add(new ArrayList<>(plcrCols.values()));
        rows.add(new ArrayList<>(pctCols.values()));
        rows.add(new ArrayList<>(erytroblasty1Cols.values()));
        rows.add(new ArrayList<>(erytroblasty2Cols.values()));
    }

    protected void addStartCharts(){
        List<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(lineDataSet.get(0));
        erytrocytyButton.setBackgroundColor(lineDataSet.get(0).getColor());
        LineData lineData = new LineData(dataSets);
        chart.setData(lineData);
        lineData.notifyDataChanged();
        chart.notifyDataSetChanged();
        chart.invalidate();
    }

    @Override
    protected  List<LineDataSet> prepareChartData(){
        List<Entry> erytrocytyValues = new ArrayList<>();
        List<Entry> hemoglobinaValues = new ArrayList<>();
        List<Entry> hematokrytValues = new ArrayList<>();
        List<Entry> mcvValues = new ArrayList<>();
        List<Entry> mchValues = new ArrayList<>();
        List<Entry> mchcValues = new ArrayList<>();
        List<Entry> plytkiValues = new ArrayList<>();
        List<Entry> leukocytyValues = new ArrayList<>();
        List<Entry> rdwsdValues = new ArrayList<>();
        List<Entry> rdwcvValues = new ArrayList<>();
        List<Entry> pdwValues = new ArrayList<>();
        List<Entry> mpvValues = new ArrayList<>();
        List<Entry> plcrValues = new ArrayList<>();
        List<Entry> pctValues = new ArrayList<>();
        List<Entry> erytroblasty1Values = new ArrayList<>();
        List<Entry> erytroblasty2Values = new ArrayList<>();

        for(BloodGasMeasurement completeBloodCountMeasurement : testMeasurements){
            switch (Ints.checkedCast(completeBloodCountMeasurement.getTestId())){
                case 1:
                    erytrocytyValues.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
                    break;
                case 2:
                    hemoglobinaValues.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
                    break;
                case 3:
                    hematokrytValues.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
                    break;
                case 4:
                    mcvValues.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
                    break;
                case 5:
                    mchValues.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
                    break;
                case 6:
                    mchcValues.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
                    break;
                case 7:
                    plytkiValues.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
                    break;
                case 8:
                    leukocytyValues.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
                    break;
                case 9:
                    rdwsdValues.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
                    break;
                case 10:
                    rdwcvValues.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
                    break;
                case 11:
                    pdwValues.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
                    break;
                case 12:
                    mpvValues.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
                    break;
                case 13:
                    plcrValues.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
                    break;
                case 14:
                    pctValues.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
                    break;
                case 15:
                    erytroblasty1Values.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
                    break;
                case 16:
                    erytroblasty2Values.add(new Entry(completeBloodCountMeasurement.getResultDateLong(), completeBloodCountMeasurement.getValue().floatValue()));
                    break;
            }
        }

        List<LineDataSet> lineDataSets = new ArrayList<>();
        lineDataSets.add(configurateChart(new LineDataSet(erytrocytyValues, ""),Color.RED));
        lineDataSets.add(configurateChart(new LineDataSet(hemoglobinaValues, ""),Color.GREEN));
        lineDataSets.add(configurateChart(new LineDataSet(hematokrytValues, ""),Color.BLUE));
        lineDataSets.add(configurateChart(new LineDataSet(mcvValues, ""),Color.YELLOW));
        lineDataSets.add(configurateChart(new LineDataSet(mchValues, ""),Color.CYAN));

        lineDataSets.add(configurateChart(new LineDataSet(mchcValues, ""),Color.MAGENTA));
        lineDataSets.add(configurateChart(new LineDataSet(plytkiValues, ""),Color.parseColor("#fd6400")));
        lineDataSets.add(configurateChart(new LineDataSet(leukocytyValues, ""),Color.parseColor("#fdc5f8")));
        lineDataSets.add(configurateChart(new LineDataSet(rdwsdValues, ""),Color.parseColor("#cad700")));
        lineDataSets.add(configurateChart(new LineDataSet(rdwcvValues, ""),Color.parseColor("#800000")));

        lineDataSets.add(configurateChart(new LineDataSet(pdwValues, ""),Color.parseColor("#fdc57a")));
        lineDataSets.add(configurateChart(new LineDataSet(mpvValues, ""),Color.parseColor("#556B2F")));
        lineDataSets.add(configurateChart(new LineDataSet(plcrValues, ""),Color.parseColor("#800080")));
        lineDataSets.add(configurateChart(new LineDataSet(pctValues, ""),Color.parseColor("#9379c9")));
        lineDataSets.add(configurateChart(new LineDataSet(erytroblasty1Values, ""),Color.parseColor("#008080")));

        lineDataSets.add(configurateChart(new LineDataSet(erytroblasty2Values, ""),Color.parseColor("#aa6400")));
        return lineDataSets;
    }

}
