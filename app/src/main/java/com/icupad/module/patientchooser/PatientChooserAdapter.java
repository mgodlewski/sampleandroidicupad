package com.icupad.module.patientchooser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.icupad.R;
import com.icupad.commons.repository.model.Patient;
import com.icupad.commons.utils.DateTimeFormatterHelper;

import java.util.List;

/**
 * Created by Marcin on 24.04.2017.
 */
public class PatientChooserAdapter extends BaseAdapter {

    private List<Patient> allPatientsList;
    private Context context;

    public PatientChooserAdapter(List<Patient> allPatientsList, Context context) {
        this.allPatientsList = allPatientsList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return allPatientsList.size();
    }

    @Override
    public Object getItem(int i) {
        return allPatientsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Patient patient = allPatientsList.get(i);

        ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(view == null){
            view = inflater.inflate(R.layout.row_patient_chooser, null);
            holder = new ViewHolder();
            holder.surname = (TextView) view.findViewById(R.id.row_surname);
            holder.name = (TextView) view.findViewById(R.id.row_name);
            holder.sex = (TextView) view.findViewById(R.id.row_sex);
            holder.birthDate = (TextView) view.findViewById(R.id.row_birthDate);
            holder.admitDate = (TextView) view.findViewById(R.id.row_admitDate);
            holder.dischargeDate = (TextView) view.findViewById(R.id.row_dischargeDate);
            view.setTag(holder);
        }else{
            holder = (ViewHolder) view.getTag();
        }

        holder.surname.setText(patient.getSurname());
        holder.name.setText(patient.getName());
        if(patient.getSex().equals("MALE")){
            holder.sex.setText("mężczyzna");
        }else if(patient.getSex().equals("FEMALE")){
            holder.sex.setText("kobieta");
        }else{
            holder.sex.setText(patient.getSex());
        }

        if(patient.getBirthDate().split(" ").length>0){
            holder.birthDate.setText(patient.getBirthDate().split(" ")[0]);
        }else{
            holder.birthDate.setText(patient.getBirthDate());
        }
        holder.admitDate.setText(DateTimeFormatterHelper.transformStringToDate(patient.getAdmitDate()));
        holder.dischargeDate.setText(DateTimeFormatterHelper.transformStringToDate(patient.getDischargeDate()));

        return view;
    }

    private class ViewHolder{
        TextView surname;
        TextView name;
        TextView sex;
        TextView birthDate;
        TextView admitDate;
        TextView dischargeDate;

    }
}
