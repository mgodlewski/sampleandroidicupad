package com.icupad.module.patientchooser;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;

import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.ModuleFragment;
import com.icupad.commons.repository.model.Patient;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.R;

import java.util.ArrayList;
import java.util.List;

public class PatientChooserFragment extends ModuleFragment {

    private static Long lastChosenPatientId = null;
    public static Long getLastChosenPatientId() {
        return lastChosenPatientId;
    }
    public static void restartLastChosenPatientId() {
        lastChosenPatientId = null;
    }

    private PublicIcupadRepository publicIcupadRepository;

    private CheckBox onlyActiveCheckbox;
    private Button chosenPatientButton;
    private ListView patientsListView;

    private final Handler handler = new Handler();

    private List<Patient> allPatientsList;
    private List<Patient> visiblePatientsList = new ArrayList<>();

    private PatientChooserAdapter patientChooserAdapter;

    public PatientChooserFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_patient_chooser, container, false);
        publicIcupadRepository = fragmentListener.getRepository();

        findAllViews(view);

        onlyActiveCheckbox.setChecked(true);
        onlyActiveCheckbox.setOnClickListener(new checkboxToggler());

        new LoadingPatientsThread().start();
        return view;
    }

    private class LoadingPatientsThread extends Thread {
        @Override
        public void run(){
            allPatientsList = publicIcupadRepository.getAllPatients();
            handler.post(new Runnable(){
                @Override
                public void run(){
                    filterVisiblePatientsByOnlyActiveCheckbox();
                    fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
                }
            });
        }
    }

    private void findAllViews(View view) {
        onlyActiveCheckbox = (CheckBox) view.findViewById(R.id.onlyActivePatientsCheckBox);
        chosenPatientButton = (Button) getActivity().findViewById(R.id.chosenPatientSurnameAndName);
        patientsListView = (ListView) view.findViewById(R.id.patientsListView);
    }

    private class checkboxToggler implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            filterVisiblePatientsByOnlyActiveCheckbox();
        }
    }

    private void filterVisiblePatientsByOnlyActiveCheckbox() {
        boolean checked = onlyActiveCheckbox.isChecked();
        visiblePatientsList = new ArrayList<>();
        for (Patient patient : allPatientsList) {
            if (!checked || patient.getActive().equals("1")) {
                visiblePatientsList.add(patient);
            }
        }
        setupListView();
    }

    public void setupListView() {
        patientChooserAdapter = new PatientChooserAdapter(visiblePatientsList, getActivity());
        patientsListView.setAdapter(patientChooserAdapter);

        patientsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Patient p = (Patient) adapterView.getItemAtPosition(i);
                lastChosenPatientId = p.getId();
                chosenPatientButton.setText(p.getName() + "  " + p.getSurname());
                chosenPatientButton.setVisibility(View.VISIBLE);
                fragmentListener.onButtonPressed(Message.OPEN_PATIENT_DETAIL_FRAGMENT);
            }
        });
    }
}
