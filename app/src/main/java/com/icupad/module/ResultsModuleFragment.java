package com.icupad.module;

import android.graphics.Color;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.icupad.androidcommons.R;
import com.icupad.commons.ModuleFragment;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.BloodGasMeasurement;
import com.icupad.module.ecmo.charts.ChartConfiguration;
import com.icupad.module.patientchooser.PatientChooserFragment;
import com.icupad.utils.model.Mode;
import com.icupad.tableView.CellData;
import com.icupad.tableView.MeasurementsTableFixHeaderAdapter;
import com.inqbarna.tablefixheaders.TableFixHeaders;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 13.06.2017.
 */
public abstract class ResultsModuleFragment extends ModuleFragment {

    final Handler handler = new Handler();
    protected PublicIcupadRepository publicIcupadRepository;

    protected TextView informationTextView;
    protected TableFixHeaders table;
    protected Button tableChartButton;
    protected Button shiftToRightButton;
    protected LineChart chart;

    protected Mode mode;

    protected List<BloodGasMeasurement> testMeasurements;
    protected List<LineDataSet> lineDataSet;

    public ResultsModuleFragment() {
    }


    public void prepareView(View view, final boolean withCharts ) {
        publicIcupadRepository = fragmentListener.getRepository();

        getAllViewElements(view);
        addClickListeners(withCharts);

        if(PatientChooserFragment.getLastChosenPatientId() != null) {
            informationTextView.setVisibility(View.GONE);

            new Thread(new Runnable(){
                @Override
                public void run(){
                    testMeasurements = getDataFromDB();

                    handler.post(new Runnable(){
                        @Override
                        public void run(){
                            chart.setVisibility(View.GONE);
                            setButtonsLinearLayoutVisibility(View.GONE);
                            mode = Mode.TABLE;
                            if(testMeasurements.isEmpty()){
                                showNoDataInfo(getResources().getString(R.string.no_data_about_patient));
                            }else {
                                table.setAdapter(getTableAdapter());
                                if(withCharts) {
                                    prepareCharts();
                                }
                            }
                            fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
                        }
                    });
                }
            }).start();

        } else {
            showNoDataInfo(getResources().getString(R.string.no_chosen_patient));
            setButtonsLinearLayoutVisibility(View.GONE);
            chart.setVisibility(View.GONE);
            table.setVisibility(View.GONE);
            fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
        }
    }

    protected abstract List<BloodGasMeasurement> getDataFromDB();


    private void showNoDataInfo(String s){
        informationTextView.setVisibility(View.VISIBLE);
        shiftToRightButton.setVisibility(View.GONE);
        tableChartButton.setVisibility(View.GONE);
        informationTextView.setText(s);
    }

    protected abstract void getAllViewElements(View view);


    private MeasurementsTableFixHeaderAdapter getTableAdapter() {
        MeasurementsTableFixHeaderAdapter adapter = new MeasurementsTableFixHeaderAdapter(getActivity());
        List<String> header = new ArrayList<>();
        List<List<CellData>> body = new ArrayList<>();
        getTableData(header, body);

        adapter.setHeader(header);
        adapter.setFirstBody(body);
        adapter.setBody(body);
        adapter.setSection(body);
        adapter.setFirstHeader("");

        return adapter;
    }

    protected abstract void getTableData(List<String> header, List<List<CellData>> rows);


    private void addClickListeners(final boolean withCharts ) {
        tableChartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mode.equals(Mode.CHART)) {
                    chart.setVisibility(View.GONE);
                    setButtonsLinearLayoutVisibility(View.GONE);
                    table.setVisibility(View.VISIBLE);
                    tableChartButton.setText(getActivity().getResources().getString(com.icupad.R.string.charts_button));
                    mode= Mode.TABLE;
                }else{
                    table.setVisibility(View.GONE);
                    setButtonsLinearLayoutVisibility(View.VISIBLE);
                    chart.setVisibility(View.VISIBLE);
                    tableChartButton.setText(getActivity().getResources().getString(com.icupad.R.string.tables_button));
                    mode=Mode.CHART;
                }
            }
        });
        shiftToRightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO
                if(withCharts) {
                    ChartConfiguration.resetZoom(chart);
                    ChartConfiguration.moveChartToLastDay(chart);
                }
            }
        });
        addClickListenersToChartButtons();
    }

    protected abstract void addClickListenersToChartButtons();


    private void prepareCharts(){
        lineDataSet = prepareChartData();
        addStartCharts();
        ChartConfiguration.configurateChart(chart, getActivity(), true);
        ChartConfiguration.configurateLegend(chart, null);
    }

    protected abstract List<LineDataSet> prepareChartData();
    protected abstract void addStartCharts();
    protected abstract void setButtonsLinearLayoutVisibility(int visible);

    protected LineDataSet configurateChart(LineDataSet dataSet, int color){
        dataSet.setColor(color);
        dataSet.setCircleColor(color);
        dataSet.setDrawValues(false);
        return dataSet;
    }

    protected int addDataSet(int i){
        LineData data = chart.getData();
        int color = Color.LTGRAY;

        if(data.contains(lineDataSet.get(i))){
            color = lineDataSet.get(i).getColor();
            if(data.getDataSets().size()>1) {
                data.removeDataSet(lineDataSet.get(i));
                color = Color.LTGRAY;
            }
        }else if(i<lineDataSet.size()) {
            if(lineDataSet.get(i).getEntryCount()!=0) {
                data.addDataSet(lineDataSet.get(i));
                color = lineDataSet.get(i).getColor();
            }
        }

        data.notifyDataChanged();
        chart.notifyDataSetChanged();
        chart.invalidate();
        return color;
    }
}
