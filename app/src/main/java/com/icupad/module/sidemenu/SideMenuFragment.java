package com.icupad.module.sidemenu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.icupad.R;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.ModuleFragment;
import com.icupad.utils.LoggedUserInfo;

public class SideMenuFragment extends ModuleFragment {

    private Button patientChooserButton;
    private Button bloodGasButton;
    private Button completeBloodCountButton;
    private Button stethoscopeButton;
    private Button conflictResolverButton;
    private Button logoutButton;
    private Button debugButton;
    private Button adminButton;
    private Button ecmoButton;
    private Button defaultTestButton;

    private boolean conflictButtonEnabled = false;

    public SideMenuFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_menu, container, false);

        findAllViews(view);
        addAllButtonListeners();

        return view;
    }

    private void findAllViews(View view) {
        patientChooserButton = (Button) view.findViewById(R.id.patientChooserButton);
        bloodGasButton = (Button) view.findViewById(R.id.bloodGasButton);
        completeBloodCountButton = (Button) view.findViewById(R.id.completeBloodCountButton);
        stethoscopeButton = (Button) view.findViewById(R.id.stethoscopeButton);
        conflictResolverButton = (Button) view.findViewById(R.id.conflictResolverButton);
        logoutButton = (Button) view.findViewById(R.id.logoutButton);
        debugButton = (Button) view.findViewById(R.id.debugButton);
        adminButton = (Button) view.findViewById(R.id.adminButton);
        ecmoButton = (Button) view.findViewById(R.id.ecmoButton);
        defaultTestButton = (Button) view.findViewById(R.id.defaultButton);
    }

    private void addAllButtonListeners() {
        patientChooserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentListener.onButtonPressed(Message.OPEN_PATIENT_CHOOSER_FRAGMENT);
            }
        });

        bloodGasButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentListener.onButtonPressed(Message.OPEN_BLOOD_GAS_FRAGMENT);
            }
        });

        completeBloodCountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentListener.onButtonPressed(Message.OPEN_COMPLETE_BLOOD_COUNT_FRAGMENT);
            }
        });

        stethoscopeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentListener.onButtonPressed(Message.OPEN_STETHOSCOPE_FRAGMENT);
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentListener.onButtonPressed(Message.LOGOUT);
            }
        });

        debugButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentListener.onButtonPressed(Message.OPEN_DEBUG_FRAGMENT);
            }
        });

        conflictResolverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentListener.onButtonPressed(Message.OPEN_CONFLICT_RESOLVER_FRAGMENT);
            }
        });

        adminButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentListener.onButtonPressed(Message.OPEN_ADMIN_FRAGMENT);
            }
        });

        ecmoButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                fragmentListener.onButtonPressed(Message.OPEN_ECMO_HISTORY_FRAGMENT);
            }
        });
        defaultTestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentListener.onButtonPressed(Message.OPEN_DEFAULT_TEST_FRAGMENT);
            }
        });
    }

    public void hideButtons() {
        patientChooserButton.setVisibility(View.INVISIBLE);
        bloodGasButton.setVisibility(View.INVISIBLE);
        completeBloodCountButton.setVisibility(View.INVISIBLE);
        stethoscopeButton.setVisibility(View.INVISIBLE);
        conflictResolverButton.setVisibility(View.INVISIBLE);
        logoutButton.setVisibility(View.INVISIBLE);
        debugButton.setVisibility(View.INVISIBLE);
        adminButton.setVisibility(View.INVISIBLE);
        ecmoButton.setVisibility(View.INVISIBLE);
        defaultTestButton.setVisibility(View.INVISIBLE);
    }

    public void showButtons() {
        patientChooserButton.setVisibility(View.VISIBLE);
        bloodGasButton.setVisibility(View.VISIBLE);
        completeBloodCountButton.setVisibility(View.VISIBLE);
        stethoscopeButton.setVisibility(View.VISIBLE);
        conflictResolverButton.setVisibility(View.VISIBLE);
        logoutButton.setVisibility(View.VISIBLE);
        ecmoButton.setVisibility(View.VISIBLE);
        defaultTestButton.setVisibility(View.VISIBLE);

        if(getResources().getBoolean(R.bool.debug_module_visibility_for_all) || LoggedUserInfo.hasDevRole()) {
            debugButton.setVisibility(View.VISIBLE);
        }
        else {
            debugButton.setVisibility(View.GONE);
        }
        if(LoggedUserInfo.hasAdminRole()) {
            adminButton.setVisibility(View.VISIBLE);
        }
        else {
            adminButton.setVisibility(View.GONE);
        }
    }

    public void disableConflictButton() {
        conflictButtonEnabled = false;
        conflictResolverButton.setEnabled(false);
    }

    public void enableConflictButton() {
        conflictButtonEnabled = true;
        conflictResolverButton.setEnabled(true);
    }

    public void disableButtons() {
        conflictResolverButton.setEnabled(false);
        setEnabilityForButtonsExceptConflictAndLogout(false);
    }

    public void enableButtons() {
        conflictResolverButton.setEnabled(conflictButtonEnabled);
        setEnabilityForButtonsExceptConflictAndLogout(true);
    }

    public void setEnabilityForButtonsExceptConflictAndLogout(boolean enability) {
        patientChooserButton.setEnabled(enability);
        bloodGasButton.setEnabled(enability);
        completeBloodCountButton.setEnabled(enability);
        stethoscopeButton.setEnabled(enability);
        debugButton.setEnabled(enability);
        adminButton.setEnabled(enability);
        ecmoButton.setEnabled(enability);
        defaultTestButton.setEnabled(enability);
    }
}
