package com.icupad.module.mainscreen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.icupad.R;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.ModuleFragment;


public class MainScreenFragment extends ModuleFragment {

    public MainScreenFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_screen, container, false);
        fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
        return view;
    }
}
