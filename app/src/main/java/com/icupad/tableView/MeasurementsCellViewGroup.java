package com.icupad.tableView;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.icupad.R;

import java.util.List;

import miguelbcr.ui.tableFixHeadesWrapper.TableFixHeaderAdapter;

/**
 * Created by Marcin on 11.06.2017.
 */
public class MeasurementsCellViewGroup extends FrameLayout
        implements
        TableFixHeaderAdapter.FirstHeaderBinder<String>,
        TableFixHeaderAdapter.HeaderBinder<String>,
        TableFixHeaderAdapter.FirstBodyBinder<List<CellData>>,
        TableFixHeaderAdapter.BodyBinder<List<CellData>>,
        TableFixHeaderAdapter.SectionBinder<List<CellData>>  {

    private static final String ABOVE_HIGH_NORM = "ABOVE_HIGH_NORM";
    private static final String BELOW_LOW_NORM = "BELOW_LOW_NORM";

    public TextView textView;
    public View vg_root;

    public MeasurementsCellViewGroup(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.cell_text_view, this, true);
        textView = (TextView) findViewById(R.id.tv_text);
        textView.setTextSize(18);
        vg_root = findViewById(R.id.vg_root);
    }

    @Override
    public void bindBody(List<CellData> items, int row, int column) {
        textView.setText(String.valueOf(items.get(column + 1).getValue()));
        textView.setGravity(Gravity.CENTER);
        if(row % 2 == 0){
            vg_root.setBackgroundResource(R.drawable.cell_white);
            if(items.get(column + 1).getAbnormality()!=null) {
                if (items.get(column + 1).getAbnormality().equals(ABOVE_HIGH_NORM)) {
                    vg_root.setBackgroundResource(R.drawable.cell_red_border_white);
                } else if (items.get(column + 1).getAbnormality().equals(BELOW_LOW_NORM)) {
                    vg_root.setBackgroundResource(R.drawable.cell_blue_border_white);
                }
            }
        }else {
            vg_root.setBackgroundResource(R.drawable.cell_gray);
            if (items.get(column + 1).getAbnormality() != null) {
                if (items.get(column + 1).getAbnormality().equals(ABOVE_HIGH_NORM)) {
                    vg_root.setBackgroundResource(R.drawable.cell_red_border_gray);
                } else if (items.get(column + 1).getAbnormality().equals(BELOW_LOW_NORM)) {
                    vg_root.setBackgroundResource(R.drawable.cell_blue_border_gray);
                }
            }
        }
    }

    @Override
    public void bindFirstBody(List<CellData> strings, int i) {
        textView.setText(String.valueOf(strings.get(0).getValue()));
        textView.setGravity(Gravity.LEFT);
        textView.setTypeface(null, Typeface.BOLD);
        vg_root.setBackgroundResource(i % 2 == 0 ? R.drawable.cell_white : R.drawable.cell_gray);
    }

    @Override
    public void bindFirstHeader(String s) {
        textView.setText(s);
        vg_root.setBackgroundResource(R.drawable.cell_gray);
    }

    @Override
    public void bindHeader(String s, int i) {
        textView.setText(s);
        textView.setGravity(Gravity.CENTER);
        textView.setTypeface(null, Typeface.BOLD);
        vg_root.setBackgroundResource(R.drawable.cell_gray);
    }

    @Override
    public void bindSection(List<CellData> strings, int i, int i1) {
    }
}
