package com.icupad.tableView;

/**
 * Created by Marcin on 11.06.2017.
 */
public class CellData {
    String value;
    String abnormality;

    public CellData(String value, String abnormality) {
        this.value = value;
        this.abnormality = abnormality;
    }

    public CellData(double value, String abnormality) {
        this.value = String.valueOf(value);
        this.abnormality = abnormality;
    }

    public CellData(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setValue(double value) {
        this.value = String.valueOf(value);
    }

    public String getAbnormality() {
        return abnormality;
    }

    public void setAbnormality(String abnromality) {
        this.abnormality = abnromality;
    }
}
