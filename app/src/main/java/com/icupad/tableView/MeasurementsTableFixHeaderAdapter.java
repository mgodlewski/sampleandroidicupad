package com.icupad.tableView;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import miguelbcr.ui.tableFixHeadesWrapper.TableFixHeaderAdapter;

/**
 * Created by Marcin on 11.06.2017.
 */
public class MeasurementsTableFixHeaderAdapter extends TableFixHeaderAdapter<
        String, MeasurementsCellViewGroup,
        String, MeasurementsCellViewGroup,
        List<CellData>,
        MeasurementsCellViewGroup,
        MeasurementsCellViewGroup,
        MeasurementsCellViewGroup> {

    private Context context;
    private MeasurementsCellViewGroup bodyMeasurementsCellViewGroup;

    public MeasurementsTableFixHeaderAdapter(Context context) {
        super(context);
        this.context = context;
    }

    public void scroll(int x){
        bodyMeasurementsCellViewGroup.setScrollX(x);
//        setScrollX(x);
    }


    @Override
    protected MeasurementsCellViewGroup inflateFirstHeader() {
        return new MeasurementsCellViewGroup(context);
    }

    @Override
    protected MeasurementsCellViewGroup inflateHeader() {
        return new MeasurementsCellViewGroup(context);
    }

    @Override
    protected MeasurementsCellViewGroup inflateFirstBody() {

        return new MeasurementsCellViewGroup(context);
    }

    @Override
    protected MeasurementsCellViewGroup inflateBody() {
        bodyMeasurementsCellViewGroup = new MeasurementsCellViewGroup(context);
        return bodyMeasurementsCellViewGroup;
    }

    @Override
    protected MeasurementsCellViewGroup inflateSection() {
        return new MeasurementsCellViewGroup(context);
    }

    @Override
    protected List<Integer> getHeaderWidths() {
        List<Integer> headerWidths = new ArrayList<>();

        // First header
        headerWidths.add(300);

        for (int i = 0; i < getHeader().size(); i++)
            headerWidths.add(170);

        return headerWidths;
    }

    @Override
    protected int getHeaderHeight() {
        return 100;
    }

    @Override
    protected int getSectionHeight() {
        return 0;
    }

    @Override
    protected int getBodyHeight() {
        return 80;
    }

    @Override
    protected boolean isSection(List<List<CellData>> list, int i) {
        return false;
    }
}