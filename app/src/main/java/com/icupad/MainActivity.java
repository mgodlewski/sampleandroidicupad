package com.icupad;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.icupad.bloodgas.BloodGasFragmentOld;
import com.icupad.module.admin.AdminFragment;
import com.icupad.module.bloodgas.BloodGasTestFragment;
import com.icupad.module.conflictresolver.ConflictResolverFragment;
import com.icupad.module.debug.DebugFragment;
import com.icupad.module.defaulttest.DefaultTestFragment;
import com.icupad.module.ecmo.fragments.EcmoActFragment;
import com.icupad.module.ecmo.fragments.EcmoCreateNewFragment;
import com.icupad.module.ecmo.fragments.EcmoHistoryFragment;
import com.icupad.module.ecmo.fragments.EcmoInfoFragment;
import com.icupad.module.ecmo.fragments.EcmoParameterFragment;
import com.icupad.module.ecmo.fragments.EcmoPatientFragment;
import com.icupad.module.ecmo.fragments.EcmoVisualizationFragment;
import com.icupad.module.login.LoginFragment;
import com.icupad.module.mainscreen.MainScreenFragment;
import com.icupad.module.sidemenu.SideMenuFragment;
import com.icupad.module.patientchooser.PatientChooserFragment;
import com.icupad.module.patientdetail.PatientDetailFragment;
import com.icupad.module.completebloodcount.CompleteBloodCountTestFragment;
import com.icupad.stethoscope.StethoscopeFragment;
import com.icupad.utils.LoggedUserInfo;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.commons.activitycommunication.FragmentListener;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.db.repository.modulerepository.IcupadRepositoryImpl;
import com.icupad.webconnection.model.ConnectionStatus;
import com.icupad.synchronization.SynchronizerService;

import net.sqlcipher.database.SQLiteDatabase;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements FragmentListener, Serializable {

    private static final String EMPTY_STRING = "";
    private int MILLISECONDS_OF_INACTIVITY_BEFORE_LOGOUT;
    private int MILLISECONDS_OF_INACTIVITY_AFTER_WARNING_BEFORE_LOGOUT;
    private int MILLISECONDS_OF_INACTIVITY_BEFORE_WARNING;

    private PublicIcupadRepository publicIcupadRepository;

    private FrameLayout frameLayout;

    private Fragment loginFragment;
    private SideMenuFragment sideMenuFragment;
    private Fragment helloFragment;
    private Fragment patientDetailFragment;
    private PatientChooserFragment patientChooserFragment;
    private Fragment bloodGasFragment;
    private CompleteBloodCountTestFragment completeBloodCountTestFragment;
    private Fragment stethoscopeFragment;
    private Fragment debugFragment;
    private Fragment conflictResolverFragment;
    private Fragment adminFragment;
    private Fragment ecmoHistoryFragment;
    private Fragment ecmoCreateNewFragment;
    private Fragment ecmoVisualizationFragment;
    private Fragment ecmoPatientFragment;
    private Fragment ecmoActFragment;
    private Fragment ecmoInfoFragment;
    private Fragment ecmoParameterFragment;
    private Fragment defaultTestFragment;

    private Map<Message, Fragment> messageToFragmentToOpen;

    private ProgressBar progressBar;
    private ImageView openCloseMenuImage;
    private TextView connectionStatusTextView;
    private Button chosenPatientButton;

    private FragmentManager fragmentManager = getFragmentManager();
    private FragmentTransaction fragmentTransaction;

    private Intent synchronizeServiceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SQLiteDatabase.loadLibs(this);

        publicIcupadRepository = new IcupadRepositoryImpl(getApplicationContext());

        PrivateDataAccessor.initCommonSharedPreferences(getApplicationContext());
        initializeAllFragments();
        initializeMessageToFragmentToOpen();
        findAllViews();

        openCloseMenuImage.setOnClickListener(new onOpenCloseMenuListener());

        MILLISECONDS_OF_INACTIVITY_BEFORE_LOGOUT
                = 1000 * getResources().getInteger(R.integer.seconds_of_inactivity_before_logout);
        MILLISECONDS_OF_INACTIVITY_AFTER_WARNING_BEFORE_LOGOUT
                = 1000 * getResources().getInteger(R.integer.seconds_of_inactivity_after_warning_before_logout);
        MILLISECONDS_OF_INACTIVITY_BEFORE_WARNING =
                MILLISECONDS_OF_INACTIVITY_BEFORE_LOGOUT - MILLISECONDS_OF_INACTIVITY_AFTER_WARNING_BEFORE_LOGOUT;

        onLogoutReplace();
    }

    private void findAllViews() {
        frameLayout = (FrameLayout) findViewById(R.id.myFragmentAnother);
        sideMenuFragment = (SideMenuFragment) getFragmentManager().findFragmentById(R.id.myFragmentMain);
        openCloseMenuImage = (ImageView)findViewById(R.id.openingClosingImageView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        connectionStatusTextView = (TextView)findViewById(R.id.connectionStatus);
        chosenPatientButton = (Button) findViewById(R.id.chosenPatientSurnameAndName);
    }

    private void initializeAllFragments() {
        loginFragment = new LoginFragment();
        helloFragment = new MainScreenFragment();
        patientChooserFragment = new PatientChooserFragment();
        completeBloodCountTestFragment = new CompleteBloodCountTestFragment();
//        bloodGasFragment = new BloodGasFragmentOld();
        bloodGasFragment = new BloodGasTestFragment();
        patientDetailFragment = new PatientDetailFragment();
        stethoscopeFragment = new StethoscopeFragment();
        debugFragment = new DebugFragment();
        conflictResolverFragment = new ConflictResolverFragment();
        adminFragment = new AdminFragment();
        ecmoHistoryFragment = new EcmoHistoryFragment();
        ecmoCreateNewFragment = new EcmoCreateNewFragment();
        ecmoVisualizationFragment = new EcmoVisualizationFragment();
        ecmoPatientFragment = new EcmoPatientFragment();
        ecmoActFragment = new EcmoActFragment();
        ecmoInfoFragment = new EcmoInfoFragment();
        ecmoParameterFragment = new EcmoParameterFragment();
        defaultTestFragment = new DefaultTestFragment();
    }

    private void initializeMessageToFragmentToOpen() {
        messageToFragmentToOpen = new HashMap<>();
        messageToFragmentToOpen.put(Message.OPEN_HELLO_FRAGMENT, helloFragment);
        messageToFragmentToOpen.put(Message.OPEN_PATIENT_CHOOSER_FRAGMENT, patientChooserFragment);
        messageToFragmentToOpen.put(Message.OPEN_COMPLETE_BLOOD_COUNT_FRAGMENT, completeBloodCountTestFragment);
        messageToFragmentToOpen.put(Message.OPEN_BLOOD_GAS_FRAGMENT, bloodGasFragment);
        messageToFragmentToOpen.put(Message.OPEN_PATIENT_DETAIL_FRAGMENT, patientDetailFragment);
        messageToFragmentToOpen.put(Message.OPEN_DEBUG_FRAGMENT, debugFragment);
        messageToFragmentToOpen.put(Message.OPEN_STETHOSCOPE_FRAGMENT, stethoscopeFragment);
        messageToFragmentToOpen.put(Message.OPEN_CONFLICT_RESOLVER_FRAGMENT, conflictResolverFragment);
        messageToFragmentToOpen.put(Message.OPEN_ADMIN_FRAGMENT, adminFragment);
        messageToFragmentToOpen.put(Message.OPEN_ECMO_HISTORY_FRAGMENT, ecmoHistoryFragment);
        messageToFragmentToOpen.put(Message.OPEN_ECMO_CREATE_NEW_FRAGMENT, ecmoCreateNewFragment);
        messageToFragmentToOpen.put(Message.OPEN_ECMO_VISUALIZATION_FRAGMENT, ecmoVisualizationFragment);
        messageToFragmentToOpen.put(Message.OPEN_ECMO_PATIENT_FRAGMENT, ecmoPatientFragment);
        messageToFragmentToOpen.put(Message.OPEN_ECMO_ACT_FRAGMENT, ecmoActFragment);
        messageToFragmentToOpen.put(Message.OPEN_ECMO_INFO_FRAGMENT, ecmoInfoFragment);
        messageToFragmentToOpen.put(Message.OPEN_ECMO_PARAMETER_FRAGMENT, ecmoParameterFragment);
        messageToFragmentToOpen.put(Message.OPEN_DEFAULT_TEST_FRAGMENT, defaultTestFragment);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Message msg) {
        switch(msg) {
            case ENABLE_CONFLICT_BUTTON:
                sideMenuFragment.enableConflictButton();
                break;
            case DISABLE_CONFLICT_BUTTON:
                sideMenuFragment.disableConflictButton();
        }
    }

    @Override
    public PublicIcupadRepository getRepository() {
        return new IcupadRepositoryImpl(getApplicationContext());
    }

    @Override
    public Long getChosenPaintId() {
        return PatientChooserFragment.getLastChosenPatientId();
    }

    @Override
    public void onButtonPressed(Message msg) {
        switch (msg) {
            case DISABLE_CONFLICT_BUTTON:
                break;
            case ENABLE_CONFLICT_BUTTON:
                break;
            case FRAGMENT_LOADING:
                progressBar.setVisibility(View.VISIBLE);
                frameLayout.setVisibility(View.GONE);
                break;
            case FRAGMENT_LOADED:
                sideMenuFragment.enableButtons();
                chosenPatientButton.setEnabled(true);
                progressBar.setVisibility(View.GONE);
                frameLayout.setVisibility(View.VISIBLE);
                break;
            case LOGIN:
                sideMenuFragment.disableButtons();
                onLoginReplace();
                break;
            case LOGOUT:
                onLogoutReplace();
                break;
            default:
                replaceFragment(messageToFragmentToOpen.get(msg));
                break;
        }
    }

    @Override
    public void replaceFragment(Fragment replacingFragment) {
        if(!replacingFragment.isAdded()) {
            sideMenuFragment.disableButtons();
            chosenPatientButton.setEnabled(false);
            loadFragment(replacingFragment);
        }
    }

    private void onLogoutReplace() {
        loadFragment(loginFragment);
        PatientChooserFragment.restartLastChosenPatientId();
        chosenPatientButton.setText(EMPTY_STRING);
        chosenPatientButton.setVisibility(View.INVISIBLE);
        hideSideMenu();
        stopService(new Intent(this, SynchronizerService.class));
        tryUnregisterReceiver();
        LoggedUserInfo.logOut();
        myHandler.removeCallbacks(myRunnableWarning);
        myHandler.removeCallbacks(myRunnableLogout);
    }

    private void onLoginReplace() {
        loadFragment(patientChooserFragment);
        showSideMenu();
        synchronizeServiceIntent = new Intent(this, SynchronizerService.class);
        synchronizeServiceIntent.putExtra(SynchronizerService.DB_SYNCHRONIZATION, (Boolean)true);
        startService(synchronizeServiceIntent);
        registerReceiver(receiver, new IntentFilter(SynchronizerService.NOTIFICATION));
    }

    private void loadFragment(final Fragment fragment) {
        progressBar.setVisibility(View.VISIBLE);
        frameLayout.setVisibility(View.GONE);
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.myFragmentAnother, fragment);
        fragmentTransaction.commit();
    }

    private void showSideMenu() {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.show(sideMenuFragment);
        fragmentTransaction.commit();
        sideMenuFragment.showButtons();
        openCloseMenuImage.setVisibility(View.VISIBLE);
    }

    private void hideSideMenu() {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.hide(sideMenuFragment);
        fragmentTransaction.commit();
        sideMenuFragment.hideButtons();
        openCloseMenuImage.setVisibility(View.GONE);
    }

    private void tryUnregisterReceiver() {
        try {
            unregisterReceiver(receiver);
        }
        catch(IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


    public void onPatientNameClicked(View view) {
        if(PatientChooserFragment.getLastChosenPatientId() != null) {
            onButtonPressed(Message.OPEN_PATIENT_DETAIL_FRAGMENT);
        }
    }

    private class onOpenCloseMenuListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            FragmentTransaction ft = MainActivity.this.getFragmentManager().beginTransaction();

            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            if (sideMenuFragment.isHidden()) {
                ft.show(sideMenuFragment);
                openCloseMenuImage.setImageResource(R.drawable.close);
            } else {
                ft.hide(sideMenuFragment);
                openCloseMenuImage.setImageResource(R.drawable.open);
            }
            ft.commit();
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String string = bundle.getString(SynchronizerService.MESSAGE);
                switch(bundle.getString(SynchronizerService.TYPE)) {
                    case SynchronizerService.TYPE_CONNECTION_STATUS:
                        reactOnConnectionStatus(ConnectionStatus.valueOf(string));
                        break;
                    case SynchronizerService.UPDATE_DB:
                        Toast.makeText(context, getResources().getString(R.string.db_data_updated_text), Toast.LENGTH_LONG).show();
                        break;
                    case SynchronizerService.FIRST_TIME_UPDATE_DB:
                        Handler handler = new Handler();
                        handler.post(new FirstDbUpdatePopUp());

                        break;
                }
            }
        }
    };

    private void reactOnConnectionStatus(ConnectionStatus connectionStatus) {
        switch(connectionStatus) {
            case NO_CONNECTION:
                connectionStatusTextView.setText(getResources().getString(R.string.connection_status_no_connection));
                connectionStatusTextView.setTextColor(Color.RED);
                break;
            case CONNECTION_TO_INTERNET:
                connectionStatusTextView.setText(getResources().getString(R.string.connection_status_no_server_found));
                connectionStatusTextView.setTextColor(Color.YELLOW);
                break;
            case CONNECTION_TO_SERVER:
                connectionStatusTextView.setText(getResources().getString(R.string.connection_status_connected_to_server));
                connectionStatusTextView.setTextColor(Color.GREEN);
                break;
            case AUTHENTICATED_CONNECTION_TO_SERVER:
                connectionStatusTextView.setText(getResources().getString(R.string.connection_status_authenticated_connected_to_server));
                connectionStatusTextView.setTextColor(Color.GREEN);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        stopService(new Intent(this, SynchronizerService.class));
        tryUnregisterReceiver();
        super.onStop();
    }

    private class FirstDbUpdatePopUp implements Runnable {
        @Override
        public void run() {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle(getResources().getString(R.string.db_first_initialization_text));
            builder.setNeutralButton(getResources().getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface di, int i) {
                        }
                    });
            builder.create().show();
        }
    }

    Handler myHandler = new Handler();
    Runnable myRunnableLogout = new Runnable() {
        @Override
        public void run() {
            myHandler.post(new Runnable() {
                @Override
                public void run() {
                    onLogoutReplace();
                    Toast.makeText(getApplicationContext(),
                            getResources().getText(R.string.information_on_inactivity_logout),
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    };
    Runnable myRunnableWarning = new Runnable() {
        @Override
        public void run() {
            String prefix = getResources().getText(R.string.warning_on_inactivity_logout_prefix).toString();
            String sufix = getResources().getText(R.string.warning_on_inactivity_logout_sufix).toString();
            final String message = prefix + " " + (MILLISECONDS_OF_INACTIVITY_AFTER_WARNING_BEFORE_LOGOUT/1000) + sufix;
            myHandler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(),
                            message,
                            Toast.LENGTH_LONG).show();
                    myHandler.postDelayed(myRunnableLogout, MILLISECONDS_OF_INACTIVITY_AFTER_WARNING_BEFORE_LOGOUT);
                }
            });
        }
    };
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        myHandler.removeCallbacks(myRunnableWarning);
        myHandler.removeCallbacks(myRunnableLogout);
        myHandler.postDelayed(myRunnableWarning, MILLISECONDS_OF_INACTIVITY_BEFORE_WARNING);
    }
}
