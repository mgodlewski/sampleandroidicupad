package com.icupad.synchronization.conflict.resolver;

import com.icupad.grable.GrableView;

public interface ConflictResolver<T extends Object> {
    void logConflict(long internalId, T remoteDto);
    GrableView populateConflictResolvingGrableView(GrableView grableView, long localInternalId, long remoteInternalId);
    void succeedConflict(GrableView grableView, long localInternalId, long remoteInternalId);
}
