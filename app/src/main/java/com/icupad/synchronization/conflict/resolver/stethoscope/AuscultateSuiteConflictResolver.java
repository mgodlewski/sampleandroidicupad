package com.icupad.synchronization.conflict.resolver.stethoscope;

import android.content.Context;

import com.google.common.collect.Lists;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.grable.GrableView;
import com.icupad.synchronization.conflict.ConflictUtils;
import com.icupad.synchronization.conflict.resolver.ConflictResolver;
import com.icupad.synchronization.function.stethoscope.AuscultateSuiteDtoToValuesFunction;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.webconnection.dto.stethoscope.AuscultateSuiteDto;
public class AuscultateSuiteConflictResolver implements ConflictResolver<AuscultateSuiteDto> {

    private AuscultateSuiteDtoToValuesFunction auscultateSuiteDtoToValuesFunction;
    private ConflictUtils conflictUtils;

    public AuscultateSuiteConflictResolver(Context context) {
        this.auscultateSuiteDtoToValuesFunction = new AuscultateSuiteDtoToValuesFunction();
        this.conflictUtils = new ConflictUtils(context, IcupadDbContract.AuscultateSuite.TABLE_NAME,
                Lists.newArrayList(IcupadDbContract.AuscultateSuite.COLUMN_NAME_DESCRIPTION));
    }

    @Override
    public void logConflict(long internalId, AuscultateSuiteDto remoteAuscultateSuiteDto) {
        conflictUtils.logConflict(internalId,
                auscultateSuiteDtoToValuesFunction.apply(remoteAuscultateSuiteDto, SyncStatus.REMOTE_CONFLICT));
    }

    @Override
    public GrableView populateConflictResolvingGrableView(GrableView grableView, long localInternalId, long remoteInternalId) {
        return conflictUtils.populateConflictResolvingGrableView(grableView, localInternalId, remoteInternalId);
    }

    @Override
    public void succeedConflict(GrableView grableView, long localInternalId, long remoteInternalId) {
        conflictUtils.succeedConflict(grableView, localInternalId, remoteInternalId);
    }
}
