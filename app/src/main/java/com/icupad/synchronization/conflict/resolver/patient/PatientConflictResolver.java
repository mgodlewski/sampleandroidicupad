package com.icupad.synchronization.conflict.resolver.patient;

import android.content.Context;

import com.google.common.collect.Lists;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.synchronization.conflict.ConflictUtils;
import com.icupad.synchronization.conflict.resolver.ConflictResolver;
import com.icupad.synchronization.function.patient.PatientDtoToValuesFunction;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.webconnection.dto.patient.PatientDto;
import com.icupad.grable.GrableView;

public class PatientConflictResolver implements ConflictResolver<PatientDto> {

    private PatientDtoToValuesFunction patientDtoToValuesFunction;
    private ConflictUtils conflictUtils;

    public PatientConflictResolver(Context context) {
        this.patientDtoToValuesFunction = new PatientDtoToValuesFunction();
        this.conflictUtils = new ConflictUtils(context, IcupadDbContract.Patient.TABLE_NAME,
                Lists.newArrayList(IcupadDbContract.Patient.COLUMN_NAME_HEIGHT,
                        IcupadDbContract.Patient.COLUMN_NAME_WEIGHT,
                        IcupadDbContract.Patient.COLUMN_NAME_BLOOD_TYPE,
                        IcupadDbContract.Patient.COLUMN_NAME_ALLERGIES,
                        IcupadDbContract.Patient.COLUMN_NAME_OTHER_IMPORTANT_INFORMATIONS));
    }

    public void logConflict(long internalId, PatientDto remotePatientDto) {
        conflictUtils.logConflict(internalId,
                patientDtoToValuesFunction.apply(remotePatientDto, SyncStatus.REMOTE_CONFLICT));
    }

    public GrableView populateConflictResolvingGrableView(GrableView grableView, long localInternalId, long remoteInternalId) {
        return conflictUtils.populateConflictResolvingGrableView(grableView, localInternalId, remoteInternalId);
    }

    public void succeedConflict(GrableView grableView, long localInternalId, long remoteInternalId) {
        conflictUtils.succeedConflict(grableView, localInternalId, remoteInternalId);
    }
}
