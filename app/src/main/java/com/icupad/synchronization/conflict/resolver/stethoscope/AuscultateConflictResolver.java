package com.icupad.synchronization.conflict.resolver.stethoscope;

import android.content.Context;

import com.google.common.collect.Lists;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.grable.GrableView;
import com.icupad.synchronization.conflict.ConflictUtils;
import com.icupad.synchronization.conflict.resolver.ConflictResolver;
import com.icupad.synchronization.function.stethoscope.AuscultateDtoToValuesFunction;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.webconnection.dto.stethoscope.AuscultateDto;

public class AuscultateConflictResolver implements ConflictResolver<AuscultateDto>{

    private AuscultateDtoToValuesFunction auscultateDtoToValuesFunction;
    private ConflictUtils conflictUtils;

    public AuscultateConflictResolver(Context context) {
        this.auscultateDtoToValuesFunction = new AuscultateDtoToValuesFunction();
        this.conflictUtils = new ConflictUtils(context, IcupadDbContract.Auscultate.TABLE_NAME,
                Lists.newArrayList(IcupadDbContract.Auscultate.COLUMN_NAME_DESCRIPTION,
                        IcupadDbContract.Auscultate.COLUMN_NAME_POLL_RESULT));
    }

    @Override
    public void logConflict(long internalId, AuscultateDto conflictAuscultateDto) {
        conflictUtils.logConflict(internalId,
                auscultateDtoToValuesFunction.apply(conflictAuscultateDto, SyncStatus.REMOTE_CONFLICT));
    }

    @Override
    public GrableView populateConflictResolvingGrableView(GrableView grableView, long localInternalId, long remoteInternalId) {
        return conflictUtils.populateConflictResolvingGrableView(grableView, localInternalId, remoteInternalId);
    }

    @Override
    public void succeedConflict(GrableView grableView, long localInternalId, long remoteInternalId) {
        conflictUtils.succeedConflict(grableView, localInternalId, remoteInternalId);
    }
}
