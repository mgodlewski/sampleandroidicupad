package com.icupad.synchronization.conflict;

import android.content.Context;

import com.icupad.commons.activitycommunication.Message;
import com.icupad.db.repository.conflictrepository.ConflictTableRepository;

import org.greenrobot.eventbus.EventBus;

public class ConflictSynchronizer {
    private ConflictTableRepository conflictTableRepository;

    public ConflictSynchronizer(Context context) {
        conflictTableRepository = new ConflictTableRepository(context);
    }

    public void synchronize() {
        if(conflictTableRepository.checkConflictExistence()) {
            EventBus.getDefault().post(Message.ENABLE_CONFLICT_BUTTON);
        }
    }
}
