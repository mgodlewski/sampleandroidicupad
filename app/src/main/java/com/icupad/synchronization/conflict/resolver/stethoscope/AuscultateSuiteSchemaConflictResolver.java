package com.icupad.synchronization.conflict.resolver.stethoscope;

import android.content.Context;

import com.google.common.collect.Lists;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.grable.GrableView;
import com.icupad.synchronization.conflict.ConflictUtils;
import com.icupad.synchronization.conflict.resolver.ConflictResolver;
import com.icupad.synchronization.function.stethoscope.AuscultateSuiteSchemaDtoToValuesFunction;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.webconnection.dto.stethoscope.AuscultateSuiteSchemaDto;

public class AuscultateSuiteSchemaConflictResolver implements ConflictResolver<AuscultateSuiteSchemaDto> {

    private AuscultateSuiteSchemaDtoToValuesFunction auscultateSuiteSchemaDtoToValuesFunction;
    private ConflictUtils conflictUtils;

    public AuscultateSuiteSchemaConflictResolver(Context context) {
        this.auscultateSuiteSchemaDtoToValuesFunction = new AuscultateSuiteSchemaDtoToValuesFunction();
        this.conflictUtils = new ConflictUtils(context, IcupadDbContract.AuscultateSuiteSchema.TABLE_NAME,
                Lists.newArrayList(IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_NAME));
    }

    @Override
    public void logConflict(long internalId, AuscultateSuiteSchemaDto remoteSuiteSchemaDto) {
        conflictUtils.logConflict(internalId,
                auscultateSuiteSchemaDtoToValuesFunction.apply(remoteSuiteSchemaDto, SyncStatus.REMOTE_CONFLICT));
    }

    @Override
    public GrableView populateConflictResolvingGrableView(GrableView grableView, long localInternalId, long remoteInternalId) {
        return conflictUtils.populateConflictResolvingGrableView(grableView, localInternalId, remoteInternalId);
    }

    @Override
    public void succeedConflict(GrableView grableView, long localInternalId, long remoteInternalId) {
        conflictUtils.succeedConflict(grableView, localInternalId, remoteInternalId);
    }
}
