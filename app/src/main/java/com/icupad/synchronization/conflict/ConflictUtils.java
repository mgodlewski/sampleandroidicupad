package com.icupad.synchronization.conflict;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.icupad.R;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.grable.GrableView;
import com.icupad.grable.model.Cell;
import com.icupad.synchronization.utils.SyncStatus;

import java.util.ArrayList;
import java.util.List;

public class ConflictUtils {
    public static final int SOLUTION_ROW_NO = 2;
    public static final int LOCAL_VALUES_ROW_NO = 1;
    public static final int REMOTE_VALUES_ROW_NO = 0;
    private final String SOLUTION_ROW_LABEL;
    private final String REMOTE_ROW_LABEL;
    private final String LOCAL_ROW_LABEL;

    private SyncTableRepository syncTableRepository;
    private IcupadDbHelper mDbHelper;
    private List<String> columns;
    private String tableName;
    private long remoteInternalId;
    private long localInternalId;

    
    
    
    public ConflictUtils(Context context, String tableName, List<String> columns) {
        this.syncTableRepository = new SyncTableRepository(context);
        this.mDbHelper = new IcupadDbHelper(context);
        this.tableName = tableName;
        this.columns = columns;

        SOLUTION_ROW_LABEL = context.getResources().getString(R.string.conflict_resolver_solution_row);
        REMOTE_ROW_LABEL = context.getResources().getString(R.string.conflict_resolver_remote_row);
        LOCAL_ROW_LABEL = context.getResources().getString(R.string.conflict_resolver_local_row);
    }

    public void logConflict(long internalId, ContentValues remoteValues) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        updateTableWithConflictLocalAndSaveInternalId(db, internalId);
        addRemoteValuesAndSaveRemoteId(db, remoteValues);
        fillConflictTable(db);
        removeValuesFromSyncTable(db);

        mDbHelper.closeDb(db);
    }

    private void updateTableWithConflictLocalAndSaveInternalId(SQLiteDatabase db, long internalId) {
        localInternalId = internalId;
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.COLUMN_NAME_SYNC, SyncStatus.LOCAL_CONFLICT.getValue());

        getQueryForModelByInternalId(db, internalId)
                .update(values);
    }

    private void addRemoteValuesAndSaveRemoteId(SQLiteDatabase db, ContentValues remoteValues) {
        Query query = QueryBuilder.aQuery(db)
                .withTable(tableName)
                .build();
        remoteInternalId = query.insert(remoteValues);
    }

    private void fillConflictTable(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.ConflictTable.COLUMN_NAME_TABLE_NAME, tableName);
        values.put(IcupadDbContract.ConflictTable.COLUMN_NAME_INFO_INTERNAL_ID, localInternalId);
        values.put(IcupadDbContract.ConflictTable.COLUMN_NAME_CONFLICT_INFO_INTERNAL_ID, remoteInternalId);

        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.ConflictTable.TABLE_NAME)
                .build();
        query.insert(values);
    }

    private void removeValuesFromSyncTable(SQLiteDatabase db) {
        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.SyncTable.TABLE_NAME)
                .withSelection(IcupadDbContract.SyncTable.COLUMN_NAME_TABLE_NAME + "=? and " +
                        IcupadDbContract.SyncTable.COLUMN_NAME_INFO_INTERNAL_ID + "=?")
                .withSelectionArgs(tableName, localInternalId)
                .build();
        query.delete();
    }

    
    
    
    
    public GrableView populateConflictResolvingGrableView(GrableView grableView, long localInternalId, long remoteInternalId) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        updateGrableWithColumnsAndRows(grableView);
        grableView.addAllCells(
                getDataForGrable(db, localInternalId, remoteInternalId));

        mDbHelper.closeDb(db);
        return grableView;
    }

    private void updateGrableWithColumnsAndRows(GrableView grableView) {
        for(String column : columns) {
            grableView.addColumn(new String[]{column});
        }

        grableView.addRow(REMOTE_ROW_LABEL);
        grableView.addRow(LOCAL_ROW_LABEL);
        grableView.addRow(SOLUTION_ROW_LABEL);
    }

    @NonNull
    private Cell[][] getDataForGrable(SQLiteDatabase db, long localInternalId, long remoteInternalId) {
        List<String>  remoteValues = getValues(db, localInternalId, columns);
        List<String>  localValues = getValues(db, remoteInternalId, columns);

        Cell[][] data = new Cell[3][columns.size()];
        for(int i = 0; i < columns.size(); i++) {
            data[REMOTE_VALUES_ROW_NO][i] = new Cell(remoteValues.get(i));
            data[LOCAL_VALUES_ROW_NO][i] = new Cell(localValues.get(i));
            data[SOLUTION_ROW_NO][i] = new Cell("");
        }
        return data;
    }
    
    private List<String>  getValues(SQLiteDatabase db, long infoInternalId, List<String> columns) {
        List<String> values = new ArrayList<>();
        Cursor cursor = getQueryForTablesColumnsByInternalId(db, infoInternalId)
                .select();
        cursor.moveToFirst();

        if(!cursor.isAfterLast()) {
            for(int i = 0; i < columns.size(); i++) {
                values.add(cursor.getString(i));
            }
        }
        cursor.close();
        return values;
    }
    
    private Query getQueryForTablesColumnsByInternalId(SQLiteDatabase db, long infoInternalId) {
        return QueryBuilder.aQuery(db)
                .withTable(tableName)
                .withColumns(columns)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                .withSelectionArgs(infoInternalId)
                .build();
    }

    
    
    
    
    public void succeedConflict(GrableView grableView, long localInternalId, long remoteInternalId) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Query baseQuery = getQueryForModelByInternalId(db, localInternalId);
        Query remoteQuery = getCursorForModelWithLastModifiedDateAndIdByRemoteInternalId(db, remoteInternalId);

        Cursor remoteCursor = remoteQuery.select();
        remoteCursor.moveToFirst();
        if(!remoteCursor.isAfterLast()) {
            
            ContentValues values = getValuesFromSyncingForUpdate(grableView, remoteCursor);
            baseQuery.update(values);

            remoteQuery.delete();
            deleteRowFromConflictTable(db, localInternalId, remoteInternalId);
            syncTableRepository.logUpdate(tableName, localInternalId);
        }
        
        remoteCursor.close();
        mDbHelper.closeDb(db);
    }

    private Query getCursorForModelWithLastModifiedDateAndIdByRemoteInternalId(SQLiteDatabase db, long remoteInternalId) {
        return QueryBuilder.aQuery(db)
                .withTable(tableName)
                .withColumns(IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_DATE,
                        IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_BY_ID)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                .withSelectionArgs(remoteInternalId)
                .build();
    }

    @NonNull
    private ContentValues getValuesFromSyncingForUpdate(GrableView grableView, Cursor remoteCursor) {
        ContentValues values = new ContentValues();
        for(int i = 0; i < columns.size(); i++) {
            values.put(columns.get(i), grableView.getCell(i, SOLUTION_ROW_NO).getValue());
        }
        values.put(IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_DATE, remoteCursor.getString(0));
        values.put(IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_BY_ID, remoteCursor.getLong(1));
        values.put(IcupadDbContract.COLUMN_NAME_SYNC, SyncStatus.DIRTY.getValue());
        return values;
    }

    private Query getQueryForModelByInternalId(SQLiteDatabase db, long internalId) {
        return QueryBuilder.aQuery(db)
                .withTable(tableName)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                .withSelectionArgs(internalId)
                .build();
    }

    private void deleteRowFromConflictTable(SQLiteDatabase db, long localInternalId, long remoteInternalId) {
        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.ConflictTable.TABLE_NAME)
                .withSelection(IcupadDbContract.ConflictTable.COLUMN_NAME_TABLE_NAME + "=? and " +
                        IcupadDbContract.ConflictTable.COLUMN_NAME_INFO_INTERNAL_ID + "=? and " +
                        IcupadDbContract.ConflictTable.COLUMN_NAME_CONFLICT_INFO_INTERNAL_ID + "=?")
                .withSelectionArgs(tableName, localInternalId, remoteInternalId)
                .build();
        query.delete();
    }
}
