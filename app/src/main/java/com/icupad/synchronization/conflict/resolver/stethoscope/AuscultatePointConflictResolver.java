package com.icupad.synchronization.conflict.resolver.stethoscope;

import android.content.Context;

import com.google.common.collect.Lists;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.grable.GrableView;
import com.icupad.synchronization.conflict.ConflictUtils;
import com.icupad.synchronization.conflict.resolver.ConflictResolver;
import com.icupad.synchronization.function.stethoscope.AuscultatePointDtoToValuesFunction;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.webconnection.dto.stethoscope.AuscultatePointDto;

public class AuscultatePointConflictResolver implements ConflictResolver<AuscultatePointDto> {

    private AuscultatePointDtoToValuesFunction auscultatePointDtoToValuesFunction;
    private ConflictUtils conflictUtils;

    public AuscultatePointConflictResolver(Context context) {
        this.auscultatePointDtoToValuesFunction = new AuscultatePointDtoToValuesFunction();
        this.conflictUtils = new ConflictUtils(context, IcupadDbContract.AuscultatePoint.TABLE_NAME,
                Lists.newArrayList(IcupadDbContract.AuscultatePoint.COLUMN_NAME_NAME,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_QUEUE_POSITION,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_X,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_Y,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_IS_FRONT));
    }

    @Override
    public void logConflict(long internalId, AuscultatePointDto remotePointDto) {
        conflictUtils.logConflict(internalId,
                auscultatePointDtoToValuesFunction.apply(remotePointDto, SyncStatus.REMOTE_CONFLICT));
    }

    @Override
    public GrableView populateConflictResolvingGrableView(GrableView grableView, long localInternalId, long remoteInternalId) {
        return conflictUtils.populateConflictResolvingGrableView(grableView, localInternalId, remoteInternalId);
    }

    @Override
    public void succeedConflict(GrableView grableView, long localInternalId, long remoteInternalId) {
        conflictUtils.succeedConflict(grableView, localInternalId, remoteInternalId);
    }
}
