package com.icupad.synchronization.conflict.resolver.patient;

import android.content.Context;

import com.google.common.collect.Lists;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.grable.GrableView;
import com.icupad.synchronization.conflict.ConflictUtils;
import com.icupad.synchronization.conflict.resolver.ConflictResolver;
import com.icupad.synchronization.function.patient.OperationDtoToValuesFunction;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.webconnection.dto.patient.OperationDto;

public class OperationConflictResolver implements ConflictResolver<OperationDto>{
    private OperationDtoToValuesFunction operationDtoToValuesFunction;
    private ConflictUtils conflictUtils;

    public OperationConflictResolver(Context context) {
        this.operationDtoToValuesFunction = new OperationDtoToValuesFunction();
        this.conflictUtils = new ConflictUtils(context, IcupadDbContract.Operation.TABLE_NAME,
                Lists.newArrayList(IcupadDbContract.Operation.COLUMN_NAME_DESCRIPTION,
                        IcupadDbContract.Operation.COLUMN_NAME_OPERATING_DATE,
                        IcupadDbContract.Operation.COLUMN_NAME_ARCHIVED));
    }

    public void logConflict(long internalId, OperationDto remoteOperationDto) {
        conflictUtils.logConflict(internalId,
                operationDtoToValuesFunction.apply(remoteOperationDto, SyncStatus.REMOTE_CONFLICT));
    }

    public GrableView populateConflictResolvingGrableView(GrableView grableView, long localInternalId, long remoteInternalId) {
        return conflictUtils.populateConflictResolvingGrableView(grableView, localInternalId, remoteInternalId);
    }

    public void succeedConflict(GrableView grableView, long localInternalId, long remoteInternalId) {
        conflictUtils.succeedConflict(grableView, localInternalId, remoteInternalId);
    }
}
