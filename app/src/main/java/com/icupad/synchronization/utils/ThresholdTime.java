package com.icupad.synchronization.utils;

import com.icupad.utils.PrivateDataAccessor;

public class ThresholdTime {
    private Long icupadLastSyncedTimestamp;
    private Long currentIcupadLastSyncedTimestamp;

    public ThresholdTime() {
        this.icupadLastSyncedTimestamp = 0L;
        this.currentIcupadLastSyncedTimestamp = 0L;
    }

    public ThresholdTime(Long icupadLastSyncedTimestamp) {
        this.icupadLastSyncedTimestamp = icupadLastSyncedTimestamp;
        this.currentIcupadLastSyncedTimestamp = icupadLastSyncedTimestamp;
    }

    public void updateAndSave() {
        PrivateDataAccessor.setLastSyncTimestamps(this);
        icupadLastSyncedTimestamp = currentIcupadLastSyncedTimestamp;
    }
    public boolean isUpdated() {
        return !(icupadLastSyncedTimestamp.equals(currentIcupadLastSyncedTimestamp));
    }

    public boolean isUpdatedAfterLongTime() {
        return icupadLastSyncedTimestamp == 0 && currentIcupadLastSyncedTimestamp != 0;
    }

    public Long getIcupadLastSyncedTimestamp() {
        return icupadLastSyncedTimestamp;
    }

    public void setCurrentIcupadLastSyncedTimestamp(Long currentIcupadLastSyncedTimestamp) {
        this.currentIcupadLastSyncedTimestamp = currentIcupadLastSyncedTimestamp;
    }
}