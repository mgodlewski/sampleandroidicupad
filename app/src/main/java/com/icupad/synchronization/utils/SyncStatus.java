package com.icupad.synchronization.utils;

public enum SyncStatus {
    REMOTE_CONFLICT(0), SYNCED(1), DIRTY(2), LOCAL_CONFLICT(3);
    private int value;

    SyncStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
