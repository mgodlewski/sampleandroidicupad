package com.icupad.synchronization.function.ecmo;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.webconnection.dto.ecmo.EcmoProcedureDto;

/**
 * Created by Marcin on 18.03.2017.
 */
public class EcmoProcedureDtoToValuesFunction {
    @NonNull
    public ContentValues apply(EcmoProcedureDto b) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_ID, b.getId());
        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_STAY, b.getStayId());
        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_CREATED_BY, b.getCreatedById());
        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_END_BY, b.getEndById());
        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_START_CAUSE, b.getStartCause());
        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_START_DATE,   DateTimeFormatterHelper.getStringFromMillis(b.getStartDate()));
        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_END_CAUSE, b.getEndCause());
        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_END_DATE,   DateTimeFormatterHelper.getStringFromMillis(b.getEndDate()));
        return values;
    }
}
