package com.icupad.synchronization.function.stethoscope;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.webconnection.dto.stethoscope.AuscultateDto;

public class AuscultateDtoToValuesFunction {
    @NonNull
    public ContentValues apply(AuscultateDto a, SyncStatus syncStatus) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_DESCRIPTION, a.getDescription());
        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_WAV_NAME, a.getWavName());
        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_POINT_ID, a.getAuscultatePointId());
        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_SUITE_ID, a.getAuscultateSuiteId());
        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_POLL_RESULT, a.getPollResult());

        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_ID, a.getId());
        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_CREATED_DATE, DateTimeFormatterHelper.getStringFromMillis(a.getCreatedDateTime()));
        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_LAST_MODIFIED_DATE, DateTimeFormatterHelper.getStringFromMillis(a.getLastModifiedDateTime()));
        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_CREATED_BY_ID, a.getCreatedById());
        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_LAST_MODIFIED_BY_ID, a.getLastModifiedById());
        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_SYNC, syncStatus.getValue());
        return values;
    }
}
