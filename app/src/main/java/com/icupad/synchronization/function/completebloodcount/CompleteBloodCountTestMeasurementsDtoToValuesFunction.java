package com.icupad.synchronization.function.completebloodcount;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.webconnection.dto.bloodgas.BloodGasTestMeasurementDto;
import com.icupad.webconnection.dto.completebloodcount.CompleteBloodCountTestMeasurementDto;

/**
 * Created by Marcin on 10.06.2017.
 */
public class CompleteBloodCountTestMeasurementsDtoToValuesFunction {
    @NonNull
    public ContentValues apply(CompleteBloodCountTestMeasurementDto b, SyncStatus syncStatus) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_NAME, b.getName());
        values.put(IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_UNIT, b.getUnit());
        values.put(IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_BLOOD_SOURCE, b.getBloodSource());
        values.put(IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_VALUE, b.getValue());
        values.put(IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_ABNORMALITY, b.getAbnormality());
        values.put(IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_RESULT_DATE, DateTimeFormatterHelper.getStringFromMillis(b.getResultDate()));
        values.put(IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_BOTTOM_DEFAULT_NORM, b.getBottomDefaultNorm());
        values.put(IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_TOP_DEFAULT_NORM, b.getTopDefaultNorm());
        values.put(IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_STAY_ID, b.getStayId());
        values.put(IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_TEST_ID, b.getTestId());
        values.put(IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_BIRTH_DATE, DateTimeFormatterHelper.getStringFromMillis(b.getBirthDate()));
        values.put(IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_SYNC, syncStatus.getValue());
        return values;
    }
}
