package com.icupad.synchronization.function.ecmo;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.webconnection.dto.ecmo.EcmoActDto;

/**
 * Created by Marcin on 19.03.2017.
 */
public class EcmoActDtoToValuesFunction {
    @NonNull
    public ContentValues apply(EcmoActDto b) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.EcmoAct.TABLE_NAME_ID, b.getId());
        values.put(IcupadDbContract.EcmoAct.TABLE_NAME_PROCEDURE_ID, b.getProcedureId());
        values.put(IcupadDbContract.EcmoAct.TABLE_NAME_USER, b.getCreatedBy());
        values.put(IcupadDbContract.EcmoAct.TABLE_NAME_DATE,  DateTimeFormatterHelper.getStringFromMillis(b.getDate()));
        values.put(IcupadDbContract.EcmoAct.TABLE_NAME_ACT, b.getAct());
        return values;
    }
}
