package com.icupad.synchronization.function.stethoscope;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.webconnection.dto.stethoscope.AuscultateSuiteSchemaDto;

public class AuscultateSuiteSchemaDtoToValuesFunction {

    @NonNull
    public ContentValues apply(AuscultateSuiteSchemaDto input, SyncStatus syncStatus) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_NAME, input.getName());
        values.put(IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_IS_PUBLIC, input.getIsPublic());

        values.put(IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_ID, input.getId());
        values.put(IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_CREATED_DATE, DateTimeFormatterHelper.getStringFromMillis(input.getCreatedDateTime()));
        values.put(IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_LAST_MODIFIED_DATE, DateTimeFormatterHelper.getStringFromMillis(input.getLastModifiedDateTime()));
        values.put(IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_CREATED_BY_ID, input.getCreatedById());
        values.put(IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_LAST_MODIFIED_BY_ID, input.getLastModifiedById());
        values.put(IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_SYNC, syncStatus.getValue());
        return values;
    }
}