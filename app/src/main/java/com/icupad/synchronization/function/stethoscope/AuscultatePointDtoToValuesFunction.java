package com.icupad.synchronization.function.stethoscope;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.webconnection.dto.stethoscope.AuscultatePointDto;

public class AuscultatePointDtoToValuesFunction {
    @NonNull
    public ContentValues apply(AuscultatePointDto input, SyncStatus syncStatus) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_NAME, input.getName());
        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_QUEUE_POSITION, input.getQueuePosition());
        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_X, input.getX());
        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_Y, input.getY());
        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_IS_FRONT, input.isFront());
        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_SUITE_SCHEMA_ID, input.getAuscultateSuiteSchemaId());

        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_ID, input.getId());
        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_CREATED_DATE, DateTimeFormatterHelper.getStringFromMillis(input.getCreatedDateTime()));
        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_LAST_MODIFIED_DATE, DateTimeFormatterHelper.getStringFromMillis(input.getLastModifiedDateTime()));
        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_CREATED_BY_ID, input.getCreatedById());
        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_LAST_MODIFIED_BY_ID, input.getLastModifiedById());
        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_SYNC, syncStatus.getValue());
        return values;
    }
}
