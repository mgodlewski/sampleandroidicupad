package com.icupad.synchronization.function.patient;

import android.content.ContentValues;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.webconnection.dto.patient.StayDto;
import com.icupad.commons.utils.DateTimeFormatterHelper;

public class StayDtoToValuesFunction {
    public ContentValues apply(StayDto input){
        ContentValues contentValues = new ContentValues();
        contentValues.put(IcupadDbContract.Stay.COLUMN_NAME_ID, input.getId());
        contentValues.put(IcupadDbContract.Stay.COLUMN_NAME_PATIENT_ID, input.getPatientId());
        contentValues.put(IcupadDbContract.Stay.COLUMN_NAME_TYPE, input.getStayType());
        contentValues.put(IcupadDbContract.Stay.COLUMN_NAME_ACTIVE, input.isActive());
        contentValues.put(IcupadDbContract.Stay.COLUMN_NAME_ADMIT_DATE, DateTimeFormatterHelper.getStringFromMillis(input.getAdmitDate()));
        contentValues.put(IcupadDbContract.Stay.COLUMN_NAME_ADMITTING_DOCTOR_HL7ID, input.getAdmittingDoctorHl7Id());
        contentValues.put(IcupadDbContract.Stay.COLUMN_NAME_ADMITTING_DOCTOR_NAME, input.getAdmittingDoctorName());
        contentValues.put(IcupadDbContract.Stay.COLUMN_NAME_NPWZ, input.getAdmittingDoctorNpwz());
        contentValues.put(IcupadDbContract.Stay.COLUMN_NAME_SURNAME, input.getAdmittingDoctorSurname());
        contentValues.put(IcupadDbContract.Stay.COLUMN_NAME_NAME, input.getAssignedPatientLocationName());
        contentValues.put(IcupadDbContract.Stay.COLUMN_NAME_DISCHARGE_DATE, DateTimeFormatterHelper.getStringFromMillis(input.getDischargeDate()));
        contentValues.put(IcupadDbContract.Stay.COLUMN_NAME_HL7ID, input.getHl7Id());

        contentValues.put(IcupadDbContract.Stay.COLUMN_NAME_CREATED_DATE, DateTimeFormatterHelper.getStringFromMillis(input.getCreatedDateTime()));
        contentValues.put(IcupadDbContract.Stay.COLUMN_NAME_LAST_MODIFIED_DATE, DateTimeFormatterHelper.getStringFromMillis(input.getLastModifiedDateTime()));
        contentValues.put(IcupadDbContract.Stay.COLUMN_NAME_CREATED_BY_ID, input.getCreatedById());
        contentValues.put(IcupadDbContract.Stay.COLUMN_NAME_LAST_MODIFIED_BY_ID, input.getLastModifiedById());
        contentValues.put(IcupadDbContract.Stay.COLUMN_NAME_SYNC, true);
        return contentValues;
    }
}
