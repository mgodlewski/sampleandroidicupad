package com.icupad.synchronization.function.patient;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.webconnection.dto.patient.OperationDto;

public class OperationDtoToValuesFunction {

    @NonNull
    public ContentValues apply(OperationDto o, SyncStatus syncStatus) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.Operation.COLUMN_NAME_ID, o.getId());
        values.put(IcupadDbContract.Operation.COLUMN_NAME_STAY_ID, o.getStayId());
        values.put(IcupadDbContract.Operation.COLUMN_NAME_OPERATING_DATE, DateTimeFormatterHelper.getStringFromMillis(o.getOperatingDate()));
        values.put(IcupadDbContract.Operation.COLUMN_NAME_DESCRIPTION, o.getDescription());
        values.put(IcupadDbContract.Operation.COLUMN_NAME_ARCHIVED, o.isArchived());

        values.put(IcupadDbContract.Operation.COLUMN_NAME_CREATED_DATE, DateTimeFormatterHelper.getStringFromMillis(o.getCreatedDateTime()));
        values.put(IcupadDbContract.Operation.COLUMN_NAME_LAST_MODIFIED_DATE, DateTimeFormatterHelper.getStringFromMillis(o.getLastModifiedDateTime()));
        values.put(IcupadDbContract.Operation.COLUMN_NAME_CREATED_BY_ID, o.getCreatedById());
        values.put(IcupadDbContract.Operation.COLUMN_NAME_LAST_MODIFIED_BY_ID, o.getLastModifiedById());
        values.put(IcupadDbContract.Operation.COLUMN_NAME_SYNC, syncStatus.getValue());

        return values;
    }
}
