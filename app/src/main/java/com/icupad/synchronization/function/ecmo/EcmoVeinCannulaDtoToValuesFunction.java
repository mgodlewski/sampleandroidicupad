package com.icupad.synchronization.function.ecmo;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.query.QueryBuilder;
import com.icupad.webconnection.dto.ecmo.EcmoConfigurationDto;
import com.icupad.webconnection.dto.ecmo.VeinCannulaDto;

/**
 * Created by Marcin on 18.03.2017.
 */
public class EcmoVeinCannulaDtoToValuesFunction {
    @NonNull
    public ContentValues apply(VeinCannulaDto b, EcmoConfigurationDto e, Long configurationInternalID) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.EcmoVeinCannula.TABLE_NAME_TYPE, b.getType());
        values.put(IcupadDbContract.EcmoVeinCannula.TABLE_NAME_SIZE, b.getSize());
        values.put(IcupadDbContract.EcmoVeinCannula.TABLE_NAME_CONFIGURATION_ID, configurationInternalID);
        values.put(IcupadDbContract.EcmoVeinCannula.TABLE_NAME_DATE, DateTimeFormatterHelper.getStringFromMillis(e.getDate()));
        return values;

    }
}
