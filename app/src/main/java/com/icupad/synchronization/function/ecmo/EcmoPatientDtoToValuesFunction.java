package com.icupad.synchronization.function.ecmo;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.webconnection.dto.ecmo.EcmoPatientDto;

/**
 * Created by Marcin on 18.03.2017.
 */
public class EcmoPatientDtoToValuesFunction {
    @NonNull
    public ContentValues apply(EcmoPatientDto b) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_ID, b.getId());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_PROCEDURE_ID, b.getProcedureId());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_USER, b.getCreatedBy());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_DATE, DateTimeFormatterHelper.getStringFromMillis(b.getDate()));
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_SAT, b.getSat());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_ABP,  b.getAbp());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_VCP, b.getVcp());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_HR,  b.getHr());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_TEMP_SURFACE, b.getTempSurface());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_TEMP_CORE, b.getTempCore());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_DIURESIS, b.getDiuresis());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_ULTRAFILTRACTION, b.getUltrafiltraction());
        return values;
    }
}
