package com.icupad.synchronization.function.ecmo;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.webconnection.dto.ecmo.EcmoConfigurationDto;

/**
 * Created by Marcin on 19.03.2017.
 */
public class EcmoCannulationStructuresDtoToValuesFunction {
    @NonNull
    public ContentValues apply(String s, EcmoConfigurationDto e, Long configurationInternalID) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.EcmoUsedStructures.TABLE_NAME_NAME, s);
        values.put(IcupadDbContract.EcmoUsedStructures.TABLE_NAME_CONFIGURATION_ID, configurationInternalID);
        values.put(IcupadDbContract.EcmoUsedStructures.TABLE_NAME_DATE,  DateTimeFormatterHelper.getStringFromMillis(e.getDate()));
        return values;
    }
}
