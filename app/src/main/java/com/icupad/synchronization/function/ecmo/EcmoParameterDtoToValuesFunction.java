package com.icupad.synchronization.function.ecmo;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.webconnection.dto.ecmo.EcmoParameterDto;

/**
 * Created by Marcin on 18.03.2017.
 */
public class EcmoParameterDtoToValuesFunction {
    @NonNull
    public ContentValues apply(EcmoParameterDto b) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_ID, b.getId());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_PROCEDURE_ID, b.getProcedureId());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_USER, b.getCreatedBy());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_DATE,  DateTimeFormatterHelper.getStringFromMillis(b.getDate()));
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_RPM, b.getRpm());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_BLOOD_FLOW, b.getBloodFlow());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_FIO2, b.getFiO2());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_GAS_FLOW, b.getGasFlow());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_P1, b.getP1());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_P2, b.getP2());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_P3, b.getP3());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_DELTA, b.getDelta());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_HEPARIN_SUPPLY, b.getHeparinSuply());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_DESCRIPTION, b.getDescription());
        return values;
    }
}
