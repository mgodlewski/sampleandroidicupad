package com.icupad.synchronization.function.patient;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.webconnection.dto.patient.PatientDto;

public class PatientDtoToValuesFunction {
    @NonNull
    public ContentValues apply(PatientDto p, SyncStatus syncStatus) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.Patient.COLUMN_NAME_ID, p.getId());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_NAME, p.getName());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_SURNAME, p.getSurname());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_SEX, p.getSex());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_BIRTH_DATE, DateTimeFormatterHelper.getStringFromMillis(p.getBirthDate()));
        values.put(IcupadDbContract.Patient.COLUMN_NAME_PESEL, p.getPesel());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_HL7ID, p.getHl7Id());

        values.put(IcupadDbContract.Patient.COLUMN_NAME_STREET, p.getAddress().getStreet());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_STREET_NUMBER, p.getAddress().getStreetNumber());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_HOUSE_NUMBER, p.getAddress().getHouseNumber());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_POSTAL_CODE, p.getAddress().getPostalCode());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_CITY, p.getAddress().getCity());


        values.put(IcupadDbContract.Patient.COLUMN_NAME_HEIGHT, p.getHeight());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_WEIGHT, p.getWeight());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_BLOOD_TYPE, p.getBloodType());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_ALLERGIES, p.getAllergies());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_OTHER_IMPORTANT_INFORMATIONS, p.getOtherImportantInformations());

        values.put(IcupadDbContract.Patient.COLUMN_NAME_CREATED_DATE, DateTimeFormatterHelper.getStringFromMillis(p.getCreatedDateTime()));
        values.put(IcupadDbContract.Patient.COLUMN_NAME_LAST_MODIFIED_DATE, DateTimeFormatterHelper.getStringFromMillis(p.getLastModifiedDateTime()));
        values.put(IcupadDbContract.Patient.COLUMN_NAME_CREATED_BY_ID, p.getCreatedById());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_LAST_MODIFIED_BY_ID, p.getLastModifiedById());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_SYNC, syncStatus.getValue());
        return values;
    }
}
