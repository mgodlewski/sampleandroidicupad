package com.icupad.synchronization.function.stethoscope;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.webconnection.dto.stethoscope.AuscultateSuiteDto;

public class AuscultateSuiteDtoToValuesFunction {
    @NonNull
    public ContentValues apply(AuscultateSuiteDto a, SyncStatus syncStatus) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_PATIENT_ID, a.getPatientId());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_DESCRIPTION, a.getDescription());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_SUITE_SCHEMA_ID, a.getAuscultateSuiteSchemaId());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_POSITION, a.getPosition());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_TEMPERATURE, a.getTemperature());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_IS_RESPIRATED, a.getIsRespirated());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_PASSIVE_OXYGEN_THERAPY, a.getPassiveOxygenTherapy());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXAMINATION_DATETIME, DateTimeFormatterHelper.getStringFromMillis(a.getExaminationDateTime()));

        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXECUTOR_HL7ID, a.getExecutorHl7Id());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXECUTOR_NAME, a.getExecutorName());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXECUTOR_SURNAME, a.getExecutorSurname());

        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_ID, a.getId());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_CREATED_DATE, DateTimeFormatterHelper.getStringFromMillis(a.getCreatedDateTime()));
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_LAST_MODIFIED_DATE, DateTimeFormatterHelper.getStringFromMillis(a.getLastModifiedDateTime()));
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_CREATED_BY_ID, a.getCreatedById());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_LAST_MODIFIED_BY_ID, a.getLastModifiedById());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_SYNC, syncStatus.getValue());
        return values;
    }
}
