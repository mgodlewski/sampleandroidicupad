package com.icupad.synchronization.function.ecmo;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.query.QueryBuilder;
import com.icupad.webconnection.dto.ecmo.EcmoConfigurationDto;

import net.sqlcipher.database.SQLiteDatabase;

/**
 * Created by Marcin on 18.03.2017.
 */
public class EcmoConfigurationDtoToValuesFunction {
    @NonNull
    public ContentValues apply(EcmoConfigurationDto b, SQLiteDatabase db) {
        Cursor serverProcedureIdCursor = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoProcedure.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.INTERNAL_ID)
                .withSelection(IcupadDbContract.EcmoProcedure.TABLE_NAME_ID+ "=?")
                .withSelectionArgs(b.getProcedureId())
                .build().select();

        if(serverProcedureIdCursor.moveToFirst()) {
            ContentValues values = new ContentValues();
            values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_ID, b.getId());
            values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_PROCEDURE_ID, serverProcedureIdCursor.getLong(0));
            values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_USER, b.getCreatedBy());
            values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_MODE, b.getMode());
            values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_CANIULATION_TYPE, b.getCaniulationType());
            values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_WENTOWANIE_LEFT_VERTICLE, b.getWentowanieLeftVentricle());
            values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_ARTERY_CANNULA_TYPE, b.getArteryCannulaType());
            values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_ARTERY_CANNULA_SIZE, b.getArteryCannulaSize());
            values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_OXYGENERATOR_TYPE, b.getOxygeneratorType());
            values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_ACTUAL, b.getActual());
            values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_DATE,  DateTimeFormatterHelper.getStringFromMillis(b.getDate()));
            serverProcedureIdCursor.close();
            return values;
        }
        return null;
    }
}
