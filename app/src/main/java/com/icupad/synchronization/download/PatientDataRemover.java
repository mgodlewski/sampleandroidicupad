package com.icupad.synchronization.download;

import android.content.Context;

import com.icupad.db.repository.modulerepository.IcupadRepositoryImpl;

public class PatientDataRemover {
    private IcupadRepositoryImpl icupadRepository;

    public PatientDataRemover(Context context) {
        this.icupadRepository = new IcupadRepositoryImpl(context);
    }

    public void removeMedicalDataForPatientId(Long patientId) {
        icupadRepository.removeOperationsForPatientId(patientId);

        icupadRepository.removeAuscultateSuitesForPatientId(patientId);
        icupadRepository.removeAuscultatesForPatientId(patientId);

        icupadRepository.removeBloodGasMeasurementsForPatientId(patientId);
        icupadRepository.removeDefaultMeasurementForPatientId(patientId);
        icupadRepository.removeCompleteBloodCountMeasurementsForPatientId(patientId);
    }
}
