package com.icupad.synchronization.download.stethoscope;

import android.content.ContentValues;
import android.content.Context;

import net.sqlcipher.database.SQLiteDatabase;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.synchronization.download.DownloaderUtils;
import com.icupad.synchronization.function.stethoscope.AuscultateSuiteDtoToValuesFunction;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.synchronization.utils.ThresholdTime;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.dto.stethoscope.AuscultateSuiteDto;

import java.io.IOException;
import java.util.List;

public class AuscultateSuiteDownloader {
    private BackendApi backendApi;
    private IcupadDbHelper mDbHelper;
    private AuscultateSuiteDtoToValuesFunction auscultateSuiteDtoToValuesFunction;
    private SQLiteDatabase db;
    private String tableName;
    private DownloaderUtils downloaderUtils;

    public AuscultateSuiteDownloader(Context context) {
        this.backendApi = new BackendApi(context);
        this.mDbHelper = new IcupadDbHelper(context);
        this.auscultateSuiteDtoToValuesFunction = new AuscultateSuiteDtoToValuesFunction();
        this.tableName = IcupadDbContract.AuscultateSuite.TABLE_NAME;
    }

    public void synchronize(ThresholdTime thresholdTime, List<Long> patientIdsToUpdate) throws IOException {
        List<AuscultateSuiteDto> auscultateSuiteDtos = backendApi.getAuscultateSuitesAfterForPatientsWithIds(thresholdTime.getIcupadLastSyncedTimestamp(), patientIdsToUpdate);

        this.db = mDbHelper.getWritableDatabase();
        this.downloaderUtils = new DownloaderUtils(this.db, this.tableName);
        db.beginTransaction();
        try {
            for(AuscultateSuiteDto a : auscultateSuiteDtos) {
                synchronizeOne(a);
            }
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
            mDbHelper.closeDb(db);
        }
    }

    private void synchronizeOne(AuscultateSuiteDto auscultateSuiteDto) throws IOException {
        long id = auscultateSuiteDto.getId();
        ContentValues values = auscultateSuiteDtoToValuesFunction.apply(auscultateSuiteDto, SyncStatus.SYNCED);

        downloaderUtils.synchronizeOneIfSynced(id, values);
        downloaderUtils.synchronizeOneIfNew(id, values);
    }
}
