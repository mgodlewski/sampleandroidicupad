package com.icupad.synchronization.download;

import android.content.Context;

import com.icupad.db.repository.modulerepository.IcupadRepositoryImpl;
import com.icupad.synchronization.download.patient.PatientDownloader;
import com.icupad.synchronization.download.patient.StayDownloader;
import com.icupad.synchronization.utils.ThresholdTime;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.utils.model.InterruptedDownloadStateModel;
import com.icupad.webconnection.api.BackendApi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

public class PatientAndStayDownloadSynchronizer {
    private boolean IFLOG = true;
    private Logger LOGGER = Logger.getLogger(this.getClass().toString());
    private IcupadRepositoryImpl icupadRepository;

    private PatientDownloader patientDownloader;
    private StayDownloader stayDownloader;

    private List<Long> patientIdsToRemove;
    private List<Long> patientIdsToAdd;
    private List<Long> patientIdsToUpdate;

    private Set<Long> presentPatientsBeforeSynchronize;
    private Set<Long> presentPatientsAfterSynchronize;

    public PatientAndStayDownloadSynchronizer(Context context) {
        this.icupadRepository = new IcupadRepositoryImpl(context);

        this.patientDownloader = new PatientDownloader(context);
        this.stayDownloader = new StayDownloader(context);
    }

    public void synchronize(ThresholdTime thresholdTime, InterruptedDownloadStateModel interruptedDownloadStateModel) throws IOException {
        if(interruptedDownloadStateModel.isSavedInterruptedDownload() && interruptedDownloadStateModel.getDownloadPhase().equals(DownloadSynchronizer.INITIAL_DOWNLOAD_PHASE)) {
            presentPatientsBeforeSynchronize = new HashSet<>(interruptedDownloadStateModel.getPresentPatientsBeforeSynchronize());
            interruptedDownloadStateModel.setSavedInterruptedDownload(false);
            PrivateDataAccessor.cleanInterruptedDownloadState();
        }
        else {
            presentPatientsBeforeSynchronize = icupadRepository.getPresentPatientIds();
            patientDownloader.synchronize(thresholdTime);
        }
        stayDownloader.synchronize(thresholdTime);
        presentPatientsAfterSynchronize = icupadRepository.getPresentPatientIds();

        clearPatientIdLists();
        addProperPatientIdsToUpdateOrToAdd(presentPatientsBeforeSynchronize, presentPatientsAfterSynchronize);
        addProperPatientIdsToRemove(presentPatientsBeforeSynchronize);
    }

    private void clearPatientIdLists() {
        patientIdsToRemove = new ArrayList<>();
        patientIdsToAdd = new ArrayList<>();
        patientIdsToUpdate = new ArrayList<>();
    }

    private void addProperPatientIdsToUpdateOrToAdd(Set<Long> presentPatientsBeforeSynchronize, Set<Long> presentPatientsAfterSynchronize) {
        for(Long patientId : presentPatientsAfterSynchronize) {
            if(presentPatientsBeforeSynchronize.contains(patientId)) {
                patientIdsToUpdate.add(patientId);
                presentPatientsBeforeSynchronize.remove(patientId);
            }
            else {
                patientIdsToAdd.add(patientId);
            }
        }
    }

    private void addProperPatientIdsToRemove(Set<Long> presentPatientsBeforeSynchronize) {
        for(Long patientId : presentPatientsBeforeSynchronize) {
            patientIdsToRemove.add(patientId);
        }
    }

    public Set<Long> getPresentPatientsBeforeSynchronize() {
        return presentPatientsBeforeSynchronize;
    }

    public List<Long> getPatientIdsToUpdate() {
        return patientIdsToUpdate;
    }

    public List<Long> getPatientIdsToAdd() {
        return patientIdsToAdd;
    }

    public List<Long> getPatientIdsToRemove() {
        return patientIdsToRemove;
    }
}
