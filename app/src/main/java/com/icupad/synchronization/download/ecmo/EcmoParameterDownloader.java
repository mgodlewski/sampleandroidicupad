package com.icupad.synchronization.download.ecmo;

import android.content.ContentValues;
import android.content.Context;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.synchronization.download.DownloaderUtils;
import com.icupad.synchronization.function.ecmo.EcmoParameterDtoToValuesFunction;
import com.icupad.synchronization.utils.ThresholdTime;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.dto.bloodgas.BloodGasTestRequestDto;
import com.icupad.webconnection.dto.ecmo.EcmoParameterDto;

import net.sqlcipher.database.SQLiteDatabase;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Marcin on 19.03.2017.
 */
public class EcmoParameterDownloader {
    private BackendApi backendApi;
    private IcupadDbHelper mDbHelper;
    private ThresholdTime thresholdTime;
    private String tableName;
    private DownloaderUtils downloaderUtils;
    private EcmoParameterDtoToValuesFunction ecmoParameterDtoToValuesFunction;

    private Logger LOGGER = Logger.getLogger(this.getClass().toString());

    public EcmoParameterDownloader(Context context) {
        this.backendApi = new BackendApi(context);
        this.mDbHelper = new IcupadDbHelper(context);
        this.tableName = IcupadDbContract.EcmoParameter.TABLE_NAME;
    }

    public void download(ThresholdTime thresholdTime, List<Long> patientIds) throws IOException {
        int page = 0;
        List<EcmoParameterDto> ecmoParameterDtos;
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        this.thresholdTime = thresholdTime;
        this.downloaderUtils = new DownloaderUtils(db, this.tableName);
        this.ecmoParameterDtoToValuesFunction = new EcmoParameterDtoToValuesFunction();

        db.beginTransaction();
        try {
            do {
                ecmoParameterDtos = backendApi.getEcmoParameterAfter(thresholdTime.getIcupadLastSyncedTimestamp(), patientIds, page);

                LOGGER.log(Level.WARNING, "Page: " + page);
                for(EcmoParameterDto p : ecmoParameterDtos) {
                    downloadOne(p);
                }

                page++;
            } while (!ecmoParameterDtos.isEmpty());
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
            mDbHelper.closeDb(db);
        }
    }

    private void downloadOne(EcmoParameterDto ecmoParameterDto) {
        ContentValues values = ecmoParameterDtoToValuesFunction.apply(ecmoParameterDto);
        downloaderUtils.saveOrUpdateOne(ecmoParameterDto.getId(),values);
    }
}
