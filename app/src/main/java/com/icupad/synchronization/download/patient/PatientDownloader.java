package com.icupad.synchronization.download.patient;

import android.content.ContentValues;
import android.content.Context;
import net.sqlcipher.database.SQLiteDatabase;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.synchronization.download.DownloaderUtils;
import com.icupad.synchronization.utils.ThresholdTime;
import com.icupad.synchronization.function.patient.PatientDtoToValuesFunction;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.webconnection.dto.patient.PatientDto;
import com.icupad.webconnection.api.BackendApi;

import java.io.IOException;
import java.util.List;

public class PatientDownloader {
    private BackendApi backendApi;
    private IcupadDbHelper mDbHelper;
    private PatientDtoToValuesFunction patientDtoToValuesFunction;
    private ThresholdTime thresholdTime;
    private String tableName;
    private DownloaderUtils downloaderUtils;

    public PatientDownloader(Context context) {
        this.backendApi = new BackendApi(context);
        this.mDbHelper = new IcupadDbHelper(context);
        this.patientDtoToValuesFunction = new PatientDtoToValuesFunction();
        this.tableName = IcupadDbContract.Patient.TABLE_NAME;
    }

    public void synchronize(ThresholdTime thresholdTime) throws IOException {
        List<PatientDto> patientDtos = backendApi.getPatientsAfter(thresholdTime.getIcupadLastSyncedTimestamp());

        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            this.thresholdTime = thresholdTime;
            this.downloaderUtils = new DownloaderUtils(db, this.tableName);

            for (PatientDto p : patientDtos) {
                synchronizeOne(p);
            }

            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
            mDbHelper.closeDb(db);
        }
    }

    private void synchronizeOne(PatientDto patientDto) {
        ContentValues values = patientDtoToValuesFunction.apply(patientDto, SyncStatus.SYNCED);

        downloaderUtils.synchronizeOneIfSynced(patientDto.getId(), values);
        downloaderUtils.synchronizeOneIfNew(patientDto.getId(), values);
    }
}
