package com.icupad.synchronization.download.ecmo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.QueryBuilder;
import com.icupad.synchronization.download.DownloaderUtils;
import com.icupad.synchronization.function.ecmo.EcmoCannulationStructuresDtoToValuesFunction;
import com.icupad.synchronization.function.ecmo.EcmoConfigurationDtoToValuesFunction;
import com.icupad.synchronization.function.ecmo.EcmoVeinCannulaDtoToValuesFunction;
import com.icupad.synchronization.utils.ThresholdTime;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.dto.ecmo.EcmoActDto;
import com.icupad.webconnection.dto.ecmo.EcmoConfigurationDto;
import com.icupad.webconnection.dto.ecmo.VeinCannulaDto;

import net.sqlcipher.database.SQLiteDatabase;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Marcin on 19.03.2017.
 */
public class EcmoConfigurationDownloader {
    private BackendApi backendApi;
    private IcupadDbHelper mDbHelper;
    private ThresholdTime thresholdTime;
    private DownloaderUtils downloaderUtilsConfiguration;
    private DownloaderUtils downloaderUtilsVeinCannula;
    private DownloaderUtils downloaderUtilsUsedStructures;
    private EcmoConfigurationDtoToValuesFunction ecmoConfigurationDtoToValuesFunction;
    private EcmoVeinCannulaDtoToValuesFunction ecmoVeinCannulaDtoToValuesFunction;
    private EcmoCannulationStructuresDtoToValuesFunction ecmoCannulationStructuresDtoToValuesFunction;

    private Logger LOGGER = Logger.getLogger(this.getClass().toString());

    public EcmoConfigurationDownloader(Context context) {
        this.backendApi = new BackendApi(context);
        this.mDbHelper = new IcupadDbHelper(context);
    }

    public void download(ThresholdTime thresholdTime, List<Long> patientIds) throws IOException {
        int page = 0;
        List<EcmoConfigurationDto> ecmoConfigurationDtos;
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        this.thresholdTime = thresholdTime;
        this.downloaderUtilsConfiguration = new DownloaderUtils(db, IcupadDbContract.EcmoConfiguration.TABLE_NAME);
        this.downloaderUtilsVeinCannula = new DownloaderUtils(db, IcupadDbContract.EcmoVeinCannula.TABLE_NAME);
        this.downloaderUtilsUsedStructures = new DownloaderUtils(db, IcupadDbContract.EcmoUsedStructures.TABLE_NAME);
        this.ecmoConfigurationDtoToValuesFunction = new EcmoConfigurationDtoToValuesFunction();
        this.ecmoVeinCannulaDtoToValuesFunction = new EcmoVeinCannulaDtoToValuesFunction();
        this.ecmoCannulationStructuresDtoToValuesFunction = new EcmoCannulationStructuresDtoToValuesFunction();

        db.beginTransaction();
        try {
            do {
                ecmoConfigurationDtos = backendApi.getEcmoConfigurationAfter(thresholdTime.getIcupadLastSyncedTimestamp(), patientIds, page);

                LOGGER.log(Level.WARNING, "Page: " + page);
                for(EcmoConfigurationDto p : ecmoConfigurationDtos) {
                    downloadOne(p, db);
                }

                page++;
            } while (!ecmoConfigurationDtos.isEmpty());
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
            mDbHelper.closeDb(db);
        }
    }

    private void downloadOne(EcmoConfigurationDto ecmoConfigurationDto, SQLiteDatabase db) {
        ContentValues values = ecmoConfigurationDtoToValuesFunction.apply(ecmoConfigurationDto, db);
        downloaderUtilsConfiguration.saveOrUpdateOne(ecmoConfigurationDto.getId(),values);

        Cursor serverConfigurationIdCursor = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoConfiguration.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.INTERNAL_ID)
                .withSelection(IcupadDbContract.EcmoConfiguration.TABLE_NAME_ID+ "=?")
                .withSelectionArgs(ecmoConfigurationDto.getId())
                .build().select();
        if(serverConfigurationIdCursor.moveToFirst()) {

            for (VeinCannulaDto v : ecmoConfigurationDto.getVeinCannulaList()) {
                ContentValues veinValues = ecmoVeinCannulaDtoToValuesFunction.apply(v, ecmoConfigurationDto,  serverConfigurationIdCursor.getLong(0));
                downloaderUtilsVeinCannula.saveOne(veinValues);
            }

            for (String s : ecmoConfigurationDto.getCannulationStructures()) {
                ContentValues structuresValues = ecmoCannulationStructuresDtoToValuesFunction.apply(s, ecmoConfigurationDto,  serverConfigurationIdCursor.getLong(0));
                downloaderUtilsUsedStructures.saveOne(structuresValues);
            }
        }
        serverConfigurationIdCursor.close();
    }
}
