package com.icupad.synchronization.download.defaulttest;

import android.content.ContentValues;
import android.content.Context;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.synchronization.download.DownloaderUtils;
import com.icupad.synchronization.function.defaulttest.DefaultTestMeasurementsDtoToValuesFunction;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.synchronization.utils.ThresholdTime;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.dto.defaulttest.DefaultTestMeasurementDto;

import net.sqlcipher.database.SQLiteDatabase;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Marcin on 10.06.2017.
 */
public class DefaultTestMeasurementsDownloader {
    private BackendApi backendApi;
    private IcupadDbHelper mDbHelper;
    private DefaultTestMeasurementsDtoToValuesFunction defaultTestMeasurementsDtoToValuesFunction;
    private ThresholdTime thresholdTime;
    private String tableName;
    private DownloaderUtils downloaderUtils;

    public DefaultTestMeasurementsDownloader(Context context) {
        this.backendApi = new BackendApi(context);
        this.mDbHelper = new IcupadDbHelper(context);
        this.tableName = IcupadDbContract.DefaultTestMeasurement.TABLE_NAME;
        defaultTestMeasurementsDtoToValuesFunction = new DefaultTestMeasurementsDtoToValuesFunction();
    }

    private Logger LOGGER = Logger.getLogger(this.getClass().toString());
    private Long startTime;
    int count = 0, countAll, percentPrintGranurality = 10, percentToPrint = 0;

    public void synchronize(ThresholdTime thresholdTime, List<Long> patientIds) throws IOException {
        int page = 0;
        List<DefaultTestMeasurementDto> defaultTestMeasurementDto;

        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        this.thresholdTime = thresholdTime;
        this.downloaderUtils = new DownloaderUtils(db, this.tableName);
        db.beginTransaction();
        try {
            do {
                defaultTestMeasurementDto = backendApi.getDefaultTestsAfterForPatientsWithIds(thresholdTime.getIcupadLastSyncedTimestamp(), patientIds, page);

                LOGGER.log(Level.WARNING, "Page: " + page);
                countAll = defaultTestMeasurementDto.size();
                startTime = System.currentTimeMillis() / 1000;
                for (DefaultTestMeasurementDto b : defaultTestMeasurementDto) {
                    synchronizeOne(b);
                    getTimePrintln();
                }

                page++;
                count = 0;
                percentToPrint = 0;
            } while (!defaultTestMeasurementDto.isEmpty());
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            mDbHelper.closeDb(db);
        }
    }

    private void getTimePrintln() {
        count++;
        int percent = (count * 100) / countAll;
        if(percent >= percentToPrint) {
            percentToPrint += percentPrintGranurality;
            Long currentTime = System.currentTimeMillis() / 1000;
            Long deltaTime = (currentTime - startTime);
            LOGGER.log(Level.WARNING, "[test11]RequestDownloader passed time: " + (deltaTime / 60) + "min " + (deltaTime % 60) + "s for: [" + percent + "%] " + count + "/" + countAll);
            startTime = currentTime;
        }
    }

    private void synchronizeOne(DefaultTestMeasurementDto defaultTestMeasurementDto) {
        ContentValues values = defaultTestMeasurementsDtoToValuesFunction.apply(defaultTestMeasurementDto, SyncStatus.SYNCED);
        downloaderUtils.saveOne(values);
    }
}
