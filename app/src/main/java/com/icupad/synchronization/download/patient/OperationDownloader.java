package com.icupad.synchronization.download.patient;

import android.content.ContentValues;
import android.content.Context;
import net.sqlcipher.database.SQLiteDatabase;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.synchronization.download.DownloaderUtils;
import com.icupad.synchronization.function.patient.OperationDtoToValuesFunction;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.synchronization.utils.ThresholdTime;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.dto.patient.OperationDto;

import java.io.IOException;
import java.util.List;

public class OperationDownloader {
    private BackendApi backendApi;
    private IcupadDbHelper mDbHelper;
    private OperationDtoToValuesFunction operationDtoToValuesFunction;
    private ThresholdTime thresholdTime;
    private String tableName;
    private DownloaderUtils downloaderUtils;

    public OperationDownloader(Context context) {
        this.backendApi = new BackendApi(context);
        this.mDbHelper = new IcupadDbHelper(context);
        this.operationDtoToValuesFunction = new OperationDtoToValuesFunction();
        this.tableName = IcupadDbContract.Operation.TABLE_NAME;
    }

    public void synchronize(ThresholdTime thresholdTime, List<Long> patientIdsToUpdate) throws IOException {
        List<OperationDto> operationDtos = backendApi.getOperationsAfterForPatientsWithIds(thresholdTime.getIcupadLastSyncedTimestamp(), patientIdsToUpdate);

        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        this.thresholdTime = thresholdTime;
        this.downloaderUtils = new DownloaderUtils(db, this.tableName);
        db.beginTransaction();
        try {
            for(OperationDto operationDto : operationDtos) {
                synchronizeOne(operationDto);
            }
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
            mDbHelper.closeDb(db);
        }
    }

    private void synchronizeOne(OperationDto operationDto) {
        ContentValues values = operationDtoToValuesFunction.apply(operationDto, SyncStatus.SYNCED);

        downloaderUtils.synchronizeOneIfSynced(operationDto.getId(), values);
        downloaderUtils.synchronizeOneIfNew(operationDto.getId(), values);
    }
}
