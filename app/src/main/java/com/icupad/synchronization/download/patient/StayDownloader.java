package com.icupad.synchronization.download.patient;

import android.content.ContentValues;
import android.content.Context;
import net.sqlcipher.database.SQLiteDatabase;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.synchronization.download.DownloaderUtils;
import com.icupad.synchronization.utils.ThresholdTime;
import com.icupad.synchronization.function.patient.StayDtoToValuesFunction;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.dto.patient.StayDto;

import java.io.IOException;
import java.util.List;

public class StayDownloader {
    private BackendApi backendApi;
    private IcupadDbHelper mDbHelper;
    private ThresholdTime thresholdTime;
    private String tableName;
    private StayDtoToValuesFunction stayDtoToValuesFunction;
    private DownloaderUtils downloaderUtils;

    public StayDownloader(Context context) {
        this.backendApi = new BackendApi(context);
        this.mDbHelper = new IcupadDbHelper(context);
        this.stayDtoToValuesFunction = new StayDtoToValuesFunction();
        this.tableName = IcupadDbContract.Stay.TABLE_NAME;
    }

    public void synchronize(ThresholdTime thresholdTime) throws IOException {
        List<StayDto> stayDtos = backendApi.getStaysAfter(thresholdTime.getIcupadLastSyncedTimestamp());

        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        this.thresholdTime = thresholdTime;
        this.downloaderUtils = new DownloaderUtils(db, this.tableName);
        db.beginTransaction();
        try {
            for(StayDto stayDto : stayDtos) {
                synchronizeOne(stayDto);
            }
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
            mDbHelper.closeDb(db);
        }
    }

    private void synchronizeOne(StayDto stayDto) {
        ContentValues contentValues = stayDtoToValuesFunction.apply(stayDto);

        downloaderUtils.synchronizeOneIfSynced(stayDto.getId(), contentValues);
        downloaderUtils.synchronizeOneIfNew(stayDto.getId(), contentValues);
    }
}