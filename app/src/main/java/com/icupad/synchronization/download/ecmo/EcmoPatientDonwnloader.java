package com.icupad.synchronization.download.ecmo;

import android.content.ContentValues;
import android.content.Context;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.synchronization.download.DownloaderUtils;
import com.icupad.synchronization.function.ecmo.EcmoPatientDtoToValuesFunction;
import com.icupad.synchronization.utils.ThresholdTime;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.dto.ecmo.EcmoActDto;
import com.icupad.webconnection.dto.ecmo.EcmoPatientDto;

import net.sqlcipher.database.SQLiteDatabase;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Marcin on 19.03.2017.
 */
public class EcmoPatientDonwnloader {
    private BackendApi backendApi;
    private IcupadDbHelper mDbHelper;
    private ThresholdTime thresholdTime;
    private String tableName;
    private DownloaderUtils downloaderUtils;
    private EcmoPatientDtoToValuesFunction ecmoPatientDtoToValuesFunction;

    private Logger LOGGER = Logger.getLogger(this.getClass().toString());

    public EcmoPatientDonwnloader(Context context) {
        this.backendApi = new BackendApi(context);
        this.mDbHelper = new IcupadDbHelper(context);
        this.tableName = IcupadDbContract.EcmoPatient.TABLE_NAME;
    }

    public void download(ThresholdTime thresholdTime, List<Long> patientIds) throws IOException {
        int page = 0;
        List<EcmoPatientDto> ecmoPatientDtos;
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        this.thresholdTime = thresholdTime;
        this.downloaderUtils = new DownloaderUtils(db, this.tableName);
        this.ecmoPatientDtoToValuesFunction = new EcmoPatientDtoToValuesFunction();

        db.beginTransaction();
        try {
            do {
                ecmoPatientDtos = backendApi.getEcmoPatientAfter(thresholdTime.getIcupadLastSyncedTimestamp(), patientIds, page);

                LOGGER.log(Level.WARNING, "Page: " + page);
                for(EcmoPatientDto p : ecmoPatientDtos) {
                    downloadOne(p);
                }

                page++;
            } while (!ecmoPatientDtos.isEmpty());
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
            mDbHelper.closeDb(db);
        }
    }

    private void downloadOne(EcmoPatientDto ecmoPatientDtos) {
        ContentValues values = ecmoPatientDtoToValuesFunction.apply(ecmoPatientDtos);
        downloaderUtils.saveOrUpdateOne(ecmoPatientDtos.getId(),values);
    }
}
