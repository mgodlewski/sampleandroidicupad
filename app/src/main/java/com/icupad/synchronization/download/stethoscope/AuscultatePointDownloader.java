package com.icupad.synchronization.download.stethoscope;

import android.content.ContentValues;
import android.content.Context;
import net.sqlcipher.database.SQLiteDatabase;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.synchronization.download.DownloaderUtils;
import com.icupad.synchronization.utils.ThresholdTime;
import com.icupad.synchronization.function.stethoscope.AuscultatePointDtoToValuesFunction;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.dto.stethoscope.AuscultatePointDto;

import java.io.IOException;
import java.util.List;

public class AuscultatePointDownloader {
    private BackendApi backendApi;
    private IcupadDbHelper mDbHelper;
    private AuscultatePointDtoToValuesFunction auscultatePointDtoToValuesFunction;
    private ThresholdTime thresholdTime;
    private String tableName;
    private DownloaderUtils downloaderUtils;

    public AuscultatePointDownloader(Context context) {
        this.backendApi = new BackendApi(context);
        this.mDbHelper = new IcupadDbHelper(context);
        this.auscultatePointDtoToValuesFunction = new AuscultatePointDtoToValuesFunction();
        this.tableName = IcupadDbContract.AuscultatePoint.TABLE_NAME;
    }

    public void synchronize(ThresholdTime thresholdTime) throws IOException {
        List<AuscultatePointDto> auscultatePointDtos = backendApi.getAuscultatePointsAfter(thresholdTime.getIcupadLastSyncedTimestamp());

        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        this.thresholdTime = thresholdTime;
        this.downloaderUtils = new DownloaderUtils(db, this.tableName);
        db.beginTransaction();
        try {
            for(AuscultatePointDto auscultatePointDto : auscultatePointDtos) {
                synchronizeOne(auscultatePointDto);
            }
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
            mDbHelper.closeDb(db);
        }
    }

    private void synchronizeOne(AuscultatePointDto auscultatePointDto) {

        ContentValues values = auscultatePointDtoToValuesFunction.apply(auscultatePointDto, SyncStatus.SYNCED);

        downloaderUtils.synchronizeOneIfSynced(auscultatePointDto.getId(), values);
        downloaderUtils.synchronizeOneIfNew(auscultatePointDto.getId(), values);
    }
}