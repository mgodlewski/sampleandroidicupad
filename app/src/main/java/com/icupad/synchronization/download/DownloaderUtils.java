package com.icupad.synchronization.download;

import android.content.ContentValues;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.synchronization.utils.SyncStatus;

import java.util.logging.Logger;

//TODO refactor
public class DownloaderUtils {

    private Logger LOGGER = Logger.getLogger(this.getClass().toString());
    static private Long deltaReadableSum = new Long(0);
    static private Long deltaWritableSum = new Long(0);
    private Long startTime;

    private SQLiteDatabase db;
    private String tableName;

    public DownloaderUtils(SQLiteDatabase db, String tableName) {
        this.db = db;
        this.tableName = tableName;
    }

    public void synchronizeOneIfSynced(long id, ContentValues values) {
        if(checkIfSynced(id)) {
            prepareLog();
            getQueryForAllWithId(id)
                    .update(values);
//            log("synchronizeOneIfSynced");
        }
    }

    public void synchronizeOneIfNew(long id, ContentValues values) {
        if(checkIfNew(id)) {
            prepareLog();
            getQueryForAll()
                    .insert(values);
//            log("synchronizeOneIfNew");
        }
    }

    public void synchronizeOne(long id, ContentValues values){
        if(checkIfNew(id)) {
            prepareLog();
            getQueryForAll()
                    .insert(values);
        }else if(checkIfSynced(id)) {
            prepareLog();
            getQueryForAllWithId(id)
                    .update(values);
        }
    }

    public void saveOrUpdateOne(long id, ContentValues values){
        if(checkIfNew(id)) {
            saveOne(values);
        }else{
            prepareLog();
            getQueryForAllWithId(id)
                    .update(values);
//            log("updateOne");
        }
    }

    public void saveOne(ContentValues values){
//        prepareLog();
        getQueryForAll()
                .insert(values);
//        log("saveNewOne");
    }

    private Query getQueryForAll() {
        return QueryBuilder.aQuery(db)
                .withTable(tableName)
                .build();
    }

    private boolean checkIfSynced(long id) {
        prepareLog();
        Cursor cursor = getQueryForAllWithIdAndSynced(id)
                .select();
        boolean isSynced = cursor.moveToFirst();
        cursor.close();
//        log("checkIfSynced");
        return isSynced;
    }

    private boolean checkIfNew(long id) {
        prepareLog();
        Cursor cursor = getQueryForAllWithId(id)
                .select();
        boolean isNew = !cursor.moveToFirst();
        cursor.close();
//        log("checkIfNew");
        return isNew;
    }


    private Query getQueryForAllWithId(long id) {
        return QueryBuilder.aQuery(db)
                .withTable(tableName)
                .withSelection(IcupadDbContract.ID + "=?")
                .withSelectionArgs(id)
                .build();
    }

    private Query getQueryForAllWithIdAndSynced(long id) {
        return QueryBuilder.aQuery(db)
                .withTable(tableName)
                .withSelection(IcupadDbContract.ID + "=? and " +
                        IcupadDbContract.COLUMN_NAME_SYNC + "=" + SyncStatus.SYNCED.getValue())
                .withSelectionArgs(id)
                .build();
    }

    private void prepareLog() {
        startTime = System.currentTimeMillis();
    }

    private void log(String text) {
        long delta = (System.currentTimeMillis() - startTime);
        deltaReadableSum += delta;
        LOGGER.info("[test11] " + text + ", delta: " + delta + " millis [" + deltaReadableSum + "]");
    }
}
