package com.icupad.synchronization.download.stethoscope;

import android.content.ContentValues;
import android.content.Context;
import net.sqlcipher.database.SQLiteDatabase;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.synchronization.download.DownloaderUtils;
import com.icupad.synchronization.function.stethoscope.AuscultateDtoToValuesFunction;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.synchronization.utils.ThresholdTime;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.dto.stethoscope.AuscultateDto;

import java.io.IOException;
import java.util.List;

public class AuscultateDownloader {
    private BackendApi backendApi;
    private IcupadDbHelper mDbHelper;
    private AuscultateDtoToValuesFunction auscultateDtoToValuesFunction;
    private ThresholdTime thresholdTime;
    private String tableName;
    private DownloaderUtils downloaderUtils;

    public AuscultateDownloader(Context context) {
        this.backendApi = new BackendApi(context);
        this.mDbHelper = new IcupadDbHelper(context);
        this.auscultateDtoToValuesFunction = new AuscultateDtoToValuesFunction();
        this.tableName = IcupadDbContract.Auscultate.TABLE_NAME;
    }

    public void synchronize(ThresholdTime thresholdTime, List<Long> patientIdsToUpdate) throws IOException {
        List<AuscultateDto> auscultateDtos = backendApi.getAuscultatesAfterForPatientsWithIds(thresholdTime.getIcupadLastSyncedTimestamp(), patientIdsToUpdate);

        this.thresholdTime = thresholdTime;
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        this.downloaderUtils = new DownloaderUtils(db, this.tableName);
        db.beginTransaction();
        try {
            for(AuscultateDto auscultateDto : auscultateDtos) {
                synchronizeOne(auscultateDto);
            }
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
            mDbHelper.closeDb(db);
        }
    }

    private void synchronizeOne(AuscultateDto auscultateDto) {
        ContentValues values = auscultateDtoToValuesFunction.apply(auscultateDto, SyncStatus.SYNCED);

        downloaderUtils.synchronizeOneIfSynced(auscultateDto.getId(), values);
        downloaderUtils.synchronizeOneIfNew(auscultateDto.getId(), values);
    }
}
