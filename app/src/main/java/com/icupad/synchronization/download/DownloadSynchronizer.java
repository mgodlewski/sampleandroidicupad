package com.icupad.synchronization.download;

import android.content.Context;

import com.icupad.synchronization.download.bloodgas.BloodGasTestMeasurementsDownloader;
import com.icupad.synchronization.download.completebloodcount.CompleteBloodCountTestMeasurementsDownloader;
import com.icupad.synchronization.download.defaulttest.DefaultTestMeasurementsDownloader;
import com.icupad.synchronization.download.ecmo.EcmoActDownloader;
import com.icupad.synchronization.download.ecmo.EcmoConfigurationDownloader;
import com.icupad.synchronization.download.ecmo.EcmoParameterDownloader;
import com.icupad.synchronization.download.ecmo.EcmoPatientDonwnloader;
import com.icupad.synchronization.download.ecmo.EcmoProcedureDownloader;
import com.icupad.synchronization.download.patient.OperationDownloader;
import com.icupad.synchronization.download.stethoscope.AuscultateDownloader;
import com.icupad.synchronization.download.stethoscope.AuscultatePointDownloader;
import com.icupad.synchronization.download.stethoscope.AuscultateSuiteDownloader;
import com.icupad.synchronization.download.stethoscope.AuscultateSuiteSchemaDownloader;
import com.icupad.utils.exception.InterruptedDownloadException;
import com.icupad.synchronization.utils.ThresholdTime;
import com.icupad.utils.LoggedUserInfo;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.utils.model.InterruptedDownloadStateModel;
import com.icupad.utils.model.builder.InterruptedDownloadStateModelBuilder;
import com.icupad.webconnection.api.BackendApi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DownloadSynchronizer {
    private static final String PATIENT_AND_STAY_LABEL = "patient and stay";
    private static final String OPERATION_LABEL = "operation";
    private static final String AUSCULTATE_SUITE_SCHEMA_LABEL = "auscultateSuiteSchema";
    private static final String AUSCULTATE_POINT_LABEL = "auscultatePoint";
    private static final String AUSCULTATE_SUITE_LABEL = "auscultateSuite";
    private static final String AUSCULTATE_LABEL = "auscultate";
    private static final String BLOOD_GAS_TEST_MEASUREMENT_LABEL = "bloodGasTestMeasurement";
    private static final String ECMO_PROCEDURE_LABEL = "ecmoProcedure";
    private static final String ECMO_PATIENT_LABEL = "ecmoPatient";
    private static final String ECMO_PARAMETER_LABEL = "ecmoParameter";
    private static final String ECMO_CONFIGURATION_LABEL = "ecmoConfiguration";
    private static final String ECMO_ACT_LABEL = "ecmoAct";
    private static final String ADDING_DOWNLOAD_PHASE = "add";
    private static final String UPDATING_DOWNLOAD_PHASE = "update";
    public static final String INITIAL_DOWNLOAD_PHASE = "initial";
    private static final String COMPLETE_BLOOD_COUNT_TEST_MEASUREMENT_LABEL = "completeBloodCountTestMeasurement";
    private static final String DEFAULT_TEST_MEASUREMENT_LABEL = "defaultTestMeasurement";
    private boolean IFLOG = true;
    private Logger LOGGER = Logger.getLogger(this.getClass().toString());
    private BackendApi backendApi;

    private PatientDataRemover patientDataRemover;

    private PatientAndStayDownloadSynchronizer patientAndStayDownloadSynchronizer;

    private OperationDownloader operationDownloader;

    private AuscultateSuiteSchemaDownloader auscultateSuiteSchemaDownloader;
    private AuscultatePointDownloader auscultatePointDownloader;
    private AuscultateSuiteDownloader auscultateSuiteDownloader;
    private AuscultateDownloader auscultateDownloader;

    private BloodGasTestMeasurementsDownloader bloodGasTestDefaultNormsDownloader;
    private DefaultTestMeasurementsDownloader defaultTestDefaultNormsDownloader;
    private CompleteBloodCountTestMeasurementsDownloader completeBloodCountTestDefaultNormsDownloader;

    private EcmoProcedureDownloader ecmoProcedureDownloader;
    private EcmoPatientDonwnloader ecmoPatientDonwnloader;
    private EcmoParameterDownloader ecmoParameterDownloader;
    private EcmoConfigurationDownloader ecmoConfigurationDownloader;
    private EcmoActDownloader ecmoActDownloader;

    private Long startTime;
    private ThresholdTime thresholdTime;

    public DownloadSynchronizer(Context context) {
        backendApi = new BackendApi(context);

        this.patientDataRemover = new PatientDataRemover(context);

        this.patientAndStayDownloadSynchronizer = new PatientAndStayDownloadSynchronizer(context);

        this.operationDownloader = new OperationDownloader(context);

        this.auscultateSuiteSchemaDownloader = new AuscultateSuiteSchemaDownloader(context);
        this.auscultatePointDownloader = new AuscultatePointDownloader(context);
        this.auscultateSuiteDownloader = new AuscultateSuiteDownloader(context);
        this.auscultateDownloader = new AuscultateDownloader(context);

        this.bloodGasTestDefaultNormsDownloader = new BloodGasTestMeasurementsDownloader(context);
        this.defaultTestDefaultNormsDownloader = new DefaultTestMeasurementsDownloader(context);
        this.completeBloodCountTestDefaultNormsDownloader = new CompleteBloodCountTestMeasurementsDownloader(context);

        this.ecmoProcedureDownloader = new EcmoProcedureDownloader(context);
        this.ecmoPatientDonwnloader = new EcmoPatientDonwnloader(context);
        this.ecmoParameterDownloader = new EcmoParameterDownloader(context);
        this.ecmoConfigurationDownloader = new EcmoConfigurationDownloader(context);
        this.ecmoActDownloader = new EcmoActDownloader(context);
    }

    public void synchronizeAndUpdateLastTimestamps(ThresholdTime thresholdTime) throws InterruptedDownloadException {
        this.thresholdTime = thresholdTime;
        List<Long> patientIdsToAdd = new ArrayList<>();
        List<Long> patientIdsToRemove = new ArrayList<>();
        List<Long> patientIdsToUpdate = new ArrayList<>();
        InterruptedDownloadStateModel interruptedDownloadStateModel = PrivateDataAccessor.loadInterruptedDownloadState();
        try {
            thresholdTime.setCurrentIcupadLastSyncedTimestamp(backendApi.getIcupadLastTimestamp());
            startTime = System.currentTimeMillis()/1000;
            getTimePrintln("general", "START");

            if(interruptedDownloadStateModel.isSavedInterruptedDownload() && !interruptedDownloadStateModel.getDataLabel().equals(PATIENT_AND_STAY_LABEL)) {
                patientIdsToAdd = interruptedDownloadStateModel.getPatientIdsToAdd();
                patientIdsToRemove = interruptedDownloadStateModel.getPatientIdsToRemove();
                patientIdsToUpdate = interruptedDownloadStateModel.getPatientIdsToUpdate();

                interruptedDownloadStateModel.setSavedInterruptedDownload(false);
                PrivateDataAccessor.cleanInterruptedDownloadState();
            }
            else {
                patientAndStayDownloadSynchronizer.synchronize(thresholdTime, interruptedDownloadStateModel);
                getTimePrintln("synchronize", PATIENT_AND_STAY_LABEL);

                patientIdsToAdd = patientAndStayDownloadSynchronizer.getPatientIdsToAdd();
                patientIdsToRemove = patientAndStayDownloadSynchronizer.getPatientIdsToRemove();
                patientIdsToUpdate = patientAndStayDownloadSynchronizer.getPatientIdsToUpdate();
                Iterator<Long> iterator = patientIdsToAdd.iterator();
//                while(iterator.hasNext()) {
//                    Long patientId = iterator.next();
//
//                    if(patientId.equals(765L) || patientId.equals(757L)) {
//                        iterator.remove();
//                    }
//                }

                interruptedDownloadStateModel.setPatientIdsToAdd(patientIdsToAdd);
                interruptedDownloadStateModel.setPatientIdsToRemove(patientIdsToRemove);
                interruptedDownloadStateModel.setPatientIdsToUpdate(patientIdsToUpdate);
                interruptedDownloadStateModel.setPresentPatientsBeforeSynchronize(new ArrayList<>(patientAndStayDownloadSynchronizer.getPresentPatientsBeforeSynchronize()));

                throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, INITIAL_DOWNLOAD_PHASE, PATIENT_AND_STAY_LABEL);
            }
            addPatientsWithIdsStartingWith(patientIdsToAdd, interruptedDownloadStateModel);
            removePatientsWithIdsStartingWith(patientIdsToRemove);
            updatePatientsWithIdsStartingWith(patientIdsToUpdate, interruptedDownloadStateModel);

            getTimePrintln("general", "FINISH");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedDownloadException e) {
            InterruptedDownloadStateModel stateModel = e.getInterruptedDownloadStateModel();
            stateModel.setSavedInterruptedDownload(true);
            stateModel.setPatientIdsToAdd(patientIdsToAdd);
            stateModel.setPatientIdsToRemove(patientIdsToRemove);
            stateModel.setPatientIdsToUpdate(patientIdsToUpdate);

            PrivateDataAccessor.saveInterruptedDownloadState(stateModel);
            throw new InterruptedDownloadException(stateModel);
        }
    }

    private void addPatientsWithIdsStartingWith(List<Long> patientIdsToAdd, InterruptedDownloadStateModel interruptedDownloadStateModel) throws IOException, InterruptedDownloadException {
        if( !patientIdsToAdd.isEmpty()) {
            LOGGER.log(Level.WARNING, patientIdsToAdd.toString());
            ThresholdTime thresholdTimeWithZeros = new ThresholdTime();

            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, ADDING_DOWNLOAD_PHASE, OPERATION_LABEL)) {
                operationDownloader.synchronize(thresholdTimeWithZeros, patientIdsToAdd);
            }
            getTimePrintln(ADDING_DOWNLOAD_PHASE, OPERATION_LABEL);



            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, ADDING_DOWNLOAD_PHASE, AUSCULTATE_SUITE_SCHEMA_LABEL)) {
                auscultateSuiteSchemaDownloader.synchronize(thresholdTimeWithZeros);
            }
            getTimePrintln(ADDING_DOWNLOAD_PHASE, AUSCULTATE_SUITE_SCHEMA_LABEL);
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, ADDING_DOWNLOAD_PHASE, AUSCULTATE_POINT_LABEL)) {
                auscultatePointDownloader.synchronize(thresholdTimeWithZeros);
            }
            getTimePrintln(ADDING_DOWNLOAD_PHASE, AUSCULTATE_POINT_LABEL);
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, ADDING_DOWNLOAD_PHASE, AUSCULTATE_SUITE_LABEL)) {
                auscultateSuiteDownloader.synchronize(thresholdTimeWithZeros, patientIdsToAdd);
            }
            getTimePrintln(ADDING_DOWNLOAD_PHASE, AUSCULTATE_SUITE_LABEL);
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, ADDING_DOWNLOAD_PHASE, AUSCULTATE_LABEL)) {
                auscultateDownloader.synchronize(thresholdTimeWithZeros, patientIdsToAdd);
            }
            getTimePrintln(ADDING_DOWNLOAD_PHASE, AUSCULTATE_LABEL);


            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, ADDING_DOWNLOAD_PHASE, BLOOD_GAS_TEST_MEASUREMENT_LABEL)) {
                bloodGasTestDefaultNormsDownloader.synchronize(thresholdTimeWithZeros, patientIdsToAdd);
            }
            getTimePrintln(ADDING_DOWNLOAD_PHASE, BLOOD_GAS_TEST_MEASUREMENT_LABEL);
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, ADDING_DOWNLOAD_PHASE, COMPLETE_BLOOD_COUNT_TEST_MEASUREMENT_LABEL)) {
                completeBloodCountTestDefaultNormsDownloader.synchronize(thresholdTimeWithZeros, patientIdsToAdd);
            }
            getTimePrintln(ADDING_DOWNLOAD_PHASE, COMPLETE_BLOOD_COUNT_TEST_MEASUREMENT_LABEL);
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, ADDING_DOWNLOAD_PHASE, DEFAULT_TEST_MEASUREMENT_LABEL)) {
                defaultTestDefaultNormsDownloader.synchronize(thresholdTimeWithZeros, patientIdsToAdd);
            }
            getTimePrintln(ADDING_DOWNLOAD_PHASE, DEFAULT_TEST_MEASUREMENT_LABEL);


            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, ADDING_DOWNLOAD_PHASE, ECMO_PROCEDURE_LABEL)) {
                ecmoProcedureDownloader.download(thresholdTimeWithZeros, patientIdsToAdd);
            }
            getTimePrintln(ADDING_DOWNLOAD_PHASE, ECMO_PROCEDURE_LABEL);
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, ADDING_DOWNLOAD_PHASE, ECMO_PATIENT_LABEL)) {
                ecmoPatientDonwnloader.download(thresholdTimeWithZeros, patientIdsToAdd);
            }
            getTimePrintln(ADDING_DOWNLOAD_PHASE, ECMO_PATIENT_LABEL);
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, ADDING_DOWNLOAD_PHASE, ECMO_PARAMETER_LABEL)) {
                ecmoParameterDownloader.download(thresholdTimeWithZeros, patientIdsToAdd);
            }
            getTimePrintln(ADDING_DOWNLOAD_PHASE, ECMO_PARAMETER_LABEL);
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, ADDING_DOWNLOAD_PHASE, ECMO_CONFIGURATION_LABEL)) {
                ecmoConfigurationDownloader.download(thresholdTimeWithZeros, patientIdsToAdd);
            }
            getTimePrintln(ADDING_DOWNLOAD_PHASE, ECMO_CONFIGURATION_LABEL);
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, ADDING_DOWNLOAD_PHASE, ECMO_ACT_LABEL)) {
                ecmoActDownloader.download(thresholdTimeWithZeros, patientIdsToAdd);
            }
            getTimePrintln(ADDING_DOWNLOAD_PHASE, ECMO_CONFIGURATION_LABEL);
        }
    }

    private boolean throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(InterruptedDownloadStateModel interruptedDownloadStateModel, String downloadPhase, String dataLabel) throws InterruptedDownloadException {
        if(LoggedUserInfo.isLoggedIn()) {
            if( !interruptedDownloadStateModel.isSavedInterruptedDownload()) {
                return true;
            }
            if(interruptedDownloadStateModel.isSavedInterruptedDownload() &&
                        interruptedDownloadStateModel.getDownloadPhase().equals(downloadPhase) &&
                        interruptedDownloadStateModel.getDataLabel().equals(dataLabel)) {
                interruptedDownloadStateModel.setSavedInterruptedDownload(false);
                PrivateDataAccessor.cleanInterruptedDownloadState();
                return true;
            }
            return false;
        }
        else {
            InterruptedDownloadStateModel stateModel = InterruptedDownloadStateModelBuilder
                    .anInterruptedDownloadStateModel()
                        .withDownloadPhase(downloadPhase)
                        .withDataLabel(dataLabel)
                        .build();
            throw new InterruptedDownloadException(stateModel);
        }
    }

    private void updatePatientsWithIdsStartingWith(List<Long> patientIdsToUpdate, InterruptedDownloadStateModel interruptedDownloadStateModel) throws IOException, InterruptedDownloadException {
        if( !patientIdsToUpdate.isEmpty()) {
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, UPDATING_DOWNLOAD_PHASE, OPERATION_LABEL)) {
                operationDownloader.synchronize(thresholdTime, patientIdsToUpdate);
            }
            getTimePrintln(UPDATING_DOWNLOAD_PHASE, OPERATION_LABEL);



            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, UPDATING_DOWNLOAD_PHASE, AUSCULTATE_SUITE_SCHEMA_LABEL)) {
                auscultateSuiteSchemaDownloader.synchronize(thresholdTime);
            }
            getTimePrintln(UPDATING_DOWNLOAD_PHASE, AUSCULTATE_SUITE_SCHEMA_LABEL);
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, UPDATING_DOWNLOAD_PHASE, AUSCULTATE_POINT_LABEL)) {
                auscultatePointDownloader.synchronize(thresholdTime);
            }
            getTimePrintln(UPDATING_DOWNLOAD_PHASE, AUSCULTATE_POINT_LABEL);
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, UPDATING_DOWNLOAD_PHASE, AUSCULTATE_SUITE_LABEL)) {
                auscultateSuiteDownloader.synchronize(thresholdTime, patientIdsToUpdate);
            }
            getTimePrintln(UPDATING_DOWNLOAD_PHASE, AUSCULTATE_SUITE_LABEL);
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, UPDATING_DOWNLOAD_PHASE, AUSCULTATE_LABEL)) {
                auscultateDownloader.synchronize(thresholdTime, patientIdsToUpdate);
            }
            getTimePrintln(UPDATING_DOWNLOAD_PHASE, AUSCULTATE_LABEL);


            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, UPDATING_DOWNLOAD_PHASE, BLOOD_GAS_TEST_MEASUREMENT_LABEL)) {
                bloodGasTestDefaultNormsDownloader.synchronize(thresholdTime, patientIdsToUpdate);
            }
            getTimePrintln(UPDATING_DOWNLOAD_PHASE, BLOOD_GAS_TEST_MEASUREMENT_LABEL);
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, UPDATING_DOWNLOAD_PHASE, COMPLETE_BLOOD_COUNT_TEST_MEASUREMENT_LABEL)) {
                completeBloodCountTestDefaultNormsDownloader.synchronize(thresholdTime, patientIdsToUpdate);
            }
            getTimePrintln(UPDATING_DOWNLOAD_PHASE, COMPLETE_BLOOD_COUNT_TEST_MEASUREMENT_LABEL);
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, UPDATING_DOWNLOAD_PHASE, DEFAULT_TEST_MEASUREMENT_LABEL)) {
                defaultTestDefaultNormsDownloader.synchronize(thresholdTime, patientIdsToUpdate);
            }
            getTimePrintln(UPDATING_DOWNLOAD_PHASE, DEFAULT_TEST_MEASUREMENT_LABEL);


            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, ADDING_DOWNLOAD_PHASE, ECMO_PROCEDURE_LABEL)) {
                ecmoProcedureDownloader.download(thresholdTime, patientIdsToUpdate);
            }
            getTimePrintln(ADDING_DOWNLOAD_PHASE, ECMO_PROCEDURE_LABEL);
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, ADDING_DOWNLOAD_PHASE, ECMO_PATIENT_LABEL)) {
                ecmoPatientDonwnloader.download(thresholdTime, patientIdsToUpdate);
            }
            getTimePrintln(ADDING_DOWNLOAD_PHASE, ECMO_PATIENT_LABEL);
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, ADDING_DOWNLOAD_PHASE, ECMO_PARAMETER_LABEL)) {
                ecmoParameterDownloader.download(thresholdTime, patientIdsToUpdate);
            }
            getTimePrintln(ADDING_DOWNLOAD_PHASE, ECMO_PARAMETER_LABEL);
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, ADDING_DOWNLOAD_PHASE, ECMO_CONFIGURATION_LABEL)) {
                ecmoConfigurationDownloader.download(thresholdTime, patientIdsToUpdate);
            }
            getTimePrintln(ADDING_DOWNLOAD_PHASE, ECMO_CONFIGURATION_LABEL);
            if(throwExceptionIfUserLoggedOutAndCheckInterruptedDownloadAndReturnIfSynchronizeThis(interruptedDownloadStateModel, ADDING_DOWNLOAD_PHASE, ECMO_ACT_LABEL)) {
                ecmoActDownloader.download(thresholdTime, patientIdsToUpdate);
            }
            getTimePrintln(ADDING_DOWNLOAD_PHASE, ECMO_ACT_LABEL);
        }
    }

    private void removePatientsWithIdsStartingWith(List<Long> patientIdsToRemove) {
        for(Long patientId : patientIdsToRemove) {
            patientDataRemover.removeMedicalDataForPatientId(patientId);
            getTimePrintln("remove", "for patientId=" + patientId);
        }
    }

    private void getTimePrintln(String group, String downloader) {
        if(IFLOG) {
            Long currentTime = System.currentTimeMillis() / 1000;
            Long deltaTime = (currentTime - startTime);
            LOGGER.log(Level.WARNING, "[test11]Passed time: " + (deltaTime / 60) + "min " + (deltaTime % 60) + "s for: [" + group + "] "+ downloader);
        }
    }
}
