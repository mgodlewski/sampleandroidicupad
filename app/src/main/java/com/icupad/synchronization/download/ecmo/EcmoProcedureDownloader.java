package com.icupad.synchronization.download.ecmo;

import android.content.ContentValues;
import android.content.Context;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.synchronization.download.DownloaderUtils;
import com.icupad.synchronization.function.ecmo.EcmoProcedureDtoToValuesFunction;
import com.icupad.synchronization.utils.ThresholdTime;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.dto.ecmo.EcmoActDto;
import com.icupad.webconnection.dto.ecmo.EcmoProcedureDto;

import net.sqlcipher.database.SQLiteDatabase;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Marcin on 18.03.2017.
 */
public class EcmoProcedureDownloader {
    private BackendApi backendApi;
    private IcupadDbHelper mDbHelper;
    private ThresholdTime thresholdTime;
    private String tableName;
    private DownloaderUtils downloaderUtils;
    private EcmoProcedureDtoToValuesFunction ecmoProcedureDtoToValuesFunction;

    private Logger LOGGER = Logger.getLogger(this.getClass().toString());

    public EcmoProcedureDownloader(Context context) {
        this.backendApi = new BackendApi(context);
        this.mDbHelper = new IcupadDbHelper(context);
        this.tableName = IcupadDbContract.EcmoProcedure.TABLE_NAME;
    }

    public void download(ThresholdTime thresholdTime, List<Long> patientIds) throws IOException {
        int page = 0;
        List<EcmoProcedureDto> ecmoProcedureDtos;
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        this.thresholdTime = thresholdTime;
        this.downloaderUtils = new DownloaderUtils(db, this.tableName);
        this.ecmoProcedureDtoToValuesFunction = new EcmoProcedureDtoToValuesFunction();

        db.beginTransaction();
        try {
            do {
                ecmoProcedureDtos = backendApi.getEcmoProcedureAfter(thresholdTime.getIcupadLastSyncedTimestamp(), patientIds, page);

                LOGGER.log(Level.WARNING, "Page: " + page);
                for(EcmoProcedureDto p : ecmoProcedureDtos) {
                    downloadOne(p);
                }

                page++;
            } while (!ecmoProcedureDtos.isEmpty());
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
            mDbHelper.closeDb(db);
        }
    }

    private void downloadOne(EcmoProcedureDto ecmoProcedureDto) {
        ContentValues values = ecmoProcedureDtoToValuesFunction.apply(ecmoProcedureDto);
        downloaderUtils.saveOrUpdateOne(ecmoProcedureDto.getId(),values);
    }
}
