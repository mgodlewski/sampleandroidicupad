package com.icupad.synchronization.download.stethoscope;

import android.content.ContentValues;
import android.content.Context;

import net.sqlcipher.database.SQLiteDatabase;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.synchronization.download.DownloaderUtils;
import com.icupad.synchronization.utils.ThresholdTime;
import com.icupad.synchronization.function.stethoscope.AuscultateSuiteSchemaDtoToValuesFunction;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.webconnection.dto.stethoscope.AuscultateSuiteSchemaDto;
import com.icupad.webconnection.api.BackendApi;

import java.io.IOException;
import java.util.List;

public class AuscultateSuiteSchemaDownloader {
    private BackendApi backendApi;
    private IcupadDbHelper mDbHelper;
    private AuscultateSuiteSchemaDtoToValuesFunction auscultateSuiteSchemaDtoToValuesFunction;
    private SQLiteDatabase db;
    private String tableName;
    private DownloaderUtils downloaderUtils;

    public AuscultateSuiteSchemaDownloader(Context context) {
        this.backendApi = new BackendApi(context);
        this.mDbHelper = new IcupadDbHelper(context);
        this.auscultateSuiteSchemaDtoToValuesFunction = new AuscultateSuiteSchemaDtoToValuesFunction();
        this.tableName = IcupadDbContract.AuscultateSuiteSchema.TABLE_NAME;
    }

    public void synchronize(ThresholdTime thresholdTime) throws IOException {
        List<AuscultateSuiteSchemaDto> auscultateSuiteSchemaDtos = backendApi.getAuscultateSuiteSchemasAfter(thresholdTime.getIcupadLastSyncedTimestamp());
        this.db = mDbHelper.getWritableDatabase();
        this.downloaderUtils = new DownloaderUtils(this.db, this.tableName);
        db.beginTransaction();
        try {
            for(AuscultateSuiteSchemaDto a : auscultateSuiteSchemaDtos) {
                synchronizeOne(a);
            }
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
            mDbHelper.closeDb(db);
        }
    }

    private void synchronizeOne(AuscultateSuiteSchemaDto auscultateSuiteSchemaDto) throws IOException {
        long id = auscultateSuiteSchemaDto.getId();
        ContentValues values = auscultateSuiteSchemaDtoToValuesFunction.apply(auscultateSuiteSchemaDto, SyncStatus.SYNCED);

        downloaderUtils.synchronizeOneIfSynced(id, values);
        downloaderUtils.synchronizeOneIfNew(id, values);
    }
}
