package com.icupad.synchronization.upload.responsedto.stethoscope;

public class AuscultateRecordingSyncResponseDto {
    private Boolean success;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
