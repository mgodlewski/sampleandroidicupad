package com.icupad.synchronization.upload;

import android.content.Context;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.synchronization.upload.service.ecmo.EcmoActUploader;
import com.icupad.synchronization.upload.service.ecmo.EcmoConfigurationUploader;
import com.icupad.synchronization.upload.service.ecmo.EcmoParameterUploader;
import com.icupad.synchronization.upload.service.ecmo.EcmoPatientUploader;
import com.icupad.synchronization.upload.service.ecmo.EcmoProcedureUploader;
import com.icupad.synchronization.upload.service.patient.OperationUploader;
import com.icupad.synchronization.upload.service.stethoscope.AuscultatePointUploader;
import com.icupad.synchronization.upload.service.stethoscope.AuscultateSuiteSchemaUploader;
import com.icupad.synchronization.upload.service.stethoscope.AuscultateSuiteUploader;
import com.icupad.synchronization.upload.service.patient.PatientUploader;
import com.icupad.synchronization.upload.service.stethoscope.AuscultateUploader;
import com.icupad.utils.PrivateDataAccessor;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UploadSynchronizer {
    private Logger LOGGER = Logger.getLogger(this.getClass().toString());

    private IcupadDbHelper mDbHelper;

    private PatientUploader patientUploader;
    private OperationUploader operationUploader;

    private AuscultateSuiteSchemaUploader auscultateSuiteSchemaUploader;
    private AuscultatePointUploader auscultatePointUploader;
    private AuscultateSuiteUploader auscultateSuiteUploader;
    private AuscultateUploader auscultateUploader;

    private EcmoProcedureUploader ecmoProcedureUploader;
    private EcmoConfigurationUploader ecmoConfigurationUploader;
    private EcmoActUploader ecmoActUploader;
    private EcmoPatientUploader ecmoPatientUploader;
    private EcmoParameterUploader ecmoParameterUploader;

    public UploadSynchronizer(Context context) {
        mDbHelper = new IcupadDbHelper(context);

        patientUploader = new PatientUploader(context);
        operationUploader = new OperationUploader(context);

        auscultateSuiteSchemaUploader = new AuscultateSuiteSchemaUploader(context);
        auscultatePointUploader = new AuscultatePointUploader(context);
        auscultateSuiteUploader = new AuscultateSuiteUploader(context);
        auscultateUploader = new AuscultateUploader(context);

        ecmoProcedureUploader = new EcmoProcedureUploader(context);
        ecmoConfigurationUploader = new EcmoConfigurationUploader(context);
        ecmoActUploader = new EcmoActUploader(context);
        ecmoPatientUploader = new EcmoPatientUploader(context);
        ecmoParameterUploader = new EcmoParameterUploader(context);
    }

    public void synchronize() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        Cursor cursor = getQueryForSyncs(db).select();
        try {
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                LOGGER.log(Level.INFO, "table name: " + cursor.getString(0)+ " id:" + cursor.getLong(1));
                uploadOne(cursor.getString(0), cursor.getLong(1));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        cursor.close();
        mDbHelper.closeDb(db);
    }

    private void uploadOne(String tableName, long internalId) throws IOException {
        switch(tableName) {
            case IcupadDbContract.Patient.TABLE_NAME:
                patientUploader.upload(internalId);
                break;
            case IcupadDbContract.Operation.TABLE_NAME:
                operationUploader.upload(internalId);
                break;
            case IcupadDbContract.AuscultateSuiteSchema.TABLE_NAME:
                auscultateSuiteSchemaUploader.upload(internalId);
                break;
            case IcupadDbContract.AuscultatePoint.TABLE_NAME:
                auscultatePointUploader.upload(internalId);
                break;
            case IcupadDbContract.AuscultateSuite.TABLE_NAME:
                auscultateSuiteUploader.upload(internalId);
                break;
            case IcupadDbContract.Auscultate.TABLE_NAME:
                auscultateUploader.upload(internalId);
                break;
            case IcupadDbContract.EcmoProcedure.TABLE_NAME:
                ecmoProcedureUploader.upload(internalId);
                break;
            case IcupadDbContract.EcmoConfiguration.TABLE_NAME:
                ecmoConfigurationUploader.upload(internalId);
                break;
            case IcupadDbContract.EcmoAct.TABLE_NAME:
                ecmoActUploader.upload(internalId);
                break;
            case IcupadDbContract.EcmoPatient.TABLE_NAME:
                ecmoPatientUploader.upload(internalId);
                break;
            case IcupadDbContract.EcmoParameter.TABLE_NAME:
                ecmoParameterUploader.upload(internalId);
                break;
        }
    }

    private Query getQueryForSyncs(SQLiteDatabase db) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.SyncTable.TABLE_NAME)
                .withColumns(IcupadDbContract.SyncTable.COLUMN_NAME_TABLE_NAME,
                        IcupadDbContract.SyncTable.COLUMN_NAME_INFO_INTERNAL_ID)
                .build();
    }
}
