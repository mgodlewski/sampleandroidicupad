package com.icupad.synchronization.upload.responsedto.ecmo;

import com.google.gson.annotations.SerializedName;
import com.icupad.webconnection.dto.ecmo.EcmoActDto;

/**
 * Created by Marcin on 02.04.2017.
 */
public class EcmoActSyncResponseDto {
    @SerializedName("success")
    private Boolean success;

    @SerializedName("ecmoActDto")
    private EcmoActDto ecmoActDto;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public EcmoActDto getEcmoActDto() {
        return ecmoActDto;
    }

    public void setEcmoActDto(EcmoActDto ecmoActDto) {
        this.ecmoActDto = ecmoActDto;
    }
}
