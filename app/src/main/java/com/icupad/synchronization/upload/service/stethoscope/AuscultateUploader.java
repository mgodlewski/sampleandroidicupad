package com.icupad.synchronization.upload.service.stethoscope;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import android.os.Environment;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.synchronization.conflict.resolver.stethoscope.AuscultateConflictResolver;
import com.icupad.synchronization.upload.responsedto.stethoscope.AuscultateRecordingSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.stethoscope.AuscultateSyncResponseDto;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.builder.stethoscope.AuscultateDtoBuilder;
import com.icupad.webconnection.dto.stethoscope.AuscultateDto;

import java.io.File;
import java.io.IOException;

public class AuscultateUploader {
    private IcupadDbHelper mDbHelper;
    private BackendApi backendApi;
    private SyncTableRepository syncTableRepository;
    private String tableName;

    private AuscultateConflictResolver auscultateConflictResolver;

    public AuscultateUploader(Context context) {
        this.mDbHelper = new IcupadDbHelper(context);
        this.backendApi = new BackendApi(context);
        this.syncTableRepository = new SyncTableRepository(context);

        this.tableName = IcupadDbContract.Auscultate.TABLE_NAME;
        auscultateConflictResolver = new AuscultateConflictResolver(context);
    }

    public void upload(long internalId) throws IOException {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = getQueryForModelToUploadWithInternalId(db, internalId)
                .select();
        AuscultateDto auscultateDto = convertFirstFromCursorToModelGson(cursor);

        if(uploadAuscultateWavIfItsNewAndReturnSuccessStatus(auscultateDto)) {
            tryUploadAuscultateDto(db, internalId, auscultateDto);
        }

        cursor.close();
        mDbHelper.closeDb(db);
    }

    private Query getQueryForModelToUploadWithInternalId(SQLiteDatabase db, long internalId) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.Auscultate.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.Auscultate.COLUMN_NAME_DESCRIPTION,
                        IcupadDbContract.Auscultate.COLUMN_NAME_WAV_NAME,
                        IcupadDbContract.Auscultate.COLUMN_NAME_POINT_ID,
                        IcupadDbContract.Auscultate.COLUMN_NAME_SUITE_ID,
                        IcupadDbContract.Auscultate.COLUMN_NAME_POLL_RESULT,

                        IcupadDbContract.Auscultate.COLUMN_NAME_ID,
                        IcupadDbContract.Auscultate.COLUMN_NAME_CREATED_DATE,
                        IcupadDbContract.Auscultate.COLUMN_NAME_LAST_MODIFIED_DATE,
                        IcupadDbContract.Auscultate.COLUMN_NAME_CREATED_BY_ID,
                        IcupadDbContract.Auscultate.COLUMN_NAME_LAST_MODIFIED_BY_ID)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                .withSelectionArgs(internalId)
                .build();
    }

    private AuscultateDto convertFirstFromCursorToModelGson(Cursor cursor) {
        if(cursor.moveToFirst()) {
            AuscultateDto auscultateDto = AuscultateDtoBuilder.anAuscultateDto()
                    .withDescription(cursor.getString(0))
                    .withWavName(cursor.getString(1))
                    .withAuscultatePointId(cursor.getLong(2))
                    .withAuscultateSuiteId(cursor.getLong(3))
                    .withPollResult(cursor.getLong(4))

                    .withId(cursor.getLong(5))
                    .withCreatedDateTime(DateTimeFormatterHelper.getMillisFromString(cursor.getString(6)))
                    .withLastModifiedDateTime(DateTimeFormatterHelper.getMillisFromString(cursor.getString(7)))
                    .withCreatedById(cursor.getLong(8))
                    .withLastModifiedById(cursor.getLong(9))
                    .build();
            return auscultateDto;
        }
        return null;
    }

    private boolean uploadAuscultateWavIfItsNewAndReturnSuccessStatus(AuscultateDto auscultateDto) {
        if(auscultateDto.getId() == null || auscultateDto.getId().equals(0L)) {
            File file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS) + "/icupad", auscultateDto.getWavName());

            try {
                AuscultateRecordingSyncResponseDto response = backendApi.uploadAuscultateRecording(file);
                if(response != null) {
                    return response.getSuccess();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }
        else {
            return true;
        }
    }

    private void tryUploadAuscultateDto(SQLiteDatabase db, long internalId, AuscultateDto auscultateDto) throws IOException {
        if(auscultateDto != null) {
            AuscultateSyncResponseDto response = backendApi.uploadAuscultate(auscultateDto);
            reactOnResponseAfterUpload(db, internalId, response);
        }
    }

    private void reactOnResponseAfterUpload(SQLiteDatabase db, long internalId, AuscultateSyncResponseDto response) {
        if(response != null && response.getSuccess() != null) {
            if (response.getSuccess()) {
                syncTableRepository.updateSucceed(tableName, internalId);
                updateAuscultateWithResponseDto(db, internalId, response.getAuscultateDto());
            }
            else {
                auscultateConflictResolver.logConflict(internalId, response.getAuscultateDto());
            }
        }
    }

    private void updateAuscultateWithResponseDto(SQLiteDatabase db, long internalId, AuscultateDto response) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.ID, response.getId());
        values.put(IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_BY_ID, response.getLastModifiedById());
        values.put(IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_DATE, response.getLastModifiedDateTime());
        values.put(IcupadDbContract.COLUMN_NAME_CREATED_BY_ID, response.getCreatedById());
        values.put(IcupadDbContract.COLUMN_NAME_CREATED_DATE, response.getCreatedDateTime());

        QueryBuilder.aQuery(db).withTable(tableName)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?").withSelectionArgs(internalId)
                .build().update(values);
    }
}
