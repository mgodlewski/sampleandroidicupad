package com.icupad.synchronization.upload.responsedto.patient;

import com.google.gson.annotations.SerializedName;
import com.icupad.webconnection.dto.patient.OperationDto;

public class OperationSyncResponseDto {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("operationDto")
    private OperationDto operationDto;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public OperationDto getOperationDto() {
        return operationDto;
    }

    public void setOperationDto(OperationDto operationDto) {
        this.operationDto = operationDto;
    }
}
