package com.icupad.synchronization.upload.service.ecmo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.synchronization.upload.responsedto.ecmo.EcmoConfigurationSyncResponseDto;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.builder.ecmo.EcmoConfigurationDtoBuilder;
import com.icupad.webconnection.builder.ecmo.VeinCannulaDtoBuilder;
import com.icupad.webconnection.dto.ecmo.EcmoConfigurationDto;
import com.icupad.webconnection.dto.ecmo.VeinCannulaDto;

import net.sqlcipher.database.SQLiteDatabase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 02.04.2017.
 */
public class EcmoConfigurationUploader {
    private IcupadDbHelper mDbHelper;
    private BackendApi backendApi;
    private String tableName;
    private SyncTableRepository syncTableRepository;

    public EcmoConfigurationUploader(Context context) {
        this.mDbHelper = new IcupadDbHelper(context);
        this.backendApi = new BackendApi(context);
        this.tableName = IcupadDbContract.EcmoConfiguration.TABLE_NAME;
        this.syncTableRepository = new SyncTableRepository(context);
    }

    public void upload(long internalId) throws IOException {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor configurationCursor = getQueryForConfigurationModelToUploadWithInternalId(db, internalId)
                .select();

        EcmoConfigurationDto ecmoConfigurationDto = convertFirstFromCursorToModel(db, configurationCursor, internalId);

        tryUploadConfigurationDto(db, internalId, ecmoConfigurationDto);

        configurationCursor.close();
        mDbHelper.closeDb(db);
    }

    private Query getQueryForConfigurationModelToUploadWithInternalId(SQLiteDatabase db, long internalId) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoConfiguration.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_ID,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_PROCEDURE_ID,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_USER,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_MODE,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_CANIULATION_TYPE,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_WENTOWANIE_LEFT_VERTICLE,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_ARTERY_CANNULA_TYPE,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_ARTERY_CANNULA_SIZE,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_OXYGENERATOR_TYPE,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_ACTUAL,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_DATE)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                .withSelectionArgs(internalId)
                .build();
    }

    private Query getQueryForVeinCannulaModelToUploadWithInternalId(SQLiteDatabase db, long id) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoVeinCannula.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.EcmoVeinCannula.TABLE_NAME_TYPE,
                        IcupadDbContract.EcmoVeinCannula.TABLE_NAME_SIZE)
                .withSelection(IcupadDbContract.EcmoVeinCannula.TABLE_NAME_CONFIGURATION_ID + "=?")
                .withSelectionArgs(id)
                .build();
    }

    private Query getQueryForUsedStructuresModelToUploadWithInternalId(SQLiteDatabase db, long id) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoUsedStructures.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.EcmoUsedStructures.TABLE_NAME_NAME)
                .withSelection(IcupadDbContract.EcmoUsedStructures.TABLE_NAME_CONFIGURATION_ID + "=?")
                .withSelectionArgs(id)
                .build();
    }

    private EcmoConfigurationDto convertFirstFromCursorToModel(SQLiteDatabase db, Cursor cursorConfiguration, long internalId) {
        if(cursorConfiguration.moveToFirst()) {
            Cursor veinCannulaCursor = getQueryForVeinCannulaModelToUploadWithInternalId(db, internalId).select();
            List<VeinCannulaDto> veinCannulaDtos = new ArrayList<>();
            veinCannulaCursor.moveToFirst();
            while(!veinCannulaCursor.isAfterLast()) {
                VeinCannulaDto veinCannulaDto = VeinCannulaDtoBuilder.aVeinCannulaDtoBuilder()
                        .withType(veinCannulaCursor.getString(0))
                        .withSize(veinCannulaCursor.getDouble(1))
                        .build();
                veinCannulaDtos.add(veinCannulaDto);
                veinCannulaCursor.moveToNext();
            }
            veinCannulaCursor.close();

            Cursor usedStructuresCursor = getQueryForUsedStructuresModelToUploadWithInternalId(db, internalId).select();
            List<String> usedStructures = new ArrayList<>();
            usedStructuresCursor.moveToFirst();
            while(!usedStructuresCursor.isAfterLast()) {
                usedStructures.add(usedStructuresCursor.getString(0));
                usedStructuresCursor.moveToNext();
            }
            usedStructuresCursor.close();

            Cursor serverProcedureIdCursor = QueryBuilder.aQuery(db)
                    .withTable(IcupadDbContract.EcmoProcedure.TABLE_NAME)
                    .withColumns(
                            IcupadDbContract.EcmoProcedure.TABLE_NAME_ID)
                    .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                    .withSelectionArgs(cursorConfiguration.getLong(1))
                    .build().select();
            if(serverProcedureIdCursor.moveToFirst()) {

                EcmoConfigurationDto ecmoConfigurationDto = EcmoConfigurationDtoBuilder.aEcmoConfigurationDtoBuilder()
                        .withId(cursorConfiguration.getLong(0))
//                        .withProcedureId(cursorConfiguration.getLong(1))//zamiast internaId powinno być id z serwera
                        .withProcedureId(serverProcedureIdCursor.getLong(0))
                        .withCreatedBy(cursorConfiguration.getLong(2))
                        .withMode(cursorConfiguration.getString(3))
                        .withCaniulationType(cursorConfiguration.getString(4))
                        .withWentowanieLeftVentricle(cursorConfiguration.getString(5))
                        .withArteryCannulaType(cursorConfiguration.getString(6))
                        .withArteryCannulaSize(cursorConfiguration.getDouble(7))
                        .withOxygeneratorType(cursorConfiguration.getString(8))
                        .withActual(cursorConfiguration.getInt(9)!= 0)
                        .withDate(DateTimeFormatterHelper.getMillisFromString(cursorConfiguration.getString(10)))
                        .withCannulationStructures(usedStructures)
                        .withVeinCannulaList(veinCannulaDtos)
                        .build();
                serverProcedureIdCursor.close();
                return ecmoConfigurationDto;
            }
            serverProcedureIdCursor.close();
        }
        return null;
    }

    private void tryUploadConfigurationDto(SQLiteDatabase db, long internalId, EcmoConfigurationDto ecmoConfigurationDto) throws IOException {
        if(ecmoConfigurationDto != null) {
            EcmoConfigurationSyncResponseDto response = backendApi.createEcmoConfiguration(ecmoConfigurationDto);
            reactOnResponseAfterUpload(db, internalId, response);
        }
    }

    private void reactOnResponseAfterUpload(SQLiteDatabase db, long internalId, EcmoConfigurationSyncResponseDto response){
        if(response != null && response.getSuccess() != null) {
            if (response.getSuccess()) {
                syncTableRepository.updateSucceedSyncTable(tableName, internalId);
                updateWithResponseDto(db, internalId, response.getEcmoConfigurationDto());
            }
        }
    }

    private void updateWithResponseDto(SQLiteDatabase db, long internalId, EcmoConfigurationDto ecmoConfigurationDto) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.ID, ecmoConfigurationDto.getId());
//        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_PROCEDURE_ID, ecmoConfigurationDto.getProcedureId());
//        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_USER, ecmoConfigurationDto.getCreatedBy());
//        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_MODE, ecmoConfigurationDto.getMode());
//        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_CANIULATION_TYPE, ecmoConfigurationDto.getCaniulationType());
//        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_WENTOWANIE_LEFT_VERTICLE, ecmoConfigurationDto.getWentowanieLeftVentricle());
//        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_ARTERY_CANNULA_TYPE, ecmoConfigurationDto.getArteryCannulaType());
//        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_ARTERY_CANNULA_SIZE, ecmoConfigurationDto.getArteryCannulaSize());
//        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_OXYGENERATOR_TYPE, ecmoConfigurationDto.getOxygeneratorType());
//        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_ACTUAL, ecmoConfigurationDto.getActual());
//        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_DATE, ecmoConfigurationDto.getDate());
        QueryBuilder.aQuery(db).withTable(tableName)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?").withSelectionArgs(internalId)
                .build().update(values);
    }
}
