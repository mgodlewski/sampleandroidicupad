package com.icupad.synchronization.upload.service.ecmo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.synchronization.upload.responsedto.ecmo.EcmoActSyncResponseDto;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.builder.ecmo.EcmoActDtoBuilder;
import com.icupad.webconnection.dto.ecmo.EcmoActDto;

import net.sqlcipher.database.SQLiteDatabase;

import java.io.IOException;

/**
 * Created by Marcin on 03.04.2017.
 */
public class EcmoActUploader {
    private IcupadDbHelper mDbHelper;
    private BackendApi backendApi;
    private String tableName;
    private SyncTableRepository syncTableRepository;

    public EcmoActUploader(Context context) {
        this.mDbHelper = new IcupadDbHelper(context);
        this.backendApi = new BackendApi(context);
        this.tableName = IcupadDbContract.EcmoAct.TABLE_NAME;
        this.syncTableRepository = new SyncTableRepository(context);
    }

    public void upload(long internalId) throws IOException {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = getQueryForConfigurationModelToUploadWithInternalId(db, internalId)
                .select();

        EcmoActDto ecmoActDto = convertFirstFromCursorToModel(db, cursor);

        tryUploadConfigurationDto(db, internalId, ecmoActDto);

        cursor.close();
        mDbHelper.closeDb(db);
    }

    private Query getQueryForConfigurationModelToUploadWithInternalId(SQLiteDatabase db, long internalId) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoAct.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.EcmoAct.TABLE_NAME_ID,
                        IcupadDbContract.EcmoAct.TABLE_NAME_PROCEDURE_ID,
                        IcupadDbContract.EcmoAct.TABLE_NAME_USER,
                        IcupadDbContract.EcmoAct.TABLE_NAME_ACT,
                        IcupadDbContract.EcmoAct.TABLE_NAME_DATE)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                .withSelectionArgs(internalId)
                .build();
    }

    private EcmoActDto convertFirstFromCursorToModel(SQLiteDatabase db, Cursor cursor) {
        if(cursor.moveToFirst()) {
            Long id = cursor.getLong(1);
            Cursor procedureIdcursor = QueryBuilder.aQuery(db)
                    .withTable(IcupadDbContract.EcmoProcedure.TABLE_NAME)
                    .withColumns(IcupadDbContract.EcmoProcedure.TABLE_NAME_ID)
                    .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                    .withSelectionArgs(cursor.getLong(1))
                    .build().select();
            if(procedureIdcursor.moveToFirst()){
                EcmoActDto ecmoActDto = EcmoActDtoBuilder.aEcmoActDtoBuilder()
                        .withId(null)
                        .withProcedureId(procedureIdcursor.getLong(0))
                        .withCreatedBy(cursor.getLong(2))
                        .withAct(cursor.getDouble(3))
                        .withDate(DateTimeFormatterHelper.getMillisFromString(cursor.getString(4)))
                        .build();
                procedureIdcursor.close();
                return ecmoActDto;
            }
            procedureIdcursor.close();
        }
        return null;
    }

    private void tryUploadConfigurationDto(SQLiteDatabase db, long internalId, EcmoActDto ecmoActDto) throws IOException {
        if(ecmoActDto != null) {
            EcmoActSyncResponseDto response = backendApi.createEcmoAct(ecmoActDto);
            reactOnResponseAfterUpload(db, internalId, response);
        }
    }

    private void reactOnResponseAfterUpload(SQLiteDatabase db, long internalId, EcmoActSyncResponseDto response){
        if(response != null && response.getSuccess() != null) {
            if (response.getSuccess()) {
                syncTableRepository.updateSucceedSyncTable(tableName, internalId);
//                updateActWithResponseDto(db, internalId, response.getEcmoActDto());
            }
        }
    }

    private void updateActWithResponseDto(SQLiteDatabase db, long internalId, EcmoActDto ecmoActDto) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.ID, ecmoActDto.getId());
        values.put(IcupadDbContract.EcmoAct.TABLE_NAME_PROCEDURE_ID, ecmoActDto.getProcedureId());
        values.put(IcupadDbContract.EcmoAct.TABLE_NAME_USER, ecmoActDto.getCreatedBy());
        values.put(IcupadDbContract.EcmoAct.TABLE_NAME_ACT, ecmoActDto.getAct());
        values.put(IcupadDbContract.EcmoAct.TABLE_NAME_DATE, ecmoActDto.getDate());
        QueryBuilder.aQuery(db).withTable(tableName)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?").withSelectionArgs(internalId)
                .build().update(values);
    }
}
