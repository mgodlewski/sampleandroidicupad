package com.icupad.synchronization.upload.responsedto.ecmo;

import com.google.gson.annotations.SerializedName;
import com.icupad.webconnection.dto.ecmo.EcmoParameterDto;

/**
 * Created by Marcin on 02.04.2017.
 */
public class EcmoParameterSyncResponseDto {
    @SerializedName("success")
    private Boolean success;

    @SerializedName("ecmoParameterDto")
    private EcmoParameterDto ecmoParameterDto;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public EcmoParameterDto getEcmoParameterDto() {
        return ecmoParameterDto;
    }

    public void setEcmoParameterDto(EcmoParameterDto ecmoParameterDto) {
        this.ecmoParameterDto = ecmoParameterDto;
    }
}
