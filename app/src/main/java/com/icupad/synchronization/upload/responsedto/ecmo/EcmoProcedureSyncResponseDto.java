package com.icupad.synchronization.upload.responsedto.ecmo;

import com.google.gson.annotations.SerializedName;
import com.icupad.webconnection.dto.ecmo.EcmoProcedureDto;
import com.icupad.webconnection.dto.patient.OperationDto;

/**
 * Created by Marcin on 02.04.2017.
 */
public class EcmoProcedureSyncResponseDto {
    @SerializedName("success")
    private Boolean success;

    @SerializedName("ecmoProcedureDto")
    private EcmoProcedureDto ecmoProcedureDto;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public EcmoProcedureDto getEcmoProcedureDto() {
        return ecmoProcedureDto;
    }

    public void setEcmoProcedureDto(EcmoProcedureDto ecmoProcedureDto) {
        this.ecmoProcedureDto = ecmoProcedureDto;
    }
}
