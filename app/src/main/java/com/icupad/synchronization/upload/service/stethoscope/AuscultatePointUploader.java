package com.icupad.synchronization.upload.service.stethoscope;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.synchronization.conflict.resolver.stethoscope.AuscultatePointConflictResolver;
import com.icupad.synchronization.upload.responsedto.stethoscope.AuscultatePointSyncResponseDto;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.builder.stethoscope.AuscultatePointDtoBuilder;
import com.icupad.webconnection.dto.stethoscope.AuscultatePointDto;

import java.io.IOException;

public class AuscultatePointUploader {
    private IcupadDbHelper mDbHelper;
    private BackendApi backendApi;
    private SyncTableRepository syncTableRepository;
    private String tableName;

    private AuscultatePointConflictResolver auscultatePointConflictResolver;

    public AuscultatePointUploader(Context context) {
        this.mDbHelper = new IcupadDbHelper(context);
        this.backendApi = new BackendApi(context);
        this.syncTableRepository = new SyncTableRepository(context);

        this.tableName = IcupadDbContract.AuscultatePoint.TABLE_NAME;
        auscultatePointConflictResolver = new AuscultatePointConflictResolver(context);
    }

    public void upload(long internalId) throws IOException {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = getQueryForModelToUploadWithInternalId(db, internalId)
                .select();
        AuscultatePointDto auscultatePointDto = convertFirstFromCursorToModelGson(cursor);

        tryUploadAuscultatePointDto(db, internalId, auscultatePointDto);

        cursor.close();
        mDbHelper.closeDb(db);
    }

    private Query getQueryForModelToUploadWithInternalId(SQLiteDatabase db, long internalId) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.AuscultatePoint.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_NAME,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_QUEUE_POSITION,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_X,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_Y,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_IS_FRONT,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_SUITE_SCHEMA_ID,

                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_ID,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_CREATED_DATE,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_LAST_MODIFIED_DATE,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_CREATED_BY_ID,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_LAST_MODIFIED_BY_ID)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                .withSelectionArgs(internalId)
                .build();
    }

    private AuscultatePointDto convertFirstFromCursorToModelGson(Cursor cursor) {
        if(cursor.moveToFirst()) {
            AuscultatePointDto auscultatePointDto = AuscultatePointDtoBuilder.anAuscultatePointDto()
                    .withName(cursor.getString(0))
                    .withQueuePosition(cursor.getInt(1))
                    .withX(cursor.getDouble(2))
                    .withY(cursor.getDouble(3))
                    .withIsFront(cursor.getInt(4) == 1)
                    .withAuscultateSuiteSchemaId(cursor.getLong(5))

                    .withId(cursor.getLong(6))
                    .withCreatedDateTime(DateTimeFormatterHelper.getMillisFromString(cursor.getString(7)))
                    .withLastModifiedDateTime(DateTimeFormatterHelper.getMillisFromString(cursor.getString(8)))
                    .withCreatedById(cursor.getLong(9))
                    .withLastModifiedById(cursor.getLong(10))
                    .build();
            return auscultatePointDto;
        }
        return null;
    }

    private void tryUploadAuscultatePointDto(SQLiteDatabase db, long internalId, AuscultatePointDto auscultatePointDto) throws IOException {
        if(auscultatePointDto != null) {
            AuscultatePointSyncResponseDto response = backendApi.uploadAuscultatePoint(auscultatePointDto);
            reactOnResponseAfterUpload(db, internalId, response);
        }
    }

    private void reactOnResponseAfterUpload(SQLiteDatabase db, long internalId, AuscultatePointSyncResponseDto response) {
        if(response != null && response.getSuccess() != null) {
            if (response.getSuccess()) {
                syncTableRepository.updateSucceed(tableName, internalId);
                updateAuscultatePointWithResponseDto(db, internalId, response.getAuscultatePointDto());
            }
            else {
                auscultatePointConflictResolver.logConflict(internalId, response.getAuscultatePointDto());
            }
        }
    }

    private void updateAuscultatePointWithResponseDto(SQLiteDatabase db, long internalId, AuscultatePointDto response) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.ID, response.getId());
        values.put(IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_BY_ID, response.getLastModifiedById());
        values.put(IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_DATE, response.getLastModifiedDateTime());
        values.put(IcupadDbContract.COLUMN_NAME_CREATED_BY_ID, response.getCreatedById());
        values.put(IcupadDbContract.COLUMN_NAME_CREATED_DATE, response.getCreatedDateTime());

        QueryBuilder.aQuery(db).withTable(tableName)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?").withSelectionArgs(internalId)
                .build().update(values);
    }
}
