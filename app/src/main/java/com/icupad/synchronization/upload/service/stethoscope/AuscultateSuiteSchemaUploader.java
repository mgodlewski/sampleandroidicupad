package com.icupad.synchronization.upload.service.stethoscope;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.synchronization.conflict.resolver.stethoscope.AuscultateSuiteSchemaConflictResolver;
import com.icupad.synchronization.upload.responsedto.stethoscope.AuscultateSuiteSchemaSyncResponseDto;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.builder.stethoscope.AuscultateSuiteSchemaDtoBuilder;
import com.icupad.webconnection.dto.stethoscope.AuscultateSuiteSchemaDto;

import java.io.IOException;

public class AuscultateSuiteSchemaUploader {
    private IcupadDbHelper mDbHelper;
    private BackendApi backendApi;
    private SyncTableRepository syncTableRepository;
    private String tableName;

    private AuscultateSuiteSchemaConflictResolver auscultateSuiteSchemaConflictResolver;

    public AuscultateSuiteSchemaUploader(Context context) {
        this.mDbHelper = new IcupadDbHelper(context);
        this.backendApi = new BackendApi(context);
        this.syncTableRepository = new SyncTableRepository(context);

        this.tableName = IcupadDbContract.AuscultateSuiteSchema.TABLE_NAME;
        auscultateSuiteSchemaConflictResolver = new AuscultateSuiteSchemaConflictResolver(context);
    }

    public void upload(long internalId) throws IOException {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Cursor suiteSchemaCursor = getQueryForSuiteSchemaToUploadByInternalId(db, internalId)
                .select();

        AuscultateSuiteSchemaDto auscultateSuiteSchemaDto =
                convertCursorsToSuiteSchemaGson(suiteSchemaCursor);
        tryToUploadSuiteSchemaAndPreparePoints(db, internalId, auscultateSuiteSchemaDto);

        suiteSchemaCursor.close();
        mDbHelper.closeDb(db);
    }

    private Query getQueryForSuiteSchemaToUploadByInternalId(SQLiteDatabase db, long internalId) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.AuscultateSuiteSchema.TABLE_NAME)
                .withColumns(IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_NAME,
                        IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_IS_PUBLIC,

                        IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_ID,
                        IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_CREATED_DATE,
                        IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_LAST_MODIFIED_DATE,
                        IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_CREATED_BY_ID,
                        IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_LAST_MODIFIED_BY_ID)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                .withSelectionArgs(internalId)
                .build();
    }

    private AuscultateSuiteSchemaDto convertCursorsToSuiteSchemaGson(Cursor suiteSchemaCursor) {
        if(suiteSchemaCursor.moveToFirst()) {
            AuscultateSuiteSchemaDto auscultateSuiteSchemaDto = AuscultateSuiteSchemaDtoBuilder.anAuscultateSuiteSchemaDto()
                    .withName(suiteSchemaCursor.getString(0))
                    .withIsPublic(suiteSchemaCursor.getInt(1) == 1)

                    .withId(suiteSchemaCursor.getLong(2))
                    .withCreatedDateTime(DateTimeFormatterHelper.getMillisFromString(suiteSchemaCursor.getString(3)))
                    .withLastModifiedDateTime(DateTimeFormatterHelper.getMillisFromString(suiteSchemaCursor.getString(4)))
                    .withCreatedById(suiteSchemaCursor.getLong(5))
                    .withLastModifiedById(suiteSchemaCursor.getLong(6))
                    .build();
            return auscultateSuiteSchemaDto;
        }
        return null;
    }

    private void tryToUploadSuiteSchemaAndPreparePoints(SQLiteDatabase db, long internalId,
                                                        AuscultateSuiteSchemaDto auscultateSuiteSchemaDto) throws IOException {
        if(auscultateSuiteSchemaDto != null) {
            AuscultateSuiteSchemaSyncResponseDto response = backendApi.uploadAuscultateSuiteSchema(auscultateSuiteSchemaDto);

            if(response != null && response.getSuccess() != null) {
                if (response.getSuccess()) {
                    long id = response.getAuscultateSuiteSchemaDto().getId();

                    syncTableRepository.updateSucceed(tableName, internalId);
                    updateAuscultateSuiteSchemaWithResponseDto(db, internalId, response.getAuscultateSuiteSchemaDto());
                    setPointsToUploadAndUpdateThemWithSuiteSchemaId(db, internalId, id);
                }
                else {
                    auscultateSuiteSchemaConflictResolver.logConflict(internalId, response.getAuscultateSuiteSchemaDto());
                }
            }
        }
    }

    private void updateAuscultateSuiteSchemaWithResponseDto(SQLiteDatabase db, long internalId, AuscultateSuiteSchemaDto response) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.ID, response.getId());
        values.put(IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_BY_ID, response.getLastModifiedById());
        values.put(IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_DATE, response.getLastModifiedDateTime());
        values.put(IcupadDbContract.COLUMN_NAME_CREATED_BY_ID, response.getCreatedById());
        values.put(IcupadDbContract.COLUMN_NAME_CREATED_DATE, response.getCreatedDateTime());

        QueryBuilder.aQuery(db).withTable(tableName)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?").withSelectionArgs(internalId)
                .build().update(values);
    }

    private void setPointsToUploadAndUpdateThemWithSuiteSchemaId(
            SQLiteDatabase db, long suiteSchemaInternalId, long suiteSchemaId) {
        Cursor cursor = QueryBuilder.aQuery(db).withTable(IcupadDbContract.AuscultatePoint.TABLE_NAME)
                .withColumns(IcupadDbContract.INTERNAL_ID)
                .withSelection(IcupadDbContract.AuscultatePoint.COLUMN_NAME_SUITE_SCHEMA_INTERNAL_ID + "=?")
                .withSelectionArgs(suiteSchemaInternalId)
                .build().select();

        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_SUITE_SCHEMA_ID, suiteSchemaId);

        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            int internalId = cursor.getInt(0);
            QueryBuilder.aQuery(db).withTable(IcupadDbContract.AuscultatePoint.TABLE_NAME)
                    .withSelection(IcupadDbContract.INTERNAL_ID + "=?").withSelectionArgs(internalId)
                    .build().update(values);
            syncTableRepository.logUpdate(IcupadDbContract.AuscultatePoint.TABLE_NAME, internalId);
        }
        cursor.close();
    }
}
