package com.icupad.synchronization.upload.service.ecmo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.synchronization.upload.responsedto.ecmo.EcmoActSyncResponseDto;
import com.icupad.synchronization.upload.responsedto.ecmo.EcmoParameterSyncResponseDto;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.builder.ecmo.EcmoActDtoBuilder;
import com.icupad.webconnection.builder.ecmo.EcmoParameterDtoBuilder;
import com.icupad.webconnection.dto.ecmo.EcmoActDto;
import com.icupad.webconnection.dto.ecmo.EcmoParameterDto;

import net.sqlcipher.database.SQLiteDatabase;

import java.io.IOException;

/**
 * Created by Marcin on 03.04.2017.
 */
public class EcmoParameterUploader {
    private IcupadDbHelper mDbHelper;
    private BackendApi backendApi;
    private String tableName;
    private SyncTableRepository syncTableRepository;

    public EcmoParameterUploader(Context context) {
        this.mDbHelper = new IcupadDbHelper(context);
        this.backendApi = new BackendApi(context);
        this.tableName = IcupadDbContract.EcmoParameter.TABLE_NAME;
        this.syncTableRepository = new SyncTableRepository(context);
    }

    public void upload(long internalId) throws IOException {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = getQueryForConfigurationModelToUploadWithInternalId(db, internalId)
                .select();

        EcmoParameterDto ecmoParameterDto = convertFirstFromCursorToModel(db, cursor);

        tryUploadConfigurationDto(db, internalId, ecmoParameterDto);

        cursor.close();
        mDbHelper.closeDb(db);
    }

    private Query getQueryForConfigurationModelToUploadWithInternalId(SQLiteDatabase db, long internalId) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoParameter.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.EcmoParameter.TABLE_NAME_ID,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_PROCEDURE_ID,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_USER,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_DATE,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_RPM,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_BLOOD_FLOW,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_FIO2,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_GAS_FLOW,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_P1,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_P2,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_P3,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_DELTA,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_HEPARIN_SUPPLY,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_DESCRIPTION)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                .withSelectionArgs(internalId)
                .build();
    }

    private EcmoParameterDto convertFirstFromCursorToModel(SQLiteDatabase db, Cursor cursor) {
        if(cursor.moveToFirst()) {
            Cursor procedureIdcursor = QueryBuilder.aQuery(db)
                    .withTable(IcupadDbContract.EcmoProcedure.TABLE_NAME)
                    .withColumns(IcupadDbContract.EcmoProcedure.TABLE_NAME_ID)
                    .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                    .withSelectionArgs(cursor.getLong(1))
                    .build().select();
            if(procedureIdcursor.moveToFirst()) {
                EcmoParameterDto ecmoParameterDto = EcmoParameterDtoBuilder.aEcmoParameterDtoBuilder()
                        .withId(null)
                        .withProcedureId(procedureIdcursor.getLong(0))
                        .withCreatedBy(cursor.getLong(2))
                        .withDate(DateTimeFormatterHelper.getMillisFromString(cursor.getString(3)))
                        .withRpm(cursor.getDouble(4))
                        .withBloodFlow(cursor.getDouble(5))
                        .withFiO2(cursor.getDouble(6))
                        .withGasFlow(cursor.getDouble(7))
                        .withP1(cursor.getDouble(8))
                        .withP2(cursor.getDouble(9))
                        .withP3(cursor.getDouble(10))
                        .withDelta(cursor.getDouble(11))
                        .withHeparinSuply(cursor.getDouble(12))
                        .withDescription(cursor.getString(13))
                        .build();
                procedureIdcursor.close();
                return ecmoParameterDto;
            }
            procedureIdcursor.close();
    }
        return null;
    }

    private void tryUploadConfigurationDto(SQLiteDatabase db, long internalId, EcmoParameterDto ecmoParameterDto) throws IOException {
        if(ecmoParameterDto != null) {
            EcmoParameterSyncResponseDto response = backendApi.createEcmoParameter(ecmoParameterDto);
            reactOnResponseAfterUpload(db, internalId, response);
        }
    }

    private void reactOnResponseAfterUpload(SQLiteDatabase db, long internalId, EcmoParameterSyncResponseDto response){
        if(response != null && response.getSuccess() != null) {
            if (response.getSuccess()) {
                syncTableRepository.updateSucceedSyncTable(tableName, internalId);
//                updateActWithResponseDto(db, internalId, response.getEcmoParameterDto());
            }
        }
    }

    private void updateActWithResponseDto(SQLiteDatabase db, long internalId, EcmoParameterDto ecmoParameterDto) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.ID, ecmoParameterDto.getId());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_PROCEDURE_ID, ecmoParameterDto.getProcedureId());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_USER, ecmoParameterDto.getCreatedBy());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_DATE, ecmoParameterDto.getDate());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_RPM, ecmoParameterDto.getRpm());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_BLOOD_FLOW, ecmoParameterDto.getBloodFlow());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_FIO2, ecmoParameterDto.getFiO2());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_GAS_FLOW, ecmoParameterDto.getGasFlow());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_P1, ecmoParameterDto.getP1());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_P2, ecmoParameterDto.getP2());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_P3, ecmoParameterDto.getP3());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_DELTA, ecmoParameterDto.getDelta());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_HEPARIN_SUPPLY, ecmoParameterDto.getHeparinSuply());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_DESCRIPTION, ecmoParameterDto.getDescription());

        QueryBuilder.aQuery(db).withTable(tableName)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?").withSelectionArgs(internalId)
                .build().update(values);
    }
}
