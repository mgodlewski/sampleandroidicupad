package com.icupad.synchronization.upload.responsedto.stethoscope;

import com.google.gson.annotations.SerializedName;
import com.icupad.webconnection.dto.stethoscope.AuscultateSuiteDto;

public class AuscultateSuiteSyncResponseDto {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("auscultateSuiteDto")
    private AuscultateSuiteDto auscultateSuiteDto;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public AuscultateSuiteDto getAuscultateSuiteDto() {
        return auscultateSuiteDto;
    }

    public void setAuscultateSuiteDto(AuscultateSuiteDto auscultateSuiteDto) {
        this.auscultateSuiteDto = auscultateSuiteDto;
    }
}
