package com.icupad.synchronization.upload.responsedto.ecmo;

import com.google.gson.annotations.SerializedName;
import com.icupad.webconnection.dto.ecmo.EcmoConfigurationDto;
import com.icupad.webconnection.dto.ecmo.EcmoProcedureDto;

/**
 * Created by Marcin on 02.04.2017.
 */
public class EcmoConfigurationSyncResponseDto {
    @SerializedName("success")
    private Boolean success;

    @SerializedName("ecmoConfigurationDto")
    private EcmoConfigurationDto ecmoConfigurationDto;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public EcmoConfigurationDto getEcmoConfigurationDto() {
        return ecmoConfigurationDto;
    }

    public void setEcmoConfigurationDto(EcmoConfigurationDto ecmoConfigurationDto) {
        this.ecmoConfigurationDto = ecmoConfigurationDto;
    }
}
