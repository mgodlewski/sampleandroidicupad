package com.icupad.synchronization.upload.service.ecmo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.synchronization.upload.responsedto.ecmo.EcmoProcedureSyncResponseDto;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.builder.ecmo.EcmoProcedureDtoBuilder;
import com.icupad.webconnection.dto.ecmo.EcmoProcedureDto;

import net.sqlcipher.database.SQLiteDatabase;

import java.io.IOException;

/**
 * Created by Marcin on 02.04.2017.
 */
public class EcmoProcedureUploader {
    private IcupadDbHelper mDbHelper;
    private BackendApi backendApi;
    private String tableName;
    private SyncTableRepository syncTableRepository;

    public EcmoProcedureUploader(Context context) {
        this.mDbHelper = new IcupadDbHelper(context);
        this.backendApi = new BackendApi(context);
        this.tableName = IcupadDbContract.EcmoProcedure.TABLE_NAME;
        this.syncTableRepository = new SyncTableRepository(context);
    }

    public void upload(long internalId) throws IOException {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = getQueryForModelToUploadWithInternalId(db, internalId)
                .select();
        EcmoProcedureDto ecmoProcedureDto = convertFirstFromCursorToModel(cursor);

        tryUploadProcedureDto(db, internalId, ecmoProcedureDto);

        cursor.close();
        mDbHelper.closeDb(db);
    }

    private Query getQueryForModelToUploadWithInternalId(SQLiteDatabase db, long internalId) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoProcedure.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.EcmoProcedure.TABLE_NAME_ID,
                        IcupadDbContract.EcmoProcedure.TABLE_NAME_STAY,
                        IcupadDbContract.EcmoProcedure.TABLE_NAME_CREATED_BY,
                        IcupadDbContract.EcmoProcedure.TABLE_NAME_END_BY,
                        IcupadDbContract.EcmoProcedure.TABLE_NAME_START_CAUSE,
                        IcupadDbContract.EcmoProcedure.TABLE_NAME_START_DATE,
                        IcupadDbContract.EcmoProcedure.TABLE_NAME_END_CAUSE,
                        IcupadDbContract.EcmoProcedure.TABLE_NAME_END_DATE)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                .withSelectionArgs(internalId)
                .build();
    }

    private EcmoProcedureDto convertFirstFromCursorToModel(Cursor cursor) {
        if(cursor.moveToFirst()) {
            EcmoProcedureDto ecmoProcedureDto = EcmoProcedureDtoBuilder.aEcmoProcedureDtoBuilder()
                    .withId(cursor.getLong(0))
                    .withStayId(cursor.getLong(1))
                    .withCreatedById(cursor.getLong(2))
                    .withEndById(cursor.getLong(3))
                    .withStartCause(cursor.getString(4))
                    .withStartDate(DateTimeFormatterHelper.getMillisFromString(cursor.getString(5)))
                    .withEndCause(cursor.getString(6))
                    .withEndDate(DateTimeFormatterHelper.getMillisFromString(cursor.getString(7)))
                    .build();
            return ecmoProcedureDto;
        }
        return null;
    }

    private void tryUploadProcedureDto(SQLiteDatabase db, long internalId, EcmoProcedureDto ecmoProcedureDto) throws IOException {
        if(ecmoProcedureDto != null) {
            EcmoProcedureSyncResponseDto response = backendApi.createEcmoProcedure(ecmoProcedureDto);
            reactOnResponseAfterUpload(db, internalId, response);
        }
    }

    private void reactOnResponseAfterUpload(SQLiteDatabase db, long internalId, EcmoProcedureSyncResponseDto response){
        if(response != null && response.getSuccess() != null) {
            if (response.getSuccess()) {
                syncTableRepository.updateSucceedSyncTable(tableName, internalId);
                updateProcedureWithResponseDto(db, internalId, response.getEcmoProcedureDto());
            }
        }
    }

    private void updateProcedureWithResponseDto(SQLiteDatabase db, long internalId, EcmoProcedureDto ecmoProcedureDto) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.ID, ecmoProcedureDto.getId());
//        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_STAY, ecmoProcedureDto.getStayId());
//        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_CREATED_BY, ecmoProcedureDto.getCreatedById());
//        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_END_BY, ecmoProcedureDto.getEndById());
//        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_START_CAUSE, ecmoProcedureDto.getStartCause());
////        values.put( IcupadDbContract.EcmoProcedure.TABLE_NAME_START_DATE, ecmoProcedureDto.getStartDate());
//        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_END_CAUSE, ecmoProcedureDto.getEndCause());
//        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_END_DATE, ecmoProcedureDto.getEndDate());

        QueryBuilder.aQuery(db).withTable(tableName)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?").withSelectionArgs(internalId)
                .build().update(values);



       /* ContentValues valuesConfiguration = new ContentValues();
        valuesConfiguration.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_PROCEDURE_ID,ecmoProcedureDto.getId());

        QueryBuilder.aQuery(db).withTable(IcupadDbContract.EcmoConfiguration.TABLE_NAME)
                .withSelection(IcupadDbContract.EcmoConfiguration.TABLE_NAME_PROCEDURE_ID + "=?")
                .withSelectionArgs(internalId)
                .build().update(valuesConfiguration);



        ContentValues valuesAct = new ContentValues();
        valuesAct.put(IcupadDbContract.EcmoAct.TABLE_NAME_PROCEDURE_ID,ecmoProcedureDto.getId());

        QueryBuilder.aQuery(db).withTable(IcupadDbContract.EcmoAct.TABLE_NAME)
                .withSelection(IcupadDbContract.EcmoAct.TABLE_NAME_PROCEDURE_ID + "=?")
                .withSelectionArgs(internalId)
                .build().update(valuesAct);



        ContentValues valuesPatient = new ContentValues();
        valuesPatient.put(IcupadDbContract.EcmoPatient.TABLE_NAME_PROCEDURE_ID,ecmoProcedureDto.getId());

        QueryBuilder.aQuery(db).withTable(IcupadDbContract.EcmoPatient.TABLE_NAME)
                .withSelection(IcupadDbContract.EcmoPatient.TABLE_NAME_PROCEDURE_ID + "=?")
                .withSelectionArgs(internalId)
                .build().update(valuesPatient);



        ContentValues valuesParameter = new ContentValues();
        valuesParameter.put(IcupadDbContract.EcmoParameter.TABLE_NAME_PROCEDURE_ID,ecmoProcedureDto.getId());

        QueryBuilder.aQuery(db).withTable(IcupadDbContract.EcmoParameter.TABLE_NAME)
                .withSelection(IcupadDbContract.EcmoParameter.TABLE_NAME_PROCEDURE_ID + "=?")
                .withSelectionArgs(internalId)
                .build().update(valuesParameter);*/
    }
}
