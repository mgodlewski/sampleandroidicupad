package com.icupad.synchronization.upload.responsedto.stethoscope;

import com.google.gson.annotations.SerializedName;
import com.icupad.webconnection.dto.stethoscope.AuscultatePointDto;

public class AuscultatePointSyncResponseDto {

    @SerializedName("success")
    private Boolean success;
    @SerializedName("auscultatePointDto")
    private AuscultatePointDto auscultatePointDto;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public AuscultatePointDto getAuscultatePointDto() {
        return auscultatePointDto;
    }

    public void setAuscultatePointDto(AuscultatePointDto auscultatePointDto) {
        this.auscultatePointDto = auscultatePointDto;
    }
}
