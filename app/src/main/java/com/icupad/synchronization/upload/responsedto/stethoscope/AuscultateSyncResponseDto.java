package com.icupad.synchronization.upload.responsedto.stethoscope;

import com.google.gson.annotations.SerializedName;
import com.icupad.webconnection.dto.stethoscope.AuscultateDto;

public class AuscultateSyncResponseDto {

    @SerializedName("success")
    private Boolean success;
    @SerializedName("auscultateDto")
    private AuscultateDto auscultateDto;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public AuscultateDto getAuscultateDto() {
        return auscultateDto;
    }

    public void setAuscultateDto(AuscultateDto auscultateDto) {
        this.auscultateDto = auscultateDto;
    }
}
