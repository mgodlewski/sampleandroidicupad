package com.icupad.synchronization.upload.service.ecmo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.synchronization.upload.responsedto.ecmo.EcmoPatientSyncResponseDto;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.builder.ecmo.EcmoPatientDtoBuilder;
import com.icupad.webconnection.dto.ecmo.EcmoPatientDto;

import net.sqlcipher.database.SQLiteDatabase;

import java.io.IOException;

/**
 * Created by Marcin on 03.04.2017.
 */
public class EcmoPatientUploader {
    private IcupadDbHelper mDbHelper;
    private BackendApi backendApi;
    private String tableName;
    private SyncTableRepository syncTableRepository;

    public EcmoPatientUploader(Context context) {
        this.mDbHelper = new IcupadDbHelper(context);
        this.backendApi = new BackendApi(context);
        this.tableName = IcupadDbContract.EcmoPatient.TABLE_NAME;
        this.syncTableRepository = new SyncTableRepository(context);
    }

    public void upload(long internalId) throws IOException {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor configurationCursor = getQueryForConfigurationModelToUploadWithInternalId(db, internalId)
                .select();

        EcmoPatientDto ecmoPatientDto = convertFirstFromCursorToModel(db, configurationCursor);

        tryUploadConfigurationDto(db, internalId, ecmoPatientDto);

        configurationCursor.close();
        mDbHelper.closeDb(db);
    }

    private Query getQueryForConfigurationModelToUploadWithInternalId(SQLiteDatabase db, long internalId) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoPatient.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.EcmoPatient.TABLE_NAME_ID,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_PROCEDURE_ID,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_USER,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_DATE,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_SAT,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_ABP,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_VCP,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_HR,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_TEMP_SURFACE,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_TEMP_CORE,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_DIURESIS,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_ULTRAFILTRACTION)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                .withSelectionArgs(internalId)
                .build();
    }

    private EcmoPatientDto convertFirstFromCursorToModel(SQLiteDatabase db, Cursor cursor) {
        if(cursor.moveToFirst()) {
            Cursor procedureIdcursor = QueryBuilder.aQuery(db)
                    .withTable(IcupadDbContract.EcmoProcedure.TABLE_NAME)
                    .withColumns(IcupadDbContract.EcmoProcedure.TABLE_NAME_ID)
                    .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                    .withSelectionArgs(cursor.getLong(1))
                    .build().select();
            if(procedureIdcursor.moveToFirst()) {
                EcmoPatientDto ecmoPatientDto = EcmoPatientDtoBuilder.aEcmoPatientDtoBuilder()
                        .withId(null)
                        .withProcedureId(procedureIdcursor.getLong(0))
                        .withCreatedBy(cursor.getLong(2))
                        .withDate(DateTimeFormatterHelper.getMillisFromString(cursor.getString(3)))
                        .withSat(cursor.getDouble(4))
                        .withAbp(cursor.getDouble(5))
                        .withVcp(cursor.getDouble(6))
                        .withHr(cursor.getDouble(7))
                        .withTempSurface(cursor.getDouble(8))
                        .withTempCore(cursor.getDouble(9))
                        .withDiuresis(cursor.getDouble(10))
                        .withUltrafiltraction(cursor.getDouble(11))
                        .build();
                procedureIdcursor.close();
                return ecmoPatientDto;
            }
            procedureIdcursor.close();
        }
        return null;
    }

    private void tryUploadConfigurationDto(SQLiteDatabase db, long internalId, EcmoPatientDto ecmoPatientDto) throws IOException {
        if(ecmoPatientDto != null) {
            EcmoPatientSyncResponseDto response = backendApi.createEcmoPatient(ecmoPatientDto);
            reactOnResponseAfterUpload(db, internalId, response);
        }
    }

    private void reactOnResponseAfterUpload(SQLiteDatabase db, long internalId, EcmoPatientSyncResponseDto response){
        if(response != null && response.getSuccess() != null) {
            if (response.getSuccess()) {
                syncTableRepository.updateSucceedSyncTable(tableName, internalId);
//                updateActWithResponseDto(db, internalId, response.getEcmoPatientDto());
            }
        }
    }

    private void updateActWithResponseDto(SQLiteDatabase db, long internalId, EcmoPatientDto ecmoPatientDto) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.ID, ecmoPatientDto.getId());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_PROCEDURE_ID, ecmoPatientDto.getProcedureId());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_USER, ecmoPatientDto.getCreatedBy());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_DATE, ecmoPatientDto.getDate());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_SAT, ecmoPatientDto.getSat());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_ABP, ecmoPatientDto.getAbp());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_VCP, ecmoPatientDto.getVcp());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_HR, ecmoPatientDto.getHr());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_TEMP_SURFACE, ecmoPatientDto.getTempSurface());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_TEMP_CORE, ecmoPatientDto.getTempCore());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_DIURESIS, ecmoPatientDto.getDiuresis());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_ULTRAFILTRACTION, ecmoPatientDto.getUltrafiltraction());

        QueryBuilder.aQuery(db).withTable(tableName)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?").withSelectionArgs(internalId)
                .build().update(values);
    }
}
