package com.icupad.synchronization.upload.service.stethoscope;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.synchronization.conflict.resolver.stethoscope.AuscultateSuiteConflictResolver;
import com.icupad.synchronization.upload.responsedto.stethoscope.AuscultateSuiteSyncResponseDto;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.webconnection.dto.stethoscope.AuscultateSuiteDto;
import com.icupad.webconnection.builder.stethoscope.AuscultateSuiteDtoBuilder;
import com.icupad.webconnection.api.BackendApi;

import java.io.IOException;

public class AuscultateSuiteUploader {
    private IcupadDbHelper mDbHelper;
    private BackendApi backendApi;
    private SyncTableRepository syncTableRepository;
    private String tableName;

    private AuscultateSuiteConflictResolver auscultateSuiteConflictResolver;

    public AuscultateSuiteUploader(Context context) {
        this.mDbHelper = new IcupadDbHelper(context);
        this.backendApi = new BackendApi(context);
        this.syncTableRepository = new SyncTableRepository(context);
        this.tableName = IcupadDbContract.AuscultateSuite.TABLE_NAME;

        auscultateSuiteConflictResolver = new AuscultateSuiteConflictResolver(context);
    }

    public void upload(long internalId) throws IOException {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Cursor auscultateSuiteCursor = getQueryForSuiteToUploadByInternalId(db, internalId)
                .select();

        AuscultateSuiteDto auscultateSuiteDto =
                convertFirstFromCursorToAuscultateSuiteGson(auscultateSuiteCursor);
        tryToUploadSuiteAndPrepareAuscultates(db, internalId, auscultateSuiteDto);

        auscultateSuiteCursor.close();
        mDbHelper.closeDb(db);
    }

    private Query getQueryForSuiteToUploadByInternalId(SQLiteDatabase db, long internalId) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.AuscultateSuite.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_ID,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_PATIENT_ID,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_DESCRIPTION,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_SUITE_SCHEMA_ID,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_POSITION,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_TEMPERATURE,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_IS_RESPIRATED,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_PASSIVE_OXYGEN_THERAPY,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXAMINATION_DATETIME,

                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXECUTOR_HL7ID,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXECUTOR_NAME,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXECUTOR_SURNAME,

                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_CREATED_DATE,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_LAST_MODIFIED_DATE,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_CREATED_BY_ID,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_LAST_MODIFIED_BY_ID)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                .withSelectionArgs(internalId)
                .build();
    }

    private AuscultateSuiteDto convertFirstFromCursorToAuscultateSuiteGson(Cursor suiteCursor) {
        if(suiteCursor.moveToFirst()) {
            AuscultateSuiteDto auscultateSuiteDto = AuscultateSuiteDtoBuilder.anAuscultateSuiteDto()
                    .withId(suiteCursor.getLong(0))
                    .withPatientId(suiteCursor.getLong(1))
                    .withDescription(suiteCursor.getString(2))
                    .withAuscultateSuiteSchemaId(suiteCursor.getLong(3))
                    .withPosition(suiteCursor.getLong(4))
                    .withTemperature(suiteCursor.getDouble(5))
                    .withIsRespirated((suiteCursor.getInt(6) == 1))
                    .withPassiveOxygenTherapy((suiteCursor.getInt(7) == 1))
                    .withExaminationDateTime(DateTimeFormatterHelper.getMillisFromString(suiteCursor.getString(8)))

                    .withExecutorHl7Id(suiteCursor.getString(9))
                    .withExecutorName(suiteCursor.getString(10))
                    .withExecutorSurname(suiteCursor.getString(11))

                    .withCreatedDateTime(DateTimeFormatterHelper.getMillisFromString(suiteCursor.getString(12)))
                    .withLastModifiedDateTime(DateTimeFormatterHelper.getMillisFromString(suiteCursor.getString(13)))
                    .withCreatedById(suiteCursor.getLong(14))
                    .withLastModifiedById(suiteCursor.getLong(15))
                    .build();
            return auscultateSuiteDto;
        }
        return null;
    }

    private void tryToUploadSuiteAndPrepareAuscultates(
            SQLiteDatabase db, long internalId, AuscultateSuiteDto auscultateSuiteDto) throws IOException {
        if(auscultateSuiteDto != null) {
            AuscultateSuiteSyncResponseDto response = backendApi.uploadAuscultateSuite(auscultateSuiteDto);

            if(response != null && response.getSuccess() != null) {
                if (response.getSuccess()) {
                    long id = response.getAuscultateSuiteDto().getId();

                    syncTableRepository.updateSucceed(tableName, internalId);
                    updateAuscultateSuiteWithResponseDto(db, internalId, response.getAuscultateSuiteDto());
                    setAuscultatesToUploadAndUpdateThemWithSuiteId(db, internalId, id);
                }
                else {
                    auscultateSuiteConflictResolver.logConflict(internalId, response.getAuscultateSuiteDto());
                }
            }
        }
    }

    private void updateAuscultateSuiteWithResponseDto(SQLiteDatabase db, long internalId, AuscultateSuiteDto response) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.ID, response.getId());
        values.put(IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_BY_ID, response.getLastModifiedById());
        values.put(IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_DATE, response.getLastModifiedDateTime());
        values.put(IcupadDbContract.COLUMN_NAME_CREATED_BY_ID, response.getCreatedById());
        values.put(IcupadDbContract.COLUMN_NAME_CREATED_DATE, response.getCreatedDateTime());

        QueryBuilder.aQuery(db).withTable(tableName)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?").withSelectionArgs(internalId)
                .build().update(values);
    }

    private void setAuscultatesToUploadAndUpdateThemWithSuiteId(
            SQLiteDatabase db, long suiteInternalId, long suiteId) {
        Cursor cursor = QueryBuilder.aQuery(db).withTable(IcupadDbContract.Auscultate.TABLE_NAME)
                .withColumns(IcupadDbContract.Auscultate._ID)
                .withSelection(IcupadDbContract.Auscultate.COLUMN_NAME_SUITE_INTERNAL_ID + "=?")
                .withSelectionArgs(suiteInternalId)
                .build().select();

        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_SUITE_ID, suiteId);

        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            int internalId = cursor.getInt(0);
            QueryBuilder.aQuery(db).withTable(IcupadDbContract.Auscultate.TABLE_NAME)
                    .withSelection(IcupadDbContract.INTERNAL_ID + "=?").withSelectionArgs(internalId)
                    .build().update(values);
            syncTableRepository.logUpdate(IcupadDbContract.Auscultate.TABLE_NAME, internalId);
        }
        cursor.close();
    }
}
