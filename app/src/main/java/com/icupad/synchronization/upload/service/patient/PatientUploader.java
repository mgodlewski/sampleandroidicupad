package com.icupad.synchronization.upload.service.patient;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.synchronization.conflict.resolver.patient.PatientConflictResolver;
import com.icupad.synchronization.upload.responsedto.patient.PatientSyncResponseDto;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.webconnection.builder.patient.AddressDtoBuilder;
import com.icupad.webconnection.builder.patient.PatientDtoBuilder;
import com.icupad.webconnection.dto.patient.AddressDto;
import com.icupad.webconnection.dto.patient.PatientDto;
import com.icupad.webconnection.api.BackendApi;

import java.io.IOException;

public class PatientUploader {
    private IcupadDbHelper mDbHelper;
    private BackendApi backendApi;
    private SyncTableRepository syncTableRepository;
    private String tableName;

    private PatientConflictResolver patientConflictResolver;

    public PatientUploader(Context context) {
        this.mDbHelper = new IcupadDbHelper(context);
        this.backendApi = new BackendApi(context);
        this.syncTableRepository = new SyncTableRepository(context);

        this.tableName = IcupadDbContract.Patient.TABLE_NAME;
        patientConflictResolver = new PatientConflictResolver(context);
    }

    public void upload(long internalId) throws IOException {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = getQueryForModelToUploadWithInternalId(db, internalId)
                .select();
        PatientDto patientDto = convertFirstFromCursorToModelGson(cursor);

        tryUploadPatientDto(db, internalId, patientDto);

        cursor.close();
        mDbHelper.closeDb(db);
    }

    private Query getQueryForModelToUploadWithInternalId(SQLiteDatabase db, long internalId) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.Patient.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.Patient.COLUMN_NAME_ID,
                        IcupadDbContract.Patient.COLUMN_NAME_NAME,
                        IcupadDbContract.Patient.COLUMN_NAME_SURNAME,
                        IcupadDbContract.Patient.COLUMN_NAME_SEX,
                        IcupadDbContract.Patient.COLUMN_NAME_BIRTH_DATE,
                        IcupadDbContract.Patient.COLUMN_NAME_PESEL,
                        IcupadDbContract.Patient.COLUMN_NAME_HL7ID,

                        IcupadDbContract.Patient.COLUMN_NAME_STREET,
                        IcupadDbContract.Patient.COLUMN_NAME_STREET_NUMBER,
                        IcupadDbContract.Patient.COLUMN_NAME_HOUSE_NUMBER,
                        IcupadDbContract.Patient.COLUMN_NAME_POSTAL_CODE,
                        IcupadDbContract.Patient.COLUMN_NAME_CITY,

                        IcupadDbContract.Patient.COLUMN_NAME_HEIGHT,
                        IcupadDbContract.Patient.COLUMN_NAME_WEIGHT,
                        IcupadDbContract.Patient.COLUMN_NAME_BLOOD_TYPE,
                        IcupadDbContract.Patient.COLUMN_NAME_ALLERGIES,
                        IcupadDbContract.Patient.COLUMN_NAME_OTHER_IMPORTANT_INFORMATIONS,

                        IcupadDbContract.Patient.COLUMN_NAME_CREATED_DATE,
                        IcupadDbContract.Patient.COLUMN_NAME_LAST_MODIFIED_DATE, IcupadDbContract.Patient.COLUMN_NAME_CREATED_BY_ID,
                        IcupadDbContract.Patient.COLUMN_NAME_LAST_MODIFIED_BY_ID)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                .withSelectionArgs(internalId)
                .build();
    }

    private PatientDto convertFirstFromCursorToModelGson(Cursor cursor) {
        if(cursor.moveToFirst()) {
            AddressDto addresGson = AddressDtoBuilder.anAddressGson()
                    .withStreet(cursor.getString(7))
                    .withStreetNumber(cursor.getString(8))
                    .withHouseNumber(cursor.getString(9))
                    .withPostalCode(cursor.getString(10))
                    .withCity(cursor.getString(11))
                    .build();

            PatientDto patienGson = PatientDtoBuilder.aPatientDto()
                    .withId(cursor.getLong(0))
                    .withName(cursor.getString(1))
                    .withSurname(cursor.getString(2))
                    .withSex(cursor.getString(3))
                    .withBirthDate(DateTimeFormatterHelper.getMillisFromString(cursor.getString(4)))
                    .withPesel(cursor.getString(5))
                    .withHl7Id(cursor.getString(6))
                    .withAddress(addresGson)

                    .withHeight(cursor.getString(12))
                    .withWeight(cursor.getString(13))
                    .withBloodType(cursor.getString(14))
                    .withAllergies(cursor.getString(15))
                    .withOtherImportantInformations(cursor.getString(16))

                    .withCreatedDateTime(DateTimeFormatterHelper.getMillisFromString(cursor.getString(17)))
                    .withLastModifiedDateTime(DateTimeFormatterHelper.getMillisFromString(cursor.getString(18)))
                    .withCreatedById(cursor.getLong(19))
                    .withLastModifiedById(cursor.getLong(20))
                    .build();
            return patienGson;
        }
        return null;
    }

    private void tryUploadPatientDto(SQLiteDatabase db, long internalId, PatientDto patientDto) throws IOException {
        if(patientDto != null) {
            PatientSyncResponseDto response = backendApi.uploadPatient(patientDto);
            reactOnResponseAfterUpload(db, internalId, response);
        }
    }

    private void reactOnResponseAfterUpload(SQLiteDatabase db, long internalId, PatientSyncResponseDto response) {
        if(response != null && response.getSuccess() != null) {
            if (response.getSuccess()) {
                syncTableRepository.updateSucceed(tableName, internalId);
                updatePatientWithResponseDto(db, internalId, response.getPatientDto());
            }
            else {
                patientConflictResolver.logConflict(internalId, response.getPatientDto());
            }
        }
    }

    private void updatePatientWithResponseDto(SQLiteDatabase db, long internalId, PatientDto response) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.ID, response.getId());
        values.put(IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_BY_ID, response.getLastModifiedById());
        values.put(IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_DATE, response.getLastModifiedDateTime());
        values.put(IcupadDbContract.COLUMN_NAME_CREATED_BY_ID, response.getCreatedById());
        values.put(IcupadDbContract.COLUMN_NAME_CREATED_DATE, response.getCreatedDateTime());

        QueryBuilder.aQuery(db).withTable(tableName)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?").withSelectionArgs(internalId)
                .build().update(values);
    }
}
