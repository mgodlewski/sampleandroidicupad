package com.icupad.synchronization.upload.responsedto.ecmo;

import com.google.gson.annotations.SerializedName;
import com.icupad.webconnection.dto.ecmo.EcmoPatientDto;

/**
 * Created by Marcin on 02.04.2017.
 */
public class EcmoPatientSyncResponseDto {
    @SerializedName("success")
    private Boolean success;

    @SerializedName("ecmoPatientDto")
    private EcmoPatientDto ecmoPatientDto;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public EcmoPatientDto getEcmoPatientDto() {
        return ecmoPatientDto;
    }

    public void setEcmoPatientDto(EcmoPatientDto ecmoPatientDto) {
        this.ecmoPatientDto = ecmoPatientDto;
    }
}
