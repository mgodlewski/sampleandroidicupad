package com.icupad.synchronization.upload.responsedto.patient;

import com.google.gson.annotations.SerializedName;
import com.icupad.webconnection.dto.patient.PatientDto;

public class PatientSyncResponseDto {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("patientDto")
    private PatientDto patientDto;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public PatientDto getPatientDto() {
        return patientDto;
    }

    public void setPatientDto(PatientDto patientDto) {
        this.patientDto = patientDto;
    }
}
