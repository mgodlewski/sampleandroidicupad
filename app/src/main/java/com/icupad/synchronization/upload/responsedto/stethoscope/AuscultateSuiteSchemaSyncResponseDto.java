package com.icupad.synchronization.upload.responsedto.stethoscope;

import com.google.gson.annotations.SerializedName;
import com.icupad.webconnection.dto.stethoscope.AuscultateSuiteSchemaDto;

public class AuscultateSuiteSchemaSyncResponseDto {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("auscultateSuiteSchemaDto")
    private AuscultateSuiteSchemaDto auscultateSuiteSchemaDto;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public AuscultateSuiteSchemaDto getAuscultateSuiteSchemaDto() {
        return auscultateSuiteSchemaDto;
    }

    public void setAuscultateSuiteSchemaDto(AuscultateSuiteSchemaDto auscultateSuiteSchemaDto) {
        this.auscultateSuiteSchemaDto = auscultateSuiteSchemaDto;
    }
}
