package com.icupad.synchronization.upload.service.patient;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.synchronization.conflict.resolver.patient.OperationConflictResolver;
import com.icupad.synchronization.upload.responsedto.patient.OperationSyncResponseDto;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.webconnection.api.BackendApi;
import com.icupad.webconnection.builder.patient.OperationDtoBuilder;
import com.icupad.webconnection.dto.patient.OperationDto;

import java.io.IOException;

public class OperationUploader {
    private IcupadDbHelper mDbHelper;
    private BackendApi backendApi;
    private SyncTableRepository syncTableRepository;
    private String tableName;

    private OperationConflictResolver operationConflictResolver;

    public OperationUploader(Context context) {
        this.mDbHelper = new IcupadDbHelper(context);
        this.backendApi = new BackendApi(context);
        this.syncTableRepository = new SyncTableRepository(context);

        this.tableName = IcupadDbContract.Operation.TABLE_NAME;
        operationConflictResolver = new OperationConflictResolver(context);
    }

    public void upload(long internalId) throws IOException {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        Cursor cursor = getQueryForModelToUploadWithInternalId(db, internalId)
                .select();
        OperationDto operationDto = convertFirstFromCursorToModelGson(cursor);

        tryUploadOperationDto(db, internalId, operationDto);

        cursor.close();
        mDbHelper.closeDb(db);
    }

    private Query getQueryForModelToUploadWithInternalId(SQLiteDatabase db, long internalId) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.Operation.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.Operation.COLUMN_NAME_ID,
                        IcupadDbContract.Operation.COLUMN_NAME_STAY_ID,
                        IcupadDbContract.Operation.COLUMN_NAME_OPERATING_DATE,
                        IcupadDbContract.Operation.COLUMN_NAME_DESCRIPTION,
                        IcupadDbContract.Operation.COLUMN_NAME_ARCHIVED,

                        IcupadDbContract.Operation.COLUMN_NAME_CREATED_DATE,
                        IcupadDbContract.Operation.COLUMN_NAME_LAST_MODIFIED_DATE,
                        IcupadDbContract.Operation.COLUMN_NAME_CREATED_BY_ID,
                        IcupadDbContract.Operation.COLUMN_NAME_LAST_MODIFIED_BY_ID)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                .withSelectionArgs(internalId)
                .build();
    }

    private OperationDto convertFirstFromCursorToModelGson(Cursor cursor) {
        if(cursor.moveToFirst()) {
            OperationDto operationDto = OperationDtoBuilder.anOperationDto()
                    .withId(cursor.getLong(0))
                    .withStayId(cursor.getLong(1))
                    .withOperatingDate(DateTimeFormatterHelper.getMillisFromString(cursor.getString(2)))
                    .withDescription(cursor.getString(3))
                    .withArchived(cursor.getInt(4) == 1)

                    .withCreatedDateTime(DateTimeFormatterHelper.getMillisFromString(cursor.getString(5)))
                    .withLastModifiedDateTime(DateTimeFormatterHelper.getMillisFromString(cursor.getString(6)))
                    .withCreatedById(cursor.getLong(7))
                    .withLastModifiedById(cursor.getLong(8))
                    .build();
            return operationDto;
        }
        else {
            return null;
        }
    }

    private void tryUploadOperationDto(SQLiteDatabase db, long internalId, OperationDto operationDto) throws IOException {
        if(operationDto != null) {
            OperationSyncResponseDto response = backendApi.uploadOperation(operationDto);
            reactOnResponseAfterUpload(db, internalId, response);
        }
    }

    private void reactOnResponseAfterUpload(SQLiteDatabase db, long internalId, OperationSyncResponseDto response) {
        if(response != null && response.getSuccess() != null) {
            if(response.getSuccess()) {
                syncTableRepository.updateSucceed(tableName, internalId);
                updateOperationWithResponseDto(db, internalId, response.getOperationDto());
            }
            else {
                operationConflictResolver.logConflict(internalId, response.getOperationDto());
            }
        }
    }

    private void updateOperationWithResponseDto(SQLiteDatabase db, long internalId, OperationDto response) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.ID, response.getId());
        values.put(IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_BY_ID, response.getLastModifiedById());
        values.put(IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_DATE, response.getLastModifiedDateTime());
        values.put(IcupadDbContract.COLUMN_NAME_CREATED_BY_ID, response.getCreatedById());
        values.put(IcupadDbContract.COLUMN_NAME_CREATED_DATE, response.getCreatedDateTime());

        QueryBuilder.aQuery(db).withTable(tableName)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?").withSelectionArgs(internalId)
                .build().update(values);
    }
}
