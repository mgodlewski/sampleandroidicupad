package com.icupad.synchronization;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.icupad.R;
import com.icupad.synchronization.conflict.ConflictSynchronizer;
import com.icupad.synchronization.download.DownloadSynchronizer;
import com.icupad.utils.exception.InterruptedDownloadException;
import com.icupad.synchronization.upload.UploadSynchronizer;
import com.icupad.synchronization.utils.ThresholdTime;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.webconnection.service.ConnectionStatusUpdater;
import com.icupad.webconnection.model.ConnectionStatus;

public class SynchronizerService extends IntentService {

    private static String SYNC = "SYNC";
    public static final String MESSAGE = "message";
    public static final String DB_SYNCHRONIZATION = "db_synchronization";
    public static final String NOTIFICATION = "com.icupad.synchronization" ;
    public static final String TYPE = "type";
    public static final String UPDATE_DB = "update_db";
    public static final String FIRST_TIME_UPDATE_DB = "first_time_update_db";
    public static final String TYPE_CONNECTION_STATUS = "connection_status";
    public int MILLISECONDS_BETWEEN_DB_SYNC;
    public int MILLISECONDS_BETWEEN_CONNECTION_CHECK_WHEN_NO_CONNECTION;

    private Context context;
    private ConnectionStatusUpdater connectionStatusUpdater;
    private ConnectionStatus connectionStatus;

    private DownloadSynchronizer downloadSynchronizer;
    private UploadSynchronizer uploadSynchronizer;
    private ConflictSynchronizer conflictSynchronizer;

    private boolean running;
    private boolean dbSynchronization;

    private ThresholdTime thresholdTime;

    public SynchronizerService() {
        super(IntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        dbSynchronization = intent.getBooleanExtra(DB_SYNCHRONIZATION, false);
        context = getApplicationContext();
        running = true;

        MILLISECONDS_BETWEEN_DB_SYNC = 1000 * context.getResources().getInteger(R.integer.seconds_between_db_synchronization);
        MILLISECONDS_BETWEEN_CONNECTION_CHECK_WHEN_NO_CONNECTION = 1000 * context.getResources().getInteger(R.integer.seconds_between_connection_check_when_no_connection);

        this.connectionStatusUpdater = new ConnectionStatusUpdater(context);
        this.downloadSynchronizer = new DownloadSynchronizer(context);
        this.uploadSynchronizer = new UploadSynchronizer(context);
        this.conflictSynchronizer = new ConflictSynchronizer(context);

        thresholdTime = PrivateDataAccessor.getLastSyncWithBackend();
        while (running) {
            if (updateAndCheckConnectionStatus() && dbSynchronization) {
                synchronized (SYNC) {
                    try {
                        synchronizeDataAndSleep();
                    } catch (InterruptedDownloadException e) {
                        running = false;
                    }
                }
            }
            else {
                sleep(MILLISECONDS_BETWEEN_CONNECTION_CHECK_WHEN_NO_CONNECTION);
            }
        }
    }

    private void synchronizeDataAndSleep() throws InterruptedDownloadException {

        downloadSynchronizer.synchronizeAndUpdateLastTimestamps(thresholdTime);
        uploadSynchronizer.synchronize();
        conflictSynchronizer.synchronize();

        updateThresholdAndEventuallyPublishUpdateDb();

        sleep(MILLISECONDS_BETWEEN_DB_SYNC);
    }

    private boolean updateAndCheckConnectionStatus() {
        connectionStatus = connectionStatusUpdater.updateAndReturnConnectionStatus();
        publishConnectionStatus();
        return connectionStatus == ConnectionStatus.AUTHENTICATED_CONNECTION_TO_SERVER;
    }

    private void updateThresholdAndEventuallyPublishUpdateDb() {
        if(thresholdTime.isUpdated()) {
            Intent intent = new Intent(NOTIFICATION);
            if(thresholdTime.isUpdatedAfterLongTime()) {
                intent.putExtra(TYPE, FIRST_TIME_UPDATE_DB);
            }
            else {
                intent.putExtra(TYPE, UPDATE_DB);
            }
            intent.putExtra(MESSAGE, connectionStatus.toString());
            context.sendBroadcast(intent);
        }
        thresholdTime.updateAndSave();
    }

    private void publishConnectionStatus() {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(TYPE, TYPE_CONNECTION_STATUS);
        intent.putExtra(MESSAGE, connectionStatus.toString());
        context.sendBroadcast(intent);
    }

    private void sleep(int i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        running = false;
    }
}
