package com.icupad.db.management;

import android.net.Uri;
import android.provider.BaseColumns;

public class IcupadDbContract {

    public static final String COLUMN_NAME_LAST_MODIFIED_DATE = "last_modified_date";
    public static final String COLUMN_NAME_LAST_MODIFIED_BY_ID = "last_modified_by_id";
    public static final String COLUMN_NAME_CREATED_DATE = "created_date";
    public static final String COLUMN_NAME_CREATED_BY_ID = "created_by_id";
    public static final String COLUMN_NAME_SYNC = "sync";
    public static final String INTERNAL_ID = BaseColumns._ID;
    public static final String ID = "id";

    public IcupadDbContract() {}

    public static final String AUTHORITY = "com.icupad.android.infos";

    public static final Uri AUTHORITY_URI = Uri.parse("content://" + AUTHORITY);

    public static abstract class Patient implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "patient");
        public static final String TABLE_NAME = "patient";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_SURNAME = "surname";
        public static final String COLUMN_NAME_SEX = "sex";
        public static final String COLUMN_NAME_BIRTH_DATE = "birth_date";
        public static final String COLUMN_NAME_PESEL = "pesel";
        public static final String COLUMN_NAME_HL7ID = "hl7id";
        public static final String COLUMN_NAME_STREET = "street";
        public static final String COLUMN_NAME_STREET_NUMBER = "street_number";
        public static final String COLUMN_NAME_HOUSE_NUMBER = "house_number";
        public static final String COLUMN_NAME_POSTAL_CODE = "postal_code";
        public static final String COLUMN_NAME_CITY = "city";

        public static final String COLUMN_NAME_HEIGHT = "height";
        public static final String COLUMN_NAME_WEIGHT = "weight";
        public static final String COLUMN_NAME_BLOOD_TYPE = "blood_type";
        public static final String COLUMN_NAME_ALLERGIES = "allergies";
        public static final String COLUMN_NAME_OTHER_IMPORTANT_INFORMATIONS = "other_important_informations";

        public static final String COLUMN_NAME_ID = IcupadDbContract.ID;
        public static final String COLUMN_NAME_CREATED_DATE = IcupadDbContract.COLUMN_NAME_CREATED_DATE;
        public static final String COLUMN_NAME_LAST_MODIFIED_DATE = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_DATE;
        public static final String COLUMN_NAME_CREATED_BY_ID = IcupadDbContract.COLUMN_NAME_CREATED_BY_ID;
        public static final String COLUMN_NAME_LAST_MODIFIED_BY_ID = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_BY_ID;
        public static final String COLUMN_NAME_SYNC = IcupadDbContract.COLUMN_NAME_SYNC;
    }

    public static abstract class Stay implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "stay");
        public static final String TABLE_NAME = "stay";
        public static final String COLUMN_NAME_ACTIVE = "active";
        public static final String COLUMN_NAME_ADMIT_DATE = "admit_date";
        public static final String COLUMN_NAME_ADMITTING_DOCTOR_HL7ID = "admitting_doctor_hl7id";
        public static final String COLUMN_NAME_ADMITTING_DOCTOR_NAME = "admitting_doctor_name";
        public static final String COLUMN_NAME_NPWZ = "npwz";
        public static final String COLUMN_NAME_SURNAME = "surname";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_DISCHARGE_DATE = "discharge_date";
        public static final String COLUMN_NAME_HL7ID = "hl7id";
        public static final String COLUMN_NAME_TYPE = "type";
        public static final String COLUMN_NAME_PATIENT_ID = "patient_id";

        public static final String COLUMN_NAME_ID = IcupadDbContract.ID;
        public static final String COLUMN_NAME_CREATED_DATE = IcupadDbContract.COLUMN_NAME_CREATED_DATE;
        public static final String COLUMN_NAME_LAST_MODIFIED_DATE = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_DATE;
        public static final String COLUMN_NAME_CREATED_BY_ID = IcupadDbContract.COLUMN_NAME_CREATED_BY_ID;
        public static final String COLUMN_NAME_LAST_MODIFIED_BY_ID = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_BY_ID;
        public static final String COLUMN_NAME_SYNC = IcupadDbContract.COLUMN_NAME_SYNC;
        public static String fullName(String column) {
            return TABLE_NAME + "." + column;
        }
    }

    public static abstract class BloodGasTestMeasurement implements BaseColumns{
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "blood_gas_test_measurement");
        public static final String TABLE_NAME = "blood_gas_test_measurements";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_UNIT = "unit";
        public static final String COLUMN_NAME_BLOOD_SOURCE = "blood_source";
        public static final String COLUMN_NAME_VALUE = "value";
        public static final String COLUMN_NAME_ABNORMALITY = "abnormality";
        public static final String COLUMN_NAME_RESULT_DATE = "result_date";
        public static final String COLUMN_NAME_BOTTOM_DEFAULT_NORM = "bottom_deafult_norm";
        public static final String COLUMN_NAME_TOP_DEFAULT_NORM = "top_default_norm";
        public static final String COLUMN_NAME_STAY_ID = "stay_id";
        public static final String COLUMN_NAME_TEST_ID = "test_id";
        public static final String COLUMN_NAME_BIRTH_DATE = "birth_date";

        public static final String COLUMN_NAME_ID = IcupadDbContract.ID;
        public static final String COLUMN_NAME_CREATED_DATE = IcupadDbContract.COLUMN_NAME_CREATED_DATE;
        public static final String COLUMN_NAME_LAST_MODIFIED_DATE = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_DATE;
        public static final String COLUMN_NAME_CREATED_BY_ID = IcupadDbContract.COLUMN_NAME_CREATED_BY_ID;
        public static final String COLUMN_NAME_LAST_MODIFIED_BY_ID = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_BY_ID;
        public static final String COLUMN_NAME_SYNC = IcupadDbContract.COLUMN_NAME_SYNC;
    }

    public static abstract class CompleteBloodCountTestMeasurement implements BaseColumns{
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "complete_blood_count_test_measurement");
        public static final String TABLE_NAME = "complete_blood_count_test_measurements";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_UNIT = "unit";
        public static final String COLUMN_NAME_BLOOD_SOURCE = "blood_source";
        public static final String COLUMN_NAME_VALUE = "value";
        public static final String COLUMN_NAME_ABNORMALITY = "abnormality";
        public static final String COLUMN_NAME_RESULT_DATE = "result_date";
        public static final String COLUMN_NAME_BOTTOM_DEFAULT_NORM = "bottom_deafult_norm";
        public static final String COLUMN_NAME_TOP_DEFAULT_NORM = "top_default_norm";
        public static final String COLUMN_NAME_STAY_ID = "stay_id";
        public static final String COLUMN_NAME_TEST_ID = "test_id";
        public static final String COLUMN_NAME_BIRTH_DATE = "birth_date";

        public static final String COLUMN_NAME_ID = IcupadDbContract.ID;
        public static final String COLUMN_NAME_CREATED_DATE = IcupadDbContract.COLUMN_NAME_CREATED_DATE;
        public static final String COLUMN_NAME_LAST_MODIFIED_DATE = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_DATE;
        public static final String COLUMN_NAME_CREATED_BY_ID = IcupadDbContract.COLUMN_NAME_CREATED_BY_ID;
        public static final String COLUMN_NAME_LAST_MODIFIED_BY_ID = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_BY_ID;
        public static final String COLUMN_NAME_SYNC = IcupadDbContract.COLUMN_NAME_SYNC;
    }

    public static abstract class DefaultTestMeasurement implements BaseColumns{
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "deafult_test_measurement");
        public static final String TABLE_NAME = "deafult_test_measurements";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_UNIT = "unit";
        public static final String COLUMN_NAME_VALUE = "value";
        public static final String COLUMN_NAME_ABNORMALITY = "abnormality";
        public static final String COLUMN_NAME_RESULT_DATE = "result_date";
        public static final String COLUMN_NAME_BOTTOM_DEFAULT_NORM = "bottom_deafult_norm";
        public static final String COLUMN_NAME_TOP_DEFAULT_NORM = "top_default_norm";
        public static final String COLUMN_NAME_STAY_ID = "stay_id";
        public static final String COLUMN_NAME_TEST_ID = "test_id";
        public static final String COLUMN_NAME_BIRTH_DATE = "birth_date";

        public static final String COLUMN_NAME_ID = IcupadDbContract.ID;
        public static final String COLUMN_NAME_CREATED_DATE = IcupadDbContract.COLUMN_NAME_CREATED_DATE;
        public static final String COLUMN_NAME_LAST_MODIFIED_DATE = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_DATE;
        public static final String COLUMN_NAME_CREATED_BY_ID = IcupadDbContract.COLUMN_NAME_CREATED_BY_ID;
        public static final String COLUMN_NAME_LAST_MODIFIED_BY_ID = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_BY_ID;
        public static final String COLUMN_NAME_SYNC = IcupadDbContract.COLUMN_NAME_SYNC;
    }

    public static abstract class Operation implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "operation");
        public static final String TABLE_NAME = "operation";
        public static final String COLUMN_NAME_STAY_ID = "stay_id";
        public static final String COLUMN_NAME_OPERATING_DATE = "operating_date";
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        public static final String COLUMN_NAME_ARCHIVED = "archived";

        public static final String COLUMN_NAME_ID = IcupadDbContract.ID;
        public static final String COLUMN_NAME_CREATED_DATE = IcupadDbContract.COLUMN_NAME_CREATED_DATE;
        public static final String COLUMN_NAME_LAST_MODIFIED_DATE = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_DATE;
        public static final String COLUMN_NAME_CREATED_BY_ID = IcupadDbContract.COLUMN_NAME_CREATED_BY_ID;
        public static final String COLUMN_NAME_LAST_MODIFIED_BY_ID = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_BY_ID;
        public static final String COLUMN_NAME_SYNC = IcupadDbContract.COLUMN_NAME_SYNC;
    }

    public static abstract class SyncTable implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "synctable");
        public static final String TABLE_NAME = "synctable";
        public static final String COLUMN_NAME_MODIFY_DATE = "modify_date";
        public static final String COLUMN_NAME_TABLE_NAME = "table_name";
        public static final String COLUMN_NAME_INFO_INTERNAL_ID = "info_internal_id";
    }

    public static abstract class ConflictTable implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "conflicttable");
        public static final String TABLE_NAME = "conflicttable";
        public static final String COLUMN_NAME_TABLE_NAME = "table_name";
        public static final String COLUMN_NAME_INFO_INTERNAL_ID = "info_id";
        public static final String COLUMN_NAME_CONFLICT_INFO_INTERNAL_ID = "conflict_info_id";
    }

    public static abstract class AuscultateSuiteSchema implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "auscultate_suite_schema");
        public static final String TABLE_NAME = "auscultate_suite_schema";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_IS_PUBLIC = "is_public";

        public static final String COLUMN_NAME_ID = IcupadDbContract.ID;
        public static final String COLUMN_NAME_CREATED_DATE = IcupadDbContract.COLUMN_NAME_CREATED_DATE;
        public static final String COLUMN_NAME_LAST_MODIFIED_DATE = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_DATE;
        public static final String COLUMN_NAME_CREATED_BY_ID = IcupadDbContract.COLUMN_NAME_CREATED_BY_ID;
        public static final String COLUMN_NAME_LAST_MODIFIED_BY_ID = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_BY_ID;
        public static final String COLUMN_NAME_SYNC = IcupadDbContract.COLUMN_NAME_SYNC;
    }

    public static abstract class AuscultatePoint implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "auscultate_point");
        public static final String TABLE_NAME = "auscultate_point";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_QUEUE_POSITION = "queue_position";
        public static final String COLUMN_NAME_X = "x";
        public static final String COLUMN_NAME_Y = "y";
        public static final String COLUMN_NAME_IS_FRONT = "is_front";
        public static final String COLUMN_NAME_SUITE_SCHEMA_ID = "suite_schema_id";
        public static final String COLUMN_NAME_SUITE_SCHEMA_INTERNAL_ID = "suite_schema_internal_id";

        public static final String COLUMN_NAME_ID = IcupadDbContract.ID;
        public static final String COLUMN_NAME_CREATED_DATE = IcupadDbContract.COLUMN_NAME_CREATED_DATE;
        public static final String COLUMN_NAME_LAST_MODIFIED_DATE = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_DATE;
        public static final String COLUMN_NAME_CREATED_BY_ID = IcupadDbContract.COLUMN_NAME_CREATED_BY_ID;
        public static final String COLUMN_NAME_LAST_MODIFIED_BY_ID = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_BY_ID;
        public static final String COLUMN_NAME_SYNC = IcupadDbContract.COLUMN_NAME_SYNC;
    }

    public static abstract class AuscultateSuite implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "auscultate_suite");
        public static final String TABLE_NAME = "auscultate_suite";
        public static final String COLUMN_NAME_PATIENT_ID = "patient_id";
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        public static final String COLUMN_NAME_SUITE_SCHEMA_ID = "suite_schema_id";
        public static final String COLUMN_NAME_POSITION = "position";
        public static final String COLUMN_NAME_TEMPERATURE = "temperature";
        public static final String COLUMN_NAME_IS_RESPIRATED = "is_respirated";
        public static final String COLUMN_NAME_PASSIVE_OXYGEN_THERAPY = "passive_oxygen_therapy";
        public static final String COLUMN_NAME_EXAMINATION_DATETIME = "examination_datetime";

        public static final String COLUMN_NAME_EXECUTOR_HL7ID = "executor_hl7id";
        public static final String COLUMN_NAME_EXECUTOR_NAME = "executor_name";
        public static final String COLUMN_NAME_EXECUTOR_SURNAME = "executor_surname";

        public static final String COLUMN_NAME_ID = IcupadDbContract.ID;
        public static final String COLUMN_NAME_CREATED_DATE = IcupadDbContract.COLUMN_NAME_CREATED_DATE;
        public static final String COLUMN_NAME_LAST_MODIFIED_DATE = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_DATE;
        public static final String COLUMN_NAME_CREATED_BY_ID = IcupadDbContract.COLUMN_NAME_CREATED_BY_ID;
        public static final String COLUMN_NAME_LAST_MODIFIED_BY_ID = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_BY_ID;
        public static final String COLUMN_NAME_SYNC = IcupadDbContract.COLUMN_NAME_SYNC;
    }

    public static abstract class Auscultate implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "auscultate");
        public static final String TABLE_NAME = "auscultate";
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        public static final String COLUMN_NAME_WAV_NAME = "wav_name";
        public static final String COLUMN_NAME_POINT_ID = "point_id";
        public static final String COLUMN_NAME_SUITE_ID = "suite_id";
        public static final String COLUMN_NAME_SUITE_INTERNAL_ID = "suite_internal_id";
        public static final String COLUMN_NAME_POLL_RESULT = "poll_result";

        public static final String COLUMN_NAME_ID = IcupadDbContract.ID;
        public static final String COLUMN_NAME_CREATED_DATE = IcupadDbContract.COLUMN_NAME_CREATED_DATE;
        public static final String COLUMN_NAME_LAST_MODIFIED_DATE = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_DATE;
        public static final String COLUMN_NAME_CREATED_BY_ID = IcupadDbContract.COLUMN_NAME_CREATED_BY_ID;
        public static final String COLUMN_NAME_LAST_MODIFIED_BY_ID = IcupadDbContract.COLUMN_NAME_LAST_MODIFIED_BY_ID;
        public static final String COLUMN_NAME_SYNC = IcupadDbContract.COLUMN_NAME_SYNC;
    }

    //**********ECMO**********///
    public static abstract class EcmoAct implements BaseColumns{
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "ecmo_act");
        public static final String TABLE_NAME = "ecmo_act";
        public static final String TABLE_NAME_ID = "id";
        public static final String TABLE_NAME_PROCEDURE_ID = "procedure_id";
        public static final String TABLE_NAME_USER = "created_by";
        public static final String TABLE_NAME_DATE = "date";
        public static final String TABLE_NAME_ACT = "act";
    }

    public static abstract class EcmoConfiguration implements BaseColumns{
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "ecmo_configuration");
        public static final String TABLE_NAME = "ecmo_configuration";
        public static final String TABLE_NAME_ID = "id";
        public static final String TABLE_NAME_PROCEDURE_ID = "procedure_id";
        public static final String TABLE_NAME_USER = "created_by";
        public static final String TABLE_NAME_MODE = "mode";
        public static final String TABLE_NAME_CANIULATION_TYPE = "caniulation_type";
        public static final String TABLE_NAME_WENTOWANIE_LEFT_VERTICLE = "wentowanie_left_verticle";
        public static final String TABLE_NAME_ARTERY_CANNULA_TYPE = "artery_cannula_type";
        public static final String TABLE_NAME_ARTERY_CANNULA_SIZE = "artery_cannula_size";
        public static final String TABLE_NAME_OXYGENERATOR_TYPE = "oxygenerator_type";
        public static final String TABLE_NAME_ACTUAL = "actual";
        public static final String TABLE_NAME_DATE = "date";
    }

    public static abstract class EcmoParameter implements BaseColumns{
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "ecmo_parameter");
        public static final String TABLE_NAME = "ecmo_parameter";
        public static final String TABLE_NAME_ID = "id";
        public static final String TABLE_NAME_PROCEDURE_ID = "procedure_id";
        public static final String TABLE_NAME_USER = "created_by";
        public static final String TABLE_NAME_DATE = "date";
        public static final String TABLE_NAME_RPM = "rpm";
        public static final String TABLE_NAME_BLOOD_FLOW = "blood_flow";
        public static final String TABLE_NAME_FIO2 = "fio2";
        public static final String TABLE_NAME_GAS_FLOW = "gas_flow";
        public static final String TABLE_NAME_P1 = "p1";
        public static final String TABLE_NAME_P2 = "p2";
        public static final String TABLE_NAME_P3 = "p3";
        public static final String TABLE_NAME_DELTA = "delta";
        public static final String TABLE_NAME_HEPARIN_SUPPLY = "heparin_supply";
        public static final String TABLE_NAME_DESCRIPTION = "description";
    }

    public static abstract class EcmoPatient implements BaseColumns{
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "ecmo_patient");
        public static final String TABLE_NAME = "ecmo_patient";
        public static final String TABLE_NAME_ID = "id";
        public static final String TABLE_NAME_PROCEDURE_ID = "procedure_id";
        public static final String TABLE_NAME_USER = "created_by";
        public static final String TABLE_NAME_DATE = "date";
        public static final String TABLE_NAME_SAT = "sat";
        public static final String TABLE_NAME_ABP = "abp";
        public static final String TABLE_NAME_VCP = "vcp";
        public static final String TABLE_NAME_HR = "hr";
        public static final String TABLE_NAME_TEMP_SURFACE = "temp_surface";
        public static final String TABLE_NAME_TEMP_CORE = "temp_core";
        public static final String TABLE_NAME_DIURESIS ="diuresis";
        public static final String TABLE_NAME_ULTRAFILTRACTION = "ultrafiltraction";
    }

    public static abstract class EcmoProcedure implements BaseColumns{
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "ecmo_procedure");
        public static final String TABLE_NAME = "ecmo_procedure";
        public static final String TABLE_NAME_ID = "id";
        public static final String TABLE_NAME_STAY = "stay";
        public static final String TABLE_NAME_CREATED_BY = "created_by";
        public static final String TABLE_NAME_END_BY = "end_by";
        public static final String TABLE_NAME_START_CAUSE = "start_cause";
        public static final String TABLE_NAME_START_DATE= "start_date";
        public static final String TABLE_NAME_END_CAUSE = "end_cause";
        public static final String TABLE_NAME_END_DATE = "end_date";
    }

    public static abstract class EcmoUsedStructures implements BaseColumns{
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "ecmo_used_structures");
        public static final String TABLE_NAME = "ecmo_used_structures";
        public static final String TABLE_NAME_CONFIGURATION_ID = "configuration_id";
        public static final String TABLE_NAME_DATE = "date";
        public static final String TABLE_NAME_NAME = "name";
    }

    public static abstract class EcmoVeinCannula implements BaseColumns{
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "ecmo_vein_cannula");
        public static final String TABLE_NAME = "ecmo_vein_cannula";
        public static final String TABLE_NAME_CONFIGURATION_ID = "configuration_id";
        public static final String TABLE_NAME_TYPE = "type";
        public static final String TABLE_NAME_SIZE = "size";
        public static final String TABLE_NAME_DATE = "date";
    }

}
