package com.icupad.db.management;

import android.content.Context;

import com.icupad.utils.PrivateDataAccessor;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class IcupadDbHelper extends SQLiteOpenHelper {

    private static final String SQL_DB_LOCK = "SQL_DB_LOCK";
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String DATETIME_TYPE = " DATETIME";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP = ",";
    private static Lock lock = new ReentrantLock();

    private static final String SQL_CREATE_PATIENT_TABLE =
            "CREATE TABLE " + IcupadDbContract.Patient.TABLE_NAME + " (" +
                    IcupadDbContract.Patient.COLUMN_NAME_CITY + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_HOUSE_NUMBER + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_POSTAL_CODE + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_STREET + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_STREET_NUMBER + TEXT_TYPE + COMMA_SEP +

                    IcupadDbContract.Patient.COLUMN_NAME_BIRTH_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_HL7ID + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_NAME  + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_PESEL + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_SEX + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_SURNAME + TEXT_TYPE + COMMA_SEP +

                    IcupadDbContract.Patient.COLUMN_NAME_HEIGHT + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_WEIGHT + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_BLOOD_TYPE + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_ALLERGIES + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_OTHER_IMPORTANT_INFORMATIONS + TEXT_TYPE + COMMA_SEP +

                    IcupadDbContract.Patient._ID + INTEGER_TYPE + "  PRIMARY KEY" + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_CREATED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_LAST_MODIFIED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_CREATED_BY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_LAST_MODIFIED_BY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.Patient.COLUMN_NAME_SYNC + INTEGER_TYPE +
                    " )";

    private static final String SQL_CREATE_STAY_TABLE =
            "CREATE TABLE " + IcupadDbContract.Stay.TABLE_NAME + " (" +
                    IcupadDbContract.Stay.COLUMN_NAME_ACTIVE + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.Stay.COLUMN_NAME_ADMIT_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.Stay.COLUMN_NAME_ADMITTING_DOCTOR_HL7ID + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Stay.COLUMN_NAME_ADMITTING_DOCTOR_NAME + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Stay.COLUMN_NAME_NPWZ + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Stay.COLUMN_NAME_SURNAME + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Stay.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Stay.COLUMN_NAME_DISCHARGE_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.Stay.COLUMN_NAME_HL7ID + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Stay.COLUMN_NAME_TYPE + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.Stay.COLUMN_NAME_PATIENT_ID + INTEGER_TYPE + COMMA_SEP +

                    IcupadDbContract.Stay._ID + INTEGER_TYPE + "  PRIMARY KEY" + COMMA_SEP +
                    IcupadDbContract.Stay.COLUMN_NAME_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.Stay.COLUMN_NAME_CREATED_BY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.Stay.COLUMN_NAME_LAST_MODIFIED_BY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.Stay.COLUMN_NAME_CREATED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.Stay.COLUMN_NAME_LAST_MODIFIED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.Stay.COLUMN_NAME_SYNC + INTEGER_TYPE +
                    " )";

    private static final String SQL_CREATE_BLOOD_GAS_TEST_MEASUREMENT_TABLE =
            "CREATE TABLE " + IcupadDbContract.BloodGasTestMeasurement.TABLE_NAME + " (" +
                    IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_UNIT + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_BLOOD_SOURCE + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_VALUE + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_ABNORMALITY + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_RESULT_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_BOTTOM_DEFAULT_NORM + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_TOP_DEFAULT_NORM + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_STAY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_TEST_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_BIRTH_DATE + DATETIME_TYPE + COMMA_SEP +

                    IcupadDbContract.BloodGasTestMeasurement._ID + INTEGER_TYPE + "  PRIMARY KEY" + COMMA_SEP +
                    IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_CREATED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_LAST_MODIFIED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_CREATED_BY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_LAST_MODIFIED_BY_ID + INTEGER_TYPE +COMMA_SEP +
                    IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_SYNC + INTEGER_TYPE +
                    " )";

    private static final String SQL_CREATE_OPERATION_TABLE =
            "CREATE TABLE " + IcupadDbContract.Operation.TABLE_NAME + " (" +
                    IcupadDbContract.Operation.COLUMN_NAME_STAY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.Operation.COLUMN_NAME_OPERATING_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.Operation.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Operation.COLUMN_NAME_ARCHIVED + INTEGER_TYPE + COMMA_SEP +

                    IcupadDbContract.Operation._ID + INTEGER_TYPE + "  PRIMARY KEY" + COMMA_SEP +
                    IcupadDbContract.Operation.COLUMN_NAME_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.Operation.COLUMN_NAME_CREATED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.Operation.COLUMN_NAME_LAST_MODIFIED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.Operation.COLUMN_NAME_CREATED_BY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.Operation.COLUMN_NAME_LAST_MODIFIED_BY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.Operation.COLUMN_NAME_SYNC + INTEGER_TYPE +
                    " )";

    private static final String SQL_CREATE_AUSCULTATE_SUITE_SCHEMA_TABLE =
            "CREATE TABLE " + IcupadDbContract.AuscultateSuiteSchema.TABLE_NAME + " (" +
                    IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_IS_PUBLIC + INTEGER_TYPE + COMMA_SEP +

                    IcupadDbContract.AuscultateSuiteSchema._ID + INTEGER_TYPE + "  PRIMARY KEY" + COMMA_SEP +
                    IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_CREATED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_LAST_MODIFIED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_CREATED_BY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_LAST_MODIFIED_BY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_SYNC + INTEGER_TYPE +
                    " )";
    private static final String SQL_CREATE_AUSCULTATE_POINT_TABLE =
            "CREATE TABLE " + IcupadDbContract.AuscultatePoint.TABLE_NAME + " (" +
                    IcupadDbContract.AuscultatePoint.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultatePoint.COLUMN_NAME_QUEUE_POSITION + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultatePoint.COLUMN_NAME_X + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultatePoint.COLUMN_NAME_Y + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultatePoint.COLUMN_NAME_IS_FRONT + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultatePoint.COLUMN_NAME_SUITE_SCHEMA_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultatePoint.COLUMN_NAME_SUITE_SCHEMA_INTERNAL_ID + INTEGER_TYPE + COMMA_SEP +

                    IcupadDbContract.AuscultatePoint._ID + INTEGER_TYPE + "  PRIMARY KEY" + COMMA_SEP +
                    IcupadDbContract.AuscultatePoint.COLUMN_NAME_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultatePoint.COLUMN_NAME_CREATED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultatePoint.COLUMN_NAME_LAST_MODIFIED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultatePoint.COLUMN_NAME_CREATED_BY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultatePoint.COLUMN_NAME_LAST_MODIFIED_BY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultatePoint.COLUMN_NAME_SYNC + INTEGER_TYPE +
                    " )";

    private static final String SQL_CREATE_AUSCULTATE_SUITE_TABLE =
            "CREATE TABLE " + IcupadDbContract.AuscultateSuite.TABLE_NAME + " (" +
                    IcupadDbContract.AuscultateSuite.COLUMN_NAME_PATIENT_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuite.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuite.COLUMN_NAME_SUITE_SCHEMA_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuite.COLUMN_NAME_POSITION + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuite.COLUMN_NAME_TEMPERATURE + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuite.COLUMN_NAME_IS_RESPIRATED + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuite.COLUMN_NAME_PASSIVE_OXYGEN_THERAPY + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXAMINATION_DATETIME + DATETIME_TYPE + COMMA_SEP +

                    IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXECUTOR_HL7ID + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXECUTOR_NAME + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXECUTOR_SURNAME + TEXT_TYPE + COMMA_SEP +

                    IcupadDbContract.AuscultateSuite._ID + INTEGER_TYPE + "  PRIMARY KEY" + COMMA_SEP +
                    IcupadDbContract.AuscultateSuite.COLUMN_NAME_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuite.COLUMN_NAME_CREATED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuite.COLUMN_NAME_LAST_MODIFIED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuite.COLUMN_NAME_CREATED_BY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuite.COLUMN_NAME_LAST_MODIFIED_BY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.AuscultateSuite.COLUMN_NAME_SYNC + INTEGER_TYPE +
                    " )";

    private static final String SQL_CREATE_AUSCULTATE_TABLE =
            "CREATE TABLE " + IcupadDbContract.Auscultate.TABLE_NAME + " (" +
                    IcupadDbContract.Auscultate.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Auscultate.COLUMN_NAME_WAV_NAME + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.Auscultate.COLUMN_NAME_POINT_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.Auscultate.COLUMN_NAME_SUITE_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.Auscultate.COLUMN_NAME_SUITE_INTERNAL_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.Auscultate.COLUMN_NAME_POLL_RESULT + INTEGER_TYPE + COMMA_SEP +

                    IcupadDbContract.Auscultate._ID + INTEGER_TYPE + "  PRIMARY KEY" + COMMA_SEP +
                    IcupadDbContract.Auscultate.COLUMN_NAME_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.Auscultate.COLUMN_NAME_CREATED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.Auscultate.COLUMN_NAME_LAST_MODIFIED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.Auscultate.COLUMN_NAME_CREATED_BY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.Auscultate.COLUMN_NAME_LAST_MODIFIED_BY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.Auscultate.COLUMN_NAME_SYNC + INTEGER_TYPE +
                    " )";

    private static final String SQL_CREATE_SYNC_TABLE =
            "CREATE TABLE " + IcupadDbContract.SyncTable.TABLE_NAME + " (" +
                    IcupadDbContract.SyncTable._ID + INTEGER_TYPE + "  PRIMARY KEY" + COMMA_SEP +
                    IcupadDbContract.SyncTable.COLUMN_NAME_MODIFY_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.SyncTable.COLUMN_NAME_TABLE_NAME + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.SyncTable.COLUMN_NAME_INFO_INTERNAL_ID + INTEGER_TYPE +
                    " )";

    private static final String SQL_CREATE_CONFLICT_TABLE =
            "CREATE TABLE " + IcupadDbContract.ConflictTable.TABLE_NAME + " (" +
                    IcupadDbContract.ConflictTable._ID + INTEGER_TYPE + "  PRIMARY KEY" + COMMA_SEP +
                    IcupadDbContract.ConflictTable.COLUMN_NAME_TABLE_NAME + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.ConflictTable.COLUMN_NAME_INFO_INTERNAL_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.ConflictTable.COLUMN_NAME_CONFLICT_INFO_INTERNAL_ID + INTEGER_TYPE +
                    " )";

    //**********ECMO**********//
    private static final String SQL_CREATE_ECMO_ACT_TABLE =
            "CREATE TABLE " + IcupadDbContract.EcmoAct.TABLE_NAME + " (" +
                    IcupadDbContract.Patient._ID + INTEGER_TYPE + "  PRIMARY KEY" + COMMA_SEP +
                    IcupadDbContract.EcmoAct.TABLE_NAME_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoAct.TABLE_NAME_PROCEDURE_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoAct.TABLE_NAME_USER + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoAct.TABLE_NAME_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoAct.TABLE_NAME_ACT + REAL_TYPE +
                    " )";

    private static final String SQL_CREATE_ECMO_CONFIGURATION_TABLE =
            "CREATE TABLE " + IcupadDbContract.EcmoConfiguration.TABLE_NAME + " (" +
                    IcupadDbContract.Patient._ID + INTEGER_TYPE + "  PRIMARY KEY" + COMMA_SEP +
                    IcupadDbContract.EcmoConfiguration.TABLE_NAME_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoConfiguration.TABLE_NAME_PROCEDURE_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoConfiguration.TABLE_NAME_USER + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoConfiguration.TABLE_NAME_MODE + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoConfiguration.TABLE_NAME_CANIULATION_TYPE + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoConfiguration.TABLE_NAME_WENTOWANIE_LEFT_VERTICLE + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoConfiguration.TABLE_NAME_ARTERY_CANNULA_TYPE + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoConfiguration.TABLE_NAME_ARTERY_CANNULA_SIZE + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoConfiguration.TABLE_NAME_OXYGENERATOR_TYPE + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoConfiguration.TABLE_NAME_ACTUAL + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoConfiguration.TABLE_NAME_DATE + DATETIME_TYPE +
                    " )";

    private static final String SQL_CREATE_ECMO_PARAMETER_TABLE =
            "CREATE TABLE " + IcupadDbContract.EcmoParameter.TABLE_NAME + " (" +
                    IcupadDbContract.Patient._ID + INTEGER_TYPE + "  PRIMARY KEY" + COMMA_SEP +
                    IcupadDbContract.EcmoParameter.TABLE_NAME_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoParameter.TABLE_NAME_PROCEDURE_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoParameter.TABLE_NAME_USER + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoParameter.TABLE_NAME_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoParameter.TABLE_NAME_RPM + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoParameter.TABLE_NAME_BLOOD_FLOW + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoParameter.TABLE_NAME_FIO2 + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoParameter.TABLE_NAME_GAS_FLOW + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoParameter.TABLE_NAME_P1 + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoParameter.TABLE_NAME_P2 + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoParameter.TABLE_NAME_P3 + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoParameter.TABLE_NAME_DELTA + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoParameter.TABLE_NAME_HEPARIN_SUPPLY + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoParameter.TABLE_NAME_DESCRIPTION + TEXT_TYPE +
                    " )";

    private static final String SQL_CREATE_ECMO_PATIENT_TABLE =
            "CREATE TABLE " + IcupadDbContract.EcmoPatient.TABLE_NAME + " (" +
                    IcupadDbContract.Patient._ID + INTEGER_TYPE + "  PRIMARY KEY" + COMMA_SEP +
                    IcupadDbContract.EcmoPatient.TABLE_NAME_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoPatient.TABLE_NAME_PROCEDURE_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoPatient.TABLE_NAME_USER + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoPatient.TABLE_NAME_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoPatient.TABLE_NAME_SAT + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoPatient.TABLE_NAME_ABP + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoPatient.TABLE_NAME_VCP + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoPatient.TABLE_NAME_HR + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoPatient.TABLE_NAME_TEMP_SURFACE + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoPatient.TABLE_NAME_TEMP_CORE + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoPatient.TABLE_NAME_DIURESIS + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoPatient.TABLE_NAME_ULTRAFILTRACTION + REAL_TYPE +
                    " )";

    private static final String SQL_CREATE_ECMO_PROCEDURE_TABLE =
            "CREATE TABLE " + IcupadDbContract.EcmoProcedure.TABLE_NAME + " (" +
                    IcupadDbContract.Patient._ID + INTEGER_TYPE + "  PRIMARY KEY" + COMMA_SEP +
                    IcupadDbContract.EcmoProcedure.TABLE_NAME_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoProcedure.TABLE_NAME_STAY + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoProcedure.TABLE_NAME_CREATED_BY + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoProcedure.TABLE_NAME_END_BY + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoProcedure.TABLE_NAME_START_CAUSE + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoProcedure.TABLE_NAME_START_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoProcedure.TABLE_NAME_END_CAUSE + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoProcedure.TABLE_NAME_END_DATE + DATETIME_TYPE +
                    " )";

    private static final String SQL_CREATE_ECMO_USED_STRUCTURES_TABLE =
            "CREATE TABLE " + IcupadDbContract.EcmoUsedStructures.TABLE_NAME + " (" +
                    "id INTEGER" + COMMA_SEP +
                    IcupadDbContract.Patient._ID + INTEGER_TYPE + "  PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                    IcupadDbContract.EcmoUsedStructures.TABLE_NAME_CONFIGURATION_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoUsedStructures.TABLE_NAME_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoUsedStructures.TABLE_NAME_NAME + TEXT_TYPE +
                    " )";

    private static final String SQL_CREATE_ECMO_VEIN_CANNULA_TABLE =
            "CREATE TABLE " + IcupadDbContract.EcmoVeinCannula.TABLE_NAME + " (" +
                    "id INTEGER" + COMMA_SEP +
                    IcupadDbContract.Patient._ID + INTEGER_TYPE + "  PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                    IcupadDbContract.EcmoVeinCannula.TABLE_NAME_CONFIGURATION_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoVeinCannula.TABLE_NAME_TYPE + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoVeinCannula.TABLE_NAME_SIZE + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.EcmoVeinCannula.TABLE_NAME_DATE + DATETIME_TYPE +
                    " )";

    //********** Complete Blood Count **********//
    private static final String SQL_CREATE_COMPLETE_BLOOD_COUNT_TEST_MEASUREMENT_TABLE =
            "CREATE TABLE " + IcupadDbContract.CompleteBloodCountTestMeasurement.TABLE_NAME + " (" +
                    IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_UNIT + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_BLOOD_SOURCE + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_VALUE + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_ABNORMALITY + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_RESULT_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_BOTTOM_DEFAULT_NORM + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_TOP_DEFAULT_NORM + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_STAY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_TEST_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_BIRTH_DATE + DATETIME_TYPE + COMMA_SEP +

                    IcupadDbContract.CompleteBloodCountTestMeasurement._ID + INTEGER_TYPE + "  PRIMARY KEY" + COMMA_SEP +
                    IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_CREATED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_LAST_MODIFIED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_CREATED_BY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_LAST_MODIFIED_BY_ID + INTEGER_TYPE +COMMA_SEP +
                    IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_SYNC + INTEGER_TYPE +
                    " )";
    private static final String SQL_CREATE_DEFAULT_TEST_MEASUREMENT_TABLE =
            "CREATE TABLE " + IcupadDbContract.DefaultTestMeasurement.TABLE_NAME + " (" +
                    IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_UNIT + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_VALUE + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_ABNORMALITY + TEXT_TYPE + COMMA_SEP +
                    IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_RESULT_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_BOTTOM_DEFAULT_NORM + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_TOP_DEFAULT_NORM + REAL_TYPE + COMMA_SEP +
                    IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_STAY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_TEST_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_BIRTH_DATE + DATETIME_TYPE + COMMA_SEP +

                    IcupadDbContract.DefaultTestMeasurement._ID + INTEGER_TYPE + "  PRIMARY KEY" + COMMA_SEP +
                    IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_CREATED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_LAST_MODIFIED_DATE + DATETIME_TYPE + COMMA_SEP +
                    IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_CREATED_BY_ID + INTEGER_TYPE + COMMA_SEP +
                    IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_LAST_MODIFIED_BY_ID + INTEGER_TYPE +COMMA_SEP +
                    IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_SYNC + INTEGER_TYPE +
                    " )";


    //**********DELETE**********//
    private static final String SQL_DELETE_PATIENT=
            "DROP TABLE IF EXISTS " + IcupadDbContract.Patient.TABLE_NAME;
    private static final String SQL_DELETE_STAY=
            "DROP TABLE IF EXISTS " + IcupadDbContract.Stay.TABLE_NAME;
    private static final String SQL_DELETE_OPERATION=
            "DROP TABLE IF EXISTS " + IcupadDbContract.Operation.TABLE_NAME;

    private static final String SQL_DELETE_BLOOD_GAS_TEST_MEASUREMENT_TABLE =
            "DROP TABLE IF EXIST " + IcupadDbContract.BloodGasTestMeasurement.TABLE_NAME;

    private static final String SQL_DELETE_AUSCULTATE_TABLE =
            "DROP TABLE IF EXISTS " + IcupadDbContract.Auscultate.TABLE_NAME;
    private static final String SQL_DELETE_AUSCULTATE_SUITE_TABLE =
            "DROP TABLE IF EXISTS " + IcupadDbContract.AuscultateSuite.TABLE_NAME;
    private static final String SQL_DELETE_AUSCULTATE_POINT_TABLE =
            "DROP TABLE IF EXISTS " + IcupadDbContract.AuscultatePoint.TABLE_NAME;
    private static final String SQL_DELETE_AUSCULTATE_SUITE_SCHEMA_TABLE =
            "DROP TABLE IF EXISTS " + IcupadDbContract.AuscultateSuiteSchema.TABLE_NAME;

    private static final String SQL_DELETE_SYNC_TABLE =
            "DROP TABLE IF EXISTS " + IcupadDbContract.SyncTable.TABLE_NAME;
    private static final String SQL_DELETE_CONFLICT_TABLE =
            "DROP TABLE IF EXISTS " + IcupadDbContract.ConflictTable.TABLE_NAME;

    private static final String SQL_DELETE_ECMO_ACT_TABLE =
            "DROP TABLE IF EXISTS " + IcupadDbContract.EcmoAct.TABLE_NAME;
    private static final String SQL_DELETE_ECMO_CONFIGURATION_TABLE =
            "DROP TABLE IF EXISTS " + IcupadDbContract.EcmoConfiguration.TABLE_NAME;
    private static final String SQL_DELETE_ECMO_PARAMETER_TABLE =
            "DROP TABLE IF EXISTS " + IcupadDbContract.EcmoParameter.TABLE_NAME;
    private static final String SQL_DELETE_ECMO_PATIENT_TABLE =
            "DROP TABLE IF EXISTS " + IcupadDbContract.EcmoPatient.TABLE_NAME;
    private static final String SQL_DELETE_ECMO_PROCEDURE_TABLE =
            "DROP TABLE IF EXISTS " + IcupadDbContract.EcmoProcedure.TABLE_NAME;
    private static final String SQL_DELETE_ECMO_USED_STRUCTURES_TABLE =
            "DROP TABLE IF EXISTS " + IcupadDbContract.EcmoUsedStructures.TABLE_NAME;
    private static final String SQL_DELETE_ECMO_VEIN_CANNULA_TABLE =
            "DROP TABLE IF EXISTS " + IcupadDbContract.EcmoVeinCannula.TABLE_NAME;

    private static final String SQL_DELETE_DEFAULT_TEST_MEASUREMENT_TABLE =
            "DROP TABLE IF EXISTS " + IcupadDbContract.DefaultTestMeasurement.TABLE_NAME;

    private static final String SQL_DELETE_COMPLETE_BLOOD_COUNT_TEST_MEASUREMENT_TABLE =
            "DROP TABLE IF EXISTS " + IcupadDbContract.CompleteBloodCountTestMeasurement.TABLE_NAME;

    //INDEXES
    private static final String SQL_CREATE_STAY_PATIENT_ID_INDEX =
            "CREATE INDEX stay_patient_id_idx ON " +  IcupadDbContract.Stay.TABLE_NAME + "(" + IcupadDbContract.Stay.COLUMN_NAME_PATIENT_ID +")";

    private static final String SQL_CREATE_BLOOD_GAS_TEST_MEASUREMENT_STAY_ID_INDEX =
            "CREATE INDEX bloodgastestmeasurement_stayid_idx ON " +  IcupadDbContract.BloodGasTestMeasurement.TABLE_NAME + "(" + IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_STAY_ID +")";

    private static final String SQL_CREATE_COMPLETE_BLOOD_COUNT_TEST_MEASUREMENT_STAY_ID_INDEX =
            "CREATE INDEX completebloodgascounttestresult_stayid_idx ON " +  IcupadDbContract.DefaultTestMeasurement.TABLE_NAME + "(" + IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_STAY_ID +")";

    private static final String SQL_CREATE_DEFAULT_TEST_MEASUREMENT_STAY_ID_INDEX =
            "CREATE INDEX defaulttestresult_stayid_idx ON " +  IcupadDbContract.CompleteBloodCountTestMeasurement.TABLE_NAME + "(" + IcupadDbContract.CompleteBloodCountTestMeasurement.COLUMN_NAME_STAY_ID +")";


    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Infos.db";

    public IcupadDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public IcupadDbHelper(Context context, String dbName, int dbVersion) {
        super(context, dbName, null, dbVersion);
    }

    private void createIndexes(SQLiteDatabase db){
        db.execSQL(SQL_CREATE_STAY_PATIENT_ID_INDEX);
        db.execSQL(SQL_CREATE_BLOOD_GAS_TEST_MEASUREMENT_STAY_ID_INDEX);

        db.execSQL(SQL_CREATE_COMPLETE_BLOOD_COUNT_TEST_MEASUREMENT_STAY_ID_INDEX);
        db.execSQL(SQL_CREATE_DEFAULT_TEST_MEASUREMENT_STAY_ID_INDEX);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_PATIENT_TABLE);
        db.execSQL(SQL_CREATE_STAY_TABLE);
        db.execSQL(SQL_CREATE_OPERATION_TABLE);

        db.execSQL(SQL_CREATE_BLOOD_GAS_TEST_MEASUREMENT_TABLE);

        db.execSQL(SQL_CREATE_AUSCULTATE_SUITE_SCHEMA_TABLE);
        db.execSQL(SQL_CREATE_AUSCULTATE_POINT_TABLE);
        db.execSQL(SQL_CREATE_AUSCULTATE_SUITE_TABLE);
        db.execSQL(SQL_CREATE_AUSCULTATE_TABLE);

        db.execSQL(SQL_CREATE_SYNC_TABLE);
        db.execSQL(SQL_CREATE_CONFLICT_TABLE);

        db.execSQL(SQL_CREATE_ECMO_ACT_TABLE);
        db.execSQL(SQL_CREATE_ECMO_CONFIGURATION_TABLE);
        db.execSQL(SQL_CREATE_ECMO_PARAMETER_TABLE);
        db.execSQL(SQL_CREATE_ECMO_PATIENT_TABLE);
        db.execSQL(SQL_CREATE_ECMO_PROCEDURE_TABLE);
        db.execSQL(SQL_CREATE_ECMO_USED_STRUCTURES_TABLE);
        db.execSQL(SQL_CREATE_ECMO_VEIN_CANNULA_TABLE);

        db.execSQL(SQL_CREATE_COMPLETE_BLOOD_COUNT_TEST_MEASUREMENT_TABLE);
        db.execSQL(SQL_CREATE_DEFAULT_TEST_MEASUREMENT_TABLE);

        createIndexes(db);
    }

    public void onDelete(SQLiteDatabase db) {
        db.execSQL(SQL_DELETE_PATIENT);
        db.execSQL(SQL_DELETE_STAY);
        db.execSQL(SQL_DELETE_OPERATION);

        db.execSQL(SQL_DELETE_BLOOD_GAS_TEST_MEASUREMENT_TABLE);

        db.execSQL(SQL_DELETE_AUSCULTATE_TABLE);
        db.execSQL(SQL_DELETE_AUSCULTATE_POINT_TABLE);
        db.execSQL(SQL_DELETE_AUSCULTATE_SUITE_TABLE);
        db.execSQL(SQL_DELETE_AUSCULTATE_SUITE_SCHEMA_TABLE);

        db.execSQL(SQL_DELETE_SYNC_TABLE);
        db.execSQL(SQL_DELETE_CONFLICT_TABLE);

        db.execSQL(SQL_DELETE_ECMO_ACT_TABLE);
        db.execSQL(SQL_DELETE_ECMO_CONFIGURATION_TABLE);
        db.execSQL(SQL_DELETE_ECMO_PARAMETER_TABLE);
        db.execSQL(SQL_DELETE_ECMO_PATIENT_TABLE);
        db.execSQL(SQL_DELETE_ECMO_PROCEDURE_TABLE);
        db.execSQL(SQL_DELETE_ECMO_USED_STRUCTURES_TABLE);
        db.execSQL(SQL_DELETE_ECMO_VEIN_CANNULA_TABLE);

        db.execSQL(SQL_DELETE_DEFAULT_TEST_MEASUREMENT_TABLE);
        db.execSQL(SQL_DELETE_COMPLETE_BLOOD_COUNT_TEST_MEASUREMENT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion < DATABASE_VERSION) {
            onCreate(db);
        }
    }

    public SQLiteDatabase getReadableDatabase() {
        return getReadableDatabase("");
    }

    public SQLiteDatabase getReadableDatabase(String comment) {
        lock.lock();
        SQLiteDatabase db = super.getReadableDatabase(PrivateDataAccessor.getSqlitePassword());
        return db;
    }

    public SQLiteDatabase getWritableDatabase() {
        return getWritableDatabase("");
    }

    public SQLiteDatabase getWritableDatabase(String comment) {
        lock.lock();
        SQLiteDatabase db = super.getWritableDatabase(PrivateDataAccessor.getSqlitePassword());
        return db;
    }

    public void closeDb(SQLiteDatabase db) {
        lock.unlock();
        db.close();
    }
}