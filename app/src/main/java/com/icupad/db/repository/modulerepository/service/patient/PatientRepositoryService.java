package com.icupad.db.repository.modulerepository.service.patient;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.icupad.commons.repository.builder.OperationBuilder;
import com.icupad.commons.repository.builder.PatientBuilder;
import com.icupad.commons.repository.builder.PatientDetailBuilder;
import com.icupad.commons.repository.builder.PatientWithStayStatusBuilder;
import com.icupad.commons.repository.model.AllPatientsAndPresentPatients;
import com.icupad.commons.repository.model.Patient;
import com.icupad.commons.repository.model.PatientDetail;
import com.icupad.commons.repository.model.PatientWithStayStatus;
import com.icupad.commons.repository.model.Stay;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.utils.PrivateDataAccessor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.icupad.synchronization.utils.SyncStatus.DIRTY;

public class PatientRepositoryService {
    protected IcupadDbHelper mDbHelper;
    protected SyncTableRepository syncTableRepository;

    public PatientRepositoryService(Context context) {
        mDbHelper = new IcupadDbHelper(context);
        syncTableRepository = new SyncTableRepository(context);
    }

    public List<Patient> getAllPatients() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        Cursor cursor = getQueryForAllPatients(db)
                .select();

        List<Patient> list = getPatientsFromCursor(cursor);

        cursor.close();
        mDbHelper.closeDb(db);
        return list;
    }

    private Query getQueryForAllPatients(SQLiteDatabase db) {
        return QueryBuilder.aQuery(db)
                .withTable(
                        IcupadDbContract.Patient.TABLE_NAME + " JOIN " + IcupadDbContract.Stay.TABLE_NAME + " ON " +
                                IcupadDbContract.Patient.TABLE_NAME + "." + IcupadDbContract.Patient.COLUMN_NAME_ID + " = " +
                                IcupadDbContract.Stay.TABLE_NAME + "." + IcupadDbContract.Stay.COLUMN_NAME_PATIENT_ID
                )
                .withColumns("DISTINCT " +
                        IcupadDbContract.Patient.TABLE_NAME + "." + IcupadDbContract.Patient.COLUMN_NAME_ID,
                        IcupadDbContract.Patient.TABLE_NAME + "." + IcupadDbContract.Patient.COLUMN_NAME_NAME,
                        IcupadDbContract.Patient.TABLE_NAME + "." + IcupadDbContract.Patient.COLUMN_NAME_SURNAME,
                        IcupadDbContract.Patient.COLUMN_NAME_SEX,
                        IcupadDbContract.Patient.COLUMN_NAME_BIRTH_DATE,
                        IcupadDbContract.Stay.COLUMN_NAME_ADMIT_DATE,
                        IcupadDbContract.Stay.COLUMN_NAME_DISCHARGE_DATE,
                        IcupadDbContract.Stay.COLUMN_NAME_ACTIVE
                )
                .withSelection(IcupadDbContract.Patient.TABLE_NAME + "." + IcupadDbContract.Patient.COLUMN_NAME_SYNC + "<>?")
                .withSelectionArgs(SyncStatus.REMOTE_CONFLICT.getValue())
                .withOrderBy(IcupadDbContract.Stay.COLUMN_NAME_ADMIT_DATE + " DESC")
                .build();
    }

    @NonNull
    private List<Patient> getPatientsFromCursor(Cursor cursor) {
        List<Patient> list = new ArrayList<>();
        Set<Long> addedPatients = new HashSet<>();
        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            Patient patient = PatientBuilder.aPatient()
                    .withId(cursor.getLong(0))
                    .withName(cursor.getString(1))
                    .withSurname(cursor.getString(2))
                    .withSex(cursor.getString(3))
                    .withBirthDate(cursor.getString(4))
                    .withAdmitDate(cursor.getString(5))
                    .withDischargeDate(cursor.getString(6))
                    .withActive(cursor.getString(7))
                    .build();
            if(addedPatients.add(patient.getId())) {
                list.add(patient);
            }
        }
        return list;
    }

    public PatientDetail getPatientDetail(long id) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        Query query = getQueryForPatientDetailById(id, db);

        Cursor cursor = query.select();
        PatientDetail patientDetail = getPatientDetailFromCursor(cursor);
        cursor.close();

        mDbHelper.closeDb(db);
        return patientDetail;
    }

    private Query getQueryForPatientDetailById(long id, SQLiteDatabase db) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.Patient.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.Patient.COLUMN_NAME_SURNAME,
                        IcupadDbContract.Patient.COLUMN_NAME_NAME,
                        IcupadDbContract.Patient.COLUMN_NAME_SEX,
                        IcupadDbContract.Patient.COLUMN_NAME_BIRTH_DATE,
                        IcupadDbContract.Patient.COLUMN_NAME_PESEL,
                        IcupadDbContract.Patient.COLUMN_NAME_STREET,
                        IcupadDbContract.Patient.COLUMN_NAME_STREET_NUMBER,
                        IcupadDbContract.Patient.COLUMN_NAME_HOUSE_NUMBER,
                        IcupadDbContract.Patient.COLUMN_NAME_POSTAL_CODE,
                        IcupadDbContract.Patient.COLUMN_NAME_CITY,
                        IcupadDbContract.Patient.COLUMN_NAME_HEIGHT,
                        IcupadDbContract.Patient.COLUMN_NAME_WEIGHT,
                        IcupadDbContract.Patient.COLUMN_NAME_BLOOD_TYPE,
                        IcupadDbContract.Patient.COLUMN_NAME_ALLERGIES,
                        IcupadDbContract.Patient.COLUMN_NAME_OTHER_IMPORTANT_INFORMATIONS
                )
                .withSelection(IcupadDbContract.Patient.COLUMN_NAME_ID + " = ?")
                .withSelectionArgs(id)
                .build();
    }

    private PatientDetail getPatientDetailFromCursor(Cursor cursor) {
        cursor.moveToFirst();
        if(!cursor.isAfterLast()) {
            return PatientDetailBuilder.aPatientDetail()
                    .withSurname(cursor.getString(0))
                    .withName(cursor.getString(1))
                    .withSex(cursor.getString(2))
                    .withBirthday(cursor.getString(3))
                    .withPesel(cursor.getString(4))
                    .withStreet(cursor.getString(5))
                    .withStreetNumber(cursor.getString(6))
                    .withHouseNumber(cursor.getString(7))
                    .withPostalCode(cursor.getString(8))
                    .withCity(cursor.getString(9))
                    .withHeight(cursor.getString(10))
                    .withWeight(cursor.getString(11))
                    .withBloodType(cursor.getString(12))
                    .withAllergies(cursor.getString(13))
                    .withOtherImportantInformations(cursor.getString(14))
                    .build();
        }
        else {
            return null;
        }
    }

    public List<Stay> getPatientStays(long id){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        List<Stay> stays = new ArrayList<>();
        Cursor cursor = getQueryForSortedDescStaysByPatientId(id, db)
                .select();

        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            Stay stay = new Stay(cursor.getLong(0), cursor.getString(1), cursor.getString(2));
            updateStayWithOperations(db, stay);
            stays.add(stay);
        }

        cursor.close();
        mDbHelper.closeDb(db);
        return stays;
    }

    private Query getQueryForSortedDescStaysByPatientId(long patientId, SQLiteDatabase db) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.Stay.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.Stay.COLUMN_NAME_ID,
                        IcupadDbContract.Stay.COLUMN_NAME_ADMIT_DATE,
                        IcupadDbContract.Stay.COLUMN_NAME_DISCHARGE_DATE)
                .withSelection(IcupadDbContract.Stay.COLUMN_NAME_PATIENT_ID + " = ?")
                .withSelectionArgs(patientId)
                .withOrderBy(IcupadDbContract.Stay.COLUMN_NAME_ADMIT_DATE + " desc")
                .build();
    }

    private void updateStayWithOperations(SQLiteDatabase db, Stay stay) {
        Cursor operationCursor = getQueryForSortedDescNotArchivedOperationsByStayId(db, stay)
                .select();
        for(operationCursor.moveToFirst(); !operationCursor.isAfterLast(); operationCursor.moveToNext()) {
            stay.addOperation(OperationBuilder.anOperation()
                    .withInternalId(operationCursor.getLong(0))
                    .withOperatingDate(operationCursor.getString(1))
                    .withDescription(operationCursor.getString(2)).build());
        }
        operationCursor.close();
    }

    private Query getQueryForSortedDescNotArchivedOperationsByStayId(SQLiteDatabase db, Stay stay) {
        return QueryBuilder.aQuery(db)
                    .withTable(IcupadDbContract.Operation.TABLE_NAME)
                    .withColumns(
                            IcupadDbContract.INTERNAL_ID,
                            IcupadDbContract.Operation.COLUMN_NAME_OPERATING_DATE,
                            IcupadDbContract.Operation.COLUMN_NAME_DESCRIPTION)
                    .withSelection(IcupadDbContract.Operation.COLUMN_NAME_STAY_ID + "=? and " +
                            IcupadDbContract.Operation.COLUMN_NAME_ARCHIVED + " <> 1")
                    .withSelectionArgs(stay.getId())
                    .withOrderBy(IcupadDbContract.Operation.COLUMN_NAME_OPERATING_DATE + " desc")
                    .build();
    }

    public void updatePatient(long id, PatientDetail patientDetail) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = getValuesForUpdatingPatient(patientDetail);

        Query query = getQueryForPatientsInternalIdById(id, db);
        query.update(values);
        updateSyncTableWithChangedPatient(query);

        mDbHelper.closeDb(db);
    }

    @NonNull
    private ContentValues getValuesForUpdatingPatient(PatientDetail patientDetail) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.Patient.COLUMN_NAME_HEIGHT, patientDetail.getHeight());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_WEIGHT, patientDetail.getWeight());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_BLOOD_TYPE, patientDetail.getBloodType());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_ALLERGIES, patientDetail.getAllergies());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_OTHER_IMPORTANT_INFORMATIONS, patientDetail.getOtherImportantInformations());
        values.put(IcupadDbContract.Patient.COLUMN_NAME_SYNC, DIRTY.getValue());
        return values;
    }

    private Query getQueryForPatientsInternalIdById(long id, SQLiteDatabase db) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.Patient.TABLE_NAME)
                .withColumns(IcupadDbContract.INTERNAL_ID)
                .withSelection(IcupadDbContract.Patient.COLUMN_NAME_ID + "=?")
                .withSelectionArgs(id)
                .build();
    }

    @NonNull
    private void updateSyncTableWithChangedPatient(Query query) {
        Cursor cursor = query.select();
        cursor.moveToFirst();
        if( !cursor.isAfterLast()) {
            syncTableRepository.logUpdate(IcupadDbContract.Patient.TABLE_NAME, cursor.getLong(0));
        }
        cursor.close();
    }

    public Set<Long> getPresentPatientIds() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        Cursor presentPatientIdsCursor = getQueryForPresentPatientIds(db)
                .select();
        Set<Long> presentPatientIds = getPatientIdsFromCursor(presentPatientIdsCursor);

        presentPatientIdsCursor.close();
        mDbHelper.closeDb(db);

        return presentPatientIds;
    }

    private Query getQueryForPresentPatientIds(SQLiteDatabase db) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.Stay.TABLE_NAME)
                .withColumns(IcupadDbContract.Stay.COLUMN_NAME_PATIENT_ID)
                .withSelection(IcupadDbContract.Stay.COLUMN_NAME_SYNC + "<>? AND " +
                        IcupadDbContract.Stay.COLUMN_NAME_ACTIVE + "=1"
                )
                .withSelectionArgs(SyncStatus.REMOTE_CONFLICT.getValue())
                .build();
    }

    private Set<Long> getPatientIdsFromCursor(Cursor cursor) {
        Set<Long> patientIds = new HashSet<>();
        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            patientIds.add(cursor.getLong(0));
        }
        return patientIds;
    }

    public void removeOperationsForPatientId(Long patientId) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        db.execSQL("DELETE FROM " + IcupadDbContract.Operation.TABLE_NAME +
                    " WHERE " + IcupadDbContract.Operation.COLUMN_NAME_STAY_ID + " IN (" +
                            " SELECT " +  IcupadDbContract.Stay.COLUMN_NAME_ID + " FROM " + IcupadDbContract.Stay.TABLE_NAME +
                            " WHERE " + IcupadDbContract.Stay.COLUMN_NAME_PATIENT_ID  + " = " + patientId + ")");

        mDbHelper.closeDb(db);
    }
}
