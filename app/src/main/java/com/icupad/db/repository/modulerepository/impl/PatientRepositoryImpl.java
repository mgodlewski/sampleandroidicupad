package com.icupad.db.repository.modulerepository.impl;

import android.content.Context;

import com.icupad.db.repository.modulerepository.service.patient.OperationRepositoryService;
import com.icupad.db.repository.modulerepository.service.patient.PatientRepositoryService;
import com.icupad.commons.repository.model.Operation;
import com.icupad.commons.repository.model.Patient;
import com.icupad.commons.repository.model.PatientDetail;
import com.icupad.commons.repository.model.Stay;

import java.util.List;
import java.util.Set;

public class PatientRepositoryImpl{
    private PatientRepositoryService patientRepositoryService;
    private OperationRepositoryService operationRepositoryService;

    public PatientRepositoryImpl(Context context) {
        patientRepositoryService = new PatientRepositoryService(context);
        operationRepositoryService = new OperationRepositoryService(context);
    }

    public List<Patient> getAllPatients() {
        return patientRepositoryService.getAllPatients();
    }

    public PatientDetail getPatientDetail(long id) {
        return patientRepositoryService.getPatientDetail(id);
    }

    public List<Stay> getPatientStays(long id){
        return patientRepositoryService.getPatientStays(id);
    }

    public void updatePatient(long patientId, PatientDetail patientDetail) {
        patientRepositoryService.updatePatient(patientId, patientDetail);
    }

    public void addOperation(Operation operation, long stayId) {
        operationRepositoryService.addOperation(operation, stayId);
    }

    public void updateOperation(Operation operation) {
        operationRepositoryService.updateOperation(operation);
    }

    public void archiveOperation(long operationInternalId) {
        operationRepositoryService.archiveOperation(operationInternalId);
    }

    public Set<Long> getPresentPatientIds() {
        return patientRepositoryService.getPresentPatientIds();
    }

    public void removeOperationsForPatientId(Long patientId) {
        patientRepositoryService.removeOperationsForPatientId(patientId);
    }
}
