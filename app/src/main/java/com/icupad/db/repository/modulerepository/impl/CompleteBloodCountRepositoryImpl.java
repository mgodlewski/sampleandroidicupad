package com.icupad.db.repository.modulerepository.impl;

import android.content.Context;

import com.icupad.commons.repository.model.BloodGasMeasurement;
import com.icupad.db.repository.modulerepository.service.completebloodcount.CompleteBloodCountRepositoryService;

import java.util.List;

/**
 * Created by Marcin on 23.05.2017.
 */
public class CompleteBloodCountRepositoryImpl {

    private CompleteBloodCountRepositoryService completeBloodCountRepositoryService;

    public CompleteBloodCountRepositoryImpl(Context context) {
        this.completeBloodCountRepositoryService = new CompleteBloodCountRepositoryService(context);
    }

    public List<BloodGasMeasurement> getCompleteBloodCountMeasurementDOMs(long stayId) {
        return completeBloodCountRepositoryService.getCompleteBloodCountMeasurementDOMs(stayId);
    }


    public void removeCompleteBloodCountMeasurementsForPatientId(Long patientId) {
        completeBloodCountRepositoryService.removeCompleteBloodCountMeasurementsForPatientId(patientId);
    }
}
