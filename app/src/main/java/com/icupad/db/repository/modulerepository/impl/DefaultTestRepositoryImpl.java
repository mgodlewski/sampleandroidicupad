package com.icupad.db.repository.modulerepository.impl;

import android.content.Context;

import com.icupad.commons.repository.model.BloodGasMeasurement;
import com.icupad.db.repository.modulerepository.service.defaulttest.DefaultTestRepositoryService;

import java.util.List;

/**
 * Created by Marcin on 23.05.2017.
 */
public class DefaultTestRepositoryImpl {

    private DefaultTestRepositoryService defaultTestRepositoryService;

    public DefaultTestRepositoryImpl(Context context) {
        this.defaultTestRepositoryService = new DefaultTestRepositoryService(context);
    }

    public List<BloodGasMeasurement> getDefaultMeasurementDOMs(long stayId) {
        return defaultTestRepositoryService.getDefaultMeasurementDOMs(stayId);
    }


    public void removeDefaultMeasurementForPatientId(Long patientId) {
        defaultTestRepositoryService.removeDefaultMeasurementForPatientId(patientId);
    }
}
