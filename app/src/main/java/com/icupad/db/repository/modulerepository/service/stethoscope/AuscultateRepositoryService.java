package com.icupad.db.repository.modulerepository.service.stethoscope;

import android.content.ContentValues;
import android.content.Context;
import net.sqlcipher.database.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.icupad.commons.repository.model.AuscultatePointTest;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.utils.PrivateDataAccessor;

public class AuscultateRepositoryService {
    protected IcupadDbHelper mDbHelper;
    protected SyncTableRepository syncTableRepository;
    private String tableName;

    public AuscultateRepositoryService(Context context) {
        mDbHelper = new IcupadDbHelper(context);
        syncTableRepository = new SyncTableRepository(context);
        tableName = IcupadDbContract.Auscultate.TABLE_NAME;
    }

    public void updateAuscultate(AuscultatePointTest auscultatePointTest) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        updateAuscultateWithValues(db, auscultatePointTest.getInternalId(),
                getPointValuesToUpdate(auscultatePointTest));
        syncTableRepository.logUpdate(tableName, auscultatePointTest.getInternalId());

        mDbHelper.closeDb(db);
    }
    @NonNull
    private ContentValues getPointValuesToUpdate(AuscultatePointTest auscultatePointTest) {
        ContentValues values;
        values = new ContentValues();
        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_DESCRIPTION, auscultatePointTest.getDescription());
        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_POLL_RESULT, auscultatePointTest.getPollResult());
        return values;
    }

    private void updateAuscultateWithValues(SQLiteDatabase db, long internalId, ContentValues values) {
        Query queryInsertAuscultation = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.Auscultate.TABLE_NAME)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                .withSelectionArgs(internalId)
                .build();
        queryInsertAuscultation.update(values);
    }

    public void removeAuscultatesForPatientId(Long patientId) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        db.execSQL("DELETE FROM " + IcupadDbContract.Auscultate.TABLE_NAME +
                " WHERE " + IcupadDbContract.Auscultate.COLUMN_NAME_SUITE_ID + " IN (" +
                " SELECT " +  IcupadDbContract.AuscultateSuite.COLUMN_NAME_ID + " FROM " + IcupadDbContract.AuscultateSuite.TABLE_NAME +
                " WHERE " + IcupadDbContract.AuscultateSuite.COLUMN_NAME_PATIENT_ID  + " = " + patientId + ")");

        mDbHelper.closeDb(db);
    }
}
