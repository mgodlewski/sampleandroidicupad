package com.icupad.db.repository.conflictrepository;

import android.content.Context;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.utils.PrivateDataAccessor;

public class ConflictTableRepository {
    private IcupadDbHelper mDbHelper;

    public ConflictTableRepository(Context context) {
        mDbHelper = new IcupadDbHelper(context);
    }

    public boolean checkConflictExistence() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.ConflictTable.TABLE_NAME)
                .build();

        Cursor cursor = query.select();
        cursor.moveToFirst();
        boolean conflictExist = !cursor.isAfterLast();

        cursor.close();
        mDbHelper.closeDb(db);
        return conflictExist;
    }
}
