package com.icupad.db.repository.modulerepository.service.stethoscope;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.icupad.commons.repository.builder.AuscultatePointBuilder;
import com.icupad.commons.repository.builder.SpinnerAuscultateSuiteSchemaBuilder;
import com.icupad.commons.repository.model.AuscultatePoint;
import com.icupad.commons.repository.model.SpinnerAuscultateSuiteSchema;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.utils.PrivateDataAccessor;

import java.util.ArrayList;
import java.util.List;

public class SuiteSchemaRepositoryService {
    protected IcupadDbHelper mDbHelper;
    protected SyncTableRepository syncTableRepository;

    public SuiteSchemaRepositoryService(Context context) {
        mDbHelper = new IcupadDbHelper(context);
        syncTableRepository = new SyncTableRepository(context);
    }

    public List<SpinnerAuscultateSuiteSchema> findSyncedSuiteSchemas() {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        List<SpinnerAuscultateSuiteSchema> suiteSchemas = new ArrayList<>();

        Cursor suiteSchemaCursor = getQueryForSyncedSuiteSchemasWithIdAndName(db)
                .select();
        for(suiteSchemaCursor.moveToFirst(); !suiteSchemaCursor.isAfterLast(); suiteSchemaCursor.moveToNext()) {
            suiteSchemas.add(
                    getSuiteSchemaWithPoints(suiteSchemaCursor.getInt(0), suiteSchemaCursor.getString(1)));
        }

        suiteSchemaCursor.close();
        mDbHelper.closeDb(db);

        return suiteSchemas;
    }

    private Query getQueryForSyncedSuiteSchemasWithIdAndName(SQLiteDatabase db) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.AuscultateSuiteSchema.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_ID,
                        IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_NAME)
                .withSelection(IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_ID + " IS NOT NULL")
                .build();
    }

    private SpinnerAuscultateSuiteSchema getSuiteSchemaWithPoints(int suiteSchemaId, String suiteSchemaName) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        List<AuscultatePoint> points = new ArrayList<>();

        Query queryPoint = getQueryForPointBySuiteSchemaId(suiteSchemaId, db);
        Cursor pointCursor = queryPoint.select();
        for(pointCursor.moveToFirst(); !pointCursor.isAfterLast(); pointCursor.moveToNext()) {
            points.add(getPointFromCursor(pointCursor));
        }
        pointCursor.close();

        return createSuiteSchema(suiteSchemaId, suiteSchemaName, points);
    }

    private Query getQueryForPointBySuiteSchemaId(int suiteSchemaId, SQLiteDatabase db) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.AuscultatePoint.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_ID,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_NAME,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_QUEUE_POSITION,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_X,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_Y,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_IS_FRONT)
                .withSelection(IcupadDbContract.AuscultatePoint.COLUMN_NAME_SUITE_SCHEMA_ID + " = ?")
                .withSelectionArgs(suiteSchemaId)
                .withOrderBy(IcupadDbContract.AuscultatePoint.COLUMN_NAME_QUEUE_POSITION)
                .build();
    }

    private AuscultatePoint getPointFromCursor(Cursor pointCursor) {
        return AuscultatePointBuilder.anAuscultatePoint()
                .withId(pointCursor.getLong(0))
                .withName(pointCursor.getString(1))
                .withQueueOrder(pointCursor.getInt(2))
                .withX(pointCursor.getDouble(3))
                .withY(pointCursor.getDouble(4))
                .withIsFront(pointCursor.getInt(5)==1)
                .build();
    }

    private SpinnerAuscultateSuiteSchema createSuiteSchema(int suiteSchemaId, String suiteSchemaName, List<AuscultatePoint> points) {
        return SpinnerAuscultateSuiteSchemaBuilder.aSpinnerAuscultateSuiteSchema()
                .withId(suiteSchemaId)
                .withName(suiteSchemaName)
                .withPoints(points)
                .build();
    }




    public void saveAuscultateSuiteSchema(SpinnerAuscultateSuiteSchema spinnerAuscultateSuiteSchema) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = getSuiteSchemaValuesToInsert(spinnerAuscultateSuiteSchema);

        long suiteSchemaInternalId = insertSuiteSchemaValuesToDbAndReturnSuiteSchemaInternalId(db, values);

        for(AuscultatePoint point : spinnerAuscultateSuiteSchema.getPoints()) {
            values = getPointValuesToInsert(suiteSchemaInternalId, point);
            insertPointValuesToDb(db, values);
        }
        syncTableRepository.logUpdate(IcupadDbContract.AuscultateSuiteSchema.TABLE_NAME, suiteSchemaInternalId);

        mDbHelper.closeDb(db);
    }

    @NonNull
    private ContentValues getSuiteSchemaValuesToInsert(SpinnerAuscultateSuiteSchema spinnerAuscultateSuiteSchema) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_NAME, spinnerAuscultateSuiteSchema.getName());
        return values;
    }

    private long insertSuiteSchemaValuesToDbAndReturnSuiteSchemaInternalId(SQLiteDatabase db, ContentValues values) {
        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.AuscultateSuiteSchema.TABLE_NAME)
                .build();
        return query.insert(values);
    }

    @NonNull
    private ContentValues getPointValuesToInsert(long suiteSchemaInternalId, AuscultatePoint point) {
        ContentValues values;
        values = new ContentValues();
        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_NAME, point.getName());
        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_QUEUE_POSITION, point.getQueueOrder());
        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_X, point.getX());
        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_Y, point.getY());
        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_IS_FRONT, point.isFront());
        values.put(IcupadDbContract.AuscultatePoint.COLUMN_NAME_SUITE_SCHEMA_INTERNAL_ID, suiteSchemaInternalId);
        return values;
    }

    private void insertPointValuesToDb(SQLiteDatabase db, ContentValues values) {
        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.AuscultatePoint.TABLE_NAME)
                .build();
        query.insert(values);
    }
}
