package com.icupad.db.repository.modulerepository.impl;

import android.content.Context;

import com.icupad.commons.repository.model.AuscultatePointTest;
import com.icupad.commons.repository.model.AuscultateSuiteTest;
import com.icupad.commons.repository.model.ListAuscultateExamination;
import com.icupad.commons.repository.model.SpinnerAuscultateSuiteSchema;
import com.icupad.db.repository.modulerepository.service.stethoscope.AuscultateRepositoryService;
import com.icupad.db.repository.modulerepository.service.stethoscope.SuiteRepositoryService;
import com.icupad.db.repository.modulerepository.service.stethoscope.SuiteSchemaRepositoryService;

import java.util.List;

public class AuscultateRepositoryImpl{
    private SuiteRepositoryService suiteRepositoryService;
    private SuiteSchemaRepositoryService suiteSchemaRepositoryService;
    private AuscultateRepositoryService auscultateRepositoryService;

    public AuscultateRepositoryImpl(Context context) {
        suiteRepositoryService = new SuiteRepositoryService(context);
        suiteSchemaRepositoryService = new SuiteSchemaRepositoryService(context);
        auscultateRepositoryService = new AuscultateRepositoryService(context);
    }

    public List<ListAuscultateExamination> findAllSuitesByPatientId(long patientId) {
        return suiteRepositoryService.findAllSuitesByPatientId(patientId);
    }

    public AuscultateSuiteTest findAuscultateSuiteTest(long internalSuiteId) {
       return suiteRepositoryService.findAuscultateSuiteTest(internalSuiteId);
    }

    public List<SpinnerAuscultateSuiteSchema> findSyncedSuiteSchemas() {
        return suiteSchemaRepositoryService.findSyncedSuiteSchemas();
    }

    public void saveAuscultateSuiteSchema(SpinnerAuscultateSuiteSchema spinnerAuscultateSuiteSchema) {
        suiteSchemaRepositoryService.saveAuscultateSuiteSchema(spinnerAuscultateSuiteSchema);
    }

    public void saveAuscultateSuiteTest(AuscultateSuiteTest auscultateSuiteTest) {
        suiteRepositoryService.saveAuscultateSuiteTest(auscultateSuiteTest);
    }

    public void updateAuscultateSuite(AuscultateSuiteTest auscultateSuiteTest) {
        suiteRepositoryService.updateAuscultateSuite(auscultateSuiteTest);
    }

    public void updateAuscultate(AuscultatePointTest auscultatePointTest) {
        auscultateRepositoryService.updateAuscultate(auscultatePointTest);
    }

    public void removeAuscultateSuitesForPatientId(Long patientId) {
        suiteRepositoryService.removeAuscultateSuitesForPatientId(patientId);
    }

    public void removeAuscultatesForPatientId(Long patientId) {
        auscultateRepositoryService.removeAuscultatesForPatientId(patientId);
    }
}