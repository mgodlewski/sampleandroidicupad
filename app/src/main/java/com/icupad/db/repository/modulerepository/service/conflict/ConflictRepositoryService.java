package com.icupad.db.repository.modulerepository.service.conflict;

import android.content.Context;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import com.icupad.commons.repository.builder.ConflictGeneralBuilder;
import com.icupad.commons.repository.model.ConflictGeneral;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.utils.PrivateDataAccessor;

import java.util.ArrayList;
import java.util.List;

public class ConflictRepositoryService {
    private IcupadDbHelper mDbHelper;

    public ConflictRepositoryService(Context context) {
        mDbHelper = new IcupadDbHelper(context);
    }

    public List<ConflictGeneral> getConflicts() {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        Cursor cursor = getQueryForAllConflicts(db)
                .select();

        List<ConflictGeneral> conflicts = getAllConflictsFromCursor(cursor);

        cursor.close();
        mDbHelper.closeDb(db);
        return conflicts;
    }

    private List<ConflictGeneral> getAllConflictsFromCursor(Cursor cursor) {
        List<ConflictGeneral> conflicts = new ArrayList<>();
        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            addConflictFromCursorToList(conflicts, cursor);
        }
        return conflicts;
    }

    private void addConflictFromCursorToList(List<ConflictGeneral> conflicts, Cursor cursor) {
        conflicts.add(ConflictGeneralBuilder.aConflictGeneral()
                .withTableName(getStringFromCursor(cursor, IcupadDbContract.ConflictTable.COLUMN_NAME_TABLE_NAME))
                .withLocalInternalId(getIntFromCursor(cursor, IcupadDbContract.ConflictTable.COLUMN_NAME_INFO_INTERNAL_ID))
                .withRemoteInternalId(getIntFromCursor(cursor, IcupadDbContract.ConflictTable.COLUMN_NAME_CONFLICT_INFO_INTERNAL_ID))
                .build()
        );
    }

    private Query getQueryForAllConflicts(SQLiteDatabase db) {
        return QueryBuilder.aQuery(db)
                    .withTable(IcupadDbContract.ConflictTable.TABLE_NAME)
                    .withColumns(IcupadDbContract.ConflictTable.COLUMN_NAME_TABLE_NAME,
                            IcupadDbContract.ConflictTable.COLUMN_NAME_INFO_INTERNAL_ID,
                            IcupadDbContract.ConflictTable.COLUMN_NAME_CONFLICT_INFO_INTERNAL_ID)
                    .build();
    }

    private String getStringFromCursor(Cursor cursor, String columnName) {
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

    private int getIntFromCursor(Cursor cursor, String columnName) {
        return cursor.getInt(cursor.getColumnIndex(columnName));
    }
}
