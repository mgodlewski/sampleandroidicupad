package com.icupad.db.repository.modulerepository.service.stethoscope;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.icupad.commons.repository.builder.AuscultatePointBuilder;
import com.icupad.commons.repository.builder.AuscultatePointTestBuilder;
import com.icupad.commons.repository.builder.ListAuscultateExaminationBuilder;
import com.icupad.commons.repository.model.AuscultatePointTest;
import com.icupad.commons.repository.model.AuscultateSuiteTest;
import com.icupad.commons.repository.model.ListAuscultateExamination;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.utils.LoggedUserInfo;
import com.icupad.utils.PrivateDataAccessor;

import java.util.ArrayList;
import java.util.List;

public class SuiteRepositoryService {
    private static final String NO_DATA = "brak danych";
    protected IcupadDbHelper mDbHelper;
    protected SyncTableRepository syncTableRepository;

    public SuiteRepositoryService(Context context) {
        mDbHelper = new IcupadDbHelper(context);
        syncTableRepository = new SyncTableRepository(context);
    }

    public List<ListAuscultateExamination> findAllSuitesByPatientId(long patientId) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        List<ListAuscultateExamination> list = new ArrayList<>();

        Cursor cursor = getQueryForSuitesByPatientId(patientId, db)
                .select();
        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            list.add(getExaminationFromCursor(cursor));
        }

        cursor.close();
        mDbHelper.closeDb(db);

        return list;
    }

    private Query getQueryForSuitesByPatientId(long patientId, SQLiteDatabase db) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.AuscultateSuite.TABLE_NAME + " JOIN " + IcupadDbContract.AuscultateSuiteSchema.TABLE_NAME + " ON "
                        + IcupadDbContract.AuscultateSuiteSchema.TABLE_NAME + "." + IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_ID + "="
                        + IcupadDbContract.AuscultateSuite.TABLE_NAME + "." + IcupadDbContract.AuscultateSuite.COLUMN_NAME_SUITE_SCHEMA_ID)
                .withColumns(
                        IcupadDbContract.AuscultateSuite.TABLE_NAME + "." + IcupadDbContract.AuscultateSuite._ID,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_SUITE_SCHEMA_ID,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXAMINATION_DATETIME,
                        IcupadDbContract.AuscultateSuiteSchema.COLUMN_NAME_NAME,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_DESCRIPTION,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXECUTOR_NAME,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXECUTOR_SURNAME,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_POSITION,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_TEMPERATURE,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_IS_RESPIRATED,
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_PASSIVE_OXYGEN_THERAPY)
                .withSelection(IcupadDbContract.AuscultateSuite.COLUMN_NAME_PATIENT_ID + "=?")
                .withSelectionArgs(patientId)
                .withOrderBy(IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXAMINATION_DATETIME + " DESC")
                .build();
    }

    private ListAuscultateExamination getExaminationFromCursor(Cursor cursor) {

        return ListAuscultateExaminationBuilder.aListAuscultateExamination()
                .withSuiteInternalId(cursor.getInt(0))
                .withSuiteSchemaId(cursor.getInt(1))
                .withDateTime(cursor.getString(2))
                .withExaminationName(cursor.getString(3))
                .withDescription(cursor.getString(4))
                .withExecutor(createExecutorString(cursor.getString(5), cursor.getString(6)))
                .withPosition(cursor.getInt(7))
                .withTemperature(cursor.getInt(8))
                .withRespirated(cursor.getInt(9) == 1)
                .withPassiveOxygenTherapy(cursor.getInt(10) == 1)
                .build();
    }

    private String createExecutorString(String name, String surname) {
        String executorString = "";
        if(name != null && !name.equals("")) {
            executorString += name + " ";
        }
        if(surname != null && !surname.equals("")) {
            executorString += surname;
        }
        if(executorString.equals("")) {
            executorString = NO_DATA;
        }
        return executorString;

    }


    public AuscultateSuiteTest findAuscultateSuiteTest(long suiteInternalId) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        AuscultateSuiteTest output = null;

        Cursor suiteCursor = getQueryForSuiteByInternalId(suiteInternalId, db)
                .select();
        suiteCursor.moveToFirst();

        if( !suiteCursor.isAfterLast()) {
            output = new AuscultateSuiteTest(
                    suiteCursor.getString(0), suiteInternalId);
            updateOutputWithPoints(suiteInternalId, suiteCursor.getLong(1), db, output);
        }
        suiteCursor.close();
        mDbHelper.closeDb(db);
        return output;
    }

    private Query getQueryForSuiteByInternalId(long suiteInternalId, SQLiteDatabase db) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.AuscultateSuite.TABLE_NAME)
                .withColumns(
                        IcupadDbContract.AuscultateSuite.COLUMN_NAME_DESCRIPTION,
                        IcupadDbContract.ID
                )
                .withSelection(IcupadDbContract.AuscultateSuite._ID + "=?")
                .withSelectionArgs(suiteInternalId)
                .build();
    }

    private void updateOutputWithPoints(long suiteInternalId, Long suiteId, SQLiteDatabase db, AuscultateSuiteTest output) {
        Query pointQuery = getQueryForOrderedPointsBySuiteInternalId(suiteInternalId, suiteId, db);
        Cursor pointCursor = pointQuery.select();
        for(pointCursor.moveToFirst(); !pointCursor.isAfterLast(); pointCursor.moveToNext()) {
            output.addAuctultatePointTest(getPointFromCursor(pointCursor));
        }
        pointCursor.close();
    }

    private Query getQueryForOrderedPointsBySuiteInternalId(long suiteInternalId, long suiteId, SQLiteDatabase db) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.Auscultate.TABLE_NAME +
                        " JOIN " + IcupadDbContract.AuscultatePoint.TABLE_NAME + " ON " +
                        IcupadDbContract.Auscultate.TABLE_NAME + "." + IcupadDbContract.Auscultate.COLUMN_NAME_POINT_ID + " = " +
                        IcupadDbContract.AuscultatePoint.TABLE_NAME + "." + IcupadDbContract.AuscultatePoint.COLUMN_NAME_ID)
                .withColumns(
                        IcupadDbContract.Auscultate.COLUMN_NAME_DESCRIPTION,
                        IcupadDbContract.Auscultate.COLUMN_NAME_WAV_NAME,
                        IcupadDbContract.Auscultate.TABLE_NAME + "." + IcupadDbContract.INTERNAL_ID,
                        IcupadDbContract.Auscultate.COLUMN_NAME_POLL_RESULT,
                        IcupadDbContract.AuscultatePoint.TABLE_NAME + "." + IcupadDbContract.AuscultatePoint.COLUMN_NAME_ID,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_NAME,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_QUEUE_POSITION,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_X,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_Y,
                        IcupadDbContract.AuscultatePoint.COLUMN_NAME_IS_FRONT)
                .withSelection(IcupadDbContract.Auscultate.COLUMN_NAME_SUITE_INTERNAL_ID + "=? OR " +
                        "(" + IcupadDbContract.Auscultate.COLUMN_NAME_SUITE_INTERNAL_ID + " IS NULL  AND ? IS NOT NULL AND " +
                        IcupadDbContract.Auscultate.COLUMN_NAME_SUITE_ID + "=?)")
                .withSelectionArgs(suiteInternalId, suiteId, suiteId)
                .withOrderBy(IcupadDbContract.AuscultatePoint.COLUMN_NAME_QUEUE_POSITION)
                .build();
    }

    private AuscultatePointTest getPointFromCursor(Cursor pointCursor) {
        return AuscultatePointTestBuilder.anAuscultatePointTest()
                .withDescription(pointCursor.getString(0))
                .withWavFile(pointCursor.getString(1))
                .withInternalId(pointCursor.getLong(2))
                .withPollResult(pointCursor.getInt(3))
                .withAuscultatePoint(AuscultatePointBuilder.anAuscultatePoint()
                        .withId(pointCursor.getLong(4))
                        .withName(pointCursor.getString(5))
                        .withQueueOrder(pointCursor.getInt(6))
                        .withX(pointCursor.getDouble(7))
                        .withY(pointCursor.getDouble(8))
                        .withIsFront(pointCursor.getInt(9) == 1)
                        .build()
                ).build();
    }




    public void saveAuscultateSuiteTest(AuscultateSuiteTest auscultateSuiteTest) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = getSuiteValuesToInsert(auscultateSuiteTest);
        long suiteInternalId = insertSuiteValuesToDbAndReturnInternalId(db, values);

        for(AuscultatePointTest apt : auscultateSuiteTest.getAuscultatePointTests()) {
            if( !apt.getWavFile().isEmpty()) {
                values = getPointValuesToInsert(suiteInternalId, apt);
                insertPointValuesToDb(db, values);
            }
        }
        mDbHelper.closeDb(db);

        syncTableRepository.logUpdate(IcupadDbContract.AuscultateSuite.TABLE_NAME, suiteInternalId);
    }

    private ContentValues getSuiteValuesToInsert(AuscultateSuiteTest auscultateSuiteTest) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_PATIENT_ID, auscultateSuiteTest.getPatientId());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_DESCRIPTION, auscultateSuiteTest.getDescription());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_SUITE_SCHEMA_ID, auscultateSuiteTest.getAuscultateSuiteSchemaId());

        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_POSITION, auscultateSuiteTest.getPosition());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_TEMPERATURE, auscultateSuiteTest.getTemperature());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_IS_RESPIRATED, auscultateSuiteTest.isRespirated());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_PASSIVE_OXYGEN_THERAPY, auscultateSuiteTest.isPassiveOxygenTherapy());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXAMINATION_DATETIME, auscultateSuiteTest.getExaminationDateTime());

        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXECUTOR_NAME, LoggedUserInfo.getName());
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_EXECUTOR_SURNAME, LoggedUserInfo.getSurname());
        return values;
    }

    private long insertSuiteValuesToDbAndReturnInternalId(SQLiteDatabase db, ContentValues values) {
        Query query = QueryBuilder.aQuery(db)
                    .withTable(IcupadDbContract.AuscultateSuite.TABLE_NAME)
                    .build();
        return query.insert(values);
    }

    //TODO move to AuscultateRepositoryService
    @NonNull
    private ContentValues getPointValuesToInsert(long suiteInternalId, AuscultatePointTest auscultatePointTest) {
        ContentValues values;
        values = new ContentValues();
        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_DESCRIPTION, auscultatePointTest.getDescription());
        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_WAV_NAME, auscultatePointTest.getWavFile());
        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_POINT_ID, auscultatePointTest.getAuscultatePoint().getId());
        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_SUITE_INTERNAL_ID, suiteInternalId);
        values.put(IcupadDbContract.Auscultate.COLUMN_NAME_POLL_RESULT, auscultatePointTest.getDescription());
        return values;
    }

    private void insertPointValuesToDb(SQLiteDatabase db, ContentValues values) {
        Query queryInsertAuscultation = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.Auscultate.TABLE_NAME)
                .build();
        queryInsertAuscultation.insert(values);
    }

    public void updateAuscultateSuite(AuscultateSuiteTest auscultateSuiteTest) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.AuscultateSuite.COLUMN_NAME_DESCRIPTION, auscultateSuiteTest.getDescription());

        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.AuscultateSuite.TABLE_NAME)
                .withSelection(IcupadDbContract.AuscultateSuite._ID + "=?")
                .withSelectionArgs(auscultateSuiteTest.getSuiteInternalId())
                .build();
        query.update(values);

        db.close();
    }

    public void removeAuscultateSuitesForPatientId(Long patientId) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.AuscultateSuite.TABLE_NAME)
                .withSelection(IcupadDbContract.AuscultateSuite.COLUMN_NAME_PATIENT_ID + " =? ")
                .withSelectionArgs(patientId)
                .build();
        query.delete();

        mDbHelper.closeDb(db);
    }
}
