package com.icupad.db.repository.modulerepository.impl;

import android.content.Context;

import com.icupad.commons.repository.model.EcmoAct;
import com.icupad.commons.repository.model.EcmoConfiguration;
import com.icupad.commons.repository.model.EcmoParameter;
import com.icupad.commons.repository.model.EcmoPatient;
import com.icupad.commons.repository.model.EcmoProcedure;
import com.icupad.db.repository.modulerepository.service.ecmo.EcmoRepositoryService;

import java.util.List;

/**
 * Created by Marcin on 22.03.2017.
 */
public class EcmoRepositoryImpl {
    EcmoRepositoryService ecmoRepositoryService;

    public EcmoRepositoryImpl(Context context) {
        this.ecmoRepositoryService = new EcmoRepositoryService(context);
    }

//    public Long getEcmoProcedureIdByStayIdAndSartDate(Long stayId, String startDate){
//        return ecmoRepositoryService.getEcmoProcedureIdByStayIdAndSartDate(stayId, startDate);
//    }

    public EcmoProcedure getEcmoProcedureById(Long procedureId){
        return ecmoRepositoryService.getEcmoProcedureById(procedureId);
    }

    public EcmoConfiguration getEcmoFullConfigurationByProcedureId(Long procedureId){
        return ecmoRepositoryService.getEcmoFullConfigurationByProcedureId(procedureId);
    }

    public List<EcmoConfiguration> getAllEcmoFullConfigurationsByPorcedureId(Long procedureId){
        return ecmoRepositoryService.getAllEcmoFullConfigurationsByPorcedureId(procedureId);
    }

    public Long saveEcmoProcedure(EcmoProcedure ecmoProcedure){
        return ecmoRepositoryService.saveEcmoProcedure(ecmoProcedure);
    }

    public void saveEcmoFullConfiguration(EcmoConfiguration ecmoConfiguration){
        ecmoRepositoryService.saveEcmoFullConfiguration(ecmoConfiguration);
    }

    public void setActualFalseInEcmoConfigurationsByProcedureId(Long procedureId){
        ecmoRepositoryService.setActualFalseInEcmoConfigurationsByProcedureId(procedureId);
    }

    public void endEcmoProcedure(EcmoProcedure ecmoProcedure){
        ecmoRepositoryService.endEcmoProcedure(ecmoProcedure);
    }

    public Long saveAct(EcmoAct ecmoAct){
        return ecmoRepositoryService.saveAct(ecmoAct);
    }

    public Long saveEcmoPatient(EcmoPatient ecmoPatient){
        return ecmoRepositoryService.saveEcmoPatient(ecmoPatient);
    }

    public Long saveEcmoParameter(EcmoParameter ecmoParameter) {
        return ecmoRepositoryService.saveEcmoParameter(ecmoParameter);
    }

    public List<EcmoParameter> getEcmoParameterByProcedureId(Long procedureId) {
        return ecmoRepositoryService.getEcmoParameterByProcedureId(procedureId);
    }

    public List<EcmoPatient> getEcmoPatientByProcedureId(Long procedureId) {
        return ecmoRepositoryService.getEcmoPatientByProcedureId(procedureId);
    }

    public List<EcmoAct> getEcmoActByProcedureId(Long procedureId) {
        return ecmoRepositoryService.getEcmoActByProcedureId(procedureId);
    }
}
