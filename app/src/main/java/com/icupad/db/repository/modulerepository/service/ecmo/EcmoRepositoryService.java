package com.icupad.db.repository.modulerepository.service.ecmo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.widget.TextView;

import com.icupad.R;
import com.icupad.commons.repository.model.EcmoAct;
import com.icupad.commons.repository.model.EcmoConfiguration;
import com.icupad.commons.repository.model.EcmoParameter;
import com.icupad.commons.repository.model.EcmoPatient;
import com.icupad.commons.repository.model.EcmoProcedure;
import com.icupad.commons.repository.model.VeinCannula;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.webconnection.dto.ecmo.EcmoConfigurationDto;
import com.icupad.webconnection.dto.ecmo.EcmoProcedureDto;
import com.icupad.webconnection.dto.ecmo.VeinCannulaDto;

import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 22.03.2017.
 */
public class EcmoRepositoryService {
    protected IcupadDbHelper mDbHelper;
    protected SyncTableRepository syncTableRepository;

    public EcmoRepositoryService(Context context) {
        mDbHelper = new IcupadDbHelper(context);
        syncTableRepository = new SyncTableRepository(context);
    }


    //**********ECMO PROCEDURE*********//
    public EcmoProcedure getEcmoProcedureById(Long procedureId){
        EcmoProcedure ecmoProcedure = new EcmoProcedure();
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoProcedure.TABLE_NAME)
                .withColumns(IcupadDbContract.EcmoProcedure.TABLE_NAME_START_DATE, IcupadDbContract.EcmoProcedure.TABLE_NAME_START_CAUSE,
                        IcupadDbContract.EcmoProcedure.TABLE_NAME_END_DATE, IcupadDbContract.EcmoProcedure.TABLE_NAME_END_CAUSE)
                .withSelection(IcupadDbContract.EcmoProcedure._ID + " =?")
                .withSelectionArgs(procedureId)
                .withOrderBy(IcupadDbContract.EcmoProcedure.TABLE_NAME_START_DATE)
                .build();

        Cursor cursor = query.select();
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            ecmoProcedure.setStartDate(cursor.getString(0));
            ecmoProcedure.setStartCause(cursor.getString(1));
            ecmoProcedure.setEndDate(cursor.getString(2));
            ecmoProcedure.setEndCause(cursor.getString(3));
            cursor.moveToNext();
        }

        cursor.close();
        mDbHelper.closeDb(db);
        return ecmoProcedure;
    }

    public Long saveEcmoProcedure(EcmoProcedure ecmoProcedure){
        Long procedureId = 0L;
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = getValuesForCreateEcmoProcedure(ecmoProcedure);

        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoProcedure.TABLE_NAME)
                .build();

        procedureId = query.insert(values);

        syncTableRepository.logUpdate(IcupadDbContract.EcmoProcedure.TABLE_NAME, procedureId);

        mDbHelper.closeDb(db);
        return procedureId;
    }

    @NonNull
    private ContentValues getValuesForCreateEcmoProcedure(EcmoProcedure ecmoProcedure){
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_STAY, ecmoProcedure.getStayId());
        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_START_DATE, ecmoProcedure.getStartDate());
        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_START_CAUSE, ecmoProcedure.getStartCause());
        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_CREATED_BY, ecmoProcedure.getCreatedById());
        return values;
    }

    //**********ECMO CONFIGURATION*********//
    public EcmoConfiguration getEcmoFullConfigurationByProcedureId(Long procedureId){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        EcmoConfiguration ecmoConfiguration = getEcmoConfigurationByProcedureId(db, procedureId);
        ecmoConfiguration.setCannulationStructures(getUsedStructuresByConfigurationIdAndDate(db, ecmoConfiguration.getId(), ecmoConfiguration.getDate()));
        ecmoConfiguration.setVeinCannulaList(getVeinCannulaByConfigurationIdAndDate(db, ecmoConfiguration.getId(), ecmoConfiguration.getDate()));

        mDbHelper.closeDb(db);

        return ecmoConfiguration;
    }

    private EcmoConfiguration getEcmoConfigurationByProcedureId(SQLiteDatabase db, Long procedureId){
        EcmoConfiguration ecmoConfiguration = new EcmoConfiguration();

        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoConfiguration.TABLE_NAME)
                .withColumns(IcupadDbContract.EcmoConfiguration.TABLE_NAME_MODE,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_CANIULATION_TYPE,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_WENTOWANIE_LEFT_VERTICLE,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_ARTERY_CANNULA_TYPE,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_ARTERY_CANNULA_SIZE,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_OXYGENERATOR_TYPE,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_DATE,
                        IcupadDbContract.EcmoConfiguration._ID)
                .withSelection(IcupadDbContract.EcmoConfiguration.TABLE_NAME_PROCEDURE_ID + " =? AND " + IcupadDbContract.EcmoConfiguration.TABLE_NAME_ACTUAL + "=?")
                .withSelectionArgs(procedureId,"1")
                .withOrderBy(IcupadDbContract.EcmoConfiguration.TABLE_NAME_DATE)
                .build();

        Cursor cursor = query.select();
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            if (!cursor.isNull(0))
                ecmoConfiguration.setMode(cursor.getString(0));
            if (!cursor.isNull(1))
                ecmoConfiguration.setCaniulationType(cursor.getString(1));
            if (!cursor.isNull(2))
                ecmoConfiguration.setWentowanieLeftVentricle(cursor.getString(2));
            if (!cursor.isNull(3))
                ecmoConfiguration.setArteryCannulaType(cursor.getString(3));
            if(!cursor.isNull(4)){
                ecmoConfiguration.setArteryCannulaSize(cursor.getDouble(4));
            }else{
                ecmoConfiguration.setArteryCannulaSize(null);
            }
            if (!cursor.isNull(5))
                ecmoConfiguration.setOxygeneratorType(cursor.getString(5));
            if (!cursor.isNull(6))
                ecmoConfiguration.setDate(cursor.getString(6));
            if (!cursor.isNull(7))
                ecmoConfiguration.setId(cursor.getLong(7));
            cursor.moveToNext();
        }
        cursor.close();
        return ecmoConfiguration;
    }

    private List<String> getUsedStructuresByConfigurationIdAndDate(SQLiteDatabase db, Long configurationId, String date){
        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoUsedStructures.TABLE_NAME)
                .withColumns(IcupadDbContract.EcmoUsedStructures.TABLE_NAME_NAME)
                .withSelection(IcupadDbContract.EcmoUsedStructures.TABLE_NAME_CONFIGURATION_ID + " =? AND " + IcupadDbContract.EcmoUsedStructures.TABLE_NAME_DATE + " =?")
                .withSelectionArgs(configurationId, date)
                .build();

        Cursor cursor = query.select();
        cursor.moveToFirst();

        List<String> cannulationStructersList = new ArrayList<>();
        while(!cursor.isAfterLast()) {
            cannulationStructersList.add(cursor.getString(0));
            cursor.moveToNext();
        }

        cursor.close();
        return cannulationStructersList;
    }

    private List<VeinCannula> getVeinCannulaByConfigurationIdAndDate(SQLiteDatabase db, Long configurationId, String date){
        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoVeinCannula.TABLE_NAME)
                .withColumns(IcupadDbContract.EcmoVeinCannula.TABLE_NAME_TYPE, IcupadDbContract.EcmoVeinCannula.TABLE_NAME_SIZE)
                .withSelection(IcupadDbContract.EcmoVeinCannula.TABLE_NAME_CONFIGURATION_ID + " =? AND " + IcupadDbContract.EcmoVeinCannula.TABLE_NAME_DATE + " =?")
                .withSelectionArgs(configurationId, date)
                .build();

        Cursor cursor = query.select();
        cursor.moveToFirst();

        List<VeinCannula> veinCannulaList= new ArrayList<>();
        while(!cursor.isAfterLast()) {
            veinCannulaList.add(new VeinCannula(cursor.getString(0), cursor.getDouble(1)));
            cursor.moveToNext();
        }
        cursor.close();
        return veinCannulaList;
    }

    public List<EcmoConfiguration> getAllEcmoFullConfigurationsByPorcedureId(Long procedureId){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        List<EcmoConfiguration> ecmoConfigurations = getAllEcmoConfigurationsByProcedureId(db, procedureId);
        for(EcmoConfiguration ecmoConfiguration : ecmoConfigurations) {
            ecmoConfiguration.setCannulationStructures(getUsedStructuresByConfigurationIdAndDate(db, ecmoConfiguration.getId(), ecmoConfiguration.getDate()));
            ecmoConfiguration.setVeinCannulaList(getVeinCannulaByConfigurationIdAndDate(db, ecmoConfiguration.getId(), ecmoConfiguration.getDate()));
        }

        mDbHelper.closeDb(db);

        return ecmoConfigurations;
    }

    private List<EcmoConfiguration> getAllEcmoConfigurationsByProcedureId(SQLiteDatabase db, Long procedureId){
        List<EcmoConfiguration> ecmoConfigurations = new ArrayList<>();

        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoConfiguration.TABLE_NAME)
                .withColumns(IcupadDbContract.EcmoConfiguration.TABLE_NAME_MODE,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_CANIULATION_TYPE,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_WENTOWANIE_LEFT_VERTICLE,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_ARTERY_CANNULA_TYPE,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_ARTERY_CANNULA_SIZE,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_OXYGENERATOR_TYPE,
                        IcupadDbContract.EcmoConfiguration.TABLE_NAME_DATE,
                        IcupadDbContract.EcmoConfiguration._ID)
                .withSelection(IcupadDbContract.EcmoConfiguration.TABLE_NAME_PROCEDURE_ID + " =?")
                .withSelectionArgs(procedureId)
                .withOrderBy(IcupadDbContract.EcmoConfiguration.TABLE_NAME_DATE)
                .build();

        Cursor cursor = query.select();
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            EcmoConfiguration ecmoConfiguration = new EcmoConfiguration();
            if (!cursor.isNull(0))
                ecmoConfiguration.setMode(cursor.getString(0));
            if (!cursor.isNull(1))
                ecmoConfiguration.setCaniulationType(cursor.getString(1));
            if (!cursor.isNull(2))
                ecmoConfiguration.setWentowanieLeftVentricle(cursor.getString(2));
            if (!cursor.isNull(3))
                ecmoConfiguration.setArteryCannulaType(cursor.getString(3));
            if(!cursor.isNull(4)){
                ecmoConfiguration.setArteryCannulaSize(cursor.getDouble(4));
            }else{
                ecmoConfiguration.setArteryCannulaSize(null);
            }
            if (!cursor.isNull(5))
                ecmoConfiguration.setOxygeneratorType(cursor.getString(5));
            if (!cursor.isNull(6))
                ecmoConfiguration.setDate(cursor.getString(6));
            if (!cursor.isNull(7))
                ecmoConfiguration.setId(cursor.getLong(7));
            cursor.moveToNext();
            ecmoConfigurations.add(ecmoConfiguration);
        }
        cursor.close();
        return ecmoConfigurations;
    }



    private List<Long> getEcmoConfigurationsIdByProcedureId(SQLiteDatabase db, Long procedureId){
        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoConfiguration.TABLE_NAME)
                .withColumns(IcupadDbContract.EcmoConfiguration._ID)
                .withSelection(IcupadDbContract.EcmoConfiguration.TABLE_NAME_PROCEDURE_ID + " =?")
                .withSelectionArgs(procedureId)
                .withOrderBy(IcupadDbContract.EcmoConfiguration.TABLE_NAME_DATE)
                .build();

        Cursor cursor = query.select();
        cursor.moveToFirst();

        List<Long> ecmoConfigurationIdList= new ArrayList<>();
        while(!cursor.isAfterLast()) {
            ecmoConfigurationIdList.add(cursor.getLong(0));
            cursor.moveToNext();
        }
        cursor.close();
        return ecmoConfigurationIdList;
    }


    public void saveEcmoFullConfiguration(EcmoConfiguration ecmoConfiguration){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        Long configurationId = saveEcmoConfiguration(db, ecmoConfiguration);
        for(VeinCannula vc : ecmoConfiguration.getVeinCannulaList()){
            saveVeinCannula(db, vc, ecmoConfiguration.getDate(), configurationId);
        }
        for(String s : ecmoConfiguration.getCannulationStructures()){
            saveUsedStructures(db, s, ecmoConfiguration.getDate(), configurationId);
        }

        mDbHelper.closeDb(db);
    }

    private Long saveEcmoConfiguration(SQLiteDatabase db, EcmoConfiguration ecmoConfiguration){
        Long configurationId = 0L;
        ContentValues values = getValuesForCreateEcmoConfiguration(ecmoConfiguration);

        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoConfiguration.TABLE_NAME)
                .build();

        configurationId = query.insert(values);

        syncTableRepository.logUpdate(IcupadDbContract.EcmoConfiguration.TABLE_NAME, configurationId);

        return configurationId;
    }

    @NonNull
    private ContentValues getValuesForCreateEcmoConfiguration(EcmoConfiguration ecmoConfiguration){
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_PROCEDURE_ID, ecmoConfiguration.getProcedureId());
        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_USER, ecmoConfiguration.getCreatedBy());
        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_MODE, ecmoConfiguration.getMode());
        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_CANIULATION_TYPE, ecmoConfiguration.getCaniulationType());
        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_WENTOWANIE_LEFT_VERTICLE, ecmoConfiguration.getWentowanieLeftVentricle());
        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_ARTERY_CANNULA_TYPE, ecmoConfiguration.getArteryCannulaType());
        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_ARTERY_CANNULA_SIZE, ecmoConfiguration.getArteryCannulaSize());
        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_OXYGENERATOR_TYPE, ecmoConfiguration.getOxygeneratorType());
        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_ACTUAL, "1");
        values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_DATE, ecmoConfiguration.getDate());
        return values;
    }

    private void saveVeinCannula(SQLiteDatabase db, VeinCannula vc, String date, Long configurationId){
        ContentValues values = getValuesForCreateEcmoVeinCannula(vc, date, configurationId);

        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoVeinCannula.TABLE_NAME)
                .build();

        query.insert(values);
    }

    @NonNull
    private ContentValues getValuesForCreateEcmoVeinCannula(VeinCannula vc, String date, Long configurationId){
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.EcmoVeinCannula.TABLE_NAME_DATE, date);
        values.put(IcupadDbContract.EcmoVeinCannula.TABLE_NAME_CONFIGURATION_ID, configurationId);
        values.put(IcupadDbContract.EcmoVeinCannula.TABLE_NAME_TYPE, vc.getType());
        values.put(IcupadDbContract.EcmoVeinCannula.TABLE_NAME_SIZE, vc.getSize());
        return values;
    }

    private void saveUsedStructures(SQLiteDatabase db, String name, String date, Long configurationId){
        ContentValues values = getValuesForCreateEcmoUsedStructures(name, date, configurationId);

        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoUsedStructures.TABLE_NAME)
                .build();

        query.insert(values);
    }

    @NonNull
    private ContentValues getValuesForCreateEcmoUsedStructures(String name, String date, Long configurationId){
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.EcmoUsedStructures.TABLE_NAME_DATE, date);
        values.put(IcupadDbContract.EcmoUsedStructures.TABLE_NAME_CONFIGURATION_ID, configurationId);
        values.put(IcupadDbContract.EcmoUsedStructures.TABLE_NAME_NAME, name);
        return values;
    }



    public void setActualFalseInEcmoConfigurationsByProcedureId(Long procedureId){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        List<Long> ecmoConfigurationIdList = getEcmoConfigurationsIdByProcedureId(db, procedureId);

        for(Long id : ecmoConfigurationIdList){
            ContentValues values = new ContentValues();
            values.put(IcupadDbContract.EcmoConfiguration.TABLE_NAME_ACTUAL, "0");
            Query query = QueryBuilder.aQuery(db)       //TODO przetestować czy nie można teg wyciągnąć nad pętle
                    .withTable(IcupadDbContract.EcmoConfiguration.TABLE_NAME)
                    .withSelection(IcupadDbContract.EcmoConfiguration._ID + "=?")
                    .withSelectionArgs(id)
                    .build();

            query.update(values);

            syncTableRepository.logUpdate(IcupadDbContract.EcmoConfiguration.TABLE_NAME, id);
        }

        mDbHelper.closeDb(db);
    }

    public void endEcmoProcedure(EcmoProcedure ecmoProcedure){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_END_BY, ecmoProcedure.getEndById());
        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_END_DATE, ecmoProcedure.getEndDate());
        values.put(IcupadDbContract.EcmoProcedure.TABLE_NAME_END_CAUSE, ecmoProcedure.getEndCause());

        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoProcedure.TABLE_NAME)
                .withSelection(IcupadDbContract.EcmoProcedure._ID + "=?")
                .withSelectionArgs(ecmoProcedure.getId())
                .build();

        query.update(values);

        syncTableRepository.logUpdate(IcupadDbContract.EcmoProcedure.TABLE_NAME, ecmoProcedure.getId());
        mDbHelper.closeDb(db);
    }


    //**********ECMO ACT*********//
    public Long saveAct(EcmoAct ecmoAct){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        Long actId = 0L;
        ContentValues values = getValuesForCreateEcmoAct(ecmoAct);

        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoAct.TABLE_NAME)
                .build();

        actId = query.insert(values);

        syncTableRepository.logUpdate(IcupadDbContract.EcmoAct.TABLE_NAME, actId);
        mDbHelper.closeDb(db);
        return actId;
    }

    @NonNull
    private ContentValues getValuesForCreateEcmoAct(EcmoAct ecmoAct){
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.EcmoAct.TABLE_NAME_DATE, ecmoAct.getDate());
        values.put(IcupadDbContract.EcmoAct.TABLE_NAME_ACT, ecmoAct.getAct());
        values.put(IcupadDbContract.EcmoAct.TABLE_NAME_USER, ecmoAct.getCreatedBy());
        values.put(IcupadDbContract.EcmoAct.TABLE_NAME_PROCEDURE_ID, ecmoAct.getProcedureId());
        return values;
    }


    //**********ECMO PATIENT*********//
    public Long saveEcmoPatient(EcmoPatient ecmoPatient){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        Long newId = 0L;
        ContentValues values = getValuesForCreateEcmoPatient(ecmoPatient);

        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoPatient.TABLE_NAME)
                .build();

        newId = query.insert(values);

        syncTableRepository.logUpdate(IcupadDbContract.EcmoPatient.TABLE_NAME, newId);
        mDbHelper.closeDb(db);
        return newId;
    }

    @NonNull
    private ContentValues getValuesForCreateEcmoPatient(EcmoPatient ecmoPatient){
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_DATE, ecmoPatient.getDate());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_USER, ecmoPatient.getCreatedBy());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_PROCEDURE_ID, ecmoPatient.getProcedureId());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_ABP, ecmoPatient.getAbp());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_DIURESIS, ecmoPatient.getDiuresis());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_HR, ecmoPatient.getHr());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_SAT, ecmoPatient.getSat());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_TEMP_SURFACE, ecmoPatient.getTempSurface());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_TEMP_CORE, ecmoPatient.getTempCore());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_ULTRAFILTRACTION, ecmoPatient.getUltrafiltraction());
        values.put(IcupadDbContract.EcmoPatient.TABLE_NAME_VCP, ecmoPatient.getVcp());
        return values;
    }


    //**********ECMO PARAMETER*********//
    public Long saveEcmoParameter(EcmoParameter ecmoParameter) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        Long newId = 0L;
        ContentValues values = getValuesForCreateEcmoParameter(ecmoParameter);

        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoParameter.TABLE_NAME)
                .build();

        newId = query.insert(values);

        syncTableRepository.logUpdate(IcupadDbContract.EcmoParameter.TABLE_NAME, newId);
        mDbHelper.closeDb(db);
        return newId;
    }

    @NonNull
    private ContentValues getValuesForCreateEcmoParameter(EcmoParameter ecmoParameter){
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_DATE, ecmoParameter.getDate());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_USER, ecmoParameter.getCreatedBy());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_PROCEDURE_ID, ecmoParameter.getProcedureId());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_BLOOD_FLOW, ecmoParameter.getBloodFlow());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_RPM, ecmoParameter.getRpm());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_FIO2, ecmoParameter.getFiO2());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_DESCRIPTION, ecmoParameter.getDescription());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_GAS_FLOW, ecmoParameter.getGasFlow());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_HEPARIN_SUPPLY, ecmoParameter.getHeparinSuply());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_P1, ecmoParameter.getP1());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_P2, ecmoParameter.getP2());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_P3, ecmoParameter.getP3());
        values.put(IcupadDbContract.EcmoParameter.TABLE_NAME_DELTA, ecmoParameter.getDelta());
        return values;
    }


    public List<EcmoParameter> getEcmoParameterByProcedureId(Long procedureId){
        List<EcmoParameter> ecmoParameters= new ArrayList<>();
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoParameter.TABLE_NAME)
                .withColumns(IcupadDbContract.EcmoParameter.TABLE_NAME_RPM,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_BLOOD_FLOW,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_GAS_FLOW,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_DATE,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_FIO2,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_P1,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_P2,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_P3,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_DELTA,
                        IcupadDbContract.EcmoParameter.TABLE_NAME_HEPARIN_SUPPLY)
                .withSelection(IcupadDbContract.EcmoParameter.TABLE_NAME_PROCEDURE_ID + " =?")
                .withSelectionArgs(procedureId)
                .withOrderBy(IcupadDbContract.EcmoParameter.TABLE_NAME_DATE)
                .build();

        Cursor cursor = query.select();
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            EcmoParameter ecmoParameter = new EcmoParameter();
            if (!cursor.isNull(0))
                ecmoParameter.setRpm(cursor.getDouble(0));
            if (!cursor.isNull(1))
                ecmoParameter.setBloodFlow(cursor.getDouble(1));
            if (!cursor.isNull(2))
                ecmoParameter.setGasFlow(cursor.getDouble(2));
            if (!cursor.isNull(3))
                ecmoParameter.setDate(cursor.getString(3));
            if (!cursor.isNull(4))
                ecmoParameter.setFiO2(cursor.getDouble(4));
            if (!cursor.isNull(5))
                ecmoParameter.setP1(cursor.getDouble(5));
            if (!cursor.isNull(6))
                ecmoParameter.setP2(cursor.getDouble(6));
            if (!cursor.isNull(7))
                ecmoParameter.setP3(cursor.getDouble(7));
            if (!cursor.isNull(8))
                ecmoParameter.setDelta(cursor.getDouble(8));
            if (!cursor.isNull(9))
                ecmoParameter.setHeparinSuply(cursor.getDouble(9));

            ecmoParameters.add(ecmoParameter);
            cursor.moveToNext();
        }

        cursor.close();
        mDbHelper.closeDb(db);
        return ecmoParameters;
    }

    public List<EcmoPatient> getEcmoPatientByProcedureId(Long procedureId) {
        List<EcmoPatient> ecmoPatients= new ArrayList<>();
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoPatient.TABLE_NAME)
                .withColumns(IcupadDbContract.EcmoPatient.TABLE_NAME_SAT,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_ABP,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_VCP,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_DATE,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_HR,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_TEMP_SURFACE,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_TEMP_CORE,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_DIURESIS,
                        IcupadDbContract.EcmoPatient.TABLE_NAME_ULTRAFILTRACTION)
                .withSelection(IcupadDbContract.EcmoPatient.TABLE_NAME_PROCEDURE_ID + " =?")
                .withSelectionArgs(procedureId)
                .withOrderBy(IcupadDbContract.EcmoPatient.TABLE_NAME_DATE)
                .build();

        Cursor cursor = query.select();
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            EcmoPatient ecmoPatient = new EcmoPatient();
            if (!cursor.isNull(0))
                ecmoPatient.setSat(cursor.getDouble(0));
            if (!cursor.isNull(1))
                ecmoPatient.setAbp(cursor.getDouble(1));
            if (!cursor.isNull(2))
                ecmoPatient.setVcp(cursor.getDouble(2));
            if (!cursor.isNull(3))
                ecmoPatient.setDate(cursor.getString(3));
            if (!cursor.isNull(4))
                ecmoPatient.setHr(cursor.getDouble(4));
            if (!cursor.isNull(5))
                ecmoPatient.setTempSurface(cursor.getDouble(5));
            if (!cursor.isNull(6))
                ecmoPatient.setTempCore(cursor.getDouble(6));
            if (!cursor.isNull(7))
                ecmoPatient.setDiuresis(cursor.getDouble(7));
            if (!cursor.isNull(8))
                ecmoPatient.setUltrafiltraction(cursor.getDouble(8));

            ecmoPatients.add(ecmoPatient);
            cursor.moveToNext();
        }

        cursor.close();
        mDbHelper.closeDb(db);
        return ecmoPatients;
    }

    public List<EcmoAct> getEcmoActByProcedureId(Long procedureId) {
        List<EcmoAct> ecmoActs= new ArrayList<>();
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        Query query = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.EcmoAct.TABLE_NAME)
                .withColumns(IcupadDbContract.EcmoAct.TABLE_NAME_ACT,
                        IcupadDbContract.EcmoAct.TABLE_NAME_DATE)
                .withSelection(IcupadDbContract.EcmoAct.TABLE_NAME_PROCEDURE_ID + " =?")
                .withSelectionArgs(procedureId)
                .withOrderBy(IcupadDbContract.EcmoAct.TABLE_NAME_DATE)
                .build();

        Cursor cursor = query.select();
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            EcmoAct ecmoAct = new EcmoAct();
            if (!cursor.isNull(0))
                ecmoAct.setAct(cursor.getDouble(0));
            if (!cursor.isNull(1))
                ecmoAct.setDate(cursor.getString(1));

            ecmoActs.add(ecmoAct);
            cursor.moveToNext();
        }

        cursor.close();
        mDbHelper.closeDb(db);
        return ecmoActs;
    }
}
