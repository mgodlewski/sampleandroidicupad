package com.icupad.db.repository.modulerepository.service.defaulttest;

import android.content.Context;
import android.database.Cursor;

import com.icupad.commons.repository.model.BloodGasMeasurement;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;

import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 23.05.2017.
 */
public class DefaultTestRepositoryService {
    private IcupadDbHelper mDbHelper;
    private SyncTableRepository syncTableRepository;

    public DefaultTestRepositoryService(Context context) {
        mDbHelper = new IcupadDbHelper(context);
        syncTableRepository = new SyncTableRepository(context);
    }

    public List<BloodGasMeasurement> getDefaultMeasurementDOMs(long stayId) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        List<BloodGasMeasurement> result = new ArrayList<>();
        Cursor cursor = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.DefaultTestMeasurement.TABLE_NAME)
                .withColumns(IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_NAME,
                        IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_UNIT,
                        IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_VALUE,
                        IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_ABNORMALITY,
                        IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_RESULT_DATE,
                        IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_BOTTOM_DEFAULT_NORM,
                        IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_TOP_DEFAULT_NORM,
                        IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_STAY_ID,
                        IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_TEST_ID,
                        IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_BIRTH_DATE)
                .withSelection(IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_STAY_ID + "=?")
                .withSelectionArgs(stayId)
                .withOrderBy(IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_RESULT_DATE)
                .build().select();

        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            BloodGasMeasurement measurement = new BloodGasMeasurement();
            measurement.setName(cursor.getString(0));
            measurement.setUnit(cursor.getString(1));
            measurement.setValue(cursor.getDouble(2));
            measurement.setAbnormality(cursor.getString(3));
            measurement.setResultDate(DateTimeFormatterHelper.getMillisFromString(cursor.getString(4)));
            measurement.setBottomDefaultNorm(cursor.getDouble(5));
            measurement.setTopDefaultNorm(cursor.getDouble(6));
            measurement.setStayId(cursor.getLong(7));
            measurement.setTestId(cursor.getLong(8));
            measurement.setBirthDate(DateTimeFormatterHelper.getMillisFromString(cursor.getString(9)));
            result.add(measurement);
        }
        cursor.close();
        mDbHelper.closeDb(db);

        return result;
    }

    public void removeDefaultMeasurementForPatientId(Long patientId) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        String sqlDelete = " FROM " + IcupadDbContract.DefaultTestMeasurement.TABLE_NAME +
                " WHERE " + IcupadDbContract.DefaultTestMeasurement.COLUMN_NAME_STAY_ID + " IN (" +
                " SELECT " +  IcupadDbContract.Stay.COLUMN_NAME_ID + " FROM " + IcupadDbContract.Stay.TABLE_NAME +
                " WHERE " + IcupadDbContract.Stay.COLUMN_NAME_PATIENT_ID  + " = " + patientId + ")";

        db.execSQL("DELETE " + sqlDelete);

        mDbHelper.closeDb(db);
    }

}
