package com.icupad.db.repository.modulerepository.impl;

import android.content.Context;

import com.icupad.commons.repository.model.ConflictGeneral;
import com.icupad.db.repository.modulerepository.service.conflict.ConflictRepositoryService;

import java.util.List;

public class ConflictRepositoryImpl{
    private ConflictRepositoryService conflictRepositoryService;

    public ConflictRepositoryImpl(Context context) {
        conflictRepositoryService = new ConflictRepositoryService(context);
    }

    public List<ConflictGeneral> getConflicts() {
        return conflictRepositoryService.getConflicts();
    }
}
