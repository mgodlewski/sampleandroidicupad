package com.icupad.db.repository.modulerepository;

import android.content.Context;

import com.icupad.commons.repository.model.AuscultatePointTest;
import com.icupad.commons.repository.model.BloodGasMeasurement;
import com.icupad.commons.repository.model.EcmoAct;
import com.icupad.commons.repository.model.EcmoConfiguration;
import com.icupad.commons.repository.model.EcmoParameter;
import com.icupad.commons.repository.model.EcmoPatient;
import com.icupad.commons.repository.model.EcmoProcedure;
import com.icupad.commons.repository.model.Patient;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.repository.modulerepository.impl.AuscultateRepositoryImpl;
import com.icupad.db.repository.modulerepository.impl.BloodGasRepositoryImpl;
import com.icupad.db.repository.modulerepository.impl.CompleteBloodCountRepositoryImpl;
import com.icupad.db.repository.modulerepository.impl.ConflictRepositoryImpl;
import com.icupad.db.repository.modulerepository.impl.DefaultTestRepositoryImpl;
import com.icupad.db.repository.modulerepository.impl.EcmoRepositoryImpl;
import com.icupad.db.repository.modulerepository.impl.PatientRepositoryImpl;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.AuscultateSuiteTest;
import com.icupad.commons.repository.model.ConflictGeneral;
import com.icupad.commons.repository.model.ListAuscultateExamination;
import com.icupad.commons.repository.model.Operation;
import com.icupad.commons.repository.model.PatientDetail;
import com.icupad.commons.repository.model.SpinnerAuscultateSuiteSchema;
import com.icupad.commons.repository.model.Stay;
import com.icupad.module.patientchooser.PatientChooserFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class IcupadRepositoryImpl implements PublicIcupadRepository {
    private BloodGasRepositoryImpl bloodGasRepositoryImpl;
    private PatientRepositoryImpl patientRepositoryImpl;
    private AuscultateRepositoryImpl auscultateRepositoryImpl;
    private ConflictRepositoryImpl conflictRepositoryImpl;
    private EcmoRepositoryImpl ecmoRepositoryImpl;
    private CompleteBloodCountRepositoryImpl completeBloodCountRepository;
    private DefaultTestRepositoryImpl defaultTestRepository;

    public IcupadRepositoryImpl(Context context) {
        bloodGasRepositoryImpl = new BloodGasRepositoryImpl(context);
        patientRepositoryImpl = new PatientRepositoryImpl(context);
        auscultateRepositoryImpl = new AuscultateRepositoryImpl(context);
        conflictRepositoryImpl = new ConflictRepositoryImpl(context);
        ecmoRepositoryImpl = new EcmoRepositoryImpl(context);
        completeBloodCountRepository = new CompleteBloodCountRepositoryImpl(context);
        defaultTestRepository = new DefaultTestRepositoryImpl(context);
    }

    public List<BloodGasMeasurement> getCompleteBloodCountMeasurement(long stayId) {
        return completeBloodCountRepository.getCompleteBloodCountMeasurementDOMs(stayId);
    }

    public List<BloodGasMeasurement> getDefaultMeasurement(long stayId) {
        return defaultTestRepository.getDefaultMeasurementDOMs(stayId);
    }

    @Override
    public List<BloodGasMeasurement> getBloodGasTestMeasurement(long stayId) {
        return bloodGasRepositoryImpl.getBloodGasTestMeasurement(stayId);
    }

    public List<Patient> getAllPatients() {
        return patientRepositoryImpl.getAllPatients();
    }

    @Override
    public PatientDetail getPatientDetail(long id) {
        return patientRepositoryImpl.getPatientDetail(id);
    }

    @Override
    public List<Stay> getPatientStays(long id) {
        return patientRepositoryImpl.getPatientStays(id);
    }

    @Override
    public long getStayId() {
        long stayId = 0L;
        List<Stay> stays = getPatientStays(PatientChooserFragment.getLastChosenPatientId());
        for (Stay s : stays){
            if(s.getDischargeDate() == null){
                stayId = s.getId();
                break;
            }
        }
        return stayId;
    }

    @Override
    public List<Long> getAllStayByUserId() {
        List<Long> stayIds = new ArrayList<>();
        List<Stay> stays = getPatientStays(PatientChooserFragment.getLastChosenPatientId());
        for (Stay s : stays){
            stayIds.add(s.getId());
        }
        return stayIds;
    }

    @Override
    public Stay getCurrentStay(){
        Stay stay = null;
        List<Stay> stays = getPatientStays(PatientChooserFragment.getLastChosenPatientId());
        for (Stay s : stays){
            if(s.getDischargeDate() == null){
                stay = s;
                break;
            }
        }
        return stay;
    }

    @Override
    public Stay getLastStay(){
        List<Stay> stays = getPatientStays(PatientChooserFragment.getLastChosenPatientId());
        Stay tempStay = stays.get(0);
        for (Stay s : stays){
            if(s.getDischargeDate() == null){
                return s;
            }
            if(DateTimeFormatterHelper.compareDate(s.getAdmitDate(),tempStay.getAdmitDate())){
                tempStay = s;
            }
        }
        return tempStay;
    }

    @Override
    public void addOperation(Operation operation, long stayId) {
        patientRepositoryImpl.addOperation(operation, stayId);
    }

    @Override
    public void updateOperation(Operation operation) {
        patientRepositoryImpl.updateOperation(operation);
    }

    @Override
    public void removeOperation(long operationInternalId) {
        patientRepositoryImpl.archiveOperation(operationInternalId);
    }

    @Override
    public void updatePatient(long patientId, PatientDetail patientDetail) {
        patientRepositoryImpl.updatePatient(patientId, patientDetail);
    }

    @Override
    public List<ListAuscultateExamination> findAuscultateExaminationByPatientId(long patientId) {
        return auscultateRepositoryImpl.findAllSuitesByPatientId(patientId);
    }

    @Override
    public List<SpinnerAuscultateSuiteSchema> findSyncedAuscultateSuiteSchemasForSpinnerPreparingExamination() {
        return auscultateRepositoryImpl.findSyncedSuiteSchemas();
    }

    @Override
    public void saveAuscultateSuiteSchema(SpinnerAuscultateSuiteSchema spinnerAuscultateSuiteSchema) {
        auscultateRepositoryImpl.saveAuscultateSuiteSchema(spinnerAuscultateSuiteSchema);
    }

    @Override
    public AuscultateSuiteTest getAuscultateSuiteWithPointsSchema(long suiteId) {
        return auscultateRepositoryImpl.findAuscultateSuiteTest(suiteId);
    }

    @Override
    public void saveAuscultateSuiteTest(AuscultateSuiteTest auscultateSuiteTest) {
        auscultateRepositoryImpl.saveAuscultateSuiteTest(auscultateSuiteTest);
    }

    @Override
    public void updateAuscultateSuite(AuscultateSuiteTest auscultateSuiteTest) {
        auscultateRepositoryImpl.updateAuscultateSuite(auscultateSuiteTest);
    }

    @Override
    public void updateAuscultate(AuscultatePointTest auscultatePointTest) {
        auscultateRepositoryImpl.updateAuscultate(auscultatePointTest);
    }

    @Override
    public List<ConflictGeneral> getConflicts() {
        return conflictRepositoryImpl.getConflicts();
    }


    public Set<Long> getPresentPatientIds() {
        return patientRepositoryImpl.getPresentPatientIds();
    }

    public void removeOperationsForPatientId(Long patientId) {
        patientRepositoryImpl.removeOperationsForPatientId(patientId);
    }

    public void removeAuscultateSuitesForPatientId(Long patientId) {
        auscultateRepositoryImpl.removeAuscultateSuitesForPatientId(patientId);
    }

    public void removeAuscultatesForPatientId(Long patientId) {
        auscultateRepositoryImpl.removeAuscultatesForPatientId(patientId);
    }

    public void removeBloodGasMeasurementsForPatientId(Long patientId) {
        bloodGasRepositoryImpl.removeBloodGasMeasurementsForPatientId(patientId);
    }

    public void removeDefaultMeasurementForPatientId(Long patientId) {
        defaultTestRepository.removeDefaultMeasurementForPatientId(patientId);
    }

    public void removeCompleteBloodCountMeasurementsForPatientId(Long patientId) {
        completeBloodCountRepository.removeCompleteBloodCountMeasurementsForPatientId(patientId);
    }

    @Override
    public EcmoProcedure getEcmoProcedureById(Long procedureId){
        return ecmoRepositoryImpl.getEcmoProcedureById(procedureId);
    }

    @Override
    public EcmoConfiguration getEcmoFullConfigurationByProcedureId(Long procedureId){
        return ecmoRepositoryImpl.getEcmoFullConfigurationByProcedureId(procedureId);
    }

    @Override
    public List<EcmoConfiguration> getAllEcmoFullConfigurationsByPorcedureId(Long procedureId){
        return ecmoRepositoryImpl.getAllEcmoFullConfigurationsByPorcedureId(procedureId);
    }

    @Override
    public Long saveEcmoProcedure(EcmoProcedure ecmoProcedure){
        return ecmoRepositoryImpl.saveEcmoProcedure(ecmoProcedure);
    }

    @Override
    public void saveEcmoFullConfiguration(EcmoConfiguration ecmoConfiguration){
        ecmoRepositoryImpl.saveEcmoFullConfiguration(ecmoConfiguration);
    }

    @Override
    public void setActualFalseInEcmoConfigurationsByProcedureId(Long procedureId){
        ecmoRepositoryImpl.setActualFalseInEcmoConfigurationsByProcedureId(procedureId);
    }

    @Override
    public void endEcmoProcedure(EcmoProcedure ecmoProcedure){
        ecmoRepositoryImpl.endEcmoProcedure(ecmoProcedure);
    }

    @Override
    public Long saveAct(EcmoAct ecmoAct){
        return ecmoRepositoryImpl.saveAct(ecmoAct);
    }


    @Override
    public Long saveEcmoPatient(EcmoPatient ecmoPatient){
        return ecmoRepositoryImpl.saveEcmoPatient(ecmoPatient);
    }

    @Override
    public Long saveEcmoParameter(EcmoParameter ecmoParameter) {
        return ecmoRepositoryImpl.saveEcmoParameter(ecmoParameter);
    }

    @Override
    public List<EcmoParameter> getEcmoParameterByProcedureId(Long procedureId) {
        return ecmoRepositoryImpl.getEcmoParameterByProcedureId(procedureId);
    }

    @Override
    public List<EcmoPatient> getEcmoPatientByProcedureId(Long procedureId) {
        return ecmoRepositoryImpl.getEcmoPatientByProcedureId(procedureId);
    }

    @Override
    public List<EcmoAct> getEcmoActByProcedureId(Long procedureId) {
        return ecmoRepositoryImpl.getEcmoActByProcedureId(procedureId);
    }


}
