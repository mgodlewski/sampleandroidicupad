package com.icupad.db.repository.syncrepository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.synchronization.utils.SyncStatus;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.utils.PrivateDataAccessor;

public class SyncTableRepository {
    private IcupadDbHelper mDbHelper;

    public SyncTableRepository(Context context) {
        mDbHelper = new IcupadDbHelper(context);
    }

    public void logUpdate(String tableName, long infoInternalId) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = getValuesToUpdateOrInsert(tableName, infoInternalId);
        Query query = getQueryForSyncTableWithLoggingInfoInternalId(db, tableName, infoInternalId);
        Cursor cursor = query.select();

        if(cursor.moveToFirst()) {
            query.update(values);
        }
        else {
            query.insert(values);
        }

        cursor.close();
        mDbHelper.closeDb(db);
    }

    @NonNull
    private ContentValues getValuesToUpdateOrInsert(String tableName, long infoInternalId) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.SyncTable.COLUMN_NAME_MODIFY_DATE, DateTimeFormatterHelper.getCurrentDateTime());
        values.put(IcupadDbContract.SyncTable.COLUMN_NAME_TABLE_NAME, tableName);
        values.put(IcupadDbContract.SyncTable.COLUMN_NAME_INFO_INTERNAL_ID, infoInternalId);
        return values;
    }

    private Query getQueryForSyncTableWithLoggingInfoInternalId(SQLiteDatabase db, String tableName, long infoInternalId) {
        return QueryBuilder.aQuery(db)
                    .withTable(IcupadDbContract.SyncTable.TABLE_NAME)
                    .withColumns(IcupadDbContract.SyncTable.COLUMN_NAME_TABLE_NAME, IcupadDbContract.SyncTable.COLUMN_NAME_INFO_INTERNAL_ID)
                    .withSelection(IcupadDbContract.SyncTable.COLUMN_NAME_TABLE_NAME + "=? and " +
                            IcupadDbContract.SyncTable.COLUMN_NAME_INFO_INTERNAL_ID + "=?")
                    .withSelectionArgs(tableName, "" + infoInternalId)
                    .build();
    }

    public void updateSucceed(String tableName, long infoInternalId) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.COLUMN_NAME_SYNC, SyncStatus.SYNCED.getValue());

        getQueryForConflictToRemove(db, tableName, infoInternalId)
                .delete();
        getQueryForRowToUpdateSyncStatus(db, tableName, infoInternalId)
                .update(values);

        mDbHelper.closeDb(db);
    }

    public void updateSucceedSyncTable(String tableName, long infoInternalId) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.COLUMN_NAME_SYNC, SyncStatus.SYNCED.getValue());

        getQueryForConflictToRemove(db, tableName, infoInternalId)
                .delete();

        mDbHelper.closeDb(db);
    }

    private Query getQueryForConflictToRemove(SQLiteDatabase db, String tableName, long infoInternalId) {
        return QueryBuilder.aQuery(db)
                    .withTable(IcupadDbContract.SyncTable.TABLE_NAME)
                    .withSelection(IcupadDbContract.SyncTable.COLUMN_NAME_TABLE_NAME + "=? and " +
                            IcupadDbContract.SyncTable.COLUMN_NAME_INFO_INTERNAL_ID + "=?")
                    .withSelectionArgs(tableName, infoInternalId)
                    .build();
    }

    private Query getQueryForRowToUpdateSyncStatus(SQLiteDatabase db, String tableName, long infoInternalId) {
        return QueryBuilder.aQuery(db)
                .withTable(tableName)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                .withSelectionArgs(infoInternalId)
                .build();
    }
}
