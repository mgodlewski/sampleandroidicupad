package com.icupad.db.repository.modulerepository.impl;

import android.content.Context;

import com.icupad.commons.repository.model.BloodGasMeasurement;
import com.icupad.db.repository.modulerepository.service.bloodgas.BloodGasRepositoryService;

import java.util.List;

public class BloodGasRepositoryImpl {
    private BloodGasRepositoryService bloodGasRepositoryService;

    public BloodGasRepositoryImpl(Context context) {
        bloodGasRepositoryService = new BloodGasRepositoryService(context);
    }

    public void removeBloodGasMeasurementsForPatientId(Long patientId) {
        bloodGasRepositoryService.removeBloodGasMeasurementsForPatientId(patientId);
    }

    public List<BloodGasMeasurement> getBloodGasTestMeasurement(long stayId){
        return bloodGasRepositoryService.getBloodGasTestMeasurement(stayId);
    }
}
