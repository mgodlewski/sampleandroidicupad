package com.icupad.db.repository.modulerepository.service.bloodgas;

import android.content.Context;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.icupad.commons.repository.model.BloodGasMeasurement;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.commons.repository.builder.BloodGasMeasurementBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.utils.PrivateDataAccessor;
import com.icupad.webconnection.dto.bloodgas.BloodGasTestMeasurementDto;

import java.util.ArrayList;
import java.util.List;

public class BloodGasRepositoryService {
    protected IcupadDbHelper mDbHelper;
    protected SyncTableRepository syncTableRepository;

    public BloodGasRepositoryService(Context context) {
        mDbHelper = new IcupadDbHelper(context);
        syncTableRepository = new SyncTableRepository(context);
    }

    public List<BloodGasMeasurement> getBloodGasTestMeasurement(long stayId){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        List<BloodGasMeasurement> result = new ArrayList<>();
        Cursor cursor = QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.BloodGasTestMeasurement.TABLE_NAME)
                .withColumns(IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_NAME,
                        IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_UNIT,
                        IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_BLOOD_SOURCE,
                        IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_VALUE,
                        IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_ABNORMALITY,
                        IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_RESULT_DATE,
                        IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_BOTTOM_DEFAULT_NORM,
                        IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_TOP_DEFAULT_NORM,
                        IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_STAY_ID,
                        IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_TEST_ID,
                        IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_BIRTH_DATE)
                .withSelection(IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_STAY_ID + "=?")
                .withSelectionArgs(stayId)
                .withOrderBy(IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_RESULT_DATE)
                .build().select();

        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            BloodGasMeasurement measurement = new BloodGasMeasurement();
            measurement.setName(cursor.getString(0));
            measurement.setUnit(cursor.getString(1));
            measurement.setBloodSource(cursor.getString(2));
            measurement.setValue(cursor.getDouble(3));
            measurement.setAbnormality(cursor.getString(4));
            measurement.setResultDate(DateTimeFormatterHelper.getMillisFromString(cursor.getString(5)));
            measurement.setBottomDefaultNorm(cursor.getDouble(6));
            measurement.setTopDefaultNorm(cursor.getDouble(7));
            measurement.setStayId(cursor.getLong(8));
            measurement.setTestId(cursor.getLong(9));
            measurement.setBirthDate(DateTimeFormatterHelper.getMillisFromString(cursor.getString(10)));
            result.add(measurement);
        }
        cursor.close();
        mDbHelper.closeDb(db);

        return result;
    }


    public void removeBloodGasMeasurementsForPatientId(Long patientId) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        String sqlDelete = " FROM " + IcupadDbContract.BloodGasTestMeasurement.TABLE_NAME +
                " WHERE " + IcupadDbContract.BloodGasTestMeasurement.COLUMN_NAME_STAY_ID + " IN (" +
                " SELECT " +  IcupadDbContract.Stay.COLUMN_NAME_ID + " FROM " + IcupadDbContract.Stay.TABLE_NAME +
                " WHERE " + IcupadDbContract.Stay.COLUMN_NAME_PATIENT_ID  + " = " + patientId + ")";

        db.execSQL("DELETE " + sqlDelete);

        mDbHelper.closeDb(db);
    }
}
