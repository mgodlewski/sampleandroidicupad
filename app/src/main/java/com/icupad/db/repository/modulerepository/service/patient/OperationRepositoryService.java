package com.icupad.db.repository.modulerepository.service.patient;

import android.content.ContentValues;
import android.content.Context;
import net.sqlcipher.database.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.icupad.commons.repository.model.Operation;
import com.icupad.db.management.IcupadDbContract;
import com.icupad.db.management.IcupadDbHelper;
import com.icupad.db.query.Query;
import com.icupad.db.query.QueryBuilder;
import com.icupad.db.repository.syncrepository.SyncTableRepository;
import com.icupad.utils.PrivateDataAccessor;

public class OperationRepositoryService {
    protected IcupadDbHelper mDbHelper;
    protected SyncTableRepository syncTableRepository;

    public OperationRepositoryService(Context context) {
        mDbHelper = new IcupadDbHelper(context);
        syncTableRepository = new SyncTableRepository(context);
    }

    public void addOperation(Operation operation, long stayId) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = getValuesForAddingOperation(operation, stayId);

        Query query = QueryBuilder.aQuery(db).withTable(IcupadDbContract.Operation.TABLE_NAME).build();
        long internalId = query.insert(values);
        updateSyncTableWithChangedOperation(internalId);

        mDbHelper.closeDb(db);
    }

    @NonNull
    private ContentValues getValuesForAddingOperation(Operation operation, long stayId) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.Operation.COLUMN_NAME_OPERATING_DATE, operation.getOperatingDate());
        values.put(IcupadDbContract.Operation.COLUMN_NAME_DESCRIPTION, operation.getDescription());
        values.put(IcupadDbContract.Operation.COLUMN_NAME_ARCHIVED, operation.isArchived());
        values.put(IcupadDbContract.Operation.COLUMN_NAME_CREATED_DATE, "");
        values.put(IcupadDbContract.Operation.COLUMN_NAME_LAST_MODIFIED_DATE, "");
        values.put(IcupadDbContract.Operation.COLUMN_NAME_STAY_ID, stayId);
        return values;
    }

    @NonNull
    private void updateSyncTableWithChangedOperation(long internalId) {
        syncTableRepository.logUpdate(IcupadDbContract.Operation.TABLE_NAME, internalId);
    }




    public void updateOperation(Operation operation) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = getValuesForUpdatingOperation(operation);

        Query query = getQueryForOperationByInternalId(operation.getInternalId(), db);
        query.update(values);
        updateSyncTableWithChangedOperation(operation.getInternalId());

        mDbHelper.closeDb(db);
    }

    private Query getQueryForOperationByInternalId(long internalId, SQLiteDatabase db) {
        return QueryBuilder.aQuery(db)
                .withTable(IcupadDbContract.Operation.TABLE_NAME)
                .withSelection(IcupadDbContract.INTERNAL_ID + "=?")
                .withSelectionArgs(internalId)
                .build();
    }

    @NonNull
    private ContentValues getValuesForUpdatingOperation(Operation operation) {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.Operation.COLUMN_NAME_OPERATING_DATE, operation.getOperatingDate());
        values.put(IcupadDbContract.Operation.COLUMN_NAME_DESCRIPTION, operation.getDescription());
        values.put(IcupadDbContract.Operation.COLUMN_NAME_ARCHIVED, operation.isArchived());
        return values;
    }




    public void archiveOperation(long operationInternalId) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = getValuesForRemovingOperation();

        Query query = getQueryForOperationByInternalId(operationInternalId, db);
        query.update(values);
        updateSyncTableWithChangedOperation(operationInternalId);

        mDbHelper.closeDb(db);
    }

    public ContentValues getValuesForRemovingOperation() {
        ContentValues values = new ContentValues();
        values.put(IcupadDbContract.Operation.COLUMN_NAME_ARCHIVED, true);
        return values;
    }
}
