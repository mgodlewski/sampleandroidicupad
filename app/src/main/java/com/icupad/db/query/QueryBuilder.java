package com.icupad.db.query;

import net.sqlcipher.database.SQLiteDatabase;

import java.util.List;

public final class QueryBuilder {
    private SQLiteDatabase db;

    private String table;
    private String[] columns;
    private String selection;
    private String[] selectionArgs;
    private String groupBy;
    private String having;
    private String orderBy;

    private QueryBuilder(SQLiteDatabase db) {
        this.db = db;
    }

    public static QueryBuilder aQuery(SQLiteDatabase db) {
        return new QueryBuilder(db);
    }

    public QueryBuilder withTable(String table) {
        this.table = table;
        return this;
    }

    public QueryBuilder withColumns(String... columns) {
        this.columns = columns;
        return this;
    }

    public QueryBuilder withColumns(List<String> columns) {
        this.columns = columns.toArray(new String[columns.size()]);
        return this;
    }

    public QueryBuilder withSelection(String selection) {
        this.selection = selection;
        return this;
    }

    public QueryBuilder withSelectionArgs(Object... input) {
        String[] strings = new String[input.length];
        for(int i = 0; i < input.length; i++) {
            strings[i] = input[i].toString();
        }
        this.selectionArgs = strings;
        return this;
    }

    public QueryBuilder withGroupBy(String groupBy) {
        this.groupBy = groupBy;
        return this;
    }

    public QueryBuilder withHaving(String having) {
        this.having = having;
        return this;
    }

    public QueryBuilder withOrderBy(String orderBy) {
        this.orderBy = orderBy;
        return this;
    }

    public Query build() {
        Query query = new Query(db);
        query.setTable(table);
        query.setColumns(columns);
        query.setSelection(selection);
        query.setSelectionArgs(selectionArgs);
        query.setGroupBy(groupBy);
        query.setHaving(having);
        query.setOrderBy(orderBy);
        return query;
    }
}
