package com.icupad.stethoscope.function;

import com.icupad.commons.repository.builder.AuscultatePointTestBuilder;
import com.icupad.commons.repository.model.AuscultatePoint;
import com.icupad.commons.repository.model.AuscultatePointTest;

public class AuscultatePointToAuscultatePointTestFunction {
    public AuscultatePointTest apply(AuscultatePoint input) {
        return AuscultatePointTestBuilder.anAuscultatePointTest()
                .withDescription("").withWavFile("").withPollResult(0)
                .withAuscultatePoint(input).build();
    }
}
