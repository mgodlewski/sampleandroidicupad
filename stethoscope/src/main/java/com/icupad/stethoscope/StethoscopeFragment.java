package com.icupad.stethoscope;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.stethoscope.painter.StethoscopeConstants;
import com.icupad.stethoscope.populator.AuscultationDataPopulator;
import com.icupad.stethoscope.switchview.BackDescriptionHelper;
import com.icupad.stethoscope.switchview.FrontDescriptionHelper;
import com.icupad.stethoscope.switchview.PrepareSchemaHelper;
import com.icupad.stethoscope.switchview.PrepareExaminationHelper;
import com.icupad.stethoscope.switchview.ExaminationHelper;
import com.icupad.stethoscope.switchview.PreviewHelper;
import com.icupad.stethoscope.switchview.ResultsHelper;
import com.icupad.stethoscope.switchview.StethoscopeViewSwitchListener;
import com.icupad.commons.activitycommunication.Message;
import com.icupad.commons.ModuleFragment;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.AuscultatePoint;
import com.icupad.commons.repository.model.AuscultateSuiteTest;
import com.icupad.grable.GrableView;
import com.icupad.grable.listener.OnRowClickListener;
import com.icupad.grable.model.GrableMode;
import com.mmm.healthcare.scope.Stethoscope;

import java.util.List;

public class StethoscopeFragment extends ModuleFragment implements StethoscopeViewSwitchListener {

    private View view;

    private GrableView examinationTable;

    private ExaminationHelper examinationHelper;
    private ResultsHelper resultsHelper;
    private PrepareSchemaHelper prepareSchemaHelper;
    private FrontDescriptionHelper frontDescriptionHelper;
    private BackDescriptionHelper backDescriptionHelper;
    private PrepareExaminationHelper prepareExaminationHelper;
    private PreviewHelper previewHelper;

    private TextView informationTextView;

    private PublicIcupadRepository publicIcupadRepository;
    private AuscultationDataPopulator auscultationDataPopulator;

    private final Handler handler = new Handler();

    private AuscultateSuiteTest auscultateSuiteTest;

    private Long chosenPatientId;

    public StethoscopeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_stethoscope, container, false);
        informationTextView = (TextView) view.findViewById(R.id.informationTextView);
        examinationTable = (GrableView) view.findViewById(R.id.examinationsTable);

        chosenPatientId = fragmentListener.getChosenPaintId();
        publicIcupadRepository = fragmentListener.getRepository();
        auscultationDataPopulator = new AuscultationDataPopulator(
                getActivity().getApplicationContext().getResources(), publicIcupadRepository);


        examinationHelper = new ExaminationHelper(this, view, publicIcupadRepository);
        resultsHelper = new ResultsHelper(this, view, publicIcupadRepository);
        prepareSchemaHelper = new PrepareSchemaHelper(this, view, publicIcupadRepository);
        frontDescriptionHelper = new FrontDescriptionHelper(this, view, publicIcupadRepository);
        backDescriptionHelper = new BackDescriptionHelper(this, view, publicIcupadRepository);
        prepareExaminationHelper = new PrepareExaminationHelper(this, view, publicIcupadRepository);
        previewHelper = new PreviewHelper(this, view, publicIcupadRepository);

        checkChosenPatientAndGetDataOrShowMessage();

        return view;
    }

    private void checkChosenPatientAndGetDataOrShowMessage() {
        if (chosenPatientId != null) {
            informationTextView.setVisibility(View.GONE);
            switchToResults();
        } else {
            showMessageOnNoPatientDataErrorAndHideOtherViews(R.string.no_chosen_patient);
            fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
        }
    }

    private class InitializingDataThread implements Runnable {
        @Override
        public void run() {
            fragmentListener.onButtonPressed(Message.FRAGMENT_LOADING);
            final GrableView tempGrableView = auscultationDataPopulator.populate(
                    chosenPatientId, examinationTable);

            handler.post(new Runnable() {
                @Override
                public void run() {
                    examinationTable = tempGrableView;
                    if (examinationTable.hasNoData()) {
                        showMessageOnNoPatientDataErrorAndHideOtherViews(R.string.no_data_about_patient);
                    }
                    else {
                        setupTable(); //has to be invoken to initialize size
                    }
                    //TODO uncomment and remove after fixing no running loading symbol
//                    try {
//                        Thread.sleep(2000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                    fragmentListener.onButtonPressed(Message.FRAGMENT_LOADED);
                    resultsHelper.setup();
                }
            });
        }
    }

    private void setupTable() {
        if (isAdded()) {
            examinationTable.setVisibility(View.VISIBLE);
            examinationTable.setOnRowClickListener(new FrontDescriptionOnExaminationClickListenerImpl());
            examinationTable.setGrableMode(GrableMode.TABLE);
            examinationTable.invalidate();
        }
    }

    private class FrontDescriptionOnExaminationClickListenerImpl implements OnRowClickListener {
        @Override
        public void onRowClick(int position) {
            if (position >= 0) {
                auscultateSuiteTest =
                        publicIcupadRepository.getAuscultateSuiteWithPointsSchema(
                                auscultationDataPopulator.getExaminationForPosition(position).getSuiteInternalId());
                switchToFrontDescription(auscultateSuiteTest, null);
            }
        }
    }

    private void showMessageOnNoPatientDataErrorAndHideOtherViews(int errorMessageSourceString) {
        informationTextView.setVisibility(View.VISIBLE);
        informationTextView.setText(getResources().getString(errorMessageSourceString));
        view.findViewById(R.id.centerTopButton).setVisibility(View.INVISIBLE);
        view.findViewById(R.id.leftTopButton).setVisibility(View.INVISIBLE);
        view.findViewById(R.id.rightBottomButton).setVisibility(View.INVISIBLE);
        view.findViewById(R.id.spinner).setVisibility(View.INVISIBLE);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void switchToPreview() {
        previewHelper.setup();
    }

    @Override
    public void switchToResults() {
        new InitializingDataThread().run();
    }

    @Override
    public void switchToPrepareSchema(List<AuscultatePoint> auscultatePoints) {
        prepareSchemaHelper.setup(auscultatePoints);
    }

    @Override
    public void switchToFrontDescription(AuscultateSuiteTest auscultateSuiteTest, AuscultateSuiteTest editedAuscultateSuiteTest) {
        if(editedAuscultateSuiteTest == null) {
            editedAuscultateSuiteTest = new AuscultateSuiteTest(auscultateSuiteTest);
        }
        StethoscopeConstants.initiateBitmaps(getActivity().getApplicationContext());
        frontDescriptionHelper.setup(auscultateSuiteTest, editedAuscultateSuiteTest, getActivity());
    }

    @Override
    public void switchToBackDescription(AuscultateSuiteTest auscultateSuiteTest, AuscultateSuiteTest editedAuscultateSuiteTest) {
        backDescriptionHelper.setup(auscultateSuiteTest, editedAuscultateSuiteTest, getActivity());
    }

    @Override
    public void switchToPrepareTest() {
        prepareExaminationHelper.setup(getActivity());
    }

    @Override
    public void switchToExamination(Stethoscope stethoscope, AuscultateSuiteTest auscultateSuiteTest) {
        auscultateSuiteTest.setPatientId(fragmentListener.getChosenPaintId());
        auscultateSuiteTest.setExaminationDateTime(DateTimeFormatterHelper.getCurrentDateTime());
        examinationHelper.setup(stethoscope, auscultateSuiteTest);
    }
}