package com.icupad.stethoscope.populator;

import android.content.res.Resources;

import com.google.common.collect.Lists;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.ListAuscultateExamination;
import com.icupad.grable.GrableView;
import com.icupad.grable.model.Cell;
import com.icupad.stethoscope.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Strings.nullToEmpty;

public class AuscultationDataPopulator {
    private PublicIcupadRepository publicIcupadRepository;
    private Resources resources;

    private Map<Integer, ListAuscultateExamination> positionToListAuscultateExamination;

    public AuscultationDataPopulator(Resources resources, PublicIcupadRepository publicIcupadRepository) {
        this.publicIcupadRepository = publicIcupadRepository;
        this.resources = resources;
    }

    public GrableView populate(Long patientId, GrableView grableView) {
        grableView.clear();
        positionToListAuscultateExamination = new HashMap<>();

        List<ListAuscultateExamination> examinations = publicIcupadRepository.findAuscultateExaminationByPatientId(patientId);

        List<String[]> columns = Lists.newArrayList(
                new String[]{resources.getString(R.string.auscultation_examination_name_label)},
                new String[]{resources.getString(R.string.auscultation_examinator_label)},
                new String[]{resources.getString(R.string.auscultation_position_label)},
                new String[]{resources.getString(R.string.auscultation_temperature_label)},
                new String[]{resources.getString(R.string.auscultation_is_respirated_label)},
                new String[]{resources.getString(R.string.auscultation_passive_oxygen_label)},
                new String[]{resources.getString(R.string.auscultation_description_label)});
        Cell[][] data = new Cell[examinations.size()][7];
        List<String> rows = new ArrayList<>();

        for(int i = 0; i < examinations.size(); i++) {
            ListAuscultateExamination examination = examinations.get(i);
            rows.add(examination.getDateTime());
            data[i][0] = new Cell(nullToEmpty(examination.getExaminationName()));
            data[i][1] = new Cell(nullToEmpty(examination.getExecutor()));
            data[i][2] = new Cell(getPositionString(examination));
            data[i][3] = new Cell(getTemperatureString(examination));
            data[i][4] = new Cell(getStringForBoolean(examination.isRespirated()));
            data[i][5] = new Cell(getStringForBoolean(examination.isPassiveOxygenTherapy()));
            data[i][6] = new Cell(nullToEmpty(examination.getDescription()));

            positionToListAuscultateExamination.put(i, examination);
        }
        grableView.addColumns(columns);
        grableView.addRows(rows);
        grableView.addAllCells(data);

        return grableView;
    }

    private String getPositionString(ListAuscultateExamination examination) {
        int position = examination.getPosition();
        if(position == 0) {
            return "leżąca";
        }
        if(position == 1) {
            return "siedząca";
        }
        if(position == 2) {
            return "stojąca";
        }
        return "";
    }

    private String getTemperatureString(ListAuscultateExamination examination) {
        return "" + (examination.getTemperature() / 10) + "." + (examination.getTemperature() % 10) + "°C";
    }

    private String getStringForBoolean(boolean value) {
        if(value) {
            return "tak";
        }
        else {
            return "nie";
        }
    }

    public ListAuscultateExamination getExaminationForPosition(int position) {
        return positionToListAuscultateExamination.get(position);
    }
}
