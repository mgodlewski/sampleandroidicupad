package com.icupad.stethoscope.painter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.text.TextPaint;

import com.icupad.stethoscope.R;
import com.icupad.stethoscope.model.PointChangeState;
import com.icupad.stethoscope.model.PointChangeStateBuilder;

public class StethoscopeConstants {
    public static final Paint POLL_LABEL_TEXT_PAINT;
    public static final Paint POLL_ELEMENT_ENABLED_PAINT_ENABLED_PAINT;
    public static final int POLL_LABEL_HEIGHT = 28;
    public static final int POLL_LABEL_TEXT_BOTTOM = 20;
    public static final int FONT_SIZE_POLL_LABEL = POLL_LABEL_HEIGHT/2;
    public static final int POLL_LABEL_MARGIN = POLL_LABEL_HEIGHT/4;

    public static final int PLAY_BUTTON_SIDE = 40;
    public static Bitmap playGreenBitmap;
    public static Bitmap playRedBitmap;

    public static final Paint DROP_POINT_RECTANGLE_BORDER_PAINT;
    public static final Paint DROP_POINT_RECTANGLE_BACKGROUND_PAINT;
    public static final TextPaint SELECTED_DESCRIPTION_TEXT_PAINT;
    public static final TextPaint UNSELECTED_DESCRIPTION_TEXT_PAINT;
    public static final Paint SELECTED_WRONG_POINT_PAINT;
    public static final Paint SELECTED_CORRECT_POINT_PAINT;
    public static final Paint UNSELECTED_WRONG_POINT_PAINT;
    public static final Paint UNSELECTED_CORRECT_POINT_PAINT;
    public static final Paint SELECTED_POINT_NUMBER_ON_PANEL_PAINT;
    public static final Paint SELECTED_POINT_NUMBER_ON_PATIENT_PAINT;
    public static final Paint UNSELECTED_POINT_NUMBER_ON_PANEL_PAINT;
    public static final Paint UNSELECTED_POINT_NUMBER_ON_PATIENT_PAINT;
    public static final Paint EMPTY_DESCRIPTION_PAINT;
    public static final Paint BACKGROUND_PAINT;

    public static final int MAX_POINT_RADIUS_ON_DESCRIPTION = 30;
    public static final int SELECTED_POINT_RADIUS_ON_PATIENT = 25;
    public static final int SELECTED_POINT_RADIUS_ON_PANEL = 20;
    public static final int UNSELECTED_POINT_RADIUS_ON_PATIENT = 20;
    public static final int UNSELECTED_POINT_RADIUS_ON_PANEL = 14;

    private static final int SELECTED_FONT_COLOR = Color.BLACK;
    private static final int UNSELECTED_FONT_COLOR = Color.BLACK;
    private static final int FONT_SIZE_ON_DESCRIPTION = 16;

    private static final int SELECTED_WRONG_POINT_COLOR = Color.parseColor("#ED271C");
    private static final int SELECTED_CORRECT_POINT_COLOR = Color.parseColor("#74D968");
    private static final int UNSELECTED_WRONG_POINT_COLOR = Color.parseColor("#F17A73");
    private static final int UNSELECTED_CORRECT_POINT_COLOR = Color.parseColor("#A8E5A1");
    private static final int EMPTY_DESCRIPTION_COLOR = Color.BLACK;
    private static final int BACKGROUND_COLOR = Color.WHITE;
    private static final int POLL_ELEMENT_ENABLED_COLOR = Color.parseColor("#BBBBBB");

    private static final int SELECTED_POINT_NUMBER_SIZE_ON_PANEL = 10;
    private static final int SELECTED_POINT_NUMBER_SIZE_ON_PATIENT = 30;
    private static final int UNSELECTED_POINT_NUMBER_SIZE_ON_PANEL = 12;
    private static final int UNSELECTED_POINT_NUMBER_SIZE_ON_PATIENT = 30;

    public static final PointChangeState pointUndoneState;
    public static final PointChangeState pointDoneState;
    public static final PointChangeState pointUnderStartDoingState;
    public static final PointChangeState pointUnderStopDoingState;

    static {
        POLL_LABEL_TEXT_PAINT = new Paint();
        POLL_LABEL_TEXT_PAINT.setColor(Color.BLACK);
        POLL_LABEL_TEXT_PAINT.setTextSize(FONT_SIZE_POLL_LABEL);

        POLL_ELEMENT_ENABLED_PAINT_ENABLED_PAINT = new Paint();
        POLL_ELEMENT_ENABLED_PAINT_ENABLED_PAINT.setColor(POLL_ELEMENT_ENABLED_COLOR);

        DROP_POINT_RECTANGLE_BORDER_PAINT = new Paint();
        DROP_POINT_RECTANGLE_BORDER_PAINT.setColor(Color.WHITE);
        DROP_POINT_RECTANGLE_BORDER_PAINT.setStyle(Paint.Style.FILL_AND_STROKE);

        DROP_POINT_RECTANGLE_BACKGROUND_PAINT = new Paint();
        DROP_POINT_RECTANGLE_BACKGROUND_PAINT.setColor(Color.BLACK);
        DROP_POINT_RECTANGLE_BACKGROUND_PAINT.setStyle(Paint.Style.STROKE);
        DROP_POINT_RECTANGLE_BACKGROUND_PAINT.setStrokeWidth(10);
        DROP_POINT_RECTANGLE_BACKGROUND_PAINT.setPathEffect(new DashPathEffect(new float[]{5, 10, 15, 20}, 0));

        SELECTED_DESCRIPTION_TEXT_PAINT = new TextPaint();
        SELECTED_DESCRIPTION_TEXT_PAINT.setColor(SELECTED_FONT_COLOR);
        SELECTED_DESCRIPTION_TEXT_PAINT.setTextSize(FONT_SIZE_ON_DESCRIPTION);

        UNSELECTED_DESCRIPTION_TEXT_PAINT = new TextPaint();
        UNSELECTED_DESCRIPTION_TEXT_PAINT.setColor(UNSELECTED_FONT_COLOR);
        UNSELECTED_DESCRIPTION_TEXT_PAINT.setTextSize(FONT_SIZE_ON_DESCRIPTION);

        SELECTED_WRONG_POINT_PAINT = new Paint();
        SELECTED_WRONG_POINT_PAINT.setColor(SELECTED_WRONG_POINT_COLOR);

        SELECTED_CORRECT_POINT_PAINT = new Paint();
        SELECTED_CORRECT_POINT_PAINT.setColor(SELECTED_CORRECT_POINT_COLOR);

        UNSELECTED_WRONG_POINT_PAINT = new Paint();
        UNSELECTED_WRONG_POINT_PAINT.setColor(UNSELECTED_WRONG_POINT_COLOR);

        UNSELECTED_CORRECT_POINT_PAINT = new Paint();
        UNSELECTED_CORRECT_POINT_PAINT.setColor(UNSELECTED_CORRECT_POINT_COLOR);

        SELECTED_POINT_NUMBER_ON_PANEL_PAINT = new Paint();
        SELECTED_POINT_NUMBER_ON_PANEL_PAINT.setColor(SELECTED_FONT_COLOR);
        SELECTED_POINT_NUMBER_ON_PANEL_PAINT.setTextSize(SELECTED_POINT_NUMBER_SIZE_ON_PANEL);

        SELECTED_POINT_NUMBER_ON_PATIENT_PAINT = new Paint();
        SELECTED_POINT_NUMBER_ON_PATIENT_PAINT.setColor(SELECTED_FONT_COLOR);
        SELECTED_POINT_NUMBER_ON_PATIENT_PAINT.setTextSize(SELECTED_POINT_NUMBER_SIZE_ON_PATIENT);

        UNSELECTED_POINT_NUMBER_ON_PANEL_PAINT = new Paint();
        UNSELECTED_POINT_NUMBER_ON_PANEL_PAINT.setColor(UNSELECTED_FONT_COLOR);
        UNSELECTED_POINT_NUMBER_ON_PANEL_PAINT.setTextSize(UNSELECTED_POINT_NUMBER_SIZE_ON_PANEL);

        UNSELECTED_POINT_NUMBER_ON_PATIENT_PAINT = new Paint();
        UNSELECTED_POINT_NUMBER_ON_PATIENT_PAINT.setColor(SELECTED_FONT_COLOR);
        UNSELECTED_POINT_NUMBER_ON_PATIENT_PAINT.setTextSize(UNSELECTED_POINT_NUMBER_SIZE_ON_PATIENT);

        EMPTY_DESCRIPTION_PAINT = new Paint();
        EMPTY_DESCRIPTION_PAINT.setColor(EMPTY_DESCRIPTION_COLOR);

        BACKGROUND_PAINT = new Paint();
        BACKGROUND_PAINT.setColor(BACKGROUND_COLOR);

        pointUndoneState = PointChangeStateBuilder.aPointChangeState()
                .withNumberOnPatientPaint(StethoscopeConstants.UNSELECTED_POINT_NUMBER_ON_PATIENT_PAINT)
                .withCircleOnPatientPaint(StethoscopeConstants.UNSELECTED_WRONG_POINT_PAINT)
                .withCircleOnPatientRadius(StethoscopeConstants.UNSELECTED_POINT_RADIUS_ON_PATIENT)
                .build();
        pointDoneState = PointChangeStateBuilder.aPointChangeState()
                .withNumberOnPatientPaint(StethoscopeConstants.UNSELECTED_POINT_NUMBER_ON_PATIENT_PAINT)
                .withCircleOnPatientPaint(StethoscopeConstants.UNSELECTED_CORRECT_POINT_PAINT)
                .withCircleOnPatientRadius(StethoscopeConstants.UNSELECTED_POINT_RADIUS_ON_PATIENT)
                .build();
        pointUnderStartDoingState = PointChangeStateBuilder.aPointChangeState()
                .withNumberOnPatientPaint(StethoscopeConstants.SELECTED_POINT_NUMBER_ON_PATIENT_PAINT)
                .withCircleOnPatientPaint(StethoscopeConstants.SELECTED_WRONG_POINT_PAINT)
                .withCircleOnPatientRadius(StethoscopeConstants.SELECTED_POINT_RADIUS_ON_PATIENT)
                .build();
        pointUnderStopDoingState = PointChangeStateBuilder.aPointChangeState()
                .withNumberOnPatientPaint(StethoscopeConstants.SELECTED_POINT_NUMBER_ON_PATIENT_PAINT)
                .withCircleOnPatientPaint(StethoscopeConstants.SELECTED_CORRECT_POINT_PAINT)
                .withCircleOnPatientRadius(StethoscopeConstants.SELECTED_POINT_RADIUS_ON_PATIENT)
                .build();
    }

    public static void initiateBitmaps(Context context) {
         playGreenBitmap = Bitmap.createScaledBitmap(
                 BitmapFactory.decodeResource(context.getResources(), R.drawable.play_green),
                 PLAY_BUTTON_SIDE, PLAY_BUTTON_SIDE, false);
         playRedBitmap = Bitmap.createScaledBitmap(
                 BitmapFactory.decodeResource(context.getResources(), R.drawable.play_red),
                 PLAY_BUTTON_SIDE, PLAY_BUTTON_SIDE, false);
    }
}
