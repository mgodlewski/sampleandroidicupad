package com.icupad.stethoscope.painter.builder;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextPaint;

import com.icupad.stethoscope.painter.model.PointDescription;
import com.icupad.stethoscope.painter.model.PointToken;
import com.icupad.stethoscope.painter.utils.OnRedrawListener;

public final class PointDescriptionBuilder {
    private OnRedrawListener onRedrawListener;
    private int circleR;
    private Paint circlePaint;
    private Paint numberPaint;
    private TextPaint descriptionPaint;
    private StringBuilder name;
    private StringBuilder description;
    private PointToken pointToken;
    private String wavFile;
    private Context context;

    private PointDescriptionBuilder() {
    }

    public static PointDescriptionBuilder aPointDescription() {
        return new PointDescriptionBuilder();
    }

    public PointDescriptionBuilder withOnRedrawListener(OnRedrawListener onRedrawListener) {
        this.onRedrawListener = onRedrawListener;
        return this;
    }

    public PointDescriptionBuilder withCircleR(int circleR) {
        this.circleR = circleR;
        return this;
    }

    public PointDescriptionBuilder withCirclePaint(Paint circlePaint) {
        this.circlePaint = circlePaint;
        return this;
    }

    public PointDescriptionBuilder withNumberPaint(Paint numberPaint) {
        this.numberPaint = numberPaint;
        return this;
    }

    public PointDescriptionBuilder withDescriptionPaint(TextPaint descriptionPaint) {
        this.descriptionPaint = descriptionPaint;
        return this;
    }

    public PointDescriptionBuilder withName(StringBuilder name) {
        this.name = name;
        return this;
    }

    public PointDescriptionBuilder withDescription(StringBuilder description) {
        this.description = description;
        return this;
    }

    public PointDescriptionBuilder withPointToken(PointToken pointToken) {
        this.pointToken = pointToken;
        return this;
    }

    public PointDescriptionBuilder withWavFile(String wavFile) {
        this.wavFile = wavFile;
        return this;
    }

    public PointDescriptionBuilder withContext(Context context) {
        this.context = context;
        return this;
    }

    public PointDescription build() {
        PointDescription pointDescription = new PointDescription(context);
        pointDescription.setOnRedrawListener(onRedrawListener);
        pointDescription.setCircleR(circleR);
        pointDescription.setCirclePaint(circlePaint);
        pointDescription.setNumberPaint(numberPaint);
        pointDescription.setDescriptionPaint(descriptionPaint);
        pointDescription.setDescription(description);
        pointDescription.setName(name);
        pointDescription.setPointToken(pointToken);
        pointDescription.setWavFile(wavFile);
        return pointDescription;
    }
}
