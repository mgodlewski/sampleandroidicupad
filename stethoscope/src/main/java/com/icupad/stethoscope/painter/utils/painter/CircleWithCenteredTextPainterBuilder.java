package com.icupad.stethoscope.painter.utils.painter;

import android.graphics.Paint;

import com.icupad.stethoscope.painter.model.PointToken;

public final class CircleWithCenteredTextPainterBuilder {
    private int x;
    private int y;
    private int r;
    private String text;
    private Paint circlePaint;
    private Paint numberPaint;

    private CircleWithCenteredTextPainterBuilder() {
    }

    public static CircleWithCenteredTextPainterBuilder aCircleWithCenteredTextPainter() {
        return new CircleWithCenteredTextPainterBuilder();
    }

    public CircleWithCenteredTextPainterBuilder withX(int x) {
        this.x = x;
        return this;
    }

    public CircleWithCenteredTextPainterBuilder withY(int y) {
        this.y = y;
        return this;
    }

    public CircleWithCenteredTextPainterBuilder withR(int r) {
        this.r = r;
        return this;
    }

    public CircleWithCenteredTextPainterBuilder withText(String text) {
        this.text = text;
        return this;
    }

    public CircleWithCenteredTextPainterBuilder withCirclePaint(Paint circlePaint) {
        this.circlePaint = circlePaint;
        return this;
    }

    public CircleWithCenteredTextPainterBuilder withNumberPaint(Paint numberPaint) {
        this.numberPaint = numberPaint;
        return this;
    }

    public CircleWithCenteredTextPainter build() {
        CircleWithCenteredTextPainter circleWithCenteredTextPainter = new CircleWithCenteredTextPainter();
        circleWithCenteredTextPainter.setX(x);
        circleWithCenteredTextPainter.setY(y);
        circleWithCenteredTextPainter.setR(r);
        circleWithCenteredTextPainter.setText(text);
        circleWithCenteredTextPainter.setCirclePaint(circlePaint);
        circleWithCenteredTextPainter.setNumberPaint(numberPaint);
        return circleWithCenteredTextPainter;
    }

    public static CircleWithCenteredTextPainter buildFromPointToken(PointToken input) {
        CircleWithCenteredTextPainter circleWithCenteredTextPainter = new CircleWithCenteredTextPainter();
        circleWithCenteredTextPainter.setX(input.getX().intValue());
        circleWithCenteredTextPainter.setY(input.getY().intValue());
        circleWithCenteredTextPainter.setR(input.getR());
        circleWithCenteredTextPainter.setText("" + input.getNumber());
        circleWithCenteredTextPainter.setCirclePaint(input.getCirclePaint());
        circleWithCenteredTextPainter.setNumberPaint(input.getNumberPaint());
        return circleWithCenteredTextPainter;
    }
}
