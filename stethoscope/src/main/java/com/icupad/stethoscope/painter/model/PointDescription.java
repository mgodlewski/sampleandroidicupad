package com.icupad.stethoscope.painter.model;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.DynamicLayout;
import android.text.Layout;
import android.text.TextPaint;

import com.icupad.stethoscope.connection.StethoscopeConnector;
import com.icupad.stethoscope.model.AuscultationPoll;
import com.icupad.stethoscope.painter.utils.OnActionListener;
import com.icupad.stethoscope.painter.utils.OnRedrawListener;
import com.icupad.commons.utils.Point;
import com.icupad.stethoscope.painter.StethoscopeConstants;
import com.icupad.stethoscope.painter.utils.painter.CircleWithCenteredTextPainterBuilder;
import com.icupad.commons.utils.MathHelper;

public class PointDescription implements OnActionListener {
    private OnRedrawListener onRedrawListener;

    private int leftX;
    private int rightX;
    private int topY;
    private int bottomY;

    private int circleX;
    private int circleY;
    private int circleR;
    private int descriptionLeftX;
    private int descriptionWidth;

    private Paint circlePaint;
    private Paint numberPaint;
    private TextPaint descriptionPaint;

    private StringBuilder name;
    private DynamicLayout nameDynamicLayout;
    private StringBuilder description;
    private DynamicLayout descriptionDynamicLayout;
    private PointToken pointToken;
    private String wavFile;
    private StringBuilder clickedText;

    private int pointerX;
    private int pointerY;
    private int pointerDeltaX;
    private int pointerDeltaY;

    private boolean isDragged;
    private int deltaY;

    private AuscultationPoll auscultationPoll;

    public PointDescription(Context context) {
        this.isDragged = false;
        this.auscultationPoll = new AuscultationPoll(context);
    }

    public void resize() {
        circleX = leftX + StethoscopeConstants.MAX_POINT_RADIUS_ON_DESCRIPTION;
        circleY = topY + StethoscopeConstants.MAX_POINT_RADIUS_ON_DESCRIPTION;
        descriptionLeftX = circleX + StethoscopeConstants.MAX_POINT_RADIUS_ON_DESCRIPTION;
        descriptionWidth = rightX - descriptionLeftX - circleR * 2 - StethoscopeConstants.PLAY_BUTTON_SIDE * 2;

        nameDynamicLayout = new DynamicLayout(name + " ", descriptionPaint, descriptionWidth,
                Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        descriptionDynamicLayout = new DynamicLayout(description + " ", descriptionPaint, descriptionWidth,
                Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

        bottomY = Math.max(circleY + StethoscopeConstants.MAX_POINT_RADIUS_ON_DESCRIPTION,
                circleY + nameDynamicLayout.getHeight() + descriptionDynamicLayout.getHeight());

        auscultationPoll.resize(new Point(descriptionLeftX, bottomY + 20), descriptionWidth);
        bottomY += auscultationPoll.getHeight() + 20;
    }

    @Override
    public OnActionListener onActionDown(OnClickableState onClickableState, Point point) {
        if(onClickableState != OnClickableState.NONE) {
            pointerDeltaX = point.x.intValue() - leftX;
            pointerDeltaY = point.y.intValue() - topY;
            if (onClickableState == OnClickableState.EACH && isCircleClicked(point)) {
                return this;
            }
            if (onClickableState == OnClickableState.EACH && isNameClicked(point)) {
                clickedText = name;
                return this;
            }
            if (onClickableState == OnClickableState.DESCRIPTION && isPlayClicked(point)) {
                if(StethoscopeConnector.isConnected()) {
                    StethoscopeConnector.playThroughStethoscope(wavFile);
                }
            }
            if (onClickableState == OnClickableState.DESCRIPTION && isDescriptionClicked(point)) {
                clickedText = description;
                return this;
            }
            if(auscultationPoll.isClicked(point)) {
                onRedrawListener.onRedraw();
                return null;
            }
        }
        return null;
    }

    @Override
    public OnActionListener onActionUp(Point point) {
        isDragged = false;
        return null;
    }

    @Override
    public OnActionListener onActionDrag(Point point) {
        isDragged = true;
        pointerX = point.x.intValue();
        pointerY = point.y.intValue();
        return null;
    }

    @Override
    public boolean isClicked(Point point) {
        return false;
    }

    @Override
    public boolean isDragged() {
        return isDragged;
    }

    public void draw(Canvas canvas, int deltaY) {
        this.deltaY = deltaY;
        if(isDragged) {
            drawEmpty(canvas, deltaY);
            drawLoosePointDescription(canvas);
            onRedrawListener.onRedraw();
        }
        else {
            drawPointDescription(canvas, deltaY);
            auscultationPoll.draw(canvas, deltaY);
        }
    }

    private void drawLoosePointDescription(Canvas canvas) {
        drawEmptyRectangle(canvas);
        drawCircle(canvas, new Point(pointerX - pointerDeltaX, pointerY - pointerDeltaY));
        drawMultilineName(canvas, (pointerX - pointerDeltaX + descriptionLeftX - leftX), (pointerY - pointerDeltaY));
    }

    private void drawEmpty(Canvas canvas, int deltaY) {
        canvas.drawRect(leftX, topY + deltaY, rightX, bottomY + deltaY, StethoscopeConstants.EMPTY_DESCRIPTION_PAINT);
    }

    private void drawPointDescription(Canvas canvas, int deltaY) {
        drawCircle(canvas, new Point(circleX, circleY + deltaY));
        drawMultilineName(canvas, descriptionLeftX, circleY + deltaY);

        drawSoundCircle(canvas, new Point(descriptionLeftX, circleY + nameDynamicLayout.getHeight() + deltaY));
        drawMultilineDescription(canvas, descriptionLeftX + StethoscopeConstants.PLAY_BUTTON_SIDE * 2,
                circleY + nameDynamicLayout.getHeight() + deltaY);
    }

    private void drawEmptyRectangle(Canvas canvas) {
        canvas.drawRect(pointerX - pointerDeltaX,
                pointerY - pointerDeltaY,
                pointerX - pointerDeltaX + rightX - leftX,
                pointerY - pointerDeltaY + bottomY - topY,
                StethoscopeConstants.BACKGROUND_PAINT);
    }

    private void drawCircle(Canvas canvas, Point point) {
        CircleWithCenteredTextPainterBuilder.aCircleWithCenteredTextPainter()
                .withX(point.x.intValue()).withY(point.y.intValue()).withR(circleR)
                .withText("" + pointToken.getNumber())
                .withCirclePaint(pointToken.getCirclePaint())
                .withNumberPaint(pointToken.getNumberPaint())
                .build()
                .draw(canvas);
    }

    private void drawSoundCircle(Canvas canvas, Point point) {
        if(wavFile.isEmpty() || wavFile == null) {
            return;
        }

        if(StethoscopeConnector.isConnected()) {
            canvas.drawBitmap(StethoscopeConstants.playGreenBitmap,
                    point.x.intValue(), point.y.intValue(), StethoscopeConstants.BACKGROUND_PAINT);
        }
        else {
            canvas.drawBitmap(StethoscopeConstants.playRedBitmap,
                    point.x.intValue(), point.y.intValue(), StethoscopeConstants.BACKGROUND_PAINT);
        }
    }

    private void drawMultilineName(Canvas canvas, int dx, int dy) {
        canvas.save();
        canvas.translate(dx,dy);
        nameDynamicLayout.draw(canvas);
        canvas.restore();
    }

    private void drawMultilineDescription(Canvas canvas, int dx, int dy) {
        canvas.save();
        canvas.translate(dx,dy);
        canvas.drawRect(0, 0, descriptionWidth, descriptionDynamicLayout.getHeight(), StethoscopeConstants.POLL_ELEMENT_ENABLED_PAINT_ENABLED_PAINT);
        descriptionDynamicLayout.draw(canvas);
        canvas.restore();
    }

    private boolean isCircleClicked(Point point) {
        return MathHelper.isInRange(point, new Point(circleX, circleY + deltaY), circleR);
    }

    private boolean isNameClicked(Point point) {
        return ((point.x > descriptionLeftX) && (point.y > topY) && (point.y < circleY + nameDynamicLayout.getHeight()));
    }

    private boolean isPlayClicked(Point point) {
        boolean clicked = MathHelper.isInRange(point,
                new Point(descriptionLeftX + StethoscopeConstants.PLAY_BUTTON_SIDE/2,
                        circleY + nameDynamicLayout.getHeight() + StethoscopeConstants.PLAY_BUTTON_SIDE/2 + deltaY),
                StethoscopeConstants.PLAY_BUTTON_SIDE/2);
        return clicked;
    }

    private boolean isDescriptionClicked(Point point) {
        Rect rect = new Rect(descriptionLeftX + StethoscopeConstants.PLAY_BUTTON_SIDE * 2,
                circleY + nameDynamicLayout.getHeight() + deltaY,
                descriptionLeftX + descriptionWidth,
                circleY + nameDynamicLayout.getHeight() + descriptionDynamicLayout.getHeight() + deltaY);
        return MathHelper.isInRect(point, rect);
    }

    @Override
    public StringBuilder getClickedText() {
        return clickedText;
    }

    public OnRedrawListener getOnRedrawListener() {
        return onRedrawListener;
    }

    public void setOnRedrawListener(OnRedrawListener onRedrawListener) {
        this.onRedrawListener = onRedrawListener;
    }

    public int getLeftX() {
        return leftX;
    }

    public void setLeftX(int leftX) {
        this.leftX = leftX;
    }

    public int getRightX() {
        return rightX;
    }

    public void setRightX(int rightX) {
        this.rightX = rightX;
    }

    public int getTopY() {
        return topY;
    }

    public void setTopY(int topY) {
        this.topY = topY;
    }

    public int getRealTopY(int deltaY) {
        if(isDragged) {
            return pointerY - pointerDeltaY - deltaY;
        }
        else {
            return topY;
        }
    }

    public int getBottomY() {
        return bottomY;
    }

    public void setBottomY(int bottomY) {
        this.bottomY = bottomY;
    }

    public Paint getCirclePaint() {
        return circlePaint;
    }

    public void setCirclePaint(Paint circlePaint) {
        this.circlePaint = circlePaint;
    }

    public Paint getNumberPaint() {
        return numberPaint;
    }

    public void setNumberPaint(Paint numberPaint) {
        this.numberPaint = numberPaint;
    }

    public TextPaint getDescriptionPaint() {
        return descriptionPaint;
    }

    public void setDescriptionPaint(TextPaint descriptionPaint) {
        this.descriptionPaint = descriptionPaint;
    }

    public StringBuilder getName() {
        return name;
    }

    public void setName(StringBuilder name) {
        this.name = name;
    }

    public PointToken getPointToken() {
        return pointToken;
    }

    public void setPointToken(PointToken pointToken) {
        this.pointToken = pointToken;
    }

    public int getCircleR() {
        return circleR;
    }

    public void setCircleR(int circleR) {
        this.circleR = circleR;
    }

    public StringBuilder getDescription() {
        return description;
    }

    public void setDescription(StringBuilder description) {
        this.description = description;
    }

    public String getWavFile() {
        return wavFile;
    }

    public void setWavFile(String wavFile) {
        this.wavFile = wavFile;
    }

    public AuscultationPoll getAuscultationPoll() {
        return auscultationPoll;
    }

    public void setPollResult(int pollResults) {
        auscultationPoll.setPollResults(pollResults);
    }
}
