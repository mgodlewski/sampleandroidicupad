package com.icupad.stethoscope.painter.function;

import com.icupad.stethoscope.painter.model.PointDescription;
import com.icupad.stethoscope.painter.model.PointsDescriptionPanel;
import com.icupad.commons.repository.builder.AuscultatePointBuilder;
import com.icupad.commons.repository.builder.SpinnerAuscultateSuiteSchemaBuilder;
import com.icupad.commons.repository.model.AuscultatePoint;
import com.icupad.commons.repository.model.SpinnerAuscultateSuiteSchema;

import java.util.ArrayList;
import java.util.List;

public class PointsWithCoefficientToSuiteSchemaFunction {
    public SpinnerAuscultateSuiteSchema apply(PointsDescriptionPanel pointsDescriptionPanel,
                                              Double originalToUsedPatientPictureCoefficient,
                                              int frontRightX) {
        List<AuscultatePoint> points = new ArrayList<>();
        for(PointDescription pointDescription : pointsDescriptionPanel.getPointDescriptionsValues()) {
            points.add(AuscultatePointBuilder.anAuscultatePoint()
                    .withName(pointDescription.getName().toString())
                    .withY(getYOfPointDescription(pointDescription, originalToUsedPatientPictureCoefficient))
                    .withQueueOrder(pointDescription.getPointToken().getNumber())
                    .withIsFront(getIsFrontOfPointDescription(pointDescription, frontRightX))
                    .withX(getXOfPointDescription(pointDescription,
                            originalToUsedPatientPictureCoefficient,frontRightX))
                    .build()
            );
        }
        SpinnerAuscultateSuiteSchema output = SpinnerAuscultateSuiteSchemaBuilder.aSpinnerAuscultateSuiteSchema()
                .withPoints(points)
                .build();
        return output;
    }

    private Double getXOfPointDescription(PointDescription pointDescription, Double originalToUsedPatientPictureCoefficient, int frontRightX) {
        Double x = pointDescription.getPointToken().getX();
        if(x > frontRightX) {
            x -= frontRightX;
        }
        x /= originalToUsedPatientPictureCoefficient;
        return x;
    }

    private Double getYOfPointDescription(PointDescription pointDescription, Double originalToUsedPatientPictureCoefficient) {
        return pointDescription.getPointToken().getY() / originalToUsedPatientPictureCoefficient;
    }

    private boolean getIsFrontOfPointDescription(PointDescription pointDescription, int frontRightX) {
        return pointDescription.getPointToken().getX() < frontRightX;
    }

}
