package com.icupad.stethoscope.painter;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.icupad.stethoscope.function.AuscultatePointToAuscultatePointTestFunction;
import com.icupad.stethoscope.model.PointChangeState;
import com.icupad.stethoscope.painter.model.PointDescription;
import com.icupad.stethoscope.painter.model.PointToken;
import com.icupad.stethoscope.painter.model.PointsDescriptionPanel;
import com.icupad.stethoscope.painter.utils.OnActionListener;
import com.icupad.stethoscope.painter.model.OnClickableState;
import com.icupad.stethoscope.painter.utils.OnRedrawListener;
import com.icupad.commons.utils.Point;
import com.icupad.stethoscope.model.StethoscopeMode;
import com.icupad.stethoscope.painter.utils.TouchEventState;
import com.icupad.stethoscope.painter.utils.painter.CircleWithCenteredTextPainterBuilder;
import com.icupad.commons.repository.builder.AuscultatePointBuilder;
import com.icupad.commons.repository.model.AuscultatePoint;
import com.icupad.commons.repository.model.AuscultatePointTest;
import com.icupad.commons.repository.model.AuscultateSuiteTest;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Strings.nullToEmpty;

public class StethoscopeView extends View implements OnRedrawListener {
    private StethoscopeMode mode;
    private StethoscopeDimensions stethoscopeDimensions;
    private StethoscopeViewConverters stethoscopeViewConverters;

    private List<PointToken> pointTokens = new ArrayList<>();
    private PointsDescriptionPanel pointsDescriptionPanel;

    private TouchEventState touchEventState = new TouchEventState();
    private OnClickableState onClickableState;

    private AuscultateSuiteTest auscultateSuiteTest;
    private PointChangeState startPointState;
    private AuscultatePointToAuscultatePointTestFunction auscultatePointToAuscultatePointTestFunction
            = new AuscultatePointToAuscultatePointTestFunction();

    public StethoscopeView(Context context) {
        super(context);
        stethoscopeDimensions = new StethoscopeDimensions(context, this);
        stethoscopeViewConverters = new StethoscopeViewConverters(context, this);
    }

    public StethoscopeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        stethoscopeDimensions = new StethoscopeDimensions(context, this);
        stethoscopeViewConverters = new StethoscopeViewConverters(context, this);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        MeasureSpecs measureSpecs = stethoscopeDimensions.onMeasure(widthMeasureSpec, heightMeasureSpec);
        this.setMeasuredDimension(measureSpecs.widthMeasureSpec, measureSpecs.heightMeasureSpec);
    }

    public void setup(StethoscopeMode mode, List<AuscultatePoint> auscultatePoints) {
        PointChangeState state = StethoscopeConstants.pointDoneState;
        AuscultateSuiteTest auscultateSuiteTest =
                stethoscopeViewConverters.auscultatePointsToAuscultateSuiteTest(auscultatePoints);
        setup(mode, auscultateSuiteTest, state);
    }

    public void setup(StethoscopeMode mode, AuscultateSuiteTest auscultateSuiteTest, PointChangeState state) {
        this.auscultateSuiteTest = auscultateSuiteTest;
        this.mode = mode;
        this.startPointState = state;

        stethoscopeDimensions.measureWidths();
        pointTokens = new ArrayList<>();
        onClickableState = stethoscopeViewConverters.modeToOnClickableState(mode);

        pointsDescriptionPanel = stethoscopeDimensions.setupEmptyDescriptionPanel(auscultateSuiteTest.getDescription());
        for(AuscultatePointTest auscultatePointTest : auscultateSuiteTest.getAuscultatePointTests()) {
            Point point = new Point(stethoscopeDimensions.getXOnCanvasOfPoint(auscultatePointTest.getAuscultatePoint()),
                    stethoscopeDimensions.getYOnCanvasOfPoint(auscultatePointTest.getAuscultatePoint()));
            createAndAddPointTokenAndPointDescription(auscultatePointTest, point);
        }
    }

    private void createAndAddPointTokenAndPointDescription(AuscultatePointTest auscultatePointTest, Point point) {
        PointToken pointToken = stethoscopeViewConverters.auscultatePointAndPointToPointToken(auscultatePointTest.getAuscultatePoint(), point, startPointState);
        pointTokens.add(pointToken);
        PointDescription pointDescription = stethoscopeViewConverters.createPointDescriptionWithDescriptionAndPointToken(
                auscultatePointTest.getAuscultatePoint().getName(), pointToken, startPointState);
        pointDescription.setDescription(new StringBuilder(nullToEmpty(auscultatePointTest.getDescription())));
        pointDescription.setWavFile(nullToEmpty(auscultatePointTest.getWavFile()));
        pointDescription.setPollResult(auscultatePointTest.getPollResult());
        pointsDescriptionPanel.addPointDescription(pointDescription);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        drawBitmaps(canvas);
        drawDropPointRectangleIfNeeded(canvas);
        drawPointTokens(canvas);

        pointsDescriptionPanel.onDraw(canvas);
    }

    private void drawBitmaps(Canvas canvas) {
        stethoscopeDimensions.drawBitmaps(canvas);
    }

    private void drawDropPointRectangleIfNeeded(Canvas canvas) {
        if(touchEventState.isPointDragged()) {
            stethoscopeDimensions.drawDropPointRectangle(canvas);
        }
    }

    private void drawPointTokens(Canvas canvas) {
        for(PointToken point : pointTokens) {
            if(isValidToShow(point)) {
                CircleWithCenteredTextPainterBuilder.buildFromPointToken(point)
                        .draw(canvas);
            }
        }
    }

    private boolean isValidToShow(PointToken point) {
        return !( (point.isFront() && mode == StethoscopeMode.BACK_DESCRIPTION) ||
                (!point.isFront() && mode == StethoscopeMode.FRONT_DESCRIPTION));
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getAction()) {
            case(MotionEvent.ACTION_DOWN):
                reactOnTouchDownEvent(new Point(event));
                break;
            case(MotionEvent.ACTION_MOVE):
                reactOnTouchMoveEvent(new Point(event));
                break;
            case(MotionEvent.ACTION_UP):
                reactOnTouchUpEvent(new Point(event));
                break;
        }
        invalidate();
        return true;
    }

    private void reactOnTouchDownEvent(Point point) {
        touchEventState.checkIfNullAndSaveIfNot(pointsDescriptionPanel.onActionDown(onClickableState, point), point);
        for (PointToken pointToken : pointTokens) {
            touchEventState.checkIfNullAndSaveIfNot(pointToken.onActionDown(onClickableState, point), point);
        }
        touchEventState.setBackgroundClickedIfNothingClicked(onClickableState, stethoscopeDimensions.getPanelLeftX(), point);
    }

    private void reactOnTouchMoveEvent(Point point) {
        touchEventState.checkDraggingStateAndDragPointedObject(point);
        touchEventState.slidePointsDescriptionPanelIfNeeded(point, pointsDescriptionPanel);
    }

    private void reactOnTouchUpEvent(Point point) {
        removePointTokenIfNeeded(point);
        showPointDescriptionChangerIfNeeded();
        createNewPointIfNeeded();
        touchEventState.finalizeTouchUpEvent(point);
    }

    private void removePointTokenIfNeeded(Point point) {
        PointToken pointToken = touchEventState.getPointTokenIfFallenInRect(point, stethoscopeDimensions.getDropPointRect());
        if(pointToken != null) {
            pointsDescriptionPanel.removePointDescription(pointToken.getNumber());
            pointTokens.remove(pointToken);
        }
    }

    private void showPointDescriptionChangerIfNeeded() {
        OnActionListener onActionListener = touchEventState.getOnActionListenerToChangeText();
        if(onActionListener != null){
            Handler handler = new Handler();
            handler.post(new StethoscopeViewStringChangerPopup(this, onActionListener.getClickedText()));
        }
    }

    private void createNewPointIfNeeded() {
        if(touchEventState.isBackgroundLongClicked()) {
            Point point = new Point(new Double(touchEventState.getFirstX()), new Double(touchEventState.getFirstY()));
            AuscultatePoint auscultatePoint = AuscultatePointBuilder.anAuscultatePoint()
                    .withQueueOrder(pointTokens.size() + 1)
                    .withName("Nowy opis")
                    .build();
            createAndAddPointTokenAndPointDescription(auscultatePointToAuscultatePointTestFunction.apply(auscultatePoint), point);
        }
    }

    @Override
    public void onRedraw() {
        pointsDescriptionPanel.onRedraw();
        invalidate();
    }

    public PointsDescriptionPanel getPointsDescriptionPanel() {
        return pointsDescriptionPanel;
    }

    public Double getOriginalToUsedPatientPictureCoefficient() {
        return stethoscopeDimensions.getOriginalToUsedPatientPictureCoefficient();
    }

    public int getFrontRightX() {
        return stethoscopeDimensions.getFrontRightX();
    }

    public void setPaintsForPoint(int pointNo, PointChangeState state) {
        pointTokens.get(pointNo).setNumberPaint(state.getNumberOnPatientPaint());
        pointTokens.get(pointNo).setCirclePaint(state.getCircleOnPatientPaint());
        pointTokens.get(pointNo).setR(state.getCircleOnPatientRadius());
        invalidate();
    }

    public void resetup() {
        setup(mode, auscultateSuiteTest, startPointState);
    }

    public StethoscopeMode getMode() {
        return mode;
    }

    public void centerOnPointDescription(int pointDescriptionNumber) {
        pointsDescriptionPanel.centerOnPointDescription(pointDescriptionNumber);
    }
}