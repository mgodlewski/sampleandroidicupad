package com.icupad.stethoscope.painter.utils;

import android.graphics.Rect;

import com.icupad.stethoscope.painter.model.OnClickableState;
import com.icupad.stethoscope.painter.model.PointDescription;
import com.icupad.stethoscope.painter.model.PointToken;
import com.icupad.stethoscope.painter.model.PointsDescriptionPanel;
import com.icupad.commons.utils.MathHelper;
import com.icupad.commons.utils.Point;

public class TouchEventState {
    private static final int MAX_RANGE_WITHOUT_DRAG = 10;
    private static final int MILLIS_TO_DRAG = 400;

    private int firstX;
    private int firstY;
    private long clickedTime;
    private OnActionListener clickedObject;
    private boolean backgroundClicked;
    private boolean pointDragged;
    private boolean sthDragged;
    private boolean panelSlided;
    private int stableDeltaY = 0;

    public TouchEventState() {
        clearFields();
    }

    public void checkIfNullAndSaveIfNot(OnActionListener onActionListener, Point point) {
        clickedTime = System.currentTimeMillis();
        firstX = point.x.intValue();
        firstY = point.y.intValue();
        if(onActionListener != null) {
            clickedObject = onActionListener;
            panelSlided = false;
        }
    }

    public void setBackgroundClickedIfNothingClicked(OnClickableState onClickableState, int panelLeftX, Point point) {
        if(onClickableState == OnClickableState.EACH && clickedObject == null && panelLeftX > point.x) {
            clickedTime = System.currentTimeMillis();
            firstX = point.x.intValue();
            firstY = point.y.intValue();
            backgroundClicked = true;
        }
    }

    public void checkDraggingStateAndDragPointedObject(Point point) {
        if(clickedObject != null && panelSlided == false) {
            if (getPassedTime() > MILLIS_TO_DRAG) {
                if(clickedObject instanceof PointToken) {
                    pointDragged = true;
                }
                else {
                    sthDragged = true;
                }
                clickedObject.onActionDrag(point);
            } else {
                if (!MathHelper.isInRange(point, new Point(firstX, firstY), MAX_RANGE_WITHOUT_DRAG)) {
                    clickedObject = null;
                }
            }
        }
    }

    public void slidePointsDescriptionPanelIfNeeded(Point point, PointsDescriptionPanel pointsDescriptionPanel) {
        if(panelSlided || (!sthDragged && !pointDragged && point.x > pointsDescriptionPanel.getLeftX() && !MathHelper.isInRange(point, new Point(firstX, firstY), MAX_RANGE_WITHOUT_DRAG))){
            if (panelSlided == false) {
                panelSlided = true;
                stableDeltaY = pointsDescriptionPanel.getDeltaY();
            }
            int deltaY = stableDeltaY + (int) (point.y - firstY);

            pointsDescriptionPanel.setDeltaY(deltaY);
        }
    }

    public PointToken getPointTokenIfFallenInRect(Point point, Rect dropPointRect) {
        if((clickedObject != null && clickedObject instanceof PointToken && pointDragged == true)
                && MathHelper.isInRect(point, dropPointRect)) {
            return (PointToken) clickedObject;
        }
        return null;
    }

    public OnActionListener getOnActionListenerToChangeText() {
        if((clickedObject != null && (clickedObject instanceof PointDescription || clickedObject instanceof PointsDescriptionPanel)
                && getPassedTime() < MILLIS_TO_DRAG)) {
            return clickedObject;
        }
        return null;
    }

    public boolean isBackgroundLongClicked() {
        return (backgroundClicked && getPassedTime() > MILLIS_TO_DRAG);
    }

    private long getPassedTime() {
        return System.currentTimeMillis() - clickedTime;
    }

    public void finalizeTouchUpEvent(Point point) {
        if(clickedObject != null) {
            clickedObject.onActionUp(point);
        }
        clearFields();
        panelSlided = false;

    }

    private void clearFields() {
        clickedTime = 0;
        clickedObject = null;
        backgroundClicked = false;
        pointDragged = false;
        sthDragged = false;
    }

    public int getFirstX() {
        return firstX;
    }

    public void setFirstX(int firstX) {
        this.firstX = firstX;
    }

    public int getFirstY() {
        return firstY;
    }

    public void setFirstY(int firstY) {
        this.firstY = firstY;
    }

    public long getClickedTime() {
        return clickedTime;
    }

    public void setClickedTime(long clickedTime) {
        this.clickedTime = clickedTime;
    }

    public OnActionListener getClickedObject() {
        return clickedObject;
    }

    public void setClickedObject(OnActionListener clickedObject) {
        this.clickedObject = clickedObject;
    }

    public boolean isBackgroundClicked() {
        return backgroundClicked;
    }

    public void setBackgroundClicked(boolean backgroundClicked) {
        this.backgroundClicked = backgroundClicked;
    }

    public boolean isPointDragged() {
        return pointDragged;
    }

    public void setPointDragged(boolean pointDragged) {
        this.pointDragged = pointDragged;
    }
}
