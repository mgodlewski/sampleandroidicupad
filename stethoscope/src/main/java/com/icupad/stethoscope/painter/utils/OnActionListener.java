package com.icupad.stethoscope.painter.utils;

import com.icupad.commons.utils.Point;
import com.icupad.stethoscope.painter.model.OnClickableState;

public interface OnActionListener {
    public OnActionListener onActionDown(OnClickableState onClickableState, Point point);
    public OnActionListener onActionUp(Point point);
    public OnActionListener onActionDrag(Point point);
    public boolean isClicked(Point point);
    public boolean isDragged();

    StringBuilder getClickedText();
}
