package com.icupad.stethoscope.painter.model;

public enum OnClickableState {
    EACH, NONE, DESCRIPTION,
}
