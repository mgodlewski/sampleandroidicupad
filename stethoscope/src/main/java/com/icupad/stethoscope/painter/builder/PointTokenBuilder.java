package com.icupad.stethoscope.painter.builder;

import android.graphics.Paint;

import com.icupad.stethoscope.painter.model.PointToken;
import com.icupad.stethoscope.painter.utils.OnRedrawListener;

public final class PointTokenBuilder {
    private Double x;
    private Double y;
    private int r;
    private boolean isFront;
    private int number;
    private Paint circlePaint;
    private Paint numberPaint;
    private OnRedrawListener onRedrawListener;
    private String description;

    private PointTokenBuilder() {
    }

    public static PointTokenBuilder aPointToken() {
        return new PointTokenBuilder();
    }

    public PointTokenBuilder withX(int x) {
        this.x = new Double(x);
        return this;
    }

    public PointTokenBuilder withX(Double x) {
        this.x = x;
        return this;
    }

    public PointTokenBuilder withY(int y) {
        this.y = new Double(y);
        return this;
    }

    public PointTokenBuilder withY(Double y) {
        this.y = y;
        return this;
    }

    public PointTokenBuilder withR(int r) {
        this.r = r;
        return this;
    }

    public PointTokenBuilder withIsFront(boolean isFront) {
        this.isFront = isFront;
        return this;
    }

    public PointTokenBuilder withNumber(int number) {
        this.number = number;
        return this;
    }

    public PointTokenBuilder withCirclePaint(Paint circlePaint) {
        this.circlePaint = circlePaint;
        return this;
    }

    public PointTokenBuilder withNumberPaint(Paint numberPaint) {
        this.numberPaint = numberPaint;
        return this;
    }

    public PointTokenBuilder withOnRedrawListener(OnRedrawListener onRedrawListener) {
        this.onRedrawListener = onRedrawListener;
        return this;
    }

    public PointTokenBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public PointToken build() {
        PointToken pointToken = new PointToken();
        pointToken.setX(x);
        pointToken.setY(y);
        pointToken.setR(r);
        pointToken.setIsFront(isFront);
        pointToken.setNumber(number);
        pointToken.setCirclePaint(circlePaint);
        pointToken.setNumberPaint(numberPaint);
        pointToken.setOnRedrawListener(onRedrawListener);
        pointToken.setDescription(description);
        return pointToken;
    }
}
