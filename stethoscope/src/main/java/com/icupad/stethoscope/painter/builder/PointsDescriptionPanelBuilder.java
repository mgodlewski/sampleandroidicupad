package com.icupad.stethoscope.painter.builder;

import com.icupad.stethoscope.painter.utils.OnRedrawListener;
import com.icupad.stethoscope.painter.model.PointsDescriptionPanel;

public final class PointsDescriptionPanelBuilder {
    private OnRedrawListener onRedrawListener;
    private int leftX;
    private int rightX;
    private int topY;
    private int bottomY;
    private StringBuilder description;

    private PointsDescriptionPanelBuilder() {
    }

    public static PointsDescriptionPanelBuilder aPointsDescriptionPanel() {
        return new PointsDescriptionPanelBuilder();
    }

    public PointsDescriptionPanelBuilder withOnRedrawListener(OnRedrawListener onRedrawListener) {
        this.onRedrawListener = onRedrawListener;
        return this;
    }

    public PointsDescriptionPanelBuilder withLeftX(int leftX) {
        this.leftX = leftX;
        return this;
    }

    public PointsDescriptionPanelBuilder withRightX(int rightX) {
        this.rightX = rightX;
        return this;
    }

    public PointsDescriptionPanelBuilder withTopY(int topY) {
        this.topY = topY;
        return this;
    }

    public PointsDescriptionPanelBuilder withBottomY(int bottomY) {
        this.bottomY = bottomY;
        return this;
    }

    public PointsDescriptionPanelBuilder withDescription(StringBuilder description) {
        this.description = description;
        return this;
    }

    public PointsDescriptionPanel build() {
        PointsDescriptionPanel pointsDescriptionPanel = new PointsDescriptionPanel();
        pointsDescriptionPanel.setOnRedrawListener(onRedrawListener);
        pointsDescriptionPanel.setLeftX(leftX);
        pointsDescriptionPanel.setRightX(rightX);
        pointsDescriptionPanel.setTopY(topY);
        pointsDescriptionPanel.setBottomY(bottomY);
        pointsDescriptionPanel.setDescription(description);
        pointsDescriptionPanel.onRedraw();
        return pointsDescriptionPanel;
    }
}
