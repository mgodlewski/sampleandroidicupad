package com.icupad.stethoscope.painter;

public class MeasureSpecs {
    public int widthMeasureSpec;
    public int heightMeasureSpec;

    public MeasureSpecs(int widthMeasureSpec, int heightMeasureSpec) {
        this.widthMeasureSpec = widthMeasureSpec;
        this.heightMeasureSpec = heightMeasureSpec;
    }
}
