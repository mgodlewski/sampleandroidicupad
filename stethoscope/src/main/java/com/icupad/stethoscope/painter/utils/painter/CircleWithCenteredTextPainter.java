package com.icupad.stethoscope.painter.utils.painter;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class CircleWithCenteredTextPainter {
    private int x;
    private int y;
    private int r;
    private String text;
    private Paint circlePaint;
    private Paint numberPaint;


    public void draw(Canvas canvas) {
        canvas.drawCircle(x, y, r, circlePaint);
        Rect bounds = new Rect();
        numberPaint.getTextBounds(text, 0 , text.length(), bounds);
        canvas.drawText(text, x - bounds.width()/2, y + bounds.height()/2, numberPaint);
    }

    public CircleWithCenteredTextPainter() {
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Paint getCirclePaint() {
        return circlePaint;
    }

    public void setCirclePaint(Paint circlePaint) {
        this.circlePaint = circlePaint;
    }

    public Paint getNumberPaint() {
        return numberPaint;
    }

    public void setNumberPaint(Paint numberPaint) {
        this.numberPaint = numberPaint;
    }
}
