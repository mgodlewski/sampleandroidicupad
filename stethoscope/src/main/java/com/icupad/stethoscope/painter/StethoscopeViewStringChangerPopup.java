package com.icupad.stethoscope.painter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.widget.EditText;

import com.icupad.stethoscope.R;

class StethoscopeViewStringChangerPopup implements Runnable {
    private StringBuilder textToChange;
    private StethoscopeView stethoscopeView;
    private Context context;
    private Resources resources;

    private AlertDialog.Builder builder;
    private EditText text;

    public StethoscopeViewStringChangerPopup(StethoscopeView stethoscopeView, StringBuilder textToChange) {
        this.textToChange = textToChange;
        this.stethoscopeView = stethoscopeView;
        this.context = stethoscopeView.getContext();
        this.resources = context.getResources();

        builder = new AlertDialog.Builder(context);
        text = new EditText(context);
    }
    @Override
    public void run() {
        text.setText(textToChange);
        builder.setTitle(resources.getString(R.string.change_description_popup_title)).setView(text);
        builder.setPositiveButton(resources.getString(R.string.change_description_popup_button), new PositiveButtonClickListener());
        builder.setNegativeButton(resources.getString(R.string.cancel_description_change_popup_button), new NegativeButtonClickListener());
        builder.create().show();
    }

    private class PositiveButtonClickListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface di, int i) {
            textToChange.replace(0, textToChange.length(), text.getText().toString());
            stethoscopeView.invalidate();
            stethoscopeView.onRedraw();
        }
    }

    private class NegativeButtonClickListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface di, int i) {
        }
    }
}