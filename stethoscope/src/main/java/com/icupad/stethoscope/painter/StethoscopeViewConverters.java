package com.icupad.stethoscope.painter;

import android.content.Context;

import com.icupad.commons.repository.builder.AuscultateSuiteTestBuilder;
import com.icupad.commons.repository.model.AuscultatePoint;
import com.icupad.commons.repository.model.AuscultatePointTest;
import com.icupad.commons.repository.model.AuscultateSuiteTest;
import com.icupad.commons.utils.Point;
import com.icupad.stethoscope.function.AuscultatePointToAuscultatePointTestFunction;
import com.icupad.stethoscope.model.PointChangeState;
import com.icupad.stethoscope.model.StethoscopeMode;
import com.icupad.stethoscope.painter.builder.PointDescriptionBuilder;
import com.icupad.stethoscope.painter.builder.PointTokenBuilder;
import com.icupad.stethoscope.painter.model.OnClickableState;
import com.icupad.stethoscope.painter.model.PointDescription;
import com.icupad.stethoscope.painter.model.PointToken;

import java.util.ArrayList;
import java.util.List;

public class StethoscopeViewConverters {
    private AuscultatePointToAuscultatePointTestFunction auscultatePointToAuscultatePointTestFunction;
    private StethoscopeView stethoscopeView;
    private Context context;

    public StethoscopeViewConverters(Context context, StethoscopeView stethoscopeView) {
        this.context = context;
        this.stethoscopeView = stethoscopeView;
        auscultatePointToAuscultatePointTestFunction = new AuscultatePointToAuscultatePointTestFunction();
    }

    public OnClickableState modeToOnClickableState(StethoscopeMode mode) {
        switch(mode) {
            case PREVIEW:
            case EXAMINATION:
                return  OnClickableState.NONE;
            case PREPARE:
                return OnClickableState.EACH;
            case FRONT_DESCRIPTION:
            case BACK_DESCRIPTION:
                return OnClickableState.DESCRIPTION;
        }
        return null;
    }

    public AuscultateSuiteTest auscultatePointsToAuscultateSuiteTest(List<AuscultatePoint> auscultatePoints) {
        List<AuscultatePointTest> auscultatePointTests = new ArrayList<>();
        for(AuscultatePoint ap : auscultatePoints) {
            auscultatePointTests.add(auscultatePointToAuscultatePointTestFunction.apply(ap));
        }

        return  AuscultateSuiteTestBuilder.anAuscultateSuiteTest()
                .withAuscultatePointTests(auscultatePointTests)
                .build();
    }

    public PointToken auscultatePointAndPointToPointToken(AuscultatePoint auscultatePoint, Point point, PointChangeState startPointState) {
        return PointTokenBuilder.aPointToken()
                .withX(point.x)
                .withY(point.y)
                .withR(startPointState.getCircleOnPatientRadius())
                .withIsFront(auscultatePoint.isFront())
                .withNumber(auscultatePoint.getQueueOrder())
                .withCirclePaint(startPointState.getCircleOnPatientPaint())
                .withNumberPaint(startPointState.getNumberOnPatientPaint())
                .withOnRedrawListener(stethoscopeView)
                .build();
    }

    public PointDescription createPointDescriptionWithDescriptionAndPointToken(String pointName, PointToken pointToken, PointChangeState startPointState) {
        return PointDescriptionBuilder.aPointDescription()
                .withPointToken(pointToken)
                .withName(new StringBuilder(pointName))
                .withOnRedrawListener(stethoscopeView)
                .withCircleR(StethoscopeConstants.UNSELECTED_POINT_RADIUS_ON_PANEL)
                .withDescriptionPaint(StethoscopeConstants.UNSELECTED_DESCRIPTION_TEXT_PAINT)
                .withCirclePaint(startPointState.getCircleOnPatientPaint())
                .withNumberPaint(startPointState.getNumberOnPatientPaint())
                .withContext(context)
                .build();
    }
}
