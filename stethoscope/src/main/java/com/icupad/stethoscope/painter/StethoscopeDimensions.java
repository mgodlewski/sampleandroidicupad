package com.icupad.stethoscope.painter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.View;

import com.icupad.commons.repository.model.AuscultatePoint;
import com.icupad.stethoscope.R;
import com.icupad.stethoscope.model.StethoscopeMode;
import com.icupad.stethoscope.painter.builder.PointsDescriptionPanelBuilder;
import com.icupad.stethoscope.painter.model.PointsDescriptionPanel;

import static com.google.common.base.Strings.nullToEmpty;

public class StethoscopeDimensions {
    public static final int BOTTOM_MARGIN = 10;
    public static final int RIGHT_MARGIN = 10;

    private Context context;
    private StethoscopeView stethoscopeView;

    private Rect dropPointRect;
    private Bitmap frontBitmap;
    private Bitmap backBitmap;

    private int bottomY;
    private int frontLeftX;
    private int frontRightX;
    private int backLeftX;
    private int backRightX;
    private int panelLeftX;
    private int panelRightX;
    private int viewWidth;
    private int topPointDescriptionY;

    private int widthMeasureSpec;
    private int heightMeasureSpec;
    private Double originalToUsedPatientPictureCoefficient;

    private boolean isFrontDrawn;
    private boolean isBackDrawn;

    public StethoscopeDimensions(Context context, StethoscopeView stethoscopeView) {
        this.context = context;
        this.stethoscopeView = stethoscopeView;
    }

    public MeasureSpecs onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        frontBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.patient_front);
        backBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.patient_back);

        measureHeightsAndWidthsAndRescaleBitmaps(widthMeasureSpec, heightMeasureSpec);

        return new MeasureSpecs(widthMeasureSpec, heightMeasureSpec);
    }

    private void measureHeightsAndWidthsAndRescaleBitmaps(int widthMeasureSpec, int heightMeasureSpec) {
        bottomY = View.MeasureSpec.getSize(heightMeasureSpec) - BOTTOM_MARGIN;
        viewWidth = View.MeasureSpec.getSize(widthMeasureSpec) - RIGHT_MARGIN;
        originalToUsedPatientPictureCoefficient =  new Double((double)bottomY / frontBitmap.getHeight());

        rescaleBitmaps(bottomY);

        dropPointRect = new Rect(frontRightX - 100, bottomY - 100, frontRightX + 100, bottomY);

        if(stethoscopeView.getMode() != null) {
            stethoscopeView.resetup();
        }
    }

    private void rescaleBitmaps(int heightSize) {
        int newFrontWidth = (int)(frontBitmap.getWidth() * originalToUsedPatientPictureCoefficient);
        int newBackWidth = (int)(backBitmap.getWidth() * originalToUsedPatientPictureCoefficient);

        frontBitmap = Bitmap.createScaledBitmap(frontBitmap, newFrontWidth, heightSize, false);
        backBitmap = Bitmap.createScaledBitmap(backBitmap, newBackWidth, heightSize, false);
    }

    public void measureWidths() {
        isFrontDrawn = true;
        isBackDrawn = true;
        frontLeftX = 0;
        frontRightX = frontBitmap.getWidth();
        backLeftX = frontRightX;
        backRightX = backLeftX + backBitmap.getWidth();
        panelLeftX = backRightX;
        panelRightX = viewWidth;

        if(stethoscopeView.getMode() == StethoscopeMode.FRONT_DESCRIPTION) {
            isBackDrawn = false;
            panelLeftX = frontRightX;
        }
        if(stethoscopeView.getMode() == StethoscopeMode.BACK_DESCRIPTION) {
            isFrontDrawn = false;
            backLeftX = 0;
            backRightX = backBitmap.getWidth();
            panelLeftX = backRightX;
        }
    }

    public PointsDescriptionPanel setupEmptyDescriptionPanel(String description) {
        return PointsDescriptionPanelBuilder.aPointsDescriptionPanel()
                .withOnRedrawListener(stethoscopeView)
                .withLeftX(panelLeftX)
                .withRightX(panelRightX)
                .withTopY(0)
                .withBottomY(bottomY)
                .withDescription(new StringBuilder(nullToEmpty(description)))
                .build();
    }

    public Double getXOnCanvasOfPoint(AuscultatePoint point) {
        if(point.isFront()) {
            return point.getX().intValue() * originalToUsedPatientPictureCoefficient;
        }
        else {
            return point.getX().intValue() * originalToUsedPatientPictureCoefficient + backLeftX;
        }
    }

    public Double getYOnCanvasOfPoint(AuscultatePoint point) {
        return point.getY() * originalToUsedPatientPictureCoefficient;
    }

    public void drawBitmaps(Canvas canvas) {
        if(isFrontDrawn) {
            canvas.drawBitmap(frontBitmap, frontLeftX, 0, StethoscopeConstants.BACKGROUND_PAINT);
        }
        if(isBackDrawn) {
            canvas.drawBitmap(backBitmap, backLeftX, 0, StethoscopeConstants.BACKGROUND_PAINT);
        }
    }

    public void drawDropPointRectangle(Canvas canvas) {
        canvas.drawRect(dropPointRect, StethoscopeConstants.DROP_POINT_RECTANGLE_BORDER_PAINT);
        canvas.drawRect(dropPointRect, StethoscopeConstants.DROP_POINT_RECTANGLE_BACKGROUND_PAINT);
        canvas.drawText("Upuść tutaj, aby usunąć", dropPointRect.centerX() - 50,
                dropPointRect.top + 20, StethoscopeConstants.UNSELECTED_DESCRIPTION_TEXT_PAINT);
    }

    public int getPanelLeftX() {
        return panelLeftX;
    }

    public Rect getDropPointRect() {
        return dropPointRect;
    }

    public Double getOriginalToUsedPatientPictureCoefficient() {
        return originalToUsedPatientPictureCoefficient;
    }

    public int getFrontRightX() {
        return frontRightX;
    }

    public int getBottomY() {
        return bottomY;
    }
}