package com.icupad.stethoscope.painter.utils;

public interface OnRedrawListener {
    void onRedraw();
}
