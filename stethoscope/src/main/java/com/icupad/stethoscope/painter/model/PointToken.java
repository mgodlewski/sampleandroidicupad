package com.icupad.stethoscope.painter.model;

import android.graphics.Paint;

import com.icupad.stethoscope.painter.utils.OnActionListener;
import com.icupad.stethoscope.painter.utils.OnRedrawListener;
import com.icupad.commons.utils.Point;
import com.icupad.commons.utils.MathHelper;

public class PointToken implements OnActionListener {
    private Double x;
    private Double y;
    private int r;
    private boolean isFront;
    private int number;
    private Paint circlePaint;
    private Paint numberPaint;
    private OnRedrawListener onRedrawListener;
    private String description;

    private Double pointerDeltaX;
    private Double pointerDeltaY;

    public OnActionListener onActionDown(OnClickableState onClickableState, Point point) {
        pointerDeltaX = point.x - x;
        pointerDeltaY = point.y - y;
        if(MathHelper.isInRange(point, new Point(x, y), r)) {
            return this;
        }
        return null;
    }

    public OnActionListener onActionUp(Point point) {
        return null;
    }

    public OnActionListener onActionDrag(Point point) {
        x = point.x - pointerDeltaX;
        y = point.y - pointerDeltaY;
        onRedrawListener.onRedraw();
        return null;
    }

    @Override
    public boolean isClicked(Point point) {
        return false;
    }

    @Override
    public boolean isDragged() {
        return false;
    }

    @Override
    public StringBuilder getClickedText() {
        return null;
    }

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }

    public boolean isFront() {
        return isFront;
    }

    public void setIsFront(boolean front) {
        isFront = front;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Paint getCirclePaint() {
        return circlePaint;
    }

    public void setCirclePaint(Paint circlePaint) {
        this.circlePaint = circlePaint;
    }

    public Paint getNumberPaint() {
        return numberPaint;
    }

    public void setNumberPaint(Paint numberPaint) {
        this.numberPaint = numberPaint;
    }

    public OnRedrawListener getOnRedrawListener() {
        return onRedrawListener;
    }

    public void setOnRedrawListener(OnRedrawListener onRedrawListener) {
        this.onRedrawListener = onRedrawListener;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
