package com.icupad.stethoscope.painter.model;

import android.graphics.Canvas;
import android.text.DynamicLayout;
import android.text.Layout;

import com.google.common.collect.Lists;
import com.icupad.stethoscope.painter.utils.OnActionListener;
import com.icupad.stethoscope.painter.utils.OnRedrawListener;
import com.icupad.commons.utils.Point;
import com.icupad.stethoscope.painter.StethoscopeConstants;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PointsDescriptionPanel implements OnRedrawListener, OnActionListener {
    private OnRedrawListener onRedrawListener;

    private int leftX;
    private int rightX;
    private int topY;
    private int bottomY;
    private int realBottomY = 0;

    private int descriptionWidth;
    private int deltaY;

    private StringBuilder description;
    private StringBuilder clickedText;
    private DynamicLayout descriptionDynamicLayout;

    Map<Integer, PointDescription> pointDescriptions = new HashMap<>();

    public void addPointDescription(PointDescription pointDescription) {
        pointDescription.setLeftX(leftX);
        pointDescription.setRightX(rightX);

        pointDescriptions.put(pointDescriptions.size(), pointDescription);
        resizeAllPointDescriptions();
    }

    public void removePointDescription(int pointTokenNo) {
        for(int i = pointTokenNo; i < pointDescriptions.size(); i++) {
            moveIthPointTokenEarlier(i);
        }
        pointDescriptions.remove(pointDescriptions.size()-1);
        onRedraw();
    }

    private void moveIthPointTokenEarlier(int i) {
        int lastPointNo = i;
        int lastId = i-1;
        int currentId = i;
        pointDescriptions.get(currentId).getPointToken().setNumber(lastPointNo);
        pointDescriptions.remove(lastId);
        pointDescriptions.put(lastId, pointDescriptions.get(currentId));
    }

    @Override
    public OnActionListener onActionDown(OnClickableState onClickableState, Point point) {
        if(onClickableState != OnClickableState.NONE && isDescriptionClicked(point)) {
            clickedText = description;
            return this;
        }
        for(PointDescription pointDescription : pointDescriptions.values()) {
            OnActionListener onActionListener = pointDescription.onActionDown(onClickableState, point);
            if(onActionListener != null) {
                return onActionListener;
            }
        }
        return null;
    }

    public StringBuilder getClickedText() {
        return clickedText;
    }

    private boolean isDescriptionClicked(Point point) {
        return ((point.x > leftX) && (point.y > topY) && (point.y < topY + descriptionDynamicLayout.getHeight()));
    }

    @Override
    public OnActionListener onActionDrag(Point point) {
        return null;
    }

    @Override
    public boolean isClicked(Point point) {
        return false;
    }

    @Override
    public boolean isDragged() {
        return false;
    }

    @Override
    public OnActionListener onActionUp(Point point) {
        return null;
    }

    public void onDraw(Canvas canvas) {
        drawMultilineName(canvas, leftX, topY);
        drawEachPointDescription(canvas);
        drawDraggedPointDescription(canvas);
    }

    private void drawMultilineName(Canvas canvas, int dx, int dy) {
        canvas.save();
        canvas.translate(dx,dy);
        canvas.drawRect(0, 0, 1000, descriptionDynamicLayout.getHeight(), StethoscopeConstants.POLL_ELEMENT_ENABLED_PAINT_ENABLED_PAINT);
        descriptionDynamicLayout.draw(canvas);
        canvas.restore();
    }

    private void drawEachPointDescription(Canvas canvas) {
        for(Integer i = 0; i < pointDescriptions.size(); i++) {
            PointDescription pointDescription = pointDescriptions.get(i);
            pointDescription.draw(canvas, deltaY);
        }
    }

    private void drawDraggedPointDescription(Canvas canvas) {
        for(Integer i = 0; i < pointDescriptions.size(); i++) {
            if(pointDescriptions.get(i).isDragged()) {
                pointDescriptions.get(i).draw(canvas, deltaY);
            }
        }
    }

    @Override
    public void onRedraw() {
        descriptionDynamicLayout = new DynamicLayout(description, StethoscopeConstants.SELECTED_DESCRIPTION_TEXT_PAINT,
                rightX - leftX, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

        rearrangeAllPointDescriptions();
        resizeAllPointDescriptions();
    }

    public void centerOnPointDescription(int pointDescriptionNumber) {
        if(pointDescriptionNumber < pointDescriptions.size()) {
            PointDescription pd = pointDescriptions.get(pointDescriptionNumber);
            int centerY = pd.getTopY() + (pd.getBottomY() - pd.getTopY())/2;
            int deltaY = bottomY/2 - centerY;
            setDeltaY(deltaY);
        }
    }

    private void rearrangeAllPointDescriptions() {
        for(Integer i = 1; i < pointDescriptions.size(); i++) {
            int lastId = i-1;
            int currentId = i;
            int lastNumber = i;
            int currentNumber = i + 1;
            PointDescription lastPointDescription = pointDescriptions.get(lastId);
            PointDescription currentPointDescription = pointDescriptions.get(currentId);

            if(lastPointDescription.getRealTopY(deltaY) > currentPointDescription.getRealTopY(deltaY)) {
                lastPointDescription.getPointToken().setNumber(currentNumber);
                currentPointDescription.getPointToken().setNumber(lastNumber);
                pointDescriptions.remove(lastId);
                pointDescriptions.remove(currentId);
                pointDescriptions.put(lastId, currentPointDescription);
                pointDescriptions.put(currentId, lastPointDescription);
            }
        }
    }

    private void resizeAllPointDescriptions() {
        int lastBottomY = descriptionDynamicLayout.getHeight();
        for(int i = 0; i < pointDescriptions.size(); i++) {
            PointDescription pointDescription = pointDescriptions.get(i);
            pointDescription.setTopY(lastBottomY);
            pointDescription.resize();
            lastBottomY = pointDescription.getBottomY();
        }
        if(pointDescriptions != null && pointDescriptions.size() > 0) {
            realBottomY = pointDescriptions.get(pointDescriptions.size() - 1).getBottomY();
        }
    }

    public OnRedrawListener getOnRedrawListener() {
        return onRedrawListener;
    }

    public void setOnRedrawListener(OnRedrawListener onRedrawListener) {
        this.onRedrawListener = onRedrawListener;
    }

    public int getLeftX() {
        return leftX;
    }

    public void setLeftX(int leftX) {
        this.leftX = leftX;
    }

    public int getRightX() {
        return rightX;
    }

    public void setRightX(int rightX) {
        this.rightX = rightX;
    }

    public int getTopY() {
        return topY;
    }

    public void setTopY(int topY) {
        this.topY = topY;
    }

    public int getBottomY() {
        return bottomY;
    }

    public void setBottomY(int bottomY) {
        this.bottomY = bottomY;
    }

    public List<PointDescription> getPointDescriptionsValues() {
        return Lists.newArrayList(pointDescriptions.values());
    }

    public StringBuilder getDescription() {
        return description;
    }

    public void setDescription(StringBuilder description) {
        this.description = description;
    }

    public int getDeltaY() {
        return deltaY;
    }

    public void setDeltaY(int deltaY) {
        int maxDeltaY = 0;
        int minDeltaY = Math.min(maxDeltaY, bottomY - realBottomY);
        deltaY = Math.max(deltaY, minDeltaY);
        deltaY = Math.min(deltaY, maxDeltaY);
        this.deltaY = deltaY;
    }
}
