package com.icupad.stethoscope.connection;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.view.WindowManager;

import com.google.common.io.Files;
import com.icupad.stethoscope.recording.FileHelper;
import com.icupad.stethoscope.recording.WavTransferConstants;
import com.mmm.healthcare.scope.ConfigurationFactory;
import com.mmm.healthcare.scope.IBluetoothManager;
import com.mmm.healthcare.scope.IStethoscopeListener;
import com.mmm.healthcare.scope.Stethoscope;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Vector;

public class StethoscopeConnector {
    private static Activity activity;
    private static Stethoscope stethoscope = null;

    public static Vector<Stethoscope> initiateAndGetDevices(Activity activity) {
        StethoscopeConnector.activity = activity;
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        try {
            ConfigurationFactory.setLicenseFile(activity.getAssets().open("license.xml"));
        } catch (IOException e) {
            return null;
        }
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(mBluetoothAdapter == null) {
            return new Vector<>();
        }
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }

        IBluetoothManager manager = ConfigurationFactory.getBluetoothManager();
        Vector<Stethoscope> stethoscopes = manager.getPairedDevices();
        return stethoscopes;
    }

    public static Stethoscope connectToStethoscopeInRange(Vector<Stethoscope> stethoscopes, IStethoscopeListener stethoscopeListener) {
        for(Stethoscope stethoscope : stethoscopes) {
            try {
                stethoscope.connect();
                stethoscope.addStethoscopeListener(stethoscopeListener);
                StethoscopeConnector.stethoscope = stethoscope;
                return stethoscope;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    public static void disconnect() {
        if (stethoscope != null) {
            stethoscope.disconnect();
        }
        stethoscope = null;
        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public static boolean isConnected() {
        return !(stethoscope == null);
    }

    public static Stethoscope getStethoscope() {
        return stethoscope;
    }

    public static void playThroughStethoscope(String wavFile) {
        FileHelper fileHelper = new FileHelper();
        File fileInput = fileHelper.getInputFile(wavFile);
        try {
            byte[] bytes = Files.toByteArray(fileInput);
            stethoscope.startAudioOutput();
            stethoscope.getAudioOutputStream()
                    .write(bytes, WavTransferConstants.HEADERS_SIZE,
                            bytes.length - WavTransferConstants.HEADERS_SIZE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
