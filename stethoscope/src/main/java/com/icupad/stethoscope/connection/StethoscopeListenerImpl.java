package com.icupad.stethoscope.connection;

import com.icupad.stethoscope.model.StethoscopeEvent;
import com.mmm.healthcare.scope.Errors;
import com.mmm.healthcare.scope.IStethoscopeListener;

import org.greenrobot.eventbus.EventBus;

public class StethoscopeListenerImpl implements IStethoscopeListener {
    @Override
    public void mButtonDown(boolean b) {

    }

    @Override
    public void mButtonUp() {
        EventBus.getDefault().post(new StethoscopeEvent("mButtonUp"));
    }

    @Override
    public void plusButtonDown(boolean b) {

    }

    @Override
    public void plusButtonUp() {

    }

    @Override
    public void minusButtonDown(boolean b) {

    }

    @Override
    public void minusButtonUp() {

    }

    @Override
    public void filterButtonDown(boolean b) {

    }

    @Override
    public void filterButtonUp() {

    }

    @Override
    public void onAndOffButtonDown(boolean b) {

    }

    @Override
    public void onAndOffButtonUp() {

    }

    @Override
    public void lowBatteryLevel() {

    }

    @Override
    public void disconnected() {

    }

    @Override
    public void error(Errors errors, String s) {

    }

    @Override
    public void endOfOutputStream() {

    }

    @Override
    public void endOfInputStream() {

    }

    @Override
    public void outOfRange(boolean b) {

    }

    @Override
    public void underrunOrOverrunError(boolean b) {

    }
}
