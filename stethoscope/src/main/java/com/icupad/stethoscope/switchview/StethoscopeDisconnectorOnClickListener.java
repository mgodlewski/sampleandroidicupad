package com.icupad.stethoscope.switchview;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.view.View;

import com.icupad.stethoscope.connection.StethoscopeConnector;

public class StethoscopeDisconnectorOnClickListener implements View.OnClickListener {
    private ViewSwitchHelper viewSwitchHelper;
    private Activity activity;

    public StethoscopeDisconnectorOnClickListener(
            ViewSwitchHelper viewSwitchHelper, Activity activity) {
        this.viewSwitchHelper = viewSwitchHelper;
        this.activity = activity;
    }
    @Override
    public void onClick(View view) {
        StethoscopeConnector.disconnect();

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.disable();
        }

        viewSwitchHelper.setViewsForDisconnectedStethoscope(activity);
    }
}
