package com.icupad.stethoscope.switchview;

import android.bluetooth.BluetoothAdapter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.view.View;

import com.icupad.commons.repository.model.AuscultatePointTest;
import com.icupad.commons.repository.model.AuscultateSuiteTest;
import com.icupad.commons.utils.DateTimeFormatterHelper;
import com.icupad.commons.utils.IntPoint;
import com.icupad.stethoscope.display.BmpFileStream;
import com.icupad.stethoscope.model.StethoscopeEvent;
import com.icupad.stethoscope.connection.StethoscopeConnector;
import com.icupad.stethoscope.painter.StethoscopeConstants;
import com.icupad.stethoscope.model.StethoscopeMode;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.stethoscope.recording.AudioWriterTask;
import com.icupad.stethoscope.recording.FileHelper;
import com.mmm.healthcare.scope.AudioInputSwitch;
import com.mmm.healthcare.scope.BitmapFactory;
import com.mmm.healthcare.scope.IBitmap;
import com.mmm.healthcare.scope.Stethoscope;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class ExaminationHelper extends ViewSwitchHelper {

    private int activePointNo ;
    private AuscultatePointTest activeAuscultatePointTest;
    private boolean recording;
    private AuscultateSuiteTest auscultateSuiteTest;
    private StethoscopeConnector stethoscopeConnector = new StethoscopeConnector();
    private Stethoscope stethoscope;

    private AudioWriterTask task;
    private FileHelper fileHelper;


    public ExaminationHelper(StethoscopeViewSwitchListener viewSwitchListener, View view, PublicIcupadRepository publicIcupadRepository) {
        super(viewSwitchListener, view, publicIcupadRepository);
    }

    public void setup(Stethoscope stethoscope, AuscultateSuiteTest auscultateSuiteTest) {
        this.auscultateSuiteTest = auscultateSuiteTest;
        this.stethoscope = stethoscope;
        stethoscopeErrorTextView.setText("Połączono z urządzeniem: " + StethoscopeConnector.getStethoscope().getName());
        stethoscopeView.setup(StethoscopeMode.EXAMINATION, auscultateSuiteTest, StethoscopeConstants.pointUndoneState);
        stethoscopeView.invalidate();
        stethoscopeView.requestLayout();

        setupVisibilitiesAndTexts();

        centerTopButton.setOnClickListener(new StethoscopeConnectorOnClickListener());

        EventBus.getDefault().register(this);

        recording = false;
        activePointNo = 0;
        activeAuscultatePointTest = auscultateSuiteTest.getAuscultatePointTests().get(activePointNo);
        drawOnStethoscopeDisplay();

        this.fileHelper = new FileHelper();
    }

    private void setupVisibilitiesAndTexts() {
        leftTopButton.setVisibility(View.INVISIBLE);
        centerTopButton.setVisibility(View.VISIBLE);
        rightBottomButton.setVisibility(View.INVISIBLE);
        spinner.setVisibility(View.INVISIBLE);
        stethoscopeErrorTextView.setVisibility(View.VISIBLE);

        examinationTable.setVisibility(View.GONE);
        stethoscopeExaminationPoll.setVisibility(View.GONE);
        stethoscopeView.setVisibility(View.VISIBLE);

        centerTopButton.setText("ZAKOŃCZ BADANIE");
    }

    private class StethoscopeConnectorOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            if(task != null) {
                task.cancel(true);
                task = null;
                stethoscope.stopAudioInput();
            }
            endExamination();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(StethoscopeEvent event) {
        if(recording) {
            stethoscopeView.setPaintsForPoint(activePointNo, StethoscopeConstants.pointDoneState);
            activePointNo++;
            stethoscopeView.centerOnPointDescription(activePointNo);

            task.cancel(true);
            task = null;
            stethoscope.stopAudioInput();

            if(activePointNo == auscultateSuiteTest.getAuscultatePointTests().size()) {
                endExamination();
                return;
            }

            activeAuscultatePointTest = auscultateSuiteTest.getAuscultatePointTests().get(activePointNo);
            stethoscopeView.setPaintsForPoint(activePointNo, StethoscopeConstants.pointUnderStartDoingState);
            recording = false;
            drawOnStethoscopeDisplay();
        }
        else {
            stethoscopeView.setPaintsForPoint(activePointNo, StethoscopeConstants.pointUnderStopDoingState);
            recording = true;
            drawOnStethoscopeDisplay();

            stethoscope.startAudioInput(AudioInputSwitch.BeforeFilter);
            task = new AudioWriterTask();

            String name = DateTimeFormatterHelper.getCurrentDateTime().replace(":","-") + "_" + "1" + "_" + activePointNo;
            activeAuscultatePointTest.setWavFile(name);
            task.execute(fileHelper.createOutputFileStream(name), stethoscope.getAudioInputStream(), true);
            //TODO don't save too short files?
        }
    }

    private void endExamination() {
        publicIcupadRepository.saveAuscultateSuiteTest(auscultateSuiteTest);
        StethoscopeConnector.disconnect();
        viewSwitchListener.switchToResults();

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.disable();
        }

        EventBus.getDefault().unregister(this);
    }

    private void drawOnStethoscopeDisplay() {
        try {
            Bitmap bmp = Bitmap.createBitmap(46, 16, Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(bmp);
            prepareCanvas(canvas);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

//                bmp.compress(Bitmap.CompressFormat.PNG, 100, baos);
            BmpFileStream bmpStream = new BmpFileStream(new DataOutputStream(baos));
            bmpStream.write(bmp);

            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            IBitmap bitmap = BitmapFactory.createBitmap(bais);
            stethoscope.setDisplay(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void prepareCanvas(Canvas canvas) {
        Paint paint = drawBackground(canvas);
        paint.setColor(Color.rgb(255, 255, 255));
        paint.setTextSize(7);

        drawStaticElements(canvas, paint);

        canvas.drawText("" + (activePointNo + 1), 5, 6, paint);
        canvas.drawText("" + auscultateSuiteTest.getAuscultatePointTests().size(), 5, 15, paint);

        drawPoints(canvas, paint);
    }

    @NonNull
    private Paint drawBackground(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(Color.rgb(0, 0, 0));
        canvas.drawRect(new Rect(0, 0, 46, 16), paint);
        return paint;
    }

    private void drawPoints(Canvas canvas, Paint paint) {
        Double minFX = getMinFX();
        Double maxFX = getMaxFX();
        Double minBX = getMinBX();
        Double maxBX = getMaxBX();
        Double minY = getMinY();
        Double maxY = getMaxY();

        int tableWidth = 9;
        int tableHeight = 14;
        boolean[][] frontTable = new boolean[tableWidth + 2][tableHeight + 2];
        boolean[][] backTable = new boolean[tableWidth + 2][tableHeight + 2];
        for(AuscultatePointTest apt : auscultateSuiteTest.getAuscultatePointTests()) {
            int y = (int)Math.round((apt.getAuscultatePoint().getY() - minY)/(maxY - minY) * (tableHeight - 1) + 1);

            if(apt.getAuscultatePoint().isFront()) {
                int x = getX(apt, minFX, maxFX, tableWidth);
                frontTable[x][y] = true;
            }
            else {
                int x = getX(apt, minBX, maxBX, tableWidth);
                backTable[x][y] = true;
            }
        }

        if(activeAuscultatePointTest.getAuscultatePoint().isFront()) {
            int x = getX(activeAuscultatePointTest, minFX, maxFX, tableWidth);
            int y = (int)Math.round((activeAuscultatePointTest.getAuscultatePoint().getY() - minY)/(maxY - minY) * (tableHeight - 1) + 1);

            frontTable[x-1][y-1] = !frontTable[x-1][y-1];
            frontTable[x][y-1] = !frontTable[x][y-1];
            frontTable[x+1][y-1] = !frontTable[x+1][y-1];

            frontTable[x-1][y] = !frontTable[x-1][y];
            frontTable[x+1][y] = !frontTable[x+1][y];

            frontTable[x-1][y+1] = !frontTable[x-1][y+1];
            frontTable[x][y+1] = !frontTable[x][y+1];
            frontTable[x+1][y+1] = !frontTable[x+1][y+1];
        }
        else {
            int x = getX(activeAuscultatePointTest, minBX, maxBX, tableWidth);
            int y = (int)Math.round((activeAuscultatePointTest.getAuscultatePoint().getY() - minY)/(maxY - minY) * (tableHeight - 1) + 1);

            backTable[x-1][y-1] = !backTable[x-1][y-1];
            backTable[x][y-1] = !backTable[x][y-1];
            backTable[x+1][y-1] = !backTable[x+1][y-1];

            backTable[x-1][y] = !backTable[x-1][y];
            backTable[x+1][y] = !backTable[x+1][y];

            backTable[x-1][y+1] = !backTable[x-1][y+1];
            backTable[x][y+1] = !backTable[x][y+1];
            backTable[x+1][y+1] = !backTable[x+1][y+1];
        }


        for(int i = 0; i < tableWidth + 2; i ++) {
            for(int j = 0; j < tableHeight + 2; j ++) {
                if(frontTable[i][j]) {
                    canvas.drawPoint(19 + i, j, paint);
                }
                if(backTable[i][j]) {
                    canvas.drawPoint(35 + i, j, paint);
                }
            }
        }
    }

    private int getX(AuscultatePointTest apt, Double minX, Double maxX, int tableWidth) {
        if(maxX - minX == 0) {
            return 1;
        }
        else {
            return (int) Math.round((apt.getAuscultatePoint().getX() - minX) / (maxX - minX) * (tableWidth - 1) + 1);
        }
    }

    private void drawStaticElements(Canvas canvas, Paint paint) {
        drawDottedLine(canvas, paint, new IntPoint(1, 8));
        drawFrontLetter(canvas, paint, new IntPoint(15, 5));
        drawBackLetter(canvas, paint, new IntPoint(30, 5));
        drawRecordingLetterIfNeeded(canvas, paint);
    }

    private void drawDottedLine(Canvas canvas, Paint paint, IntPoint point) {
        canvas.drawLine(1, 8, 2, 8, paint);
        canvas.drawLine(4, 8, 5, 8, paint);
        canvas.drawLine(7, 8, 8, 8, paint);
        canvas.drawLine(10, 8, 11, 8, paint);
    }

    private void drawFrontLetter(Canvas canvas, Paint paint, IntPoint point) {
        canvas.drawLine(point.x, point.y-4, point.x+3, point.y-4, paint);
        canvas.drawLine(point.x, point.y-2, point.x+2, point.y-2, paint);
        canvas.drawLine(point.x, point.y-4, point.x, point.y, paint);
    }

    private void drawBackLetter(Canvas canvas, Paint paint, IntPoint point) {
        canvas.drawLine(30, 1, 32, 1, paint);
        canvas.drawLine(30, 3, 33, 3, paint);
        canvas.drawLine(30, 5, 33, 5, paint);
        canvas.drawLine(30, 1, 30, 5, paint);
        canvas.drawLine(32, 1, 32, 3, paint);
        canvas.drawLine(33, 3, 33, 5, paint);
    }

    private void drawRecordingLetterIfNeeded(Canvas canvas, Paint paint) {
        if(recording) {
            canvas.drawLine(1, 1, 1, 5, paint);
            canvas.drawPoint(2, 1, paint);
            canvas.drawPoint(3, 2, paint);
            canvas.drawPoint(2, 3, paint);
            canvas.drawPoint(3, 4, paint);
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        EventBus.getDefault().unregister(this);
    }

    private Double getMinFX() {
        Double result = Double.MAX_VALUE;
        for(AuscultatePointTest apt : auscultateSuiteTest.getAuscultatePointTests()) {
            if(apt.getAuscultatePoint().isFront() && apt.getAuscultatePoint().getX() < result) {
                result = apt.getAuscultatePoint().getX();
            }
        }
        return result;
    }

    private Double getMaxFX() {
        Double result = (double)0;
        for(AuscultatePointTest apt : auscultateSuiteTest.getAuscultatePointTests()) {
            if(apt.getAuscultatePoint().isFront() && apt.getAuscultatePoint().getX() > result) {
                result = apt.getAuscultatePoint().getX();
            }
        }
        return result;
    }

    private Double getMinBX() {
        Double result = Double.MAX_VALUE;
        for(AuscultatePointTest apt : auscultateSuiteTest.getAuscultatePointTests()) {
            if( !apt.getAuscultatePoint().isFront() && apt.getAuscultatePoint().getX() < result) {
                result = apt.getAuscultatePoint().getX();
            }
        }
        return result;
    }

    private Double getMaxBX() {
        Double result = (double)0;
        for(AuscultatePointTest apt : auscultateSuiteTest.getAuscultatePointTests()) {
            if( !apt.getAuscultatePoint().isFront() && apt.getAuscultatePoint().getX() > result) {
                result = apt.getAuscultatePoint().getX();
            }
        }
        return result;
    }

    private Double getMinY() {
        Double result = Double.MAX_VALUE;
        for(AuscultatePointTest apt : auscultateSuiteTest.getAuscultatePointTests()) {
            if(apt.getAuscultatePoint().getY() < result) {
                result = apt.getAuscultatePoint().getY();
            }
        }
        return result;
    }

    private Double getMaxY() {
        Double result = (double)0;
        for(AuscultatePointTest apt : auscultateSuiteTest.getAuscultatePointTests()) {
            if(apt.getAuscultatePoint().getY() > result) {
                result = apt.getAuscultatePoint().getY();
            }
        }
        return result;
    }
}
