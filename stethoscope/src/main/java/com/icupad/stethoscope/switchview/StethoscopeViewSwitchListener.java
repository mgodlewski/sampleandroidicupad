package com.icupad.stethoscope.switchview;

import android.content.Context;

import com.icupad.commons.repository.model.AuscultatePoint;
import com.icupad.commons.repository.model.AuscultateSuiteTest;
import com.mmm.healthcare.scope.Stethoscope;

import java.util.List;

public interface StethoscopeViewSwitchListener {
    void switchToPreview();
    void switchToResults();
    void switchToPrepareSchema(List<AuscultatePoint> auscultatePoints);
    void switchToFrontDescription(AuscultateSuiteTest auscultateSuiteTest, AuscultateSuiteTest editedAuscultateSuiteTest);
    void switchToBackDescription(AuscultateSuiteTest auscultateSuiteTest, AuscultateSuiteTest editedAuscultateSuiteTest);
    void switchToPrepareTest();

    void switchToExamination(Stethoscope stethoscope, AuscultateSuiteTest auscultateSuiteTest);
}
