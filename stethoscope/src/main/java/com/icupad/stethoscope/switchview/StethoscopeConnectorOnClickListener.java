package com.icupad.stethoscope.switchview;


import android.app.Activity;
import android.view.View;

import com.icupad.commons.repository.model.AuscultateSuiteTest;
import com.icupad.stethoscope.connection.StethoscopeConnector;
import com.icupad.stethoscope.connection.StethoscopeListenerImpl;
import com.icupad.stethoscope.painter.StethoscopeView;
import com.mmm.healthcare.scope.Stethoscope;

import java.util.Vector;

public class StethoscopeConnectorOnClickListener implements View.OnClickListener {
    private ViewSwitchHelper viewSwitchHelper;
    private Activity activity;

    public StethoscopeConnectorOnClickListener(
            ViewSwitchHelper viewSwitchHelper, Activity activity) {
        this.viewSwitchHelper = viewSwitchHelper;
        this.activity = activity;
    }
    @Override
    public void onClick(View view) {
        Vector<Stethoscope> stethoscopes = StethoscopeConnector.initiateAndGetDevices(activity);
        Stethoscope stethoscope = StethoscopeConnector.connectToStethoscopeInRange(stethoscopes, new StethoscopeListenerImpl());

        if(stethoscope != null) {
            viewSwitchHelper.setViewsForConnectedStethoscope(activity);
        }
        else {
            viewSwitchHelper.stethoscopeErrorTextView.setText("Nie wykryto żadnego urządzenia");
        }
        viewSwitchHelper.stethoscopeErrorTextView.setVisibility(View.VISIBLE);
    }
}
