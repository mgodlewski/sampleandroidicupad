package com.icupad.stethoscope.switchview;

import android.app.Activity;
import android.view.View;

import com.icupad.commons.repository.model.AuscultatePointTest;
import com.icupad.stethoscope.connection.StethoscopeConnector;
import com.icupad.stethoscope.model.StethoscopeMode;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.AuscultateSuiteTest;
import com.icupad.stethoscope.painter.StethoscopeConstants;
import com.icupad.stethoscope.painter.StethoscopeView;
import com.icupad.stethoscope.painter.model.PointDescription;

public class FrontDescriptionHelper extends ViewSwitchHelper{
    private AuscultateSuiteTest auscultateSuiteTest;
    private AuscultateSuiteTest editedAuscultateSuiteTest;

    public FrontDescriptionHelper(StethoscopeViewSwitchListener viewSwitchListener, View view, PublicIcupadRepository publicIcupadRepository) {
        super(viewSwitchListener, view, publicIcupadRepository);
    }

    public void setup(AuscultateSuiteTest auscultateSuiteTest, AuscultateSuiteTest editedAuscultateSuiteTest, Activity activity) {
        this.auscultateSuiteTest = auscultateSuiteTest;
        this.editedAuscultateSuiteTest = editedAuscultateSuiteTest;
        setupVisibilitiesAndTextsForFrontDescriptionView();

        stethoscopeView.setup(StethoscopeMode.FRONT_DESCRIPTION, editedAuscultateSuiteTest, StethoscopeConstants.pointDoneState);
        stethoscopeView.invalidate();

        leftTopButton.setOnClickListener(new BackDescriptionOnClickListener());
        centerTopButton.setOnClickListener(new StethoscopeConnectorOnClickListener(this, activity));
        rightBottomButton.setOnClickListener(new SaveOnClickListener(auscultateSuiteTest, editedAuscultateSuiteTest));

        if(StethoscopeConnector.isConnected()) {
            setViewsForConnectedStethoscope(activity);
        }
        else {
            setViewsForDisconnectedStethoscope(activity);
        }
    }

    private void setupVisibilitiesAndTextsForFrontDescriptionView() {
        leftTopButton.setVisibility(View.VISIBLE);
        centerTopButton.setVisibility(View.VISIBLE);
        rightBottomButton.setVisibility(View.VISIBLE);
        spinner.setVisibility(View.INVISIBLE);
        stethoscopeErrorTextView.setVisibility(View.INVISIBLE);

        examinationTable.setVisibility(View.GONE);
        stethoscopeExaminationPoll.setVisibility(View.GONE);
        stethoscopeView.setVisibility(View.VISIBLE);

        leftTopButton.setText("TYŁ");
        centerTopButton.setText("POŁĄCZ ZE STETOSKOPEM");
        rightBottomButton.setText("ZAPISZ OPISY");
    }

    private class BackDescriptionOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            updateAuscultateSuiteTestWithStethoscopeView(editedAuscultateSuiteTest, stethoscopeView);
            viewSwitchListener.switchToBackDescription(auscultateSuiteTest, editedAuscultateSuiteTest);
        }
    }

    @Override
    public void setViewsForDisconnectedStethoscope(Activity activity) {
        stethoscopeErrorTextView.setText("");
        stethoscopeView.invalidate();
        centerTopButton.setOnClickListener(
                new StethoscopeConnectorOnClickListener(this, activity));
        centerTopButton.setText("Połącz ze stetoskopem");
    }

    @Override
    public void setViewsForConnectedStethoscope(Activity activity) {
        stethoscopeErrorTextView.setText("Połączono z urządzeniem: " + StethoscopeConnector.getStethoscope().getName());
        stethoscopeView.invalidate();
        centerTopButton.setOnClickListener(
                new StethoscopeDisconnectorOnClickListener(this, activity));
        centerTopButton.setText("Rozłącz");
    }

    private class SaveOnClickListener implements View.OnClickListener {
        AuscultateSuiteTest auscultateSuiteTest;
        AuscultateSuiteTest editedAuscultateSuiteTest;

        SaveOnClickListener(AuscultateSuiteTest auscultateSuiteTest, AuscultateSuiteTest editedAuscultateSuiteTest) {
            this.auscultateSuiteTest = auscultateSuiteTest;
            this.editedAuscultateSuiteTest = editedAuscultateSuiteTest;
        }

        @Override
        public void onClick(View view) {
            editedAuscultateSuiteTest.setDescription(stethoscopeView.getPointsDescriptionPanel().getDescription().toString());
            if( !auscultateSuiteTest.getDescription().equals(editedAuscultateSuiteTest.getDescription())) {
                editedAuscultateSuiteTest.setSuiteInternalId(auscultateSuiteTest.getSuiteInternalId());
                publicIcupadRepository.updateAuscultateSuite(editedAuscultateSuiteTest);
            }
            for(int i = 0; i < auscultateSuiteTest.getAuscultatePointTests().size(); i++) {
                updateAuscultateSuiteTestWithStethoscopeView(editedAuscultateSuiteTest, stethoscopeView);
            }
            viewSwitchListener.switchToResults();
        }
    }

    private void updateAuscultateSuiteTestWithStethoscopeView(AuscultateSuiteTest ast, StethoscopeView sv) {
        //TODO add setting description for suite and check if it works and copy it for the back view
//        ast.setDescription(sv.get)

        for(int i = 0; i < ast.getAuscultatePointTests().size(); i++) {
            AuscultatePointTest oldApt = ast.getAuscultatePointTests().get(i);
            PointDescription pointDescription = sv.getPointsDescriptionPanel().getPointDescriptionsValues().get(i);

            if( !(oldApt.getDescription().equals(pointDescription.getDescription().toString()) && oldApt.getPollResult() == pointDescription.getAuscultationPoll().getPollResults())) {
                oldApt.setDescription(pointDescription.getDescription().toString());
                oldApt.setPollResult(pointDescription.getAuscultationPoll().getPollResults());
                publicIcupadRepository.updateAuscultate(oldApt);
            }
        }
    }
}
