package com.icupad.stethoscope.switchview;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;

import com.icupad.commons.repository.builder.AuscultateSuiteTestBuilder;
import com.icupad.commons.repository.model.AuscultatePointTest;
import com.icupad.commons.repository.model.AuscultateSuiteTest;
import com.icupad.stethoscope.R;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.AuscultatePoint;
import com.icupad.commons.repository.model.SpinnerAuscultateSuiteSchema;
import com.icupad.stethoscope.connection.StethoscopeConnector;
import com.icupad.stethoscope.function.AuscultatePointToAuscultatePointTestFunction;
import com.icupad.stethoscope.model.ExaminationPosition;
import com.mmm.healthcare.scope.Stethoscope;

import java.util.ArrayList;
import java.util.List;

public class PrepareExaminationHelper extends ViewSwitchHelper {

    private static final int INITIAL_TEMPERATURE = 366;
    private static final String CELCIUS_DEGREE_SYMBOLS = "°C";
    private Activity activity;

    private List<AuscultatePoint> auscultatePoints = new ArrayList<>();
    private List<SpinnerAuscultateSuiteSchema> suiteSchemas;
    private AuscultateSuiteTest auscultateSuiteTest;
    private AuscultatePointToAuscultatePointTestFunction auscultatePointToAuscultatePointTestFunction
            = new AuscultatePointToAuscultatePointTestFunction();
    private int temperature;

    public PrepareExaminationHelper(StethoscopeViewSwitchListener viewSwitchListener, View view, PublicIcupadRepository publicIcupadRepository) {
        super(viewSwitchListener, view, publicIcupadRepository);
    }

    public void setup(Activity activity) {
        this.activity = activity;

        setupVisibilitiesAndTextsForPreviewSchemaView();
        setupSpinner();

        leftTopButton.setOnClickListener(new TableViewSetterOnClickListener());
        rightBottomButton.setOnClickListener(new PrepareSchemaViewOnClickListener());
        centerTopButton.setOnClickListener(new StethoscopeConnectorOnClickListenerWrapper());

        bigMinusTemperatureButton.setOnClickListener(new TemperatureChangerClickListener(-5));
        minusTemperatureButton.setOnClickListener(new TemperatureChangerClickListener(-1));
        plusTemperatureButton.setOnClickListener(new TemperatureChangerClickListener(1));
        bigPlusTemperatureButton.setOnClickListener(new TemperatureChangerClickListener(5));

        temperature = INITIAL_TEMPERATURE;
        updateTemperatureTextViewWithTemperature();

    }

    private void updateTemperatureTextViewWithTemperature() {
        temperatureTextView.setText("" + (temperature/10) + "." + (temperature%10) +  CELCIUS_DEGREE_SYMBOLS);
    }

    private void setupVisibilitiesAndTextsForPreviewSchemaView() {
        leftTopButton.setVisibility(View.VISIBLE);
        centerTopButton.setVisibility(View.VISIBLE);
        rightBottomButton.setVisibility(View.INVISIBLE);
        spinner.setVisibility(View.VISIBLE);
        stethoscopeErrorTextView.setVisibility(View.INVISIBLE);

        examinationTable.setVisibility(View.GONE);
        stethoscopeExaminationPoll.setVisibility(View.VISIBLE);
        stethoscopeView.setVisibility(View.INVISIBLE);

        //TODO labels
        leftTopButton.setText("WYNIKI");
        rightBottomButton.setText("KOPIUJ I EDYTUJ SCHEMAT");
        centerTopButton.setText("POŁĄCZ ZE STETOSKOPEM");
    }

    private void setupSpinner() {
        ArrayAdapter<String> adapter = getAuscultateSuiteSchemasArrayAdapter();
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AuscultationSuiteSchemaShowerOnClickListener());
    }

    @NonNull
    private ArrayAdapter<String> getAuscultateSuiteSchemasArrayAdapter() {
        suiteSchemas = publicIcupadRepository.findSyncedAuscultateSuiteSchemasForSpinnerPreparingExamination();
        String[] suiteSchemasNames = new String[suiteSchemas.size()];
        for (int i = 0; i < suiteSchemas.size(); i++) {
            suiteSchemasNames[i] = suiteSchemas.get(i).getName();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(view.getContext(),
                android.R.layout.simple_spinner_item, suiteSchemasNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter;
    }

    private class TableViewSetterOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            viewSwitchListener.switchToResults();
        }
    }

    private class PrepareSchemaViewOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            viewSwitchListener.switchToPrepareSchema(auscultatePoints);
        }
    }

    private class AuscultationSuiteSchemaShowerOnClickListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            auscultatePoints = suiteSchemas.get(i).getPoints();

            auscultateSuiteTest = AuscultateSuiteTestBuilder.anAuscultateSuiteTest()
                    .withAuscultateSuiteSchemaId(suiteSchemas.get(i).getId())
                    .withName(suiteSchemas.get(i).getName())
                    .withDescription("")
                    .build();

            for(AuscultatePoint ap : auscultatePoints) {
                AuscultatePointTest apt = auscultatePointToAuscultatePointTestFunction.apply(ap);
                auscultateSuiteTest.addAuctultatePointTest(apt);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private class StethoscopeConnectorOnClickListenerWrapper implements View.OnClickListener{
        StethoscopeConnectorOnClickListener stethoscopeConnectorOnClickListener;
        public StethoscopeConnectorOnClickListenerWrapper() {
            stethoscopeConnectorOnClickListener = new StethoscopeConnectorOnClickListener(PrepareExaminationHelper.this, activity);
        }
        @Override
        public void onClick(View view) {
            stethoscopeConnectorOnClickListener.onClick(view);
            Stethoscope stethoscope = StethoscopeConnector.getStethoscope();
            if(stethoscope != null) {
                updateAuscultateSuiteTestWithPollValues();
                viewSwitchListener.switchToExamination(stethoscope, auscultateSuiteTest);
            }
        }
    }

    private void updateAuscultateSuiteTestWithPollValues() {
        if(((RadioButton)stethoscopeExaminationPoll.findViewById(R.id.position_lie)).isChecked()) {
            auscultateSuiteTest.setPosition(ExaminationPosition.lie.getPositionId());
        }
        if(((RadioButton)stethoscopeExaminationPoll.findViewById(R.id.position_sit)).isChecked()) {
            auscultateSuiteTest.setPosition(ExaminationPosition.sit.getPositionId());
        }
        if(((RadioButton)stethoscopeExaminationPoll.findViewById(R.id.position_stand)).isChecked()) {
            auscultateSuiteTest.setPosition(ExaminationPosition.stand.getPositionId());
        }

        auscultateSuiteTest.setPassiveOxygenTherapy(
                ((RadioButton)stethoscopeExaminationPoll.findViewById(R.id.passive_oxygen_therapy_true)).isChecked());

        auscultateSuiteTest.setRespirated(
                ((RadioButton)stethoscopeExaminationPoll.findViewById(R.id.respiration_true)).isChecked());

        auscultateSuiteTest.setTemperature(temperature);
    }

    public List<AuscultatePoint> getAuscultatePoints() {
        return auscultatePoints;
    }

    private class TemperatureChangerClickListener implements View.OnClickListener {
        private int deltaPerClick;
        public TemperatureChangerClickListener(int deltaPerClick) {
            this.deltaPerClick = deltaPerClick;
        }

        @Override
        public void onClick(View view) {
            temperature += deltaPerClick;
            updateTemperatureTextViewWithTemperature();
        }
    }
}