package com.icupad.stethoscope.switchview;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.icupad.stethoscope.connection.StethoscopeConnector;
import com.icupad.stethoscope.model.StethoscopeMode;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.AuscultatePoint;
import com.icupad.commons.repository.model.SpinnerAuscultateSuiteSchema;

import java.util.ArrayList;
import java.util.List;

public class PreviewHelper extends ViewSwitchHelper {

    private List<AuscultatePoint> auscultatePoints = new ArrayList<>();
    private List<SpinnerAuscultateSuiteSchema> suiteSchemas;
    private StethoscopeConnector stethoscopeConnector = new StethoscopeConnector();

    public PreviewHelper(StethoscopeViewSwitchListener viewSwitchListener, View view, PublicIcupadRepository publicIcupadRepository) {
        super(viewSwitchListener, view, publicIcupadRepository);
    }

    public void setup() {
        stethoscopeView.setup(StethoscopeMode.PREVIEW, auscultatePoints);
        stethoscopeView.invalidate();

        setupVisibilitiesAndTextsForPreviewSchemaView();
        setupSpinner();

        leftTopButton.setOnClickListener(new TableViewSetterOnClickListener());
        rightBottomButton.setOnClickListener(new PrepareSchemaViewOnClickListener());
        centerTopButton.setOnClickListener(new StethoscopeConnectorOnClickListener());
    }

    private void setupVisibilitiesAndTextsForPreviewSchemaView() {
        leftTopButton.setVisibility(View.VISIBLE);
        centerTopButton.setVisibility(View.VISIBLE);
        rightBottomButton.setVisibility(View.VISIBLE);
        spinner.setVisibility(View.VISIBLE);
        stethoscopeErrorTextView.setVisibility(View.INVISIBLE);

        examinationTable.setVisibility(View.GONE);
        stethoscopeExaminationPoll.setVisibility(View.GONE);
        stethoscopeView.setVisibility(View.VISIBLE);

        leftTopButton.setText("WYNIKI");
        rightBottomButton.setText("KOPIUJ I EDYTUJ SCHEMAT");
        centerTopButton.setText("POŁĄCZ ZE STETOSKOPEM");
    }

    private void setupSpinner() {
        ArrayAdapter<String> adapter = getAuscultateSuiteSchemasArrayAdapter();
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AuscultationSuiteSchemaShowerOnClickListener());
    }

    @NonNull
    private ArrayAdapter<String> getAuscultateSuiteSchemasArrayAdapter() {
        suiteSchemas = publicIcupadRepository.findSyncedAuscultateSuiteSchemasForSpinnerPreparingExamination();
        String[] suiteSchemasNames = new String[suiteSchemas.size()];
        for (int i = 0; i < suiteSchemas.size(); i++) {
            suiteSchemasNames[i] = suiteSchemas.get(i).getName();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(view.getContext(),
                android.R.layout.simple_spinner_item, suiteSchemasNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter;
    }

    private class TableViewSetterOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            viewSwitchListener.switchToResults();
        }
    }

    private class PrepareSchemaViewOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            viewSwitchListener.switchToPrepareSchema(auscultatePoints);
        }
    }

    private class StethoscopeConnectorOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            stethoscopeConnector.initiateAndGetDevices((Activity)view.getContext());
        }
    }

    private class AuscultationSuiteSchemaShowerOnClickListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            auscultatePoints = suiteSchemas.get(i).getPoints();
            stethoscopeView.setup(StethoscopeMode.PREVIEW, auscultatePoints);
            stethoscopeView.invalidate();
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    public List<AuscultatePoint> getAuscultatePoints() {
        return auscultatePoints;
    }
}
