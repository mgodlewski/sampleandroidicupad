package com.icupad.stethoscope.switchview;

import android.view.View;

import com.icupad.commons.repository.PublicIcupadRepository;

public class ResultsHelper extends ViewSwitchHelper {

    public ResultsHelper(StethoscopeViewSwitchListener viewSwitchListener, View view, PublicIcupadRepository publicIcupadRepository) {
        super(viewSwitchListener, view, publicIcupadRepository);
    }

    public void setup() {
        setupVisibilitiesAndTextsForResultsView();
        centerTopButton.setOnClickListener(new PreviewExaminationViewSetterOnClickListener());
    }

    private void setupVisibilitiesAndTextsForResultsView() {
        leftTopButton.setVisibility(View.INVISIBLE);
        centerTopButton.setVisibility(View.VISIBLE);
        rightBottomButton.setVisibility(View.INVISIBLE);
        spinner.setVisibility(View.INVISIBLE);
        stethoscopeErrorTextView.setVisibility(View.INVISIBLE);

        examinationTable.setVisibility(View.VISIBLE);
        stethoscopeExaminationPoll.setVisibility(View.GONE);
        stethoscopeView.setVisibility(View.INVISIBLE);

        centerTopButton.setText("Nowe badanie");
    }

    private class PreviewExaminationViewSetterOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            viewSwitchListener.switchToPrepareTest();
        }
    }
}
