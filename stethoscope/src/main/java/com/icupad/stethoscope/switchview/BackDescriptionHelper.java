package com.icupad.stethoscope.switchview;

import android.app.Activity;
import android.view.View;

import com.icupad.commons.repository.model.AuscultatePointTest;
import com.icupad.stethoscope.connection.StethoscopeConnector;
import com.icupad.stethoscope.model.StethoscopeMode;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.AuscultateSuiteTest;
import com.icupad.stethoscope.painter.StethoscopeConstants;
import com.icupad.stethoscope.painter.StethoscopeView;
import com.icupad.stethoscope.painter.model.PointDescription;

public class BackDescriptionHelper extends ViewSwitchHelper {
    private AuscultateSuiteTest editedAuscultateSuiteTest;
    private AuscultateSuiteTest auscultateSuiteTest;

    public BackDescriptionHelper(StethoscopeViewSwitchListener viewSwitchListener, View view, PublicIcupadRepository publicIcupadRepository) {
        super(viewSwitchListener, view, publicIcupadRepository);
    }
    public void setup(AuscultateSuiteTest auscultateSuiteTest, AuscultateSuiteTest editedAuscultateSuiteTest, Activity activity) {
        this.auscultateSuiteTest = auscultateSuiteTest;
        this.editedAuscultateSuiteTest = editedAuscultateSuiteTest;

        setupVisibilitiesAndTextsForBackDescriptionView();
        stethoscopeView.setup(StethoscopeMode.BACK_DESCRIPTION, editedAuscultateSuiteTest, StethoscopeConstants.pointDoneState);
        stethoscopeView.invalidate();

        leftTopButton.setOnClickListener(new FrontDescriptionOnClickListener());
        rightBottomButton.setOnClickListener(new SaveOnClickListener(auscultateSuiteTest, editedAuscultateSuiteTest));

        if(StethoscopeConnector.isConnected()) {
            setViewsForConnectedStethoscope(activity);
        }
        else {
            setViewsForDisconnectedStethoscope(activity);
        }
    }

    private void setupVisibilitiesAndTextsForBackDescriptionView() {
        leftTopButton.setVisibility(View.VISIBLE);
        centerTopButton.setVisibility(View.VISIBLE);
        rightBottomButton.setVisibility(View.VISIBLE);
        spinner.setVisibility(View.INVISIBLE);
        stethoscopeErrorTextView.setVisibility(View.INVISIBLE);

        examinationTable.setVisibility(View.GONE);
        stethoscopeExaminationPoll.setVisibility(View.GONE);
        stethoscopeView.setVisibility(View.VISIBLE);

        leftTopButton.setText("PRZÓD");
        centerTopButton.setText("POŁĄCZ ZE STETOSKOPEM");
        rightBottomButton.setText("ZAPISZ OPISY");
    }

    private class FrontDescriptionOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            updateAuscultateSuiteTestWithStethoscopeView(editedAuscultateSuiteTest, stethoscopeView);
            viewSwitchListener.switchToFrontDescription(auscultateSuiteTest, editedAuscultateSuiteTest);
        }
    }

    @Override
    public void setViewsForDisconnectedStethoscope(Activity activity) {
        stethoscopeErrorTextView.setText("");
        stethoscopeView.invalidate();
        centerTopButton.setOnClickListener(
                new StethoscopeConnectorOnClickListener(this, activity));
        centerTopButton.setText("Połącz ze stetoskopem");
    }

    @Override
    public void setViewsForConnectedStethoscope(Activity activity) {
        stethoscopeErrorTextView.setText("Połączono z urządzeniem: " + StethoscopeConnector.getStethoscope().getName());
        stethoscopeView.invalidate();
        centerTopButton.setOnClickListener(
                new StethoscopeDisconnectorOnClickListener(this, activity));
        centerTopButton.setText("Rozłącz");
    }

    private class SaveOnClickListener implements View.OnClickListener {
        AuscultateSuiteTest auscultateSuiteTest;
        AuscultateSuiteTest editedAuscultateSuiteTest;

        SaveOnClickListener(AuscultateSuiteTest auscultateSuiteTest, AuscultateSuiteTest editedAuscultateSuiteTest) {
            this.auscultateSuiteTest = auscultateSuiteTest;
            this.editedAuscultateSuiteTest = editedAuscultateSuiteTest;
        }

        @Override
        public void onClick(View view) {
            if( !auscultateSuiteTest.getDescription().equals(editedAuscultateSuiteTest.getDescription())) {
                publicIcupadRepository.updateAuscultateSuite(editedAuscultateSuiteTest);
            }
            for(int i = 0; i < auscultateSuiteTest.getAuscultatePointTests().size(); i++) {
                updateAuscultateSuiteTestWithStethoscopeView(editedAuscultateSuiteTest, stethoscopeView);
            }
            viewSwitchListener.switchToResults();
        }
    }

    private void updateAuscultateSuiteTestWithStethoscopeView(AuscultateSuiteTest ast, StethoscopeView sv) {
        for(int i = 0; i < ast.getAuscultatePointTests().size(); i++) {
            AuscultatePointTest oldApt = ast.getAuscultatePointTests().get(i);
            PointDescription pointDescription = sv.getPointsDescriptionPanel().getPointDescriptionsValues().get(i);

            if( !(oldApt.getDescription().equals(pointDescription.getDescription().toString()) && oldApt.getPollResult() == pointDescription.getAuscultationPoll().getPollResults())) {
                oldApt.setDescription(pointDescription.getDescription().toString());
                oldApt.setPollResult(pointDescription.getAuscultationPoll().getPollResults());
                publicIcupadRepository.updateAuscultate(oldApt);
            }
        }
    }
}
