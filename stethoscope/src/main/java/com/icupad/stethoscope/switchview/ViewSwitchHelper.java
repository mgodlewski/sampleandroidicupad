package com.icupad.stethoscope.switchview;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.icupad.stethoscope.R;
import com.icupad.stethoscope.painter.StethoscopeView;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.grable.GrableView;

public abstract class ViewSwitchHelper {
    protected StethoscopeViewSwitchListener viewSwitchListener;
    protected View view;
    protected PublicIcupadRepository publicIcupadRepository;

    protected Button leftTopButton;
    protected Button centerTopButton;
    protected Button rightBottomButton;
    protected Spinner spinner;
    protected TextView stethoscopeErrorTextView;

    protected GrableView examinationTable;
    protected LinearLayout stethoscopeExaminationPoll;
    protected StethoscopeView stethoscopeView;

    protected TextView temperatureTextView;
    protected Button bigMinusTemperatureButton;
    protected Button minusTemperatureButton;
    protected Button plusTemperatureButton;
    protected Button bigPlusTemperatureButton;

    public ViewSwitchHelper(StethoscopeViewSwitchListener viewSwitchListener, View view, PublicIcupadRepository publicIcupadRepository) {
        this.viewSwitchListener = viewSwitchListener;
        this.view = view;
        this.publicIcupadRepository = publicIcupadRepository;

        leftTopButton = (Button) view.findViewById(R.id.leftTopButton);
        centerTopButton = (Button) view.findViewById(R.id.centerTopButton);
        rightBottomButton = (Button) view.findViewById(R.id.rightBottomButton);
        spinner = (Spinner) view.findViewById(R.id.spinner);
        stethoscopeErrorTextView = (TextView) view.findViewById(R.id.stethoscope_error_text_view);

        examinationTable = (GrableView) view.findViewById(R.id.examinationsTable);
        stethoscopeExaminationPoll = (LinearLayout) view.findViewById(R.id.stethoscope_examination_poll_on_start);
        stethoscopeView = (StethoscopeView) view.findViewById(R.id.examinationCanvas);

        temperatureTextView = (TextView)stethoscopeExaminationPoll.findViewById(R.id.temperature);
        bigMinusTemperatureButton = (Button)stethoscopeExaminationPoll.findViewById(R.id.big_minus_temperature_button);
        minusTemperatureButton = (Button)stethoscopeExaminationPoll.findViewById(R.id.minus_temperature_button);
        plusTemperatureButton = (Button)stethoscopeExaminationPoll.findViewById(R.id.plus_temperature_button);
        bigPlusTemperatureButton = (Button)stethoscopeExaminationPoll.findViewById(R.id.big_plus_temperature_button);
    }

    public void setViewsForDisconnectedStethoscope(Activity activity) {
    }

    public void setViewsForConnectedStethoscope(Activity activity) {

    }
}
