package com.icupad.stethoscope.switchview;

import android.view.View;

import com.icupad.stethoscope.painter.function.PointsWithCoefficientToSuiteSchemaFunction;
import com.icupad.stethoscope.model.StethoscopeMode;
import com.icupad.commons.repository.PublicIcupadRepository;
import com.icupad.commons.repository.model.AuscultatePoint;
import com.icupad.commons.repository.model.SpinnerAuscultateSuiteSchema;

import java.util.ArrayList;
import java.util.List;

public class PrepareSchemaHelper extends ViewSwitchHelper{

    private List<AuscultatePoint> auscultatePoints = new ArrayList<>();

    public PrepareSchemaHelper(StethoscopeViewSwitchListener viewSwitchListener, View view, PublicIcupadRepository publicIcupadRepository) {
        super(viewSwitchListener, view, publicIcupadRepository);
    }

    public void setup(List<AuscultatePoint> auscultatePoints) {
        this.auscultatePoints = auscultatePoints;
        stethoscopeView.setup(StethoscopeMode.PREPARE, auscultatePoints);
        stethoscopeView.invalidate();

        setupVisibilitiesAndTextsForPrepareSchemaView();

        leftTopButton.setOnClickListener(new TableViewSetterOnClickListener());
        rightBottomButton.setOnClickListener(new AuscultationSuiteSchemaSaverOnClickListener());
    }

    private void setupVisibilitiesAndTextsForPrepareSchemaView() {
        leftTopButton.setVisibility(View.VISIBLE);
        centerTopButton.setVisibility(View.INVISIBLE);
        rightBottomButton.setVisibility(View.VISIBLE);
        spinner.setVisibility(View.INVISIBLE);
        stethoscopeErrorTextView.setVisibility(View.INVISIBLE);

        examinationTable.setVisibility(View.GONE);
        stethoscopeExaminationPoll.setVisibility(View.GONE);
        stethoscopeView.setVisibility(View.VISIBLE);

        leftTopButton.setText("WYNIKI");
        rightBottomButton.setText("ZAPISZ SCHEMAT");
    }

    private class AuscultationSuiteSchemaSaverOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
//                int selectedSuiteSchemaSpinnerId = spinner.getSelectedItemPosition();
//                SpinnerAuscultateSuiteSchema suiteSchema = suiteSchemas.get(selectedSuiteSchemaSpinnerId);
//                suiteS
            PointsWithCoefficientToSuiteSchemaFunction function = new PointsWithCoefficientToSuiteSchemaFunction();
            SpinnerAuscultateSuiteSchema spinnerAuscultateSuiteSchema = function.apply(
                    stethoscopeView.getPointsDescriptionPanel(),
                    stethoscopeView.getOriginalToUsedPatientPictureCoefficient(),
                    stethoscopeView.getFrontRightX()
            );
            spinnerAuscultateSuiteSchema.setName("nowy schemat (" + System.currentTimeMillis() + ")");
            publicIcupadRepository.saveAuscultateSuiteSchema(spinnerAuscultateSuiteSchema);
        }
    }

    private class TableViewSetterOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            viewSwitchListener.switchToResults();
        }
    }
}
