package com.icupad.stethoscope.recording;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import static com.icupad.stethoscope.recording.WavTransferConstants.*;

public class WavFileOutputStream extends ByteArrayOutputStream {
    private DataOutputStream outputStream;

    public WavFileOutputStream(FileOutputStream fos) {
        super();
        this.outputStream = new DataOutputStream(new BufferedOutputStream(fos));
    }

    @Override
    public void close() throws IOException {
        writeWav();
        super.close();
    }

    private void writeWav() throws IOException {
        byte[] data = this.toByteArray();
        int dataSize = data.length;
        long chunkSize = WavTransferConstants.HEADERS_SIZE + dataSize;
        int restOfFileSize = Integer.reverseBytes((int) chunkSize);

        outputStream.writeBytes(RIFF);
        outputStream.writeInt(restOfFileSize);
        outputStream.writeBytes(WAVE);
        outputStream.writeBytes(FMT);
        outputStream.writeInt(Integer.reverseBytes(PCM_CHUNK_SIZE));
        outputStream.writeShort(Short.reverseBytes(MY_FORMAT));
        outputStream.writeShort(Short.reverseBytes(MY_CHANNELS));
        outputStream.writeInt(Integer.reverseBytes(MY_SAMPLE_RATE));
        outputStream.writeInt(Integer.reverseBytes(MY_BYTE_RATE));
        outputStream.writeShort(Short.reverseBytes(MY_BLOCK_ALIGN));
        outputStream.writeShort(Short.reverseBytes(MY_BITS_PER_SAMPLE));
        outputStream.writeBytes(DATA_STRING);
        outputStream.writeInt(Integer.reverseBytes(dataSize));
        outputStream.write(data, 0, data.length);
        outputStream.close();
    }
}
