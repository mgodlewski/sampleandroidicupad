package com.icupad.stethoscope.recording;

import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class AudioWriterTask extends AsyncTask<Object, Integer, Void> {
    private OutputStream fileStream;

    protected Void doInBackground(Object... params) {
        this.fileStream = (WavFileOutputStream) params[0];
        InputStream stetStream = (InputStream) params[1];

        try {
            saveStream(stetStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void saveStream( InputStream stetStream) throws IOException {
        byte buffer[] = new byte[128];

        while (!isCancelled()) {
            if (stetStream.read(buffer) > 0) {
                fileStream.write(buffer);
            }
        }
        fileStream.close();
    }
}

