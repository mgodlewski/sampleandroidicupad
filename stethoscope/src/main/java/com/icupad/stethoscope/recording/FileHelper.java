package com.icupad.stethoscope.recording;

import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;

public class FileHelper {
    private final File directory;

    public FileHelper() {
        this.directory = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS), "icupad");
        this.directory.mkdirs();
    }

    public WavFileOutputStream createOutputFileStream(String file) {
        try {
            return new WavFileOutputStream(new FileOutputStream(new File(this.directory.getPath(), file)));
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    public File getInputFile(String file) {
        return new File(this.directory.getPath(), file);
    }
}