package com.icupad.stethoscope.recording;

public class WavTransferConstants {

    public static final int HEADERS_SIZE = 36;
    public static final String RIFF = "RIFF";
    public static final String WAVE = "WAVE";
    public static final String FMT = "fmt ";
    public static final int PCM_CHUNK_SIZE = 16;
    public static final String DATA_STRING = "data";
    public static final short MY_FORMAT = 1;
    public static final short MY_CHANNELS = 1;
    public static final int MY_SAMPLE_RATE = 4000;
    public static final short MY_BITS_PER_SAMPLE = 16;
    public static final int MY_BYTE_RATE = MY_SAMPLE_RATE * MY_CHANNELS * MY_BITS_PER_SAMPLE / 8;
    public static final short MY_BLOCK_ALIGN = (MY_CHANNELS * MY_BITS_PER_SAMPLE / 8);
}
