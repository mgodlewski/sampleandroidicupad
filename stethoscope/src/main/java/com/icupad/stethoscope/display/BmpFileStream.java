package com.icupad.stethoscope.display;

import android.graphics.Bitmap;
import android.graphics.Color;

import java.io.DataOutputStream;
import java.io.IOException;

public class BmpFileStream {
    public static final String BMP_HEADER = "BM";
    public static final int NO_COMPRESSION = 0;
    public static final int ZERO_SHORT = 0;
    public static final int DIB_SIZE = 40;
    public static final int WIDTH = 46;
    public static final int HEIGHT = 16;
    public static final int BYTES_PER_INFO = 3;
    public static final short COLOR_PLANES = 1;
    public static final short BITS_PER_PIXEL = 24;
    public static final int PRINT_RES = 2835;
    public static final int COLOR_PALETTE = 0;
    public static final int IMPORTANT_COLORS = 0;
    public static final int MAX_COLOR_VALUE = 255;
    public static final int MAX_PADDING = 4;
    public static final int HEADERS_SIZE = 54;
    private final DataOutputStream stream;

    public BmpFileStream(DataOutputStream stream) {
        this.stream = stream;
    }


    public void write(Bitmap bmp) throws IOException {

        int myDataSize = WIDTH  * HEIGHT * BYTES_PER_INFO;
        int paddingSize = MAX_PADDING - (WIDTH % MAX_PADDING) * HEIGHT;
        long chunkSize = HEADERS_SIZE + myDataSize + paddingSize;
        int restOfFileSize = Integer.reverseBytes((int) chunkSize);

        writeBmpHeader(restOfFileSize);
        writeDibHeader(myDataSize, paddingSize);
        writeData(bmp);

        stream.close();
    }
    private void writeBmpHeader(int restOfFileSize) throws IOException {
        stream.writeBytes(BMP_HEADER);
        stream.writeInt(restOfFileSize);

        stream.writeShort(ZERO_SHORT);
        stream.writeShort(ZERO_SHORT);

        stream.writeInt(Integer.reverseBytes(HEADERS_SIZE));
    }

    private void writeDibHeader(int myDataSize, int paddingSize) throws IOException {
        stream.writeInt(Integer.reverseBytes(DIB_SIZE));
        stream.writeInt(Integer.reverseBytes(WIDTH));
        stream.writeInt(Integer.reverseBytes(HEIGHT));
        stream.writeShort(Short.reverseBytes(COLOR_PLANES));
        stream.writeShort(Short.reverseBytes(BITS_PER_PIXEL));
        stream.writeInt(Integer.reverseBytes(NO_COMPRESSION));
        stream.writeInt(Integer.reverseBytes(myDataSize + paddingSize));
        stream.writeInt(Integer.reverseBytes(PRINT_RES));
        stream.writeInt(Integer.reverseBytes(PRINT_RES));
        stream.writeInt(Integer.reverseBytes(COLOR_PALETTE));
        stream.writeInt(Integer.reverseBytes(IMPORTANT_COLORS));
    }

    private void writeData(Bitmap bmp) throws IOException {
        for (int i = HEIGHT - 1; i >= 0; --i) {
            for (int j = 0; j < WIDTH; ++j) {
                int pixel = bmp.getPixel(j, i);
                writePixel(pixel);
            }
            stream.write(ZERO_SHORT);
            stream.write(ZERO_SHORT);
        }
    }

    private void writePixel(int pixel) throws IOException {
        int val = 0;
        if (Color.red(pixel) > 0) {
            val = 1;
        }
        for (int i = 0; i < BYTES_PER_INFO; ++i) {
            stream.write(val * MAX_COLOR_VALUE);
        }
    }
}
