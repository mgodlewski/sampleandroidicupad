package com.icupad.stethoscope.model;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.icupad.commons.utils.MathHelper;
import com.icupad.commons.utils.Point;
import com.icupad.stethoscope.R;
import com.icupad.stethoscope.painter.StethoscopeConstants;

import java.util.ArrayList;
import java.util.List;

public class AuscultationPoll {
    private List<AuscultationPollElement> auscultationPollElements;
    private boolean visible;
    private int height;
    private int deltaY;

    public AuscultationPoll(Context context) {
        auscultationPollElements = new ArrayList<>();
        for(String string : context.getResources().getStringArray(R.array.poll_values)) {
            auscultationPollElements.add(new AuscultationPollElement(string));
        }
    }

    public void resize(Point leftTop, int width) {
        Point lastRightTop = new Point(leftTop);
        for(AuscultationPollElement pollElement : auscultationPollElements) {
            if(lastRightTop.x + pollElement.getWidth() > leftTop.x + width) {
                lastRightTop = new Point(leftTop.x, lastRightTop.y + StethoscopeConstants.POLL_LABEL_HEIGHT);
            }
            pollElement.setLeftTopPoint(lastRightTop);
            lastRightTop = pollElement.getRightTop();
        }
        height = (int)(auscultationPollElements.get(auscultationPollElements.size()-1).getBottomY() - leftTop.y);
    }

    public void draw(Canvas canvas, int deltaY) {
        this.deltaY = deltaY;
        for(AuscultationPollElement pollElement : auscultationPollElements) {
            pollElement.draw(canvas, deltaY);
        }
    }

    public int getPollResults() {
        int result = 0;
        for(int i = 0; i < auscultationPollElements.size(); i++) {
            if(auscultationPollElements.get(i).isEnabled()) {
                result += 1 << i;
            }
        }
        return result;
    }

    public void setPollResults(int pollResults) {
        for(int i = 0; i < auscultationPollElements.size(); i++) {
            auscultationPollElements.get(i).setEnabled(pollResults % 2 == 1);
            pollResults /= 2;
        }
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public int getHeight() {
        return height;
    }

    public boolean isClicked(Point point) {
        for(AuscultationPollElement pointElement : auscultationPollElements) {
            if(MathHelper.isInRect(point, pointElement.getRect(deltaY))) {
                pointElement.switchEnable();
                return true;
            }
        }
        return false;
    }

}
