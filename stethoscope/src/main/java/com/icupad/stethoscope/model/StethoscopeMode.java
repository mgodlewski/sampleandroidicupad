package com.icupad.stethoscope.model;

public enum StethoscopeMode {
    PREVIEW, PREPARE, EXAMINATION, FRONT_DESCRIPTION, BACK_DESCRIPTION
}
