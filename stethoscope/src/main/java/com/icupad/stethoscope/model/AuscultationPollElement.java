package com.icupad.stethoscope.model;

import android.graphics.Canvas;
import android.graphics.Rect;

import com.icupad.commons.utils.Point;
import com.icupad.stethoscope.painter.StethoscopeConstants;

public class AuscultationPollElement {
    private String label;
    private int leftX;
    private int rightX;
    private int topY;
    private int bottomY;
    private boolean enabled;

    public AuscultationPollElement(String label) {
        this.label = label;
    }

    public int getHeight() {
        return StethoscopeConstants.FONT_SIZE_POLL_LABEL + 2 * StethoscopeConstants.POLL_LABEL_MARGIN;
    }

    public int getWidth() {
        return (int)StethoscopeConstants.POLL_LABEL_TEXT_PAINT.measureText(label)
                + 2 * StethoscopeConstants.POLL_LABEL_MARGIN;
    }

    public void setLeftTopPoint(Point point) {
        this.leftX = point.getIntX();
        this.topY = point.getIntY();
        this.rightX = this.leftX + getWidth();
        this.bottomY = this.topY + getHeight();
    }

    public void draw(Canvas canvas, int deltaY) {
        if(enabled) {
            canvas.drawRect(leftX, topY + deltaY, rightX, bottomY + deltaY, StethoscopeConstants.POLL_ELEMENT_ENABLED_PAINT_ENABLED_PAINT);
        }
        canvas.drawText(label,
                leftX + StethoscopeConstants.POLL_LABEL_MARGIN,
                topY + StethoscopeConstants.POLL_LABEL_TEXT_BOTTOM + deltaY,
                StethoscopeConstants.POLL_LABEL_TEXT_PAINT);
    }

    public void switchEnable() {
        this.enabled = !this.enabled;
    }

    public Point getRightTop() {
        return new Point(rightX, topY);
    }

    public boolean isEnabled() {
        return enabled;
    }

    public int getBottomY() {
        return bottomY;
    }

    public Rect getRect(int deltaY) {
        return new Rect(leftX, topY + deltaY, rightX, bottomY + deltaY);
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
