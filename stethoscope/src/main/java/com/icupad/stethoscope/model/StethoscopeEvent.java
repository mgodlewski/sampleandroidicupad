package com.icupad.stethoscope.model;

public class StethoscopeEvent {
    public final String message;
    public StethoscopeEvent(String message) {
        this.message = message;
    }
}
