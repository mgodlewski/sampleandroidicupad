package com.icupad.stethoscope.model;

public enum ExaminationPosition {
    lie(0), sit(1), stand(2);

    private int positionId;

    ExaminationPosition(int i) {
        positionId = i;
    }

    public int getPositionId() {
        return positionId;
    }
}
