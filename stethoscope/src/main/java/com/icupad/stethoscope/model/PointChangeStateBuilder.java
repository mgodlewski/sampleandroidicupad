package com.icupad.stethoscope.model;

import android.graphics.Paint;

public final class PointChangeStateBuilder {
    private Paint numberOnPatientPaint;
    private Paint circleOnPatientPaint;
    private int circleOnPatientRadius;

    private PointChangeStateBuilder() {
    }

    public static PointChangeStateBuilder aPointChangeState() {
        return new PointChangeStateBuilder();
    }

    public PointChangeStateBuilder withNumberOnPatientPaint(Paint numberOnPatientPaint) {
        this.numberOnPatientPaint = numberOnPatientPaint;
        return this;
    }

    public PointChangeStateBuilder withCircleOnPatientPaint(Paint circleOnPatientPaint) {
        this.circleOnPatientPaint = circleOnPatientPaint;
        return this;
    }

    public PointChangeStateBuilder withCircleOnPatientRadius(int circleOnPatientRadius) {
        this.circleOnPatientRadius = circleOnPatientRadius;
        return this;
    }

    public PointChangeState build() {
        PointChangeState pointChangeState = new PointChangeState();
        pointChangeState.setNumberOnPatientPaint(numberOnPatientPaint);
        pointChangeState.setCircleOnPatientPaint(circleOnPatientPaint);
        pointChangeState.setCircleOnPatientRadius(circleOnPatientRadius);
        return pointChangeState;
    }
}
