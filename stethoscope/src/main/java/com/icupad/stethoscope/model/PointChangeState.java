package com.icupad.stethoscope.model;

import android.graphics.Paint;

public class PointChangeState {
    private Paint numberOnPatientPaint;
    private Paint circleOnPatientPaint;
    private int circleOnPatientRadius;

    public Paint getNumberOnPatientPaint() {
        return numberOnPatientPaint;
    }

    public void setNumberOnPatientPaint(Paint numberOnPatientPaint) {
        this.numberOnPatientPaint = numberOnPatientPaint;
    }

    public Paint getCircleOnPatientPaint() {
        return circleOnPatientPaint;
    }

    public void setCircleOnPatientPaint(Paint circleOnPatientPaint) {
        this.circleOnPatientPaint = circleOnPatientPaint;
    }

    public int getCircleOnPatientRadius() {
        return circleOnPatientRadius;
    }

    public void setCircleOnPatientRadius(int circleOnPatientRadius) {
        this.circleOnPatientRadius = circleOnPatientRadius;
    }
}
