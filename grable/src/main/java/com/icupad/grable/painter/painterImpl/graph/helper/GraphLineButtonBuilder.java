package com.icupad.grable.painter.painterImpl.graph.helper;

public final class GraphLineButtonBuilder {
    private boolean available;
    private boolean visible;
    private String text;
    private int color;
    private int rowNo;
    private int leftX;
    private int topY;
    private int graphNo;
    private int position;

    private GraphLineButtonBuilder() {
    }

    public static GraphLineButtonBuilder aGraphLineButton() {
        return new GraphLineButtonBuilder();
    }

    public GraphLineButtonBuilder withAvailable(boolean available) {
        this.available = available;
        return this;
    }

    public GraphLineButtonBuilder withVisible(boolean visible) {
        this.visible = visible;
        return this;
    }

    public GraphLineButtonBuilder withText(String text) {
        this.text = text;
        return this;
    }

    public GraphLineButtonBuilder withColor(int color) {
        this.color = color;
        return this;
    }

    public GraphLineButtonBuilder withRowNo(int rowNo) {
        this.rowNo = rowNo;
        return this;
    }

    public GraphLineButtonBuilder withLeftX(int leftX) {
        this.leftX = leftX;
        return this;
    }

    public GraphLineButtonBuilder withTopY(int topY) {
        this.topY = topY;
        return this;
    }

    public GraphLineButtonBuilder withGraphNo(int graphNo) {
        this.graphNo = graphNo;
        return this;
    }

    public GraphLineButtonBuilder withPosition(int position) {
        this.position = position;
        return this;
    }

    public GraphLineButton build() {
        GraphLineButton graphLineButton = new GraphLineButton();
        graphLineButton.setAvailable(available);
        graphLineButton.setVisible(visible);
        graphLineButton.setText(text);
        graphLineButton.setColor(color);
        graphLineButton.setRowNo(rowNo);
        graphLineButton.setLeftX(leftX);
        graphLineButton.setTopY(topY);
        graphLineButton.setGraphNo(graphNo);
        graphLineButton.setPosition(position);
        graphLineButton.setTextAndWidthSettings();
        return graphLineButton;
    }
}
