package com.icupad.grable.painter;

import android.graphics.Canvas;
import android.view.View;

import com.icupad.grable.listener.OnCellClickListener;
import com.icupad.grable.listener.OnRowClickListener;

public interface Painter {
    void draw(Canvas canvas, View view);

    void onClick(float x, float y);

    void setDeltas(float deltaX, float deltaY);

    int getWidth();

    int getHeight();

    int getRowsNamesWidth();

    void setOnRowClickListener(OnRowClickListener onRowClickListener);

    void setOnCellClickListener(OnCellClickListener onCellClickListener);
}
