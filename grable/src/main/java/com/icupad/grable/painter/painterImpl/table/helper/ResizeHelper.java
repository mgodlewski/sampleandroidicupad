package com.icupad.grable.painter.painterImpl.table.helper;

import com.icupad.grable.model.Column;
import com.icupad.grable.model.Row;
import com.icupad.grable.painter.GrableConstants;

import java.util.List;

public class ResizeHelper {

    public int setRowHeightsAndReturnWholeHeight(List<Row> rows, List<Column> columns) {
        int columnNameLevels = getColumnNameLevels(columns);
        int height = columnNameLevels * GrableConstants.DEFAULT_NAME_ROW_HEIGHT + GrableConstants.LINE_WIDTH;
        for(int i = 0; i < rows.size(); i++) {
            Row row = rows.get(i);
            row.setHeight(GrableConstants.DEFAULT_NAME_ROW_HEIGHT);
            row.setTopY(height);
            height += GrableConstants.DEFAULT_NAME_ROW_HEIGHT + GrableConstants.LINE_WIDTH;
        }
        return height;
    }

    public int getColumnNameLevels(List<Column> columns) {
        int columnNameLevels = 0;
        for(Column column : columns) {
            columnNameLevels = Math.max(columnNameLevels, column.getColumnLevelsNo());
        }
        return columnNameLevels;
    }

    public int setColumnWidthsAndReturnWholeWidth(List<Row> rows, List<Column> columns) {
        int nameColumnWidth = getRowsNamesWidth(rows);
        int width = nameColumnWidth + GrableConstants.LINE_WIDTH;

        setColumnWidthsBasedOnColumnNamesAndDefaultWidths(columns);
        resetColumnWidthsBasedOnDataCellWidths(rows, columns);
        width = setColumnsLeftXAndReturnWholeWidth(columns, width);

        return width;
    }

    public int getRowsNamesWidth(List<Row> rows) {
        int nameColumnWidth = 0;
        for(Row row : rows) {
            if(row.getPrintedTitle() != null) {
                nameColumnWidth = Math.max(nameColumnWidth, (int) GrableConstants.TEXT_PAINT.measureText(row.getPrintedTitle()));
            }
        }
        if(nameColumnWidth > 0) {
            nameColumnWidth += GrableConstants.FIRST_COLUMN_MARGIN;
        }
        return nameColumnWidth;
    }

    private void setColumnWidthsBasedOnColumnNamesAndDefaultWidths(List<Column> columns) {
        for(Column column : columns) {
            int maxWidthInColumn = GrableConstants.DEFAULT_DATA_COLUMN_WIDTH;
            for(String s : column.getColumnLevels())
                maxWidthInColumn = Math.max(maxWidthInColumn,
                        (int)GrableConstants.TEXT_PAINT.measureText(s) + GrableConstants.MINIMUM_SPACE_BETWEEN_COLUMNS);
            column.setWidth(maxWidthInColumn);
        }
    }

    private void resetColumnWidthsBasedOnDataCellWidths(List<Row> rows, List<Column> columns) {
        for(Row row : rows) {
            for(int j = 0; j < columns.size(); j++) {
                Column column = columns.get(j);
                column.setWidth(
                        Math.max(column.getWidth(),
                                (int) GrableConstants.TEXT_PAINT.measureText(row.getCellValue(j))
                                        + GrableConstants.MINIMUM_SPACE_BETWEEN_COLUMNS) );
            }
        }
    }

    private int setColumnsLeftXAndReturnWholeWidth(List<Column> columns, int width) {
        for(Column column : columns) {
            column.setStartX(width);
            width += column.getWidth() + GrableConstants.LINE_WIDTH;
        }
        return width;
    }
}
