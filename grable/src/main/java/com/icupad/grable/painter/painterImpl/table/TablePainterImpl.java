package com.icupad.grable.painter.painterImpl.table;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

import com.icupad.grable.GrableView;
import com.icupad.grable.listener.OnCellClickListener;
import com.icupad.grable.listener.OnRowClickListener;
import com.icupad.grable.model.Column;
import com.icupad.grable.model.Row;
import com.icupad.grable.painter.GrableConstants;
import com.icupad.grable.painter.Painter;
import com.icupad.grable.painter.painterImpl.helper.VisibleAreaComputer;
import com.icupad.grable.painter.painterImpl.table.helper.ResizeHelper;

import java.util.List;

import static com.icupad.grable.painter.GrableConstants.CELL_BACKGROUND_MARGIN;

public class TablePainterImpl implements Painter {

    private final GrableView grableView;
    private List<Column> columns;
    private List<Row> rows;

    private int rowsNamesWidth;
    private int columnNameLevels;
    private int height;
    private int width;
    private float deltaX;
    private float deltaY;

    private int startingRow;
    private int endingRow;
    private int startingColumn;
    private int endingColumn;

    private OnRowClickListener onRowClickListener;
    private OnCellClickListener onCellClickListener;

    private ResizeHelper resizeHelper;
    private VisibleAreaComputer visibleAreaComputer;

    public TablePainterImpl(GrableView grableView, List<Column> columns, List<Row> rows) {
        this.grableView = grableView;
        this.columns = columns;
        this.rows = rows;


        resizeHelper = new ResizeHelper();
        columnNameLevels = resizeHelper.getColumnNameLevels(columns);
        rowsNamesWidth = resizeHelper.getRowsNamesWidth(rows);
        height = resizeHelper.setRowHeightsAndReturnWholeHeight(rows, columns);
        width = resizeHelper.setColumnWidthsAndReturnWholeWidth(rows, columns);
        visibleAreaComputer = new VisibleAreaComputer(rows, columns);
    }

    @Override
    public void draw(Canvas canvas, View view) {
        paintWholeTableBackground(canvas);
        paintEvenRowsBackground(canvas);
        computeStartingAndEndingRowsAndColumns();
        writeCellDataWithBackgroundColor(canvas);
        paintOverRowNames(canvas);
        paintOverColumnNames(canvas);
        writeRowNames(canvas);
        writeColumnNames(canvas);
        paintOverLeftTopCorner(canvas);
        drawBorderLines(canvas);
    }

    private void paintWholeTableBackground(Canvas canvas) {
        canvas.drawRect(0 + deltaX, 0 + deltaY, width + deltaX, height, GrableConstants.ODD_ROWS_PAINT);
    }

    private void paintEvenRowsBackground(Canvas canvas) {
        for (int j = 0; j < rows.size(); j += 2) {
            Row row = rows.get(j);
            canvas.drawRect(rowsNamesWidth + deltaX, row.getTopY() + deltaY, width + deltaX, row.getBottomY() + deltaY, GrableConstants.EVEN_ROWS_PAINT);
        }
    }

    private void computeStartingAndEndingRowsAndColumns() {
        startingRow = visibleAreaComputer.getStartingRowForDeltaY(deltaY);
        endingRow =  visibleAreaComputer.getEndingRowForDeltaYAndMaxHaightAndAfterRow(
                deltaY, grableView.getHeight(), startingRow);
        startingColumn = visibleAreaComputer.getStartingColumnForDeltaX(deltaX);
        endingColumn = visibleAreaComputer.getEndingColumnForDeltaXAndMaxWidthAndAfterColumn(
                deltaX, grableView.getWidth(), startingColumn);
    }

    private void writeCellDataWithBackgroundColor(Canvas canvas) {
        Paint paint = new Paint();
        for(int i = startingColumn; i < endingColumn; i++) {
            Column column = columns.get(i);
            for (int j = startingRow; j < endingRow; j++) {
                Row row = rows.get(j);
                if (row.getBackgroundColor(i) != null) {
                    paint.setColor(row.getBackgroundColor(i));
                    canvas.drawRect(column.getStartX() + deltaX,
                            row.getTopY() + CELL_BACKGROUND_MARGIN + deltaY,
                            column.getStartX() - CELL_BACKGROUND_MARGIN + column.getWidth() + deltaX,
                            row.getBottomY() - CELL_BACKGROUND_MARGIN + deltaY,
                            paint);
                }
                canvas.drawText(row.getCellValue(i), column.getStartX() + deltaX, row.getTextY() + deltaY, GrableConstants.TEXT_PAINT);
            }
        }
    }

    private void paintOverRowNames(Canvas canvas) {
        canvas.drawRect(0, 0, rowsNamesWidth, height, GrableConstants.ODD_ROWS_PAINT);
        for (int j = 0; j < rows.size(); j += 2) {
            Row row = rows.get(j);
            canvas.drawRect(0, row.getTopY() + deltaY, rowsNamesWidth, row.getBottomY() + deltaY, GrableConstants.EVEN_ROWS_PAINT);
        }
    }

    private void paintOverColumnNames(Canvas canvas) {
        canvas.drawRect(0 + deltaX, 0, width + deltaX, GrableConstants.DEFAULT_NAME_ROW_HEIGHT * columnNameLevels + GrableConstants.LINE_WIDTH, GrableConstants.ODD_ROWS_PAINT);
    }

    private void writeRowNames(Canvas canvas) {
        for (int i = startingRow; i < endingRow; i++) {
            Row row = rows.get(i);
            canvas.drawText(row.getPrintedTitle(), 0, row.getTextY() + deltaY, GrableConstants.TEXT_PAINT);
        }
    }

    private void writeColumnNames(Canvas canvas) {
        for (int columnId = startingColumn; columnId < endingColumn; columnId++) {
            for (int columnLevel = 0; columnLevel < columnNameLevels; columnLevel++) {
                Column column = columns.get(columnId);
                if (isColumnNameVisible(column, columnId, columnLevel)) {
                    writeColumnName(canvas, column, columnId, columnLevel);
                }
            }
        }
    }

    private boolean isColumnNameVisible(Column column, int columnId, int columnLevel) {
        if(columnId == startingColumn) {
            return true;
        }
        boolean hasDifferentColumnNameThanPreviousColumn
                = !columns.get(columnId - 1).getColumnLevel(columnLevel).equals(column.getColumnLevel(columnLevel));

        float previuosColumnRightX = columns.get(columnId-1).getStartX()
                + columns.get(columnId - 1).getWidth() + deltaX + GrableConstants.LINE_WIDTH;
        boolean isFirstColumnVisibleWithThatColumnName = previuosColumnRightX < rowsNamesWidth;

        return hasDifferentColumnNameThanPreviousColumn || isFirstColumnVisibleWithThatColumnName;
    }

    private void writeColumnName(Canvas canvas, Column column, int columnId, int columnLevel) {
        float x = column.getStartX() + deltaX;
        float y = GrableConstants.DEFAULT_NAME_ROW_HEIGHT * (columnLevel + 1) - GrableConstants.DEFAULT_TEXT_SIZE / 2;

        if (x > rowsNamesWidth) {
            canvas.drawText(column.getColumnLevel(columnLevel), x, y, GrableConstants.TEXT_PAINT);
        }
        else if (isNextColumnVisible(columnId)) {
            if (hasNextColumnEqualName(column, columnId, columnLevel)) {
                canvas.drawText(column.getColumnLevel(columnLevel), rowsNamesWidth, y, GrableConstants.TEXT_PAINT);
            } else {
                canvas.drawText(column.getColumnLevel(columnLevel), x, y, GrableConstants.TEXT_PAINT);
            }
        }
    }

    private boolean isNextColumnVisible(int columnId) {
        return columnId + 1 < columns.size() && columns.get(columnId + 1).getStartX() + deltaX > rowsNamesWidth;
    }

    private boolean hasNextColumnEqualName(Column column, int columnId, int columnLevel) {
        return columns.get(columnId + 1).getColumnLevel(columnLevel).equals(column.getColumnLevel(columnLevel));
    }

    private void paintOverLeftTopCorner(Canvas canvas) {
        canvas.drawRect(0, 0, rowsNamesWidth, GrableConstants.DEFAULT_NAME_ROW_HEIGHT * columnNameLevels, GrableConstants.ODD_ROWS_PAINT);
    }

    private void drawBorderLines(Canvas canvas) {
        if (rowsNamesWidth > 0) {
            canvas.drawLine(rowsNamesWidth, 0, rowsNamesWidth, height, GrableConstants.TABLE_BORDER_PAINT);
            canvas.drawLine(0 + deltaX, GrableConstants.DEFAULT_NAME_ROW_HEIGHT * columnNameLevels, width + deltaX, GrableConstants.DEFAULT_NAME_ROW_HEIGHT * columnNameLevels, GrableConstants.TABLE_BORDER_PAINT);
        }
    }

    @Override
    public void onClick(float x, float y) {
        Integer clickedRowId = getClickedRowId(y);
        Integer clickedColumnId = getClickedColumnId(x);
        if(onCellClickListener != null && clickedRowId >= 0 && clickedColumnId >= 0 && clickedColumnId != null && clickedRowId != null) {
            onCellClickListener.onCellClick(clickedColumnId, clickedRowId);
        }

        if(onRowClickListener != null && clickedRowId != null) {
            onRowClickListener.onRowClick(clickedRowId);
        }
    }

    private Integer getClickedRowId(float y) {
        if(y < columnNameLevels * GrableConstants.DEFAULT_NAME_ROW_HEIGHT + GrableConstants.LINE_WIDTH) {
            return -1;
        }
        else {
            for(int i = 0; i < rows.size(); i++) {
                Row row = rows.get(i);
                if(row.getTopY() + deltaY <= y && row.getBottomY() + deltaY >= y) {
                    return i;
                }
            }
        }
        return null;
    }

    private Integer getClickedColumnId(float x) {
        if(x < rowsNamesWidth) {
            return -1;
        }
        else {
            for(int i = 0; i < columns.size(); i++) {
                Column column = columns.get(i);
                if(column.getStartX() + deltaX <= x && column.getStartX() + column.getWidth() + deltaX >= x) {
                    return i;
                }
            }
        }
        return null;
    }

    @Override
    public void setDeltas(float deltaX, float deltaY) {
        this.deltaX = deltaX;
        this.deltaY = deltaY;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    public int getRowsNamesWidth() {
        return rowsNamesWidth;
    }

    @Override
    public void setOnRowClickListener(OnRowClickListener onRowClickListener) {
        this.onRowClickListener = onRowClickListener;
    }

    @Override
    public void setOnCellClickListener(OnCellClickListener onCellClickListener) {
        this.onCellClickListener = onCellClickListener;
    }
}
