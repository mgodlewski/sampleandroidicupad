package com.icupad.grable.painter.painterImpl.graph.helper;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

import com.icupad.grable.GrableView;

public class GraphLineButton {
    public static final int DEFAULT_WIDTH = 100;
    public static final int DEFAULT_HEIGHT = GrableView.DEFAULT_TEXT_SIZE*3;

    private boolean available;
    private boolean visible;
    private String text;
    private int color;
    private int originalColor;
    private int rowNo;
    private int leftX;
    private int topY;
    private int graphNo;
    private int position;
    private int width;
    private int textX;
    private int textY;
    private Paint textPaint;


    public void setTextAndWidthSettings() {
        textPaint = new Paint();
        textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        textPaint.setTextSize(GrableView.DEFAULT_TEXT_SIZE);

        width = Math.max(DEFAULT_WIDTH, (int)(textPaint.measureText(text) + 20));
        textX = leftX + (int)((width - textPaint.measureText(text))/2);
        textY = topY + 2 * GrableView.DEFAULT_TEXT_SIZE;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean toggleVisible() {
        if(visible) {
            visible = false;
        } else {
            visible = true;
        }
        return visible;
    }

    public boolean isVisible() {
        return visible;
    }

    public String getText() {
        return text;
    }

    public int getColor() {
        if(visible)
            return color;
        else
            return getColorWithChange(0.3);
    }

    public int getOriginalColor() {
        return originalColor;
    }

    public int getRowNo() {
        return rowNo;
    }

    public int getGraphNo() {
        return graphNo;
    }

    public int getPosition() {
        return position;
    }

    public int getLeftX() {
        return leftX;
    }

    public int getTopY() {
        return topY;
    }

    public int getRightX() {
        return leftX + width;
    }

    public int getBottomY() {
        return topY + DEFAULT_HEIGHT;
    }

    public int getTextX() {
        return textX;
    }

    public int getTextY() {
        return textY;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setColor(int color) {
        this.originalColor = color;
        this.color = color;
    }

    public void setRowNo(int rowNo) {
        this.rowNo = rowNo;
    }

    public void setLeftX(int leftX) {
        this.leftX = leftX;
    }

    public void setTopY(int topY) {
        this.topY = topY;
    }

    public void setGraphNo(int graphNo) {
        this.graphNo = graphNo;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Paint getTextPaint() {
        if(visible)
            textPaint.setColor(Color.BLACK);
        else
            textPaint.setColor(Color.GRAY);
        return textPaint;
    }

    private int getColorWithChange(double i) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[1] = (float)Math.max(0, hsv[1] - i);
        return Color.HSVToColor(hsv);
    }
}