package com.icupad.grable.painter;

import android.graphics.Color;
import android.graphics.Paint;

public class GrableConstants {
    public static final int FIRST_COLUMN_MARGIN = 16;
    public static final int LINE_WIDTH = 3;
    public static final int DEFAULT_TEXT_SIZE = 18;
    public static final int DEFAULT_NAME_ROW_HEIGHT = DEFAULT_TEXT_SIZE * 2;
    public static final Paint TEXT_PAINT;
    public static final Paint EVEN_ROWS_PAINT;
    public static final Paint ODD_ROWS_PAINT;
    public static final Paint TABLE_BORDER_PAINT;

    private static final int ODD_ROWS_COLOR = Color.WHITE;
    private static final int EVEN_ROWS_COLOR = Color.parseColor("#BBBBBB");
    private static final int TABLE_BORDER_COLOR = Color.BLACK;

    public static final int COLUMN_NAME_COLOR = Color.parseColor("#BBBBBB");
    public static final int DEFAULT_DATA_COLUMN_WIDTH = 100;

    public static final int CELL_BACKGROUND_MARGIN = 4;
    public static final int MINIMUM_SPACE_BETWEEN_COLUMNS = 17;

    static {
        TEXT_PAINT = new Paint();
        TEXT_PAINT.setColor(Color.BLACK);
        TEXT_PAINT.setTextSize(DEFAULT_TEXT_SIZE);

        EVEN_ROWS_PAINT = new Paint();
        EVEN_ROWS_PAINT.setColor(ODD_ROWS_COLOR);

        ODD_ROWS_PAINT = new Paint();
        ODD_ROWS_PAINT.setColor(EVEN_ROWS_COLOR);

        TABLE_BORDER_PAINT = new Paint();
        TABLE_BORDER_PAINT.setColor(TABLE_BORDER_COLOR);
        TABLE_BORDER_PAINT.setStrokeWidth(LINE_WIDTH);
    }
}
