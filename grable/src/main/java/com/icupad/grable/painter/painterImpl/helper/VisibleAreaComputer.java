package com.icupad.grable.painter.painterImpl.helper;

import com.icupad.grable.model.Column;
import com.icupad.grable.model.Row;

import java.util.List;

public class VisibleAreaComputer {
    private List<Row> rows;
    private List<Column> columns;

    public VisibleAreaComputer(List<Row> rows, List<Column> columns) {
        this.rows = rows;
        this.columns = columns;
    }

    public int getStartingRowForDeltaY(float deltaY) {
        int startingRow = 0;
        for(int i = 0; i < rows.size(); i++) {
            Row row = rows.get(i);
            if(row.getBottomY() + deltaY > 0) {
                startingRow = Math.max(i - 1, 0);
                break;
            }
        }
        return startingRow;
    }

    public int getEndingRowForDeltaYAndMaxHaightAndAfterRow(float deltaY, int maxHeight, int startingRow) {
        int endingRow = 0;
        for(int i = startingRow; i < rows.size(); i++) {
            Row row = rows.get(i);
            if(row.getTopY() + deltaY > maxHeight) {
                endingRow = rows.size();
                break;
            }
        }
        return endingRow;
    }

    public int getStartingColumnForDeltaX(float deltaX) {
        int startingColumn = 0;
        for(int i = 0; i < columns.size(); i++) {
            Column column = columns.get(i);
            if(column.getStartX() + column.getWidth() + deltaX > 0) {
                startingColumn = Math.max(i - 1, 0);
                break;
            }
        }
        return startingColumn;
    }

    public int getEndingColumnForDeltaXAndMaxWidthAndAfterColumn(float deltaX, int maxWidth, int startingColumn) {
        int endingColumn = columns.size();
        for(int i = startingColumn; i<columns.size(); i++){
            Column column = columns.get(i);
            if (column.getStartX() + deltaX > maxWidth) {
                endingColumn = i+1;
                break;
            }
        }
        return endingColumn;

    }
}
