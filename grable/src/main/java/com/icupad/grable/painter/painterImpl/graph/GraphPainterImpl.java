package com.icupad.grable.painter.painterImpl.graph;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

import com.icupad.grable.GrableView;
import com.icupad.grable.listener.OnCellClickListener;
import com.icupad.grable.listener.OnRowClickListener;
import com.icupad.grable.model.Cell;
import com.icupad.grable.model.Column;
import com.icupad.grable.model.Row;
import com.icupad.grable.painter.Painter;
import com.icupad.grable.painter.painterImpl.graph.helper.GraphLineButton;
import com.icupad.grable.painter.painterImpl.graph.helper.GraphLineButtonBuilder;
import com.icupad.grable.painter.painterImpl.helper.VisibleAreaComputer;

import java.util.ArrayList;
import java.util.List;

public class GraphPainterImpl implements Painter {
    private static final int DEFAULT_FIRST_COLUMN_WIDTH = 16;
    private static final int LINE_WIDTH = 3;
    public static final int DEFAULT_TEXT_SIZE = 18;
    private static final int DEFAULT_FIRST_ROW_HEIGHT = DEFAULT_TEXT_SIZE * 2;
    private static final String RED_COLOR = "#F25858";
    private static final String BLUE_COLOR = "#5FA3DA";
    private static final String GREEN_COLOR = "#60BD67";
    private static final String PURPLE_COLOR = "#B476B2";
    private static final String YELLOW_COLOR = "#E0CE43";
    private static final String BROWN_COLOR = "#B29030";
    private static final String PINK_COLOR = "#F27CB1";
    private static final String ORANGE_COLOR = "#F8A53D";
    private static final String BLACK_COLOR = "#4E4C4D";
    private static final String GRAY_COLOR = "#C9C9C9";
    private static final int BUTTON_MARGIN = 20;
    private static final int MAX_VISIBLE_BUTTON_NO = 9;
    private GrableView grableView;

    private List<Column> columns;
    private List<Row> rows;

    private int firstColumnWidth;
    private int firstRowsNo;
    private int height = 100;
    private int width = 100;
    private float deltaX;
    private float deltaY;

    private float maxValue;
    private float minValue;

    private int graphTopY = 0;
    private int graphBottomY = 300;

    private Paint textPaint;
    private Paint backgroundPaint;
    private String[] colors = {RED_COLOR,  BLUE_COLOR, GREEN_COLOR, PURPLE_COLOR,
            YELLOW_COLOR, BROWN_COLOR, PINK_COLOR, ORANGE_COLOR, BLACK_COLOR, };
    private String notUsedColor = GRAY_COLOR;

    private List<GraphLineButton> buttons = new ArrayList<>();

    private VisibleAreaComputer visibleAreaComputer;

    public GraphPainterImpl(GrableView grableView, List<Column> columns, List<Row> rows) {
        this.grableView = grableView;
        this.columns = columns;
        this.rows = rows;

        textPaint = new Paint();
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(DEFAULT_TEXT_SIZE);

        backgroundPaint = new Paint();
        backgroundPaint.setColor(Color.parseColor("#EEEEEE"));

        visibleAreaComputer = new VisibleAreaComputer(rows, columns);

        createButtons();
        resize();
        getHeightOfUsedCanvas();
    }

    private void resize() {
        findMinAndMaxValues();
        getWidths();
    }

    private void findMinAndMaxValues() {
        maxValue = Float.MIN_VALUE;
        minValue = Float.MAX_VALUE;
        for(Row row : rows) {
            if(row.isVisible()) {
                minValue = Math.min(minValue, row.getMinValue());
                maxValue = Math.max(maxValue, row.getMaxValue());
            }
        }
    }

    private void getWidths() {
        firstColumnWidth = (int)Math.max(textPaint.measureText("" + minValue), textPaint.measureText(("" + maxValue)));
        width = firstColumnWidth + LINE_WIDTH;

        int maxWidthInColumn;
        firstRowsNo = 0;
        for(int i = 0; i < columns.size(); i++) {
            Column column = columns.get(i);
            column.setStartX(width);
            maxWidthInColumn = 0;
            for(int j = 0; j < column.getColumnLevelsNo(); j++)
                maxWidthInColumn = Math.max(maxWidthInColumn, (int)textPaint.measureText(column.getColumnLevel(j)));
            column.setWidth(maxWidthInColumn);
            width += maxWidthInColumn + LINE_WIDTH;

            firstRowsNo = Math.max(firstRowsNo, column.getColumnLevelsNo());
        }
    }

    private void createButtons() {
        int newButtonY = graphBottomY + DEFAULT_TEXT_SIZE * firstRowsNo + 20;
        int newButtonX = BUTTON_MARGIN;
        for(int i = 0; i < rows.size(); i++) {
            if(newButtonX + textPaint.measureText(rows.get(i).getPrintedTitle()) + 20 > grableView.getWidth()) {
                newButtonX = BUTTON_MARGIN;
                newButtonY += 20 + GraphLineButton.DEFAULT_HEIGHT;
            }
            GraphLineButton button;
            if(i < MAX_VISIBLE_BUTTON_NO) {
                button = GraphLineButtonBuilder.aGraphLineButton()
                        .withText(rows.get(i).getPrintedTitle()).withColor(Color.parseColor(colors[i]))
                        .withRowNo(i).withGraphNo(0).withPosition(i).withLeftX(newButtonX).withTopY(newButtonY)
                        .withVisible(true).withAvailable(true).build();
                rows.get(i).setVisible(true);
            }
            else {
                button = GraphLineButtonBuilder.aGraphLineButton()
                        .withText(rows.get(i).getPrintedTitle()).withColor(Color.parseColor(notUsedColor))
                        .withRowNo(i).withGraphNo(0).withPosition(i).withLeftX(newButtonX).withTopY(newButtonY)
                        .withVisible(false).withAvailable(false).build();
                rows.get(i).setVisible(false);
            }
            buttons.add(button);
            newButtonX = button.getRightX() + BUTTON_MARGIN;
        }
    }

    private void getHeightOfUsedCanvas() {
        height = buttons.get(buttons.size()-1).getBottomY();
    }

    @Override
    public void draw(Canvas canvas, View view) {
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);

        writeColumnNamesWithFloatingMulticolumnNames(canvas);

        int startingRow = 0;
        int endingRow =  rows.size();
        int startingColumn = 0;
        int endingColumn = columns.size();

        for(int i = 0; i < rows.size(); i++) {
            Row row = rows.get(i);
            if(row.getBottomY() + deltaY > 0) {
                startingRow = Math.max(i - 1, 0);
                break;
            }
        }
        for(int i = startingRow; i < rows.size(); i++) {
            Row row = rows.get(i);
            if(row.getTopY() + deltaX > grableView.getHeight()) {
                endingRow = rows.size();
                break;
            }
        }

        for(int i = 0; i < columns.size(); i++) {
            Column column = columns.get(i);
            if(column.getStartX() + column.getWidth() + deltaX > 0) {
                startingColumn = Math.max(i - 1, 0);
                break;
            }
        }
        for(int i = startingColumn; i < columns.size(); i++) {
            Column column = columns.get(i);
            if(column.getStartX() + deltaX> grableView.getWidth()) {
                endingColumn = i + 1;
                break;
            }
        }

        //TODO refactor
        //draw points, lines and background markers indicating values being too low or too high
        //only for rows that are visible
        for(int j = startingRow; j < endingRow; j++) {
            Row row = rows.get(j);
            if(!row.isVisible())
                continue;
            Integer lastX = null;
            Integer lastY = null;
            for(int i = startingColumn; i < endingColumn; i++) {
                Column column = columns.get(i);
                Cell cell = row.getCell(i);

                if(cell != null && cell.isFloatValue()) {
                    int x = (int)(column.getStartX() + column.getWidth()/2);
                    int y = (int)((graphBottomY-graphTopY) * ((maxValue-Float.parseFloat(row.getCellValue(i)))/(maxValue-minValue)));
                    cell.setGraphX(x);
                    cell.setGraphY(y);
                    paint.setColor(buttons.get(j).getColor());
                    paint.setStyle(Paint.Style.FILL_AND_STROKE);
                    paint.setStrokeWidth(3);

                    canvas.drawCircle(x + deltaX, y + deltaY, 5, paint);
                    if(lastX != null) {
                        canvas.drawLine(lastX + deltaX, lastY + deltaY, x + deltaX, y + deltaY, paint);
                    }
                    if(cell.isWithBackground()){
                        paint.setColor(row.getBackgroundColor(i));
                        paint.setStyle(Paint.Style.STROKE);
                        canvas.drawRect(x - 8 + deltaX, y - 8 + deltaY, x + 8 + deltaX, y + 8 + deltaY, paint);
                    }

                    lastX = x;
                    lastY = y;
                }
            }
        }
        //paint over points and lines on the left of the graph
        canvas.drawRect(0, graphTopY, firstColumnWidth, height, backgroundPaint);

        drawGraphLinesAndYLabels(canvas, paint);
        drawButtons(canvas, paint);
    }

    //TODO refactor
    private void writeColumnNamesWithFloatingMulticolumnNames(Canvas canvas) {
        for (int i = 0; i < columns.size(); i++) {
            Column column = columns.get(i);
            float x = column.getStartX() + deltaX;
            for (int j = 0; j < firstRowsNo; j++) {
                float y = graphBottomY + DEFAULT_TEXT_SIZE * (firstRowsNo - j);
                //header can be displayed only if its first or if it's first with that value or if earlier one is not visible anymore
                if (i == 0 || !columns.get(i - 1).getColumnLevel(j).equals(column.getColumnLevel(j))
                        || columns.get(i-1).getStartX() + columns.get(i-1).getWidth() + deltaX + LINE_WIDTH < firstColumnWidth) {
                    if (x > firstColumnWidth) {
                        canvas.drawText(column.getColumnLevel(j), x, y + deltaY, textPaint);
                    } else if (i + 1 < columns.size() && columns.get(i + 1).getStartX() + deltaX > firstColumnWidth) {
                        if (columns.get(i + 1).getColumnLevel(j).equals(column.getColumnLevel(j))) {
                            canvas.drawText(column.getColumnLevel(j), firstColumnWidth, y + deltaY, textPaint);
                        } else {
                            canvas.drawText(column.getColumnLevel(j), x, y + deltaY, textPaint);
                        }
                    }
                }
            }
        }
    }

    private void drawGraphLinesAndYLabels(Canvas canvas, Paint paint) {
        paint.setColor(Color.BLACK);
        canvas.drawLine(firstColumnWidth, graphTopY + deltaY, firstColumnWidth, graphBottomY + deltaY, paint);
        canvas.drawLine(firstColumnWidth, graphBottomY + deltaY, graphTopY + width + deltaX, graphBottomY + deltaY, paint);
        canvas.drawText("" + minValue, 0, graphBottomY + deltaY, textPaint);
        canvas.drawText(String.format("%.2f", minValue + (maxValue-minValue)*0.33), 0, DEFAULT_TEXT_SIZE + (float)((graphBottomY-graphTopY - DEFAULT_TEXT_SIZE)*(1 - 0.33)) + deltaY, textPaint);
        canvas.drawText(String.format("%.2f", minValue + (maxValue-minValue)*0.66), 0, DEFAULT_TEXT_SIZE + (float)((graphBottomY-graphTopY - DEFAULT_TEXT_SIZE)*(1 - 0.66)) + deltaY, textPaint);
        canvas.drawText("" + maxValue, 0, graphTopY + DEFAULT_TEXT_SIZE + deltaY, textPaint);
    }

    private void drawButtons(Canvas canvas, Paint paint) {
        for(GraphLineButton button : buttons) {
            paint.setColor(button.getColor());
            paint.setStyle(Paint.Style.FILL_AND_STROKE);
            canvas.drawRect(button.getLeftX(), button.getTopY() + deltaY, button.getRightX(), button.getBottomY() + deltaY, paint);
            canvas.drawText(button.getText(), button.getTextX(), button.getTextY() + deltaY, button.getTextPaint());
        }
    }

    @Override
    public void onClick(float x, float y) {
        for(int i = 0; i < buttons.size(); i++) {
            GraphLineButton button = buttons.get(i);
            if(x > button.getLeftX() && x < button.getRightX() && y > button.getTopY() + deltaY && y < button.getBottomY() + deltaY) {
                if( !button.isAvailable()) {
                    for(int j = 0; j < buttons.size(); j++) {
                        GraphLineButton buttonToReplace = buttons.get(j);
                        if(buttonToReplace.isAvailable() && !buttonToReplace.isVisible()) {
                            buttonToReplace.setAvailable(false);
                            button.setAvailable(true);

                            button.setColor(buttonToReplace.getOriginalColor());
                            buttonToReplace.setColor(Color.parseColor(notUsedColor));
                            break;
                        }
                    }
                }

                if(button.isAvailable()) {
                    button.toggleVisible();
                    rows.get(i).setVisible(button.isVisible());
                    resize();
                }
                return;
            }
        }
    }

    @Override
    public void setDeltas(float deltaX, float deltaY) {
        this.deltaX = deltaX;
        this.deltaY = deltaY;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    public int getRowsNamesWidth() {
        return firstColumnWidth;
    }

    @Override
    public void setOnRowClickListener(OnRowClickListener onRowClickListener) {

    }

    @Override
    public void setOnCellClickListener(OnCellClickListener onCellClickListener) {

    }
}
