package com.icupad.grable;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.icupad.grable.listener.OnCellClickListener;
import com.icupad.grable.listener.OnRowClickListener;
import com.icupad.grable.model.Cell;
import com.icupad.grable.model.Column;
import com.icupad.grable.model.GrableMode;
import com.icupad.grable.model.Row;
import com.icupad.grable.painter.Painter;
import com.icupad.grable.painter.painterImpl.graph.GraphPainterImpl;
import com.icupad.grable.painter.painterImpl.table.TablePainterImpl;

import java.util.ArrayList;
import java.util.List;

public class GrableView extends View {
    public static final int DEFAULT_TEXT_SIZE = 18;
    public static final int RIGHT_MARGIN = 30;
    private static final int MAX_DIFFERENCE_IN_AXIS_FOR_CLICK = 15;

    private List<Column> columns = new ArrayList<>();
    private List<Row> rows = new ArrayList<>();

    private GrableMode mode;
    private Painter painter;

    private OnRowClickListener onRowClickListener;
    private OnCellClickListener onCellClickListener;

    private float startX;
    private float startY;
    private float deltaX = 0;
    private float deltaY = 0;
    private float oldDeltaX;
    private float oldDeltaY;

    private int widthMeasureSpec;
    private int heightMeasureSpec;

    public GrableView(Context context) {
        super(context);
        setWillNotDraw(false);
    }

    public GrableView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setWillNotDraw(false);
    }

    public void addColumns(List<String[]> columnLevelsList) {
        for(String[] columnLevels : columnLevelsList) {
            addColumn(columnLevels);
        }
    }

    public void addColumn(String[] columnLevels) {
        Column column = new Column(columnLevels);
        columns.add(column);
    }

    public void addRows(List<String> titles) {
        for(String title : titles) {
            addRow(title);
        }
    }

    public void addRow(String title) {
        Row row = new Row(title);
        rows.add(row);
    }

    public void addCellAtColumnAtRow(Cell cell, int column, int row) {
        rows.get(row - 1).addCellAtColumnNo(cell, column - 1);
    }

    public void addAllCells(Cell[][] data) {
        for(int i = 0; i < data.length; i++) {
            for(int j = 0; j < data[i].length; j++) {
                rows.get(i).addCell(data[i][j]);
            }
        }
    }

    public void updateCellBackgroundColorAtXAtY(Integer backgroundColor, int x, int y) {
        Cell cell = getCell(x, y);
        cell.setBackgroundColor(backgroundColor);
        rows.get(y).updateCell(x, cell);
    }

    public void updateCellValueAtXAtY(String value, int x, int y) {
        Cell cell = getCell(x, y);
        cell.setValue(value);
        rows.get(y).updateCell(x, cell);
    }

    public Cell getCell(int x, int y) {
        return rows.get(y).getCell(x);
    }

    public void clear() {
        columns = new ArrayList<>();
        rows = new ArrayList<>();
        setGrableMode(mode);
    }

    public void setGrableMode(GrableMode mode) {
        this.mode = mode;
        if(!hasNoData()) {
            if (mode == GrableMode.GRAPH) {
                painter = new GraphPainterImpl(this, columns, rows);
            }
            else {
                painter = new TablePainterImpl(this, columns, rows);
            }
            painter.setOnRowClickListener(onRowClickListener);
            painter.setOnCellClickListener(onCellClickListener);
            int width = View.MeasureSpec.makeMeasureSpec(Math.min(View.MeasureSpec.getSize(widthMeasureSpec),
                    painter.getWidth() + RIGHT_MARGIN), View.MeasureSpec.getMode(widthMeasureSpec));
            int height = View.MeasureSpec.makeMeasureSpec(Math.min(View.MeasureSpec.getSize(heightMeasureSpec),
                    painter.getHeight()), View.MeasureSpec.getMode(heightMeasureSpec));
            this.setMeasuredDimension(width, height);
        }
    }

    public void setPosition(int xPos) {
        if(xPos >= 0) {
            if(xPos < columns.size()) {
                deltaX = -(columns.get(xPos).getStartX() - painter.getRowsNamesWidth());
            }
            else {
                deltaX = -(columns.get(columns.size() - 1).getStartX() - painter.getRowsNamesWidth());
            }
        } else {
            if(xPos > -columns.size()) {
                deltaX = -(columns.get(columns.size() + xPos).getStartX() - painter.getRowsNamesWidth());
            }
            else {
                deltaX = -(columns.get(0).getStartX() - painter.getRowsNamesWidth());
            }
        }

        normalizeDeltasAndSetDeltasForPainter();
        invalidate();
    }

    private void normalizeDeltasAndSetDeltasForPainter() {
        deltaX = Math.max(Math.min(deltaX, 0), - (Math.max(painter.getWidth() - getWidth() + RIGHT_MARGIN, 0)));
        deltaY = Math.max(Math.min(deltaY, 0), - (Math.max(painter.getHeight() - getHeight(), 0)));
        painter.setDeltas(deltaX, deltaY);
    }

    public boolean hasNoData() {
        return (columns == null || columns.size() == 0);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        this.widthMeasureSpec = widthMeasureSpec;
        this.heightMeasureSpec = heightMeasureSpec;
        if(painter != null) {
            int width = View.MeasureSpec.makeMeasureSpec(Math.min(View.MeasureSpec.getSize(widthMeasureSpec), painter.getWidth() + RIGHT_MARGIN),
                    View.MeasureSpec.getMode(widthMeasureSpec));
            int height = View.MeasureSpec.makeMeasureSpec(Math.min(View.MeasureSpec.getSize(heightMeasureSpec), painter.getHeight()),
                    View.MeasureSpec.getMode(heightMeasureSpec));
            this.setMeasuredDimension(width, height);
        } else {
            this.setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(painter != null) {
            normalizeDeltasAndSetDeltasForPainter();
            painter.draw(canvas, this);
        }
    }

    //takes care of each swipe gesture
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        invalidate();
        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                startX = event.getX();
                startY = event.getY();
                oldDeltaX = deltaX;
                oldDeltaY = deltaY;
            } break;
            case MotionEvent.ACTION_MOVE: {
                deltaX = Math.max(Math.min(oldDeltaX + event.getX() - startX, 0), - (Math.max(painter.getWidth() - getWidth() + RIGHT_MARGIN,0)));
                deltaY = Math.max(Math.min(oldDeltaY + event.getY() - startY, 0), - (Math.max(painter.getHeight() - getHeight(), 0)));
                painter.setDeltas(deltaX, deltaY);
            } break;
            case MotionEvent.ACTION_UP: {
                if(Math.abs(event.getX() - startX) < MAX_DIFFERENCE_IN_AXIS_FOR_CLICK
                        && Math.abs(event.getY() - startY) < MAX_DIFFERENCE_IN_AXIS_FOR_CLICK) {
                    painter.onClick(startX, startY);
                }
            }
        }
        return true;
    }

    public void setOnRowClickListener(OnRowClickListener onRowClickListener) {
        this.onRowClickListener = onRowClickListener;
        if(painter != null) {
            painter.setOnRowClickListener(onRowClickListener);
        }
    }

    public void setOnCellClickListener(OnCellClickListener onCellClickListener) {
        this.onCellClickListener = onCellClickListener;
        if(painter != null) {
            painter.setOnCellClickListener(onCellClickListener);
        }
    }
}
