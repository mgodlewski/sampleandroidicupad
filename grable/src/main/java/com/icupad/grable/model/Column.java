package com.icupad.grable.model;

public class Column {
    private String[] columnLevels;
    private int width;
    private float startX;


    public Column(String[] columnLevels) {
        this.columnLevels = columnLevels;
    }
    public int getColumnLevelsNo() {
        return columnLevels.length;
    }
    public String getColumnLevel(int i) {
        return columnLevels[i];//TODO there could be wrong i
    }

    public int getWidth() {
        return width;
    }

    public float getStartX() {
        return startX;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setStartX(int startX) {
        this.startX = startX;
    }

    public String[] getColumnLevels() {
        return columnLevels;
    }
}
