package com.icupad.grable.model;

public enum GrableMode {
    GRAPH, TABLE
}
