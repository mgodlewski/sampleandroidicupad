package com.icupad.grable.model;

import com.icupad.grable.GrableView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class Row {
    private String title;
    private Map<Integer, Cell> data = new HashMap<>();
    private int height = GrableView.DEFAULT_TEXT_SIZE * 2;
    private float topY;
    private float textY;
    private float bottomY;
    private float maxValue = Float.MIN_VALUE;
    private float minValue = Float.MAX_VALUE;
    private float graphNumber; //abs value is number of graphs, if negative then switched off
    private boolean visible = true;

    public Row(String title) {
        this.title = title;
    }

    public void clearData() {
        data = new HashMap<>();
    }

    public void addCell(Cell cell) {
        data.put(data.size(), cell);
        if(cell != null && cell.getValue() != null) {
            try {
                cell.setFloatValue(false);
                maxValue = Math.max(maxValue, Float.parseFloat(cell.getValue()));
                minValue = Math.min(minValue, Float.parseFloat(cell.getValue()));
                cell.setFloatValue(true);
            }
            catch(NumberFormatException e) {
            }
        }
    }

    public void addCellAtColumnNo(Cell cell, int columnNo) {
        data.put(columnNo, cell);
        if(cell != null) {
            try {
                cell.setFloatValue(false);
                maxValue = Math.max(maxValue, Float.parseFloat(cell.getValue()));
                minValue = Math.min(minValue, Float.parseFloat(cell.getValue()));
                cell.setFloatValue(true);
            }
            catch(NumberFormatException e) {
//                System.out.println("Exception: " + e.getMessage());
            }
        }
    }

    public void updateCell(int x, Cell cell) {
        data.put(x, cell);
    }

    public Cell getCell(int i) {
        return data.get(i);
    }

    public String getCellValue(int i) {
        if(data.get(i) != null && data.get(i).getValue() != null)
            return data.get(i).getValue();
        else
            return "---";
    }
    public Integer getBackgroundColor(int i) {
        if(data.get(i) != null)
            return data.get(i).getBackgroundColor();
        else
            return 0;
    }

    public int getHeight() {
        return height;
    }

    public String getPrintedTitle() {
        return title;
    }

    public float getTopY() {
        return topY;
    }

    public float getTextY() {
        return textY;
    }

    public float getBottomY() {
        return bottomY;
    }

    public float getMaxValue() {
        return maxValue;
    }

    public float getMinValue() {
        return minValue;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public String toString() {
        return "Row{" +
                "title='" + title + '\'' +
                ", data=" + data +
                '}';
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setTopY(float topY) {
        this.topY = topY;
        this.bottomY = topY + height;
        this.textY = topY + (float)(height * 0.75);
        this.bottomY = this.topY + height;
    }
}