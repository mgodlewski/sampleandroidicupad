package com.icupad.grable.model;

public class Cell {
    private String value;
    private int backgroundColor;
    private boolean withBackground = false;
    private boolean floatValue;
    private int graphX;
    private int graphY;

    public Cell(String value) {
        this.value = value;
    }

    public Cell(String value, Integer backgroundColor) {
        this.value = value;
        if(backgroundColor != null) {
            this.backgroundColor = backgroundColor;
            this.withBackground = true;
        }
    }

    public String getValue() {
        return value;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public boolean isFloatValue() {
        return floatValue;
    }

    public int getGraphX() {
        return graphX;
    }

    public int getGraphY() {
        return graphY;
    }

    public void setFloatValue(boolean floatValue) {
        this.floatValue = floatValue;
    }

    public void setGraphX(int graphX) {
        this.graphX = graphX;
    }

    public void setGraphY(int graphY) {
        this.graphY = graphY;
    }

    public boolean isWithBackground() {
        return withBackground;
    }
}
