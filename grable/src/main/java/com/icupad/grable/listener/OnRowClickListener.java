package com.icupad.grable.listener;

public interface OnRowClickListener{
    public void onRowClick(int position);
}
