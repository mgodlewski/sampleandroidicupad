package com.icupad.grable.listener;

public interface OnCellClickListener {
    public void onCellClick(int x, int y);
}
